﻿
#region Using directives

using System;
using System.Collections;
using System.Collections.Specialized;


using System.Web.Configuration;
using System.Data;
using System.Data.Common;
using System.Configuration.Provider;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

using LMMR.Entities;
using LMMR.Data;
using LMMR.Data.Bases;

#endregion

namespace LMMR.Data.SqlClient
{
	/// <summary>
	/// This class is the Sql implementation of the NetTiersProvider.
	/// </summary>
	public sealed class SqlNetTiersProvider : LMMR.Data.Bases.NetTiersProvider
	{
		private static object syncRoot = new Object();
		private string _applicationName;
        private string _connectionString;
        private bool _useStoredProcedure;
        string _providerInvariantName;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SqlNetTiersProvider"/> class.
		///</summary>
		public SqlNetTiersProvider()
		{	
		}		
		
		/// <summary>
        /// Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        /// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
        /// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"></see> on a provider after the provider has already been initialized.</exception>
        /// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		public override void Initialize(string name, NameValueCollection config)
        {
            // Verify that config isn't null
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            // Assign the provider a default name if it doesn't have one
            if (String.IsNullOrEmpty(name))
            {
                name = "SqlNetTiersProvider";
            }

            // Add a default "description" attribute to config if the
            // attribute doesn't exist or is empty
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "NetTiers Sql provider");
            }

            // Call the base class's Initialize method
            base.Initialize(name, config);

            // Initialize _applicationName
            _applicationName = config["applicationName"];

            if (string.IsNullOrEmpty(_applicationName))
            {
                _applicationName = "/";
            }
            config.Remove("applicationName");


            #region "Initialize UseStoredProcedure"
            string storedProcedure  = config["useStoredProcedure"];
           	if (string.IsNullOrEmpty(storedProcedure))
            {
                throw new ProviderException("Empty or missing useStoredProcedure");
            }
            this._useStoredProcedure = Convert.ToBoolean(config["useStoredProcedure"]);
            config.Remove("useStoredProcedure");
            #endregion

			#region ConnectionString

			// Initialize _connectionString
			_connectionString = config["connectionString"];
			config.Remove("connectionString");

			string connect = config["connectionStringName"];
			config.Remove("connectionStringName");

			if ( String.IsNullOrEmpty(_connectionString) )
			{
				if ( String.IsNullOrEmpty(connect) )
				{
					throw new ProviderException("Empty or missing connectionStringName");
				}

				if ( DataRepository.ConnectionStrings[connect] == null )
				{
					throw new ProviderException("Missing connection string");
				}

				_connectionString = DataRepository.ConnectionStrings[connect].ConnectionString;
			}

            if ( String.IsNullOrEmpty(_connectionString) )
            {
                throw new ProviderException("Empty connection string");
			}

			#endregion
            
             #region "_providerInvariantName"

            // initialize _providerInvariantName
            this._providerInvariantName = config["providerInvariantName"];

            if (String.IsNullOrEmpty(_providerInvariantName))
            {
                throw new ProviderException("Empty or missing providerInvariantName");
            }
            config.Remove("providerInvariantName");

            #endregion

        }
		
		/// <summary>
		/// Creates a new <see cref="TransactionManager"/> instance from the current datasource.
		/// </summary>
		/// <returns></returns>
		public override TransactionManager CreateTransaction()
		{
			return new TransactionManager(this._connectionString);
		}
		
		/// <summary>
		/// Gets a value indicating whether to use stored procedure or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this repository use stored procedures; otherwise, <c>false</c>.
		/// </value>
		public bool UseStoredProcedure
		{
			get {return this._useStoredProcedure;}
			set {this._useStoredProcedure = value;}
		}
		
		 /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
		public string ConnectionString
		{
			get {return this._connectionString;}
			set {this._connectionString = value;}
		}
		
		/// <summary>
	    /// Gets or sets the invariant provider name listed in the DbProviderFactories machine.config section.
	    /// </summary>
	    /// <value>The name of the provider invariant.</value>
	    public string ProviderInvariantName
	    {
	        get { return this._providerInvariantName; }
	        set { this._providerInvariantName = value; }
	    }		
		
		///<summary>
		/// Indicates if the current <see cref="NetTiersProvider"/> implementation supports Transacton.
		///</summary>
		public override bool IsTransactionSupported
		{
			get
			{
				return true;
			}
		}

		
		#region "PackageMasterProvider"
			
		private SqlPackageMasterProvider innerSqlPackageMasterProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PackageMaster"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PackageMasterProviderBase PackageMasterProvider
		{
			get
			{
				if (innerSqlPackageMasterProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPackageMasterProvider == null)
						{
							this.innerSqlPackageMasterProvider = new SqlPackageMasterProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPackageMasterProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPackageMasterProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPackageMasterProvider SqlPackageMasterProvider
		{
			get {return PackageMasterProvider as SqlPackageMasterProvider;}
		}
		
		#endregion
		
		
		#region "PackageByHotelProvider"
			
		private SqlPackageByHotelProvider innerSqlPackageByHotelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PackageByHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PackageByHotelProviderBase PackageByHotelProvider
		{
			get
			{
				if (innerSqlPackageByHotelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPackageByHotelProvider == null)
						{
							this.innerSqlPackageByHotelProvider = new SqlPackageByHotelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPackageByHotelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPackageByHotelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPackageByHotelProvider SqlPackageByHotelProvider
		{
			get {return PackageByHotelProvider as SqlPackageByHotelProvider;}
		}
		
		#endregion
		
		
		#region "PackageItemsProvider"
			
		private SqlPackageItemsProvider innerSqlPackageItemsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PackageItems"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PackageItemsProviderBase PackageItemsProvider
		{
			get
			{
				if (innerSqlPackageItemsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPackageItemsProvider == null)
						{
							this.innerSqlPackageItemsProvider = new SqlPackageItemsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPackageItemsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPackageItemsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPackageItemsProvider SqlPackageItemsProvider
		{
			get {return PackageItemsProvider as SqlPackageItemsProvider;}
		}
		
		#endregion
		
		
		#region "ActualPackagePriceProvider"
			
		private SqlActualPackagePriceProvider innerSqlActualPackagePriceProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ActualPackagePrice"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ActualPackagePriceProviderBase ActualPackagePriceProvider
		{
			get
			{
				if (innerSqlActualPackagePriceProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlActualPackagePriceProvider == null)
						{
							this.innerSqlActualPackagePriceProvider = new SqlActualPackagePriceProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlActualPackagePriceProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlActualPackagePriceProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlActualPackagePriceProvider SqlActualPackagePriceProvider
		{
			get {return ActualPackagePriceProvider as SqlActualPackagePriceProvider;}
		}
		
		#endregion
		
		
		#region "PackageByHotelTrailProvider"
			
		private SqlPackageByHotelTrailProvider innerSqlPackageByHotelTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PackageByHotelTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PackageByHotelTrailProviderBase PackageByHotelTrailProvider
		{
			get
			{
				if (innerSqlPackageByHotelTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPackageByHotelTrailProvider == null)
						{
							this.innerSqlPackageByHotelTrailProvider = new SqlPackageByHotelTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPackageByHotelTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPackageByHotelTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPackageByHotelTrailProvider SqlPackageByHotelTrailProvider
		{
			get {return PackageByHotelTrailProvider as SqlPackageByHotelTrailProvider;}
		}
		
		#endregion
		
		
		#region "OthersProvider"
			
		private SqlOthersProvider innerSqlOthersProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Others"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OthersProviderBase OthersProvider
		{
			get
			{
				if (innerSqlOthersProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOthersProvider == null)
						{
							this.innerSqlOthersProvider = new SqlOthersProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOthersProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOthersProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOthersProvider SqlOthersProvider
		{
			get {return OthersProvider as SqlOthersProvider;}
		}
		
		#endregion
		
		
		#region "OtherItemsPricingbyHotelProvider"
			
		private SqlOtherItemsPricingbyHotelProvider innerSqlOtherItemsPricingbyHotelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OtherItemsPricingbyHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OtherItemsPricingbyHotelProviderBase OtherItemsPricingbyHotelProvider
		{
			get
			{
				if (innerSqlOtherItemsPricingbyHotelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOtherItemsPricingbyHotelProvider == null)
						{
							this.innerSqlOtherItemsPricingbyHotelProvider = new SqlOtherItemsPricingbyHotelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOtherItemsPricingbyHotelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOtherItemsPricingbyHotelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOtherItemsPricingbyHotelProvider SqlOtherItemsPricingbyHotelProvider
		{
			get {return OtherItemsPricingbyHotelProvider as SqlOtherItemsPricingbyHotelProvider;}
		}
		
		#endregion
		
		
		#region "PackageDescriptionProvider"
			
		private SqlPackageDescriptionProvider innerSqlPackageDescriptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PackageDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PackageDescriptionProviderBase PackageDescriptionProvider
		{
			get
			{
				if (innerSqlPackageDescriptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPackageDescriptionProvider == null)
						{
							this.innerSqlPackageDescriptionProvider = new SqlPackageDescriptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPackageDescriptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPackageDescriptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPackageDescriptionProvider SqlPackageDescriptionProvider
		{
			get {return PackageDescriptionProvider as SqlPackageDescriptionProvider;}
		}
		
		#endregion
		
		
		#region "PolicyHotelMappingProvider"
			
		private SqlPolicyHotelMappingProvider innerSqlPolicyHotelMappingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PolicyHotelMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PolicyHotelMappingProviderBase PolicyHotelMappingProvider
		{
			get
			{
				if (innerSqlPolicyHotelMappingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPolicyHotelMappingProvider == null)
						{
							this.innerSqlPolicyHotelMappingProvider = new SqlPolicyHotelMappingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPolicyHotelMappingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPolicyHotelMappingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPolicyHotelMappingProvider SqlPolicyHotelMappingProvider
		{
			get {return PolicyHotelMappingProvider as SqlPolicyHotelMappingProvider;}
		}
		
		#endregion
		
		
		#region "PackageItemMappingProvider"
			
		private SqlPackageItemMappingProvider innerSqlPackageItemMappingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PackageItemMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PackageItemMappingProviderBase PackageItemMappingProvider
		{
			get
			{
				if (innerSqlPackageItemMappingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPackageItemMappingProvider == null)
						{
							this.innerSqlPackageItemMappingProvider = new SqlPackageItemMappingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPackageItemMappingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPackageItemMappingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPackageItemMappingProvider SqlPackageItemMappingProvider
		{
			get {return PackageItemMappingProvider as SqlPackageItemMappingProvider;}
		}
		
		#endregion
		
		
		#region "PermissionsProvider"
			
		private SqlPermissionsProvider innerSqlPermissionsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Permissions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PermissionsProviderBase PermissionsProvider
		{
			get
			{
				if (innerSqlPermissionsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPermissionsProvider == null)
						{
							this.innerSqlPermissionsProvider = new SqlPermissionsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPermissionsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPermissionsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPermissionsProvider SqlPermissionsProvider
		{
			get {return PermissionsProvider as SqlPermissionsProvider;}
		}
		
		#endregion
		
		
		#region "OtherDescriptionLanguageProvider"
			
		private SqlOtherDescriptionLanguageProvider innerSqlOtherDescriptionLanguageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OtherDescriptionLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OtherDescriptionLanguageProviderBase OtherDescriptionLanguageProvider
		{
			get
			{
				if (innerSqlOtherDescriptionLanguageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOtherDescriptionLanguageProvider == null)
						{
							this.innerSqlOtherDescriptionLanguageProvider = new SqlOtherDescriptionLanguageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOtherDescriptionLanguageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOtherDescriptionLanguageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOtherDescriptionLanguageProvider SqlOtherDescriptionLanguageProvider
		{
			get {return OtherDescriptionLanguageProvider as SqlOtherDescriptionLanguageProvider;}
		}
		
		#endregion
		
		
		#region "PackageMasterDescriptionProvider"
			
		private SqlPackageMasterDescriptionProvider innerSqlPackageMasterDescriptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PackageMasterDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PackageMasterDescriptionProviderBase PackageMasterDescriptionProvider
		{
			get
			{
				if (innerSqlPackageMasterDescriptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPackageMasterDescriptionProvider == null)
						{
							this.innerSqlPackageMasterDescriptionProvider = new SqlPackageMasterDescriptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPackageMasterDescriptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPackageMasterDescriptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPackageMasterDescriptionProvider SqlPackageMasterDescriptionProvider
		{
			get {return PackageMasterDescriptionProvider as SqlPackageMasterDescriptionProvider;}
		}
		
		#endregion
		
		
		#region "NewsLetterSubscriberProvider"
			
		private SqlNewsLetterSubscriberProvider innerSqlNewsLetterSubscriberProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="NewsLetterSubscriber"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override NewsLetterSubscriberProviderBase NewsLetterSubscriberProvider
		{
			get
			{
				if (innerSqlNewsLetterSubscriberProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlNewsLetterSubscriberProvider == null)
						{
							this.innerSqlNewsLetterSubscriberProvider = new SqlNewsLetterSubscriberProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlNewsLetterSubscriberProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlNewsLetterSubscriberProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlNewsLetterSubscriberProvider SqlNewsLetterSubscriberProvider
		{
			get {return NewsLetterSubscriberProvider as SqlNewsLetterSubscriberProvider;}
		}
		
		#endregion
		
		
		#region "MeetingRoomProvider"
			
		private SqlMeetingRoomProvider innerSqlMeetingRoomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MeetingRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MeetingRoomProviderBase MeetingRoomProvider
		{
			get
			{
				if (innerSqlMeetingRoomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMeetingRoomProvider == null)
						{
							this.innerSqlMeetingRoomProvider = new SqlMeetingRoomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMeetingRoomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMeetingRoomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMeetingRoomProvider SqlMeetingRoomProvider
		{
			get {return MeetingRoomProvider as SqlMeetingRoomProvider;}
		}
		
		#endregion
		
		
		#region "MainPointProvider"
			
		private SqlMainPointProvider innerSqlMainPointProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MainPoint"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MainPointProviderBase MainPointProvider
		{
			get
			{
				if (innerSqlMainPointProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMainPointProvider == null)
						{
							this.innerSqlMainPointProvider = new SqlMainPointProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMainPointProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMainPointProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMainPointProvider SqlMainPointProvider
		{
			get {return MainPointProvider as SqlMainPointProvider;}
		}
		
		#endregion
		
		
		#region "LeftMenuLinkProvider"
			
		private SqlLeftMenuLinkProvider innerSqlLeftMenuLinkProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LeftMenuLink"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LeftMenuLinkProviderBase LeftMenuLinkProvider
		{
			get
			{
				if (innerSqlLeftMenuLinkProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLeftMenuLinkProvider == null)
						{
							this.innerSqlLeftMenuLinkProvider = new SqlLeftMenuLinkProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLeftMenuLinkProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLeftMenuLinkProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLeftMenuLinkProvider SqlLeftMenuLinkProvider
		{
			get {return LeftMenuLinkProvider as SqlLeftMenuLinkProvider;}
		}
		
		#endregion
		
		
		#region "LanguageProvider"
			
		private SqlLanguageProvider innerSqlLanguageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Language"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LanguageProviderBase LanguageProvider
		{
			get
			{
				if (innerSqlLanguageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLanguageProvider == null)
						{
							this.innerSqlLanguageProvider = new SqlLanguageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLanguageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLanguageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLanguageProvider SqlLanguageProvider
		{
			get {return LanguageProvider as SqlLanguageProvider;}
		}
		
		#endregion
		
		
		#region "MeetingRoomTrailProvider"
			
		private SqlMeetingRoomTrailProvider innerSqlMeetingRoomTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MeetingRoomTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MeetingRoomTrailProviderBase MeetingRoomTrailProvider
		{
			get
			{
				if (innerSqlMeetingRoomTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMeetingRoomTrailProvider == null)
						{
							this.innerSqlMeetingRoomTrailProvider = new SqlMeetingRoomTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMeetingRoomTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMeetingRoomTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMeetingRoomTrailProvider SqlMeetingRoomTrailProvider
		{
			get {return MeetingRoomTrailProvider as SqlMeetingRoomTrailProvider;}
		}
		
		#endregion
		
		
		#region "MeetingRoomConfigProvider"
			
		private SqlMeetingRoomConfigProvider innerSqlMeetingRoomConfigProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MeetingRoomConfig"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MeetingRoomConfigProviderBase MeetingRoomConfigProvider
		{
			get
			{
				if (innerSqlMeetingRoomConfigProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMeetingRoomConfigProvider == null)
						{
							this.innerSqlMeetingRoomConfigProvider = new SqlMeetingRoomConfigProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMeetingRoomConfigProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMeetingRoomConfigProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMeetingRoomConfigProvider SqlMeetingRoomConfigProvider
		{
			get {return MeetingRoomConfigProvider as SqlMeetingRoomConfigProvider;}
		}
		
		#endregion
		
		
		#region "MeetingRoomConfigTrailProvider"
			
		private SqlMeetingRoomConfigTrailProvider innerSqlMeetingRoomConfigTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MeetingRoomConfigTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MeetingRoomConfigTrailProviderBase MeetingRoomConfigTrailProvider
		{
			get
			{
				if (innerSqlMeetingRoomConfigTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMeetingRoomConfigTrailProvider == null)
						{
							this.innerSqlMeetingRoomConfigTrailProvider = new SqlMeetingRoomConfigTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMeetingRoomConfigTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMeetingRoomConfigTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMeetingRoomConfigTrailProvider SqlMeetingRoomConfigTrailProvider
		{
			get {return MeetingRoomConfigTrailProvider as SqlMeetingRoomConfigTrailProvider;}
		}
		
		#endregion
		
		
		#region "MeetingRoomDescProvider"
			
		private SqlMeetingRoomDescProvider innerSqlMeetingRoomDescProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MeetingRoomDesc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MeetingRoomDescProviderBase MeetingRoomDescProvider
		{
			get
			{
				if (innerSqlMeetingRoomDescProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMeetingRoomDescProvider == null)
						{
							this.innerSqlMeetingRoomDescProvider = new SqlMeetingRoomDescProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMeetingRoomDescProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMeetingRoomDescProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMeetingRoomDescProvider SqlMeetingRoomDescProvider
		{
			get {return MeetingRoomDescProvider as SqlMeetingRoomDescProvider;}
		}
		
		#endregion
		
		
		#region "MeetingRoomPictureVideoTrailProvider"
			
		private SqlMeetingRoomPictureVideoTrailProvider innerSqlMeetingRoomPictureVideoTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MeetingRoomPictureVideoTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MeetingRoomPictureVideoTrailProviderBase MeetingRoomPictureVideoTrailProvider
		{
			get
			{
				if (innerSqlMeetingRoomPictureVideoTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMeetingRoomPictureVideoTrailProvider == null)
						{
							this.innerSqlMeetingRoomPictureVideoTrailProvider = new SqlMeetingRoomPictureVideoTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMeetingRoomPictureVideoTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMeetingRoomPictureVideoTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMeetingRoomPictureVideoTrailProvider SqlMeetingRoomPictureVideoTrailProvider
		{
			get {return MeetingRoomPictureVideoTrailProvider as SqlMeetingRoomPictureVideoTrailProvider;}
		}
		
		#endregion
		
		
		#region "MeetingRoomPictureVideoProvider"
			
		private SqlMeetingRoomPictureVideoProvider innerSqlMeetingRoomPictureVideoProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MeetingRoomPictureVideo"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MeetingRoomPictureVideoProviderBase MeetingRoomPictureVideoProvider
		{
			get
			{
				if (innerSqlMeetingRoomPictureVideoProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMeetingRoomPictureVideoProvider == null)
						{
							this.innerSqlMeetingRoomPictureVideoProvider = new SqlMeetingRoomPictureVideoProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMeetingRoomPictureVideoProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMeetingRoomPictureVideoProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMeetingRoomPictureVideoProvider SqlMeetingRoomPictureVideoProvider
		{
			get {return MeetingRoomPictureVideoProvider as SqlMeetingRoomPictureVideoProvider;}
		}
		
		#endregion
		
		
		#region "PriorityBasketProvider"
			
		private SqlPriorityBasketProvider innerSqlPriorityBasketProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PriorityBasket"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PriorityBasketProviderBase PriorityBasketProvider
		{
			get
			{
				if (innerSqlPriorityBasketProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPriorityBasketProvider == null)
						{
							this.innerSqlPriorityBasketProvider = new SqlPriorityBasketProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPriorityBasketProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPriorityBasketProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPriorityBasketProvider SqlPriorityBasketProvider
		{
			get {return PriorityBasketProvider as SqlPriorityBasketProvider;}
		}
		
		#endregion
		
		
		#region "MeetingRoomDescTrailProvider"
			
		private SqlMeetingRoomDescTrailProvider innerSqlMeetingRoomDescTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MeetingRoomDescTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MeetingRoomDescTrailProviderBase MeetingRoomDescTrailProvider
		{
			get
			{
				if (innerSqlMeetingRoomDescTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMeetingRoomDescTrailProvider == null)
						{
							this.innerSqlMeetingRoomDescTrailProvider = new SqlMeetingRoomDescTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMeetingRoomDescTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMeetingRoomDescTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMeetingRoomDescTrailProvider SqlMeetingRoomDescTrailProvider
		{
			get {return MeetingRoomDescTrailProvider as SqlMeetingRoomDescTrailProvider;}
		}
		
		#endregion
		
		
		#region "RankingAlgoMasterProvider"
			
		private SqlRankingAlgoMasterProvider innerSqlRankingAlgoMasterProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="RankingAlgoMaster"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RankingAlgoMasterProviderBase RankingAlgoMasterProvider
		{
			get
			{
				if (innerSqlRankingAlgoMasterProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRankingAlgoMasterProvider == null)
						{
							this.innerSqlRankingAlgoMasterProvider = new SqlRankingAlgoMasterProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRankingAlgoMasterProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRankingAlgoMasterProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRankingAlgoMasterProvider SqlRankingAlgoMasterProvider
		{
			get {return RankingAlgoMasterProvider as SqlRankingAlgoMasterProvider;}
		}
		
		#endregion
		
		
		#region "RankingAlgoConditionProvider"
			
		private SqlRankingAlgoConditionProvider innerSqlRankingAlgoConditionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="RankingAlgoCondition"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RankingAlgoConditionProviderBase RankingAlgoConditionProvider
		{
			get
			{
				if (innerSqlRankingAlgoConditionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRankingAlgoConditionProvider == null)
						{
							this.innerSqlRankingAlgoConditionProvider = new SqlRankingAlgoConditionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRankingAlgoConditionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRankingAlgoConditionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRankingAlgoConditionProvider SqlRankingAlgoConditionProvider
		{
			get {return RankingAlgoConditionProvider as SqlRankingAlgoConditionProvider;}
		}
		
		#endregion
		
		
		#region "UsersProvider"
			
		private SqlUsersProvider innerSqlUsersProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Users"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersProviderBase UsersProvider
		{
			get
			{
				if (innerSqlUsersProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersProvider == null)
						{
							this.innerSqlUsersProvider = new SqlUsersProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersProvider SqlUsersProvider
		{
			get {return UsersProvider as SqlUsersProvider;}
		}
		
		#endregion
		
		
		#region "StaticLebelProvider"
			
		private SqlStaticLebelProvider innerSqlStaticLebelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="StaticLebel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override StaticLebelProviderBase StaticLebelProvider
		{
			get
			{
				if (innerSqlStaticLebelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlStaticLebelProvider == null)
						{
							this.innerSqlStaticLebelProvider = new SqlStaticLebelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlStaticLebelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlStaticLebelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlStaticLebelProvider SqlStaticLebelProvider
		{
			get {return StaticLebelProvider as SqlStaticLebelProvider;}
		}
		
		#endregion
		
		
		#region "StaticLebelDescriptionProvider"
			
		private SqlStaticLebelDescriptionProvider innerSqlStaticLebelDescriptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="StaticLebelDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override StaticLebelDescriptionProviderBase StaticLebelDescriptionProvider
		{
			get
			{
				if (innerSqlStaticLebelDescriptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlStaticLebelDescriptionProvider == null)
						{
							this.innerSqlStaticLebelDescriptionProvider = new SqlStaticLebelDescriptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlStaticLebelDescriptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlStaticLebelDescriptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlStaticLebelDescriptionProvider SqlStaticLebelDescriptionProvider
		{
			get {return StaticLebelDescriptionProvider as SqlStaticLebelDescriptionProvider;}
		}
		
		#endregion
		
		
		#region "StaffAccountProvider"
			
		private SqlStaffAccountProvider innerSqlStaffAccountProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="StaffAccount"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override StaffAccountProviderBase StaffAccountProvider
		{
			get
			{
				if (innerSqlStaffAccountProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlStaffAccountProvider == null)
						{
							this.innerSqlStaffAccountProvider = new SqlStaffAccountProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlStaffAccountProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlStaffAccountProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlStaffAccountProvider SqlStaffAccountProvider
		{
			get {return StaffAccountProvider as SqlStaffAccountProvider;}
		}
		
		#endregion
		
		
		#region "StatisticsProvider"
			
		private SqlStatisticsProvider innerSqlStatisticsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Statistics"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override StatisticsProviderBase StatisticsProvider
		{
			get
			{
				if (innerSqlStatisticsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlStatisticsProvider == null)
						{
							this.innerSqlStatisticsProvider = new SqlStatisticsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlStatisticsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlStatisticsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlStatisticsProvider SqlStatisticsProvider
		{
			get {return StatisticsProvider as SqlStatisticsProvider;}
		}
		
		#endregion
		
		
		#region "SpecialPriceForBedroomProvider"
			
		private SqlSpecialPriceForBedroomProvider innerSqlSpecialPriceForBedroomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SpecialPriceForBedroom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SpecialPriceForBedroomProviderBase SpecialPriceForBedroomProvider
		{
			get
			{
				if (innerSqlSpecialPriceForBedroomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSpecialPriceForBedroomProvider == null)
						{
							this.innerSqlSpecialPriceForBedroomProvider = new SqlSpecialPriceForBedroomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSpecialPriceForBedroomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSpecialPriceForBedroomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSpecialPriceForBedroomProvider SqlSpecialPriceForBedroomProvider
		{
			get {return SpecialPriceForBedroomProvider as SqlSpecialPriceForBedroomProvider;}
		}
		
		#endregion
		
		
		#region "ZoneProvider"
			
		private SqlZoneProvider innerSqlZoneProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Zone"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ZoneProviderBase ZoneProvider
		{
			get
			{
				if (innerSqlZoneProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlZoneProvider == null)
						{
							this.innerSqlZoneProvider = new SqlZoneProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlZoneProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlZoneProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlZoneProvider SqlZoneProvider
		{
			get {return ZoneProvider as SqlZoneProvider;}
		}
		
		#endregion
		
		
		#region "TransferProvider"
			
		private SqlTransferProvider innerSqlTransferProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Transfer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TransferProviderBase TransferProvider
		{
			get
			{
				if (innerSqlTransferProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTransferProvider == null)
						{
							this.innerSqlTransferProvider = new SqlTransferProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTransferProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTransferProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTransferProvider SqlTransferProvider
		{
			get {return TransferProvider as SqlTransferProvider;}
		}
		
		#endregion
		
		
		#region "WhiteLabelProvider"
			
		private SqlWhiteLabelProvider innerSqlWhiteLabelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="WhiteLabel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override WhiteLabelProviderBase WhiteLabelProvider
		{
			get
			{
				if (innerSqlWhiteLabelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlWhiteLabelProvider == null)
						{
							this.innerSqlWhiteLabelProvider = new SqlWhiteLabelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlWhiteLabelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlWhiteLabelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlWhiteLabelProvider SqlWhiteLabelProvider
		{
			get {return WhiteLabelProvider as SqlWhiteLabelProvider;}
		}
		
		#endregion
		
		
		#region "WhiteLabelMappingWithHotelProvider"
			
		private SqlWhiteLabelMappingWithHotelProvider innerSqlWhiteLabelMappingWithHotelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="WhiteLabelMappingWithHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override WhiteLabelMappingWithHotelProviderBase WhiteLabelMappingWithHotelProvider
		{
			get
			{
				if (innerSqlWhiteLabelMappingWithHotelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlWhiteLabelMappingWithHotelProvider == null)
						{
							this.innerSqlWhiteLabelMappingWithHotelProvider = new SqlWhiteLabelMappingWithHotelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlWhiteLabelMappingWithHotelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlWhiteLabelMappingWithHotelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlWhiteLabelMappingWithHotelProvider SqlWhiteLabelMappingWithHotelProvider
		{
			get {return WhiteLabelMappingWithHotelProvider as SqlWhiteLabelMappingWithHotelProvider;}
		}
		
		#endregion
		
		
		#region "UserDetailsProvider"
			
		private SqlUserDetailsProvider innerSqlUserDetailsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UserDetails"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UserDetailsProviderBase UserDetailsProvider
		{
			get
			{
				if (innerSqlUserDetailsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUserDetailsProvider == null)
						{
							this.innerSqlUserDetailsProvider = new SqlUserDetailsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUserDetailsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUserDetailsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUserDetailsProvider SqlUserDetailsProvider
		{
			get {return UserDetailsProvider as SqlUserDetailsProvider;}
		}
		
		#endregion
		
		
		#region "SpecialPriceAndPromoTrailProvider"
			
		private SqlSpecialPriceAndPromoTrailProvider innerSqlSpecialPriceAndPromoTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SpecialPriceAndPromoTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SpecialPriceAndPromoTrailProviderBase SpecialPriceAndPromoTrailProvider
		{
			get
			{
				if (innerSqlSpecialPriceAndPromoTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSpecialPriceAndPromoTrailProvider == null)
						{
							this.innerSqlSpecialPriceAndPromoTrailProvider = new SqlSpecialPriceAndPromoTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSpecialPriceAndPromoTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSpecialPriceAndPromoTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSpecialPriceAndPromoTrailProvider SqlSpecialPriceAndPromoTrailProvider
		{
			get {return SpecialPriceAndPromoTrailProvider as SqlSpecialPriceAndPromoTrailProvider;}
		}
		
		#endregion
		
		
		#region "SpecialPriceAndPromoProvider"
			
		private SqlSpecialPriceAndPromoProvider innerSqlSpecialPriceAndPromoProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SpecialPriceAndPromo"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SpecialPriceAndPromoProviderBase SpecialPriceAndPromoProvider
		{
			get
			{
				if (innerSqlSpecialPriceAndPromoProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSpecialPriceAndPromoProvider == null)
						{
							this.innerSqlSpecialPriceAndPromoProvider = new SqlSpecialPriceAndPromoProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSpecialPriceAndPromoProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSpecialPriceAndPromoProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSpecialPriceAndPromoProvider SqlSpecialPriceAndPromoProvider
		{
			get {return SpecialPriceAndPromoProvider as SqlSpecialPriceAndPromoProvider;}
		}
		
		#endregion
		
		
		#region "SpandPpackageProvider"
			
		private SqlSpandPpackageProvider innerSqlSpandPpackageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SpandPpackage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SpandPpackageProviderBase SpandPpackageProvider
		{
			get
			{
				if (innerSqlSpandPpackageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSpandPpackageProvider == null)
						{
							this.innerSqlSpandPpackageProvider = new SqlSpandPpackageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSpandPpackageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSpandPpackageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSpandPpackageProvider SqlSpandPpackageProvider
		{
			get {return SpandPpackageProvider as SqlSpandPpackageProvider;}
		}
		
		#endregion
		
		
		#region "ServayDescriptionProvider"
			
		private SqlServayDescriptionProvider innerSqlServayDescriptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ServayDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ServayDescriptionProviderBase ServayDescriptionProvider
		{
			get
			{
				if (innerSqlServayDescriptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlServayDescriptionProvider == null)
						{
							this.innerSqlServayDescriptionProvider = new SqlServayDescriptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlServayDescriptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlServayDescriptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlServayDescriptionProvider SqlServayDescriptionProvider
		{
			get {return ServayDescriptionProvider as SqlServayDescriptionProvider;}
		}
		
		#endregion
		
		
		#region "SearchTracerProvider"
			
		private SqlSearchTracerProvider innerSqlSearchTracerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SearchTracer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SearchTracerProviderBase SearchTracerProvider
		{
			get
			{
				if (innerSqlSearchTracerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSearchTracerProvider == null)
						{
							this.innerSqlSearchTracerProvider = new SqlSearchTracerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSearchTracerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSearchTracerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSearchTracerProvider SqlSearchTracerProvider
		{
			get {return SearchTracerProvider as SqlSearchTracerProvider;}
		}
		
		#endregion
		
		
		#region "RpfProvider"
			
		private SqlRpfProvider innerSqlRpfProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Rpf"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RpfProviderBase RpfProvider
		{
			get
			{
				if (innerSqlRpfProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRpfProvider == null)
						{
							this.innerSqlRpfProvider = new SqlRpfProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRpfProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRpfProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRpfProvider SqlRpfProvider
		{
			get {return RpfProvider as SqlRpfProvider;}
		}
		
		#endregion
		
		
		#region "RolesProvider"
			
		private SqlRolesProvider innerSqlRolesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Roles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RolesProviderBase RolesProvider
		{
			get
			{
				if (innerSqlRolesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRolesProvider == null)
						{
							this.innerSqlRolesProvider = new SqlRolesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRolesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRolesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRolesProvider SqlRolesProvider
		{
			get {return RolesProvider as SqlRolesProvider;}
		}
		
		#endregion
		
		
		#region "ServeyAnswerProvider"
			
		private SqlServeyAnswerProvider innerSqlServeyAnswerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ServeyAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ServeyAnswerProviderBase ServeyAnswerProvider
		{
			get
			{
				if (innerSqlServeyAnswerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlServeyAnswerProvider == null)
						{
							this.innerSqlServeyAnswerProvider = new SqlServeyAnswerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlServeyAnswerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlServeyAnswerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlServeyAnswerProvider SqlServeyAnswerProvider
		{
			get {return ServeyAnswerProvider as SqlServeyAnswerProvider;}
		}
		
		#endregion
		
		
		#region "ServeyAnswerDescriptionProvider"
			
		private SqlServeyAnswerDescriptionProvider innerSqlServeyAnswerDescriptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ServeyAnswerDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ServeyAnswerDescriptionProviderBase ServeyAnswerDescriptionProvider
		{
			get
			{
				if (innerSqlServeyAnswerDescriptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlServeyAnswerDescriptionProvider == null)
						{
							this.innerSqlServeyAnswerDescriptionProvider = new SqlServeyAnswerDescriptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlServeyAnswerDescriptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlServeyAnswerDescriptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlServeyAnswerDescriptionProvider SqlServeyAnswerDescriptionProvider
		{
			get {return ServeyAnswerDescriptionProvider as SqlServeyAnswerDescriptionProvider;}
		}
		
		#endregion
		
		
		#region "SpandPbedroomProvider"
			
		private SqlSpandPbedroomProvider innerSqlSpandPbedroomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SpandPbedroom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SpandPbedroomProviderBase SpandPbedroomProvider
		{
			get
			{
				if (innerSqlSpandPbedroomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSpandPbedroomProvider == null)
						{
							this.innerSqlSpandPbedroomProvider = new SqlSpandPbedroomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSpandPbedroomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSpandPbedroomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSpandPbedroomProvider SqlSpandPbedroomProvider
		{
			get {return SpandPbedroomProvider as SqlSpandPbedroomProvider;}
		}
		
		#endregion
		
		
		#region "SpandPmeetingRoomProvider"
			
		private SqlSpandPmeetingRoomProvider innerSqlSpandPmeetingRoomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SpandPmeetingRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SpandPmeetingRoomProviderBase SpandPmeetingRoomProvider
		{
			get
			{
				if (innerSqlSpandPmeetingRoomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSpandPmeetingRoomProvider == null)
						{
							this.innerSqlSpandPmeetingRoomProvider = new SqlSpandPmeetingRoomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSpandPmeetingRoomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSpandPmeetingRoomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSpandPmeetingRoomProvider SqlSpandPmeetingRoomProvider
		{
			get {return SpandPmeetingRoomProvider as SqlSpandPmeetingRoomProvider;}
		}
		
		#endregion
		
		
		#region "ServeyQuestionProvider"
			
		private SqlServeyQuestionProvider innerSqlServeyQuestionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ServeyQuestion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ServeyQuestionProviderBase ServeyQuestionProvider
		{
			get
			{
				if (innerSqlServeyQuestionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlServeyQuestionProvider == null)
						{
							this.innerSqlServeyQuestionProvider = new SqlServeyQuestionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlServeyQuestionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlServeyQuestionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlServeyQuestionProvider SqlServeyQuestionProvider
		{
			get {return ServeyQuestionProvider as SqlServeyQuestionProvider;}
		}
		
		#endregion
		
		
		#region "ServeyResponseProvider"
			
		private SqlServeyResponseProvider innerSqlServeyResponseProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ServeyResponse"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ServeyResponseProviderBase ServeyResponseProvider
		{
			get
			{
				if (innerSqlServeyResponseProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlServeyResponseProvider == null)
						{
							this.innerSqlServeyResponseProvider = new SqlServeyResponseProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlServeyResponseProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlServeyResponseProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlServeyResponseProvider SqlServeyResponseProvider
		{
			get {return ServeyResponseProvider as SqlServeyResponseProvider;}
		}
		
		#endregion
		
		
		#region "InvoiceCommissionProvider"
			
		private SqlInvoiceCommissionProvider innerSqlInvoiceCommissionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="InvoiceCommission"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override InvoiceCommissionProviderBase InvoiceCommissionProvider
		{
			get
			{
				if (innerSqlInvoiceCommissionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlInvoiceCommissionProvider == null)
						{
							this.innerSqlInvoiceCommissionProvider = new SqlInvoiceCommissionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlInvoiceCommissionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlInvoiceCommissionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlInvoiceCommissionProvider SqlInvoiceCommissionProvider
		{
			get {return InvoiceCommissionProvider as SqlInvoiceCommissionProvider;}
		}
		
		#endregion
		
		
		#region "ServeyresultProvider"
			
		private SqlServeyresultProvider innerSqlServeyresultProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Serveyresult"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ServeyresultProviderBase ServeyresultProvider
		{
			get
			{
				if (innerSqlServeyresultProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlServeyresultProvider == null)
						{
							this.innerSqlServeyresultProvider = new SqlServeyresultProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlServeyresultProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlServeyresultProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlServeyresultProvider SqlServeyresultProvider
		{
			get {return ServeyresultProvider as SqlServeyresultProvider;}
		}
		
		#endregion
		
		
		#region "CurrencyProvider"
			
		private SqlCurrencyProvider innerSqlCurrencyProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Currency"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CurrencyProviderBase CurrencyProvider
		{
			get
			{
				if (innerSqlCurrencyProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCurrencyProvider == null)
						{
							this.innerSqlCurrencyProvider = new SqlCurrencyProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCurrencyProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCurrencyProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCurrencyProvider SqlCurrencyProvider
		{
			get {return CurrencyProvider as SqlCurrencyProvider;}
		}
		
		#endregion
		
		
		#region "BedRoomProvider"
			
		private SqlBedRoomProvider innerSqlBedRoomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BedRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BedRoomProviderBase BedRoomProvider
		{
			get
			{
				if (innerSqlBedRoomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBedRoomProvider == null)
						{
							this.innerSqlBedRoomProvider = new SqlBedRoomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBedRoomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBedRoomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBedRoomProvider SqlBedRoomProvider
		{
			get {return BedRoomProvider as SqlBedRoomProvider;}
		}
		
		#endregion
		
		
		#region "BookingProvider"
			
		private SqlBookingProvider innerSqlBookingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Booking"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BookingProviderBase BookingProvider
		{
			get
			{
				if (innerSqlBookingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBookingProvider == null)
						{
							this.innerSqlBookingProvider = new SqlBookingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBookingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBookingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBookingProvider SqlBookingProvider
		{
			get {return BookingProvider as SqlBookingProvider;}
		}
		
		#endregion
		
		
		#region "BedRoomPictureImageProvider"
			
		private SqlBedRoomPictureImageProvider innerSqlBedRoomPictureImageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BedRoomPictureImage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BedRoomPictureImageProviderBase BedRoomPictureImageProvider
		{
			get
			{
				if (innerSqlBedRoomPictureImageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBedRoomPictureImageProvider == null)
						{
							this.innerSqlBedRoomPictureImageProvider = new SqlBedRoomPictureImageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBedRoomPictureImageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBedRoomPictureImageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBedRoomPictureImageProvider SqlBedRoomPictureImageProvider
		{
			get {return BedRoomPictureImageProvider as SqlBedRoomPictureImageProvider;}
		}
		
		#endregion
		
		
		#region "CountryProvider"
			
		private SqlCountryProvider innerSqlCountryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Country"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CountryProviderBase CountryProvider
		{
			get
			{
				if (innerSqlCountryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCountryProvider == null)
						{
							this.innerSqlCountryProvider = new SqlCountryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCountryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCountryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCountryProvider SqlCountryProvider
		{
			get {return CountryProvider as SqlCountryProvider;}
		}
		
		#endregion
		
		
		#region "BookingLogsProvider"
			
		private SqlBookingLogsProvider innerSqlBookingLogsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BookingLogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BookingLogsProviderBase BookingLogsProvider
		{
			get
			{
				if (innerSqlBookingLogsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBookingLogsProvider == null)
						{
							this.innerSqlBookingLogsProvider = new SqlBookingLogsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBookingLogsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBookingLogsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBookingLogsProvider SqlBookingLogsProvider
		{
			get {return BookingLogsProvider as SqlBookingLogsProvider;}
		}
		
		#endregion
		
		
		#region "CmsProvider"
			
		private SqlCmsProvider innerSqlCmsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Cms"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CmsProviderBase CmsProvider
		{
			get
			{
				if (innerSqlCmsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCmsProvider == null)
						{
							this.innerSqlCmsProvider = new SqlCmsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCmsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCmsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCmsProvider SqlCmsProvider
		{
			get {return CmsProvider as SqlCmsProvider;}
		}
		
		#endregion
		
		
		#region "CancelationPolicyProvider"
			
		private SqlCancelationPolicyProvider innerSqlCancelationPolicyProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CancelationPolicy"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CancelationPolicyProviderBase CancelationPolicyProvider
		{
			get
			{
				if (innerSqlCancelationPolicyProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCancelationPolicyProvider == null)
						{
							this.innerSqlCancelationPolicyProvider = new SqlCancelationPolicyProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCancelationPolicyProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCancelationPolicyProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCancelationPolicyProvider SqlCancelationPolicyProvider
		{
			get {return CancelationPolicyProvider as SqlCancelationPolicyProvider;}
		}
		
		#endregion
		
		
		#region "CityLanguageProvider"
			
		private SqlCityLanguageProvider innerSqlCityLanguageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CityLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CityLanguageProviderBase CityLanguageProvider
		{
			get
			{
				if (innerSqlCityLanguageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCityLanguageProvider == null)
						{
							this.innerSqlCityLanguageProvider = new SqlCityLanguageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCityLanguageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCityLanguageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCityLanguageProvider SqlCityLanguageProvider
		{
			get {return CityLanguageProvider as SqlCityLanguageProvider;}
		}
		
		#endregion
		
		
		#region "BookedMeetingRoomProvider"
			
		private SqlBookedMeetingRoomProvider innerSqlBookedMeetingRoomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BookedMeetingRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BookedMeetingRoomProviderBase BookedMeetingRoomProvider
		{
			get
			{
				if (innerSqlBookedMeetingRoomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBookedMeetingRoomProvider == null)
						{
							this.innerSqlBookedMeetingRoomProvider = new SqlBookedMeetingRoomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBookedMeetingRoomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBookedMeetingRoomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBookedMeetingRoomProvider SqlBookedMeetingRoomProvider
		{
			get {return BookedMeetingRoomProvider as SqlBookedMeetingRoomProvider;}
		}
		
		#endregion
		
		
		#region "BuildMeetingConfigureProvider"
			
		private SqlBuildMeetingConfigureProvider innerSqlBuildMeetingConfigureProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BuildMeetingConfigure"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BuildMeetingConfigureProviderBase BuildMeetingConfigureProvider
		{
			get
			{
				if (innerSqlBuildMeetingConfigureProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBuildMeetingConfigureProvider == null)
						{
							this.innerSqlBuildMeetingConfigureProvider = new SqlBuildMeetingConfigureProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBuildMeetingConfigureProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBuildMeetingConfigureProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBuildMeetingConfigureProvider SqlBuildMeetingConfigureProvider
		{
			get {return BuildMeetingConfigureProvider as SqlBuildMeetingConfigureProvider;}
		}
		
		#endregion
		
		
		#region "CancelationPolicyLanguageProvider"
			
		private SqlCancelationPolicyLanguageProvider innerSqlCancelationPolicyLanguageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CancelationPolicyLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CancelationPolicyLanguageProviderBase CancelationPolicyLanguageProvider
		{
			get
			{
				if (innerSqlCancelationPolicyLanguageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCancelationPolicyLanguageProvider == null)
						{
							this.innerSqlCancelationPolicyLanguageProvider = new SqlCancelationPolicyLanguageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCancelationPolicyLanguageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCancelationPolicyLanguageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCancelationPolicyLanguageProvider SqlCancelationPolicyLanguageProvider
		{
			get {return CancelationPolicyLanguageProvider as SqlCancelationPolicyLanguageProvider;}
		}
		
		#endregion
		
		
		#region "BuildPackageConfigureProvider"
			
		private SqlBuildPackageConfigureProvider innerSqlBuildPackageConfigureProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BuildPackageConfigure"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BuildPackageConfigureProviderBase BuildPackageConfigureProvider
		{
			get
			{
				if (innerSqlBuildPackageConfigureProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBuildPackageConfigureProvider == null)
						{
							this.innerSqlBuildPackageConfigureProvider = new SqlBuildPackageConfigureProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBuildPackageConfigureProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBuildPackageConfigureProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBuildPackageConfigureProvider SqlBuildPackageConfigureProvider
		{
			get {return BuildPackageConfigureProvider as SqlBuildPackageConfigureProvider;}
		}
		
		#endregion
		
		
		#region "BuildPackageConfigureDescProvider"
			
		private SqlBuildPackageConfigureDescProvider innerSqlBuildPackageConfigureDescProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BuildPackageConfigureDesc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BuildPackageConfigureDescProviderBase BuildPackageConfigureDescProvider
		{
			get
			{
				if (innerSqlBuildPackageConfigureDescProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBuildPackageConfigureDescProvider == null)
						{
							this.innerSqlBuildPackageConfigureDescProvider = new SqlBuildPackageConfigureDescProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBuildPackageConfigureDescProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBuildPackageConfigureDescProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBuildPackageConfigureDescProvider SqlBuildPackageConfigureDescProvider
		{
			get {return BuildPackageConfigureDescProvider as SqlBuildPackageConfigureDescProvider;}
		}
		
		#endregion
		
		
		#region "CityProvider"
			
		private SqlCityProvider innerSqlCityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="City"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CityProviderBase CityProvider
		{
			get
			{
				if (innerSqlCityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCityProvider == null)
						{
							this.innerSqlCityProvider = new SqlCityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCityProvider SqlCityProvider
		{
			get {return CityProvider as SqlCityProvider;}
		}
		
		#endregion
		
		
		#region "AgentUserProvider"
			
		private SqlAgentUserProvider innerSqlAgentUserProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AgentUser"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AgentUserProviderBase AgentUserProvider
		{
			get
			{
				if (innerSqlAgentUserProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAgentUserProvider == null)
						{
							this.innerSqlAgentUserProvider = new SqlAgentUserProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAgentUserProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAgentUserProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAgentUserProvider SqlAgentUserProvider
		{
			get {return AgentUserProvider as SqlAgentUserProvider;}
		}
		
		#endregion
		
		
		#region "BookedBedRoomProvider"
			
		private SqlBookedBedRoomProvider innerSqlBookedBedRoomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BookedBedRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BookedBedRoomProviderBase BookedBedRoomProvider
		{
			get
			{
				if (innerSqlBookedBedRoomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBookedBedRoomProvider == null)
						{
							this.innerSqlBookedBedRoomProvider = new SqlBookedBedRoomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBookedBedRoomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBookedBedRoomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBookedBedRoomProvider SqlBookedBedRoomProvider
		{
			get {return BookedBedRoomProvider as SqlBookedBedRoomProvider;}
		}
		
		#endregion
		
		
		#region "AgencyClientProvider"
			
		private SqlAgencyClientProvider innerSqlAgencyClientProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AgencyClient"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AgencyClientProviderBase AgencyClientProvider
		{
			get
			{
				if (innerSqlAgencyClientProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAgencyClientProvider == null)
						{
							this.innerSqlAgencyClientProvider = new SqlAgencyClientProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAgencyClientProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAgencyClientProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAgencyClientProvider SqlAgencyClientProvider
		{
			get {return AgencyClientProvider as SqlAgencyClientProvider;}
		}
		
		#endregion
		
		
		#region "AgencyInvoiceProvider"
			
		private SqlAgencyInvoiceProvider innerSqlAgencyInvoiceProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AgencyInvoice"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AgencyInvoiceProviderBase AgencyInvoiceProvider
		{
			get
			{
				if (innerSqlAgencyInvoiceProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAgencyInvoiceProvider == null)
						{
							this.innerSqlAgencyInvoiceProvider = new SqlAgencyInvoiceProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAgencyInvoiceProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAgencyInvoiceProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAgencyInvoiceProvider SqlAgencyInvoiceProvider
		{
			get {return AgencyInvoiceProvider as SqlAgencyInvoiceProvider;}
		}
		
		#endregion
		
		
		#region "InvoiceProvider"
			
		private SqlInvoiceProvider innerSqlInvoiceProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Invoice"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override InvoiceProviderBase InvoiceProvider
		{
			get
			{
				if (innerSqlInvoiceProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlInvoiceProvider == null)
						{
							this.innerSqlInvoiceProvider = new SqlInvoiceProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlInvoiceProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlInvoiceProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlInvoiceProvider SqlInvoiceProvider
		{
			get {return InvoiceProvider as SqlInvoiceProvider;}
		}
		
		#endregion
		
		
		#region "AvailabilityProvider"
			
		private SqlAvailabilityProvider innerSqlAvailabilityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Availability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AvailabilityProviderBase AvailabilityProvider
		{
			get
			{
				if (innerSqlAvailabilityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAvailabilityProvider == null)
						{
							this.innerSqlAvailabilityProvider = new SqlAvailabilityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAvailabilityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAvailabilityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAvailabilityProvider SqlAvailabilityProvider
		{
			get {return AvailabilityProvider as SqlAvailabilityProvider;}
		}
		
		#endregion
		
		
		#region "AvailabilityTrailProvider"
			
		private SqlAvailabilityTrailProvider innerSqlAvailabilityTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AvailabilityTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AvailabilityTrailProviderBase AvailabilityTrailProvider
		{
			get
			{
				if (innerSqlAvailabilityTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAvailabilityTrailProvider == null)
						{
							this.innerSqlAvailabilityTrailProvider = new SqlAvailabilityTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAvailabilityTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAvailabilityTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAvailabilityTrailProvider SqlAvailabilityTrailProvider
		{
			get {return AvailabilityTrailProvider as SqlAvailabilityTrailProvider;}
		}
		
		#endregion
		
		
		#region "BedRoomPictureImageTrailProvider"
			
		private SqlBedRoomPictureImageTrailProvider innerSqlBedRoomPictureImageTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BedRoomPictureImageTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BedRoomPictureImageTrailProviderBase BedRoomPictureImageTrailProvider
		{
			get
			{
				if (innerSqlBedRoomPictureImageTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBedRoomPictureImageTrailProvider == null)
						{
							this.innerSqlBedRoomPictureImageTrailProvider = new SqlBedRoomPictureImageTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBedRoomPictureImageTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBedRoomPictureImageTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBedRoomPictureImageTrailProvider SqlBedRoomPictureImageTrailProvider
		{
			get {return BedRoomPictureImageTrailProvider as SqlBedRoomPictureImageTrailProvider;}
		}
		
		#endregion
		
		
		#region "AvailibilityOfRoomsProvider"
			
		private SqlAvailibilityOfRoomsProvider innerSqlAvailibilityOfRoomsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AvailibilityOfRooms"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AvailibilityOfRoomsProviderBase AvailibilityOfRoomsProvider
		{
			get
			{
				if (innerSqlAvailibilityOfRoomsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAvailibilityOfRoomsProvider == null)
						{
							this.innerSqlAvailibilityOfRoomsProvider = new SqlAvailibilityOfRoomsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAvailibilityOfRoomsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAvailibilityOfRoomsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAvailibilityOfRoomsProvider SqlAvailibilityOfRoomsProvider
		{
			get {return AvailibilityOfRoomsProvider as SqlAvailibilityOfRoomsProvider;}
		}
		
		#endregion
		
		
		#region "BedRoomDescProvider"
			
		private SqlBedRoomDescProvider innerSqlBedRoomDescProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BedRoomDesc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BedRoomDescProviderBase BedRoomDescProvider
		{
			get
			{
				if (innerSqlBedRoomDescProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBedRoomDescProvider == null)
						{
							this.innerSqlBedRoomDescProvider = new SqlBedRoomDescProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBedRoomDescProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBedRoomDescProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBedRoomDescProvider SqlBedRoomDescProvider
		{
			get {return BedRoomDescProvider as SqlBedRoomDescProvider;}
		}
		
		#endregion
		
		
		#region "BedRoomDescTrailProvider"
			
		private SqlBedRoomDescTrailProvider innerSqlBedRoomDescTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BedRoomDescTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BedRoomDescTrailProviderBase BedRoomDescTrailProvider
		{
			get
			{
				if (innerSqlBedRoomDescTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBedRoomDescTrailProvider == null)
						{
							this.innerSqlBedRoomDescTrailProvider = new SqlBedRoomDescTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBedRoomDescTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBedRoomDescTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBedRoomDescTrailProvider SqlBedRoomDescTrailProvider
		{
			get {return BedRoomDescTrailProvider as SqlBedRoomDescTrailProvider;}
		}
		
		#endregion
		
		
		#region "BedRoomTrailProvider"
			
		private SqlBedRoomTrailProvider innerSqlBedRoomTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BedRoomTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BedRoomTrailProviderBase BedRoomTrailProvider
		{
			get
			{
				if (innerSqlBedRoomTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBedRoomTrailProvider == null)
						{
							this.innerSqlBedRoomTrailProvider = new SqlBedRoomTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBedRoomTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBedRoomTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBedRoomTrailProvider SqlBedRoomTrailProvider
		{
			get {return BedRoomTrailProvider as SqlBedRoomTrailProvider;}
		}
		
		#endregion
		
		
		#region "CmsDescriptionProvider"
			
		private SqlCmsDescriptionProvider innerSqlCmsDescriptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CmsDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CmsDescriptionProviderBase CmsDescriptionProvider
		{
			get
			{
				if (innerSqlCmsDescriptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCmsDescriptionProvider == null)
						{
							this.innerSqlCmsDescriptionProvider = new SqlCmsDescriptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCmsDescriptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCmsDescriptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCmsDescriptionProvider SqlCmsDescriptionProvider
		{
			get {return CmsDescriptionProvider as SqlCmsDescriptionProvider;}
		}
		
		#endregion
		
		
		#region "CommissionPercentageProvider"
			
		private SqlCommissionPercentageProvider innerSqlCommissionPercentageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CommissionPercentage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CommissionPercentageProviderBase CommissionPercentageProvider
		{
			get
			{
				if (innerSqlCommissionPercentageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCommissionPercentageProvider == null)
						{
							this.innerSqlCommissionPercentageProvider = new SqlCommissionPercentageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCommissionPercentageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCommissionPercentageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCommissionPercentageProvider SqlCommissionPercentageProvider
		{
			get {return CommissionPercentageProvider as SqlCommissionPercentageProvider;}
		}
		
		#endregion
		
		
		#region "HotelProvider"
			
		private SqlHotelProvider innerSqlHotelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Hotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelProviderBase HotelProvider
		{
			get
			{
				if (innerSqlHotelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelProvider == null)
						{
							this.innerSqlHotelProvider = new SqlHotelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelProvider SqlHotelProvider
		{
			get {return HotelProvider as SqlHotelProvider;}
		}
		
		#endregion
		
		
		#region "ZoneLanguageProvider"
			
		private SqlZoneLanguageProvider innerSqlZoneLanguageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ZoneLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ZoneLanguageProviderBase ZoneLanguageProvider
		{
			get
			{
				if (innerSqlZoneLanguageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlZoneLanguageProvider == null)
						{
							this.innerSqlZoneLanguageProvider = new SqlZoneLanguageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlZoneLanguageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlZoneLanguageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlZoneLanguageProvider SqlZoneLanguageProvider
		{
			get {return ZoneLanguageProvider as SqlZoneLanguageProvider;}
		}
		
		#endregion
		
		
		#region "HotelContactProvider"
			
		private SqlHotelContactProvider innerSqlHotelContactProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HotelContact"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelContactProviderBase HotelContactProvider
		{
			get
			{
				if (innerSqlHotelContactProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelContactProvider == null)
						{
							this.innerSqlHotelContactProvider = new SqlHotelContactProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelContactProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelContactProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelContactProvider SqlHotelContactProvider
		{
			get {return HotelContactProvider as SqlHotelContactProvider;}
		}
		
		#endregion
		
		
		#region "HotelTrailProvider"
			
		private SqlHotelTrailProvider innerSqlHotelTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HotelTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelTrailProviderBase HotelTrailProvider
		{
			get
			{
				if (innerSqlHotelTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelTrailProvider == null)
						{
							this.innerSqlHotelTrailProvider = new SqlHotelTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelTrailProvider SqlHotelTrailProvider
		{
			get {return HotelTrailProvider as SqlHotelTrailProvider;}
		}
		
		#endregion
		
		
		#region "FrontEndBottomProvider"
			
		private SqlFrontEndBottomProvider innerSqlFrontEndBottomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FrontEndBottom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FrontEndBottomProviderBase FrontEndBottomProvider
		{
			get
			{
				if (innerSqlFrontEndBottomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFrontEndBottomProvider == null)
						{
							this.innerSqlFrontEndBottomProvider = new SqlFrontEndBottomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFrontEndBottomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFrontEndBottomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFrontEndBottomProvider SqlFrontEndBottomProvider
		{
			get {return FrontEndBottomProvider as SqlFrontEndBottomProvider;}
		}
		
		#endregion
		
		
		#region "HotelContactTrailProvider"
			
		private SqlHotelContactTrailProvider innerSqlHotelContactTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HotelContactTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelContactTrailProviderBase HotelContactTrailProvider
		{
			get
			{
				if (innerSqlHotelContactTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelContactTrailProvider == null)
						{
							this.innerSqlHotelContactTrailProvider = new SqlHotelContactTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelContactTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelContactTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelContactTrailProvider SqlHotelContactTrailProvider
		{
			get {return HotelContactTrailProvider as SqlHotelContactTrailProvider;}
		}
		
		#endregion
		
		
		#region "HotelDescProvider"
			
		private SqlHotelDescProvider innerSqlHotelDescProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HotelDesc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelDescProviderBase HotelDescProvider
		{
			get
			{
				if (innerSqlHotelDescProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelDescProvider == null)
						{
							this.innerSqlHotelDescProvider = new SqlHotelDescProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelDescProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelDescProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelDescProvider SqlHotelDescProvider
		{
			get {return HotelDescProvider as SqlHotelDescProvider;}
		}
		
		#endregion
		
		
		#region "HoteldescTrailProvider"
			
		private SqlHoteldescTrailProvider innerSqlHoteldescTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HoteldescTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HoteldescTrailProviderBase HoteldescTrailProvider
		{
			get
			{
				if (innerSqlHoteldescTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHoteldescTrailProvider == null)
						{
							this.innerSqlHoteldescTrailProvider = new SqlHoteldescTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHoteldescTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHoteldescTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHoteldescTrailProvider SqlHoteldescTrailProvider
		{
			get {return HoteldescTrailProvider as SqlHoteldescTrailProvider;}
		}
		
		#endregion
		
		
		#region "HotelPhotoVideoGallaryProvider"
			
		private SqlHotelPhotoVideoGallaryProvider innerSqlHotelPhotoVideoGallaryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HotelPhotoVideoGallary"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelPhotoVideoGallaryProviderBase HotelPhotoVideoGallaryProvider
		{
			get
			{
				if (innerSqlHotelPhotoVideoGallaryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelPhotoVideoGallaryProvider == null)
						{
							this.innerSqlHotelPhotoVideoGallaryProvider = new SqlHotelPhotoVideoGallaryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelPhotoVideoGallaryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelPhotoVideoGallaryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelPhotoVideoGallaryProvider SqlHotelPhotoVideoGallaryProvider
		{
			get {return HotelPhotoVideoGallaryProvider as SqlHotelPhotoVideoGallaryProvider;}
		}
		
		#endregion
		
		
		#region "HotelOwnerLinkProvider"
			
		private SqlHotelOwnerLinkProvider innerSqlHotelOwnerLinkProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HotelOwnerLink"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelOwnerLinkProviderBase HotelOwnerLinkProvider
		{
			get
			{
				if (innerSqlHotelOwnerLinkProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelOwnerLinkProvider == null)
						{
							this.innerSqlHotelOwnerLinkProvider = new SqlHotelOwnerLinkProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelOwnerLinkProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelOwnerLinkProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelOwnerLinkProvider SqlHotelOwnerLinkProvider
		{
			get {return HotelOwnerLinkProvider as SqlHotelOwnerLinkProvider;}
		}
		
		#endregion
		
		
		#region "HotelFacilitiesTrailProvider"
			
		private SqlHotelFacilitiesTrailProvider innerSqlHotelFacilitiesTrailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HotelFacilitiesTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelFacilitiesTrailProviderBase HotelFacilitiesTrailProvider
		{
			get
			{
				if (innerSqlHotelFacilitiesTrailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelFacilitiesTrailProvider == null)
						{
							this.innerSqlHotelFacilitiesTrailProvider = new SqlHotelFacilitiesTrailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelFacilitiesTrailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelFacilitiesTrailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelFacilitiesTrailProvider SqlHotelFacilitiesTrailProvider
		{
			get {return HotelFacilitiesTrailProvider as SqlHotelFacilitiesTrailProvider;}
		}
		
		#endregion
		
		
		#region "HotelFacilitiesProvider"
			
		private SqlHotelFacilitiesProvider innerSqlHotelFacilitiesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HotelFacilities"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HotelFacilitiesProviderBase HotelFacilitiesProvider
		{
			get
			{
				if (innerSqlHotelFacilitiesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHotelFacilitiesProvider == null)
						{
							this.innerSqlHotelFacilitiesProvider = new SqlHotelFacilitiesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHotelFacilitiesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHotelFacilitiesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHotelFacilitiesProvider SqlHotelFacilitiesProvider
		{
			get {return HotelFacilitiesProvider as SqlHotelFacilitiesProvider;}
		}
		
		#endregion
		
		
		#region "HearUsQuestionProvider"
			
		private SqlHearUsQuestionProvider innerSqlHearUsQuestionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HearUsQuestion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HearUsQuestionProviderBase HearUsQuestionProvider
		{
			get
			{
				if (innerSqlHearUsQuestionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHearUsQuestionProvider == null)
						{
							this.innerSqlHearUsQuestionProvider = new SqlHearUsQuestionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHearUsQuestionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHearUsQuestionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHearUsQuestionProvider SqlHearUsQuestionProvider
		{
			get {return HearUsQuestionProvider as SqlHearUsQuestionProvider;}
		}
		
		#endregion
		
		
		#region "GroupProvider"
			
		private SqlGroupProvider innerSqlGroupProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Group"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override GroupProviderBase GroupProvider
		{
			get
			{
				if (innerSqlGroupProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlGroupProvider == null)
						{
							this.innerSqlGroupProvider = new SqlGroupProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlGroupProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlGroupProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlGroupProvider SqlGroupProvider
		{
			get {return GroupProvider as SqlGroupProvider;}
		}
		
		#endregion
		
		
		#region "FrontEndBottomDescriptionProvider"
			
		private SqlFrontEndBottomDescriptionProvider innerSqlFrontEndBottomDescriptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FrontEndBottomDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FrontEndBottomDescriptionProviderBase FrontEndBottomDescriptionProvider
		{
			get
			{
				if (innerSqlFrontEndBottomDescriptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFrontEndBottomDescriptionProvider == null)
						{
							this.innerSqlFrontEndBottomDescriptionProvider = new SqlFrontEndBottomDescriptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFrontEndBottomDescriptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFrontEndBottomDescriptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFrontEndBottomDescriptionProvider SqlFrontEndBottomDescriptionProvider
		{
			get {return FrontEndBottomDescriptionProvider as SqlFrontEndBottomDescriptionProvider;}
		}
		
		#endregion
		
		
		#region "DashboardLinkProvider"
			
		private SqlDashboardLinkProvider innerSqlDashboardLinkProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="DashboardLink"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override DashboardLinkProviderBase DashboardLinkProvider
		{
			get
			{
				if (innerSqlDashboardLinkProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlDashboardLinkProvider == null)
						{
							this.innerSqlDashboardLinkProvider = new SqlDashboardLinkProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlDashboardLinkProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlDashboardLinkProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlDashboardLinkProvider SqlDashboardLinkProvider
		{
			get {return DashboardLinkProvider as SqlDashboardLinkProvider;}
		}
		
		#endregion
		
		
		#region "CurrencyExchangeLogsProvider"
			
		private SqlCurrencyExchangeLogsProvider innerSqlCurrencyExchangeLogsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CurrencyExchangeLogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CurrencyExchangeLogsProviderBase CurrencyExchangeLogsProvider
		{
			get
			{
				if (innerSqlCurrencyExchangeLogsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCurrencyExchangeLogsProvider == null)
						{
							this.innerSqlCurrencyExchangeLogsProvider = new SqlCurrencyExchangeLogsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCurrencyExchangeLogsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCurrencyExchangeLogsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCurrencyExchangeLogsProvider SqlCurrencyExchangeLogsProvider
		{
			get {return CurrencyExchangeLogsProvider as SqlCurrencyExchangeLogsProvider;}
		}
		
		#endregion
		
		
		#region "CreditCardDetailProvider"
			
		private SqlCreditCardDetailProvider innerSqlCreditCardDetailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CreditCardDetail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CreditCardDetailProviderBase CreditCardDetailProvider
		{
			get
			{
				if (innerSqlCreditCardDetailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCreditCardDetailProvider == null)
						{
							this.innerSqlCreditCardDetailProvider = new SqlCreditCardDetailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCreditCardDetailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCreditCardDetailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCreditCardDetailProvider SqlCreditCardDetailProvider
		{
			get {return CreditCardDetailProvider as SqlCreditCardDetailProvider;}
		}
		
		#endregion
		
		
		#region "CountryLanguageProvider"
			
		private SqlCountryLanguageProvider innerSqlCountryLanguageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CountryLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CountryLanguageProviderBase CountryLanguageProvider
		{
			get
			{
				if (innerSqlCountryLanguageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCountryLanguageProvider == null)
						{
							this.innerSqlCountryLanguageProvider = new SqlCountryLanguageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCountryLanguageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCountryLanguageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCountryLanguageProvider SqlCountryLanguageProvider
		{
			get {return CountryLanguageProvider as SqlCountryLanguageProvider;}
		}
		
		#endregion
		
		
		#region "DbAuditProvider"
			
		private SqlDbAuditProvider innerSqlDbAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="DbAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override DbAuditProviderBase DbAuditProvider
		{
			get
			{
				if (innerSqlDbAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlDbAuditProvider == null)
						{
							this.innerSqlDbAuditProvider = new SqlDbAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlDbAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlDbAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlDbAuditProvider SqlDbAuditProvider
		{
			get {return DbAuditProvider as SqlDbAuditProvider;}
		}
		
		#endregion
		
		
		#region "EmailConfigProvider"
			
		private SqlEmailConfigProvider innerSqlEmailConfigProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EmailConfig"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EmailConfigProviderBase EmailConfigProvider
		{
			get
			{
				if (innerSqlEmailConfigProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEmailConfigProvider == null)
						{
							this.innerSqlEmailConfigProvider = new SqlEmailConfigProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEmailConfigProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEmailConfigProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEmailConfigProvider SqlEmailConfigProvider
		{
			get {return EmailConfigProvider as SqlEmailConfigProvider;}
		}
		
		#endregion
		
		
		#region "FinancialInfoProvider"
			
		private SqlFinancialInfoProvider innerSqlFinancialInfoProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FinancialInfo"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FinancialInfoProviderBase FinancialInfoProvider
		{
			get
			{
				if (innerSqlFinancialInfoProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFinancialInfoProvider == null)
						{
							this.innerSqlFinancialInfoProvider = new SqlFinancialInfoProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFinancialInfoProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFinancialInfoProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFinancialInfoProvider SqlFinancialInfoProvider
		{
			get {return FinancialInfoProvider as SqlFinancialInfoProvider;}
		}
		
		#endregion
		
		
		#region "FacilityTypeProvider"
			
		private SqlFacilityTypeProvider innerSqlFacilityTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FacilityType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FacilityTypeProviderBase FacilityTypeProvider
		{
			get
			{
				if (innerSqlFacilityTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFacilityTypeProvider == null)
						{
							this.innerSqlFacilityTypeProvider = new SqlFacilityTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFacilityTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFacilityTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFacilityTypeProvider SqlFacilityTypeProvider
		{
			get {return FacilityTypeProvider as SqlFacilityTypeProvider;}
		}
		
		#endregion
		
		
		#region "FacilityProvider"
			
		private SqlFacilityProvider innerSqlFacilityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Facility"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FacilityProviderBase FacilityProvider
		{
			get
			{
				if (innerSqlFacilityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFacilityProvider == null)
						{
							this.innerSqlFacilityProvider = new SqlFacilityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFacilityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFacilityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFacilityProvider SqlFacilityProvider
		{
			get {return FacilityProvider as SqlFacilityProvider;}
		}
		
		#endregion
		
		
		#region "EmailConfigMappingProvider"
			
		private SqlEmailConfigMappingProvider innerSqlEmailConfigMappingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EmailConfigMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EmailConfigMappingProviderBase EmailConfigMappingProvider
		{
			get
			{
				if (innerSqlEmailConfigMappingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEmailConfigMappingProvider == null)
						{
							this.innerSqlEmailConfigMappingProvider = new SqlEmailConfigMappingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEmailConfigMappingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEmailConfigMappingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEmailConfigMappingProvider SqlEmailConfigMappingProvider
		{
			get {return EmailConfigMappingProvider as SqlEmailConfigMappingProvider;}
		}
		
		#endregion
		
		
		#region "InvoiceCommPercentageProvider"
			
		private SqlInvoiceCommPercentageProvider innerSqlInvoiceCommPercentageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="InvoiceCommPercentage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override InvoiceCommPercentageProviderBase InvoiceCommPercentageProvider
		{
			get
			{
				if (innerSqlInvoiceCommPercentageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlInvoiceCommPercentageProvider == null)
						{
							this.innerSqlInvoiceCommPercentageProvider = new SqlInvoiceCommPercentageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlInvoiceCommPercentageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlInvoiceCommPercentageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlInvoiceCommPercentageProvider SqlInvoiceCommPercentageProvider
		{
			get {return InvoiceCommPercentageProvider as SqlInvoiceCommPercentageProvider;}
		}
		
		#endregion
		
		
		
		#region "AvailabilityWithSpandpProvider"
		
		private SqlAvailabilityWithSpandpProvider innerSqlAvailabilityWithSpandpProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AvailabilityWithSpandp"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AvailabilityWithSpandpProviderBase AvailabilityWithSpandpProvider
		{
			get
			{
				if (innerSqlAvailabilityWithSpandpProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAvailabilityWithSpandpProvider == null)
						{
							this.innerSqlAvailabilityWithSpandpProvider = new SqlAvailabilityWithSpandpProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAvailabilityWithSpandpProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAvailabilityWithSpandpProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAvailabilityWithSpandpProvider SqlAvailabilityWithSpandpProvider
		{
			get {return AvailabilityWithSpandpProvider as SqlAvailabilityWithSpandpProvider;}
		}
		
		#endregion
		
		
		#region "BookingRequestViewListProvider"
		
		private SqlBookingRequestViewListProvider innerSqlBookingRequestViewListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="BookingRequestViewList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override BookingRequestViewListProviderBase BookingRequestViewListProvider
		{
			get
			{
				if (innerSqlBookingRequestViewListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlBookingRequestViewListProvider == null)
						{
							this.innerSqlBookingRequestViewListProvider = new SqlBookingRequestViewListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlBookingRequestViewListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlBookingRequestViewListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlBookingRequestViewListProvider SqlBookingRequestViewListProvider
		{
			get {return BookingRequestViewListProvider as SqlBookingRequestViewListProvider;}
		}
		
		#endregion
		
		
		#region "ViewAvailabilityAndSpecialManagerProvider"
		
		private SqlViewAvailabilityAndSpecialManagerProvider innerSqlViewAvailabilityAndSpecialManagerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewAvailabilityAndSpecialManager"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewAvailabilityAndSpecialManagerProviderBase ViewAvailabilityAndSpecialManagerProvider
		{
			get
			{
				if (innerSqlViewAvailabilityAndSpecialManagerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewAvailabilityAndSpecialManagerProvider == null)
						{
							this.innerSqlViewAvailabilityAndSpecialManagerProvider = new SqlViewAvailabilityAndSpecialManagerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewAvailabilityAndSpecialManagerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewAvailabilityAndSpecialManagerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewAvailabilityAndSpecialManagerProvider SqlViewAvailabilityAndSpecialManagerProvider
		{
			get {return ViewAvailabilityAndSpecialManagerProvider as SqlViewAvailabilityAndSpecialManagerProvider;}
		}
		
		#endregion
		
		
		#region "ViewAvailabilityOfBedRoomProvider"
		
		private SqlViewAvailabilityOfBedRoomProvider innerSqlViewAvailabilityOfBedRoomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewAvailabilityOfBedRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewAvailabilityOfBedRoomProviderBase ViewAvailabilityOfBedRoomProvider
		{
			get
			{
				if (innerSqlViewAvailabilityOfBedRoomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewAvailabilityOfBedRoomProvider == null)
						{
							this.innerSqlViewAvailabilityOfBedRoomProvider = new SqlViewAvailabilityOfBedRoomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewAvailabilityOfBedRoomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewAvailabilityOfBedRoomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewAvailabilityOfBedRoomProvider SqlViewAvailabilityOfBedRoomProvider
		{
			get {return ViewAvailabilityOfBedRoomProvider as SqlViewAvailabilityOfBedRoomProvider;}
		}
		
		#endregion
		
		
		#region "ViewAvailabilityOfRoomsProvider"
		
		private SqlViewAvailabilityOfRoomsProvider innerSqlViewAvailabilityOfRoomsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewAvailabilityOfRooms"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewAvailabilityOfRoomsProviderBase ViewAvailabilityOfRoomsProvider
		{
			get
			{
				if (innerSqlViewAvailabilityOfRoomsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewAvailabilityOfRoomsProvider == null)
						{
							this.innerSqlViewAvailabilityOfRoomsProvider = new SqlViewAvailabilityOfRoomsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewAvailabilityOfRoomsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewAvailabilityOfRoomsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewAvailabilityOfRoomsProvider SqlViewAvailabilityOfRoomsProvider
		{
			get {return ViewAvailabilityOfRoomsProvider as SqlViewAvailabilityOfRoomsProvider;}
		}
		
		#endregion
		
		
		#region "ViewBookingHotelsProvider"
		
		private SqlViewBookingHotelsProvider innerSqlViewBookingHotelsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewBookingHotels"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewBookingHotelsProviderBase ViewBookingHotelsProvider
		{
			get
			{
				if (innerSqlViewBookingHotelsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewBookingHotelsProvider == null)
						{
							this.innerSqlViewBookingHotelsProvider = new SqlViewBookingHotelsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewBookingHotelsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewBookingHotelsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewBookingHotelsProvider SqlViewBookingHotelsProvider
		{
			get {return ViewBookingHotelsProvider as SqlViewBookingHotelsProvider;}
		}
		
		#endregion
		
		
		#region "ViewbookingrequestProvider"
		
		private SqlViewbookingrequestProvider innerSqlViewbookingrequestProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Viewbookingrequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewbookingrequestProviderBase ViewbookingrequestProvider
		{
			get
			{
				if (innerSqlViewbookingrequestProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewbookingrequestProvider == null)
						{
							this.innerSqlViewbookingrequestProvider = new SqlViewbookingrequestProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewbookingrequestProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewbookingrequestProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewbookingrequestProvider SqlViewbookingrequestProvider
		{
			get {return ViewbookingrequestProvider as SqlViewbookingrequestProvider;}
		}
		
		#endregion
		
		
		#region "ViewClientContractProvider"
		
		private SqlViewClientContractProvider innerSqlViewClientContractProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewClientContract"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewClientContractProviderBase ViewClientContractProvider
		{
			get
			{
				if (innerSqlViewClientContractProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewClientContractProvider == null)
						{
							this.innerSqlViewClientContractProvider = new SqlViewClientContractProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewClientContractProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewClientContractProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewClientContractProvider SqlViewClientContractProvider
		{
			get {return ViewClientContractProvider as SqlViewClientContractProvider;}
		}
		
		#endregion
		
		
		#region "ViewFacilitiesProvider"
		
		private SqlViewFacilitiesProvider innerSqlViewFacilitiesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewFacilities"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewFacilitiesProviderBase ViewFacilitiesProvider
		{
			get
			{
				if (innerSqlViewFacilitiesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewFacilitiesProvider == null)
						{
							this.innerSqlViewFacilitiesProvider = new SqlViewFacilitiesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewFacilitiesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewFacilitiesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewFacilitiesProvider SqlViewFacilitiesProvider
		{
			get {return ViewFacilitiesProvider as SqlViewFacilitiesProvider;}
		}
		
		#endregion
		
		
		#region "ViewFacilityIconProvider"
		
		private SqlViewFacilityIconProvider innerSqlViewFacilityIconProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewFacilityIcon"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewFacilityIconProviderBase ViewFacilityIconProvider
		{
			get
			{
				if (innerSqlViewFacilityIconProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewFacilityIconProvider == null)
						{
							this.innerSqlViewFacilityIconProvider = new SqlViewFacilityIconProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewFacilityIconProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewFacilityIconProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewFacilityIconProvider SqlViewFacilityIconProvider
		{
			get {return ViewFacilityIconProvider as SqlViewFacilityIconProvider;}
		}
		
		#endregion
		
		
		#region "ViewFindAvailableBedroomProvider"
		
		private SqlViewFindAvailableBedroomProvider innerSqlViewFindAvailableBedroomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewFindAvailableBedroom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewFindAvailableBedroomProviderBase ViewFindAvailableBedroomProvider
		{
			get
			{
				if (innerSqlViewFindAvailableBedroomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewFindAvailableBedroomProvider == null)
						{
							this.innerSqlViewFindAvailableBedroomProvider = new SqlViewFindAvailableBedroomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewFindAvailableBedroomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewFindAvailableBedroomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewFindAvailableBedroomProvider SqlViewFindAvailableBedroomProvider
		{
			get {return ViewFindAvailableBedroomProvider as SqlViewFindAvailableBedroomProvider;}
		}
		
		#endregion
		
		
		#region "ViewForAvailabilityCheckProvider"
		
		private SqlViewForAvailabilityCheckProvider innerSqlViewForAvailabilityCheckProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewForAvailabilityCheck"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewForAvailabilityCheckProviderBase ViewForAvailabilityCheckProvider
		{
			get
			{
				if (innerSqlViewForAvailabilityCheckProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewForAvailabilityCheckProvider == null)
						{
							this.innerSqlViewForAvailabilityCheckProvider = new SqlViewForAvailabilityCheckProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewForAvailabilityCheckProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewForAvailabilityCheckProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewForAvailabilityCheckProvider SqlViewForAvailabilityCheckProvider
		{
			get {return ViewForAvailabilityCheckProvider as SqlViewForAvailabilityCheckProvider;}
		}
		
		#endregion
		
		
		#region "ViewForCommissionFieldsProvider"
		
		private SqlViewForCommissionFieldsProvider innerSqlViewForCommissionFieldsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewForCommissionFields"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewForCommissionFieldsProviderBase ViewForCommissionFieldsProvider
		{
			get
			{
				if (innerSqlViewForCommissionFieldsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewForCommissionFieldsProvider == null)
						{
							this.innerSqlViewForCommissionFieldsProvider = new SqlViewForCommissionFieldsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewForCommissionFieldsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewForCommissionFieldsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewForCommissionFieldsProvider SqlViewForCommissionFieldsProvider
		{
			get {return ViewForCommissionFieldsProvider as SqlViewForCommissionFieldsProvider;}
		}
		
		#endregion
		
		
		#region "ViewForRequestSearchProvider"
		
		private SqlViewForRequestSearchProvider innerSqlViewForRequestSearchProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewForRequestSearch"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewForRequestSearchProviderBase ViewForRequestSearchProvider
		{
			get
			{
				if (innerSqlViewForRequestSearchProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewForRequestSearchProvider == null)
						{
							this.innerSqlViewForRequestSearchProvider = new SqlViewForRequestSearchProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewForRequestSearchProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewForRequestSearchProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewForRequestSearchProvider SqlViewForRequestSearchProvider
		{
			get {return ViewForRequestSearchProvider as SqlViewForRequestSearchProvider;}
		}
		
		#endregion
		
		
		#region "ViewForSetInvoiceProvider"
		
		private SqlViewForSetInvoiceProvider innerSqlViewForSetInvoiceProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewForSetInvoice"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewForSetInvoiceProviderBase ViewForSetInvoiceProvider
		{
			get
			{
				if (innerSqlViewForSetInvoiceProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewForSetInvoiceProvider == null)
						{
							this.innerSqlViewForSetInvoiceProvider = new SqlViewForSetInvoiceProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewForSetInvoiceProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewForSetInvoiceProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewForSetInvoiceProvider SqlViewForSetInvoiceProvider
		{
			get {return ViewForSetInvoiceProvider as SqlViewForSetInvoiceProvider;}
		}
		
		#endregion
		
		
		#region "ViewMeetingRoomAvailabilityProvider"
		
		private SqlViewMeetingRoomAvailabilityProvider innerSqlViewMeetingRoomAvailabilityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewMeetingRoomAvailability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewMeetingRoomAvailabilityProviderBase ViewMeetingRoomAvailabilityProvider
		{
			get
			{
				if (innerSqlViewMeetingRoomAvailabilityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewMeetingRoomAvailabilityProvider == null)
						{
							this.innerSqlViewMeetingRoomAvailabilityProvider = new SqlViewMeetingRoomAvailabilityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewMeetingRoomAvailabilityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewMeetingRoomAvailabilityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewMeetingRoomAvailabilityProvider SqlViewMeetingRoomAvailabilityProvider
		{
			get {return ViewMeetingRoomAvailabilityProvider as SqlViewMeetingRoomAvailabilityProvider;}
		}
		
		#endregion
		
		
		#region "ViewMeetingRoomForRequestProvider"
		
		private SqlViewMeetingRoomForRequestProvider innerSqlViewMeetingRoomForRequestProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewMeetingRoomForRequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewMeetingRoomForRequestProviderBase ViewMeetingRoomForRequestProvider
		{
			get
			{
				if (innerSqlViewMeetingRoomForRequestProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewMeetingRoomForRequestProvider == null)
						{
							this.innerSqlViewMeetingRoomForRequestProvider = new SqlViewMeetingRoomForRequestProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewMeetingRoomForRequestProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewMeetingRoomForRequestProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewMeetingRoomForRequestProvider SqlViewMeetingRoomForRequestProvider
		{
			get {return ViewMeetingRoomForRequestProvider as SqlViewMeetingRoomForRequestProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportAvailabilityProvider"
		
		private SqlViewReportAvailabilityProvider innerSqlViewReportAvailabilityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportAvailability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportAvailabilityProviderBase ViewReportAvailabilityProvider
		{
			get
			{
				if (innerSqlViewReportAvailabilityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportAvailabilityProvider == null)
						{
							this.innerSqlViewReportAvailabilityProvider = new SqlViewReportAvailabilityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportAvailabilityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportAvailabilityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportAvailabilityProvider SqlViewReportAvailabilityProvider
		{
			get {return ViewReportAvailabilityProvider as SqlViewReportAvailabilityProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportBookingRequestHistoryProvider"
		
		private SqlViewReportBookingRequestHistoryProvider innerSqlViewReportBookingRequestHistoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportBookingRequestHistory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportBookingRequestHistoryProviderBase ViewReportBookingRequestHistoryProvider
		{
			get
			{
				if (innerSqlViewReportBookingRequestHistoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportBookingRequestHistoryProvider == null)
						{
							this.innerSqlViewReportBookingRequestHistoryProvider = new SqlViewReportBookingRequestHistoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportBookingRequestHistoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportBookingRequestHistoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportBookingRequestHistoryProvider SqlViewReportBookingRequestHistoryProvider
		{
			get {return ViewReportBookingRequestHistoryProvider as SqlViewReportBookingRequestHistoryProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportCancelationProvider"
		
		private SqlViewReportCancelationProvider innerSqlViewReportCancelationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportCancelation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportCancelationProviderBase ViewReportCancelationProvider
		{
			get
			{
				if (innerSqlViewReportCancelationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportCancelationProvider == null)
						{
							this.innerSqlViewReportCancelationProvider = new SqlViewReportCancelationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportCancelationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportCancelationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportCancelationProvider SqlViewReportCancelationProvider
		{
			get {return ViewReportCancelationProvider as SqlViewReportCancelationProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportCommissionControlProvider"
		
		private SqlViewReportCommissionControlProvider innerSqlViewReportCommissionControlProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportCommissionControl"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportCommissionControlProviderBase ViewReportCommissionControlProvider
		{
			get
			{
				if (innerSqlViewReportCommissionControlProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportCommissionControlProvider == null)
						{
							this.innerSqlViewReportCommissionControlProvider = new SqlViewReportCommissionControlProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportCommissionControlProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportCommissionControlProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportCommissionControlProvider SqlViewReportCommissionControlProvider
		{
			get {return ViewReportCommissionControlProvider as SqlViewReportCommissionControlProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportforProfileCompanyAgencyProvider"
		
		private SqlViewReportforProfileCompanyAgencyProvider innerSqlViewReportforProfileCompanyAgencyProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportforProfileCompanyAgency"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportforProfileCompanyAgencyProviderBase ViewReportforProfileCompanyAgencyProvider
		{
			get
			{
				if (innerSqlViewReportforProfileCompanyAgencyProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportforProfileCompanyAgencyProvider == null)
						{
							this.innerSqlViewReportforProfileCompanyAgencyProvider = new SqlViewReportforProfileCompanyAgencyProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportforProfileCompanyAgencyProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportforProfileCompanyAgencyProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportforProfileCompanyAgencyProvider SqlViewReportforProfileCompanyAgencyProvider
		{
			get {return ViewReportforProfileCompanyAgencyProvider as SqlViewReportforProfileCompanyAgencyProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportInventoryHotelProvider"
		
		private SqlViewReportInventoryHotelProvider innerSqlViewReportInventoryHotelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportInventoryHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportInventoryHotelProviderBase ViewReportInventoryHotelProvider
		{
			get
			{
				if (innerSqlViewReportInventoryHotelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportInventoryHotelProvider == null)
						{
							this.innerSqlViewReportInventoryHotelProvider = new SqlViewReportInventoryHotelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportInventoryHotelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportInventoryHotelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportInventoryHotelProvider SqlViewReportInventoryHotelProvider
		{
			get {return ViewReportInventoryHotelProvider as SqlViewReportInventoryHotelProvider;}
		}
		
		#endregion
		
		
		#region "ViewreportInventoryReportofExistingHotelProvider"
		
		private SqlViewreportInventoryReportofExistingHotelProvider innerSqlViewreportInventoryReportofExistingHotelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewreportInventoryReportofExistingHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewreportInventoryReportofExistingHotelProviderBase ViewreportInventoryReportofExistingHotelProvider
		{
			get
			{
				if (innerSqlViewreportInventoryReportofExistingHotelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewreportInventoryReportofExistingHotelProvider == null)
						{
							this.innerSqlViewreportInventoryReportofExistingHotelProvider = new SqlViewreportInventoryReportofExistingHotelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewreportInventoryReportofExistingHotelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewreportInventoryReportofExistingHotelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewreportInventoryReportofExistingHotelProvider SqlViewreportInventoryReportofExistingHotelProvider
		{
			get {return ViewreportInventoryReportofExistingHotelProvider as SqlViewreportInventoryReportofExistingHotelProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportLeadTimeProvider"
		
		private SqlViewReportLeadTimeProvider innerSqlViewReportLeadTimeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportLeadTime"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportLeadTimeProviderBase ViewReportLeadTimeProvider
		{
			get
			{
				if (innerSqlViewReportLeadTimeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportLeadTimeProvider == null)
						{
							this.innerSqlViewReportLeadTimeProvider = new SqlViewReportLeadTimeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportLeadTimeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportLeadTimeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportLeadTimeProvider SqlViewReportLeadTimeProvider
		{
			get {return ViewReportLeadTimeProvider as SqlViewReportLeadTimeProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportOnLineInventoryReportProvider"
		
		private SqlViewReportOnLineInventoryReportProvider innerSqlViewReportOnLineInventoryReportProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportOnLineInventoryReport"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportOnLineInventoryReportProviderBase ViewReportOnLineInventoryReportProvider
		{
			get
			{
				if (innerSqlViewReportOnLineInventoryReportProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportOnLineInventoryReportProvider == null)
						{
							this.innerSqlViewReportOnLineInventoryReportProvider = new SqlViewReportOnLineInventoryReportProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportOnLineInventoryReportProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportOnLineInventoryReportProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportOnLineInventoryReportProvider SqlViewReportOnLineInventoryReportProvider
		{
			get {return ViewReportOnLineInventoryReportProvider as SqlViewReportOnLineInventoryReportProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportProductionCompanyAgencyProvider"
		
		private SqlViewReportProductionCompanyAgencyProvider innerSqlViewReportProductionCompanyAgencyProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportProductionCompanyAgency"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportProductionCompanyAgencyProviderBase ViewReportProductionCompanyAgencyProvider
		{
			get
			{
				if (innerSqlViewReportProductionCompanyAgencyProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportProductionCompanyAgencyProvider == null)
						{
							this.innerSqlViewReportProductionCompanyAgencyProvider = new SqlViewReportProductionCompanyAgencyProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportProductionCompanyAgencyProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportProductionCompanyAgencyProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportProductionCompanyAgencyProvider SqlViewReportProductionCompanyAgencyProvider
		{
			get {return ViewReportProductionCompanyAgencyProvider as SqlViewReportProductionCompanyAgencyProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportProductionReportHotelProvider"
		
		private SqlViewReportProductionReportHotelProvider innerSqlViewReportProductionReportHotelProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportProductionReportHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportProductionReportHotelProviderBase ViewReportProductionReportHotelProvider
		{
			get
			{
				if (innerSqlViewReportProductionReportHotelProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportProductionReportHotelProvider == null)
						{
							this.innerSqlViewReportProductionReportHotelProvider = new SqlViewReportProductionReportHotelProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportProductionReportHotelProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportProductionReportHotelProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportProductionReportHotelProvider SqlViewReportProductionReportHotelProvider
		{
			get {return ViewReportProductionReportHotelProvider as SqlViewReportProductionReportHotelProvider;}
		}
		
		#endregion
		
		
		#region "ViewReportRankingProvider"
		
		private SqlViewReportRankingProvider innerSqlViewReportRankingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewReportRanking"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewReportRankingProviderBase ViewReportRankingProvider
		{
			get
			{
				if (innerSqlViewReportRankingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewReportRankingProvider == null)
						{
							this.innerSqlViewReportRankingProvider = new SqlViewReportRankingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewReportRankingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewReportRankingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewReportRankingProvider SqlViewReportRankingProvider
		{
			get {return ViewReportRankingProvider as SqlViewReportRankingProvider;}
		}
		
		#endregion
		
		
		#region "ViewRequestProvider"
		
		private SqlViewRequestProvider innerSqlViewRequestProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewRequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewRequestProviderBase ViewRequestProvider
		{
			get
			{
				if (innerSqlViewRequestProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewRequestProvider == null)
						{
							this.innerSqlViewRequestProvider = new SqlViewRequestProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewRequestProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewRequestProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewRequestProvider SqlViewRequestProvider
		{
			get {return ViewRequestProvider as SqlViewRequestProvider;}
		}
		
		#endregion
		
		
		#region "ViewSearchForDay2Provider"
		
		private SqlViewSearchForDay2Provider innerSqlViewSearchForDay2Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewSearchForDay2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewSearchForDay2ProviderBase ViewSearchForDay2Provider
		{
			get
			{
				if (innerSqlViewSearchForDay2Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewSearchForDay2Provider == null)
						{
							this.innerSqlViewSearchForDay2Provider = new SqlViewSearchForDay2Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewSearchForDay2Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewSearchForDay2Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewSearchForDay2Provider SqlViewSearchForDay2Provider
		{
			get {return ViewSearchForDay2Provider as SqlViewSearchForDay2Provider;}
		}
		
		#endregion
		
		
		#region "ViewSelectAllDetailsByPackageandMrProvider"
		
		private SqlViewSelectAllDetailsByPackageandMrProvider innerSqlViewSelectAllDetailsByPackageandMrProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewSelectAllDetailsByPackageandMr"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewSelectAllDetailsByPackageandMrProviderBase ViewSelectAllDetailsByPackageandMrProvider
		{
			get
			{
				if (innerSqlViewSelectAllDetailsByPackageandMrProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewSelectAllDetailsByPackageandMrProvider == null)
						{
							this.innerSqlViewSelectAllDetailsByPackageandMrProvider = new SqlViewSelectAllDetailsByPackageandMrProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewSelectAllDetailsByPackageandMrProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewSelectAllDetailsByPackageandMrProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewSelectAllDetailsByPackageandMrProvider SqlViewSelectAllDetailsByPackageandMrProvider
		{
			get {return ViewSelectAllDetailsByPackageandMrProvider as SqlViewSelectAllDetailsByPackageandMrProvider;}
		}
		
		#endregion
		
		
		#region "ViewSelectAvailabilityProvider"
		
		private SqlViewSelectAvailabilityProvider innerSqlViewSelectAvailabilityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewSelectAvailability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewSelectAvailabilityProviderBase ViewSelectAvailabilityProvider
		{
			get
			{
				if (innerSqlViewSelectAvailabilityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewSelectAvailabilityProvider == null)
						{
							this.innerSqlViewSelectAvailabilityProvider = new SqlViewSelectAvailabilityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewSelectAvailabilityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewSelectAvailabilityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewSelectAvailabilityProvider SqlViewSelectAvailabilityProvider
		{
			get {return ViewSelectAvailabilityProvider as SqlViewSelectAvailabilityProvider;}
		}
		
		#endregion
		
		
		#region "ViewSpandPpercentageOfMeetingRoomProvider"
		
		private SqlViewSpandPpercentageOfMeetingRoomProvider innerSqlViewSpandPpercentageOfMeetingRoomProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewSpandPpercentageOfMeetingRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewSpandPpercentageOfMeetingRoomProviderBase ViewSpandPpercentageOfMeetingRoomProvider
		{
			get
			{
				if (innerSqlViewSpandPpercentageOfMeetingRoomProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewSpandPpercentageOfMeetingRoomProvider == null)
						{
							this.innerSqlViewSpandPpercentageOfMeetingRoomProvider = new SqlViewSpandPpercentageOfMeetingRoomProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewSpandPpercentageOfMeetingRoomProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewSpandPpercentageOfMeetingRoomProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewSpandPpercentageOfMeetingRoomProvider SqlViewSpandPpercentageOfMeetingRoomProvider
		{
			get {return ViewSpandPpercentageOfMeetingRoomProvider as SqlViewSpandPpercentageOfMeetingRoomProvider;}
		}
		
		#endregion
		
		
		#region "ViewSpandPpercentageOfPackageProvider"
		
		private SqlViewSpandPpercentageOfPackageProvider innerSqlViewSpandPpercentageOfPackageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewSpandPpercentageOfPackage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewSpandPpercentageOfPackageProviderBase ViewSpandPpercentageOfPackageProvider
		{
			get
			{
				if (innerSqlViewSpandPpercentageOfPackageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewSpandPpercentageOfPackageProvider == null)
						{
							this.innerSqlViewSpandPpercentageOfPackageProvider = new SqlViewSpandPpercentageOfPackageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewSpandPpercentageOfPackageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewSpandPpercentageOfPackageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewSpandPpercentageOfPackageProvider SqlViewSpandPpercentageOfPackageProvider
		{
			get {return ViewSpandPpercentageOfPackageProvider as SqlViewSpandPpercentageOfPackageProvider;}
		}
		
		#endregion
		
		
		#region "ViewWsCheckAvailabilityProvider"
		
		private SqlViewWsCheckAvailabilityProvider innerSqlViewWsCheckAvailabilityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ViewWsCheckAvailability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ViewWsCheckAvailabilityProviderBase ViewWsCheckAvailabilityProvider
		{
			get
			{
				if (innerSqlViewWsCheckAvailabilityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlViewWsCheckAvailabilityProvider == null)
						{
							this.innerSqlViewWsCheckAvailabilityProvider = new SqlViewWsCheckAvailabilityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlViewWsCheckAvailabilityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlViewWsCheckAvailabilityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlViewWsCheckAvailabilityProvider SqlViewWsCheckAvailabilityProvider
		{
			get {return ViewWsCheckAvailabilityProvider as SqlViewWsCheckAvailabilityProvider;}
		}
		
		#endregion
		
		
		#region "General data access methods"

		#region "ExecuteNonQuery"
		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		public override void ExecuteNonQuery(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			database.ExecuteNonQuery(commandWrapper);	
			
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		public override void ExecuteNonQuery(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			database.ExecuteNonQuery(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(commandType, commandText);	
		}
		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteNonQuery(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteDataReader"
		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteReader(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteReader(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteReader(commandWrapper);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteReader(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteReader(commandType, commandText);	
		}
		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteReader(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteDataSet"
		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteDataSet(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteDataSet(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteDataSet(commandWrapper);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteDataSet(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteDataSet(commandType, commandText);	
		}
		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteDataSet(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteScalar"
		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override object ExecuteScalar(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteScalar(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteScalar(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override object ExecuteScalar(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteScalar(commandWrapper);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteScalar(commandWrapper, transactionManager.TransactionObject);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override object ExecuteScalar(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteScalar(commandType, commandText);	
		}
		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteScalar(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#endregion


	}
}
