﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Web;

namespace LMMR.Business
{
    public abstract class FacilitiesBase
    {

        //Declare Variable to store data
        private TList<Facility> _varGetFacility;


        //Declare variable to get all facility type
        public TList<Facility> propGetFacility
        {
            get { return _varGetFacility; }
            set { _varGetFacility = value; }

        }

        //Declare Variable to store data
        private TList<HotelFacilities> _varGetHotelFacility;

        //Declare variable to get all facilities by hotel
        public TList<HotelFacilities> propGetHotelFacility
        {
            get { return _varGetHotelFacility; }
            set { _varGetHotelFacility = value; }

        }

        //Declare Variable to message
        private string _varMessage;

        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }


        #region
        /// <summary>
        //This method is used for insert new facility by hotel id
        /// </summary>         
        public string AddNewFacilities(int HotelId, int FacilityId)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                HotelFacilities ObjHotelFacility = new HotelFacilities();
                ObjHotelFacility.FacilitiesId = FacilityId;
                ObjHotelFacility.HotelId = HotelId;

                if (DataRepository.HotelFacilitiesProvider.Insert(ObjHotelFacility))
                {
                    //Audit trail maintain
                    TrailManager.LogAuditTrail<HotelFacilities>(tm, ObjHotelFacility, ObjHotelFacility.GetOriginalEntity(), AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.Facilities), ObjHotelFacility.TableName, Convert.ToString(HotelId));
                    //Audit trail maintain
                    propMessage = "Add successfully!";
                }
                else
                {
                    propMessage = "Please try again!";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }

            return propMessage;
        }
        #endregion

        
        #region
        /// <summary>
        //This method is used for update facility by hotel id
        /// </summary>  
        public string UpdateFacilities(ref HotelFacilities FacilityId)
        {       
             TransactionManager tm = null;
             try
             {
                 tm = DataRepository.Provider.CreateTransaction();
                 tm.BeginTransaction();
                 Guid gid = Guid.NewGuid();
                 //Audit trail maintain
                 HotelFacilities old = FacilityId.GetOriginalEntity();
                 old.Id = FacilityId.Id;
                 TrailManager.LogAuditTrail<HotelFacilities>(tm, FacilityId, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.Facilities), FacilityId.TableName, Convert.ToString(FacilityId.HotelId));
                 //Audit trail maintain
                 if (DataRepository.HotelFacilitiesProvider.Update(FacilityId))
                 {
                     propMessage = "Update successfully!";

                 }
                 else
                 {
                     propMessage = "Please try again!";
                 }
                 tm.Commit();
             }
             catch (Exception ex)
             {
                 tm.Rollback();
             }

            return propMessage;
        }
        #endregion


        #region
        /// <summary>
        //This method is used for delete facility by hotel id
        /// </summary>
        public string DeleteFacilities(int hotelId)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                HotelFacilities ObjHotelFacilitydel = new HotelFacilities();
                ObjHotelFacilitydel.HotelId = hotelId;
                TList<HotelFacilities> lstFac = DataRepository.HotelFacilitiesProvider.GetByHotelId(hotelId);
                Guid gid = Guid.NewGuid();
                //Audit trail maintain
                HotelFacilities old = ObjHotelFacilitydel.GetOriginalEntity();
                old.Id = ObjHotelFacilitydel.Id;
                TrailManager.LogAuditTrail<HotelFacilities>(tm, ObjHotelFacilitydel, old, AuditAction.SD, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.Facilities), ObjHotelFacilitydel.TableName, Convert.ToString(hotelId));
                //Audit trail maintain
                if (DataRepository.HotelFacilitiesProvider.Delete(lstFac) == lstFac.Count)
                {
                    propMessage = "Deleted successfully!";
                }
                else
                {
                    propMessage = "Please try again!";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }

            return propMessage;
        }
        #endregion

        #region
        /// <summary>
        /// Bind six static facilities type
        /// </summary>        
        public  TList<Facility> GetFacilityGnl()
        {
            TList<Facility> ObjGnl = DataRepository.FacilityProvider.GetByFacilityTypeId(1);
            return ObjGnl;
        }

        public TList<Facility> GetFacilityAct()
        {
            TList<Facility> ObjAct = DataRepository.FacilityProvider.GetByFacilityTypeId(2);
            return ObjAct;
        }
        public TList<Facility> GetFacilitySer()
        {
            TList<Facility> ObjSer = DataRepository.FacilityProvider.GetByFacilityTypeId(3);
            return ObjSer;
        }
        public TList<Facility> GetFacilityMeeting()
        {
            TList<Facility> ObjMeeting = DataRepository.FacilityProvider.GetByFacilityTypeId(4);
            return ObjMeeting;
        }
        public TList<Facility> GetFacilityBedroom()
        {
            TList<Facility> ObjBedroom = DataRepository.FacilityProvider.GetByFacilityTypeId(5);
            return ObjBedroom;
        }        

        #endregion
        
                
        #region
        /// <summary>
        //This method is used to Get Facilities by hotel id and facility id
        /// </summary>
        public TList<HotelFacilities> GetFacilitiesByHotelID(int FacilityId,int hotelId)
        {

            int totalout = 0;
            string whereclause = "Hotel_Id='" + hotelId + "' and " + " FacilitiesId='" + FacilityId + "' ";
            TList<HotelFacilities> ObjFacId = DataRepository.HotelFacilitiesProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out totalout);            
            return ObjFacId;
        }
        #endregion

        
        #region
        /// <summary>
        //This method is used to get all list of facility by hotel id
        /// </summary>
        public TList<HotelFacilities> GetHotelFacilitiesByHotelid(int hotelId)
        {
            propGetHotelFacility = DataRepository.HotelFacilitiesProvider.GetByHotelId(hotelId);
            return propGetHotelFacility;
        }
        #endregion

        #region
        /// <summary>
        //This method is used to get facility icon by hotel id
        /// </summary>
        public VList<ViewFacilityIcon> GetFacilityIcon(string where, string orderby)
        {
            int totalcount = 0;
            VList<ViewFacilityIcon> Vlistfic = new VList<ViewFacilityIcon>();
            Vlistfic = DataRepository.ViewFacilityIconProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            return Vlistfic;
        }
        #endregion

    }
}
