﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;

namespace LMMR.Business
{
    public class AvailabilityAndSpecialManagerBase
    {
        public AvailabilityAndSpecialManagerBase()
        {
        } 

        public VList<ViewAvailabilityAndSpecialManager> GetAllAvailabilityAndSpecial()
        {
            return DataRepository.ViewAvailabilityAndSpecialManagerProvider.GetAll();
        }

        public VList<ViewAvailabilityAndSpecialManager> GetByPaged(string whereclause,string orderby,int Start, int pageindex)
        {
            int count = 0;
            if (string.IsNullOrEmpty(orderby))
            {
                orderby = ViewAvailabilityAndSpecialManagerColumn.Id + " ASC";
            }
            return DataRepository.ViewAvailabilityAndSpecialManagerProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out count);
        }
    }
}
