﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.IO;
using System.Security;
using System.Security.AccessControl;
using System.Security.Permissions;
namespace LMMR.Business
{
    public class LanguageManager
    {
        public LanguageManager()
        {

        }

        public TList<Language> GetData()
        {
            return DataRepository.LanguageProvider.GetAll();
        }
        public Language GetLanguageByID(Int64 Languageid)
        {
            return DataRepository.LanguageProvider.GetById(Languageid);
        }
        //Get all language from database wich is active and ready to use.
        public TList<Language> GetActiveLanguage()
        {
            string where = LanguageColumn.IsActive + "=1";
            int count = 0;

            return DataRepository.LanguageProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out count);
        }

        public bool DeleteLanguage(Int64 languageid,string languageName)
        {
            if (DataRepository.LanguageProvider.Delete(languageid))
            {
                try
                {
                    Directory.Delete(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + languageName.ToLower().Replace(" ", "_"), true);
                }
                catch
                {
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddLanguage(Language l)
        {
            if (DataRepository.LanguageProvider.Insert(l))
            {
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.Replace(" ", "_")))
                {
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.Replace(" ", "_"));
                    DirectoryInfo d1 = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.Replace(" ", "_"));
                    DirectorySecurity md1 = d1.GetAccessControl();
                    md1.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.Write, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                    d1.SetAccessControl(md1);

                    if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.Replace(" ", "_")))
                    {
                        File.Copy(AppDomain.CurrentDomain.BaseDirectory + "resource\\english\\resultmessage.xml", AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.Replace(" ", "_") + "\\resultmessage.xml");
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CheckLanguageExist(string languageName, long langid)
        {
            TList<Language> l = DataRepository.LanguageProvider.GetAll();
            Language oldLang = l.Find(a => a.Name.ToLower() == languageName.ToLower() && a.Id != langid);
            if (oldLang != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdateLanguage(Language l,string oldname)
        {
            if (DataRepository.LanguageProvider.Update(l))
            {
                if (l.Name.ToLower() != oldname.ToLower())
                {
                    if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + oldname.ToLower().Replace(" ", "_")) && !Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.ToLower().Replace(" ", "_")))
                    {
                        Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.ToLower().Replace(" ", "_"));
                        DirectoryInfo d1 = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.Replace(" ", "_"));
                        DirectorySecurity md1 = d1.GetAccessControl();
                        md1.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                        d1.SetAccessControl(md1);
                        if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + oldname.ToLower().Replace(" ", "_")) && Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.ToLower().Replace(" ", "_")))
                        {
                            File.Copy(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + oldname.ToLower().Replace(" ", "_") + "\\resultmessage.xml", AppDomain.CurrentDomain.BaseDirectory + "resource\\" + l.Name.ToLower().Replace(" ", "_") + "\\resultmessage.xml");
                        }
                        try
                        {
                            Directory.Delete(AppDomain.CurrentDomain.BaseDirectory + "resource\\" + oldname.ToLower().Replace(" ", "_"), true);
                        }
                        catch
                        {
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
