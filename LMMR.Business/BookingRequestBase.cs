﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Data;
using System.IO;
using System.Configuration;

namespace LMMR.Business
{
    public abstract class BookingRequestBase
    {
        //Declare Variable to store data
        private TList<HotelPhotoVideoGallary> _varGetAllHotelPictureVideo;

        //Property to Store the TList of Entities.HotelPictureVideo class
        public TList<HotelPhotoVideoGallary> propGetAllHotelPictureVideo
        {
            get { return _varGetAllHotelPictureVideo; }
            set { _varGetAllHotelPictureVideo = value; }

        }

        //Declare Variable to store data
        private TList<Others> _varGetOthers;

        //Property to Store the TList of Entities.HotelPictureVideo class
        public TList<Others> propGetAllOthers
        {
            get { return _varGetOthers; }
            set { _varGetOthers = value; }

        }

        //Declare Variable to store data
        private TList<PriorityBasket> _varGetAllPriorityBasket;

        //Property to Store the TList of Entities.HotelPictureVideo class
        public TList<PriorityBasket> propGetAllPriorityBasket
        {
            get { return _varGetAllPriorityBasket; }
            set { _varGetAllPriorityBasket = value; }

        }

        //Declare Variables to store data
        private TList<ActualPackagePrice> _varGetActualPackagePrice;

        //Property to store row of Entities.Hotel class
        public TList<ActualPackagePrice> propGetActualPrice
        {
            get { return _varGetActualPackagePrice; }
            set { _varGetActualPackagePrice = value; }
        }

        //Declare Variables to store data
        private TList<PriorityBasket> _varGetPriorityBasket;

        //Property to store row of Entities.Hotel class
        public TList<PriorityBasket> propGetPriorityBasket
        {
            get { return _varGetPriorityBasket; }
            set { _varGetPriorityBasket = value; }
        }  


        //Declare Variable to message
        private string _varCountry;

        //Property to store message
        public string propCountry
        {
            get { return _varCountry; }
            set { _varCountry = value; }
        }
        //Declare Variable to message
        private string _varCity;

        //Property to store message
        public string propCity
        {
            get { return _varCity; }
            set { _varCity = value; }
        }
        //Declare Variable to message
        private string _varDate;

        //Property to store message
        public string propDate
        {
            get { return _varDate; }
            set { _varDate = value; }
        }
        //Declare Variable to message
        private string _varDuration;

        //Property to store message
        public string propDuration
        {
            get { return _varDuration; }
            set { _varDuration = value; }
        }

        //Declare Variable to message
        private string _varDays;

        //Property to store message
        public string propDays
        {
            get { return _varDays; }
            set { _varDays = value; }
        }

        //Declare Variable to message
        private string _varDay2;

        //Property to store message
        public string propDay2
        {
            get { return _varDay2; }
            set { _varDay2 = value; }
        }

        //Declare Variable to message
        private string _varParticipants;

        //Property to store message
        public string propParticipants
        {
            get { return _varParticipants; }
            set { _varParticipants = value; }
        }

        //Declare Variable to message
        private string _varParticipantSecondary;

        //Property to store message
        public string propParticipantSecondary
        {
            get { return _varParticipantSecondary; }
            set { _varParticipantSecondary = value; }
        }

        //Declare Variable to message
        private string _varRadius;

        //Property to store message
        public string DropRadius
        {
            get { return _varRadius; }
            set { _varRadius = value; }
        }


        //Declare Variable to store data
        private TList<ServeyResponse> _varGetAllServeyResponse;

        //Property to Store the TList of Entities.SurveyResponse class
        public TList<ServeyResponse> propGetAllServeyResponse
        {
            get { return _varGetAllServeyResponse; }
            set { _varGetAllServeyResponse = value; }

        }

        //Declare Variable to store data
        private TList<Serveyresult> _varGetAllServeyresult;

        //Property to Store the TList of Entities.SurveyResponse class
        public TList<Serveyresult> propGetAllServeyresult
        {
            get { return _varGetAllServeyresult; }
            set { _varGetAllServeyresult = value; }

        }


        /// <summary>
        /// Function to get all hotel list after search for Booking
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public VList<BookingRequestViewList> GetBookingReqDetails(string where,string orderby)
        {
            int totalcount = 0;
            VList<BookingRequestViewList> brViewList = new VList<BookingRequestViewList>();
            if (where.Length > 0)
            {
                where += " and ";
            }
            //orderby = "";
            where += BookingRequestViewListColumn.IsPriority + "<>'NO'";
            brViewList = DataRepository.BookingRequestViewListProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            return brViewList;          
        }
        //public VList<BookingRequestViewList> GetBookingReqDetails(string where, string orderby, int CurrentPage, int PageSize, out int totalcount)
        //{
        //    totalcount = 0;
        //    VList<BookingRequestViewList> brViewList = new VList<BookingRequestViewList>();
        //    if (where.Length > 0)
        //    {
        //        where += " and ";
        //    }
        //    where += BookingRequestViewListColumn.IsPriority + "<>'NO'";
        //    brViewList = DataRepository.BookingRequestViewListProvider.GetAll(where, orderby, CurrentPage * PageSize, PageSize, out totalcount);
        //    return brViewList;
        //}
        /// <summary>
        /// Function to get all hotel list after search for Request
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public VList<ViewForRequestSearch> GetReqDetails(string where, string orderby)
        {
            int totalcount = 0;
            VList<ViewForRequestSearch> brReqList = new VList<ViewForRequestSearch>();
            //where += " and " + BookingRequestViewListColumn.IsPriority + "<>'NO'";
            brReqList = DataRepository.ViewForRequestSearchProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            return brReqList;
        }

        /// <summary>
        /// Function to get all list of HotelPictureVideo
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<HotelPhotoVideoGallary> GetAllHotelPictureVideo(int hotelID)
        {
            int Total = 0;
            string whereclause = "HotelId=" + hotelID + " and FileType=1";
            propGetAllHotelPictureVideo = DataRepository.HotelPhotoVideoGallaryProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            return propGetAllHotelPictureVideo;
        }

        /// <summary>
        /// Function to get all list of HotelPictureVideo for Ismain
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<HotelPhotoVideoGallary> GetAlternateTextPictureVideo(int hotelID)
        {
            int Total = 0;
            string whereclause = "HotelId=" + hotelID + " and IsMain=1";
            propGetAllHotelPictureVideo = DataRepository.HotelPhotoVideoGallaryProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            return propGetAllHotelPictureVideo;
        }

        
        /// <summary>
        //This function to get actual package price by hotelid
        /// </summary>  
        public TList<ActualPackagePrice> GetActualpkgPricebyHotelid(int hotelId)
        {
            int Total = 0;
            string whereclause = "HotelId=" + hotelId + " and PackageId=2";
            propGetActualPrice = DataRepository.ActualPackagePriceProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            return propGetActualPrice;
        }

        public VList<ViewMeetingRoomAvailability> GetMeetingRoomAvailability(string where)
        {
            int totalcount = 0;
            VList<ViewMeetingRoomAvailability> VListMeeting = new VList<ViewMeetingRoomAvailability>();
            VListMeeting = DataRepository.ViewMeetingRoomAvailabilityProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out totalcount);
            return VListMeeting;
        }


        public VList<ViewMeetingRoomAvailability> GetAllOnlineMeetingRoomSecondary(string where, int Duration)
        {
            int totalcount = 0;
            VList<ViewMeetingRoomAvailability> VListMeeting = new VList<ViewMeetingRoomAvailability>();
            VListMeeting = DataRepository.ViewMeetingRoomAvailabilityProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out totalcount);
            VList<ViewMeetingRoomAvailability> objMeeting = new VList<ViewMeetingRoomAvailability>();
            if (Duration == 2)
            {
                int count = VListMeeting.Count;
                foreach (ViewMeetingRoomAvailability m in VListMeeting)
                {
                    objMeeting.Add(m);
                    //if (m.AvailabilityDate == StartDate.AddDays(1))
                    //{
                      //  objMeeting.Add(m);
                    //}
                        
                }
            }
            return objMeeting;


            //propGetAllMeetingRoom = DataRepository.MeetingRoomProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            //List<MeetingRoom> objMeeting = new List<MeetingRoom>();
            //if (duration == 2)
            //{
            //    int count = propGetAllMeetingRoom.Count;
            //    foreach (MeetingRoom m in propGetAllMeetingRoom)
            //    {
            //        objMeeting.Add(m);
            //        objMeeting.Add(m);
            //    }
            //}
            //return objMeeting;
        }

        /// <summary>
        /// Function to get all hotel list after search for Request
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public VList<ViewMeetingRoomForRequest> GetMeetingRoomRequest(string where, string orderby)
        {
            int totalcount = 0;
            VList<ViewMeetingRoomForRequest> brReqList = new VList<ViewMeetingRoomForRequest>();
            //where += " and " + BookingRequestViewListColumn.IsPriority + "<>'NO'";
            brReqList = DataRepository.ViewMeetingRoomForRequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            return brReqList;
        }

        /// <summary>
        /// Function to get all hotel list for secondary for duration 2 for Request
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public VList<ViewMeetingRoomForRequest> GetReqDetailsForSecondary(string where, int Duration)
        {          
            int totalcount = 0;
            VList<ViewMeetingRoomForRequest> VListMeeting = new VList<ViewMeetingRoomForRequest>();
            VListMeeting = DataRepository.ViewMeetingRoomForRequestProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out totalcount);
            VList<ViewMeetingRoomForRequest> objMeeting = new VList<ViewMeetingRoomForRequest>();
            if (Duration == 2)
            {
                int count = VListMeeting.Count;
                foreach (ViewMeetingRoomForRequest m in VListMeeting)
                {
                    objMeeting.Add(m);  
                    objMeeting.Add(m); 
                }
            }
            return objMeeting;
        }

        /// <summary>
        //This function to check hotel order as per priority basket
        /// </summary>  
        public TList<SpecialPriceAndPromo> GetSpecialPriceandPromoByDate(int HotelId, DateTime AvailabilityDate)
        {
            TList<SpecialPriceAndPromo> objSpecialPriceAndPromo = new TList<SpecialPriceAndPromo>();
            int counttotal = 0;
            string whereclause = SpecialPriceAndPromoColumn.HotelId + "=" + HotelId + " and " + SpecialPriceAndPromoColumn.SpecialPriceDate + "='" + AvailabilityDate + "'";
            string orderbyclause = string.Empty;
            objSpecialPriceAndPromo = DataRepository.SpecialPriceAndPromoProvider.GetPaged(whereclause, orderbyclause, 0, int.MaxValue, out counttotal);
            return objSpecialPriceAndPromo;
        }

        //Bind priority basket for booking/request
        public TList<PriorityBasket> GetPriorityBasket(string where, string orderby)
        {
            int totalout = 0;
            propGetPriorityBasket = DataRepository.PriorityBasketProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalout);
            DataRepository.PriorityBasketProvider.DeepLoad(propGetPriorityBasket);
            return propGetPriorityBasket;
        }

        //Insert hotel into Priority Basket
        public void InsertPriorityBaket(PriorityBasket objPri)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DataRepository.PriorityBasketProvider.Insert(objPri);
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
        }

        //Delete hotel from Priority Basket
        public void DeletePriorityBaket(PriorityBasket objPri)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DataRepository.PriorityBasketProvider.Delete(objPri);
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
        }

        //Update Priority Basket as per hotel id
        public bool UpdatePriorityBaket(PriorityBasket objPri)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DataRepository.PriorityBasketProvider.Update(tm, objPri);
                tm.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }

        //Get details as per basket id
        public PriorityBasket GetBasketByID(int basketId)
        {
            PriorityBasket objBasket = DataRepository.PriorityBasketProvider.GetById(basketId);
            return objBasket;
        }

        //Get details as per basket id and 
        public PriorityBasket GetBasketByPriorityID(int basketId,bool Type)
        {
            PriorityBasket objBasket = DataRepository.PriorityBasketProvider.GetById(basketId);
            return objBasket;
        }

        //Check availability check for Prioritybasket booking case
        public VList<ViewForAvailabilityCheck> AvailabilityCheck(string where)
        {
            int totalcount = 0;
            VList<ViewForAvailabilityCheck> Vlistreq = new VList<ViewForAvailabilityCheck>();
            Vlistreq = DataRepository.ViewForAvailabilityCheckProvider.GetPaged(where, String.Empty, 0, int.MaxValue, out totalcount);
            return Vlistreq;
        }


        //Status change for booking/request in priority basket by user id
        public bool ChangePriorityActiveStatus(Int32 Id, bool status)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                PriorityBasket objPriority = DataRepository.PriorityBasketProvider.GetById(Id);
                objPriority.IsActive = status;
                DataRepository.PriorityBasketProvider.Update(tm, objPriority);
                tm.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Function to get number of request basket by hotel id
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<Others> GetNoBasketbyHotelId(int CountryId)
        {
            int Total = 0;
            string whereclause = "CountryId=" + CountryId + "";
            propGetAllOthers = DataRepository.OthersProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            return propGetAllOthers;
        }



        /// <summary>
        /// Function to get survey response as per hotel id
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<ServeyResponse> GetServeyResponse(Int64 HotelId)
        {
            int Total = 0;
            string whereclause = "VanueId=" + HotelId + " and " + "ReviewStatus=1";            
            propGetAllServeyResponse = DataRepository.ServeyResponseProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            return propGetAllServeyResponse;
        }

        /// <summary>
        /// Function to get survey result as per hotel id
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<Serveyresult> GetServeyresult(string where)
        {
            int Total = 0;
            propGetAllServeyresult = DataRepository.ServeyresultProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out Total);
            return propGetAllServeyresult;
        }
    }
}
