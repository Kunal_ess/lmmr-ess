﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
#endregion


namespace LMMR.Business
{
    public abstract class ManageCMSContentBase
    {
        #region Constructor
        public ManageCMSContentBase() { }
        #endregion

        #region Variables

        private bool _varIsMailExist;
        private TList<CmsDescription> _varGetCMSContent;
        private TList<Hotel> _varGetHotelNames;
        private string _varstMessage;
        private int _varMinConsiderSpl;
        private TList<Others> _varOthers;


        #endregion

        #region Properties
        public TList<CmsDescription> propGetCMSContent
        {
            get { return _varGetCMSContent; }
            set { _varGetCMSContent = value; }
        }

        public string strMessage
        {
            get { return _varstMessage; }
            set { _varstMessage = value; }
        }

        public bool propIsMailExist
        {
            get { return _varIsMailExist; }
            set { _varIsMailExist = value; }
        }

        public TList<Hotel> propGetHotelNames
        {
            get { return _varGetHotelNames; }
            set { _varGetHotelNames = value; }
        }

        public int propMinConsiderSpl
        {
            get { return _varMinConsiderSpl; }
            set { _varMinConsiderSpl = value; }

        }
        public TList<Others> propOthers
        {
            get { return _varOthers; }
            set { _varOthers = value; }
        }
        #endregion

        #region Functions

        /// <summary>
        /// Get hotel name from Hotel table
        /// </summary>
        /// <returns></returns>
        public TList<Hotel> GetTopFiveHotel()
        {
            //TransactionManager tm = null;
            //try
            //{
            //    tm = DataRepository.Provider.CreateTransaction();
            //    tm.BeginTransaction();
                int totalcount = 0;
                string orderby = "GoOnlineDate desc";
                string where = "IsActive=1 and GoOnline=1 and Isremoved=0";
                int max = 5;
                TList<Hotel> Tlistreq = new TList<Hotel>();
                Tlistreq = DataRepository.HotelProvider.GetPaged(where, orderby, 0, max, out totalcount);
            //    tm.Commit();
            //}
            //catch
            //{
            //    tm.Rollback();
            //}
                return Tlistreq;
        }

        /// <summary>
        /// Get  CMSDescription by Cms id
        /// </summary>
        public TList<CmsDescription> GetCMSContent(long cmsTypeID, long languageID)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                int count = 0;
                string orderbyclause = string.Empty;
                string where = "CmsId=" + cmsTypeID + " and IsActive=1";
                if (languageID != 0)
                {
                    if (where.Length > 0)
                    {
                        where += " and ";
                    }
                    where += "LanguageID=" + languageID;
                }
                propGetCMSContent = DataRepository.CmsDescriptionProvider.GetPaged(tm, where, orderbyclause, 0, int.MaxValue, out count);

                if (propGetCMSContent.Count <= 0)
                {
                    long GetlanguageID = 0;
                    TList<Language> lng = DataRepository.LanguageProvider.GetAll(tm);
                    if (lng.Find(a => a.Name == "English") != null)
                    {
                        GetlanguageID = lng.Find(a => a.Name == "English").Id;
                    }
                    int count1 = 0;
                    string orderbyclause1 = string.Empty;
                    string where1 = "CmsId=" + cmsTypeID + "and LanguageID=" + GetlanguageID + " and IsActive=1";
                    propGetCMSContent = DataRepository.CmsDescriptionProvider.GetPaged(tm, where1, orderbyclause1, 0, int.MaxValue, out count1);
                }
                tm.Commit();
            }
            catch
            {
                tm.Rollback();
            }
            return propGetCMSContent;
        }

        /// <summary>
        /// Insert values in Newslettersubscriber
        /// </summary>
        /// <param name="objNewsLetter"></param>
        public bool AddSubscription(NewsLetterSubscriber objNewsLetter)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                objNewsLetter.Isactive = "1";
                objNewsLetter.SubmittedDate = DateTime.Now;
                DataRepository.NewsLetterSubscriberProvider.Insert(objNewsLetter);
                tm.Commit();
                return true;
            }
            catch
            {
                tm.Rollback();
                return false;
            }

        }

        /// <summary>
        /// Retrieve the mailid from newsscriber page
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Bool</returns>
        public bool IsMailExist(string email)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                TList<NewsLetterSubscriber> objnews = DataRepository.NewsLetterSubscriberProvider.GetAll();
                propIsMailExist = true;
                foreach (NewsLetterSubscriber n in objnews)
                {
                    if (email.ToLower() == n.SubscriberEmailid.ToLower())
                    {
                        propIsMailExist = false;
                        break;
                    }

                }
                tm.Commit();
            }
            catch
            {
                tm.Rollback();
            }
            return propIsMailExist;
        }

        /// <summary>
        /// This Method is used for  Scrollmovement of hotel of the week 
        /// </summary>
        /// <returns></returns>
        public TList<Others> GetMinConsiderSpl()
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                propOthers = DataRepository.OthersProvider.GetAll();
                tm.Commit();
            }
            catch { tm.Rollback(); }
            return propOthers;
        }


        public City GetCityIdByName(string strCityName)
        {
            int count = 0;
            string orderbyclause = string.Empty;
            string where = "City='" + strCityName + "'";
            City objCityName = DataRepository.CityProvider.GetPaged(where, orderbyclause, 0, int.MaxValue, out count).FirstOrDefault();
            return objCityName;
        }

        public Zone GetZoneIdByName(long intcityID, string strZoneName)
        {
            int count = 0;
            string orderbyclause = string.Empty;
            string where = "Zone='" + strZoneName + "' AND CityId =" + intcityID + "";
            Zone objZoneName = DataRepository.ZoneProvider.GetPaged(where, orderbyclause, 0, int.MaxValue, out count).FirstOrDefault();
            return objZoneName;
        }

        public Country GetCountryIdByName(string strCountryName)
        {
            int count = 0;
            string orderbyclause = string.Empty;
            string where = CountryColumn.CountryName + "= '" + strCountryName + "'";
            Country objCountryName = DataRepository.CountryProvider.GetPaged(where, orderbyclause, 0, int.MaxValue, out count).FirstOrDefault();
            return objCountryName == null ? new Country() : objCountryName;
        }
        #endregion
    }
}
