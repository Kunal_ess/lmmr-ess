﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
#endregion

namespace LMMR.Business
{
    public class Userdatabase
    {
        public string update(Users newUser)
        {
            TransactionManager transaction = null;

            try
            {
                //transaction = DataRepository.Provider.CreateTransaction();
                //transaction.BeginTransaction();
                ////if (newUser.IsActive) 
                ////{

                ////    newUser.IsActive = false;
                ////}
                ////else
                ////{
                ////    newUser.IsActive = true;
                ////}
                //if ((DataRepository.UsersProvider.Update(newUser)))
                //{

                //    // Show proper message
                //}
                //else
                //{
                //    return "Information could not be saved.";
                //}
                //transaction.Commit();
                return "Information updated successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }

        public string Delete(UserDetails newuserdelete)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                newuserdelete.IsDeleted = true;
                if ((DataRepository.UserDetailsProvider.Update(newuserdelete)))
                {
                    Users changeuseractive = DataRepository.UsersProvider.GetByUserId(Convert.ToInt64(newuserdelete.UserId));
                    changeuseractive.IsActive = false;
                    DataRepository.UsersProvider.Update(changeuseractive);
                    // Show proper message
                }
                else
                {
                    return "Information could not be Delete.";
                }
                transaction.Commit();
                return "Information Deleted successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Deleted.Please contact Administrator.";
            }
        }

        public bool DeleteUser(Users objUser, UserDetails objUserDetail)
        {
            TransactionManager transaction = null;
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if(objUserDetail!=null)
                {
                    DataRepository.UserDetailsProvider.Update(objUserDetail);
                }
                if (objUser!=null)
                {
                    DataRepository.UsersProvider.Update(objUser);
                }

                transaction.Commit();
                return true;
            }
            catch
            {
                transaction.Rollback();
                return false;

            }

        }

    }
}
