﻿
#region Included NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;

#endregion

namespace LMMR.Business
{
    public abstract class SuperAdminTaskManagerBase
    {
        #region Methods

        /// <summary>
        /// This method updates the RankingAlgoMaster entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>

        public string updateInfo(RankingAlgoMaster entity)
        {
            TransactionManager transaction = null;
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if (DataRepository.RankingAlgoMasterProvider.Update(transaction,entity))
                {
                    transaction.Commit();
                    return "Information updated successfully.";
                }
                else
                {
                    return "Information could not be saved.";
                }
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.";
            }
        }

        /// <summary>
        /// This method gets all the first 11 records from the rankingAlgoMaster table.
        /// </summary>
        /// <returns></returns>

        public TList<RankingAlgoMaster> getInfo()
        {
            int count;
            return (DataRepository.RankingAlgoMasterProvider.GetPaged(string.Empty, string.Empty, 0, 11, out count));
        }

        /// <summary>
        /// This method gets all the active languages.
        /// </summary>
        /// <returns></returns>

        public TList<Language> getActiveLanguages()
        {
            int count;
            return (DataRepository.LanguageProvider.GetPaged("IsActive = 1",string.Empty,0,int.MaxValue,out count));
        }

        /// <summary>
        /// This method gets the cms description on the basis of cmsId and languageId.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>

        public string getCmsDescription(long cmsId, int languageId)
        {
            int count;
            if (DataRepository.CmsDescriptionProvider.GetPaged("CmsId = " + cmsId + " and LanguageId = " + languageId, string.Empty, 0, int.MaxValue, out count).Count != 0)
            {
                return DataRepository.CmsDescriptionProvider.GetPaged("CmsId = " + cmsId + " and LanguageId = " + languageId, string.Empty, 0, int.MaxValue, out count)[0].ContentsDesc;
            }
            return "";
        }

        /// <summary>
        /// This method updates the cms description entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>

        public bool updateCMSDescriptionEntity(CmsDescription entity)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                result = DataRepository.CmsDescriptionProvider.Update(tm,entity);
                tm.Commit();
            }
            catch
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }
        public bool updateCMSDescriptionEntity(TList<CmsDescription> entity)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                if (DataRepository.CmsDescriptionProvider.Update(tm, entity) > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                tm.Commit();
            }
            catch
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }
        /// <summary>
        /// This method gets the cms description entity on the basis of cmsId and languageId.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>

        public CmsDescription getCmsDescriptionEntity(long cmsId, long languageId)
        {
            int count;
            if (DataRepository.CmsDescriptionProvider.GetPaged("CmsId = " + cmsId + " and LanguageId = " + languageId, string.Empty, 0, int.MaxValue, out count).Count != 0)
            {
                return DataRepository.CmsDescriptionProvider.GetPaged("CmsId = " + cmsId + " and LanguageId = " + languageId, string.Empty, 0, int.MaxValue, out count)[0];
            }
            return null;
        }

        /// <summary>
        /// This method saves/inserts cms description entity.
        /// </summary>
        /// <param name="newEntity"></param>
        /// <returns>bool</returns>

        public bool saveCMSDescriptionEntity(CmsDescription newEntity)
        {
            return DataRepository.CmsDescriptionProvider.Insert(newEntity);
        }

        public bool saveCMSDescriptionEntity(TList<CmsDescription> newEntity)
        {
            return DataRepository.CmsDescriptionProvider.Insert(newEntity)>0?true:false;
        }

        /// <summary>
        /// This method gets the cms entity on the basis of cmsId.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <returns>Cms</returns>

        public Cms getCmsEntity(long cmsId)
        {
            return DataRepository.CmsProvider.GetById(cmsId);
        }

        /// <summary>
        /// This method returns multiple cms descriptions on the basis of cmsId and languageId.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        
        public TList<CmsDescription> getCmsDescriptionAll(long cmsId,int languageId)
        {
            int count;
            return DataRepository.CmsDescriptionProvider.GetPaged("CmsId = " + cmsId + " and LanguageId = " + languageId, string.Empty, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// This method returns cms description on the basis of Id.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>

        public CmsDescription getCmsDescriptionOnID(long Id)
        {
            return DataRepository.CmsDescriptionProvider.GetById(Id);
        }

        /// <summary>
        /// This method returns the cms description records on the basis of the cmsId.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <returns>TList<CmsDescription></returns>

        public TList<CmsDescription> getCmsDescriptionOnCmsID(long cmsId)
        {
            return DataRepository.CmsDescriptionProvider.GetByCmsId(cmsId);
        }

        /// <summary>
        /// This is an overload method which returns cmsDescription entity on the basis of cmsId, LanguageId, ContentShortDesc.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <param name="languageId"></param>
        /// <param name="contentShortDesc"></param>
        /// <returns>CmsDescription</returns>

        public CmsDescription getCmsDescriptionEntity(long cmsId, long languageId ,string contentTitle)
        {
            int count;
            return DataRepository.CmsDescriptionProvider.GetPaged("CmsId = " + cmsId + " and LanguageId = " + languageId + " and ContentTitle = '" + contentTitle + "'", string.Empty, 0, int.MaxValue, out count).Count == 0 ? null : DataRepository.CmsDescriptionProvider.GetPaged("CmsId = " + cmsId + " and LanguageId = " + languageId + " and ContentTitle = '" + contentTitle + "'", string.Empty, 0, int.MaxValue, out count)[0];
        }

        /// <summary>
        /// This  method returns cms entity on the basis of cmsType.
        /// </summary>
        /// <param name="cmsType"></param>
        /// <returns>Cms</returns>

        public Cms getCmsEntityOnType(string cmsType)
        {
            int count;
            Cms obj=DataRepository.CmsProvider.GetPaged("CmsType = '"+cmsType+"'",string.Empty,0,int.MaxValue,out count).FirstOrDefault();
            if (obj != null)
            {
                return obj;
            }
            return null;
        }
        public TList<Cms> getAllCmsDetails()
        {
            return DataRepository.CmsProvider.GetAll();
        }
        public TList<CmsDescription> getAllCmsDescDetails()
        {
            return DataRepository.CmsDescriptionProvider.GetAll();
        }
        /// <summary>
        /// This method inserts a new cms type into the cms table.
        /// </summary>
        /// <param name="ofType"></param>
        /// <returns>long</returns>

        public long createCmsEntity(string ofType)
        {
            int count;
            Cms entity = new Cms();
            entity.CmsType = ofType;
            entity.IsActive = true;
            entity.CreationDate = DateTime.Now;
            bool result = DataRepository.CmsProvider.Insert(entity);
            if(result)
            {
                entity=null;
                entity = DataRepository.CmsProvider.GetPaged("CmsType = '"+ofType+"'",string.Empty,0,int.MaxValue,out count).FirstOrDefault();
                if(entity!=null)
                {
                    return entity.Id;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// This method updates the cms entity.
        /// </summary>
        /// <param name="entity"></param>

        public bool updateCmsEntity(Cms entity)
        {
            return DataRepository.CmsProvider.Update(entity);
        }

        /// <summary>
        /// This method deletes a cms description entity on the basis of primary key Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public bool deleteCmsDescriptionEntity(long id)
        {
            bool status=false;
            CmsDescription obj = DataRepository.CmsDescriptionProvider.GetById(id);
            TList<CmsDescription> allCmsDescriptions = getAllCmsDescriptionsOnTitle(obj.ContentTitle);
            foreach (CmsDescription desc in allCmsDescriptions)
            {
                status = DataRepository.CmsDescriptionProvider.Delete(desc.Id);
            }
            return status;
        }

        /// <summary>
        /// This method returns the ACTIVE cms description records on the basis of the cmsId.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <returns></returns>

        public TList<CmsDescription> getActiveCmsDescriptionOnCmsID(long cmsId)
        {
            int count;
            return DataRepository.CmsDescriptionProvider.GetPaged("CmsId=" + cmsId + " and IsActive=1", string.Empty, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// This method returns all the zones on the basis of cityId.
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>

        public TList<Zone> getZonesByCityId(long cityId)
        {
            int count;
            return DataRepository.ZoneProvider.GetPaged("CityId=" + cityId, string.Empty, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// This method returns all the cms descriptions on the basis of title.
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>

        public TList<CmsDescription> getAllCmsDescriptionsOnTitle(string title)
        {
            int count;
            return (DataRepository.CmsDescriptionProvider.GetPaged("ContentTitle='" + title + "'", string.Empty, 0, int.MaxValue, out count));
        }

        #endregion
    }
}
