﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Data;
using System.IO;
using System.Configuration;
using System.Web;
using System.Xml;

namespace LMMR.Business
{

    public class LogBase
    {
        private DataTable GetTableGenearlInfo()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Name", System.Type.GetType("System.String"));
            table.Columns.Add("Country", System.Type.GetType("System.String"));
            table.Columns.Add("City", System.Type.GetType("System.String"));
            table.Columns.Add("Zone", System.Type.GetType("System.String"));
            table.Columns.Add("Stars", System.Type.GetType("System.String"));
            table.Columns.Add("Zip Code", System.Type.GetType("System.String"));
            table.Columns.Add("Hotel Address", System.Type.GetType("System.String"));
            table.Columns.Add("Longitude", System.Type.GetType("System.String"));
            table.Columns.Add("Latitude", System.Type.GetType("System.String"));
            table.Columns.Add("Is Active ?", System.Type.GetType("System.String"));
            table.Columns.Add("Operator Choice", System.Type.GetType("System.String"));
            table.Columns.Add("Contract Value", System.Type.GetType("System.String"));
            table.Columns.Add("Time Zone", System.Type.GetType("System.String"));
            table.Columns.Add("Phone Ext", System.Type.GetType("System.String"));
            table.Columns.Add("Phone", System.Type.GetType("System.String"));
            table.Columns.Add("Fax Ext", System.Type.GetType("System.String"));
            table.Columns.Add("Fax", System.Type.GetType("System.String"));
            table.Columns.Add("Renting time Morning (From)", System.Type.GetType("System.String"));
            table.Columns.Add("Renting time Morning (To)", System.Type.GetType("System.String"));
            table.Columns.Add("Renting time Afternoon (From)", System.Type.GetType("System.String"));
            table.Columns.Add("Renting time Afternoon (To)", System.Type.GetType("System.String"));
            table.Columns.Add("Renting time Full (From)", System.Type.GetType("System.String"));
            table.Columns.Add("Renting time Full (To)", System.Type.GetType("System.String"));
            table.Columns.Add("Types of Credit Card Accepted", System.Type.GetType("System.String"));
            table.Columns.Add("Themes", System.Type.GetType("System.String"));
            table.Columns.Add("Is Bedroom Available", System.Type.GetType("System.String"));
            table.Columns.Add("No of Bedroom Available", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));

            return table;
        }

        private DataTable GetTableContact()
        {
            DataTable table = new DataTable();
            table.Columns.Add("ContactType", System.Type.GetType("System.String"));
            table.Columns.Add("UserType", System.Type.GetType("System.String"));
            table.Columns.Add("JobTitle", System.Type.GetType("System.String"));
            table.Columns.Add("FirstName", System.Type.GetType("System.String"));
            table.Columns.Add("LastName", System.Type.GetType("System.String"));
            table.Columns.Add("Phone", System.Type.GetType("System.String"));
            table.Columns.Add("Ext", System.Type.GetType("System.String"));
            table.Columns.Add("Email", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
        private DataTable GetTableFacilities()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Facilities", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
        private DataTable GetTableMeetingDescription()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Name", System.Type.GetType("System.String"));
            table.Columns.Add("DayLight", System.Type.GetType("System.String"));
            table.Columns.Add("Surface", System.Type.GetType("System.String"));
            table.Columns.Add("Height", System.Type.GetType("System.String"));
            table.Columns.Add("Price Halfday", System.Type.GetType("System.String"));
            table.Columns.Add("Price Fullday", System.Type.GetType("System.String"));
            table.Columns.Add("Is Online ?", System.Type.GetType("System.String"));
            table.Columns.Add("Is Deleted ?", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
        private DataTable GetTableMeetingConfiguration()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Name", System.Type.GetType("System.String"));
            table.Columns.Add("RoomShape", System.Type.GetType("System.String"));
            table.Columns.Add("Min Capacity", System.Type.GetType("System.String"));
            table.Columns.Add("Max Capicity", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
    
        private DataTable GetTablebedrooms()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Bedroom Type", System.Type.GetType("System.String"));
            table.Columns.Add("Allotment", System.Type.GetType("System.String"));           
            table.Columns.Add("Price Double", System.Type.GetType("System.String"));
            table.Columns.Add("Price Single", System.Type.GetType("System.String"));
            table.Columns.Add("Is BreakFast Included", System.Type.GetType("System.String"));           
             table.Columns.Add("Breakfast Price", System.Type.GetType("System.String"));
            table.Columns.Add("Is Deleted?", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
        private DataTable GetTablePricing()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Package/Category Name", System.Type.GetType("System.String"));
            table.Columns.Add("Item Name", System.Type.GetType("System.String"));
            table.Columns.Add("Halfday Price", System.Type.GetType("System.String"));
            table.Columns.Add("FulldayPrice", System.Type.GetType("System.String"));
            table.Columns.Add("IsOnline", System.Type.GetType("System.String"));            
            table.Columns.Add("Is Complementary?", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
        private DataTable GetTableBooking()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Reference No", System.Type.GetType("System.String"));
            table.Columns.Add("Last Status", System.Type.GetType("System.String"));
            table.Columns.Add("Current Status", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
        private DataTable GetTableAvailabilityMR()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Date", System.Type.GetType("System.String"));
            table.Columns.Add("Name of Meeting room / Bedroom Type", System.Type.GetType("System.String"));
            table.Columns.Add("Morning Status", System.Type.GetType("System.String"));
            table.Columns.Add("Afternoon Status", System.Type.GetType("System.String"));
            table.Columns.Add("No of rooms available", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
     
        private DataTable GetTableSpecialPromo()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Special Price Date", System.Type.GetType("System.String"));
            table.Columns.Add("Package Percent", System.Type.GetType("System.String"));
            table.Columns.Add("MeetingRoom Percent", System.Type.GetType("System.String"));
            table.Columns.Add("Updated By", System.Type.GetType("System.String"));
            table.Columns.Add("Updated Date", System.Type.GetType("System.String"));
            return table;
        }
        public DataTable GetTable(string tablename, string HotelID, DateTime startdate, DateTime enddate)
        {
       
            DataTable dt = new DataTable();
            DataSet dsmain = new DataSet();
         
           // TList<DbAudit> dba = DataRepository.DbAuditProvider.GetAll(); 
          //  TList<DbAudit> dba = DataRepository.DbAuditProvider.GetPaged(whereClause, orderBy, 0, 0, 0);
            int totalcount = 0;
            string where = " EntityTblName ='" + tablename + "' and  RevisionStamp  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "' and ReferenceId ='" + HotelID + "'";
            string orderby = "AuditID desc";
            TList<DbAudit> dba = DataRepository.DbAuditProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            TList<MeetingRoom> tistMR = DataRepository.MeetingRoomProvider.GetByHotelId(Convert.ToInt64(HotelID));
            TList<Availability> tistAR = DataRepository.AvailabilityProvider.GetByHotelId(Convert.ToInt64(HotelID));
            foreach (DbAudit db in dba)
            {
                DataSet dschild = new DataSet();
                DataSet dstimestamp = new DataSet();
                dstimestamp.Tables.Add(new DataTable());

                dstimestamp.Tables[0].Columns.Add("updatedBy", typeof(string));
                dstimestamp.Tables[0].Columns.Add("UpdateDate", typeof(string));

                //if (db.EntityTblName == tablename)
                //{

                    string xml = db.NewData;
                    TextReader txtReader = new StringReader(xml);
                    XmlReader reader = new XmlTextReader(txtReader);
                    dschild.ReadXml(reader);
                    dschild.Tables[0].Columns.Add("updatedBy", typeof(string));
                    dschild.Tables[0].Columns.Add("UpdateDate", typeof(string));

                    if (Convert.ToInt64(db.UserName) == 0)
                    {
                        db.UserName = 1;
                    }
                    dschild.Tables[0].Rows[0]["updatedBy"] = DataRepository.UsersProvider.GetByUserId(Convert.ToInt64(db.UserName)).FirstName;
                    dschild.Tables[0].Rows[0]["UpdateDate"] = db.RevisionStamp;
                    if (tablename == "Hotel")
                    {
                        if (dschild.Tables[0].Rows[0]["id"].ToString() == HotelID)
                        {
                            dsmain.Merge(dschild);
                        }
                    }
                    if (tablename == "HotelContact" || tablename == "HotelFacilities" || tablename == "MeetingRoom" || tablename == "BedRoom" || tablename == "PackageByHotel" || tablename == "SpecialPriceAndPromo")
                    {
                        if (dschild.Tables[0].Rows[0]["HotelId"].ToString() == HotelID)
                        {
                            dsmain.Merge(dschild);
                        }
                    }

                    if (tablename == "MeetingRoomConfig")
                    {
                        long MeetingRoomId = Convert.ToInt64(dschild.Tables[0].Rows[0]["MeetingRoomId"]);
                        List<MeetingRoom> temp = tistMR.Where(u => u.Id == MeetingRoomId).ToList();

                        foreach (MeetingRoom mr in temp)
                        {
                            dsmain.Merge(dschild);
                        }

                    }
                    if (tablename == "AvailibilityOfRooms")
                    {
                        long AvailibilityIdForBedRoom = Convert.ToInt64(dschild.Tables[0].Rows[0]["AvailibilityIdForBedRoom"]);
                        List<Availability> temp = tistAR.Where(u => u.Id == AvailibilityIdForBedRoom).ToList();

                        foreach (Availability mr in temp)
                        {
                            dsmain.Merge(dschild);
                        }

                    }
                    if (tablename == "ClientContract")
                    {
                        if (dschild.Tables[0].Rows[0]["id"].ToString() == HotelID)
                        {
                            dsmain.Merge(dschild);
                        }
                    }

                //}
            }

            if (dsmain.Tables.Count>0)
            {
                
                #region Hotel
                if (tablename == "Hotel")
                {
                    dt = GetTableGenearlInfo();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {

                        string country = DataRepository.CountryProvider.GetById(Convert.ToInt64(dr["CountryId"])).CountryName;
                        string city = DataRepository.CityProvider.GetById(Convert.ToInt64(dr["CityId"])).City;
                        string Zone = DataRepository.ZoneProvider.GetById(Convert.ToInt64(dr["ZoneId"])).Zone;
                        string isactive = dr["IsActive"].ToString();
                        string TimeZone = dr["TimeZone"].ToString();
                        string noofbed = "N/A";
                        if (dr["IsBedroomAvailable"].ToString() == "true")
                        {
                            noofbed = dr["NumberOfBedroom"].ToString();
                        }

                        dt.Rows.Add(dr["Name"].ToString(), country, city, Zone, dr["Stars"].ToString(), dr["ZipCode"].ToString(), dr["HotelAddress"].ToString(), dr["Longitude"].ToString(), dr["Latitude"].ToString(), isactive, dr["OperatorChoice"].ToString(), dr["ContractValue"].ToString(), TimeZone, dr["CountryCodePhone"].ToString(), dr["PhoneNumber"].ToString(), dr["CountryCodeFax"].ToString(), dr["FaxNumber"].ToString(), dr["RtMFrom"].ToString(), dr["RtMTo"].ToString(), dr["RtAFrom"].ToString(), dr["RtATo"].ToString(), dr["RtFFrom"].ToString(), dr["RtFTo"].ToString(), dr["CreditCardType"].ToString(), dr["Theme"].ToString(), dr["IsBedroomAvailable"].ToString(), noofbed, dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());


                    }

                    dt.AcceptChanges();

                }
                #endregion
                #region Hotelcontact
                if (tablename == "HotelContact")
                {
                    dt = GetTableContact();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {
                        dt.Rows.Add(dr["ContactType"].ToString(), dr["UserType"].ToString(), dr["JobTitle"].ToString(), dr["FirstName"].ToString(), dr["LastName"].ToString(), dr["Ext"].ToString(), dr["Phone"].ToString(), dr["Email"].ToString(), dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());
                    }

                    dt.AcceptChanges();

                }
                #endregion
                #region Hotel facility
                if (tablename == "HotelFacilities")
                {
                    dt = GetTableFacilities();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {
                        if (Convert.ToInt64(dr["FacilitiesId"].ToString()) != 0)
                        {
                            string facilityname = DataRepository.FacilityProvider.GetById(Convert.ToInt64(dr["FacilitiesId"].ToString())).FacilityName;
                            dt.Rows.Add(facilityname, dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());
                        }
                    }

                    dt.AcceptChanges();

                }
                #endregion
                #region Meetingroom
                if (tablename == "MeetingRoom")
                {
                    dt = GetTableMeetingDescription();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {
                        string daylight = "No";
                        if (dr["DayLight"].ToString() == "0")
                        {
                            daylight = "No";
                        }
                        if (dr["DayLight"].ToString() == "1")
                        {
                            daylight = "Yes";
                        }
                        if (dr["DayLight"].ToString() == "2")
                        {
                            daylight = "Partial";
                        }
                        dt.Rows.Add(dr["Name"].ToString(), daylight, dr["Surface"].ToString(), dr["Height"].ToString(), dr["HalfdayPrice"].ToString(), dr["FulldayPrice"].ToString(), dr["IsOnline"].ToString(), dr["IsDeleted"].ToString(), dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());

                    }

                    dt.AcceptChanges();


                }
                #endregion
                #region Mr Config
                if (tablename == "MeetingRoomConfig")
                {
                    dt = GetTableMeetingConfiguration();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {
                        string roomshape = Enum.GetName(typeof(MeetingRoomShapeName), Convert.ToInt32(dr["RoomShapeId"].ToString()));
                        string mrname = DataRepository.MeetingRoomProvider.GetById(Convert.ToInt64(dr["MeetingRoomId"].ToString())).Name;
                        dt.Rows.Add(mrname, roomshape, dr["MinCapacity"].ToString(), dr["MaxCapicity"].ToString(), dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());

                    }

                    dt.AcceptChanges();

                }
                #endregion
                #region Bedroom
                if (tablename == "BedRoom")
                {
                    dt = GetTablebedrooms();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {
                        string type = Enum.GetName(typeof(BedRoomType), Convert.ToInt32(dr["Types"].ToString()));
                        string pd = "", ps = "", bp = "", bedroonincl = "";
                        if (string.IsNullOrEmpty(dr["PriceDouble"].ToString()))
                        {
                            pd = "N/A";

                        }
                        else
                        {
                            pd = dr["PriceDouble"].ToString();
                        }
                        if (string.IsNullOrEmpty(dr["PriceSingle"].ToString()))
                        {
                            ps = "N/A";

                        }
                        else
                        {
                            ps = dr["PriceSingle"].ToString();
                        }

                        if (dr["IsBreakFastInclude"].ToString() == "true")
                        {
                            bp = "N/A";
                            bedroonincl = "Yes";
                        }
                        else
                        {

                            bp = Convert.ToString(dr["BreakfastPrice"].ToString());
                            bedroonincl = "No";
                        }

                        dt.Rows.Add(type, dr["Allotment"].ToString(), pd, ps, bedroonincl, bp, dr["IsDeleted"].ToString(), dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());

                    }

                    dt.AcceptChanges();

                }
                #endregion
                #region pricing
                if (tablename == "PackageByHotel")
                {
                    dt = GetTablePricing();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {
                        // string type = Enum.GetName(typeof(BedRoomType), Convert.ToInt32(dr["Types"].ToString()));
                        string packCategory = "", Itemname = "", online = "", compli = "";

                        Itemname = DataRepository.PackageItemsProvider.GetById(Convert.ToInt64(dr["ItemId"].ToString())).ItemName;

                        if (string.IsNullOrEmpty(dr["PackageId"].ToString()))
                        {
                            packCategory = Enum.GetName(typeof(ItemType), Convert.ToInt32(DataRepository.PackageItemsProvider.GetById(Convert.ToInt64(dr["ItemId"].ToString())).ItemType));


                            if (dr["IsOnline"].ToString() == "true")
                            {
                                online = "Yes";
                            }
                            else
                            {

                                online = "No";
                            }
                        }
                        else
                        {
                            packCategory = DataRepository.PackageMasterProvider.GetById(Convert.ToInt64(dr["PackageId"].ToString())).PackageName;

                            if (dr["IsOnline"].ToString() == "true")
                            {
                                online = "No";
                            }
                            else
                            {

                                online = "Yes";
                            }
                        }



                        if (dr["IsComplementary"].ToString() == "true")
                        {
                            compli = "Yes";
                        }
                        else
                        {

                            compli = "No";
                        }
                        dt.Rows.Add(packCategory, Itemname, dr["HalfdayPrice"].ToString(), dr["FulldayPrice"].ToString(), online, compli, dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());

                    }

                    dt.AcceptChanges();

                }
                #endregion
                #region SpceialPromo
                if (tablename == "SpecialPriceAndPromo")
                {
                    dt = GetTableSpecialPromo();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {

                        dt.Rows.Add(dr["SpecialPriceDate"].ToString(), dr["DdrPercent"].ToString(), dr["MeetingRoomPercent"].ToString(), dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());

                    }

                    dt.AcceptChanges();

                }
                #endregion
                #region Availbility
                if (tablename == "AvailibilityOfRooms")
                {
                    dt = GetTableAvailabilityMR();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {
                        string availdate = DataRepository.AvailabilityProvider.GetById(Convert.ToInt64(dr["AvailibilityIdForBedRoom"].ToString())).AvailabilityDate.ToString();

                        string meetingroomnam = "";
                        string noofrooms = "N/A";
                        string mornStat = Enum.GetName(typeof(AvailabilityStatus), Convert.ToInt32(dr["MorningStatus"].ToString()));

                        string aftrnoonStat = Enum.GetName(typeof(AvailabilityStatus), Convert.ToInt32(dr["AfternoonStatus"].ToString()));
                        if (dr["Roomtype"].ToString() == "0")
                        {
                            meetingroomnam = DataRepository.MeetingRoomProvider.GetById(Convert.ToInt64(dr["RoomId"].ToString())).Name;
                            noofrooms = "N/A";
                        }
                        else
                        {
                            int bedid = Convert.ToInt32( DataRepository.BedRoomProvider.GetById(Convert.ToInt64(dr["RoomId"].ToString())).Types);
                            meetingroomnam = Enum.GetName(typeof(BedRoomType), Convert.ToInt32(bedid)); 
                            noofrooms = dr["NumberOfRoomsAvailable"].ToString();
                            aftrnoonStat = "";
                        }

                  

                        dt.Rows.Add(availdate, meetingroomnam, mornStat, aftrnoonStat,noofrooms, dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());

                    }

                    dt.AcceptChanges();

                }
                #endregion

                #region ClientContract
                if (tablename == "ClientContract")
                {
                    dt = GetTableGenearlInfo();
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {

                        string country = DataRepository.CountryProvider.GetById(Convert.ToInt64(dr["CountryId"])).CountryName;
                        string city = DataRepository.CityProvider.GetById(Convert.ToInt64(dr["CityId"])).City;
                        string Zone = DataRepository.ZoneProvider.GetById(Convert.ToInt64(dr["ZoneId"])).Zone;
                        string isactive = dr["IsActive"].ToString();
                        string TimeZone = dr["TimeZone"].ToString();
                        string noofbed = "N/A";
                        if (dr["IsBedroomAvailable"].ToString() == "true")
                        {
                            noofbed = dr["NumberOfBedroom"].ToString();
                        }

                        dt.Rows.Add(dr["Name"].ToString(), country, city, Zone, dr["Stars"].ToString(), dr["ZipCode"].ToString(), dr["HotelAddress"].ToString(), dr["Longitude"].ToString(), dr["Latitude"].ToString(), isactive, dr["OperatorChoice"].ToString(), dr["ContractValue"].ToString(), TimeZone, dr["CountryCodePhone"].ToString(), dr["PhoneNumber"].ToString(), dr["CountryCodeFax"].ToString(), dr["FaxNumber"].ToString(), dr["RtMFrom"].ToString(), dr["RtMTo"].ToString(), dr["RtAFrom"].ToString(), dr["RtATo"].ToString(), dr["RtFFrom"].ToString(), dr["RtFTo"].ToString(), dr["CreditCardType"].ToString(), dr["Theme"].ToString(), dr["IsBedroomAvailable"].ToString(), noofbed, dr["updatedBy"].ToString(), dr["UpdateDate"].ToString());


                    }

                    dt.AcceptChanges();

                }
                #endregion
            }
            else
            {
                dt = new DataTable();
            }
             return dt;


            }


        public DataTable Booking(string HotelID, string booktype, DateTime startdate, DateTime enddate)
         {

             DataTable dt = new DataTable();
             int totalcount = 0;
             string where = " HotelID ='" + HotelID + "'  and ModifyDate between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
             string orderby = "ModifyDate desc";
             TList<BookingLogs> dba = DataRepository.BookingLogsProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
             dt = GetTableBooking();

             DataRepository.BookingLogsProvider.DeepLoad(dba, true, DeepLoadType.IncludeChildren, typeof(Booking));

             foreach(BookingLogs bl in dba)
             {
                 if (bl.BookingIdSource.BookType.ToString() == booktype)
                 {

                     string userbname = DataRepository.UsersProvider.GetByUserId(Convert.ToInt64(bl.UserId)).FirstName;
                     dt.Rows.Add(bl.BookingId, bl.LastStatus, bl.CurrentStatus, userbname, bl.ModifyDate);
                 }
             }

             return dt;
         }
        }
    }
