﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.Web;


namespace LMMR.Business
{
   public abstract class ContactDetailBase
   {
       #region Variables and Properties
       //Declare Variable to store data
       private TList<HotelContact> _varContactDetail;
       ILog logger = log4net.LogManager.GetLogger(typeof(ContactDetailBase));
       //Declare variable to get all hotel contact details 
       public TList<HotelContact> propContactDetail
       {
            get { return _varContactDetail; }
            set { _varContactDetail = value; }

        }        

        //Declare Variable to message
        private string _varMessage;

        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        //Declare Variable to message
        private int _varMsg;

        //Property to store message
        public int propMsg
        {
            get { return _varMsg; }
            set { _varMsg = value; }
        }

        //Declare Variable to store data
        private HotelContact  _varGetHotelContract;

        //Property to store row of Entities.Hotel class
        public HotelContact propGetHotelContract
        {
            get { return _varGetHotelContract; }
            set { _varGetHotelContract = value; }
        }
       #endregion

        #region
        /// <summary>
        //This method is used for insert new ContactDetail
        /// </summary>        
        public string AddNewContactDetail(HotelContact objContact)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                if (DataRepository.HotelContactProvider.Insert(tm, objContact))
                {
                    //Audit trail maintain
                    TrailManager.LogAuditTrail<HotelContact>(tm, objContact, objContact.GetOriginalEntity(), AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.ContactDetails), objContact.TableName, Convert.ToString(objContact.HotelId));
                    //Audit trail maintain
                    propMessage = "Added successfully";
                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMessage;
        }
        #endregion


        #region
        /// <summary>
        //This method is used for update new ContactDetail
        /// </summary>           
        public string UpdateContactDetail(ref HotelContact objContact)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                //Audit trail maintain
                HotelContact old = objContact.GetOriginalEntity();
                old.Id = objContact.Id;
                TrailManager.LogAuditTrail<HotelContact>(tm, objContact, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.ContactDetails), objContact.TableName, Convert.ToString(objContact.HotelId));
                //Audit trail maintain
                if (DataRepository.HotelContactProvider.Update(tm, objContact))
                {
                    propMessage = "Updated successfully";
                    
                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMessage;
        }
        #endregion


        #region
        /// <summary>
        //This method is used for fetch the data from Contactdetails
        /// </summary>          
        public TList<HotelContact> GetContactDetails(int hotelId, string contactType)
        {
            HotelContact Hotelcon = new HotelContact();
            Hotelcon.HotelId = hotelId;
            Hotelcon.ContactType = contactType;            
            int totalout = 0;
            string whereclause = "Hotel_Id='" + hotelId + "' and " + " ContactType='" + contactType.ToString().Trim() + "' and " + "JobTitle<>''";
            TList<HotelContact> ObjContact = DataRepository.HotelContactProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out totalout);
            return ObjContact;
        }
        #endregion
        

        #region
        /// <summary>
        //This method is used to get all list of hotelcontact by id
        /// </summary>   
        public TList<HotelContact> GetHotelContactByHotelid(int hotelId)
        {
            propContactDetail = DataRepository.HotelContactProvider.GetByHotelId(hotelId);
            return propContactDetail;
        }
        #endregion

        #region
        /// <summary>
        //This function to get HotelContact by id
        /// </summary>  
        public HotelContact GetContractByID(int Id)
        {
            propGetHotelContract = DataRepository.HotelContactProvider.GetById(Id);
            return propGetHotelContract;
        }
        #endregion

      
   }
}
