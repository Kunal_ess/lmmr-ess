﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMMR.Business
{
    public abstract class RequestManagerBase
    {
        public RequestManagerBase()
        {

        }

    }
        //--Created By Gaurav Shrivastava--//
        //For creating booking including search result This'll hold the data and save its object to the database as it is.
        //1. Create the object of CreateRequest class.
        //2. Assign the values from the start of search.
        //3. Save this object in Database.with the refrence of GUID
        //4. So for searching all the time we'll Append this object and store in database(I think we'll create this object at the last step of search) After login of user.
        //5. We'll Have a option to maintain that data.
        public class CreateRequest
        {
            public string RequestID
            {
                get;
                set;
            }
            public Int64 CurrentUserId
            {
                get;
                set;
            }
            public DateTime ArivalDate
            {
                get;
                set;
            }
            public DateTime DepartureDate
            {
                get;
                set;
            }
            public int Duration
            {
                get;
                set;
            }
            public List<BuildHotelsRequest> HotelList
            {
                get;
                set;
            }
            public List<NumberOfDays> DaysList
            {
                get;
                set;
            }
            public List<BuildYourMeetingroomRequest> BuildYourMeetingroomList
            {
                get;
                set;
            }
            public Int64 PackageID
            {
                get;
                set;
            }
            public List<PackageItemDetails> PackageItemList
            {
                get;
                set;
            }
            public List<RequestExtra> ExtraList
            {
                get;
                set;
            }
            public List<RequestEquipment> EquipmentList
            {
                get;
                set;
            }
            public List<RequestOthers> OthersList
            {
                get;
                set;
            }
            public bool IsAccomodation
            {
                get;
                set;
            }
            public List<RequestAccomodation> RequestAccomodationList
            {
                get;
                set;
            }
            public string SpecialRequest
            {
                get;
                set;
            }

            public string BookType
            {
                get;
                set;
            }
            public string ChannelID
            {
                get;
                set;
            }
        }
        
        /// <summary>
        /// Build Hotel details accroding to Hotel
        /// </summary>
        public class BuildHotelsRequest
        {
            public Int64 HotelID
            {
                get;
                set;
            }
            public List<BuildMeetingRoomRequest> MeetingroomList
            {
                get;
                set;
            }
        }
        public class BuildMeetingRoomRequest
        {
            public Int64 MeetingRoomID
            {
                get;
                set;
            }
            public Int64 ConfigurationID
            {
                get;
                set;
            }
            public int Quantity
            {
                get;
                set;
            }
            public bool IsMain//If only one Meetingroom then it'll be main
            {
                get;
                set;
            }
        }
        public class NumberOfDays
        {

            public Int32 SelectedDay
            {
                get;
                set;
            }

            public Int32 SelectTime//if 0 then "fullday" if 1 then "Morning" if 2 then "Afternoon"
            {
                get;
                set;
            }

            public string StartTime
            {
                get;
                set;
            }

            public string EndTime
            {
                get;
                set;
            }
        }
        public class BuildYourMeetingroomRequest
        {
            public int SelectDay
            {
                get;
                set;
            }
            public Int64 ItemID
            {
                get;
                set;
            }
            public int Quantity
            {
                get;
                set;
            }
        }
        public class PackageItemDetails
        {
            public Int64 ItemID
            {
                get;
                set;
            }
            public int Quantity
            {
                get;
                set;
            }
        }
        public class RequestExtra
        {
            public Int64 ItemID
            {
                get;
                set;
            }
            public int Quantity
            {
                get;
                set;
            }
        }
        public class RequestEquipment
        {
            public Int64 ItemID
            {
                get;
                set;
            }
            public int Quantity
            {
                get;
                set;
            }
            public int SelectedDay
            {
                get;
                set;
            }
        }
        public class RequestOthers
        {
            public Int64 ItemID
            {
                get;
                set;
            }
            public int Quantity
            {
                get;
                set;
            }
            public int SelectedDay
            {
                get;
                set;
            }
        }
        public class RequestAccomodation
        {
            public int QuantitySingle
            {
                get;
                set;
            }
            public int QuantityDouble
            {
                get;
                set;
            }
            public DateTime Checkin
            {
                get;
                set;
            }
        }
}
