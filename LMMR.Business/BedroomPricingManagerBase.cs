﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.Web;
#endregion


namespace LMMR.Business
{
    public abstract class BedroomPricingManagerBase
    {
        #region Variables and Properties
        ILog logger = log4net.LogManager.GetLogger(typeof(BedroomPricingManagerBase));
        private TList<BedRoom> _varGetBedroomByHotelId;

        private string _varMessage;

        private BedRoom _varGetBedroomByID;


        //Property to Store message.
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        public BedRoom propGedBedroomByID
        {
            get { return _varGetBedroomByID; }
            set { _varGetBedroomByID = value; }

        }

        //Property to Store the TList of Entities.BedRoom class
        public TList<BedRoom> propGetBedroomByHotelId
        {
            get { return _varGetBedroomByHotelId; }
            set { _varGetBedroomByHotelId = value; }
        }
        #endregion
        #region Function
        public TList<BedRoom> GetBedroomByHotelID(int hotelID)
        {

            int Total = 0;
            try
            {
                string whereclause = "HotelId=" + hotelID + " and IsDeleted='" + false + "' ";
                propGetBedroomByHotelId = DataRepository.BedRoomProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return propGetBedroomByHotelId;
        }
        public string UpdateBedroomPrice(BedRoom objBedroom)
        {
            
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                BedRoom old = objBedroom.GetOriginalEntity();
                old.Id = objBedroom.Id;
                if ((old.PriceSingle == null ? 0 : old.PriceSingle) == 0 && (old.PriceDouble == null ? 0 : old.PriceDouble) == 0)
                {
                    new HotelManager().UpdateBedroomSpecialPrice(tm, objBedroom.HotelId, objBedroom.Id, Convert.ToDecimal(objBedroom.PriceSingle), Convert.ToDecimal(objBedroom.PriceDouble));
                }
                TrailManager.LogAuditTrail<BedRoom>(tm, objBedroom, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PricingBedRoom), objBedroom.TableName, Convert.ToString(old.HotelId));
                //Audit trail maintain
                if (DataRepository.BedRoomProvider.Update(tm,objBedroom))
                {
                    propMessage = "Bedroom price updated successfully";
                }
                else
                {
                    propMessage = "Please try again!";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
           return propMessage;
        }

        public BedRoom GedBedroomById(int bedroomID)
        {
            propGedBedroomByID = DataRepository.BedRoomProvider.GetById(bedroomID);
            return propGedBedroomByID;
        }
        #endregion
    }
}
