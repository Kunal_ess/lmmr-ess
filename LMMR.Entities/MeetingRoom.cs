﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'MeetingRoom' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class MeetingRoom : MeetingRoomBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="MeetingRoom"/> instance.
		///</summary>
		public MeetingRoom():base(){}	
		
		#endregion
	}
}
