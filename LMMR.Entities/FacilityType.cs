﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'FacilityType' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class FacilityType : FacilityTypeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="FacilityType"/> instance.
		///</summary>
		public FacilityType():base(){}	
		
		#endregion
	}
}
