﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'PackageByHotel_Trail' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PackageByHotelTrail : PackageByHotelTrailBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PackageByHotelTrail"/> instance.
		///</summary>
		public PackageByHotelTrail():base(){}	
		
		#endregion
	}
}
