﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'MeetingRoomConfig_Trail' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IMeetingRoomConfigTrail 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "MeetingRoomConfig_Trail"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// MeetingRoom_Id : 
		/// </summary>
		System.Int64  MeetingRoomId  { get; set; }
		
		/// <summary>
		/// RoomShapeId : 
		/// </summary>
		System.Int32  RoomShapeId  { get; set; }
		
		/// <summary>
		/// MinCapacity : 
		/// </summary>
		System.Int32  MinCapacity  { get; set; }
		
		/// <summary>
		/// MaxCapicity : 
		/// </summary>
		System.Int32  MaxCapicity  { get; set; }
		
		/// <summary>
		/// HotelID : 
		/// </summary>
		System.Int64  HotelId  { get; set; }
		
		/// <summary>
		/// UpdatedBy : 
		/// </summary>
		System.Int64  UpdatedBy  { get; set; }
		
		/// <summary>
		/// UpdateDate : 
		/// </summary>
		System.DateTime?  UpdateDate  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


