﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'CreditCardDetail' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CreditCardDetail : CreditCardDetailBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CreditCardDetail"/> instance.
		///</summary>
		public CreditCardDetail():base(){}	
		
		#endregion
	}
}
