﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'CommissionPercentage' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CommissionPercentage : CommissionPercentageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CommissionPercentage"/> instance.
		///</summary>
		public CommissionPercentage():base(){}	
		
		#endregion
	}
}
