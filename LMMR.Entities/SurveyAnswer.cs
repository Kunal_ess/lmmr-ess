﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'survey_Answer' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SurveyAnswer : SurveyAnswerBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SurveyAnswer"/> instance.
		///</summary>
		public SurveyAnswer():base(){}	
		
		#endregion
	}
}
