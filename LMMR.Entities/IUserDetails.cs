﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'UserDetails' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IUserDetails 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "UserDetails"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// UserId : 
		/// </summary>
		System.Int64?  UserId  { get; set; }
		
		/// <summary>
		/// CompanyName : 
		/// </summary>
		System.String  CompanyName  { get; set; }
		
		/// <summary>
		/// CountryId : 
		/// </summary>
		System.Int64?  CountryId  { get; set; }
		
		/// <summary>
		/// CountryCode : 
		/// </summary>
		System.String  CountryCode  { get; set; }
		
		/// <summary>
		/// CityId : 
		/// </summary>
		System.Int64?  CityId  { get; set; }
		
		/// <summary>
		/// Phone : 
		/// </summary>
		System.String  Phone  { get; set; }
		
		/// <summary>
		/// Address : 
		/// </summary>
		System.String  Address  { get; set; }
		
		/// <summary>
		/// PostalCode : 
		/// </summary>
		System.String  PostalCode  { get; set; }
		
		/// <summary>
		/// HearFromUs : 
		/// </summary>
		System.String  HearFromUs  { get; set; }
		
		/// <summary>
		/// ActivationDate : 
		/// </summary>
		System.DateTime?  ActivationDate  { get; set; }
		
		/// <summary>
		/// IATANo : 
		/// </summary>
		System.String  IataNo  { get; set; }
		
		/// <summary>
		/// VATNo : 
		/// </summary>
		System.String  VatNo  { get; set; }
		
		/// <summary>
		/// VATValue : 
		/// </summary>
		System.Int64?  VatValue  { get; set; }
		
		/// <summary>
		/// Name : 
		/// </summary>
		System.String  Name  { get; set; }
		
		/// <summary>
		/// Department : 
		/// </summary>
		System.String  Department  { get; set; }
		
		/// <summary>
		/// CommunicationEmail : 
		/// </summary>
		System.String  CommunicationEmail  { get; set; }
		
		/// <summary>
		/// InvoiceSeriesFormat : 
		/// </summary>
		System.String  InvoiceSeriesFormat  { get; set; }
		
		/// <summary>
		/// AgencyCode : Agency Code System generated value inside this field
		/// </summary>
		System.String  AgencyCode  { get; set; }
		
		/// <summary>
		/// AgencyReffId : For Agency user Add Agency Reff Code
		/// </summary>
		System.String  AgencyReffId  { get; set; }
		
		/// <summary>
		/// IsFinancialInfo : 
		/// </summary>
		System.Boolean  IsFinancialInfo  { get; set; }
		
		/// <summary>
		/// IsDeleted : 
		/// </summary>
		System.Boolean  IsDeleted  { get; set; }
		
		/// <summary>
		/// TransferUserId : 
		/// </summary>
		System.Int64?  TransferUserId  { get; set; }
		
		/// <summary>
		/// DeletedDate : 
		/// </summary>
		System.DateTime?  DeletedDate  { get; set; }
		
		/// <summary>
		/// LinkWith : 
		/// </summary>
		System.String  LinkWith  { get; set; }
		
		/// <summary>
		/// LanguageId : 
		/// </summary>
		System.Int32?  LanguageId  { get; set; }
		
		/// <summary>
		/// CityName : 
		/// </summary>
		System.String  CityName  { get; set; }
		
		/// <summary>
		/// VisitCount : 
		/// </summary>
		System.Int32?  VisitCount  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


