﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'CMS' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Cms : CmsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Cms"/> instance.
		///</summary>
		public Cms():base(){}	
		
		#endregion
	}
}
