﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ViewSPandPPercentageOfMeetingRoom' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ViewSpandPpercentageOfMeetingRoom : ViewSpandPpercentageOfMeetingRoomBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ViewSpandPpercentageOfMeetingRoom"/> instance.
		///</summary>
		public ViewSpandPpercentageOfMeetingRoom():base(){}	
		
		#endregion
	}
}
