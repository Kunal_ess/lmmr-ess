﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'MeetingRoomPictureVideo_Trail' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class MeetingRoomPictureVideoTrail : MeetingRoomPictureVideoTrailBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="MeetingRoomPictureVideoTrail"/> instance.
		///</summary>
		public MeetingRoomPictureVideoTrail():base(){}	
		
		#endregion
	}
}
