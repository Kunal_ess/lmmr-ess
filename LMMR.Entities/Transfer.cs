﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Transfer' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Transfer : TransferBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Transfer"/> instance.
		///</summary>
		public Transfer():base(){}	
		
		#endregion
	}
}
