﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'PackageByHotel' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PackageByHotel : PackageByHotelBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PackageByHotel"/> instance.
		///</summary>
		public PackageByHotel():base(){}	
		
		#endregion
	}
}
