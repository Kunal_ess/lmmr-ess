﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ServeyQuestion' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ServeyQuestion : ServeyQuestionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ServeyQuestion"/> instance.
		///</summary>
		public ServeyQuestion():base(){}	
		
		#endregion
	}
}
