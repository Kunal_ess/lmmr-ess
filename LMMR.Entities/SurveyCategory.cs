﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'survey_category' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SurveyCategory : SurveyCategoryBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SurveyCategory"/> instance.
		///</summary>
		public SurveyCategory():base(){}	
		
		#endregion
	}
}
