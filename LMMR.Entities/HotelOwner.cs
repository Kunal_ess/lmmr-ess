﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'HotelOwner' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class HotelOwner : HotelOwnerBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="HotelOwner"/> instance.
		///</summary>
		public HotelOwner():base(){}	
		
		#endregion
	}
}
