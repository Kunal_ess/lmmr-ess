﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'FinancialInfo' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class FinancialInfo : FinancialInfoBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="FinancialInfo"/> instance.
		///</summary>
		public FinancialInfo():base(){}	
		
		#endregion
	}
}
