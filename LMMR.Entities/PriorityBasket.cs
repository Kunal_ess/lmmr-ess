﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'PriorityBasket' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PriorityBasket : PriorityBasketBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PriorityBasket"/> instance.
		///</summary>
		public PriorityBasket():base(){}	
		
		#endregion
	}
}
