﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'HotelFacilities_trail' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IHotelFacilitiesTrail 
	{
		/// <summary>			
		/// ID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "HotelFacilities_trail"</remarks>
		System.Int64 Id { get; set; }
				
		/// <summary>
		/// keep a copy of the original so it can be used for editable primary keys.
		/// </summary>
		System.Int64 OriginalId { get; set; }
			
		
		
		/// <summary>
		/// HotelFacilitiesID : 
		/// </summary>
		System.Int64  HotelFacilitiesId  { get; set; }
		
		/// <summary>
		/// Hotel_Id : 
		/// </summary>
		System.Int64  HotelId  { get; set; }
		
		/// <summary>
		/// FacilitiesId : 
		/// </summary>
		System.Int64  FacilitiesId  { get; set; }
		
		/// <summary>
		/// UpdateDate : 
		/// </summary>
		System.DateTime?  UpdateDate  { get; set; }
		
		/// <summary>
		/// UpdatedBy : 
		/// </summary>
		System.Int32?  UpdatedBy  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


