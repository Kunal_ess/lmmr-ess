﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'Booking' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IBooking 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "Booking"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// HotelId : 
		/// </summary>
		System.Int64  HotelId  { get; set; }
		
		/// <summary>
		/// MainMeetingRoomId : 
		/// </summary>
		System.Int64  MainMeetingRoomId  { get; set; }
		
		/// <summary>
		/// BookingDate : 
		/// </summary>
		System.DateTime?  BookingDate  { get; set; }
		
		/// <summary>
		/// ArrivalDate : 
		/// </summary>
		System.DateTime?  ArrivalDate  { get; set; }
		
		/// <summary>
		/// DepartureDate : 
		/// </summary>
		System.DateTime?  DepartureDate  { get; set; }
		
		/// <summary>
		/// IsBookedByAgencyUser : 
		/// </summary>
		System.Boolean?  IsBookedByAgencyUser  { get; set; }
		
		/// <summary>
		/// AgencyUserId : 
		/// </summary>
		System.Int64?  AgencyUserId  { get; set; }
		
		/// <summary>
		/// AgencyClientId : 
		/// </summary>
		System.Int64?  AgencyClientId  { get; set; }
		
		/// <summary>
		/// CreatorId : 
		/// </summary>
		System.Int64  CreatorId  { get; set; }
		
		/// <summary>
		/// IsPackageSelected : 
		/// </summary>
		System.Boolean  IsPackageSelected  { get; set; }
		
		/// <summary>
		/// Accomodation : 
		/// </summary>
		System.Int32?  Accomodation  { get; set; }
		
		/// <summary>
		/// IsBedroom : 
		/// </summary>
		System.Boolean  IsBedroom  { get; set; }
		
		/// <summary>
		/// SpecialRequest : 
		/// </summary>
		System.String  SpecialRequest  { get; set; }
		
		/// <summary>
		/// IsCancled : 
		/// </summary>
		System.Boolean?  IsCancled  { get; set; }
		
		/// <summary>
		/// IsComissionDone : 
		/// </summary>
		System.Boolean?  IsComissionDone  { get; set; }
		
		/// <summary>
		/// ComissionSubmitDate : 
		/// </summary>
		System.DateTime?  ComissionSubmitDate  { get; set; }
		
		/// <summary>
		/// RevenueReason : 
		/// </summary>
		System.String  RevenueReason  { get; set; }
		
		/// <summary>
		/// RevenueAmount : 
		/// </summary>
		System.Decimal?  RevenueAmount  { get; set; }
		
		/// <summary>
		/// BookType : if 0 then Book otherwise request
		/// </summary>
		System.Int32?  BookType  { get; set; }
		
		/// <summary>
		/// IsUserBookingProcessDone : If paid then Yes(1) else No(0)
		/// </summary>
		System.Boolean  IsUserBookingProcessDone  { get; set; }
		
		/// <summary>
		/// RequestStatus : 
		/// </summary>
		System.Int32?  RequestStatus  { get; set; }
		
		/// <summary>
		/// IsFrozen : 
		/// </summary>
		System.Boolean  IsFrozen  { get; set; }
		
		/// <summary>
		/// FrozenDate : 
		/// </summary>
		System.DateTime?  FrozenDate  { get; set; }
		
		/// <summary>
		/// FinalTotalPrice : 
		/// </summary>
		System.Decimal?  FinalTotalPrice  { get; set; }
		
		/// <summary>
		/// BedRoomTotalPrice : 
		/// </summary>
		System.Decimal?  BedRoomTotalPrice  { get; set; }
		
		/// <summary>
		/// MeetingRoomTotalPrice : 
		/// </summary>
		System.Decimal?  MeetingRoomTotalPrice  { get; set; }
		
		/// <summary>
		/// Duration : 
		/// </summary>
		System.Int64?  Duration  { get; set; }
		
		/// <summary>
		/// IsSurveyDone : 
		/// </summary>
		System.Boolean  IsSurveyDone  { get; set; }
		
		/// <summary>
		/// SurveyDate : 
		/// </summary>
		System.DateTime?  SurveyDate  { get; set; }
		
		/// <summary>
		/// CurrencyID : 
		/// </summary>
		System.Int64?  CurrencyId  { get; set; }
		
		/// <summary>
		/// Channel : 
		/// </summary>
		System.String  Channel  { get; set; }
		
		/// <summary>
		/// SupportingDocNetto : 
		/// </summary>
		System.String  SupportingDocNetto  { get; set; }
		
		/// <summary>
		/// InvoiceId : 
		/// </summary>
		System.Int64?  InvoiceId  { get; set; }
		
		/// <summary>
		/// BookingXML : 
		/// </summary>
		string  BookingXml  { get; set; }
		
		/// <summary>
		/// ConfirmRevenueAmount : 
		/// </summary>
		System.Decimal?  ConfirmRevenueAmount  { get; set; }
		
		/// <summary>
		/// ChannelBy : 
		/// </summary>
		System.String  ChannelBy  { get; set; }
		
		/// <summary>
		/// ChannelID : 
		/// </summary>
		System.String  ChannelId  { get; set; }
		
		/// <summary>
		/// RequestCollectionID : 
		/// </summary>
		System.String  RequestCollectionId  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _transferNewBookingIdGetByOldBookingId
		/// </summary>	
		TList<Transfer> TransferCollectionGetByOldBookingId {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _bookedMeetingRoomBookingId
		/// </summary>	
		TList<BookedMeetingRoom> BookedMeetingRoomCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _buildPackageConfigureBookingId
		/// </summary>	
		TList<BuildPackageConfigure> BuildPackageConfigureCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _serveyResponseBookingId
		/// </summary>	
		TList<ServeyResponse> ServeyResponseCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _buildMeetingConfigureBookingId
		/// </summary>	
		TList<BuildMeetingConfigure> BuildMeetingConfigureCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _bookingLogsBookingId
		/// </summary>	
		TList<BookingLogs> BookingLogsCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _bookedBedRoomBookingId
		/// </summary>	
		TList<BookedBedRoom> BookedBedRoomCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _invoiceBookedId
		/// </summary>	
		TList<Invoice> InvoiceCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _transferNewBookingIdGetByNewBookingId
		/// </summary>	
		TList<Transfer> TransferCollectionGetByNewBookingId {  get;  set;}	

		#endregion Data Properties

	}
}


