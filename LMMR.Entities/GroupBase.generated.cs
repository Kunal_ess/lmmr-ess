﻿
/*
	File generated by NetTiers templates [www.nettiers.com]
	Important: Do not modify this file. Edit the file Group.cs instead.
*/

#region using directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using LMMR.Entities.Validation;
#endregion

namespace LMMR.Entities
{
	///<summary>
	/// An object representation of the 'Group' table. [No description found the database]	
	///</summary>
	[Serializable]
	[DataObject, CLSCompliant(true)]
	public abstract partial class GroupBase : EntityBase, IGroup, IEntityId<GroupKey>, System.IComparable, System.ICloneable, ICloneableEx, IEditableObject, IComponent, INotifyPropertyChanged
	{		
		#region Variable Declarations
		
		/// <summary>
		///  Hold the inner data of the entity.
		/// </summary>
		private GroupEntityData entityData;
		
		/// <summary>
		/// 	Hold the original data of the entity, as loaded from the repository.
		/// </summary>
		private GroupEntityData _originalData;
		
		/// <summary>
		/// 	Hold a backup of the inner data of the entity.
		/// </summary>
		private GroupEntityData backupData; 
		
		/// <summary>
		/// 	Key used if Tracking is Enabled for the <see cref="EntityLocator" />.
		/// </summary>
		private string entityTrackingKey;
		
		/// <summary>
		/// 	Hold the parent TList&lt;entity&gt; in which this entity maybe contained.
		/// </summary>
		/// <remark>Mostly used for databinding</remark>
		[NonSerialized]
		private TList<Group> parentCollection;
		
		private bool inTxn = false;
		
		/// <summary>
		/// Occurs when a value is being changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event GroupEventHandler ColumnChanging;		
		
		/// <summary>
		/// Occurs after a value has been changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event GroupEventHandler ColumnChanged;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="GroupBase"/> instance.
		///</summary>
		public GroupBase()
		{
			this.entityData = new GroupEntityData();
			this.backupData = null;
		}		
		
		///<summary>
		/// Creates a new <see cref="GroupBase"/> instance.
		///</summary>
		///<param name="_groupCode"></param>
		///<param name="_groupName"></param>
		///<param name="_jsCode"></param>
		public GroupBase(System.Int64 _groupCode, System.String _groupName, System.String _jsCode)
		{
			this.entityData = new GroupEntityData();
			this.backupData = null;

			this.GroupCode = _groupCode;
			this.GroupName = _groupName;
			this.JsCode = _jsCode;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="Group"/> instance.
		///</summary>
		///<param name="_groupCode"></param>
		///<param name="_groupName"></param>
		///<param name="_jsCode"></param>
		public static Group CreateGroup(System.Int64 _groupCode, System.String _groupName, System.String _jsCode)
		{
			Group newGroup = new Group();
			newGroup.GroupCode = _groupCode;
			newGroup.GroupName = _groupName;
			newGroup.JsCode = _jsCode;
			return newGroup;
		}
				
		#endregion Constructors
			
		#region Properties	
		
		#region Data Properties		
		/// <summary>
		/// 	Gets or sets the Id property. 
		///		
		/// </summary>
		/// <value>This type is bigint.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		[Required(ErrorMessage = "Id is required")]




		[ReadOnlyAttribute(false)/*, XmlIgnoreAttribute()*/, DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(true, true, false)]
		public virtual System.Int64 Id
		{
			get
			{
				return this.entityData.Id; 
			}
			
			set
			{
				if (this.entityData.Id == value)
					return;
				
                OnPropertyChanging("Id");                    
				OnColumnChanging(GroupColumn.Id, this.entityData.Id);
				this.entityData.Id = value;
				this.EntityId.Id = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(GroupColumn.Id, this.entityData.Id);
				OnPropertyChanged("Id");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the GroupCode property. 
		///		
		/// </summary>
		/// <value>This type is bigint.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		[Required(ErrorMessage = "GroupCode is required")]




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.Int64 GroupCode
		{
			get
			{
				return this.entityData.GroupCode; 
			}
			
			set
			{
				if (this.entityData.GroupCode == value)
					return;
				
                OnPropertyChanging("GroupCode");                    
				OnColumnChanging(GroupColumn.GroupCode, this.entityData.GroupCode);
				this.entityData.GroupCode = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(GroupColumn.GroupCode, this.entityData.GroupCode);
				OnPropertyChanged("GroupCode");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the GroupName property. 
		///		
		/// </summary>
		/// <value>This type is nvarchar.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		/// <exception cref="ArgumentNullException">If you attempt to set to null.</exception>
		
		[Required(ErrorMessage = "GroupName is required")]




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false, 100)]
		public virtual System.String GroupName
		{
			get
			{
				return this.entityData.GroupName; 
			}
			
			set
			{
				if (this.entityData.GroupName == value)
					return;
				
                OnPropertyChanging("GroupName");                    
				OnColumnChanging(GroupColumn.GroupName, this.entityData.GroupName);
				this.entityData.GroupName = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(GroupColumn.GroupName, this.entityData.GroupName);
				OnPropertyChanged("GroupName");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the JsCode property. 
		///		
		/// </summary>
		/// <value>This type is text.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		/// <exception cref="ArgumentNullException">If you attempt to set to null.</exception>
		
		[Required(ErrorMessage = "JsCode is required")]




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.String JsCode
		{
			get
			{
				return this.entityData.JsCode; 
			}
			
			set
			{
				if (this.entityData.JsCode == value)
					return;
				
                OnPropertyChanging("JsCode");                    
				OnColumnChanging(GroupColumn.JsCode, this.entityData.JsCode);
				this.entityData.JsCode = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(GroupColumn.JsCode, this.entityData.JsCode);
				OnPropertyChanged("JsCode");
			}
		}
		
		#endregion Data Properties		

		#region Source Foreign Key Property
				
		#endregion
		
		#region Children Collections
	
		/// <summary>
		///	Holds a collection of Hotel objects
		///	which are related to this object through the relation FK_Hotel_Group
		/// </summary>	
		[System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
		public virtual TList<Hotel> HotelCollection
		{
			get { return entityData.HotelCollection; }
			set { entityData.HotelCollection = value; }	
		}
		#endregion Children Collections
		
		#endregion
		#region Validation
		
		/// <summary>
		/// Assigns validation rules to this object based on model definition.
		/// </summary>
		/// <remarks>This method overrides the base class to add schema related validation.</remarks>
		protected override void AddValidationRules()
		{
			//Validation rules based on database schema.
			ValidationRules.AddRule( CommonRules.NotNull,
				new ValidationRuleArgs("GroupName", "Group Name"));
			ValidationRules.AddRule( CommonRules.StringMaxLength, 
				new CommonRules.MaxLengthRuleArgs("GroupName", "Group Name", 100));
			ValidationRules.AddRule( CommonRules.NotNull,
				new ValidationRuleArgs("JsCode", "Js Code"));
		}
   		#endregion
		
		#region Table Meta Data
		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string TableName
		{
			get { return "Group"; }
		}
		
		/// <summary>
		///		The name of the underlying database table's columns.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string[] TableColumns
		{
			get
			{
				return new string[] {"Id", "GroupCode", "GroupName", "JSCode"};
			}
		}
		#endregion 
		
		#region IEditableObject
		
		#region  CancelAddNew Event
		/// <summary>
        /// The delegate for the CancelAddNew event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		public delegate void CancelAddNewEventHandler(object sender, EventArgs e);
    
    	/// <summary>
		/// The CancelAddNew event.
		/// </summary>
		[field:NonSerialized]
		public event CancelAddNewEventHandler CancelAddNew ;

		/// <summary>
        /// Called when [cancel add new].
        /// </summary>
        public void OnCancelAddNew()
        {    
			if (!SuppressEntityEvents)
			{
	            CancelAddNewEventHandler handler = CancelAddNew ;
            	if (handler != null)
	            {    
    	            handler(this, EventArgs.Empty) ;
        	    }
	        }
        }
		#endregion 
		
		/// <summary>
		/// Begins an edit on an object.
		/// </summary>
		void IEditableObject.BeginEdit() 
	    {
	        //Console.WriteLine("Start BeginEdit");
	        if (!inTxn) 
	        {
	            this.backupData = this.entityData.Clone() as GroupEntityData;
	            inTxn = true;
	            //Console.WriteLine("BeginEdit");
	        }
	        //Console.WriteLine("End BeginEdit");
	    }
	
		/// <summary>
		/// Discards changes since the last <c>BeginEdit</c> call.
		/// </summary>
	    void IEditableObject.CancelEdit() 
	    {
	        //Console.WriteLine("Start CancelEdit");
	        if (this.inTxn) 
	        {
	            this.entityData = this.backupData;
	            this.backupData = null;
				this.inTxn = false;

				if (this.bindingIsNew)
	        	//if (this.EntityState == EntityState.Added)
	        	{
					if (this.parentCollection != null)
						this.parentCollection.Remove( (Group) this ) ;
				}	            
	        }
	        //Console.WriteLine("End CancelEdit");
	    }
	
		/// <summary>
		/// Pushes changes since the last <c>BeginEdit</c> or <c>IBindingList.AddNew</c> call into the underlying object.
		/// </summary>
	    void IEditableObject.EndEdit() 
	    {
	        //Console.WriteLine("Start EndEdit" + this.custData.id + this.custData.lastName);
	        if (this.inTxn) 
	        {
	            this.backupData = null;
				if (this.IsDirty) 
				{
					if (this.bindingIsNew) {
						this.EntityState = EntityState.Added;
						this.bindingIsNew = false ;
					}
					else
						if (this.EntityState == EntityState.Unchanged) 
							this.EntityState = EntityState.Changed ;
				}

				this.bindingIsNew = false ;
	            this.inTxn = false;	            
	        }
	        //Console.WriteLine("End EndEdit");
	    }
	    
	    /// <summary>
        /// Gets or sets the parent collection of this current entity, if available.
        /// </summary>
        /// <value>The parent collection.</value>
	    [XmlIgnore]
		[Browsable(false)]
	    public override object ParentCollection
	    {
	        get 
	        {
	            return this.parentCollection;
	        }
	        set 
	        {
	            this.parentCollection = value as TList<Group>;
	        }
	    }
	    
	    /// <summary>
        /// Called when the entity is changed.
        /// </summary>
	    private void OnEntityChanged() 
	    {
	        if (!SuppressEntityEvents && !inTxn && this.parentCollection != null) 
	        {
	            this.parentCollection.EntityChanged(this as Group);
	        }
	    }


		#endregion
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed Group Entity 
		///</summary>
		protected virtual Group Copy(IDictionary existingCopies)
		{
			if (existingCopies == null)
			{
				// This is the root of the tree to be copied!
				existingCopies = new Hashtable();
			}

			//shallow copy entity
			Group copy = new Group();
			existingCopies.Add(this, copy);
			copy.SuppressEntityEvents = true;
				copy.Id = this.Id;
				copy.GroupCode = this.GroupCode;
				copy.GroupName = this.GroupName;
				copy.JsCode = this.JsCode;
			
		
			//deep copy nested objects
			copy.HotelCollection = (TList<Hotel>) MakeCopyOf(this.HotelCollection, existingCopies); 
			copy.EntityState = this.EntityState;
			copy.SuppressEntityEvents = false;
			return copy;
		}		
		
		
		
		///<summary>
		///  Returns a Typed Group Entity 
		///</summary>
		public virtual Group Copy()
		{
			return this.Copy(null);	
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone()
		{
			return this.Copy(null);
		}
		
		///<summary>
		/// ICloneableEx.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone(IDictionary existingCopies)
		{
			return this.Copy(existingCopies);
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x == null)
				return null;
				
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x, IDictionary existingCopies)
		{
			if (x == null)
				return null;
			
			if (x is ICloneableEx)
			{
				// Return a deep copy of the object
				return ((ICloneableEx)x).Clone(existingCopies);
			}
			else if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable or IClonableEx Interface.");
		}
		
		
		///<summary>
		///  Returns a Typed Group Entity which is a deep copy of the current entity.
		///</summary>
		public virtual Group DeepCopy()
		{
			return EntityHelper.Clone<Group>(this as Group);	
		}
		#endregion
		
		#region Methods	
			
		///<summary>
		/// Revert all changes and restore original values.
		///</summary>
		public override void CancelChanges()
		{
			IEditableObject obj = (IEditableObject) this;
			obj.CancelEdit();

			this.entityData = null;
			if (this._originalData != null)
			{
				this.entityData = this._originalData.Clone() as GroupEntityData;
			}
			else
			{
				//Since this had no _originalData, then just reset the entityData with a new one.  entityData cannot be null.
				this.entityData = new GroupEntityData();
			}
		}	
		
		/// <summary>
		/// Accepts the changes made to this object.
		/// </summary>
		/// <remarks>
		/// After calling this method, properties: IsDirty, IsNew are false. IsDeleted flag remains unchanged as it is handled by the parent List.
		/// </remarks>
		public override void AcceptChanges()
		{
			base.AcceptChanges();

			// we keep of the original version of the data
			this._originalData = null;
			this._originalData = this.entityData.Clone() as GroupEntityData;
		}
		
		#region Comparision with original data
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsPropertyChanged(GroupColumn column)
		{
			switch(column)
			{
					case GroupColumn.Id:
					return entityData.Id != _originalData.Id;
					case GroupColumn.GroupCode:
					return entityData.GroupCode != _originalData.GroupCode;
					case GroupColumn.GroupName:
					return entityData.GroupName != _originalData.GroupName;
					case GroupColumn.JsCode:
					return entityData.JsCode != _originalData.JsCode;
			
				default:
					return false;
			}
		}
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="columnName">The column name.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsPropertyChanged(string columnName)
		{
			return 	IsPropertyChanged(EntityHelper.GetEnumValue< GroupColumn >(columnName));
		}
		
		/// <summary>
		/// Determines whether the data has changed from original.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if data has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool HasDataChanged()
		{
			bool result = false;
			result = result || entityData.Id != _originalData.Id;
			result = result || entityData.GroupCode != _originalData.GroupCode;
			result = result || entityData.GroupName != _originalData.GroupName;
			result = result || entityData.JsCode != _originalData.JsCode;
			return result;
		}	
		
		///<summary>
		///  Returns a Group Entity with the original data.
		///</summary>
		public Group GetOriginalEntity()
		{
			if (_originalData != null)
				return CreateGroup(
				_originalData.GroupCode,
				_originalData.GroupName,
				_originalData.JsCode
				);
				
			return (Group)this.Clone();
		}
		#endregion
	
	#region Value Semantics Instance Equality
        ///<summary>
        /// Returns a value indicating whether this instance is equal to a specified object using value semantics.
        ///</summary>
        ///<param name="Object1">An object to compare to this instance.</param>
        ///<returns>true if Object1 is a <see cref="GroupBase"/> and has the same value as this instance; otherwise, false.</returns>
        public override bool Equals(object Object1)
        {
			// Cast exception if Object1 is null or DbNull
			if (Object1 != null && Object1 != DBNull.Value && Object1 is GroupBase)
				return ValueEquals(this, (GroupBase)Object1);
			else
				return false;
        }

        /// <summary>
		/// Serves as a hash function for a particular type, suitable for use in hashing algorithms and data structures like a hash table.
        /// Provides a hash function that is appropriate for <see cref="GroupBase"/> class 
        /// and that ensures a better distribution in the hash table
        /// </summary>
        /// <returns>number (hash code) that corresponds to the value of an object</returns>
        public override int GetHashCode()
        {
			return this.Id.GetHashCode() ^ 
					this.GroupCode.GetHashCode() ^ 
					this.GroupName.GetHashCode() ^ 
					this.JsCode.GetHashCode();
        }
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object using value semantics.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="GroupBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(GroupBase toObject)
		{
			if (toObject == null)
				return false;
			return ValueEquals(this, toObject);
		}
		#endregion
		
		///<summary>
		/// Determines whether the specified <see cref="GroupBase"/> instances are considered equal using value semantics.
		///</summary>
		///<param name="Object1">The first <see cref="GroupBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="GroupBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool ValueEquals(GroupBase Object1, GroupBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;
				
			bool equal = true;
			if (Object1.Id != Object2.Id)
				equal = false;
			if (Object1.GroupCode != Object2.GroupCode)
				equal = false;
			if (Object1.GroupName != Object2.GroupName)
				equal = false;
			if (Object1.JsCode != Object2.JsCode)
				equal = false;
					
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
			//return this. GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]) .CompareTo(((GroupBase)obj).GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]));
		}
		
		/*
		// static method to get a Comparer object
        public static GroupComparer GetComparer()
        {
            return new GroupComparer();
        }
        */

        // Comparer delegates back to Group
        // Employee uses the integer's default
        // CompareTo method
        /*
        public int CompareTo(Item rhs)
        {
            return this.Id.CompareTo(rhs.Id);
        }
        */

/*
        // Special implementation to be called by custom comparer
        public int CompareTo(Group rhs, GroupColumn which)
        {
            switch (which)
            {
            	
            	
            	case GroupColumn.Id:
            		return this.Id.CompareTo(rhs.Id);
            		
            		                 
            	
            	
            	case GroupColumn.GroupCode:
            		return this.GroupCode.CompareTo(rhs.GroupCode);
            		
            		                 
            	
            	
            	case GroupColumn.GroupName:
            		return this.GroupName.CompareTo(rhs.GroupName);
            		
            		                 
            	
            	
            	case GroupColumn.JsCode:
            		return this.JsCode.CompareTo(rhs.JsCode);
            		
            		                 
            }
            return 0;
        }
        */
	
		#endregion
		
		#region IComponent Members
		
		private ISite _site = null;

		/// <summary>
		/// Gets or Sets the site where this data is located.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public ISite Site
		{
			get{ return this._site; }
			set{ this._site = value; }
		}

		#endregion

		#region IDisposable Members
		
		/// <summary>
		/// Notify those that care when we dispose.
		/// </summary>
		[field:NonSerialized]
		public event System.EventHandler Disposed;

		/// <summary>
		/// Clean up. Nothing here though.
		/// </summary>
		public virtual void Dispose()
		{
			this.parentCollection = null;
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		/// <summary>
		/// Clean up.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				EventHandler handler = Disposed;
				if (handler != null)
					handler(this, EventArgs.Empty);
			}
		}
		
		#endregion
				
		#region IEntityKey<GroupKey> Members
		
		// member variable for the EntityId property
		private GroupKey _entityId;

		/// <summary>
		/// Gets or sets the EntityId property.
		/// </summary>
		[XmlIgnore]
		public virtual GroupKey EntityId
		{
			get
			{
				if ( _entityId == null )
				{
					_entityId = new GroupKey(this);
				}

				return _entityId;
			}
			set
			{
				if ( value != null )
				{
					value.Entity = this;
				}
				
				_entityId = value;
			}
		}
		
		#endregion
		
		#region EntityState
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false) , XmlIgnoreAttribute()]
		public override EntityState EntityState 
		{ 
			get{ return entityData.EntityState;	 } 
			set{ entityData.EntityState = value; } 
		}
		#endregion 
		
		#region EntityTrackingKey
		///<summary>
		/// Provides the tracking key for the <see cref="EntityLocator"/>
		///</summary>
		[XmlIgnore]
		public override string EntityTrackingKey
		{
			get
			{
				if(entityTrackingKey == null)
					entityTrackingKey = new System.Text.StringBuilder("Group")
					.Append("|").Append( this.Id.ToString()).ToString();
				return entityTrackingKey;
			}
			set
		    {
		        if (value != null)
                    entityTrackingKey = value;
		    }
		}
		#endregion 
		
		#region ToString Method
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{5}{4}- Id: {0}{4}- GroupCode: {1}{4}- GroupName: {2}{4}- JsCode: {3}{4}{6}", 
				this.Id,
				this.GroupCode,
				this.GroupName,
				this.JsCode,
				System.Environment.NewLine, 
				this.GetType(),
				this.Error.Length == 0 ? string.Empty : string.Format("- Error: {0}\n",this.Error));
		}
		
		#endregion ToString Method
		
		#region Inner data class
		
	/// <summary>
	///		The data structure representation of the 'Group' table.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Serializable]
	internal protected class GroupEntityData : ICloneable, ICloneableEx
	{
		#region Variable Declarations
		private EntityState currentEntityState = EntityState.Added;
		
		#region Primary key(s)
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "Group"</remarks>
		public System.Int64 Id;
			
		#endregion
		
		#region Non Primary key(s)
		
		/// <summary>
		/// GroupCode : 
		/// </summary>
		public System.Int64 GroupCode = (long)0;
		
		/// <summary>
		/// GroupName : 
		/// </summary>
		public System.String GroupName = string.Empty;
		
		/// <summary>
		/// JSCode : 
		/// </summary>
		public System.String JsCode = string.Empty;
		#endregion
			
		#region Source Foreign Key Property
				
		#endregion
        
		#endregion Variable Declarations

		#region Data Properties

		#region HotelCollection
		
		private TList<Hotel> _hotelGroupId;
		
		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _hotelGroupId
		/// </summary>
		
		public TList<Hotel> HotelCollection
		{
			get
			{
				if (_hotelGroupId == null)
				{
				_hotelGroupId = new TList<Hotel>();
				}
	
				return _hotelGroupId;
			}
			set { _hotelGroupId = value; }
		}
		
		#endregion

		#endregion Data Properties
		#region Clone Method

		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public Object Clone()
		{
			GroupEntityData _tmp = new GroupEntityData();
						
			_tmp.Id = this.Id;
			
			_tmp.GroupCode = this.GroupCode;
			_tmp.GroupName = this.GroupName;
			_tmp.JsCode = this.JsCode;
			
			#region Source Parent Composite Entities
			#endregion
		
			#region Child Collections
			//deep copy nested objects
			if (this._hotelGroupId != null)
				_tmp.HotelCollection = (TList<Hotel>) MakeCopyOf(this.HotelCollection); 
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone(IDictionary existingCopies)
		{
			if (existingCopies == null)
				existingCopies = new Hashtable();
				
			GroupEntityData _tmp = new GroupEntityData();
						
			_tmp.Id = this.Id;
			
			_tmp.GroupCode = this.GroupCode;
			_tmp.GroupName = this.GroupName;
			_tmp.JsCode = this.JsCode;
			
			#region Source Parent Composite Entities
			#endregion
		
			#region Child Collections
			//deep copy nested objects
			_tmp.HotelCollection = (TList<Hotel>) MakeCopyOf(this.HotelCollection, existingCopies); 
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		#endregion Clone Method
		
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public EntityState	EntityState
		{
			get { return currentEntityState;  }
			set { currentEntityState = value; }
		}
	
	}//End struct

		#endregion
		
				
		
		#region Events trigger
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="GroupColumn"/> which has raised the event.</param>
		public virtual void OnColumnChanging(GroupColumn column)
		{
			OnColumnChanging(column, null);
			return;
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="GroupColumn"/> which has raised the event.</param>
		public virtual void OnColumnChanged(GroupColumn column)
		{
			OnColumnChanged(column, null);
			return;
		} 
		
		
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="GroupColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public virtual void OnColumnChanging(GroupColumn column, object value)
		{
			if(IsEntityTracked && EntityState != EntityState.Added && !EntityManager.TrackChangedEntities)
                EntityManager.StopTracking(entityTrackingKey);
                
			if (!SuppressEntityEvents)
			{
				GroupEventHandler handler = ColumnChanging;
				if(handler != null)
				{
					handler(this, new GroupEventArgs(column, value));
				}
			}
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="GroupColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public virtual void OnColumnChanged(GroupColumn column, object value)
		{
			if (!SuppressEntityEvents)
			{
				GroupEventHandler handler = ColumnChanged;
				if(handler != null)
				{
					handler(this, new GroupEventArgs(column, value));
				}
			
				// warn the parent list that i have changed
				OnEntityChanged();
			}
		} 
		#endregion
			
	} // End Class
	
	
	#region GroupEventArgs class
	/// <summary>
	/// Provides data for the ColumnChanging and ColumnChanged events.
	/// </summary>
	/// <remarks>
	/// The ColumnChanging and ColumnChanged events occur when a change is made to the value 
	/// of a property of a <see cref="Group"/> object.
	/// </remarks>
	public class GroupEventArgs : System.EventArgs
	{
		private GroupColumn column;
		private object value;
		
		///<summary>
		/// Initalizes a new Instance of the GroupEventArgs class.
		///</summary>
		public GroupEventArgs(GroupColumn column)
		{
			this.column = column;
		}
		
		///<summary>
		/// Initalizes a new Instance of the GroupEventArgs class.
		///</summary>
		public GroupEventArgs(GroupColumn column, object value)
		{
			this.column = column;
			this.value = value;
		}
		
		///<summary>
		/// The GroupColumn that was modified, which has raised the event.
		///</summary>
		///<value cref="GroupColumn" />
		public GroupColumn Column { get { return this.column; } }
		
		/// <summary>
        /// Gets the current value of the column.
        /// </summary>
        /// <value>The current value of the column.</value>
		public object Value{ get { return this.value; } }

	}
	#endregion
	
	///<summary>
	/// Define a delegate for all Group related events.
	///</summary>
	public delegate void GroupEventHandler(object sender, GroupEventArgs e);
	
	#region GroupComparer
		
	/// <summary>
	///	Strongly Typed IComparer
	/// </summary>
	public class GroupComparer : System.Collections.Generic.IComparer<Group>
	{
		GroupColumn whichComparison;
		
		/// <summary>
        /// Initializes a new instance of the <see cref="T:GroupComparer"/> class.
        /// </summary>
		public GroupComparer()
        {            
        }               
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:GroupComparer"/> class.
        /// </summary>
        /// <param name="column">The column to sort on.</param>
        public GroupComparer(GroupColumn column)
        {
            this.whichComparison = column;
        }

		/// <summary>
        /// Determines whether the specified <see cref="Group"/> instances are considered equal.
        /// </summary>
        /// <param name="a">The first <see cref="Group"/> to compare.</param>
        /// <param name="b">The second <c>Group</c> to compare.</param>
        /// <returns>true if objA is the same instance as objB or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public bool Equals(Group a, Group b)
        {
            return this.Compare(a, b) == 0;
        }

		/// <summary>
        /// Gets the hash code of the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public int GetHashCode(Group entity)
        {
            return entity.GetHashCode();
        }

        /// <summary>
        /// Performs a case-insensitive comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        /// <returns></returns>
        public int Compare(Group a, Group b)
        {
        	EntityPropertyComparer entityPropertyComparer = new EntityPropertyComparer(this.whichComparison.ToString());
        	return entityPropertyComparer.Compare(a, b);
        }

		/// <summary>
        /// Gets or sets the column that will be used for comparison.
        /// </summary>
        /// <value>The comparison column.</value>
        public GroupColumn WhichComparison
        {
            get { return this.whichComparison; }
            set { this.whichComparison = value; }
        }
	}
	
	#endregion
	
	#region GroupKey Class

	/// <summary>
	/// Wraps the unique identifier values for the <see cref="Group"/> object.
	/// </summary>
	[Serializable]
	[CLSCompliant(true)]
	public class GroupKey : EntityKeyBase
	{
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the GroupKey class.
		/// </summary>
		public GroupKey()
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the GroupKey class.
		/// </summary>
		public GroupKey(GroupBase entity)
		{
			this.Entity = entity;

			#region Init Properties

			if ( entity != null )
			{
				this.Id = entity.Id;
			}

			#endregion
		}
		
		/// <summary>
		/// Initializes a new instance of the GroupKey class.
		/// </summary>
		public GroupKey(System.Int64 _id)
		{
			#region Init Properties

			this.Id = _id;

			#endregion
		}
		
		#endregion Constructors

		#region Properties
		
		// member variable for the Entity property
		private GroupBase _entity;
		
		/// <summary>
		/// Gets or sets the Entity property.
		/// </summary>
		public GroupBase Entity
		{
			get { return _entity; }
			set { _entity = value; }
		}
		
		// member variable for the Id property
		private System.Int64 _id;
		
		/// <summary>
		/// Gets or sets the Id property.
		/// </summary>
		public System.Int64 Id
		{
			get { return _id; }
			set
			{
				if ( this.Entity != null )
					this.Entity.Id = value;
				
				_id = value;
			}
		}
		
		#endregion

		#region Methods
		
		/// <summary>
		/// Reads values from the supplied <see cref="IDictionary"/> object into
		/// properties of the current object.
		/// </summary>
		/// <param name="values">An <see cref="IDictionary"/> instance that contains
		/// the key/value pairs to be used as property values.</param>
		public override void Load(IDictionary values)
		{
			#region Init Properties

			if ( values != null )
			{
				Id = ( values["Id"] != null ) ? (System.Int64) EntityUtil.ChangeType(values["Id"], typeof(System.Int64)) : (long)0;
			}

			#endregion
		}

		/// <summary>
		/// Creates a new <see cref="IDictionary"/> object and populates it
		/// with the property values of the current object.
		/// </summary>
		/// <returns>A collection of name/value pairs.</returns>
		public override IDictionary ToDictionary()
		{
			IDictionary values = new Hashtable();

			#region Init Dictionary

			values.Add("Id", Id);

			#endregion Init Dictionary

			return values;
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return String.Format("Id: {0}{1}",
								Id,
								System.Environment.NewLine);
		}

		#endregion Methods
	}
	
	#endregion	

	#region GroupColumn Enum
	
	/// <summary>
	/// Enumerate the Group columns.
	/// </summary>
	[Serializable]
	public enum GroupColumn : int
	{
		/// <summary>
		/// Id : 
		/// </summary>
		[EnumTextValue("Id")]
		[ColumnEnum("Id", typeof(System.Int64), System.Data.DbType.Int64, true, true, false)]
		Id = 1,
		/// <summary>
		/// GroupCode : 
		/// </summary>
		[EnumTextValue("GroupCode")]
		[ColumnEnum("GroupCode", typeof(System.Int64), System.Data.DbType.Int64, false, false, false)]
		GroupCode = 2,
		/// <summary>
		/// GroupName : 
		/// </summary>
		[EnumTextValue("GroupName")]
		[ColumnEnum("GroupName", typeof(System.String), System.Data.DbType.String, false, false, false, 100)]
		GroupName = 3,
		/// <summary>
		/// JsCode : 
		/// </summary>
		[EnumTextValue("JSCode")]
		[ColumnEnum("JSCode", typeof(System.String), System.Data.DbType.String, false, false, false)]
		JsCode = 4
	}//End enum

	#endregion GroupColumn Enum

} // end namespace
