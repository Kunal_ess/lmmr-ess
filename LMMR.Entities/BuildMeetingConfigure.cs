﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BuildMeetingConfigure' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BuildMeetingConfigure : BuildMeetingConfigureBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BuildMeetingConfigure"/> instance.
		///</summary>
		public BuildMeetingConfigure():base(){}	
		
		#endregion
	}
}
