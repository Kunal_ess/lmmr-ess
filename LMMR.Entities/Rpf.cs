﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'RPF' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Rpf : RpfBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Rpf"/> instance.
		///</summary>
		public Rpf():base(){}	
		
		#endregion
	}
}
