﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'AgentUser' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class AgentUser : AgentUserBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="AgentUser"/> instance.
		///</summary>
		public AgentUser():base(){}	
		
		#endregion
	}
}
