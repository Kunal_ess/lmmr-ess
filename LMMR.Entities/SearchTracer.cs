﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'SearchTracer' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SearchTracer : SearchTracerBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SearchTracer"/> instance.
		///</summary>
		public SearchTracer():base(){}	
		
		#endregion
	}
}
