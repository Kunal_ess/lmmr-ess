﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'PackageMasterDescription' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PackageMasterDescription : PackageMasterDescriptionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PackageMasterDescription"/> instance.
		///</summary>
		public PackageMasterDescription():base(){}	
		
		#endregion
	}
}
