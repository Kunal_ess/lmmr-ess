﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Survey' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Survey : SurveyBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Survey"/> instance.
		///</summary>
		public Survey():base(){}	
		
		#endregion
	}
}
