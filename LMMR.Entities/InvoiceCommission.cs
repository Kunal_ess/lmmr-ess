﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'InvoiceCommission' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class InvoiceCommission : InvoiceCommissionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="InvoiceCommission"/> instance.
		///</summary>
		public InvoiceCommission():base(){}	
		
		#endregion
	}
}
