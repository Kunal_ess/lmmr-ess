﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'CountryLanguage' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CountryLanguage : CountryLanguageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CountryLanguage"/> instance.
		///</summary>
		public CountryLanguage():base(){}	
		
		#endregion
	}
}
