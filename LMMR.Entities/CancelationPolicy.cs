﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'CancelationPolicy' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CancelationPolicy : CancelationPolicyBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CancelationPolicy"/> instance.
		///</summary>
		public CancelationPolicy():base(){}	
		
		#endregion
	}
}
