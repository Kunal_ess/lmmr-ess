﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'WhiteLabelMappingWithHotel' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class WhiteLabelMappingWithHotel : WhiteLabelMappingWithHotelBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="WhiteLabelMappingWithHotel"/> instance.
		///</summary>
		public WhiteLabelMappingWithHotel():base(){}	
		
		#endregion
	}
}
