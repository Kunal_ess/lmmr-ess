﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'OtherItemsPricingbyHotel' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class OtherItemsPricingbyHotel : OtherItemsPricingbyHotelBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="OtherItemsPricingbyHotel"/> instance.
		///</summary>
		public OtherItemsPricingbyHotel():base(){}	
		
		#endregion
	}
}
