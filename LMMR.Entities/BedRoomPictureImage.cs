﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BedRoomPictureImage' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BedRoomPictureImage : BedRoomPictureImageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BedRoomPictureImage"/> instance.
		///</summary>
		public BedRoomPictureImage():base(){}	
		
		#endregion
	}
}
