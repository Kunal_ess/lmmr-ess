﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'HotelFacilities' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class HotelFacilities : HotelFacilitiesBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="HotelFacilities"/> instance.
		///</summary>
		public HotelFacilities():base(){}	
		
		#endregion
	}
}
