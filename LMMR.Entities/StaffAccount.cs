﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'StaffAccount' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class StaffAccount : StaffAccountBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="StaffAccount"/> instance.
		///</summary>
		public StaffAccount():base(){}	
		
		#endregion
	}
}
