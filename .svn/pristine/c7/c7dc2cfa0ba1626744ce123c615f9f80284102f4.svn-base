﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="HotelFacilitiesTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class HotelFacilitiesTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.HotelFacilitiesTrail, LMMR.Entities.HotelFacilitiesTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.HotelFacilitiesTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.HotelFacilitiesTrail Get(TransactionManager transactionManager, LMMR.Entities.HotelFacilitiesTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_HotelFacilities_trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelFacilitiesTrail"/> class.</returns>
		public LMMR.Entities.HotelFacilitiesTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelFacilities_trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelFacilitiesTrail"/> class.</returns>
		public LMMR.Entities.HotelFacilitiesTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelFacilities_trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelFacilitiesTrail"/> class.</returns>
		public LMMR.Entities.HotelFacilitiesTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelFacilities_trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelFacilitiesTrail"/> class.</returns>
		public LMMR.Entities.HotelFacilitiesTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelFacilities_trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelFacilitiesTrail"/> class.</returns>
		public LMMR.Entities.HotelFacilitiesTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelFacilities_trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelFacilitiesTrail"/> class.</returns>
		public abstract LMMR.Entities.HotelFacilitiesTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;HotelFacilitiesTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;HotelFacilitiesTrail&gt;"/></returns>
		public static TList<HotelFacilitiesTrail> Fill(IDataReader reader, TList<HotelFacilitiesTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.HotelFacilitiesTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("HotelFacilitiesTrail")
					.Append("|").Append((System.Int64)reader[((int)HotelFacilitiesTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<HotelFacilitiesTrail>(
					key.ToString(), // EntityTrackingKey
					"HotelFacilitiesTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.HotelFacilitiesTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)HotelFacilitiesTrailColumn.Id - 1)];
					c.OriginalId = c.Id;
					c.HotelFacilitiesId = (System.Int64)reader[((int)HotelFacilitiesTrailColumn.HotelFacilitiesId - 1)];
					c.HotelId = (System.Int64)reader[((int)HotelFacilitiesTrailColumn.HotelId - 1)];
					c.FacilitiesId = (System.Int64)reader[((int)HotelFacilitiesTrailColumn.FacilitiesId - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)HotelFacilitiesTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)HotelFacilitiesTrailColumn.UpdateDate - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)HotelFacilitiesTrailColumn.UpdatedBy - 1)))?null:(System.Int32?)reader[((int)HotelFacilitiesTrailColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HotelFacilitiesTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelFacilitiesTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.HotelFacilitiesTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)HotelFacilitiesTrailColumn.Id - 1)];
			entity.OriginalId = (System.Int64)reader["ID"];
			entity.HotelFacilitiesId = (System.Int64)reader[((int)HotelFacilitiesTrailColumn.HotelFacilitiesId - 1)];
			entity.HotelId = (System.Int64)reader[((int)HotelFacilitiesTrailColumn.HotelId - 1)];
			entity.FacilitiesId = (System.Int64)reader[((int)HotelFacilitiesTrailColumn.FacilitiesId - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)HotelFacilitiesTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)HotelFacilitiesTrailColumn.UpdateDate - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)HotelFacilitiesTrailColumn.UpdatedBy - 1)))?null:(System.Int32?)reader[((int)HotelFacilitiesTrailColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HotelFacilitiesTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelFacilitiesTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.HotelFacilitiesTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["ID"];
			entity.OriginalId = (System.Int64)dataRow["ID"];
			entity.HotelFacilitiesId = (System.Int64)dataRow["HotelFacilitiesID"];
			entity.HotelId = (System.Int64)dataRow["Hotel_Id"];
			entity.FacilitiesId = (System.Int64)dataRow["FacilitiesId"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int32?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelFacilitiesTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.HotelFacilitiesTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.HotelFacilitiesTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.HotelFacilitiesTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.HotelFacilitiesTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.HotelFacilitiesTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.HotelFacilitiesTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region HotelFacilitiesTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.HotelFacilitiesTrail</c>
	///</summary>
	public enum HotelFacilitiesTrailChildEntityTypes
	{
	}
	
	#endregion HotelFacilitiesTrailChildEntityTypes
	
	#region HotelFacilitiesTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;HotelFacilitiesTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelFacilitiesTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelFacilitiesTrailFilterBuilder : SqlFilterBuilder<HotelFacilitiesTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailFilterBuilder class.
		/// </summary>
		public HotelFacilitiesTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelFacilitiesTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelFacilitiesTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelFacilitiesTrailFilterBuilder
	
	#region HotelFacilitiesTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;HotelFacilitiesTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelFacilitiesTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelFacilitiesTrailParameterBuilder : ParameterizedSqlFilterBuilder<HotelFacilitiesTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailParameterBuilder class.
		/// </summary>
		public HotelFacilitiesTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelFacilitiesTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelFacilitiesTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelFacilitiesTrailParameterBuilder
	
	#region HotelFacilitiesTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;HotelFacilitiesTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelFacilitiesTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class HotelFacilitiesTrailSortBuilder : SqlSortBuilder<HotelFacilitiesTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailSqlSortBuilder class.
		/// </summary>
		public HotelFacilitiesTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion HotelFacilitiesTrailSortBuilder
	
} // end namespace
