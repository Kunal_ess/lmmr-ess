﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Xml;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

public partial class ListBookings : BasePage
{
    #region Variables

    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", userId = "0";
    
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public string UserCurrency
    {
        get { return "1"; }
        set { ViewState["UserCurrency"] = "1"; }
    }
    public string HotelCurrency
    {
        get { return "1"; }
        set { ViewState["HotelCurrency"] = "1"; }
    }
    public decimal CurrencyConvert
    {
        get { return 1; }
        set { ViewState["CurrencyConvert"] = 1; }
    }
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomID
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomConfigureID
    {
        get;
        set;
    }
    public int CurrentPackageType
    {
        get;
        set;
    }
    public List<ManagePackageItem> CurrentPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> objHp
    {
        get;
        set;
    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session check           
            divbookingdetails.Visible = false;
            divprintpdfllink.Visible = false;

            if (Session["CurrentRestUser"] != null)
            {
                Users objUsers = (Users)Session["CurrentRestUser"];
                lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                objUsers.LastLogin = DateTime.Now;
                lstLoginTime.Text = DateTime.Now.ToLongDateString();
                AfterLogin(objUsers.UserId, Convert.ToInt32(objUsers.Usertype));
            }
            else
            {
                Response.Redirect("~/Default.aspx", false);
                return;
            }
            //userId = "493";//tempporarily static to be commented
            userId = Convert.ToString(Session["CurrentRestUserID"].ToString());
            //Session check
            modalcheckcomm.Hide();

            if (!IsPostBack)
            {
                BindURLs();
                btnSubmit.ValidationGroup = "popup";
                ViewState["Where"] = string.Empty;
                ViewState["Order"] = string.Empty;
                ViewState["SearchAlpha"] = "all";
                FilterGridWithHeader(string.Empty, string.Empty, string.Empty);
                //divlinkpro.Visible = true;
                bindgrid();
                bindAllDDLs();
            }
            else
            {
                ApplyPaging();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    //----------------------------new(24/5/12)------------------------------------

    /// <summary>
    /// This method displays the user profile area.
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="userType"></param>
    public void AfterLogin(long userID, int userType)
    {
        beforeLogin.Style.Add("display", "none");
        afterLogin.Style.Add("display", "block");
        Session["registerType"] = userType;
    }

    public void BindURLs()
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            joinToday.HRef = SiteRootPath + "join-today/" + l.Name.ToLower();
        }
        else
        {
            joinToday.HRef = SiteRootPath + "join-today/english";
        }
    }

    /// <summary>
    /// This method binds the city dropdownlist.
    /// </summary>
    public void bindcityDDL()
    {
        string whereclause = "CreatorId in (" + userId + ") and booktype = 0"; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList cityDDL = header.FindControl("cityDDL") as DropDownList;
        cityDDL.DataTextField = "City";
        cityDDL.DataValueField = "Id";
        cityDDL.DataSource = objViewBooking_Hotel.getAllCities(vlistreq);
        cityDDL.DataBind();
        cityDDL.Items.Insert(0,new System.Web.UI.WebControls.ListItem("--Select City--", "0"));
    }

    /// <summary>
    /// This method binds the hotel DDL.
    /// </summary>
    public void bindHotelDDL()
    {
        string whereclause = "CreatorId in (" + userId + ") and booktype = 0"; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList hotelDDL = header.FindControl("hotelDDL") as DropDownList;
        hotelDDL.DataTextField = "Name";
        hotelDDL.DataValueField = "Id";
        hotelDDL.DataSource = objViewBooking_Hotel.getAllHotels(vlistreq);
        hotelDDL.DataBind();
        hotelDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Hotel--", "0"));
    }

    /// <summary>
    /// This method binds the meetingRoom DDL.
    /// </summary>
    public void bindMeetingRoomDDL()
    {
        string whereclause = "CreatorId in (" + userId + ") and booktype = 0"; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.Name);
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList meetingRoomDDL = header.FindControl("meetingRoomDDL") as DropDownList;
        meetingRoomDDL.DataTextField = "Name";
        meetingRoomDDL.DataValueField = "MainMeetingRoomId";
        meetingRoomDDL.DataSource = vlistreq;
        meetingRoomDDL.DataBind();
        meetingRoomDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Meeting Room--", "0"));
    }

    /// <summary>
    /// This method binds bookingDate DDL.
    /// </summary>
    public void bindbookingDateDDL()
    {
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList bookingDateDDL = header.FindControl("bookingDateDDL") as DropDownList;
        string whereclause = "CreatorId in (" + userId + ") and booktype = 0"; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.BookingDate);
        List<string> dateList = new List<string>();
        foreach(Viewbookingrequest temp in vlistreq)
        {
            string dt = temp.BookingDate.ToString();
            string[] str = dt.Split(new char[]{' '});
            string[] formatted = Regex.Split(str[0], "/");
            string newStr = "";
            newStr += formatted[1] + "/";
            newStr += formatted[0] + "/";
            newStr += formatted[2];
            if(!dateList.Contains(newStr))
            dateList.Add(newStr);
        }
        bookingDateDDL.DataSource = dateList;
        //bookingDateDDL.DataTextField = "Display";
        //bookingDateDDL.DataValueField = "Display";
        //bookingDateDDL.DataSource = vlistreq.Select(a => new { Display = Convert.ToDateTime(a.BookingDate).ToString("dd/M/yyyy") });
        bookingDateDDL.DataBind();
        bookingDateDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Booking Date--", "0"));
    }

    /// <summary>
    /// This method binds the arrival date DropDownList.
    /// </summary>
    public void bindArrivalDateDDL()
    {
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList arrivalDateDDL = header.FindControl("arrivalDateDDL") as DropDownList;
        string whereclause = "CreatorId in (" + userId + ") and booktype = 0"; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.ArrivalDate);
        List<string> dateList = new List<string>();
        foreach (Viewbookingrequest temp in vlistreq)
        {
            string dt = temp.ArrivalDate.ToString();
            string[] str = dt.Split(new char[] { ' ' });
            string[] formatted = Regex.Split(str[0], "/");
            string newStr = "";
            newStr += formatted[1] + "/";
            newStr += formatted[0] + "/";
            newStr += formatted[2];
            if (!dateList.Contains(newStr))
                dateList.Add(newStr);
        }
        arrivalDateDDL.DataSource = dateList;
        //bookingDateDDL.DataTextField = "Display";
        //bookingDateDDL.DataValueField = "Display";
        //bookingDateDDL.DataSource = vlistreq.Select(a => new { Display = Convert.ToDateTime(a.BookingDate).ToString("dd/M/yyyy") });
        arrivalDateDDL.DataBind();
        arrivalDateDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Arrival Date--", "0"));
    }

    /// <summary>
    /// This method binds value DDl.
    /// </summary>
    public void bindvalueDDL()
    {
        string whereclause = "CreatorId in (" + userId + ") and booktype = 0"; // book type for request and booking
        string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby).FindAllDistinct(ViewbookingrequestColumn.FinalTotalPrice);
        DataTable dt = new DataTable();
        dt.Columns.Add("PriceForDDLText", typeof(string));
        dt.Columns.Add("PriceForDDLValue", typeof(double));
        foreach (Viewbookingrequest temp in vlistreq)
        {
            DataRow dr = dt.NewRow();
            dr["PriceForDDLText"] = String.Format("{0:#,###}", temp.FinalTotalPrice);
            dr["PriceForDDLValue"] = temp.FinalTotalPrice;
            dt.Rows.Add(dr);
        }

        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList valueDDL = header.FindControl("valueDDL") as DropDownList;
        valueDDL.DataTextField = "PriceForDDLText";
        valueDDL.DataValueField = "PriceForDDLValue";
        valueDDL.DataSource = dt; //.Select(a => new { FinalTotalPrice = String.Format("{0:#,###}",a.FinalTotalPrice) });
        valueDDL.DataBind();
        valueDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Value--", "0"));
    }

    /// <summary>
    /// This method gets the city name according to the hotelID.
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getCityName(string hotelId)
    {
        return objViewBooking_Hotel.getCityName(hotelId);
    }

    /// <summary>
    /// This method gets the hotel name on the basis of hotelID.
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getHotelName(string hotelId)
    {
        return objViewBooking_Hotel.GetbyHotelID(Convert.ToInt32(hotelId)).Name;
    }

    /// <summary>
    /// This method gets the cancellation limit for a particular hotel on the basis of its ID.
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getCancellationLimit(int hotelId)
    {
        long policyId = objViewBooking_Hotel.getSpecialPolicyId(hotelId);
        int cancellationLimit;
        if (policyId != 0)
        {
            cancellationLimit = Convert.ToInt32(objViewBooking_Hotel.getPolicy(policyId).CancelationLimit);
        }
        else
        {
            long countryId = objViewBooking_Hotel.getHotelCountryId(hotelId);
            cancellationLimit = Convert.ToInt32(objViewBooking_Hotel.getDefaultPolicy(countryId).CancelationLimit);
        }
        return cancellationLimit.ToString();
    }

    /// <summary>
    /// This is the event handler for value DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void valueDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList valueDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("valueDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype = 0" + " and FinalTotalPrice = "+ valueDDL.SelectedItem.Value; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();


            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for bookingDate DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void bookingDateDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList bookingDateDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("bookingDateDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype = 0"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            VList<Viewbookingrequest> filtered = new VList<Viewbookingrequest>();
            foreach (Viewbookingrequest temp in vlistreq)
            {
                string dt = temp.BookingDate.ToString();
                string[] str = dt.Split(new char[]{' '});
                string[] formatted = Regex.Split(str[0], "/");
                string newStr = "";
                newStr += formatted[1] + "/";
                newStr += formatted[0] + "/";
                newStr += formatted[2];
                if (bookingDateDDL.SelectedValue == newStr)
                {
                    filtered.Add(temp);
                }
            }
            grdViewBooking.DataSource = filtered;
            Session["latestGridState"] = filtered;
            grdViewBooking.DataBind();
            bindAllDDLs();


            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for meetingRoom DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void meetingRoomDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList meetingRoomDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("meetingRoomDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype = 0" + " and Name = '" + meetingRoomDDL.SelectedItem.Text + "'"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();

            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for chkInvoiceSent checkbox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkInvoiceSent_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gr in grdViewBooking.Rows)
        {
            CheckBox chkInvoiceSent = (CheckBox)gr.FindControl("chkInvoiceSent");
            if (chkInvoiceSent.Checked == true)
            {
                long bookingId = Convert.ToInt64(chkInvoiceSent.ToolTip);
                objViewBooking_Hotel.CancelBooking(bookingId);
            }
        }
        //CheckBox chkInvoiceSent=(CheckBox)grdViewBooking.ro
        //if()
    }

    /// <summary>
    /// This is the event handler for hotel DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hotelDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList hotelDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("hotelDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype = 0" + " and HotelId = " + hotelDDL.SelectedItem.Value; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();

            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        
        
    }

    /// <summary>
    /// This is the event handler for city DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void cityDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList cityDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("cityDDL");
            string whereclause = "CreatorId in (" + userId + ") and BookType = 0"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            VList<Viewbookingrequest> filteredSet = new VList<Viewbookingrequest>();
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            foreach (Viewbookingrequest temp in vlistreq)
            {
                long cityId = objViewBooking_Hotel.getCityId(temp.HotelId);
                if (cityId == Convert.ToInt64(cityDDL.SelectedItem.Value))
                {
                    filteredSet.Add(temp);
                }
            }
            grdViewBooking.DataSource = filteredSet;
            Session["latestGridState"] = filteredSet;
            grdViewBooking.DataBind();
            bindAllDDLs();
            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for arrivalDate DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void arrivalDateDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList arrivalDateDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("arrivalDateDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype = 0"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            VList<Viewbookingrequest> filtered = new VList<Viewbookingrequest>();
            foreach (Viewbookingrequest temp in vlistreq)
            {
                string dt = temp.ArrivalDate.ToString();
                string[] str = dt.Split(new char[] { ' ' });
                string[] formatted = Regex.Split(str[0], "/");
                string newStr = "";
                newStr += formatted[1] + "/";
                newStr += formatted[0] + "/";
                newStr += formatted[2];
                if (arrivalDateDDL.SelectedValue == newStr)
                {
                    filtered.Add(temp);
                }
            }
            grdViewBooking.DataSource = filtered;
            Session["latestGridState"] = filtered;
            grdViewBooking.DataBind();
            bindAllDDLs();


            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This method handles the paging functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            string whereclaus = String.Empty;
            string orderby = String.Empty;

            HtmlAnchor anc = (sender as HtmlAnchor);
            var d = anc.InnerHtml;
            //ViewState["SearchAlpha"] = d;
            if (d.Trim() == "all")
            {
                whereclaus = ViewbookingrequestColumn.HotelName + " LIKE '%' ";
                ViewState["SearchAlpha"] = d;
            }
            else
            {
                whereclaus = ViewbookingrequestColumn.HotelName + " LIKE '" + d + "%' ";
                ViewState["SearchAlpha"] = d;
            }
            ViewState["Where2"] = whereclaus;
            ViewState["CurrentPage"] = 0;
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            if (ViewState["Where"] != null)
            {
                FilterGridWithHeader(ViewState["Where"].ToString(), ViewState["Order"].ToString(), string.Empty);
            }
            else
            {
                FilterGridWithHeader(ViewState["Where2"].ToString(), ViewState["Order"].ToString(), string.Empty);
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void grvClientContract_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewBooking.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This method filters the grid.
    /// </summary>
    /// <param name="wherec"></param>
    /// <param name="order"></param>
    /// <param name="clientNameStarts"></param>
    public void FilterGridWithHeader(string wherec, string order, string clientNameStarts)
    {
        try
        {
            string whereclause;
            whereclause = "CreatorId in (" + userId + ") and BookType = 0";
            if (wherec != "")
            {
                whereclause = "CreatorId in (" + userId + ") and BookType = 0" + " and " + wherec; // book type for request and booking
            }
            if (ViewState["Where2"] != null)
            {
                if (!string.IsNullOrEmpty(ViewState["Where2"].ToString().Trim()))
                {
                    if (!string.IsNullOrEmpty(whereclause.Trim()))
                    {
                        whereclause += " and ";
                    }
                    whereclause += ViewState["Where2"];
                }
            }
            string orderby = order;
            orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            grdViewBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();

            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }
            bindAllDDLs();
            ApplyPaging();
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This method calls the binding methods of all the DDL's in the header.
    /// </summary>
    public void bindAllDDLs()
    {
        bindcityDDL();
        bindHotelDDL();
        bindMeetingRoomDDL();
        bindbookingDateDDL();
        bindArrivalDateDDL();
        bindvalueDDL();
    }

    /// <summary>
    /// This is the event handler for the searching button's Clicked event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtGo_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["SearchAlpha"] = "all";
            string whereclause = "CreatorId in (" + userId + ") and BookType = 0";
            if (txtBookingID.Text != "")
            {
                whereclause += " and Id = " + txtBookingID.Text.Trim(); // book type for request and booking
            }
            if (txtFrom.Text != "" && txtTo.Text != "")
            {
                //whereclause += " and ArrivalDate between convert(DATETIME, '" + txtFrom.Text + " and DepartureDate= " + txtTo.Text;
                // whereclause += " and ArrivalDate between convert(DATETIME, '" + txtFrom.Text + "' and convert(DATETIME , '" + txtTo.Text + "'";
                whereclause += " and ArrivalDate between convert(DATETIME, '" + txtFrom.Text + "',103) and convert(DATETIME , '" + txtTo.Text + "',103) ";
            }
            else
            {
                if (!String.IsNullOrEmpty(txtFrom.Text))
                {
                    whereclause += "and ArrivalDate >= convert(DATETIME, '" + txtFrom.Text + "',103)";
                }
                if (!String.IsNullOrEmpty(txtTo.Text))
                {
                    whereclause += "and ArrivalDate <= convert(DATETIME, '" + txtTo.Text + "',103)";
                }
            }
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();
            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for the lbtCancel checkbox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtCancel_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        objViewBooking_Hotel.CancelBooking(Convert.ToInt64(lb.ToolTip));
        bindgrid();

    }

    /// <summary>
    /// This is the event handler for the hypManageProfile hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        //Response.Redirect("Registration.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "manage-profile/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "manage-profile/english");
        }
    }
    
    /// <summary>
    /// This is the event handler for the hypChangepassword hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        //Response.Redirect("ChangePassword.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "change-password/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "change-password/english");
        }
    }

    /// <summary>
    /// This is the event handler for the hypListBookings hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hypListBookings_Click(object sender, EventArgs e)
    {
        //Response.Redirect("ListBookings.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-bookings/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-bookings/english");
        }
    }

    /// <summary>
    /// This is the event handler for the hypListRequests hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hypListRequests_Click(object sender, EventArgs e)
    {
        //Response.Redirect("ListRequests.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-requests/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-requests/english");
        }
    }


   // -------------------------------------------------------------------


    protected void bindgrid()
    {
        try
        {
            string whereclause = "CreatorId in (" + userId + ") and BookType = 0"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();

            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    #region View Booking Details
    /// <summary>
    /// Method to view details of Booking would be seen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblRefNo_Click(object sender, EventArgs e)
    {
        //Changed The Code For Using Usercontrol:Pratik//
        try
        {
            divprintpdfllink.Visible = true;

            LinkButton lnk = (LinkButton)sender;
            //-- viewstate object
            ViewState["BookingID"] = lnk.Text;
            Session["ListBookings"] = "ListBookings";
            bookingDetails.BindBooking(Convert.ToInt32(lnk.Text));
            divbookingdetails.Visible = true;
            bookingDetails.Visible = true;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }
    #endregion

    #region Bookingbind


    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }

    //public void BindBooking()
    //{
    //    lblFromDt.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
    //    lblToDate.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
    //    lblBookedDays.Text = objBooking.Duration == 1 ? objBooking.Duration + " Day" : objBooking.Duration + " Days";

    //    // BindHotelDetails(objBooking.HotelID);

    //    rptMeetingRoom.DataSource = objBooking.MeetingroomList;
    //    rptMeetingRoom.DataBind();
    //    BindAccomodation();
    //    lblSpecialRequest.Text = objBooking.SpecialRequest;
    //    Calculate(objBooking);
    //    lblNetTotal.Text = String.Format("{0:#,###}", Math.Round((objBooking.TotalBookingPrice - VatCalculation.Sum(a => a.CalculatedPrice)) * CurrencyConvert, 2));
    //    rptVatList.DataSource = VatCalculation;
    //    rptVatList.DataBind();
    //    lblFinalTotal.Text = String.Format("{0:#,###}", Math.Round(objBooking.TotalBookingPrice * CurrencyConvert, 2));
    //    tAndCPopUp.OnClientClick = "return open_win2('Policy.aspx?hid=" + objBooking.HotelID + "')";
        
    //}

   
    //public void BindAccomodation()
    //{
    //    if (objBooking.ManageAccomodationLst.Count > 0)
    //    {
    //        pnlAccomodation.Visible = false;
    //        rptAccomodation.DataSource = objBooking.ManageAccomodationLst;
    //        rptAccomodation.DataBind();
    //        lblAccomodationPrice.Text = String.Format("{0:#,###}", Math.Round(objBooking.AccomodationPriceTotal * CurrencyConvert, 2));
    //    }
    //    else
    //    {
    //        pnlAccomodation.Visible = false;
    //    }
    //}
    protected void rptExtra_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFrom = (Label)e.Item.FindControl("lblFrom");
            Label lblTo = (Label)e.Item.FindControl("lblTo");
            Label lblQuntity = (Label)e.Item.FindControl("lblQuntity");
            ManageExtras mngExt = e.Item.DataItem as ManageExtras;
            if (mngExt != null)
            {
                PackageItems p = objHp.Where(a => a.Id == mngExt.ItemId).FirstOrDefault();
                if (p != null)
                {
                    lblPackageItem.Text = p.ItemName;
                    lblFrom.Text = mngExt.FromTime;
                    lblTo.Text = mngExt.ToTime;
                    lblQuntity.Text = mngExt.Quantity.ToString();
                }
            }
            else
            {
                lblQuntity.Text = "0";
            }
        }
    }

    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFromTime = (Label)e.Item.FindControl("lblFromTime");
            Label lblToTime = (Label)e.Item.FindControl("lblToTime");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                ManagePackageItem mpi = CurrentPackageItem.Where(a => a.ItemId == p.Id).FirstOrDefault();
                if (mpi != null)
                {
                    lblPackageItem.Text = p.ItemName;
                    lblFromTime.Text = mpi.FromTime;
                    lblQuantity.Text += Convert.ToString(mpi.Quantity);
                    if (p.ItemName.ToLower().Contains("coffee break(s) morning and/or afternoon"))//|| p.ItemName.ToLower().Contains("morning / afternoon") || p.ItemName.ToLower().Contains("morning/ afternoon") || p.ItemName.ToLower().Contains("morning /afternoon") || p.ItemName.ToLower().Contains("morning / afternoon coffee break")
                    {
                        lblToTime.Text += mpi.ToTime;
                        lblToTime.Visible = true;
                    }
                    else
                    {
                        lblToTime.Visible = false;
                        lblQuantity.Visible = false;
                    }
                }
            }
        }
    }
    protected void rptVatList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblVatPrice = (Label)e.Item.FindControl("lblVatPrice");
            VatCollection v = e.Item.DataItem as VatCollection;
            if (v != null)
            {
                lblVatPrice.Text = String.Format("{0:#,###}", Math.Round(v.CalculatedPrice * CurrencyConvert, 2));
            }
        }
    }

    protected void rptAccomodation_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Accomodation objAccomodation = e.Item.DataItem as Accomodation;

            Label lblBedroomName = (Label)e.Item.FindControl("lblBedroomName");
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            Label lblBedroomPrice = (Label)e.Item.FindControl("lblBedroomPrice");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalBedroomPrice = (Label)e.Item.FindControl("lblTotalBedroomPrice");
            if (objAccomodation != null)
            {
                //BedRoom objBedroom = objHotel.GetBedRoomDetailsByBedroomID(objAccomodation.BedroomId);
                //lblBedroomName.Text = Enum.GetName(typeof(BedRoomType), objBedroom.Types);
                //lblBedroomType.Text = objAccomodation.RoomType;
                //lblBedroomPrice.Text = Math.Round(Convert.ToDecimal(objAccomodation.RoomType == "Single" ? objBedroom.PriceSingle : objBedroom.PriceDouble) * CurrencyConvert, 2).ToString();
                //lblQuantity.Text = objAccomodation.Quantity.ToString();
                //lblTotalBedroomPrice.Text = Math.Round(Convert.ToDecimal(objAccomodation.RoomType == "Single" ? objBedroom.PriceSingle : objBedroom.PriceDouble) * CurrencyConvert * objAccomodation.Quantity, 2).ToString();
                //Repeater rptRoomManage = (Repeater)e.Item.FindControl("rptRoomManage");
                //rptRoomManage.DataSource = objAccomodation.RoomManageLst;
                //rptRoomManage.DataBind();
            }
        }
    }
    protected void rptRoomManage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            RoomManage objRoomManage = e.Item.DataItem as RoomManage;
            Label lblRoomIndex = (Label)e.Item.FindControl("lblRoomIndex");
            Label lblAllotmantName = (Label)e.Item.FindControl("lblAllotmantName");
            Label lblCheckIn = (Label)e.Item.FindControl("lblCheckIn");
            Label lblCheckOut = (Label)e.Item.FindControl("lblCheckOut");
            Label lblNote = (Label)e.Item.FindControl("lblNote");
            lblRoomIndex.Text = (e.Item.ItemIndex + 1).ToString();
            lblAllotmantName.Text = objRoomManage.RoomAssignedTo;
            lblCheckIn.Text = objRoomManage.CheckInTime;
            lblCheckOut.Text = objRoomManage.checkoutTime;
            lblNote.Text = objRoomManage.Notes;
        }
    }
    protected void rptMeetingRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMR objBookedMr = e.Item.DataItem as BookedMR;
            CurrentMeetingRoomID = objBookedMr.MRId;
            CurrentMeetingRoomConfigureID = objBookedMr.MrConfigId;

            Repeater rptMeetingRoomConfigure = (Repeater)e.Item.FindControl("rptMeetingRoomConfigure");
            rptMeetingRoomConfigure.DataSource = objBookedMr.MrDetails;
            rptMeetingRoomConfigure.DataBind();
        }
    }


    protected void rptMeetingRoomConfigure_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMrConfig objBookedMrConfig = e.Item.DataItem as BookedMrConfig;
            //Bind Meetingroom//
            MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(CurrentMeetingRoomID);
            MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
            MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == CurrentMeetingRoomConfigureID).FirstOrDefault();

            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblMeetingRoomType = (Label)e.Item.FindControl("lblMeetingRoomType");
            Label lblMinMaxCapacity = (Label)e.Item.FindControl("lblMinMaxCapacity");
            Label lblMeetingRoomActualPrice = (Label)e.Item.FindControl("lblMeetingRoomActualPrice");
            Label lblTotalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalMeetingRoomPrice");
            Label lblTotalFinalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalFinalMeetingRoomPrice");
            lblMeetingRoomName.Text = objMeetingroom.Name;
            lblMeetingRoomType.Text = Enum.GetName(typeof(RoomShape), objMrConfig.RoomShapeId);
            lblMinMaxCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
            lblMeetingRoomActualPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice * CurrencyConvert, 2).ToString();
            lblTotalFinalMeetingRoomPrice.Text = String.Format("{0:#,###}", Math.Round(objBookedMrConfig.MeetingroomPrice * CurrencyConvert, 2));
            decimal intDiscountMR = objBookedMrConfig.MeetingroomDiscount;
            lblTotalMeetingRoomPrice.Text = String.Format("{0:#,###}",Math.Round(objBookedMrConfig.MeetingroomPrice * CurrencyConvert, 2));            

            //Bind Other details//
            Label lblSelectedDay = (Label)e.Item.FindControl("lblSelectedDay");
            Label lblStart = (Label)e.Item.FindControl("lblStart");
            Label lblEnd = (Label)e.Item.FindControl("lblEnd");
            Label lblNumberOfParticepant = (Label)e.Item.FindControl("lblNumberOfParticepant");
            lblSelectedDay.Text = objBookedMrConfig.SelectedDay.ToString();
            //lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            lblStart.Text = objBookedMrConfig.FromTime;
            lblEnd.Text = objBookedMrConfig.ToTime;
            lblNumberOfParticepant.Text = objBookedMrConfig.NoOfParticepant.ToString();
            Panel pnlNotIsBreakdown = (Panel)e.Item.FindControl("pnlNotIsBreakdown");
            Panel pnlIsBreakdown = (Panel)e.Item.FindControl("pnlIsBreakdown");
            if (objBookedMrConfig.IsBreakdown == true)
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = true;
                    pnlNotIsBreakdown.Visible = false;
                }
            }
            else
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = false;
                    pnlNotIsBreakdown.Visible = true;
                }
            }
            #region Package and Equipment Collection
            //Bind Package//
            Panel pnlIsPackageSelected = (Panel)e.Item.FindControl("pnlIsPackageSelected");
            Panel pnlBuildMeeting = (Panel)e.Item.FindControl("pnlBuildMeeting");
            HtmlTableCell divpriceMeetingroom = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom");
            HtmlTableCell divpriceMeetingroom1 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom1");
            HtmlTableCell divpriceMeetingroom2 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom2");
            HtmlTableCell divpriceMeetingroom3 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom3");
            HtmlTableCell divpriceMeetingroom4 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom4");
            if (objBookedMrConfig.PackageID == 0)
            {
                pnlIsPackageSelected.Visible = false;
                if (objBookedMrConfig.BuildManageMRLst.Count > 0)
                {
                    pnlBuildMeeting.Visible = true;
                    Repeater rptBuildMeeting = (Repeater)e.Item.FindControl("rptBuildMeeting");
                    Label lblTotalBuildPackagePrice = (Label)e.Item.FindControl("lblTotalBuildPackagePrice");
                    CurrentPackageType = objBookedMrConfig.SelectedTime;
                    rptBuildMeeting.DataSource = objBookedMrConfig.BuildManageMRLst;
                    rptBuildMeeting.DataBind();
                    lblTotalBuildPackagePrice.Text = Math.Round((objBookedMrConfig.BuildPackagePriceTotal - (objBookedMrConfig.MeetingroomPrice)) * CurrencyConvert, 2).ToString();
                }
                else
                {
                    pnlBuildMeeting.Visible = false;
                }
                divpriceMeetingroom.Style.Add("display", "block");
                divpriceMeetingroom1.Style.Add("display", "block");
                divpriceMeetingroom2.Style.Add("display", "block");
                divpriceMeetingroom3.Style.Add("display", "block");
                divpriceMeetingroom4.Style.Add("display", "block");
            }
            else
            {
                pnlIsPackageSelected.Visible = true;
                pnlBuildMeeting.Visible = false;
                Label lblSelectedPackage = (Label)e.Item.FindControl("lblSelectedPackage");
                Label lblPackageDescription = (Label)e.Item.FindControl("lblPackageDescription");
                Label lblPackagePrice = (Label)e.Item.FindControl("lblPackagePrice");
                Label lblTotalPackagePrice = (Label)e.Item.FindControl("lblTotalPackagePrice");
                lblPackagePrice.Text = Math.Round(objBookedMrConfig.PackagePriceTotal * CurrencyConvert, 2).ToString();
                Repeater rptPackageItem = (Repeater)e.Item.FindControl("rptPackageItem");

                PackageByHotel p = objHotel.GetPackageDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageByHotelColumn.PackageId).Where(a => a.PackageId == objBookedMrConfig.PackageID).FirstOrDefault();
                if (p != null)
                {
                    CurrentPackageItem = objBookedMrConfig.ManagePackageLst;
                    rptPackageItem.DataSource = objHotel.GetPackageItemDetailsByPackageID(Convert.ToInt64(p.PackageId));
                    rptPackageItem.DataBind();

                    lblSelectedPackage.Text = p.PackageIdSource.PackageName;
                    if (p.PackageIdSource.PackageName.ToLower() == "favourite")
                    {
                        lblPackageDescription.Text = GetKeyResult("FAVOURITEDESCRIPTION");
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "elegant")
                    {
                        lblPackageDescription.Text = GetKeyResult("ELEGANTDESCRIPTION");
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "standard")
                    {
                        lblPackageDescription.Text = GetKeyResult("STANDARDDESCRIPTION");
                    }
                }
                Panel pnlIsExtra = (Panel)e.Item.FindControl("pnlIsExtra");
                Label lblextratitle = (Label)e.Item.FindControl("lblextratitle");
                Label lblExtraPrice = (Label)e.Item.FindControl("lblExtraPrice");
                if (objBookedMrConfig.ManageExtrasLst.Count > 0)
                {
                    Repeater rptExtra = (Repeater)e.Item.FindControl("rptExtra");
                    objHp = objHotel.GetIsExtraItemDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageItemsColumn.Id);
                    rptExtra.DataSource = objBookedMrConfig.ManageExtrasLst;
                    rptExtra.DataBind();
                    pnlIsExtra.Visible = true;

                    lblExtraPrice.Text = Math.Round(objBookedMrConfig.ExtraPriceTotal * CurrencyConvert, 2).ToString();
                }
                else
                {
                    lblExtraPrice.Visible = false;

                    lblextratitle.Visible = false;
                    pnlIsExtra.Visible = false;

                }
                divpriceMeetingroom.Visible = false;
                divpriceMeetingroom1.Visible = false;
                divpriceMeetingroom2.Visible = false;
                divpriceMeetingroom3.Visible = false;
                divpriceMeetingroom4.Visible = false;
                lblTotalPackagePrice.Text = Math.Round((objBookedMrConfig.PackagePriceTotal + objBookedMrConfig.ExtraPriceTotal) * CurrencyConvert, 2).ToString();
            }
            Panel pnlEquipment = (Panel)e.Item.FindControl("pnlEquipment");
            if (objBookedMrConfig.EquipmentLst.Count > 0)
            {
                pnlEquipment.Visible = true;
                Repeater rptEquipment = (Repeater)e.Item.FindControl("rptEquipment");
                rptEquipment.DataSource = objBookedMrConfig.EquipmentLst;
                rptEquipment.DataBind();
                Label lblTotalEquipmentPrice = (Label)e.Item.FindControl("lblTotalEquipmentPrice");
                lblTotalEquipmentPrice.Text = Math.Round(objBookedMrConfig.EquipmentPriceTotal * CurrencyConvert, 2).ToString();
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            #endregion
        }
    }

    protected void rptBuildMeeting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BuildYourMR objBuildYourMR = e.Item.DataItem as BuildYourMR;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalItemPrice = (Label)e.Item.FindControl("lblTotalItemPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.FoodBeverages).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objBuildYourMR.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objBuildYourMR.Quantity.ToString();
                lblTotalItemPrice.Text = Math.Round(objBuildYourMR.ItemPrice * objBuildYourMR.Quantity * CurrencyConvert, 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if(objp!=null)
                //{
                //     = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objBuildYourMR.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptEquipment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageEquipment objManageEquipment = e.Item.DataItem as ManageEquipment;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Equipment).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity * CurrencyConvert, 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if (objp != null)
                //{
                //    lblTotalPrice.Text = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objManageEquipment.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }



    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion

    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                        }
                    }
                    //Manage Package Item
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                        }
                    }

                    //if (PackageSelected)
                    //{
                    //    objBooking.TotalBookingPrice += TotalPackagePrice + TotalExtraPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                    //else
                    //{
                    //    objBooking.TotalBookingPrice += TotalMeetingroomPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                }
            }

        }
    }
    #endregion
    #endregion

    

    #region Printdetails
    /// <summary>
    /// to print the details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        try
        {
            PrintHelper.PrintWebControl(Divdetails);

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    #endregion

    #region change status
    /// <summary>
    /// to change status
    /// </summary>

    public void changestatus()
    {
        try
        {
            if (objViewBooking_Hotel.updaterequestStatus(Convert.ToInt64(ViewState["BookingID"].ToString())))
            {
                bindgrid();
                divbookingdetails.Visible = false;

                Response.Redirect("Viewbookings.aspx?type=" + Convert.ToString(Request.QueryString["Type"]));
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region save as pdf
    /// <summary>
    /// method to save as pdf
    /// </summary>

    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>document.getElementsByTagName('html')[0].style.overflow = 'auto';jQuery('#Loding_overlay').hide();</script>");
        }




    }
    #endregion

    #region Process
    /// <summary>
    /// method to move to process
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Lnkmovetoprocessed_Click(object sender, EventArgs e)
    {
        changestatus();

    }
    #endregion

    #region RowDataBound
    /// <summary>
    /// RowDataBound of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                GridViewRow gr = e.Row;
                LinkButton btnchkcommision = (LinkButton)gr.FindControl("btnchkcommision");
                Label lblArrivalDt = (Label)gr.FindControl("lblArrivalDt");
                LinkButton lblRefNo = (LinkButton)gr.FindControl("lblRefNo");
                TList<BookedMeetingRoom> bookedMR = objViewBooking_Hotel.getbookedmeetingroom(Convert.ToInt64(lblRefNo.Text));
                Label lblDepartureDt = (Label)gr.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)gr.FindControl("lblExpiryDt");
                Label lblCityName = (Label)gr.FindControl("lblCityName");
                Label lblHotelName = (Label)gr.FindControl("lblHotelName");
                Label lblBookingDt=(Label)gr.FindControl("lblBookingDt");
                Label lblCancellationLimit = (Label)gr.FindControl("lblCancellationLimit");
                Label lblCancelled = (Label)gr.FindControl("lblCancelled");
                LinkButton lbtCancel = (LinkButton)gr.FindControl("lbtCancel");
                lblCityName.Text = getCityName(lblCityName.ToolTip);
                double x = (Convert.ToDouble(getCancellationLimit(Convert.ToInt32(lblCancellationLimit.ToolTip))));
                lblCancellationLimit.Text = "N/A";
                
                string[] fragments = Regex.Split(lblArrivalDt.Text, "/");
                string newStr = "";
                newStr += fragments[1]+"/";
                newStr += fragments[0]+"/";
                newStr += fragments[2];
                DateTime arrivalDate = Convert.ToDateTime(newStr);
                if (arrivalDate < DateTime.Now.Date)
                {
                    //Do Nothing and let the checkbox be not visible.
                }
                else
                {
                    double days = Math.Abs(((DateTime.Now.Date - arrivalDate).TotalDays));
                    if (days >= Convert.ToInt32(getCancellationLimit(Convert.ToInt32(lblCancellationLimit.ToolTip))))
                    {
                        lbtCancel.Visible = true;
                        lblCancellationLimit.Text = (DateTime.ParseExact(lblArrivalDt.Text, "dd/MM/yyyy", null).AddDays(-(Convert.ToDouble(getCancellationLimit(Convert.ToInt32(lblCancellationLimit.ToolTip)))))).ToString("dd/MM/yyyy");
                    }
                }
                int requestStatus = objViewBooking_Hotel.getRequestStatus(Convert.ToInt64(lblRefNo.Text));
                if (requestStatus == 2)
                {
                    lblCancelled.Text = "Cancelled";
                    lblCancelled.Visible = true;
                    lbtCancel.Visible = false;
                    lblCancellationLimit.Text = "N/A";

                }
                if (lblCancelled.Text != "Cancelled" && lbtCancel.Visible == false)
                {
                    lblCancelled.Text = "N/A";
                    lblCancelled.Visible = true;
                }
                // the replace check image wherver renvue is done
                //Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(btnchkcommision.CommandArgument));


                
                
                


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Confirm Commsion
    /// <summary>
    /// when check commsion was confirmed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {


            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (chkconfirmrevenue.Checked)
            {
                objbooking.RevenueAmount = Convert.ToDecimal(lblrevenue.Text);
                objbooking.RevenueReason = "";
            }
            else
            {
                #region check file upload
                string strPlanName = string.Empty;
                if (ulPlan.HasFile)
                {
                    string fileExtension = Path.GetExtension(ulPlan.PostedFile.FileName.ToString());

                    if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".doc" || fileExtension == ".docx" || fileExtension.ToLower() == ".pdf")
                    {
                        strPlanName = Path.GetFileName(ulPlan.FileName);
                    }
                    else
                    {
                        divmessage.InnerHtml = "Please select file in (word,excel,pdf) formats only";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }

                    if (ulPlan.PostedFile.ContentLength > 1048576)
                    {
                        divmessage.InnerHtml = "Filesize of Supporting Document is too large. Maximum file size permitted is 1 MB.";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtrealvalue.Text))
                    {
                        divmessage.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(txtrealvalue.Text))
                        divmessage.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
                    else
                        divmessage.InnerHtml = "Kindly upload the Supporting Document.";
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    modalcheckcomm.Show();
                    return;
                }

                #endregion

                ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "SupportDocNetto/") + strPlanName);

                if (string.IsNullOrEmpty(txtrealvalue.Text))
                    txtrealvalue.Text = "0";
                decimal netvalue = Convert.ToDecimal(txtrealvalue.Text);
                decimal revenue = (Convert.ToDecimal(objbooking.FinalTotalPrice) / (netvalue - 1)) * 100;
                objbooking.RevenueAmount = revenue;
                objbooking.RevenueReason = ddlreason.SelectedItem.Text;
                objbooking.SupportingDocNetto = strPlanName;
            }

            if (objViewBooking_Hotel.updatecheckComm(objbooking))
            {
                if (objViewBooking_Hotel.updaterequestStatus(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString())))
                {
                    bindgrid();
                    Response.Redirect("Viewbookings.aspx?type=" + Typelink);

                }
            }
            txtrealvalue.Text = "";
            ddlreason.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Check Commission
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;
            txtrealvalue.Text = "";
            txtrealvalue.ReadOnly = true;
            ddlreason.SelectedIndex = 0;
            chkconfirmrevenue.Checked = true;
            divmessage.InnerHtml = "";
            divmessage.Style.Add("display", "none");
            modalcheckcomm.Show();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region CheckedChanged Confirm revenue
    /// <summary>
    /// when check commsion was checked/unchecked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void chkconfirmrevenue_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (chkconfirmrevenue.Checked)
            {
                txtrealvalue.ReadOnly = true;
                txtrealvalue.Text = "";
                ddlreason.SelectedIndex = 0;
                btnSubmit.ValidationGroup = "novalidation";
            }
            else
            {
                txtrealvalue.ReadOnly = false;
                ddlreason.SelectedIndex = 0;
                btnSubmit.ValidationGroup = "popup";
            }
            divmessage.Style.Add("display", "none");
            modalcheckcomm.Show();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region PageIndexChanging
    /// <summary>
    /// PageIndex Changing of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewBooking.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            bindgrid();
            bindAllDDLs();                  
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    #region Apply Paging
    /// <summary>
    /// Method to apply Paging in grid
    /// </summary>
    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grdViewBooking.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewBooking.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdViewBooking.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewBooking.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;

                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    #endregion

    #region save as pdf
    /// <summary>
    /// method to save as pdf
    /// </summary>

    protected void uiLinkButtonSaveAsPdfGrid_Click(object sender, EventArgs e)
    {
        try
        {

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdViewBooking.AllowPaging = false;
            

            grdViewBooking.DataSource = Session["latestGridState"];
            Control phhide = grdViewBooking.TopPagerRow.FindControl("ph");
            phhide.Visible = false;
            //bindgrid();
            //grdViewBooking.Columns[8].Visible = false;
            PrepareGridViewForExport(grdViewBooking);
            grdViewBooking.GridLines = GridLines.Both;
            grdViewBooking.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {
        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {
                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select City--")
                    {
                        l.Text="City";
                    }
                    if((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Hotel--")
                    {
                        l.Text="Hotel/Facility";
                    }
                    if((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Meeting Room--")
                    {
                        l.Text="Meeting Room";
                    }
                    if((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Booking Date--")
                    {
                        l.Text="Booking Date";
                    }
                    if((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Value--")
                    {
                        l.Text="Value";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Arrival Date--")
                    {
                        l.Text = "Arrival Date";
                    }

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion
    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion





    #region RowCreated
    /// <summary>
    /// method to RowCreated
    /// </summary>
    protected void grdViewBooking_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //string rowID = String.Empty;
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FF9'");
            //    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            //    ViewState["rowID"] = e.Row.RowIndex;

            //}
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    /// <summary>
    /// This is the event handler for lbtReset LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtReset_Click(object sender, EventArgs e)
    {
        txtBookingID.Text = "";
        txtFrom.Text = "";
        txtTo.Text = "";
        bindgrid();
        bindAllDDLs();
        ViewState["SearchAlpha"] = "all";
    }
}