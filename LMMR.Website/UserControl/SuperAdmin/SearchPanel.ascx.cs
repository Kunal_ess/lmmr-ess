﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class UserControl_SuperAdmin_SearchPanel : System.Web.UI.UserControl
{
    ClientContract objClient = new ClientContract();
    public event EventHandler SearchButtonClick;

    public long propCountryID
    {
        get
        {
            return Convert.ToInt64(drpSearchCountry.SelectedValue);
        }
        
    }

    public long propCityID
    {
        get
        {
            return Convert.ToInt64(drpSearchCity.SelectedValue);
        }
    }

    public DateTime propFromDate
    {
        get
        {
            return Convert.ToDateTime(txtFromdate.Text);
        }
    }

    public DateTime propToDate
    {
        get
        {
            return Convert.ToDateTime(txtTodate.Text);
        }
    }

    public string Venus
    {
        get
        {
            return Convert.ToString(txtVenue.Text);
        }
    }

    public bool Isvalidation { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Url.AbsolutePath.Contains("ControlPanel.aspx") || Request.Url.AbsolutePath.Contains("controlpanel.aspx"))
            {
                trVenue.Visible = true;
                // Hide show your div according your page.
            }
            else if (Request.Url.AbsolutePath.Contains("SearchBooking.aspx"))
            {
                // Hide show your div according your page.
            }
            FillCountryForSearch();
            txtFromdate.Text = "dd/mm/yy";
            txtTodate.Text = "dd/mm/yy";
        }
    }

    /// <summary>
    /// Selected Index Change of Dropdown Country list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpSearchCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCityFroSearch(Convert.ToInt32(drpSearchCountry.SelectedValue));
    }

    /// <summary>
    /// Reset all the filter values
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        txtTodate.Text = "dd/mm/yy";
        txtFromdate.Text = "dd/mm/yy";
        hdnfromdate.Value = "dd/mm/yy";
        hdntodate.Value = "dd/mm/yy";
        drpSearchCountry.SelectedValue = "0";
        FillCityFroSearch(0);
        txtVenue.Text = "";
    }

    /// <summary>
    /// This event fire for event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        SearchButtonClick.Invoke(sender, e);
    }

    /// <summary>
    /// Bind all the country Table in dropdown.
    /// </summary>
    protected void FillCountryForSearch()
    {
        //get list of countries.
        TList<Country> objCountry = objClient.GetByAllCountry();
        if (objCountry.Count > 0)
        {
            drpSearchCountry.DataValueField = "Id";
            drpSearchCountry.DataTextField = "CountryName";
            drpSearchCountry.DataSource = objCountry.OrderBy(a => a.CountryName);
            drpSearchCountry.DataBind();
            drpSearchCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
        }
        else
        {
            drpSearchCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
        }

    }

    protected void FillCityFroSearch(int intcountryID)
    {
            if (intcountryID > 0)
            {
                drpSearchCity.DataValueField = "Id";
                drpSearchCity.DataTextField = "City";
                drpSearchCity.DataSource = objClient.GetCityByCountry(Convert.ToInt32(drpSearchCountry.SelectedValue)).OrderBy(a => a.City);
                drpSearchCity.DataBind();
                drpSearchCity.Items.Insert(0, new ListItem("--Select city--", ""));
                drpSearchCity.Enabled = true;
            }
            else
            {
                drpSearchCity.Items.Clear();
                drpSearchCity.Items.Insert(0, new ListItem("--Select city--", ""));
                drpSearchCity.Enabled = false;
            }
    }


}