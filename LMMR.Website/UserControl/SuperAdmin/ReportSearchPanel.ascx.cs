﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class UserControl_SuperAdmin_ReportSearchPanel : System.Web.UI.UserControl
{
    ClientContract objClient = new ClientContract();
    public event EventHandler SearchButtonClick;
    public event EventHandler ClearButtonClick;
    public long propCountryID
    {
        get
        {
            return Convert.ToInt64(drpSearchCountry.SelectedValue);
        }
        
    }
    public string propCountryName
    {
        get
        {
            return drpSearchCountry.SelectedItem.Text;
        }

    }
    public long propCityID
    {
        get
        {
            return Convert.ToInt64(drpSearchCity.SelectedValue == "--Select city--" ? "All Cities" : drpSearchCity.SelectedValue);
        }
    }
    public string propCityName
    {
        get
        {
            return drpSearchCity.SelectedItem.Text;
        }
    }

    public DateTime propFromDate
    {
        get
        {            
            if (txtFromdate.Text != "" && txtFromdate.Text.ToLower() != "dd/mm/yy")
            {
                DateTime startdate = new DateTime(Convert.ToInt32("20" + txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
                return startdate;
            }
            else
            {
                return DateTime.MinValue;
            }
        }
    }

    public DateTime propToDate
    {
        get
        {
            if (txtTodate.Text != "" && txtTodate.Text.ToLower() != "dd/mm/yy")
            {
                DateTime enddate = new DateTime(Convert.ToInt32("20" + txtTodate.Text.Split('/')[2]), Convert.ToInt32(txtTodate.Text.Split('/')[1]), Convert.ToInt32(txtTodate.Text.Split('/')[0]));
                return enddate;
            }
            else
            {
                return DateTime.MinValue;
            }
        }
    }

    public string Venus
    {
        get
        {
            return Convert.ToString(txtVenue.Text);
        }
    }
    public string CompanyName
    {
        get
        {
            return Convert.ToString(txtCompanyName.Text);
        }
    }
    public string propFlName
    {
        get
        {
            return Convert.ToString(txtFlname.Text);
        }
    }
    public string Years
    {
        get
        {
            return Convert.ToString(string.IsNullOrEmpty(txtYear.Text.Trim()) ? DateTime.Now.Year.ToString() : txtYear.Text.Trim());
        }
    }
    public long propProfileName
    {
        get
        {            
            return Convert.ToInt64(rdbProfile.SelectedValue);
        }
    }
    public string PropHotelIds
    {
        get
        {
            string ids = "";
            for (int i = 0; i < chkchkListHotel.Items.Count;i++ )
            {
                if (chkchkListHotel.Items[i].Selected)
                {
                    if (chkchkListHotel.Items[i].Value == "0")
                    {
                        break;
                    }
                    if (ids.Length > 0)
                    {
                        ids += ",";
                    }
                    ids += chkchkListHotel.Items[i].Value;
                }
            }
            return ids;
        }
    }    
    public bool Isvalidation { get; set; }
    public string ReportType="";
    protected void Page_Load(object sender, EventArgs e)
    {
        trEmptyVenueCheckBoxList.Visible = false;
        if (!IsPostBack)
        {
            if (Request.Url.AbsolutePath.Contains("ControlPanel.aspx") || Request.Url.AbsolutePath.Contains("controlpanel.aspx"))
            {
                trVenue.Visible = true;
                // Hide show your div according your page.
            }
            else if (Request.Url.AbsolutePath.Contains("SearchBooking.aspx"))
            {
                // Hide show your div according your page.
            }
            FillCountryForSearch();
            txtFromdate.Text = "dd/mm/yy";
            txtTodate.Text = "dd/mm/yy";
            
            rdbProfile.Items.Clear();
            rdbProfile.Items.Add(new ListItem("Select", "0"));           
            rdbProfile.SelectedValue = "0";
        }        
    }

    /// <summary>
    /// Selected Index Change of Dropdown Country list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpSearchCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCityFroSearch(Convert.ToInt32(drpSearchCountry.SelectedValue));
        ReportType = Page.GetType().GetProperty("ReportName").GetValue(Page, null).ToString();
        if (ReportType != "" || ReportType != "No Report")
        {
            if (ReportType == "lead time report" || ReportType == "availability report" || ReportType == "online inventory changes report" || ReportType == "commission control report")
            {
                txtVenue_TextChanged(null, null);
            }
            //else if (ReportType == "availability report")
            //{
            //    txtVenue_TextChanged(null, null);
            //}
            else if (ReportType == "profile company / agency report" || ReportType == "production company / meet2 report")
            {
                txtCompanyName_TextChanged(null, null);
            }

        }
    }
    protected void drpSearchCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReportType = Page.GetType().GetProperty("ReportName").GetValue(Page, null).ToString();
        if (ReportType != "" || ReportType != "No Report")
        {
            if (ReportType == "lead time report" || ReportType == "availability report" || ReportType == "online inventory changes report" || ReportType== "commission control report")
            {
                txtVenue_TextChanged(null, null);
            }
            //else if (ReportType == "availability report")
            //{
            //    txtVenue_TextChanged(null, null);
            //}
            else if (ReportType == "profile company / agency report" || ReportType == "production company / meet2 report")
            {
                txtCompanyName_TextChanged(null, null);
            }

        }
    }
    /// <summary>
    /// Reset all the filter values
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        txtTodate.Text = "dd/mm/yy";
        txtFromdate.Text = "dd/mm/yy";
        hdnfromdate.Value = "dd/mm/yy";
        hdntodate.Value = "dd/mm/yy";
        txtYear.Text = "";
        drpSearchCountry.SelectedValue = "0";
        FillCityFroSearch(0);
        txtVenue.Text = "";
        txtFlname.Text = "";
        txtCompanyName.Text = "";
        trVenueCheckBoxList.Visible = false;;
        trEmptyVenueCheckBoxList.Visible = false;
        chkchkListHotel.Items.Clear();
        //chkchkListHotel.DataBind();
        ClearButtonClick.Invoke(sender, e); 
        
    }

    public void ClearRecords()
    {
        txtTodate.Text = "dd/mm/yy";
        txtFromdate.Text = "dd/mm/yy";
        hdnfromdate.Value = "dd/mm/yy";
        hdntodate.Value = "dd/mm/yy";
        drpSearchCountry.SelectedValue = "0";
        FillCityFroSearch(0);
        txtVenue.Text = "";
        txtCompanyName.Text = "";
        trVenueCheckBoxList.Visible = false;
        chkchkListHotel.DataSource = null;
        chkchkListHotel.DataBind();
        
    }
    
    public void GetPropertiesforControl()
    {       
        ReportType = Page.GetType().GetProperty("ReportName").GetValue(Page, null).ToString();
        if (ReportType != "" || ReportType != "no report")
        {
            CalendarExtender1.EndDate = DateTime.MaxValue;
            CalendarExtender2.EndDate = DateTime.MaxValue;
            CalendarExtender1.StartDate = DateTime.MinValue;
            CalendarExtender2.StartDate = DateTime.MinValue;
            if (ReportType == "lead time report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = true;
                divCompanyName.Visible = false;
                tblYear.Visible = false;
                divProfile.Visible = false;
                tblDateFromTo.Visible = true;
                tblNewsLetter.Visible = false;
                CalendarExtender1.EndDate = DateTime.Now.AddDays(59);
                CalendarExtender2.EndDate = DateTime.Now.AddDays(59);
                CalendarExtender1.StartDate = DateTime.Now;
                CalendarExtender2.StartDate = DateTime.Now;

            }
            else if (ReportType == "availability report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = true;
                divCompanyName.Visible = false;
                tblYear.Visible = false;
                divProfile.Visible = false;
                tblDateFromTo.Visible = true;
                tblNewsLetter.Visible = false;
                CalendarExtender1.EndDate = DateTime.Now.AddDays(59);
                CalendarExtender2.EndDate = DateTime.Now.AddDays(59);
                CalendarExtender1.StartDate = DateTime.Now;
                CalendarExtender2.StartDate = DateTime.Now;
            }
            else if (ReportType == "commission control report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divProfile.Visible = false;
                //rdbProfile.Items.Clear();
                //rdbProfile.Items.Add(new ListItem("Booking", "0"));
                //rdbProfile.Items.Add(new ListItem("Request", "1"));
                //rdbProfile.SelectedValue = "0";
                divVenue.Visible = true;
                divCompanyName.Visible = false;
                tblNewsLetter.Visible = false;
                tblYear.Visible = false;
                CalendarExtender1.EndDate = DateTime.Now;
                CalendarExtender2.EndDate = DateTime.Now;
                tblDateFromTo.Visible = true;
            }
            else if (ReportType == "revenue report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divProfile.Visible = true;
                tblNewsLetter.Visible = false;
                rdbProfile.Items.Clear();
                rdbProfile.Items.Add(new ListItem("Booking", "0"));
                rdbProfile.Items.Add(new ListItem("Request", "1"));
                rdbProfile.SelectedValue = "0";
                divVenue.Visible = true;
                divCompanyName.Visible = false;
                tblYear.Visible = false;

                tblDateFromTo.Visible = true;
            }
            else if (ReportType == "online inventory changes report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = true;
                divCompanyName.Visible = false;
                tblNewsLetter.Visible = false;
                tblYear.Visible = false;
                divProfile.Visible = false;
                tblDateFromTo.Visible = true;
                CalendarExtender1.EndDate = DateTime.Now.AddDays(59);
                CalendarExtender2.EndDate = DateTime.Now.AddDays(59);
            }
            else if (ReportType == "cancelation report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                tblNewsLetter.Visible = false;
                divVenue.Visible = false;
                divCompanyName.Visible = false;
                tblYear.Visible = false;
                divProfile.Visible = false;
                tblDateFromTo.Visible = true;
            }
            else if (ReportType == "inventory report of existing hotel")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = false;
                tblNewsLetter.Visible = false;
                divProfile.Visible = false;
                divCompanyName.Visible = false;
                tblYear.Visible = false;
                tblDateFromTo.Visible = false;
                trVenue.Visible = true;
            }
            else if (ReportType == "added hotel in period inventory report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = false;
                tblNewsLetter.Visible = false;
                divCompanyName.Visible = false;
                tblYear.Visible = false;
                tblDateFromTo.Visible = true;
                divProfile.Visible = false;
            }
            else if (ReportType == "booking / request report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = false;
                divCompanyName.Visible = false;
                tblNewsLetter.Visible = false;
                tblYear.Visible = false;
                divProfile.Visible = true;
                rdbProfile.Items.Clear();
                rdbProfile.Items.Add(new ListItem("Booking", "0"));
                rdbProfile.Items.Add(new ListItem("Request", "1"));
                rdbProfile.SelectedValue = "0";
                tblDateFromTo.Visible = true;
            }
            else if (ReportType == "profile company / agency report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = false;
                tblNewsLetter.Visible = false;
                divCompanyName.Visible = true;
                lblcomname.Text = "Name : ";
                tblYear.Visible = false;
                divProfile.Visible = true;
                tblDateFromTo.Visible = false;
                tblDateFromTo.Visible = true;
           
                rdbProfile.Items.Clear();
                rdbProfile.Items.Add(new ListItem("Company", "9"));
                rdbProfile.Items.Add(new ListItem("Meet2", "7"));
                rdbProfile.Items.Add(new ListItem("Individual", "10"));
                rdbProfile.SelectedValue = "9";
            }
            else if (ReportType == "production company / meet2 report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = false;
                divCompanyName.Visible = true;
                tblYear.Visible = true;
                tblDateFromTo.Visible = false;
                divProfile.Visible = true;
                tblNewsLetter.Visible = false;
                lblcomname.Text = "Company Name :";
                rdbProfile.Items.Clear();
                rdbProfile.Items.Add(new ListItem("Company", "9"));
                rdbProfile.Items.Add(new ListItem("Meet2", "7"));
                //rdbProfile.Items.Add(new ListItem("Individual", "10"));
                rdbProfile.SelectedValue = "9";
            }
            else if (ReportType == "Production Report Hotel and Meeting facilities")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = false;
                divCompanyName.Visible = true;
                divProfile.Visible = false;
                tblNewsLetter.Visible = false;
                lblcomname.Text = "Venue(s) :";
                tblYear.Visible = true;
                tblDateFromTo.Visible = false;

            }
            else if (ReportType == "ranking report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = false;
                divProfile.Visible = false;
                divCompanyName.Visible = false;
                tblYear.Visible = false;
                tblDateFromTo.Visible = false;
                trVenue.Visible = true;
                tblNewsLetter.Visible = false;
            }
            else if (ReportType == "conversion booking report")
            {
                trCountry.Visible = true;
                divCity.Visible = true;
                divVenue.Visible = true;
                divProfile.Visible = false;
                divCompanyName.Visible = false;
                tblYear.Visible = true;
                tblDateFromTo.Visible = false;
                trVenue.Visible = true;
                tblNewsLetter.Visible = false;
            }
            else if (ReportType == "newsletter report")
            {
                trCountry.Visible = false;
                divCity.Visible = false;
                divVenue.Visible = false;
                divProfile.Visible = false;
                divCompanyName.Visible = false;
                tblYear.Visible = false;
                tblDateFromTo.Visible = false;                
                tblNewsLetter.Visible = true;
            }
        }
    }
    /// <summary>
    /// This event fire for event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {        
        SearchButtonClick.Invoke(sender, e);        
    }

    /// <summary>
    /// Bind all the country Table in dropdown.
    /// </summary>
    protected void FillCountryForSearch()
    {
        //get list of countries.
        TList<Country> objCountry = objClient.GetByAllCountry();
        if (objCountry.Count > 0)
        {
            drpSearchCountry.DataValueField = "Id";
            drpSearchCountry.DataTextField = "CountryName";
            drpSearchCountry.DataSource = objCountry.OrderBy(a => a.CountryName);
            drpSearchCountry.DataBind();
            drpSearchCountry.Items.Insert(0, new ListItem("--All Country--", "0"));
        }
        else
        {
            drpSearchCountry.Items.Insert(0, new ListItem("--All Country--", "0"));
        }

    }

    protected void FillCityFroSearch(int intcountryID)
    {
            if (intcountryID > 0)
            {
                drpSearchCity.DataValueField = "Id";
                drpSearchCity.DataTextField = "City";
                drpSearchCity.DataSource = objClient.GetCityByCountry(Convert.ToInt32(drpSearchCountry.SelectedValue)).OrderBy(a => a.City);
                drpSearchCity.DataBind();
                drpSearchCity.Items.Insert(0, new ListItem("--All cities--", "0"));
                drpSearchCity.Enabled = true;
            }
            else
            {
                drpSearchCity.Items.Clear();
                drpSearchCity.Items.Insert(0, new ListItem("--All cities--", "0"));
                drpSearchCity.Enabled = false;
            }
    }


    protected void txtVenue_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtVenue.Text.Trim()))
        {
            HotelInfo ObjHotelinfo = new HotelInfo();
            string whereclauseforHotel = HotelColumn.IsRemoved + "=0 AND " + HotelColumn.Name + " Like '" + txtVenue.Text + "%' ";
            if (drpSearchCountry.SelectedIndex != 0)
            {
                whereclauseforHotel += " AND " + HotelColumn.CountryId + "=" + drpSearchCountry.SelectedValue;
            }
            if (drpSearchCity.SelectedIndex != 0)
            {
                whereclauseforHotel += "  AND " + HotelColumn.CityId + "=" + drpSearchCity.SelectedValue;
            }
            TList<Hotel> THotel = ObjHotelinfo.GetHotelbyCondition(whereclauseforHotel, string.Empty).FindAllDistinct(HotelColumn.Name);
            if (THotel.Count > 0)
            {
                chkchkListHotel.DataSource = THotel;
                chkchkListHotel.DataTextField = "Name";
                chkchkListHotel.DataValueField = "Id";
                chkchkListHotel.DataBind();
                trVenueCheckBoxList.Visible = true;
                trEmptyVenueCheckBoxList.Visible = false;
                lblVenue.Text = "Select Venue(s)";
                //chkchkListHotel.Items.Insert(0, new ListItem("All", "0"));
            }
            else
            {
                chkchkListHotel.DataSource = null;
                chkchkListHotel.DataBind();
                trVenueCheckBoxList.Visible = false;
                trEmptyVenueCheckBoxList.Visible = true;
            }
        }
        else
        {
            chkchkListHotel.DataSource = null;
            chkchkListHotel.DataBind();
            trVenueCheckBoxList.Visible = false;
            trEmptyVenueCheckBoxList.Visible = false;
        }
    }

    protected void txtCompanyName_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtCompanyName.Text.Trim()))
        {
            ReportType = Page.GetType().GetProperty("ReportName").GetValue(Page, null).ToString();

            switch (ReportType)
            {
                case "profile company / agency report":
                    string whereclauseforHotel;
                    VList<ViewReportforProfileCompanyAgency> tuser = new VList<ViewReportforProfileCompanyAgency>();
                    ReportCreation rc = new ReportCreation();
                    if (rdbProfile.SelectedValue == "7")
                    {
                        whereclauseforHotel = ViewReportforProfileCompanyAgencyColumn.Usertype + "=7 AND " + ViewReportforProfileCompanyAgencyColumn.CompanyName + " Like '" + txtCompanyName.Text + "%' ";
                    }
                    else if (rdbProfile.SelectedValue == "10")
                    {
                        whereclauseforHotel = ViewReportforProfileCompanyAgencyColumn.Usertype + "=10 AND " + ViewReportforProfileCompanyAgencyColumn.CompanyName + " Like '" + txtCompanyName.Text + "%' ";
                    }
                    else
                    {
                        whereclauseforHotel = ViewReportforProfileCompanyAgencyColumn.Usertype + "=9 AND " + ViewReportforProfileCompanyAgencyColumn.CompanyName + " Like '" + txtCompanyName.Text + "%' ";
                    }


                    if (drpSearchCountry.SelectedIndex != 0)
                    {
                        whereclauseforHotel += " AND " + ViewReportforProfileCompanyAgencyColumn.CountryId + "=" + drpSearchCountry.SelectedValue;
                    }
                    if (drpSearchCity.SelectedIndex != 0)
                    {
                        whereclauseforHotel += "  AND " + ViewReportforProfileCompanyAgencyColumn.CityName + "='" + drpSearchCity.SelectedItem.Text + "' ";
                    }
                    tuser = rc.GetProfileCompanyAgencyReport(whereclauseforHotel).FindAllDistinct(ViewReportforProfileCompanyAgencyColumn.UserId);
                    if (tuser.Count > 0)
                    {
                        chkchkListHotel.DataSource = tuser;
                        chkchkListHotel.DataTextField = "CompanyName";
                        chkchkListHotel.DataValueField = "UserId";
                        chkchkListHotel.DataBind();
                        trVenueCheckBoxList.Visible = true;
                        trEmptyVenueCheckBoxList.Visible = false;
                        lblVenue.Text = "Select CompanyName";
                        //chkchkListHotel.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        chkchkListHotel.DataSource = null;
                        chkchkListHotel.DataBind();
                        trVenueCheckBoxList.Visible = false;
                        trEmptyVenueCheckBoxList.Visible = true;
                    }
                    break;
                case "Production Report Hotel and Meeting facilities":
                    VList<ViewReportProductionReportHotel> thotel = new VList<ViewReportProductionReportHotel>();
                    ReportCreation rchotel = new ReportCreation();
                    string whereclauseforHotelprodut = ViewReportProductionReportHotelColumn.HotelName + " Like '" + txtCompanyName.Text + "%' ";
                    if (drpSearchCountry.SelectedIndex != 0)
                    {
                        whereclauseforHotelprodut += " AND " + ViewReportProductionReportHotelColumn.CountryId + "=" + drpSearchCountry.SelectedValue;
                    }
                    if (drpSearchCity.SelectedIndex != 0)
                    {
                        whereclauseforHotelprodut += "  AND " + ViewReportProductionReportHotelColumn.CityId + "=" + drpSearchCity.SelectedValue;
                    }

                    thotel = rchotel.GetProductionReportHotel(whereclauseforHotelprodut).FindAllDistinct(ViewReportProductionReportHotelColumn.Id);
                    if (thotel.Count > 0)
                    {
                        chkchkListHotel.DataSource = thotel;
                        chkchkListHotel.DataTextField = "HotelName";
                        chkchkListHotel.DataValueField = "Id";
                        chkchkListHotel.DataBind();
                        trVenueCheckBoxList.Visible = true;
                        trEmptyVenueCheckBoxList.Visible = false;
                        lblVenue.Text = "Select HotelName";
                        //chkchkListHotel.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        chkchkListHotel.DataSource = null;
                        chkchkListHotel.DataBind();
                        trVenueCheckBoxList.Visible = false;
                        trEmptyVenueCheckBoxList.Visible = true;
                    }
                    break;
                case "production company / meet2 report":
                    VList<ViewReportProductionCompanyAgency> tcompanyagency = new VList<ViewReportProductionCompanyAgency>();
                    ReportCreation rccompanyagency = new ReportCreation();
                    string whereclauseforcompanyagency;
                    if (rdbProfile.SelectedValue == "7")
                    {
                        whereclauseforcompanyagency = ViewReportProductionCompanyAgencyColumn.Usertype + "=7 AND " + ViewReportProductionCompanyAgencyColumn.CompanyName + " Like '" + txtCompanyName.Text + "%' ";
                    }
                    else
                    {
                        whereclauseforcompanyagency = ViewReportProductionCompanyAgencyColumn.Usertype + "=9 AND " + ViewReportProductionCompanyAgencyColumn.CompanyName + " Like '" + txtCompanyName.Text + "%' ";
                    }

                    if (drpSearchCountry.SelectedIndex != 0)
                    {
                        whereclauseforcompanyagency += " AND " + ViewReportProductionCompanyAgencyColumn.CountryId + "=" + drpSearchCountry.SelectedValue;
                    }
                    if (drpSearchCity.SelectedIndex != 0)
                    {
                        whereclauseforcompanyagency += "  AND " + ViewReportProductionCompanyAgencyColumn.CityName + " Like '" + drpSearchCity.SelectedItem.Text + "%' ";
                    }

                    tcompanyagency = rccompanyagency.GetProductionReport(whereclauseforcompanyagency).FindAllDistinct(ViewReportProductionCompanyAgencyColumn.Userid);

                    if (tcompanyagency.Count > 0)
                    {
                        chkchkListHotel.DataSource = tcompanyagency;
                        chkchkListHotel.DataTextField = "CompanyName";
                        chkchkListHotel.DataValueField = "Userid";
                        chkchkListHotel.DataBind();
                        trVenueCheckBoxList.Visible = true;
                        trEmptyVenueCheckBoxList.Visible = false;
                        lblVenue.Text = "Select CompanyName";
                        //chkchkListHotel.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        chkchkListHotel.DataSource = null;
                        chkchkListHotel.DataBind();
                        trVenueCheckBoxList.Visible = false;
                        trEmptyVenueCheckBoxList.Visible = true;
                    }
                    break;
            }
        }
        else
        {
            chkchkListHotel.DataSource = null;
            chkchkListHotel.DataBind();
            trVenueCheckBoxList.Visible = false;
            trEmptyVenueCheckBoxList.Visible = false;
        }
    }
}