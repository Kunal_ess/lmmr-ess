﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_SuperAdmin_TopMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        foreach (LinkButton h in this.Controls.OfType<LinkButton>())
        {
            if (!string.IsNullOrEmpty(h.PostBackUrl))
            {
                if (Request.RawUrl.ToLower().Contains(h.PostBackUrl.ToLower().Replace("~", "")))
                {
                    h.CssClass = "select";
                }
                if (Request.RawUrl.ToLower().Contains("managecountry.aspx"))
                {
                    h.CssClass = "select";
                }
            }
        }

        if (Request.RawUrl.ToLower().Contains("managecountryothers.aspx") || Request.RawUrl.ToLower().Contains("applicationothers.aspx") || Request.RawUrl.ToLower().Contains("managelanguage.aspx"))
        {
            hyperlink12.CssClass = "select";
        }
    }
}