﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class UserControl_Agency_TopMenu : System.Web.UI.UserControl
{
    public Users objUsers
    {
        get
        {
            return (Session["CurrentAgencyUser"] as Users);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (HyperLink h in this.Controls.OfType<HyperLink>())
            {
                if (!string.IsNullOrEmpty(h.NavigateUrl))
                {
                    if (Request.RawUrl.ToLower().Contains(h.NavigateUrl.ToLower().Replace("~", "")))
                    {
                        h.CssClass = "select";
                    }
                }
            }           
            if (Request.RawUrl.ToLower().Contains("/searchbookingrequest.aspx"))
            {
                lnkAgencyClients.CssClass = "select";
            }
            else if (Request.RawUrl.ToLower().Contains("/booking.aspx") || Request.RawUrl.ToLower().Contains("/request.aspx") || Request.RawUrl.ToLower().Contains("/bookingstep") || Request.RawUrl.ToLower().Contains("/requeststep"))
            {
                lnkAgencyClients.CssClass = "select";
            }
            else
            {
                lnkAgencyClients.CssClass = "";
            }
        }
    }
    protected void lnkAgencyClients_Click(object sender, EventArgs e)
    {
        //Session.Remove("masterInput");
        //Session.Remove("CurrencyID");
        Session["SearchType"] = "Basic";
        Session["CurrentUserSeleted"] = null;
        Session["IsSearchStart"] = null;
        Session["ComeFromBookingAndRequestPage"] = null;
        //Request.Url.AbsolutePath.Contains("SearchBookingRequest.aspx");
        Response.Redirect("SearchBookingRequest.aspx");
    }
}