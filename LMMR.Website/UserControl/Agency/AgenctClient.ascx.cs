﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

public partial class UserControl_Agency_AgenctClient : System.Web.UI.UserControl
{
    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(UserControl_Agency_AgenctClient));
    ClientContract objClient = new ClientContract();
    HotelInfo ObjHotelinfo = new HotelInfo();
    HotelManager objHotelManager = new HotelManager();
    EmailConfigManager em = new EmailConfigManager();
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    ManageAgentUser objAgent = new ManageAgentUser();
    VList<Viewbookingrequest> vlistBooking;
    VList<Viewbookingrequest> vlistreq;
    string strUsers = string.Empty;
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Check session exist or not
            if (Session["CurrentAgencyUserID"] == null)
            {
                //Response.Redirect("~/login.aspx");
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }

            if (!IsPostBack)
            {
                BindAgencyClients();               
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #region Bind groupclient Admin link
    public void BindAgencyClients()
    {
        GetUserLinkId();
        TList<Users> user = objClient.GetAllClientsOfAgencyUsers(strUsers);
        grvClients.DataSource = user;
        grvClients.DataBind();
    }

    public void GetUserLinkId()
    {
        UserDetails objUsers = objAgent.GetUserDetailByID(Convert.ToInt64(Session["CurrentAgencyUserID"])).FirstOrDefault();
        strUsers = Session["CurrentAgencyUserID"].ToString();
        if (strUsers == string.Empty)
        {
            strUsers = objUsers.LinkWith;
        }
        else
        {
            if (objUsers != null)
            {
                strUsers = strUsers + "," + objUsers.LinkWith;
            }
        }
        if (strUsers.EndsWith(","))
        {
            strUsers = strUsers.Substring(0, strUsers.Length - 1);
        }
    }
    #endregion

    #region Get CompanyName/Phone number from userdetails
    protected void grvClients_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Users data = e.Row.DataItem as Users;
            Label lblPhoneNumber = (Label)e.Row.FindControl("lblPhoneNumber");
            Label lblCompanyName = (Label)e.Row.FindControl("lblCompanyName");
            HiddenField hdnId = (HiddenField)e.Row.FindControl("hdnId");
            TList<UserDetails> objDtls = ObjHotelinfo.GetUserDetails(Convert.ToInt32(data.UserId));
            if (objDtls.Count > 0)
            {
                lblPhoneNumber.Text = objDtls[0].Phone;
                lblCompanyName.Text = objDtls[0].CompanyName;
            }            
        }
    }
    #endregion

    #region Search Methods
    /// <summary>
    //Start Search function for client name and from-to date
    /// </summary>    
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetUserLinkId();
            if (txtClientName.Text != "")
            {
                string name = txtClientName.Text;
                grvClients.DataSource = objClient.GetClientsOfAgencySearch(name, strUsers);
                grvClients.DataBind();
            }
            else
            {
                grvClients.DataSource = objClient.GetAllClientsOfAgencyUsers(strUsers);
                grvClients.DataBind();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void lbtReset_Click(object sender, EventArgs e)
    {
        txtClientName.Text = "";
        BindAgencyClients();
        pnlBookingdtl.Visible = false;
        lblLoggingUser.Text = "";
    }
    #endregion

    #region Redirect search page for booking/request
    //protected void lnbName_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        LinkButton lnbName = (LinkButton)sender;
    //        ViewState["Name"] = lnbName.CommandArgument;
    //        //lblLoggingUser.Text = "You can start booking as Client : " + lnbName.Text;
    //        //Response.Redirect("SearchBookingRequest.aspx");
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }
    //}
    #endregion   

    public void GetBookingRequestDate()
    {
        if (hdnUserId.Value != "0")
        {
            pnlBookingdtl.Visible = true;
            string whereclause = "AgencyClientId='" + hdnUserId.Value + "' and booktype = 0";
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistBooking = objViewBooking_Hotel.Bindgrid(whereclause, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
            if (vlistBooking.Count > 0)
            {
                lblBookingId.Text = Convert.ToString(vlistBooking[0].Id);
                DateTime BookDate = (DateTime)vlistBooking[0].BookingDate;
                lblBookingDate.Text = BookDate.ToString("dd/MM/yyyy");
            }
            else
            {
                lblBookingId.Text = "N/A";
                lblBookingDate.Text = "N/A";
            }

            string whereclauseReq = "AgencyClientId='" + hdnUserId.Value + "' and booktype in (1,2) ";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclauseReq, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
            if (vlistreq.Count > 0)
            {
                lblRequestId.Text = Convert.ToString(vlistreq[0].Id);
                DateTime RequestDate = (DateTime)vlistreq[0].BookingDate;
                lblRequestDate.Text = RequestDate.ToString("dd/MM/yyyy");
            }
            else
            {
                lblRequestId.Text = "N/A";
                lblRequestDate.Text = "N/A";
            }

            NewUser Userdetls = new NewUser();
            Users user = Userdetls.GetUserByID(Convert.ToInt32(hdnUserId.Value));
            if (user != null)
            {
                lblLoggingUser.Text = "You can start booking with this selected client : " + user.FirstName + "-" + user.LastName;
            }
        }
        else
        {
            pnlBookingdtl.Visible = false;
            lblLoggingUser.Text = "";
        }
    }
    protected void rbselect_CheckedChanged(object sender, EventArgs e)
    {
        GetBookingRequestDate();
    }
}