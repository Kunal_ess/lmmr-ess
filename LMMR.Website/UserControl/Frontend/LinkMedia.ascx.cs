﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Linq;
#endregion

public partial class UserControl_Frontend_LinkMedia : BaseUserControl
{
    #region Variables and Properties
    public int intTypeCMSid;
    ManageCMSContent objManageCMSContent = new ManageCMSContent();

    //SuperAdminTaskManager manager = new SuperAdminTaskManager();

    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
 		if (!Page.IsPostBack)
        {
            BindBottomLink();
            BindMediaLink();
        }
    }
    #endregion

    #region Functions

    /// <summary>
    /// This method bind the benefits with Datalist
    /// </summary>
    void BindBottomLink()
    {
        string filepath;
        long testing = Convert.ToInt64(Session["LanguageID"]);
        Cms cmsObj = new Cms();
        cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "companyinfo");
        if (cmsObj == null)
        {
            cmsObj = manager.getCmsEntityOnType("CompanyInfo");
        }
        TList<CmsDescription> list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true);
        if (list == null)
        {
            list = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
        }
        else if (list.Count == 0)
        {
            list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == 1 && a.IsActive == true);
        }
        CmsDescription obj1 = null;
        lbtCompanyInfo.Text = GetKeyResult("COMPANYINFORMATION");//obj1.ContentShortDesc;
        lbtForInvesters.Text = GetKeyResult("FORINVESTORS");//obj1.ContentShortDesc;
        lbtPrivacyStatement.Text = GetKeyResult("PRIVACYSTATEMENTS");//obj1.ContentShortDesc;
        lbtLegalAspects.Text = GetKeyResult("LEGALASPECTS");//obj1.ContentShortDesc;
        lbtTermsNCOnditions.Text = GetKeyResult("TERMSANDCONDITIONS");//obj1.ContentShortDesc;
        if (list != null)
        {
            if (list.Count != 0)
            {
                obj1 = list[0];
            }
        }

        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtCompanyInfo.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            lbtCompanyInfo.Enabled = false;
        }
        cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "forinvestors");
        if (cmsObj == null)
        {
            cmsObj = manager.getCmsEntityOnType("ForInvestors");
        }
        list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true);
        if (list == null)
        {
            list = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
        }
        else if (list.Count == 0)
        {
            list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == 1 && a.IsActive == true);
        }
        if (list != null)
        {
            if (list.Count != 0)
            {
                obj1 = list[0];
            }
        }

        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtForInvesters.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            lbtForInvesters.Enabled = false;
        }
        cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "privacystatement");
        if (cmsObj == null)
        {
            cmsObj = manager.getCmsEntityOnType("PrivacyStatement");
        }
        list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true);
        if (list == null)
        {
            list = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
        }
        else if (list.Count == 0)
        {
            list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == 1 && a.IsActive == true);
        }
        if (list != null)
        {
            if (list.Count != 0)
            {
                obj1 = list[0];
            }
        }
        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtPrivacyStatement.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            lbtPrivacyStatement.Enabled = false;
        }
        cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "legalaspects");
        if (cmsObj == null)
        {
            cmsObj = manager.getCmsEntityOnType("LegalAspects");
        }
        list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true);
        if (list == null)
        {
            list = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
        }
        else if (list.Count == 0)
        {
            list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == 1 && a.IsActive == true);
        }
        if (list != null)
        {
            if (list.Count != 0)
            {
                obj1 = list[0];
            }
        }
        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtLegalAspects.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            lbtLegalAspects.Enabled = false;
        }        
        cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "termsandconditions");
        if (cmsObj == null)
        {
            cmsObj = manager.getCmsEntityOnType("TermsAndConditions");
        }
        list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true);
        if (list == null)
        {
            list = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
        }
        else if (list.Count == 0)
        {
            list = PropCMSDESCRIPTION.FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == 1 && a.IsActive == true);
        }
        if (list != null)
        {
            if (list.Count != 0)
            {
                obj1 = list[0];
            }
        }
        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtTermsNCOnditions.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            lbtTermsNCOnditions.Enabled = false;
        }
    }

    void BindMediaLink()
    {
        Cms cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "links");
        if (cmsObj != null)
        {
            rptrSocialNetworking.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true);
            rptrSocialNetworking.DataBind();
            if (rptrSocialNetworking.Items.Count<=0)
            {
                rptrSocialNetworking.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64("1") && a.IsActive == true);
                rptrSocialNetworking.DataBind();
            }
        }
        else
        {
            Cms cmsObj2 = manager.getCmsEntityOnType("Links");
            if (cmsObj2 != null)
            {
                TList<CmsDescription> list = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"]));
                if (list != null)
                {
                    rptrSocialNetworking.DataSource = list;
                    rptrSocialNetworking.DataBind();
                }
                else
                {
                    list = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1"));
                    rptrSocialNetworking.DataSource = list;
                    rptrSocialNetworking.DataBind();
                }
            }
        }
        
    }


    #endregion

    #region event
    protected void RepeaterBottomLink_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CmsDescription obj = e.Item.DataItem as CmsDescription;
            if (obj != null)
            {
                LinkButton hypBottomLink = (LinkButton)e.Item.FindControl("hypBottomLink");
                hypBottomLink.Text = obj.ContentsDesc;
                string filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj.PageUrl.ToString();
                hypBottomLink.OnClientClick = "return open_win('" + filepath + "')";
                //hypBottomLink.NavigateUrl = "~/Uploads/MediaFile/" + obj.PageUrl;
            }
        }
    }
    #endregion
    protected void rptrSocialNetworking_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        CmsDescription obj = e.Item.DataItem as CmsDescription;
        if (obj != null)
        {
            HyperLink hypSocialSite = (HyperLink)e.Item.FindControl("hypSocialSite");
            string path = ConfigurationManager.AppSettings["FilePath"] + "SocialImage/" + obj.ContentImage;
            hypSocialSite.ImageUrl = path;
            hypSocialSite.NavigateUrl = obj.PageUrl;
        }
    }
}