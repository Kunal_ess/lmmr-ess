﻿#region Using Directives
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.IO;
using System.Linq;
#endregion

public partial class UserControl_Frontend_WithoutNews : BaseUserControl
{
    #region Variables and Properties
    public int intTypeCMSid;
    public int intLanguageID;
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    NewsLetterSubscriber objNewsSubscriber = new NewsLetterSubscriber();
    SendMails objSendmail = new SendMails();
    EmailConfigManager em = new EmailConfigManager();
    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        TextBoxWatermarkExtender1.WatermarkText = GetKeyResult("ENTERYOUREMAIL");
        if (Session["LanguageID"] == null)
        {
            Session["LanguageID"] = 1;
        }

        btnsub.Text = GetKeyResult("SUBSCRIBE");
    }
    #endregion

    #region Functions

    

    /// <summary>
    /// This function used for clear news subscription form.
    /// </summary>
    public void Clearform()
    {
        txtemail.Text = "Enter your email";
        Lblemailid.Text = "";
        txtFirstnam.Text = "";
        txtlastnam.Text = "";
    }

    /// <summary>
    /// This method return the pass the key value through GetResult Function 
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    //public string GetKeyResult(string key)
    //{
    //    return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}
    #endregion

    #region events
    /// <summary>
    /// In this evevt Mail is sent to the user who want to subscribe news .
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubscribe_Click(object sender, EventArgs e)
    {
        if (!objManageCMSContent.IsMailExist(hdnemail.Value))
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert('Your email id already exists.');</script>");
            Clearform();
        }
        else
        {
            objNewsSubscriber.SubscriberEmailid = hdnemail.Value;
            objNewsSubscriber.FirstName = titleDDL.SelectedItem.Text + txtFirstnam.Text;
            objNewsSubscriber.LastName = txtlastnam.Text;

            string message = string.Empty ;
            if (objManageCMSContent.AddSubscription(objNewsSubscriber))
            {
                EmailConfig eConfig = em.GetByName("New subscriber welcome");
                if (eConfig.IsActive)
                {
                    message = GetKeyResult("THANKYOUFORSUBSCRIPTION");//   message = "Thank you for your subscription. Stay tuned on our monthly Meeting Advise or Newsletter";
                    objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                    objSendmail.ToEmail = hdnemail.Value;
                    objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                    //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/NEWSubscribe.html"));
                    string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                    EmailConfigMapping emap2 = new EmailConfigMapping();
                    emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                    bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                    EmailValueCollection objEmailValues = new EmailValueCollection();

                    objSendmail.Subject = "Request for news subscription";
                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailFornewssubscribe(txtFirstnam.Text, txtlastnam.Text))
                    {
                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                    }
                    objSendmail.Body = bodymsg;
                    objSendmail.SendMail();
                }
             
                Clearform();

                Page.RegisterStartupScript("aa", "<script language='javascript'>alert('" + message + "');</script>");
            }
            else
            {
                message = "Your email id already exists!";
                Page.RegisterStartupScript("aa", "<script language='javascript'>alert('" + message + "');</script>");
            }
        }
        txtemail.Text = "";
        errorPopupnews.Style.Add("display", "none");
        errorPopupnews.Attributes.Add("class", "error");
    }

    /// <summary>
    /// This event used for gridview row bound.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdvwnews_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CmsDescription obj = e.Row.DataItem as CmsDescription;
            if (obj != null)
            {
                HyperLink HynNews = (HyperLink)e.Row.FindControl("HynNews");
                HynNews.Text = obj.ContentsDesc.ToString();
                HynNews.NavigateUrl = obj.PageUrl.ToString();
                Label lbldate = (Label)e.Row.FindControl("lbldate");
                if (lbldate != null)
                {
                    string date = String.Format("{0:MMMM-dd-yyyy}", obj.UpdatedDate);

                    if (!string.IsNullOrEmpty(date))
                    {
                        lbldate.Text = date;
                    }
                }

            }

        }
    }
    #endregion

}