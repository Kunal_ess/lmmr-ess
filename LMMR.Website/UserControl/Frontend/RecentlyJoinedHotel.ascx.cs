﻿#region using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Linq;
#endregion

public partial class UserControl_Frontend_RecentlyJoinedHotel : BaseUserControl
{
    #region Variables and Properties
    public int intTypeCMSid;
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindRecentlyJoinedHotel();
        }
    }
    #endregion

    #region Functions
    /// <summary>
    /// This method bind the Recently Joined Hotels with Repeater
    /// </summary>
    void BindRecentlyJoinedHotel()
    {
        Repeaterhotelrecjoin.DataSource = objManageCMSContent.GetTopFiveHotel();
        Repeaterhotelrecjoin.DataBind();
    }
    #endregion

    #region function
    ///// <summary>
    ///// This method return the value in Getresult function
    ///// </summary>
    ///// <param name="Key"></param>
    ///// <returns></returns>
    //public string GetKeyResult(string key)
    //{
    //    return  System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}
    #endregion
    protected void Repeaterhotelrecjoin_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Hotel hotel = e.Item.DataItem as Hotel;
            if (hotel != null)
            {

                //HyperLink HypHotelName = (HyperLink)e.Item.FindControl("HypHotelName");
                //HypHotelName.Text = hotel.Name.ToString();
                //HypHotelName.NavigateUrl = hotel.Name.ToString();
                Label HypHotelName = (Label)e.Item.FindControl("HypHotelName");
                HypHotelName.Text = hotel.Name.ToString();

                Label lblHoteldate = (Label)e.Item.FindControl("lblHoteldate");
                lblHoteldate.Text = hotel.CreationDate.ToString();

                if (lblHoteldate != null)
                {
                    string strGoOnlineDate = String.Format("{0:MMMM-dd-yyyy}", hotel.GoOnlineDate);

                    if (!string.IsNullOrEmpty(strGoOnlineDate))
                    {
                        if (strGoOnlineDate == String.Format("{0:MM-dd-yyyy}", DateTime.Now))
                        {
                            lblHoteldate.Text = "Today";
                        }
                        else if (strGoOnlineDate == String.Format("{0:MM-dd-yyyy}", DateTime.Now.AddDays(-1)))
                        {
                            lblHoteldate.Text = "Yesterday";
                        }
                        else
                        {
                            lblHoteldate.Text = strGoOnlineDate;
                        }


                    }

                }
            }
        }

    }
}
