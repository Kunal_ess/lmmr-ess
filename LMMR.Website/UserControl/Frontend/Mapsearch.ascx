﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Mapsearch.ascx.cs" Inherits="ucSearchLeft" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="left-inner-top">
    <div class="map">
            <asp:Image ID="imgMapSearch" ImageUrl="~/images/map.png" runat="server" onclick="ShowMapDiv();"/>
    </div>
    <div class="logo">
        <a href="#">
            <img src="<%= SiteRootPath %>images/logo-front.png" /></a>
    </div>
    <!--left-form START HERE-->
    <div class="left-form">
        <div id="Country">
            <asp:DropDownList ID="drpCountry" runat="server" CssClass="bigselect" OnSelectedIndexChanged="drpCountry_SelectedIndexChanged"
                AutoPostBack="True">
            </asp:DropDownList>
        </div>
        <div id="City">
            <asp:DropDownList ID="drpCity" CssClass="bigselect" runat="server">
            </asp:DropDownList>
        </div>
        
        <div id="Date">
            <input name="AnotherDate" class="dateinput">
            <input type="button" value="select" class="datebutton" onclick="displayDatePicker('AnotherDate', this);">
        </div>
        <div id="Quantity-day">
            <div id="Quantity">
                <label>
                    Duration &nbsp;</label><input type="text" value="1" name="quantity[]" id="quantity74"
                        class="inputbox">
                <input type="button" onclick="var qty_el = document.getElementById('quantity74'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;"
                    class="button_up">
                <input type="button" onclick="var qty_el = document.getElementById('quantity74'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) qty_el.value--;return false;"
                    class="button_down">
            </div>
            <div id="daysname">
                day(s)
            </div>
            <div id="Day">
                <asp:DropDownList ID="drpFullDay" runat="server">
                    <asp:ListItem Selected="True" Value="0" Text="--Select--"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div id="Participants">
            <label>
                Participants &nbsp;</label><input type="text" value="1" name="Participants[]" id="Participants74"
                    class="inputbox">
            <input type="button" onclick="var qty_el = document.getElementById('Participants74'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;"
                class="button_up">
            <input type="button" onclick="var qty_el = document.getElementById('Participants74'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) qty_el.value--;return false;"
                class="button_down">
        </div>
        <asp:Button ID="btnFind" runat="server" Text="Find your meeting room!" class="find-button" />
    </div>
    <!--left-form ENDS HERE-->
</div>
<%--<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        jquery('#<%=imgMapSearch.ClientID %>').hide();
    });
    function ShowMapDiv() {
        if (jquery('#<%=imgMapSearch.ClientID %>').hide() == true) {
            jquery('#<%=imgMapSearch.ClientID %>').show();
        }
        else{
            jquery('#<%=imgMapSearch.ClientID %>').show();
        }
    
    }
</script>
--%>