﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.IO;
using System.Text;
using System.Linq;
using System.Xml;
#endregion

public partial class UserControl_Frontend_TopMenu : BaseUserControl
{
    CurrencyManager cm = new CurrencyManager();
    LanguageManager lm = new LanguageManager();

    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            BindLanguage();
            BindCurrency();
            if (Request.Url.AbsoluteUri.ToLower().Contains("default.aspx"))
            {
                lnkBtnHome.CssClass = "select";
            }
            else if (Request.Url.AbsoluteUri.ToLower().Contains("aboutus.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("bookingadvantage.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("requestadvantage.aspx"))
            {
                lnkBtnAbout.CssClass = "select";

            }
            else if (Request.Url.AbsoluteUri.ToLower().Contains("contactus.aspx"))
            {
                lnkBtnContactUs.CssClass = "select";

            }
            else if (Request.Url.AbsoluteUri.ToLower().Contains("jointoday.aspx"))
            {
                lnkBtnJoinToday.CssClass = "select";
            }
            else if (Request.Url.AbsoluteUri.ToLower().Contains("login.aspx"))
            {
                lnkBtnLogin.CssClass = "select";
            }
            bindURls();
        }
        if (Request.Url.AbsoluteUri.ToLower().Contains("mapsearch.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("searchbooking.aspx"))
        {
            //showlocationdiv.Style.Add("display", "block");
            lnkBtnHome.CssClass = "select";
        }
        if (Session["CurrentRestUserID"] == null)
        {
            lnkBtnLogout.Visible = false;
            lnkBtnLogin.Visible = true;
        }
        else
        {
            lnkBtnLogout.Visible = true;
            lnkBtnLogin.Visible = false;
        }
    }

    public void BindLanguage()
    {
        DDLlanguage.DataTextField = "Name";
        DDLlanguage.DataValueField = "ID";
        DDLlanguage.DataSource = lm.GetData().Where(u => u.IsActive == true).ToList();//.Where(a=>a.Id ==1 )
        DDLlanguage.DataBind();
        if (Request.QueryString["wl"] != null)
        {
            if (DDLlanguage.Items.FindByText("English") != null)
            {
                Session.Remove("LanguageID");
                DDLlanguage.Items.FindByText("English").Selected = true;
                Session["LanguageID"] = DDLlanguage.SelectedValue;
            }
        }
        DDLlanguage.SelectedValue = Convert.ToString(Session["LanguageID"]);
        if (DDLlanguage.SelectedItem.Text.ToLower() == "english")
        {
            ltrtestscript.Text = @"<script language='javascript' type='text/javascript'>   
        var google_conversion_id = 1018259257;
        var google_conversion_language = 'en';
        var google_conversion_format = '3';
        var google_conversion_color = 'ffffff';
        var google_conversion_label = 'nZ0rCKej1QIQuc7F5QM';
        var google_conversion_value = 0;
        </script>
        <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
        </script>
        <noscript>
        <div style='display: inline;'>
        <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=nZ0rCKej1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
        </div>
        </noscript>";
        }
        else if (DDLlanguage.SelectedItem.Text.ToLower() == "french")
        {
            ltrtestscript.Text = @"<script language='javascript' type='text/javascript'>
         var google_conversion_id = 1018259257;
        var google_conversion_language = 'en';
        var google_conversion_format = '3';
        var google_conversion_color = 'ffffff';
        var google_conversion_label = 'XAV3CP-n1QIQuc7F5QM';
        var google_conversion_value = 0;
        </script>
        <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
        </script>
        <noscript>
        <div style='display: inline;'>
        <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=XAV3CP-n1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
        </div>
        </noscript>";
        }
        else if (DDLlanguage.SelectedItem.Text.ToLower() == "dutch")
        {
            ltrtestscript.Text = @"<script language='javascript' type='text/javascript'>
        var google_conversion_id = 1018259257;
        var google_conversion_language = 'en';
        var google_conversion_format = '3';
        var google_conversion_color = 'ffffff';
        var google_conversion_label = '-b9KCIen1QIQuc7F5QM';
        var google_conversion_value = 0;
        </script>
        <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
        </script>
        <noscript>
        <div style='display: inline;'>
        <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=-b9KCIen1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
        </div>
        </noscript>";
        }
        else
        {
            ltrtestscript.Text = @"<script language='javascript' type='text/javascript'>   
        var google_conversion_id = 1018259257;
        var google_conversion_language = 'en';
        var google_conversion_format = '3';
        var google_conversion_color = 'ffffff';
        var google_conversion_label = 'nZ0rCKej1QIQuc7F5QM';
        var google_conversion_value = 0;
        </script>
        <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
        </script>
        <noscript>
        <div style='display: inline;'>
        <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=nZ0rCKej1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
        </div>
        </noscript>";
        }

        if (Request.Url.AbsolutePath.Contains("SubscribeNewsOK.aspx") || Request.Url.AbsolutePath.Contains("BookingStep4.aspx") || Request.Url.AbsolutePath.Contains("RequestStep4.aspx"))
        {
            if (DDLlanguage.SelectedItem.Text.ToLower() == "english")
            {
                Literal1.Text = @"<script language='javascript' type='text/javascript'>
                    
                    var google_conversion_id = 1018259257;
                    var google_conversion_language = 'en';
                    var google_conversion_format = '3';
                    var google_conversion_color = 'ffffff';
                    var google_conversion_label = 'JqcrCJel1QIQuc7F5QM';
                    var google_conversion_value = 0;
                    </script>
                    <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
                    </script>
                    <noscript>
                    <div style='display: inline;'>
                    <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=JqcrCJel1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
                    </div>
                    </noscript>";
            }
            else if (DDLlanguage.SelectedItem.Text.ToLower() == "french")
            {
                Literal1.Text = @"<script language='javascript' type='text/javascript'>
                    
                    var google_conversion_id = 1018259257;
                    var google_conversion_language = 'en';
                    var google_conversion_format = '3';
                    var google_conversion_color = 'ffffff';
                    var google_conversion_label = 'p9XCCN-r1QIQuc7F5QM';
                    var google_conversion_value = 0;
                    </script>
                    <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
                    </script>
                    <noscript>
                    <div style='display: inline;'>
                    <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=p9XCCN-r1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
                    </div>
                    </noscript>";
            }
            else if (DDLlanguage.SelectedItem.Text.ToLower() == "dutch")
            {
                Literal1.Text = @"<script language='javascript' type='text/javascript'>
                    
                    var google_conversion_id = 1018259257;
                    var google_conversion_language = 'en';
                    var google_conversion_format = '3';
                    var google_conversion_color = 'ffffff';
                    var google_conversion_label = '2zV8CO-p1QIQuc7F5QM';
                    var google_conversion_value = 0;
                    </script>
                    <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
                    </script>
                    <noscript>
                    <div style='display: inline;'>
                    <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=2zV8CO-p1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
                    </div>
                    </noscript>";
            }


        }
    }

    public void bindURls()
    {
        lnkBtnHome.PostBackUrl = SiteRootPath + "default/" + DDLlanguage.SelectedItem.Text.ToLower();
        lnkBtnAbout.PostBackUrl = SiteRootPath + "about-us/" + DDLlanguage.SelectedItem.Text.ToLower();
        lnkBtnContactUs.PostBackUrl = SiteRootPath + "contact-us/" + DDLlanguage.SelectedItem.Text.ToLower();
        lnkBtnJoinToday.PostBackUrl = SiteRootPath + "join-today/" + DDLlanguage.SelectedItem.Text.ToLower();
        lnkBtnLogin.PostBackUrl = SiteRootPath + "login/" + DDLlanguage.SelectedItem.Text.ToLower();
    }

    public void BindCurrency()
    {
        drpCurrency.DataTextField = "Currency";
        drpCurrency.DataValueField = "Id";
        drpCurrency.DataSource = cm.GetAllCurrency();
        drpCurrency.DataBind();
        if (Session["CurrencyID"] == null)
        {
            Session["CurrencyID"] = drpCurrency.Items[1].Value;
            drpCurrency.SelectedIndex = drpCurrency.Items.IndexOf(drpCurrency.Items[1]);
        }
        if (Request.QueryString["wl"] != null)
        {
            Session.Remove("CurrencyID");
            Session["CurrencyID"] = drpCurrency.Items[1].Value;
        }
        drpCurrency.SelectedValue = Convert.ToString(Session["CurrencyID"]);
    }
    /// <summary>
    /// this events is used for index changing of dropdown list.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DDLlanguage_SelectedIndexChanged(object sender, EventArgs e)
    {

        Language l = lm.GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        //Session["LanguageID"] = DDLlanguage.SelectedValue;
        //Session["Language"] = l;
        string urlbind = Request.RawUrl;
        if (Request.QueryString["wl"] == null)
        {
            if (l != null)
            {
                urlbind = urlbind.Replace(l.Name.ToLower(), DDLlanguage.SelectedItem.Text.ToLower());
            }
            else
            {
                urlbind = urlbind.Replace("english", DDLlanguage.SelectedItem.Text.ToLower());
            }
        }
        else
        {
            if (DDLlanguage.Items.FindByText("English") != null)
            {
                Session.Remove("LanguageID");
                DDLlanguage.Items.FindByText("English").Selected = true;
                Session["LanguageID"] = DDLlanguage.SelectedValue;
            }
            //Session["LanguageID"] = DDLlanguage.SelectedValue;
        }
        Session["PostBack"] = null;
        Response.Redirect(urlbind);
    }
    ///// <summary>
    ///// This method is taking all the data of language table
    ///// </summary>
    ///// <returns></returns>
    //public TList<Language> GetData()
    //{
    //    TList<Language> objlanguage = DataRepository.LanguageProvider.GetAll();
    //    return objlanguage;
    //}
    /// <summary>
    /// This method is passing the key value
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    //This is used for language conversion for static contants.

    protected void lnkBtnLogout_Click(object sender, EventArgs e)
    {
        Session.Remove("CurrentRestUserID");
        Session.Remove("CurrentRestUser");
        SearchTracer st = null;
        Createbooking objBooking = null;
        BookingManager bm = new BookingManager();
        if (objBooking == null)
        {
            if (Session["SerachID"] == null)
            {
                objBooking = Session["Search"] as Createbooking;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
        }
        if (objBooking != null)
        {
            bm.ResetIsReserverFalse(objBooking);
        }
        Session.Remove("Search");
        Session.Remove("SerachID");
        Session.Remove("Request");
        Session.Remove("RequestID");
        Session.Remove("CurrentUser");
        Session.Remove("masterInput");
        //Response.Redirect(Request.RawUrl);
        //Response.Redirect("Login.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
    }
    protected void drpCurrency_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Request.QueryString["wl"] == null)
        {
            Session["CurrencyID"] = drpCurrency.SelectedValue;
            Session["PostBack"] = null;
            Response.Redirect(Request.RawUrl);
        }
    }
}