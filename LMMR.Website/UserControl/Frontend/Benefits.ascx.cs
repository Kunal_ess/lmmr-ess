﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Linq;
#endregion

public partial class UserControl_Frontend_Benefits : BaseUserControl
{

    #region Variables and Properties
    public int intTypeCMSid;
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    
    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindBenefits();
        }
    }
    #endregion

    #region Functions
    /// <summary>
    /// This method bind the benefits with Datalist
    /// </summary>
    void BindBenefits()
    {
        Cms cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "benifits");
        if (cmsObj != null)
        {
            DataLsticons.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true );
            DataLsticons.DataBind();
            if (DataLsticons.Items.Count <= 0)
            {
                DataLsticons.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64("1") && a.IsActive == true);
                DataLsticons.DataBind();
            }
        }
        else
        {
            Cms cmsObj2 = manager.getCmsEntityOnType("Benifits");
            if (cmsObj2 != null)
            {
                DataLsticons.DataSource = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"]));
                DataLsticons.DataBind();
                if (DataLsticons.Items.Count <= 0)
                {
                    DataLsticons.DataSource = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1"));
                    DataLsticons.DataBind();
                }
            }
        }
    }
    #endregion

}