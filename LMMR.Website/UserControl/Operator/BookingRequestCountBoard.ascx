﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BookingRequestCountBoard.ascx.cs"
    Inherits="UserControl_Operator_BookingRequestCountBoard" %>
<table width="100%" border="0" cellpadding="5" bgcolor="#FFFFFF">
    <tr bgcolor="#CCD8D8">
        <td width="50%">
            <b>Booking</b>
        </td>
        <td width="50%">
            <b>Requests</b>
        </td>
    </tr>
    <tr bgcolor="#fff">
        <td>
            <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#98BCD6">
                <tr bgcolor="#fff">
                    <td>
                        Today booking :
                    </td>
                    <td>
                        <asp:Label ID="lblTodayBooking" runat="server"></asp:Label>
                    </td>
                    <td>
                        <a href="#">view</a>
                    </td>
                    <td>
                        <a href="chart.aspx?type=dy&bk=0">graph</a>
                    </td>
                </tr>
                <tr bgcolor="#E3F0F1">
                    <td>
                        Current month :
                    </td>
                    <td>
                        <asp:Label ID="lblCurrentMonthBooking" runat="server"></asp:Label>
                    </td>
                    <td>
                        <a href="#">view</a>
                    </td>
                    <td>
                        <a href="chart.aspx?type=mn&bk=0">graph</a>
                    </td>
                </tr>
                <tr bgcolor="#fff">
                    <td>
                        Current year :
                    </td>
                    <td>
                        <asp:Label ID="lblCurrentYearBooking" runat="server"></asp:Label>
                    </td>
                    <td>
                        <a href="#">view</a>
                    </td>
                    <td>
                        <a href="chart.aspx?type=yr&bk=0">graph</a>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#98BCD6">
                <tr bgcolor="#fff">
                    <td>
                        Today requests :
                    </td>
                    <td>
                        <asp:Label ID="lblTodayRequest" runat="server"></asp:Label>
                    </td>
                    <td>
                        <a href="#">view</a>
                    </td>
                    <td>
                        <a href="chart.aspx?type=dy&bk=1">graph</a>
                    </td>
                </tr>
                <tr bgcolor="#E3F0F1">
                    <td>
                        Send offer/tentative :
                    </td>
                    <td>
                        <asp:Label ID="lblTotalTentativeRequest" runat="server"></asp:Label>
                    </td>
                    <td>
                        <a href="#">view</a>
                    </td>
                    <td>
                        <a href="chart.aspx?type=mn&bk=1">graph</a>
                    </td>
                </tr>
                <tr bgcolor="#fff">
                    <td>
                        No action :
                    </td>
                    <td>
                        <asp:Label ID="lblToNoactionRequest" runat="server"></asp:Label>
                    </td>
                    <td>
                        <a href="#">view</a>
                    </td>
                    <td>
                        <a href="chart.aspx?type=yr&bk=1">graph</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
