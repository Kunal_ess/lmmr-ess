﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenu.ascx.cs" Inherits="UserControl_Sales_TopMenu" %>
<!--Main navigation start -->
<ul id="subnav">
    <li class="cat-item">
        <asp:LinkButton ID="hyperlink2" runat="server" PostBackUrl="~/Operator/ClientContract.aspx">Clients-Contracts</asp:LinkButton></li>
    <li class="cat-item">
        <asp:LinkButton ID="hyperlink3" runat="server" PostBackUrl="~/Operator/SearchDetails.aspx">Search-Booking</asp:LinkButton>
    </li>
    <li class="cat-item">
        <asp:LinkButton ID="hyperlink5" runat="server" PostBackUrl="~/Operator/RequestSearchDetails.aspx">Search-Request</asp:LinkButton>
    </li>
    <li class="cat-item">
        <asp:LinkButton ID="hyperlink4" runat="server" PostBackUrl="~/SuperAdmin/ReportManager.aspx">Reports</asp:LinkButton>
    </li>
</ul>
