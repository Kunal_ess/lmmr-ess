﻿jQuery(document).ready(function () {
    jQuery(".error").hide()
    jQuery("#adjust-meeting #contentbody_txtFrom").attr("disabled", true)
    jQuery("#adjust-meeting #contentbody_txtTo").attr("disabled", true)
    jQuery("#adjust-bedroom #contentbody_txtBRFrom").attr("disabled", true)
    jQuery("#adjust-bedroom #contentbody_txtBRTo").attr("disabled", true)
    jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:text').attr("disabled", true)
    jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:text').attr("disabled", true)
    jQuery("#adjust-meeting .adjust-meeting-form-body-box2 #contentbody_chkAll").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:text').attr("disabled", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("checked", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:text').attr("value", "0")
            jQuery(this).attr("disabled", false)
            jQuery(this).attr("checked", true)
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3 #contentbody_txtAll").attr("disabled", false)
        }
        else {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", false)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:text').attr("disabled", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("checked", false)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:text').attr("value", "0")
        }
    })
    jQuery("#adjust-bedroom .adjust-meeting-form-body-box2 #contentbody_chkBRAll").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:text').attr("disabled", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("checked", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:text').attr("value", "0")
            jQuery(this).attr("disabled", false)
            jQuery(this).attr("checked", true)
            jQuery("#adjust-bedroom #contentbody_txtBRALL").attr("disabled", false)
        }
        else {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", false)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:text').attr("disabled", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("checked", false)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:text').attr("value", "0")
        }
    })
    jQuery("#adjust-meeting .adjust-meeting-form-body-box2 #contentbody_chkAll").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", true)
            jQuery(this).attr("disabled", false)
        }
        else {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", false)
        }
    })
    jQuery("#adjust-bedroom .adjust-meeting-form-body-box2 #contentbody_chkBRAll").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", true)
            jQuery(this).attr("disabled", false)
        }
        else {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", false)
        }
    })
    jQuery("#adjust-meeting #contentbody_chkSunday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #contentbody_txtSunday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #contentbody_txtSunday").attr("disabled", true)
            jQuery("#adjust-meeting #contentbody_txtSunday").attr("value", "0")
        }
    })
    jQuery("#adjust-bedroom #contentbody_chkBRSunday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #contentbody_txtBRSunday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #contentbody_txtBRSunday").attr("disabled", true)
            jQuery("#adjust-bedroom #contentbody_txtBRSunday").attr("value", "0")
        }
    })
    jQuery("#adjust-meeting #contentbody_chkMonday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #contentbody_txtMonday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #contentbody_txtMonday").attr("disabled", true)
            jQuery("#adjust-meeting #contentbody_txtMonday").attr("value", "0")
        }
    })
    jQuery("#adjust-bedroom #contentbody_chkBRMonday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #contentbody_txtBRMonday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #contentbody_txtBRMonday").attr("disabled", true)
            jQuery("#adjust-bedroom #contentbody_txtBRMonday").attr("value", "0")
        }
    })
    jQuery("#adjust-meeting #contentbody_chkTuesday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #contentbody_txtTuesday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #contentbody_txtTuesday").attr("disabled", true)
            jQuery("#adjust-meeting #contentbody_txtTuesday").attr("value", "0")
        }
    })
    jQuery("#adjust-bedroom #contentbody_chkBRTuesday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #contentbody_txtBRTuesday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #contentbody_txtBRTuesday").attr("disabled", true)
            jQuery("#adjust-bedroom #contentbody_txtBRTuesday").attr("value", "0")
        }
    })
    jQuery("#adjust-meeting #contentbody_chkWednesday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #contentbody_txtWednesday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #contentbody_txtWednesday").attr("disabled", true)
            jQuery("#adjust-meeting #contentbody_txtWednesday").attr("value", "0")
        }
    })
    jQuery("#adjust-bedroom #contentbody_chkBRWednesday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #contentbody_txtBRWednesday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #contentbody_txtBRWednesday").attr("disabled", true)
            jQuery("#adjust-bedroom #contentbody_txtBRWednesday").attr("value", "0")
        }
    })
    jQuery("#adjust-meeting #contentbody_chkThursday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #contentbody_txtThursday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #contentbody_txtThursday").attr("disabled", true)
            jQuery("#adjust-meeting #contentbody_txtThursday").attr("value", "0")
        }
    })
    jQuery("#adjust-bedroom #contentbody_chkBRThursday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #contentbody_txtBRThursday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #contentbody_txtBRThursday").attr("disabled", true)
            jQuery("#adjust-bedroom #contentbody_txtBRThursday").attr("value", "0")
        }
    })
    jQuery("#adjust-meeting #contentbody_chkFriday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #contentbody_txtFriday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #contentbody_txtFriday").attr("disabled", true)
            jQuery("#adjust-meeting #contentbody_txtFriday").attr("value", "0")
        }
    })
    jQuery("#adjust-bedroom #contentbody_chkBRFriday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #contentbody_txtBRFriday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #contentbody_txtBRFriday").attr("disabled", true)
            jQuery("#adjust-bedroom #contentbody_txtBRFriday").attr("value", "0")
        }
    })
    jQuery("#adjust-meeting #contentbody_chkSaturday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #contentbody_txtSaturday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #contentbody_txtSaturday").attr("disabled", true)
            jQuery("#adjust-meeting #contentbody_txtSaturday").attr("value", "0")
        }
    })
    jQuery("#adjust-bedroom #contentbody_chkBRSaturday").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #contentbody_txtBRSaturday").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #contentbody_txtBRSaturday").attr("disabled", true)
            jQuery("#adjust-bedroom #contentbody_txtBRSaturday").attr("value", "0")
        }
    })
    jQuery("#adjust-meeting .cancelbtn").click(function () {
        jQuery("#adjust-meeting .error").hide()
        jQuery("#adjust-meeting #contentbody_txtFrom").val("dd/mm/yy")
        jQuery("#adjust-meeting #contentbody_txtTo").val("dd/mm/yy")
        jQuery("#adjust-meeting .adjust-meeting-form-body-box2right input:checkbox").attr("checked", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box2right input:checkbox").attr("disabled", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box3left input:text").attr("value", "0")
        jQuery("#adjust-meeting .adjust-meeting-form-body-box3left input:text").attr("disabled", true)
    })
    jQuery("#adjust-bedroom .cancelbtn").click(function () {
        jQuery("#adjust-bedroom .error").hide()
        jQuery("#adjust-bedroom #contentbody_txtBRFrom").val("dd/mm/yy")
        jQuery("#adjust-bedroom #contentbody_txtBRTo").val("dd/mm/yy")
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box2right input:checkbox").attr("checked", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box2right input:checkbox").attr("disabled", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left input:text").attr("value", "0")
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left input:text").attr("disabled", true)
    })
    jQuery("#contentbody_ancSubmit").bind("click", function () {
        if (jQuery("#contentbody_txtFrom").val() == "dd/mm/yy" || jQuery("#contentbody_txtFrom").val() == null || jQuery("#contentbody_txtFrom").val() == "") {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("Please select From date.")
            return false
        }
        if (jQuery("#contentbody_txtTo").val() == "dd/mm/yy" || jQuery("#contentbody_txtTo").val() == null || jQuery("#contentbody_txtTo").val() == "") {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("Please select To date.")
            return false
        }
        if (jQuery("#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox:checked").length == 0) {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("Please select atleast one day.")
            return false
        }
        var fromdate = jQuery("#contentbody_txtFrom").val()
        var todate = jQuery("#contentbody_txtTo").val()
        var todayArr = todate.split('/')
        var formdayArr = fromdate.split('/')
        var fromdatecheck = new Date()
        var todatecheck = new Date()
        fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0])
        todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0])
        if (fromdatecheck > todatecheck) {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("From date must be less than To date.")
            return false
        }
        if (jQuery("#adjust-meeting .adjust-meeting-form-body-box3 input:text:not(:disabled)").length > 0) {
            var validvalue = true
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3 input:text:not(:disabled)").each(function () {
                if (!jQuery(this).is("disabled")) {
                    var myVal = jQuery(this).val()
                    if ((parseInt(myVal, 10) > parseInt(jQuery("#contentbody_hdnMaxPercentage").val(), 10) || parseInt(myVal, 10) < parseInt(jQuery("#contentbody_hdnMinPercentage").val(), 10))) {
                        jQuery("#adjust-meeting .error").show()
                        jQuery("#adjust-meeting .error").html("Please enter value between " + parseInt(jQuery("#contentbody_hdnMinPercentage").val(), 10) + " and " + parseInt(jQuery("#contentbody_hdnMaxPercentage").val(), 10) + ".")
                        validvalue = false
                        return false
                    }
                    else if (isNaN(myVal)) {
                        jQuery("#adjust-meeting .error").show()
                        jQuery("#adjust-meeting .error").html("Please enter value between " + parseInt(jQuery("#contentbody_hdnMinPercentage").val(), 10) + " and " + parseInt(jQuery("#contentbody_hdnMaxPercentage").val(), 10) + ".")
                        validvalue = false
                        return false
                    } 
                }
            })
            if (!validvalue) {
                jQuery("#adjust-meeting .error").show()
                jQuery("#adjust-meeting .error").html("Please enter value between " + parseInt(jQuery("#contentbody_hdnMinPercentage").val(), 10) + " and " + parseInt(jQuery("#contentbody_hdnMaxPercentage").val(), 10) + ".")
                return false
            } 
        }
        jQuery("#adjust-meeting .error").html("")
        jQuery("#adjust-meeting .error").hide()
        jQuery("#adjust-meeting #contentbody_txtFrom").attr("disabled", false)
        jQuery("#adjust-meeting #contentbody_txtTo").attr("disabled", false)
    })
    jQuery("#contentbody_ancBRSubmit").bind("click", function () {
        if (jQuery("#contentbody_txtBRFrom").val() == "dd/mm/yy" || jQuery("#contentbody_txtBRFrom").val() == null || jQuery("#contentbody_txtBRFrom").val() == "") {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("Please select From date.")
            return false
        }
        if (jQuery("#contentbody_txtBRTo").val() == "dd/mm/yy" || jQuery("#contentbody_txtBRTo").val() == null || jQuery("#contentbody_txtBRTo").val() == "") {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("Please select To date.")
            return false
        }
        if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox:checked").length == 0) {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("Please select atleast one day.")
            return false
        }
        var fromdate = jQuery("#contentbody_txtBRFrom").val()
        var todate = jQuery("#contentbody_txtBRTo").val()
        var todayArr = todate.split('/')
        var formdayArr = fromdate.split('/')
        var fromdatecheck = new Date()
        var todatecheck = new Date()
        fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0])
        todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0])
        if (fromdatecheck > todatecheck) {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("From date must be less than To date.")
            return false
        }
        if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box3 input:text:not(:disabled)").length > 0) {
            var validvalue = true
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3 input:text:not(:disabled)").each(function () {
                if (!jQuery(this).is("disabled")) {
                    var myVal = jQuery(this).val()
                    if (parseFloat(myVal) < 0 || (parseFloat(myVal) > 1000)) {
                        jQuery("#adjust-bedroom .error").show()
                        jQuery("#adjust-bedroom .error").html("Please enter value greater then equal to 0.")
                        validvalue = false
                        return false
                    }
                    else if (isNaN(myVal)) {
                        jQuery("#adjust-bedroom .error").show()
                        jQuery("#adjust-bedroom .error").html("Please enter value greater then equal to 0.")
                        validvalue = false
                        return false
                    } 
                }
            })
            if (!validvalue) {
                jQuery("#adjust-bedroom .error").show()
                jQuery("#adjust-bedroom .error").html("Please enter value greater then equal to 0.")
                return false
            } 
        }
        jQuery("#adjust-bedroom .error").html("")
        jQuery("#adjust-bedroom .error").hide()
        jQuery("#adjust-bedroom #contentbody_txtBRFrom").attr("disabled", false)
        jQuery("#adjust-bedroom #contentbody_txtBRTo").attr("disabled", false)
    })
})
