﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Policy.aspx.cs" Inherits="Policy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Last minute Meeting Room</title>
    <style>
        .superadmin-mainbody-inner
        {
            width: 674px;
            float: right;
            height: auto;
            font-size:12px;
            font-family:Arial;
            overflow: hidden;
        }
        .superadmin-mainbody-inner h1
        {
            color: #444444;
            font: normal 26px Arial, Helvetica, sans-serif;
            margin: 0px;
            padding: 0px;
            float: left;
        }
        /*sanjay css for page Super admin cms */
        .cancelation-tabbody1
        {
            width: 674px;
            border-top: #a3d4f7 solid 1px;
            height: auto;
            background: #FFF;
            overflow: hidden;
        }
        
        /*css for tabs start here */
        .cancelation-tabbody1 .TabbedPanels
        {
            overflow: hidden;
            margin: 0px;
            padding: 0px;
            clear: none;
            width: 100%; /* IE Hack to force proper layout when preceded by a paragraph. (hasLayout Bug)*/
        }
        
        
        .cancelation-tabbody1 .TabbedPanelsTabGroup
        {
            margin: 0px 0px 0px 0px;
            padding: 0px;
        }
        
        
        .cancelation-tabbody1 .TabbedPanelsTab
        {
            position: relative;
            top: 0px !important;
            float: left;
            margin: 0px 0px 0px 0px !important;
            background: url("../images/ss") no-repeat scroll -1px 0 transparent;
            list-style: none;
            padding: 8px 0px 0px 0px !important;
            -moz-user-select: none;
            -khtml-user-select: none;
            cursor: pointer;
            font-weight: bold;
            color: #444444;
            width: 29px;
            height: 21px;
            text-align: center;
        }
        
        
        .cancelation-tabbody1 .TabbedPanelsTabHover
        {
            background: url("../images/superadmin-lag-bg.png") no-repeat scroll -1px 0 transparent;
            color: #fff;
        }
        
        
        .cancelation-tabbody1 .TabbedPanelsTabSelected
        {
            background: url("../images/superadmin-lag-bg.png") no-repeat scroll -1px 0 transparent;
            color: #fff;
            text-align: center;
        }
        
        
        
        
        
        .cancelation-tabbody1 .TabbedPanelsContentGroup
        {
            clear: both;
            float: left;
            overflow: hidden;
            height: auto;
            padding-left:10px;
            border: #0377c8 solid 1px;
            width: 660px;
        }
        
        
        .cancelation-tabbody1 .TabbedPanelsContent
        {
            overflow: hidden;
        }
        
        
        .cancelation-tabbody1 .TabbedPanelsContentVisible
        {
        }
        
        
        
        .cancelation-tabbody1 .VTabbedPanels
        {
            overflow: hidden;
            zoom: 1;
        }
        
        
        .cancelation-tabbody1 .VTabbedPanels .TabbedPanelsTabGroup
        {
            float: left;
            width: 10em;
            height: 20em;
            background-color: #fff;
            position: relative;
            border: solid 1px #999;
        }
        
        
        .cancelation-tabbody1 .VTabbedPanels .TabbedPanelsTab
        {
            float: none;
            margin: 0px;
            border-top: none;
            border-left: none;
            border-right: none;
        }
        
        
        .cancelation-tabbody1 .VTabbedPanels .TabbedPanelsTabSelected
        {
            background-color: #e3f0f1;
        }
        
        
        .cancelation-tabbody1 .VTabbedPanels .TabbedPanelsContentGroup
        {
            clear: none;
            float: left;
            padding: 0px;
            width: 30em;
            height: 20em;
        }
        .superadmin-mainbody-sub4
        {
            width: 670px;
            border-top: #a3d4f7 solid 1px;
            padding: 5px 0px 5px 10px;
            background-color: #FFF;
            line-height: 30px;
            overflow: hidden;
        }
        .superadmin-mainbody-sub4-left
        {
            float: left;
            width: 670px;
            padding-bottom: 10px;
        }
        .superadmin-mainbody-sub4-right
        {
            float: right;
            width: 300px;
            text-align: right;
        }
        .superadmin-mainbody-sub4-right a
        {
            color: #1B7AC2;
            text-decoration: underline;
            padding-right: 10px;
        }
        .superadmin-mainbody-sub4-right a:hover
        {
            color: #1B7AC2;
            text-decoration: none;
        }
        .inputboxsmall
        {
            background: url("../images/qty-input-box.png") no-repeat scroll right top transparent;
            border: medium none;
            height: 26px;
            min-height: 25px;
            text-align: center;
            width: 44px;
        }
        .superadmin-mainbody-sub5
        {
            width: 670px;
            border-top: #a3d4f7 solid 1px;
            padding: 5px 0px 5px 10px;
            background-color: #FFF;
            line-height: 30px;
            overflow: hidden;
            padding-left: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table width="650px;">
        <tr>
            <td>
                <div class="superadmin-mainbody-inner" id="divPolicy" runat="server">
                    <div class="superadmin-mainbody-sub4">
                        Policy Name : <b>
                            <asp:Label ID="lblName" runat="server" Text=""></asp:Label></b>
                    </div>
                    <div class="superadmin-mainbody-sub4">
                        <div class="superadmin-mainbody-sub4-left">
                            User can cancel a booking : <b>
                                <asp:Label ID="lblCalcellationLimit" runat="server" Text=""></asp:Label></b>
                            days before arrival
                        </div>
                    </div>
                    <div class="superadmin-mainbody-sub4">
                        <b>Section 1</b> Between day <b>
                            <asp:Label ID="lblTimeFrameMax1" runat="server" Text=""></asp:Label></b> and
                        day <b>
                            <asp:Label ID="lblTimeFrameMin1" runat="server" Text=""></asp:Label></b>
                    </div>
                    <div class="cancelation-tabbody1">
                        <div id="TabbedPanels2" class="TabbedPanels">
                            <div class="TabbedPanelsContentGroup">
                                <div class="TabbedPanelsContent">
                                    <div class="tab-textarea1">
                                        <asp:Label ID="lblSection1" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="superadmin-mainbody-sub4">
                        <b>Section 2</b> Between day <b>
                            <asp:Label ID="lblTimeFrameMax2" runat="server" Text=""></asp:Label></b> and
                        day <b>
                            <asp:Label ID="lblTimeFrameMin2" runat="server" Text=""></asp:Label></b>
                    </div>
                    <div class="cancelation-tabbody1">
                        <div id="TabbedPanels3" class="TabbedPanels">
                            <div class="TabbedPanelsContentGroup">
                                <div class="TabbedPanelsContent">
                                    <div class="tab-textarea1">
                                        <asp:Label ID="lblSection2" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="superadmin-mainbody-sub4">
                        <b>Section 3</b> Between day <b>
                            <asp:Label ID="lblTimeFrameMax3" runat="server" Text=""></asp:Label></b> and
                        day <b>
                            <asp:Label ID="lblTimeFrameMin3" runat="server" Text=""></asp:Label></b>
                    </div>
                    <div class="cancelation-tabbody1">
                        <div id="TabbedPanels4" class="TabbedPanels">
                            <div class="TabbedPanelsContentGroup">
                                <div class="TabbedPanelsContent">
                                    <div class="tab-textarea1">
                                        <asp:Label ID="lblSection3" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="superadmin-mainbody-sub4">
                        <b>Section 4</b> Between day <b>
                            <asp:Label ID="lblTimeFrameMax4" runat="server" Text=""></asp:Label></b> and
                        day <b>
                            <asp:Label ID="lblTimeFrameMin4" runat="server" Text=""></asp:Label></b>
                    </div>
                    <div class="cancelation-tabbody1">
                        <div id="Div1" class="TabbedPanels">
                            <div class="TabbedPanelsContentGroup">
                                <div class="TabbedPanelsContent">
                                    <div class="tab-textarea1">
                                        <asp:Label ID="lblSection4" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <%--<div style="padding-left: 10px;" runat="server" id="divMessage">
                            In case you want to change or update the cancellation & change policy, please contact
                            our Support team at <a href="mailto:hotelsupport@lastminutemeetingroom.com">hotelsupport@lastminutemeetingroom.com</a>
                            <br />
                        </div>--%>
                        <br />
                    </div>
                </div>
            </td>
        </tr>
    </table>
    </form>

</body>
</html>
