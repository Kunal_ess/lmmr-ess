﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class MapSearch : System.Web.UI.Page
{

    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    
    public int intTypeCMSid;
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetReadMoreLeft();
            GetReadMoreRight();
        }
    }

    /// <summary>
    /// This function used for get read more file and desc of leftside.
    /// </summary>
    void GetReadMoreLeft()
    {
        intTypeCMSid = 19;
        TList<CmsDescription> objreadmoreLeft = objManageCMSContent.GetCMSContent(intTypeCMSid, Convert.ToInt64(Session["LanguageID"]));
        if (objreadmoreLeft.Count > 0)
        {
            lblReadMoreLeftDesc.Text = objreadmoreLeft[0].ContentsDesc;
            hypReadMoreLeft.NavigateUrl = "Uploads/ReadMore/" + objreadmoreLeft[0].ContentsDesc;
        }
    }

    /// <summary>
    /// This function used for get read more file and desc of rightside.
    /// </summary>
    void GetReadMoreRight()
    {
        //intTypeCMSid = 20;
        //TList<CmsDescription> objreadmoreRight = objManageCMSContent.GetCMSContent(intTypeCMSid, Convert.ToInt64(Session["LanguageID"]));
        //if (objreadmoreRight.Count > 0)
        //{
        //    lblReadMoreRightDesc.Text = objreadmoreRight[0].ContentsDesc;
        //    hypReadMoreRight.NavigateUrl = "Uploads/ReadMore/" + objreadmoreRight[0].ContentsDesc;
        //}
        Cms cmsObj = (Session["CMS"] as TList<Cms>).Find(a => a.CmsType.ToLower() == "readmoreright");
        if (cmsObj != null)
        {
            TList<CmsDescription> objreadmoreRight = (Session["CMSDESCRIPTION"] as TList<CmsDescription>).FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]));
            if (objreadmoreRight != null)
            {
                if (objreadmoreRight.Count <= 0)
                {
                    lblReadMoreRightDesc.Text = objreadmoreRight[0].ContentsDesc;
                }
            }
        }
        else
        {
            Cms cmsObj2 = new SuperAdminTaskManager().getCmsEntityOnType("ReadMoreRight");
            if (cmsObj != null)
            {
                TList<CmsDescription> objreadmoreRight = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"]));
                if (objreadmoreRight != null)
                {
                    if (objreadmoreRight.Count <= 0)
                    {
                        lblReadMoreRightDesc.Text = objreadmoreRight[0].ContentsDesc;
                    }
                }
            }
        }
    }

    /// <summary>
    /// This method is passing the key value
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
}