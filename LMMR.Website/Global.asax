﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="LMMR.Business" %>
<%@ Import Namespace="System.Timers" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.IO.Compression" %>
<script RunAt="server">
    protected void Application_PreSendRequestHeaders()
    {
        // ensure that if GZip/Deflate Encoding is applied that headers are set
        // also works when error occurs if filters are still active
        try
        {
            if (HttpContext.Current != null)
            {
                HttpResponse response = HttpContext.Current.Response;
                if (response.Filter is GZipStream && response.Headers["Content-encoding"] != "gzip")
                    response.AppendHeader("Content-encoding", "gzip");
                else if (response.Filter is DeflateStream && response.Headers["Content-encoding"] != "deflate")
                    response.AppendHeader("Content-encoding", "deflate");
            }
        }
        catch (Exception ex)
        {
        }
    }
    void Application_PreRequestHandlerExecute(object sender, EventArgs e)
    {
        try
        {
            HttpApplication app = sender as HttpApplication;
            string acceptEncoding = app.Request.Headers["Accept-Encoding"];
            Stream prevUncompressedStream = app.Response.Filter;

            if (!(app.Context.CurrentHandler is Page ||
                app.Context.CurrentHandler.GetType().Name == "SyncSessionlessHandler") ||
                app.Request["HTTP_X_MICROSOFTAJAX"] != null)
                return;

            if (acceptEncoding == null || acceptEncoding.Length == 0)
                return;

            acceptEncoding = acceptEncoding.ToLower();

            if (acceptEncoding.Contains("deflate") || acceptEncoding == "*")
            {
                // defalte
                app.Response.Filter = new DeflateStream(prevUncompressedStream,
                    CompressionMode.Compress);
                app.Response.AppendHeader("Content-Encoding", "deflate");
            }
            else if (acceptEncoding.Contains("gzip"))
            {
                // gzip
                app.Response.Filter = new GZipStream(prevUncompressedStream,
                    CompressionMode.Compress);
                app.Response.AppendHeader("Content-Encoding", "gzip");
            }
        }
        catch (Exception ex)
        {
        }
    }
    void Application_Start(object sender, EventArgs e)
    {
        //System.GC.Collect();
        // Code that runs on application startup
        log4net.Config.DOMConfigurator.Configure();
        // Code that runs on application startup
        System.Timers.Timer myTimer = new System.Timers.Timer();
        // Set the Interval to 10 seconds (10000 milliseconds).
        myTimer.Interval = 1200000;

        myTimer.AutoReset = true;
        myTimer.Elapsed += new ElapsedEventHandler(myTimer_Elapsed);
        myTimer.Enabled = true;

    }
    
    public void myTimer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
    {
        HotelManager hm = new HotelManager();
        hm.AddUpdateNewSpandP();
        BookingManager bm = new BookingManager();
        bm.CheckBookingComplete();
    }


    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    void Application_BeginRequest(object sender, EventArgs e)
    {
        HttpApplication app = sender as HttpApplication;
        string newUrl;
        if (!string.IsNullOrEmpty(HttpContext.Current.Request.ApplicationPath) && HttpContext.Current.Request.ApplicationPath != "/")
        {
            newUrl = GetActualPath(app.Request.Path.ToLower().Replace(HttpContext.Current.Request.ApplicationPath.ToLower() + "/", ""));
        }
        else
        {
            string urls = app.Request.Path.ToLower().StartsWith("/") ? app.Request.Path.ToLower().Substring(1) : app.Request.Path.ToLower();
            newUrl = GetActualPath(urls);
        }
        
        if (!string.IsNullOrEmpty(newUrl))
        {
            app.Context.RewritePath("~/" + newUrl, false);
            return;
        }
    }
    
    void Application_EndRequest(object sender, EventArgs e)
    {
        //System.GC.Collect();  
    }
    
    string GetActualPath(string currentURL)
    {
        string Actualpath = string.Empty;
        if (!currentURL.ToLower().Contains(".aspx") && !currentURL.ToLower().EndsWith("uploads/temp/") && !currentURL.ToLower().Contains(".axd") && !currentURL.ToLower().Contains(".bmp") && !currentURL.ToLower().Contains(".css") && !currentURL.ToLower().Contains(".js") && !currentURL.ToLower().Contains(".jpg") && !currentURL.ToLower().Contains(".png") && !currentURL.ToLower().Contains(".gif") && !currentURL.ToLower().Contains(".jpeg"))
        {
            string[] currentURlArray = currentURL.Split('/');
            
            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "\\UrlRewriting.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(FilePath);
            XmlNode nodes = doc.SelectSingleNode("pages/page[@url='" + currentURlArray[currentURlArray.Length - 1] + "']");
            if (nodes != null)
            {
                Actualpath = nodes.InnerText;
            }
            else
            {
                if (currentURlArray.Length > 0)
                {
                    nodes = doc.SelectSingleNode("pages/page[@url='" + currentURlArray[0] + "']");
                    if (nodes != null)
                    {
                        Actualpath = nodes.InnerText;
                    }
                }
            }
            doc = null;
        }
        return Actualpath;
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
        //System.GC.Collect();
    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        Exception ex = HttpContext.Current.Server.GetLastError();
        if (ex != null)
        {
            if (ex.GetType() != typeof(HttpException) && ex.GetType() == typeof(HttpUnhandledException) && ex.GetType() != typeof(System.Threading.ThreadAbortException) && ex.GetType() != typeof(System.IO.FileNotFoundException))
            {
                try
                {
                    //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    HttpContext.Current.Server.ClearError();
                    //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex, HttpContext.Current));
                }
                catch
                {                    
                }
                
            }
        }
        
    }

    /// <summary>
    /// Class is called only on the first request
    /// </summary>
    private class AppStart
    {
        static bool _init = false;
        private static Object _lock = new Object();

        /// <summary>
        /// Does nothing after first request
        /// </summary>
        /// <param name="context"></param>
        public static void Start(HttpContext context)
        {
            if (_init)
            {
                return;
            }
            //create class level lock in case multiple sessions start simultaneously
            lock (_lock)
            {
                if (!_init)
                {
                    UriBuilder builder = new UriBuilder();
                    //HttpRuntime.AppDomainAppVirtualPath 
                    builder.Scheme = "http";
                    builder.Host = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
                    builder.Port = int.Parse(System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"]);
                    string path = System.Web.HttpContext.Current.Request.ApplicationPath;
                    if (path != "/")
                        path += "/";
                    //path = builder.ToString().Remove(builder.ToString().Length -1) +  path;
                    //LMMREnvironment.ApplicationPathRelativeRoot = path.Trim();
                    //LMMREnvironment.ApplicationPathRelativeRootWithHost = builder.ToString().Remove(builder.ToString().Length - 1) + path;
                    //HttpRuntime.Cache.Insert("basePath", LMMREnvironment.ApplicationPathRelativeRootWithHost);
                }
            }
        }
    }

    protected void Session_Start(object sender, EventArgs e)
    {
        //initializes Cache on first request
        AppStart.Start(HttpContext.Current);

    }


    void Session_End(object sender, EventArgs e)
    {
        //System.GC.Collect();
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
