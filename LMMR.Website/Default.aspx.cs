﻿#region using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Linq;
using System.Xml;
using log4net;
using log4net.Config;
#endregion

public partial class _Default : BasePage
{
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    UserDetails details;
    NewUser user = new NewUser();
    public string firstName, lastName;
    public bool welcome = false;
    public int intTypeCMSid;
    public string welcomeMessage;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentRestUser"] != null)
        {
            Users objUsers = (Users)Session["CurrentRestUser"];
            lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
            objUsers.LastLogin = DateTime.Now;
            lstLoginTime.Text = DateTime.Now.ToLongDateString();
            AfterLogin(objUsers.UserId,Convert.ToInt32(objUsers.Usertype));
        }

        if (Session["status"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["status"] + "'" + ");</script>");
            Session["status"] = null;
        }
        if (Session["passwordChangeStatus"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["passwordChangeStatus"] + "'" + ");</script>");
            Session["passwordChangeStatus"] = null;
        }

        //For welcome Message
        details = user.getDetailsByID(Convert.ToInt64(Session["CurrentRestUserID"]));
        if (details != null)
        {
            if (details.VisitCount == 0 || details.VisitCount == null)
            {
                Users usr = user.GetUserByID(Convert.ToInt64(Session["CurrentRestUserID"]));
                midSegement.Visible = false;
                welcome = true;
                welcomeDIV.Visible = true;
                details.VisitCount = 1;
                user.updateDetails(details);
                firstName = usr.FirstName;
                lastName = usr.LastName;
                welcomeMessage = user.getWelcomeMessage("FrontEndWelcomeMessage", Convert.ToInt32(details.LanguageId));
            }
            else
            {
                midSegement.Visible = true;
                welcome = false;
                welcomeDIV.Visible = false;
            }
        }
               
        if (!IsPostBack)
        {            
            Dictionary<long, string> participants = Session["participants"] as Dictionary<long, string>;
            if (participants != null)
            {
                if (participants.Count > 1)
                {
                    var first = participants.FirstOrDefault();
                    participants.Clear();
                    participants.Add(first.Key, first.Value);
                }
            }
            GetReadMoreLeft();
            GetReadMoreRight();
            #region Remove Search
            SearchTracer st = null;
            Createbooking objBooking = null;
            BookingManager bm = new BookingManager();
            if (objBooking == null)
            {
                if (Session["SerachID"] == null)
                {
                    objBooking = Session["Search"] as Createbooking;
                }
                else
                {
                    st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                    objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                }
            }
            if (objBooking != null)
            {
                bm.ResetIsReserverFalse(objBooking);
            }
            Session.Remove("Search");
            Session.Remove("SearchID");
            Session.Remove("Request");
            Session.Remove("RequestID");
            #endregion
            BindURLs();
        }

        
    }



    public void BindURLs()
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            hypReadMoreLeft.NavigateUrl = SiteRootPath + "booking-advantage/" + l.Name.ToLower();
            hypReadMoreRight.NavigateUrl = SiteRootPath + "request-advantage/" + l.Name.ToLower();
            joinToday.HRef = SiteRootPath + "join-today/" + l.Name.ToLower();
        }
        else
        {
            hypReadMoreLeft.NavigateUrl = SiteRootPath + "booking-advantage/english";
            hypReadMoreRight.NavigateUrl = SiteRootPath + "request-advantage/english";
            joinToday.HRef = SiteRootPath + "join-today/english";
        }
    }
    /// <summary>
    /// This function used for manage profile after login.
    /// </summary>
    public void AfterLogin(long userID,int userType)
    {
        beforeLogin.Style.Add("display", "none");
        afterLogin.Style.Add("display", "block");
        Session["registerType"] = userType;
        //hypManageProfile.NavigateUrl = "Registration.aspx";
    }


    /// <summary>
    /// This event used for logout linkbutton.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkLogOut_Click(object sender, EventArgs e)
    {


    }
    /// <summary>
    /// This function used for get read more file and desc of leftside.
    /// </summary>
    void GetReadMoreLeft()
    {
        intTypeCMSid = 19;
        TList<CmsDescription> objreadmoreLeft = objManageCMSContent.GetCMSContent(intTypeCMSid, Convert.ToInt64(Session["LanguageID"]));
        if (objreadmoreLeft != null)
        {
            if (objreadmoreLeft.Count > 0)
            {
                lblReadMoreLeftDesc.Text = objreadmoreLeft[0].ContentsDesc;
                //hypReadMoreLeft.NavigateUrl = "Uploads/ReadMore/" + objreadmoreLeft[0].ContentsDesc;
            }
        }
    }

   

    /// <summary>
    /// This function used for get read more file and desc of rightside.
    /// </summary>
    void GetReadMoreRight()
    {
        intTypeCMSid = 20;
        TList<CmsDescription> objreadmoreRight = objManageCMSContent.GetCMSContent(intTypeCMSid, Convert.ToInt64(Session["LanguageID"]));
        if (objreadmoreRight != null)
        {
            if (objreadmoreRight.Count > 0)
            {
                lblReadMoreRightDesc.Text = objreadmoreRight[0].ContentsDesc;
                //hypReadMoreRight.NavigateUrl = "Uploads/ReadMore/" + objreadmoreRight[0].ContentsDesc;
            }
        }
        //Cms cmsObj = (Session["CMS"] as TList<Cms>).Find(a => a.CmsType.ToLower() == "readmoreright");
        //if (cmsObj != null)
        //{
        //    TList<CmsDescription> objreadmoreRight = (Session["CMSDESCRIPTION"] as TList<CmsDescription>).FindAll(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]));
        //    if (objreadmoreRight != null)
        //    {
        //        if (objreadmoreRight.Count <= 0)
        //        {
        //            lblReadMoreRightDesc.Text = objreadmoreRight[0].ContentsDesc;
        //        }
        //    }
        //}
        //else
        //{
        //    Cms cmsObj2 = new SuperAdminTaskManager().getCmsEntityOnType("ReadMoreRight");
        //    if (cmsObj != null)
        //    {
        //        TList<CmsDescription> objreadmoreRight = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"]));
        //        if (objreadmoreRight != null)
        //        {
        //            if (objreadmoreRight.Count <= 0)
        //            {
        //                lblReadMoreRightDesc.Text = objreadmoreRight[0].ContentsDesc;
        //            }
        //        }
        //    }
        //}
    }


    /// <summary>
    /// This method is passing the key value
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    //public string GetKeyResult(string key)
    //{
    //    if (XMLLanguage == null)
    //    {
    //        XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
    //    }
    //    XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + key + "']");
    //    return System.Net.WebUtility.HtmlEncode(nodes.InnerText);// System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}

    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "manage-profile/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "manage-profile/english");
        }
    }
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "change-password/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "change-password/english");
        }
    }

    protected void hypListBookings_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-bookings/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-bookings/english");
        }
    }

    protected void hypListRequests_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-requests/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-requests/english");
        }
    }
}