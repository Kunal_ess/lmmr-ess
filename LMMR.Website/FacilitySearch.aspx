﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FacilitySearch.aspx.cs" Inherits="FacilitySearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="contact-details">
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                <td valign="top">
                    <b>General</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkGeneral" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" Enabled="false"
                                    Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Activities</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkActivities" runat="server" RepeatDirection="Horizontal" Enabled="false"
                                    Width="100%" RepeatColumns="3">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Services</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkServices" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" Enabled="false"
                                    Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Meeting Specifics</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkMeeting" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" Enabled="false"
                                    Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Bedrooms</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkBedrooms" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" Enabled="false"
                                    Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
