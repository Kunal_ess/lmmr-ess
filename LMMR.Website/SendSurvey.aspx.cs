﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;
using LMMR.Data;
using System.Globalization;



#endregion

public partial class SendSurvey : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_booking_Click(object sender, EventArgs e)
    {
        try
        {

        //Viewbookingrequest booking = new Viewbookingrequest();
        //Users obj_users = new Users();
            string firstDate = DateTime.Now.ToString("yyyy-MM-dd");
            string lastDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            //string lastDate = DateTime.Now.ToString("yyyy-MM-dd");
            string where = "DepartureDate" + " between '" + lastDate + "' and '" + firstDate + "' and requeststatus=6";
        string orderby = "";
        int totalcount = 0;
        int intCount = 1;
        VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
        Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        if (Vlistreq.Count > 0)
        {
            foreach (Viewbookingrequest vl in Vlistreq)
            {
            SendMails mail = new SendMails();
            mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
            mail.ToEmail = vl.EmailId;
            mail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
            string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/SurveyEmailFormatBooking.htm"));
            EmailValueCollection objEmailValues = new EmailValueCollection();
            mail.Subject = "LMMR User Survey Booking";
            //string strActivationLink = "activation.aspx?activate=" + newUser.UserId;
            string user = vl.Contact + vl.LastName;
            string hotelname = vl.HotelName;
            string creatorid = vl.CreatorId.ToString();
            string venueid = vl.HotelId.ToString();
            string bookingID = vl.Id.ToString();
            string link = "SurveyDetails.aspx";
            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForSurveybooking(user, hotelname, ConfigurationManager.AppSettings["Sender"], creatorid, venueid, bookingID, link))
            {
                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
            }

            mail.Body = bodymsg;
            mail.SendMail();
               
            }
            lbl1.Text = "Survey booking Send Successfully";
        }
        else
        {
            lbl1.Text = "There is no Booking for the current date";
        }
        
            
        }

        catch (Exception ex)
        {
            lbl1.Text = ex.ToString();
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        

    }
    protected void btn_request_Click(object sender, EventArgs e)
    {
        try
        {

            //Viewbookingrequest booking = new Viewbookingrequest();
            //Users obj_users = new Users();
            string firstDate = DateTime.Now.ToString("yyyy-MM-dd");
            string lastDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            //string lastDate = DateTime.Now.ToString("yyyy-MM-dd");
            string where = "DepartureDate" + " between '" + lastDate + "' and '" + firstDate + "' and requeststatus=7";
            string orderby = "";
            int totalcount = 0;
            int intCount = 1;
            VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
            Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount).FindAllDistinct(ViewbookingrequestColumn.HotelId);
            if (Vlistreq.Count > 0)
            {
                foreach (Viewbookingrequest vl in Vlistreq)
                {
                    SendMails mail = new SendMails();
                    mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                    mail.ToEmail = vl.EmailId;
                    mail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                    string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/SurveyEmailformatRequest.htm"));
                    EmailValueCollection objEmailValues = new EmailValueCollection();
                    mail.Subject = "LMMR User Survey Request";
                    //string strActivationLink = "activation.aspx?activate=" + newUser.UserId;
                    string user = vl.Contact + vl.LastName;
                    //string hotelname = vl.HotelName;
                    string creatorid = vl.CreatorId.ToString();
                    string venueid = vl.HotelId.ToString();
                    string bookingID = vl.Id.ToString();
                    string link = "SurveyDetailsRequest.aspx";
                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForSurveyrequest(user, ConfigurationManager.AppSettings["Sender"], creatorid, venueid, bookingID, link))
                    {
                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                    }

                    mail.Body = bodymsg;
                    mail.SendMail();



                }
                lbl1.Text = "Survey Request Send Successfully";
            }
            else
            {
                lbl1.Text = "There is no request for the current date";
            }



        }

        catch (Exception ex)
        {
            lbl1.Text = ex.ToString();
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
}