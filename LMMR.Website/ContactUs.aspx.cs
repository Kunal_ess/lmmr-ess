﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class Default2 : BasePage
{
    ManageCMSContent obj = new ManageCMSContent();
    
    public void BindURLs()
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            jointoday.HRef=SiteRootPath + "jointoday/" + l.Name.ToLower();
        }
        else
        {
            jointoday.HRef=SiteRootPath + "jointoday/english";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentRestUser"] != null)
        {
            Users objUsers = (Users)Session["CurrentRestUser"];
            lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
            objUsers.LastLogin = DateTime.Now;
            lstLoginTime.Text = DateTime.Now.ToLongDateString();
            AfterLogin(objUsers.UserId, Convert.ToInt32(objUsers.Usertype));
        }

        if (Session["status"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["status"] + "'" + ");</script>");
            Session["status"] = null;
        }
        if (Session["passwordChangeStatus"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["passwordChangeStatus"] + "'" + ");</script>");
            Session["passwordChangeStatus"] = null;
        }

        if (Session["LanguageID"] == null)
        {
            Session["LanguageID"] = 1;
        }
        if (!IsPostBack)
        {
            GetContactUsData();
        
        }

    }


    public void AfterLogin(long userID, int userType)
    {
        beforeLogin.Style.Add("display", "none");
        afterLogin.Style.Add("display", "block");
        Session["registerType"] = userType;
        //hypManageProfile.NavigateUrl = "Registration.aspx";
    }

    /// <summary>
    /// This method return the value in Getresult function
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }

    public void GetContactUsData()
    {
        Cms cmsObj = new SuperAdminTaskManager().getCmsEntityOnType("ContactUs");
        if (cmsObj != null)
        {
            TList<CmsDescription> getDesc = obj.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
            if (getDesc.Count > 0)
            {
                lblContectUs.Text = getDesc[0].ContentsDesc;
            }
            else
            {
                lblContectUs.Text = "Contact Us Description Not found";
            }
        }
        else
        {
            lblContectUs.Text = "Contact Us Not found";
        }
    }

    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "manage-profile/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "manage-profile/english");
        }
    }
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "change-password/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "change-password/english");
        }
    }

    protected void hypListBookings_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-bookings/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-bookings/english");
        }
    }

    protected void hypListRequests_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-requests/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-requests/english");
        }
    }
}