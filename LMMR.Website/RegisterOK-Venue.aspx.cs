﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class SubscribeNewsOK : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        divmessage.Style.Add("display", "block");
        divmessage.Attributes.Add("class", "succesfuly");
        if (!IsPostBack)
        {
            BindURLs();
        }
        
    }
    public void BindURLs()
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            joinToday.HRef = SiteRootPath + "join-today/" + l.Name.ToLower();
        }
        else
        {
            joinToday.HRef = SiteRootPath + "join-today/english";
        }
    }

    /// <summary>
    /// This method return the value in Getresult function
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    //public string GetKeyResult(string key)
    //{
    //    return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}
    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        Response.Redirect("Registration.aspx");
    }
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("ChangePassword.aspx");
    }
}