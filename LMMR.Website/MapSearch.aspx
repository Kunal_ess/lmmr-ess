﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="MapSearch.aspx.cs" Inherits="MapSearch" %>

<%@ Register Src="UserControl/Frontend/HotelOfTheWeek.ascx" TagName="HotelOfTheWeek"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/Frontend/Benefits.ascx" TagName="Benefits" TagPrefix="uc2" %>
<%@ Register Src="UserControl/Frontend/RecentlyJoinedHotel.ascx" TagName="RecentlyJoinedHotel"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Frontend/NewsSubscriber.ascx" TagName="NewsSubscriber"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <uc1:HotelOfTheWeek ID="HotelOfTheWeek2" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <div class="mainbody-left">
        <div class="banner">
                <div id="map_canvas" style="width: 460px; height: 300px; float: left;">
                </div>
            <%--<img src="images/banner.png" />--%>
        </div>
        <div class="video">
            <div class="videoleftbody">
                <div class="videobody">
                    <img src="images/con-img1.png" />
                </div>
                <div class="video-button">
                    <a href="#" onclick="return Navigate();"><%= GetKeyResult("FORGUESTS")%></a>
                </div>
            </div>
            <div class="videorightbody">
                <div class="videobody">
                    <img src="images/con-img2.png" />
                </div>
                <div class="video-button">
                    <a href="#" onclick="return Navigate();"><%= GetKeyResult("FORHOTELS")%></a>
                </div>
            </div>
        </div>
        <div class="mid">
            <div class="left">
                <h2>
                    <%= GetKeyResult("HOWTOBOOKONLINE")%></h2>
                <p>
                    <asp:Label ID="lblReadMoreLeftDesc" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    <asp:HyperLink ID="hypReadMoreLeft" runat="server">Read More</asp:HyperLink>
                </p>
            </div>
            <div class="right">
                <h2>
                    <%= GetKeyResult("HOWTOSENDAREQUEST")%></h2>
                <p>
                    <asp:Label ID="lblReadMoreRightDesc" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    <asp:HyperLink ID="hypReadMoreRight" runat="server">Read More</asp:HyperLink></p>
            </div>
        </div>
        <uc2:Benefits ID="Benefits1" runat="server" />
    </div>
    <div class="mainbody-right">
        <div class="mainbody-right-call">
            <div class="need">
                <span>
                    <%= GetKeyResult("NEEDHELP")%></span>
                <br />
                <%= GetKeyResult("CALLUS")%>
                5/7
            </div>
            <div class="phone">
                +32 2 344 25 50 </div>
             <p style="font-size:10px;color:White;text-align:center">  CALL US and WE DO IT FOR YOU</p>
            <div class="mail">
                <a href="#">
                    <%= GetKeyResult("CONTACTUSVIAMAIL")%></a>
            </div>
        </div>
        <div class="mainbody-right-join">
            <div class="join">
                <a href="JoinToday.aspx">
                    <%= GetKeyResult("JOINUSTODAY")%></a></div>
           (For Hotels, Meeting facilities, Event locations)
        </div>
        <div class="mainbody-right-you">
            <img src="images/youtube.png" />
        </div>
        <div class="mainbody-right-news">
            <uc4:NewsSubscriber ID="NewsSubscriber1" runat="server" />
        </div>
        <uc3:RecentlyJoinedHotel ID="RecentlyJoinedHotel1" runat="server" />
    </div>
    <script type="text/javascript" language="javascript">

        function Navigate() {

            var url = 'Video.aspx';
            var winName = 'myWindow';
            var w = '500';
            var h = '300';
            var scroll = 'no';
            var popupWindow = null;
            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
            settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
            popupWindow = window.open(url, winName, settings)
            return false;
        }

    </script>
             
    
</asp:Content>
