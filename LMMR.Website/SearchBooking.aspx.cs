﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.Configuration;
using log4net;
using log4net.Config;
using AjaxControlToolkit;
using System.Xml;
#endregion

public partial class SearchBooking : BasePage
{
    #region variables
    public class Mapdate
    {
        public string Hotelbname;
        public string Logitude;
        public string Latitude;
        public string IsPromoted;

    }

    public static TList<MeetingRoom> objmeetingroom = new TList<MeetingRoom>();
    public static TList<Hotel> objhotel = new TList<Hotel>();
    public static TList<MeetingRoomConfig> objConfig = new TList<MeetingRoomConfig>();
    ILog logger = log4net.LogManager.GetLogger(typeof(SearchBooking));
    ManageMapSearch objManageMapSearch = new ManageMapSearch();
    public string HotelData
    {
        get;
        set;
    }
    VList<BookingRequestViewList> vlist;
    BookingRequest objBookingRequest = new BookingRequest();
    MeetingRoomDescManager ObjMeetingRoomDesc = new MeetingRoomDescManager();
    MeetingRoomConfigManager ObjMeetingRoomConfigManager = new MeetingRoomConfigManager();
    Facilities objFacility = new Facilities();
    VList<ViewFacilityIcon> vicon;
    HotelInfo ObjHotelinfo = new HotelInfo();
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public static string status = "";
    public static DateTime BokingDate;
    CurrencyManager cm = new CurrencyManager();
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public decimal CurrencyConvert
    {
        get { return Convert.ToDecimal(ViewState["CurrencyConvert"]); }
        set { ViewState["CurrencyConvert"] = value; }
    }
    public string UserCurrency
    {
        get { return Convert.ToString(ViewState["UserCurrency"]); }
        set { ViewState["UserCurrency"] = value; }
    }
    public string HotelCurrency
    {
        get { return Convert.ToString(ViewState["HotelCurrency"]); }
        set { ViewState["HotelCurrency"] = value; }
    }
    #endregion

    #region Page load
    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!Page.IsPostBack)
        {
            if (Session["masterInput"] != null)
            {
                BindSearchResult();
            }

            //if (objhotel != null)
            //{
            //    divshoppingcart.Style.Add("display", "block");
            //    lstViewAddToShopping.DataSource = objhotel;
            //    lstViewAddToShopping.DataBind();
            //}                      
        }
        else
        {
            ApplyPaging();
        }
    }
    #endregion

    #region Bind Search Result    
    // Seach Booking and request algorithm according to the search criteria   
    private void BindSearchResult()
    {
        try
        {
            objmeetingroom.Clear();
            objhotel.Clear();
            objConfig.Clear();
            divshoppingcart.Style.Add("display", "none");
            string whereclaus = Convert.ToString(Session["Where"]);
            string orderby = "IsPriority DESC," + HotelColumn.BookingAlgo + " DESC";
            vlist = objBookingRequest.GetBookingReqDetails(whereclaus, orderby);
            //TList<PriorityBasket> prb=objBookingRequest.GetOrderforPriorityBasket(vlist,'',1,1)

            grvBooking.DataSource = vlist.FindAllDistinct(BookingRequestViewListColumn.HotelId);
            grvBooking.DataBind();
            lblResultCount.Text = vlist.FindAllDistinct(BookingRequestViewListColumn.HotelId).Count.ToString();

            grvRequest.DataSource = ObjHotelinfo.GetAllHotel();
            grvRequest.DataBind();
            ApplyPaging();            

            //This Code for map

            HotelData = "[";
            if (vlist.Count > 0)
            {
                foreach (BookingRequestViewList h in vlist.FindAllDistinct(BookingRequestViewListColumn.HotelId))
                {
                    string IsPromoted = string.Empty;
                    if (h.DdrPercent < 0)
                    {
                        IsPromoted = "YES";
                    }
                    else
                    {
                        IsPromoted = "NO";
                    }

                    if (HotelData == "[")
                    {

                        HotelData += "['" + h.HotelName + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "']";
                    }
                    else
                    {
                        HotelData = HotelData + ",[" + "'" + h.HotelName + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "']";
                    }
                }
                HotelData += "]";
            }
            //////////////////////////////////////

            GetCenterPoint(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }


    #endregion

    #region Prepare Search step
    //Get Center point for google map
    void GetCenterPoint(int cityID)
    {

        //Function to get main center point of the city.
        string strLatitudeOfCenter = "";
        string strLogitudeOfCenter = "";
        string strCenterPlace = "";
        string strCenterPoint = "";
        int intZoomLevel = 10;
        TList<MainPoint> objCenterPoint = objManageMapSearch.GetCenterPoint(cityID);
        if (objCenterPoint.Count > 0)
        {
            MainPoint objPoint = objCenterPoint.Find(a => a.IsCenter == true);
            strLatitudeOfCenter = objPoint.Latitude.ToString();
            strLogitudeOfCenter = objPoint.Longitude.ToString();
            HiddenField hdnLatitude = (HiddenField)Master.FindControl("hdnMapLatitude");
            HiddenField hdnLogitude = (HiddenField)Master.FindControl("hdnMapLogitude");
            hdnLatitude.Value = objPoint.Latitude.ToString();
            hdnLogitude.Value = objPoint.Longitude.ToString();
            strCenterPlace = objPoint.MainPointName;
            intZoomLevel = 12;
        }
        strCenterPoint = strLatitudeOfCenter + "," + strLogitudeOfCenter;
        ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadMap3", "OnDemand('" + strCenterPoint + "','" + strCenterPlace + "','" + intZoomLevel + "');", true);
    }
    #endregion

    #region Get Hotel details
    //Get Hotel details and show for booking gridview
    protected void grvBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                HiddenField hdnId = (HiddenField)e.Row.FindControl("hdnHotelId");
                Label lblAddress = (Label)e.Row.FindControl("lblAddress");
                Label lblDescription = (Label)e.Row.FindControl("lblDescription");
                Label lblMeetingroomOnline = (Label)e.Row.FindControl("lblMeetingroomOnline");
                Image imgStar = (Image)e.Row.FindControl("imgStar");
                Image imgHotel = (Image)e.Row.FindControl("imgHotel");
                Hotel htl = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnId.Value));

                string whereclausedesc = "Hotel_Id=" + htl.Id + " and " + "Language_Id=" + Session["LanguageID"] + "";
                TList<HotelDesc> lsthdesc = ObjHotelinfo.GetHotelDesc(whereclausedesc, String.Empty);
                if (lsthdesc.Count > 0)
                {
                    lblDescription.Text = (lsthdesc[0].Description == null ? "" : lsthdesc[0].Description);
                    lblAddress.Text = (lsthdesc[0].Address == null ? "" : lsthdesc[0].Address);
                }
                else
                {
                    int englishID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                    string wherecls = "Hotel_Id=" + htl.Id + " and " + "Language_Id=" + englishID + "";
                    TList<HotelDesc> lstEng = ObjHotelinfo.GetHotelDesc(wherecls, String.Empty);
                    lblDescription.Text = (lstEng[0].Description == null ? "" : lstEng[0].Description);
                    lblAddress.Text = (lstEng[0].Address == null ? "" : lstEng[0].Address);
                }
                //for static hotel star
                if (htl.Stars == 1)
                {
                    imgStar.ImageUrl = "~/Images/1.png";
                }
                else if (htl.Stars == 2)
                {
                    imgStar.ImageUrl = "~/Images/2.png";
                }
                else if (htl.Stars == 3)
                {
                    imgStar.ImageUrl = "~/Images/3.png";
                }
                else if (htl.Stars == 4)
                {
                    imgStar.ImageUrl = "~/Images/4.png";
                }
                else if (htl.Stars == 5)
                {
                    imgStar.ImageUrl = "~/Images/5.png";
                }
                else if (htl.Stars == 6)
                {
                    imgStar.ImageUrl = "~/Images/6.png";
                }
                else if (htl.Stars == 7)
                {
                    imgStar.ImageUrl = "~/Images/7.png";
                }
                //for static hotel star

                if (htl.Logo != null)
                {
                    imgHotel.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + htl.Logo;
                }
                
                System.Web.UI.HtmlControls.HtmlGenericControl divVideo = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divVideo"));
                TList<HotelPhotoVideoGallary> ObjHotelImageVideo = objBookingRequest.GetAllHotelPictureVideo(Convert.ToInt32(htl.Id));
                if (ObjHotelImageVideo.Count == 0)
                {
                    divVideo.Visible = false;
                }
                LinkButton button = (LinkButton)e.Row.FindControl("lnbVideo");
                button.Attributes.Add("onclick", string.Format("Navigate('{0}')", hdnId.Value));

                //Show count number for online book
                ObjMeetingRoomDesc = new MeetingRoomDescManager();
                lblMeetingroomOnline.Text = Convert.ToString(ObjMeetingRoomDesc.GetAllOnlineMeetingRoom(Convert.ToInt32(htl.Id)).Count);
                
                Label lblActPkgPrice = (Label)e.Row.FindControl("lblActPkgPrice");
                Label lblActPAckagePrice = (Label)e.Row.FindControl("lblActPAckagePrice");
                Label lblDiscountPrice = (Label)e.Row.FindControl("lblDiscountPrice");

                DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));                

                //Get Price from actualpackageprice by hotelid with discount
                if (Convert.ToDecimal(lblDiscountPrice.Text) < 0)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divDiscount"));
                    mydiv.Style.Add("display", "block");
                    lblActPkgPrice.Text = String.Format("{0:#,###}", objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(hdnId.Value))[0].ActualFullDayPrice);                    
                    lblDiscountPrice.Text = String.Format("{0:#,###}", (Math.Abs((Convert.ToDecimal(lblActPkgPrice.Text) * Convert.ToDecimal(lblDiscountPrice.Text) / 100))));

                    if (((Session["masterInput"] as BookingRequest)).propDuration == "2")
                    {
                        TList<SpecialPriceAndPromo> Spl = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(hdnId.Value), fromDate.AddDays(1));
                        if (Convert.ToDecimal(Spl[0].DdrPercent) > 0)
                        {
                            lblDiscountPrice.Text = String.Format("{0:#,###}", (Math.Abs(Convert.ToDecimal(lblDiscountPrice.Text) / 2)));
                            return;
                        }

                        decimal day2Perc = (Math.Abs((Convert.ToDecimal(lblActPkgPrice.Text) * Convert.ToDecimal(Spl[0].DdrPercent) / 100)));
                        decimal average = (Convert.ToDecimal(lblDiscountPrice.Text) + day2Perc) / 2;
                        lblDiscountPrice.Text = String.Format("{0:#,###}", average);
                    }
                }
                //Get Price from actualpackageprice by hotelid without discount
                else
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                    mydiv.Style.Add("display", "block");
                    lblActPAckagePrice.Text = String.Format("{0:#,###}", objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(hdnId.Value))[0].ActualFullDayPrice);                    
                }                         

                //Get Facility images
                //Navigate to Facility icon
                HyperLink hypClock = (HyperLink)e.Row.FindControl("hypClock");
                hypClock.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                HyperLink hypDateIcon = (HyperLink)e.Row.FindControl("hypDateIcon");
                hypDateIcon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                HyperLink hyplockicon = (HyperLink)e.Row.FindControl("hyplockicon");
                hyplockicon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                HyperLink hypPrinticon = (HyperLink)e.Row.FindControl("hypPrinticon");
                hypPrinticon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                HyperLink hypchticon = (HyperLink)e.Row.FindControl("hypchticon");
                hypchticon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                //HyperLink hyplasticon = (HyperLink)e.Row.FindControl("hyplasticon");
                //hyplasticon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));

                string whereclaus = "Hotel_Id=" + hdnId.Value + " and Icon='private-parking.png'";
                vicon = objFacility.GetFacilityIcon(whereclaus, String.Empty);
                if (vicon.Count > 0)
                {
                    hypPrinticon.Visible = true;
                }

                string whereclaus1 = "Hotel_Id=" + hdnId.Value + " and Icon='bar-resto.png'";
                vicon = objFacility.GetFacilityIcon(whereclaus1, String.Empty);
                if (vicon.Count > 0)
                {
                    hypClock.Visible = true;
                }

                string whereclaus2 = "Hotel_Id=" + hdnId.Value + " and Icon='wifi.png'";
                vicon = objFacility.GetFacilityIcon(whereclaus2, String.Empty);
                if (vicon.Count > 0)
                {
                    hypchticon.Visible = true;
                }

                string whereclaus3 = "Hotel_Id=" + hdnId.Value + " and Icon='daylight-in-all-meeting-rooms.png'";
                vicon = objFacility.GetFacilityIcon(whereclaus3, String.Empty);
                if (vicon.Count > 0)
                {
                    hypDateIcon.Visible = true;
                }

                string whereclaus4 = "Id=" + hdnId.Value + " and " + "IsBedroomAvailable=1";
                TList<Hotel> lsthLink = ObjHotelinfo.GetHotelbyCondition(whereclaus4, String.Empty);
                if (lsthLink.Count > 0)
                {
                    hyplockicon.Visible = true;
                }
                //End Facility Images
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Apply Paging
    //Apply paging for Booking gridview
    private void ApplyPaging()
    {
        GridViewRow row = grvBooking.TopPagerRow;
        if (row != null)
        {
            PlaceHolder ph;
            LinkButton lnkPaging;
            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;
            lnkPrevPage = new LinkButton();
            lnkPrevPage.CssClass = "pre";
            lnkPrevPage.Width = Unit.Pixel(73);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "prev";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkPrevPage);
            if (grvBooking.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;

            }
            for (int i = 1; i <= grvBooking.PageCount; i++)
            {
                lnkPaging = new LinkButton();
                if (ViewState["CurrentPage"] != null)
                {
                    if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                lnkPaging.Width = Unit.Pixel(16);
                lnkPaging.Text = i.ToString();
                lnkPaging.CommandName = "Page";
                lnkPaging.CommandArgument = i.ToString();
                if (i == grvBooking.PageIndex + 1)
                    ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPaging);
            }
            lnkNextPage = new LinkButton();
            lnkNextPage.CssClass = "nex";
            lnkNextPage.Width = Unit.Pixel(42);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "next";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkNextPage);
            ph = (PlaceHolder)row.FindControl("ph");
            if (grvBooking.PageIndex == grvBooking.PageCount - 1)
            {
                lnkNextPage.Enabled = false;

            }
        }

    }
    #endregion

    #region Add to shoppingcart for booking
   //This event is used for Add to shopping cart for Normal and secondary booking
    protected void btnContinue_Click(object sender, EventArgs e)
    {
       
        if (rbNo.Checked == true)
        {
            Session["IsSecondary"] = "NO";        
            divshoppingcart.Style.Add("display", "block");
            MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnConfigurationId.Value));
            objConfig.Add(objmeetconfg);
            MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnMeetingRoomId.Value));
            objmeetingroom.Add(objmeet);
            Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnHotelId.Value));
            objhotel.Add(getHotel);
            lstViewAddToShopping.DataSource = objhotel;
            lstViewAddToShopping.DataBind();
        }
        else if (rbYes.Checked == true)
        {
            MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnConfigurationId.Value));
            objConfig.Add(objmeetconfg);
            int meetingroomId = Convert.ToInt32(hdnMeetingRoomId.Value);
            var IsMeetingExist = objmeetingroom.Find(a=>a.Id==meetingroomId);
            if (IsMeetingExist == null)
            {
                MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnMeetingRoomId.Value));
                objmeetingroom.Add(objmeet);
            }
            int hotelID = Convert.ToInt32(hdnHotelId.Value);
            var IsHotelExist=objhotel.Find(a=>a.Id==hotelID);
            if (IsHotelExist==null)
            {
            Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnHotelId.Value));
            objhotel.Add(getHotel);
            }
          
            lstViewAddToShopping.DataSource = objhotel;
            lstViewAddToShopping.DataBind();
            int BookCounter = 0;
            for (int i = 0; i < grvBooking.Rows.Count; i++) // select status in drop downlist box of nested grid view
            {
                BookCounter++;
                if (BookCounter == 1)
                {
                    Repeater childgrid = (Repeater)grvBooking.Rows[i].FindControl("rptSecondaryMeeting");
                    GridView grvBookMeeting = (GridView)grvBooking.Rows[i].FindControl("grvBookMeeting");
                    ObjMeetingRoomDesc = new MeetingRoomDescManager();

                    if (Session["masterInput"] != null)
                    {
                        string status = "";
                        string days = ((Session["masterInput"] as BookingRequest)).propDays;
                        DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                        if (days == "0")
                        {
                            status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                        }
                        else if (days == "1")
                        {
                            status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                        }
                        else if (days == "2")
                        {
                            status = ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                        }

                        if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                        {
                            string whereclaus = "HotelId='" + Convert.ToInt32(hdnHotelId.Value) + "' and AvailabilityDate='" + fromDate + "' and " + status + " and MeetingRoomId not in('" + Convert.ToInt32(hdnMeetingRoomId.Value) + "')";
                            childgrid.DataSource = objBookingRequest.GetAllOnlineMeetingRoomSecondary(whereclaus, 2);
                            childgrid.DataBind();
                        }
                        else
                        {
                            string whereclaus = "HotelId='" + Convert.ToInt32(hdnHotelId.Value) + "' and AvailabilityDate='" + fromDate + "' and " + status + " and MeetingRoomId not in('" + Convert.ToInt32(hdnMeetingRoomId.Value) + "')";
                            childgrid.DataSource = objBookingRequest.GetMeetingRoomAvailability(whereclaus);
                            childgrid.DataBind();
                        }
                    }
                    grvBookMeeting.Visible = false;
                    System.Web.UI.HtmlControls.HtmlGenericControl divRequest = ((System.Web.UI.HtmlControls.HtmlGenericControl)grvBooking.Rows[i].FindControl("divRequest"));
                    divRequest.Visible = false;
                    if (Session["IsSecondary"].ToString() == "YES")
                    {
                        divshoppingcart.Style.Add("display", "block");
                    }
                    else
                    {

                        Session["IsSecondary"] = "YES";
                    }
                }
            }
        }

    }

    protected void lnkAddtoShopping_Click(object sender, EventArgs e)
    {
        btnContinue_Click(null, null);        
    }
    #endregion

    #region Pageindex for Booking Gridview
    //This event is used to maintain paging for booking
    protected void grvBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvBooking.PageIndex = e.NewPageIndex;
        ViewState["CurrentPage"] = e.NewPageIndex;
        BindSearchResult();
    }
    #endregion

    #region Event Rendor   
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion

    #region Bind Meetingroom after click on hotel
    //Bind Meetingroom for booking and request after clicking see all
    protected void grvBooking_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            string days = ((Session["masterInput"] as BookingRequest)).propDays;
            BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
            if (days == "0")
            {
                status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
            }
            else if (days == "1")
            {
                status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
            }
            else if (days == "2")
            {
                status = ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
            }
        }

        if (e.CommandName == "Popup" && e.CommandArgument != null)
        {
            Session["IsSecondary"] = "";
            int hotelid = Convert.ToInt32(e.CommandArgument.ToString());
            hdnHotelId.Value = Convert.ToString(hotelid);
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            GridView gv = (GridView)row.FindControl("grvBookMeeting");
            GridView gvr = (GridView)row.FindControl("grvRequestMeeting");

            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBmeeting"));
            mydiv.Style.Add("display", "block");        
              
            string whereclaus = "HotelId='" + hotelid + "' and AvailabilityDate='" + BokingDate + "' and " + status + "";
            gv.DataSource = objBookingRequest.GetMeetingRoomAvailability(whereclaus);
            gv.DataBind();
            gvr.DataSource = objBookingRequest.GetMeetingRoomAvailability(whereclaus);
            gvr.DataBind();
        }

        if (e.CommandName == "Request" && e.CommandArgument != null)
        {
            Session["IsSecondary"] = "";
            int hotelid = Convert.ToInt32(e.CommandArgument.ToString());
            hdnHotelId.Value = Convert.ToString(hotelid);
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            GridView gvr = (GridView)row.FindControl("grvRequestMeeting");

            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBmeeting"));
            mydiv.Style.Add("display", "block");
            System.Web.UI.HtmlControls.HtmlGenericControl divBookingMeeting = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBookingMeeting"));
            divBookingMeeting.Style.Add("display", "none");     
            System.Web.UI.HtmlControls.HtmlGenericControl divRequest = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divRequest"));
            divRequest.Style.Add("display", "block");        
           
            string whereclaus = "HotelId='" + hotelid + "' and AvailabilityDate='" + BokingDate + "' and " + status + "";
            gvr.DataSource = objBookingRequest.GetMeetingRoomAvailability(whereclaus);
            gvr.DataBind();                              
        }

        if (e.CommandName == "Booking" && e.CommandArgument != null)
        {
            Session["IsSecondary"] = "";
            int hotelid = Convert.ToInt32(e.CommandArgument.ToString());
            hdnHotelId.Value = Convert.ToString(hotelid);
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            GridView gv = (GridView)row.FindControl("grvBookMeeting");
            HiddenField hdf = (HiddenField)row.FindControl("hdnHotelId");
            //Repeater grSecondary = (Repeater)row.FindControl("rptSecondaryMeeting");

            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBmeeting"));
            mydiv.Style.Add("display", "block");
            System.Web.UI.HtmlControls.HtmlGenericControl divBookingMeeting = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBookingMeeting"));
            divBookingMeeting.Style.Add("display", "block");
            System.Web.UI.HtmlControls.HtmlGenericControl divRequest = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divRequest"));
            divRequest.Style.Add("display", "none");   
           
            string whereclaus = "HotelId='" + hotelid + "' and AvailabilityDate='" + BokingDate + "' and " + status + "";
            gv.DataSource = objBookingRequest.GetMeetingRoomAvailability(whereclaus);
            gv.DataBind();            
        }

    }
    #endregion

    #region Bind Meetingroom Configuration after click on hotel
    //Bind Meetingroom Configuration for booking after clicking see all
    protected void grvBookMeeting_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            //for image,plan,room size and height from meeting table
            Image hotelImage = (Image)e.Row.FindControl("imgHotelImage");
            hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Row.DataItem, "Picture");
            HyperLink PlanView = (HyperLink)e.Row.FindControl("linkviewPlan");
            if (DataBinder.Eval(e.Row.DataItem, "MRPlan") != "")
            {
                PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Row.DataItem, "MRPlan");
            }
            else
            {
                PlanView.Visible = false;
            }


            HiddenField hdnMId = (HiddenField)e.Row.FindControl("hdnMId");
            //TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomIDParticipant(Convert.ToInt32(hdnMId.Value), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants));
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(Convert.ToInt32(hdnMId.Value));

            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int Participant = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Row.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Row.FindControl("lblTheatreMax");
                        CheckBox chkTheatre = (CheckBox)e.Row.FindControl("chkTheatre");
                        chkTheatre.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "');");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkTheatre.Visible = false;                          
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkTheatre.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblReceptionMin = (Label)e.Row.FindControl("lblReceptionMin");
                        Label lblReceptionMax = (Label)e.Row.FindControl("lblReceptionMax");
                        CheckBox chkSchool = (CheckBox)e.Row.FindControl("chkSchool");
                        chkSchool.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "');");
                        lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkSchool.Visible = false;                            
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkSchool.Visible = false;
                        }


                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Row.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Row.FindControl("lblUShapeMax");
                        CheckBox chkUshape = (CheckBox)e.Row.FindControl("chkUshape");
                        chkUshape.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "');");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkUshape.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkUshape.Visible = false;
                        }

                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBanquetMin = (Label)e.Row.FindControl("lblBanquetMin");
                        Label lblBanquetMax = (Label)e.Row.FindControl("lblBanquetMax");
                        CheckBox chkBoardroom = (CheckBox)e.Row.FindControl("chkBoardroom");
                        chkBoardroom.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "');");
                        lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkBoardroom.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkBoardroom.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Row.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Row.FindControl("lblCocktailMax");
                        CheckBox chkCocktail = (CheckBox)e.Row.FindControl("chkCocktail");
                        chkCocktail.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "');");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkCocktail.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkCocktail.Visible = false;
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region Bind Meetingroom Configuration for request after click on hotel
    //Bind Meetingroom Configuration for request after clicking see all
    protected void grvRequestMeeting_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            //for image,plan,room size and height from meeting table
            Image hotelImage = (Image)e.Row.FindControl("imgHotelImage");
            hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Row.DataItem, "Picture");
            HyperLink PlanView = (HyperLink)e.Row.FindControl("linkviewPlan");
            if (DataBinder.Eval(e.Row.DataItem, "MRPlan") != "")
            {
                PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Row.DataItem, "MRPlan");
            }
            else
            {
                PlanView.Visible = false;
            }


            HiddenField hdnMId = (HiddenField)e.Row.FindControl("hdnMId");
            //TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomIDParticipant(Convert.ToInt32(hdnMId.Value), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants));
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(Convert.ToInt32(hdnMId.Value));

            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Row.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Row.FindControl("lblTheatreMax");
                        CheckBox chkTheatre = (CheckBox)e.Row.FindControl("chkTheatre");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            chkTheatre.Visible = false;
                            lblTheatreMin.Text = "";
                            lblTheatreMax.Text = "";
                        }
                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblReceptionMin = (Label)e.Row.FindControl("lblReceptionMin");
                        Label lblReceptionMax = (Label)e.Row.FindControl("lblReceptionMax");
                        CheckBox chkSchool = (CheckBox)e.Row.FindControl("chkSchool");
                        lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            chkSchool.Visible = false;
                            lblReceptionMin.Text = "";
                            lblReceptionMax.Text = "";
                        }


                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Row.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Row.FindControl("lblUShapeMax");
                        CheckBox chkUshape = (CheckBox)e.Row.FindControl("chkUshape");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            chkUshape.Visible = false;
                            lblUShapeMin.Text = "";
                            lblUShapeMax.Text = "";
                        }

                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBanquetMin = (Label)e.Row.FindControl("lblBanquetMin");
                        Label lblBanquetMax = (Label)e.Row.FindControl("lblBanquetMax");
                        CheckBox chkBoardroom = (CheckBox)e.Row.FindControl("chkBoardroom");
                        lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            chkBoardroom.Visible = false;
                            lblBanquetMin.Text = "";
                            lblBanquetMax.Text = "";
                        }
                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Row.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Row.FindControl("lblCocktailMax");
                        CheckBox chkCocktail = (CheckBox)e.Row.FindControl("chkCocktail");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            chkCocktail.Visible = false;
                            lblCocktailMin.Text = "";
                            lblCocktailMax.Text = "";
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region Bind Shopping Cart
    //Bind Shopping Cart
    protected void lstViewAddToShopping_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListView lstView = (ListView)e.Item.FindControl("lstViewMeetingRoomAddToCart");
            lstView.DataSource = objmeetingroom;
            lstView.DataBind();
        }
    }
    #endregion

    #region Remove hotel and meeting room from shopping cart listview
    //Remove hotel and meeting room from shopping cart listview
    protected void lstViewAddToShopping_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        int intHotelID = 0;
        switch (e.CommandName.Trim())
        {
            case "deletehotel":
                intHotelID = Convert.ToInt32(e.CommandArgument);
                Hotel d = objhotel.Find(a => a.Id == intHotelID);
                objhotel.Remove(d);
                if (objhotel.Count > 0)
                {
                    divshoppingcart.Style.Add("display", "none");
                }
                lstViewAddToShopping.DataSource = objhotel;
                lstViewAddToShopping.DataBind();
                break;
        }
    }
    #endregion

    #region Remove hotel and meeting room from shopping cart listview
    //Remove hotel and meeting room from shopping cart listview
    protected void lstViewMeetingRoomAddToCart_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        int intmeetingroom = 0;
        switch (e.CommandName.Trim())
        {
            case "deletemeetingroom":
                intmeetingroom = Convert.ToInt32(e.CommandArgument);
                MeetingRoom d = objmeetingroom.Find(a => a.Id == intmeetingroom);
                objmeetingroom.Remove(d);
                lstViewAddToShopping.DataSource = objhotel;
                lstViewAddToShopping.DataBind();
                break;
        }
    }
    #endregion

    #region Bind secondary meeting room for booking gridview
    //Bind secondary meeting room for booking gridview
    protected void rptSecondaryMeeting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //try
        //{
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //for image,plan,room size and height from meeting table
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));                
            Label lblDate = (Label)e.Item.FindControl("lblDate");
            if (e.Item.ItemType == ListItemType.AlternatingItem)
            {
                lblDate.Text =Convert.ToString( fromDate.AddDays(1).ToShortDateString());
            }
            else
            {
                lblDate.Text = Convert.ToString(fromDate.ToShortDateString());
                Image hotelImage = (Image)e.Item.FindControl("imgHotelImage");
                hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Item.DataItem, "Picture");
                HyperLink PlanView = (HyperLink)e.Item.FindControl("linkviewPlan");
                if (DataBinder.Eval(e.Item.DataItem, "MRPlan") != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Item.DataItem, "MRPlan");
                }
                else
                {
                    PlanView.Visible = false;
                }
            }

            System.Web.UI.HtmlControls.HtmlControl divRptId = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divRptId"));
            int Participant = Convert.ToInt32(txtSecparticipants.Text);
            ((Session["masterInput"] as BookingRequest)).propParticipantSecondary =Convert.ToString(Participant);
            HiddenField hdnMId = (HiddenField)e.Item.FindControl("hdnMId");
            //TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomIDParticipant(Convert.ToInt32(hdnMId.Value), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants));
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(Convert.ToInt32(hdnMId.Value));

            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                        CheckBox chkTheatre = (CheckBox)e.Item.FindControl("chkTheatre");
                        Label lblTheatreMin1 = (Label)e.Item.FindControl("lblTheatreMin1");
                        Label lblTheatreMax1 = (Label)e.Item.FindControl("lblTheatreMax1");
                        CheckBox chkTheatre1 = (CheckBox)e.Item.FindControl("chkTheatre1");
                        Label lblTheatreMin2 = (Label)e.Item.FindControl("lblTheatreMin2");
                        Label lblTheatreMax2 = (Label)e.Item.FindControl("lblTheatreMax2");
                        CheckBox chkTheatre2 = (CheckBox)e.Item.FindControl("chkTheatre2");
                        chkTheatre.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','1','" + divRptId.ClientID + "','2');");
                        chkTheatre1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','2','" + divRptId.ClientID + "','2');");
                        chkTheatre2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','0','" + divRptId.ClientID + "','2');");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                        lblTheatreMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax1.Text = getRow.MaxCapicity.ToString();
                        lblTheatreMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkTheatre.Visible = false;
                            chkTheatre1.Visible = false;
                            chkTheatre2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkTheatre.Visible = false;
                            chkTheatre1.Visible = false;
                            chkTheatre2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblReceptionMin = (Label)e.Item.FindControl("lblReceptionMin");
                        Label lblReceptionMax = (Label)e.Item.FindControl("lblReceptionMax");
                        CheckBox chkSchool = (CheckBox)e.Item.FindControl("chkSchool");
                        Label lblReceptionMin1 = (Label)e.Item.FindControl("lblReceptionMin1");
                        Label lblReceptionMax1 = (Label)e.Item.FindControl("lblReceptionMax1");
                        CheckBox chkSchool1 = (CheckBox)e.Item.FindControl("chkSchool1");
                        Label lblReceptionMin2 = (Label)e.Item.FindControl("lblReceptionMin2");
                        Label lblReceptionMax2 = (Label)e.Item.FindControl("lblReceptionMax2");
                        CheckBox chkSchool2 = (CheckBox)e.Item.FindControl("chkSchool2");
                        chkSchool.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','1','" + divRptId.ClientID + "','3');");
                        chkSchool1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','2','" + divRptId.ClientID + "','3');");
                        chkSchool2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','0','" + divRptId.ClientID + "','3');");
                        lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                        lblReceptionMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax1.Text = getRow.MaxCapicity.ToString();
                        lblReceptionMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkSchool.Visible = false;
                            chkSchool1.Visible = false;
                            chkSchool2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkSchool.Visible = false;
                            chkSchool1.Visible = false;
                            chkSchool2.Visible = false;
                        }


                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                        CheckBox chkUshape = (CheckBox)e.Item.FindControl("chkUshape");
                        Label lblUShapeMin1 = (Label)e.Item.FindControl("lblUShapeMin1");
                        Label lblUShapeMax1 = (Label)e.Item.FindControl("lblUShapeMax1");
                        CheckBox chkUshape1 = (CheckBox)e.Item.FindControl("chkUshape1");
                        Label lblUShapeMin2 = (Label)e.Item.FindControl("lblUShapeMin2");
                        Label lblUShapeMax2 = (Label)e.Item.FindControl("lblUShapeMax2");
                        CheckBox chkUshape2 = (CheckBox)e.Item.FindControl("chkUshape2");
                        chkUshape.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','1','" + divRptId.ClientID + "','4');");
                        chkUshape1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','2','" + divRptId.ClientID + "','4');");
                        chkUshape2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','0','" + divRptId.ClientID + "','4');");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                        lblUShapeMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax1.Text = getRow.MaxCapicity.ToString();
                        lblUShapeMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkUshape.Visible = false;
                            chkUshape1.Visible = false;
                            chkUshape2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkUshape.Visible = false;
                            chkUshape1.Visible = false;
                            chkUshape2.Visible = false;
                        }

                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBanquetMin = (Label)e.Item.FindControl("lblBanquetMin");
                        Label lblBanquetMax = (Label)e.Item.FindControl("lblBanquetMax");
                        CheckBox chkBoardroom = (CheckBox)e.Item.FindControl("chkBoardroom");
                        Label lblBanquetMin1 = (Label)e.Item.FindControl("lblBanquetMin1");
                        Label lblBanquetMax1 = (Label)e.Item.FindControl("lblBanquetMax1");
                        CheckBox chkBoardroom1 = (CheckBox)e.Item.FindControl("chkBoardroom1");
                        Label lblBanquetMin2 = (Label)e.Item.FindControl("lblBanquetMin2");
                        Label lblBanquetMax2 = (Label)e.Item.FindControl("lblBanquetMax2");
                        CheckBox chkBoardroom2 = (CheckBox)e.Item.FindControl("chkBoardroom2");
                        chkBoardroom.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','1','" + divRptId.ClientID + "','5');");
                        chkBoardroom1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','2','" + divRptId.ClientID + "','5');");
                        chkBoardroom2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','0','" + divRptId.ClientID + "',,'5');");
                        lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                        lblBanquetMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax1.Text = getRow.MaxCapicity.ToString();
                        lblBanquetMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkBoardroom.Visible = false;
                            chkBoardroom1.Visible = false;
                            chkBoardroom2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkBoardroom.Visible = false;
                            chkBoardroom1.Visible = false;
                            chkBoardroom2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                        CheckBox chkCocktail = (CheckBox)e.Item.FindControl("chkCocktail");
                        Label lblCocktailMin1 = (Label)e.Item.FindControl("lblCocktailMin1");
                        Label lblCocktailMax1 = (Label)e.Item.FindControl("lblCocktailMax1");
                        CheckBox chkCocktail1 = (CheckBox)e.Item.FindControl("chkCocktail1");
                        Label lblCocktailMin2 = (Label)e.Item.FindControl("lblCocktailMin2");
                        Label lblCocktailMax2 = (Label)e.Item.FindControl("lblCocktailMax2");
                        CheckBox chkCocktail2 = (CheckBox)e.Item.FindControl("chkCocktail2");
                        chkCocktail.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','1','" + divRptId.ClientID + "','6');");
                        chkCocktail1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','2','" + divRptId.ClientID + "',,'6');");
                        chkCocktail2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + hdnMId.Value + "','" + getRow.Id + "','0','" + divRptId.ClientID + "','6');");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                        lblCocktailMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax1.Text = getRow.MaxCapicity.ToString();
                        lblCocktailMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkCocktail.Visible = false;
                            chkCocktail1.Visible = false;
                            chkCocktail2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkCocktail.Visible = false;
                            chkCocktail1.Visible = false;
                            chkCocktail2.Visible = false;
                        }
                    }
                }
            }

        }
        //}
        //catch (Exception ex)
        //{
        //    logger.Error(ex);
        //}

    }
    #endregion

    #region Continue with checkout
    //Send variable to booking step1 (Continue with checkout)
    protected void lbnContinue_Click(object sender, EventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
            Createbooking objCreateBooking = new Createbooking();
            objCreateBooking.HotelID = Convert.ToInt32(hdnHotelId.Value);
            //Convert.ToInt32(hdnMeetingRoomId.Value)
            //Set All the required Field
            Hotel objHotelDetail = new HotelManager().GetHotelDetailsById(objCreateBooking.HotelID);
            objCreateBooking.ArivalDate = fromDate;
            objCreateBooking.DepartureDate = fromDate.AddDays(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration) - 1);
            if (Session["CurrentRestUserID"] != null)
            {
                objCreateBooking.CurrentUserId = Convert.ToInt64(Session["CurrentRestUserID"]);
            }
            else
            {
                objCreateBooking.CurrentUserId = 0;
            }
            objCreateBooking.Duration = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration);
            //List of Selected meetingroom details
            objCreateBooking.MeetingroomList = new List<BookedMR>();
            for (int i = 0; i < objmeetingroom.Count; i++)
            {
                BookedMR objBookedMR = new BookedMR();
                if (i == 0)
                {

                    objBookedMR.MRId = objmeetingroom[0].Id;
                    objBookedMR.MrConfigId = objConfig[0].Id;
                    //Set Configure for all Meetingroom
                    objBookedMR.MrDetails = new List<BookedMrConfig>();
                    for (int j = 0; j < Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration); j++)
                    {
                        BookedMrConfig objBookedMrConfig = new BookedMrConfig();
                        objBookedMrConfig.NoOfParticepant = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
                        if (j == 0)
                        {
                            objBookedMrConfig.SelectedDay = 1;
                            objBookedMrConfig.SelectedTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays);//FullDay 0 | Morning 1| afrenoon 2
                            objBookedMrConfig.FromTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFFrom : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMFrom : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtAFrom : objHotelDetail.RtFFrom));
                            objBookedMrConfig.ToTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFTo : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMTo : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtATo : objHotelDetail.RtFTo));
                        }
                        else
                        {
                            objBookedMrConfig.SelectedDay = 2;
                            objBookedMrConfig.SelectedTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2);
                            objBookedMrConfig.FromTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFFrom : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMFrom : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtAFrom : objHotelDetail.RtFFrom));
                            objBookedMrConfig.ToTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFTo : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMTo : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtATo : objHotelDetail.RtFTo));
                        }
                        objBookedMR.MrDetails.Add(objBookedMrConfig);
                    }
                }
                if (i == 1)
                {
                    objBookedMR.MRId = objmeetingroom[1].Id;
                    objBookedMR.MrConfigId = objConfig[1].Id;
                    //Set Configure for all Meetingroom
                    objBookedMR.MrDetails = new List<BookedMrConfig>();
                    for (int j = 0; j < Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration); j++)
                    {
                        BookedMrConfig objBookedMrConfig = new BookedMrConfig();
                        objBookedMrConfig.NoOfParticepant = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipantSecondary);
                        if (j == 0)
                        {
                            objBookedMrConfig.SelectedDay = 1;
                            //objBookedMrConfig.SelectedTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays);//FullDay 0 | Morning 1| afrenoon 2
                            objBookedMrConfig.SelectedTime = Convert.ToInt32(hdnType.Value);//FullDay 0 | Morning 1| afrenoon 2
                            objBookedMrConfig.FromTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFFrom : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMFrom : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtAFrom : objHotelDetail.RtFFrom));
                            objBookedMrConfig.ToTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFTo : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMTo : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtATo : objHotelDetail.RtFTo));
                        }
                        else
                        {
                            objBookedMrConfig.SelectedDay = 2;
                            objBookedMrConfig.SelectedTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2);
                            objBookedMrConfig.FromTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFFrom : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMFrom : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtAFrom : objHotelDetail.RtFFrom));
                            objBookedMrConfig.ToTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFTo : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMTo : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtATo : objHotelDetail.RtFTo));
                        }
                        objBookedMR.MrDetails.Add(objBookedMrConfig);
                    }
                }
                objCreateBooking.MeetingroomList.Add(objBookedMR);
            }

            //After Complete this part send this object in term of Session to the Bokking 1st Step Page.
            Session["Search"] = objCreateBooking;
            Response.Redirect("BookingStep1.aspx");
        }
    }
    #endregion

    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        if (XMLLanguage == null)
        {
            XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        }
        XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + key + "']");
        return System.Net.WebUtility.HtmlDecode(nodes.InnerText);// System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion
}