﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;
using LMMR.Data;
using System.Globalization;



#endregion

public partial class SurveyDetailsRequest : System.Web.UI.Page
{
    #region variable declaration
    Surveydata obj = new Surveydata();
    surveybookingrequest objsurveybookingrequest = new surveybookingrequest();
    ServeyAnswer insertsurvey = new ServeyAnswer();
    TList<ServeyAnswer> objsurvey = new TList<ServeyAnswer>();
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    //VList<ViewBooking_Hotel> objbooking = new VList<ViewBooking_Hotel>();
    string status;
    bool status1;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        int Userid = Convert.ToInt32(Request.QueryString["UserID"]);
        int venueid = Convert.ToInt32(Request.QueryString["venue"]);
        int bookingID = Convert.ToInt32(Request.QueryString["bookingID"]);

        if (!IsPostBack)
        {
            bindhotel();
            divmessage.Visible = false;
            lbl1.Text = objsurveybookingrequest.GetSurveyNameByid(12);
            lbl2.Text = objsurveybookingrequest.GetSurveyNameByid(13);
            lbl3.Text = objsurveybookingrequest.GetSurveyNameByid(14);
            lbl4.Text = objsurveybookingrequest.GetSurveyNameByid(15);
            lbl5.Text = objsurveybookingrequest.GetSurveyNameByid(16);
            lbl6.Text = objsurveybookingrequest.GetSurveyNameByid(17);
            lbl7.Text = objsurveybookingrequest.GetSurveyNameByid(18);
            lbl8.Text = objsurveybookingrequest.GetSurveyNameByid(19);
            lbl9.Text = objsurveybookingrequest.GetSurveyNameByid(20);
            lbl10.Text = objsurveybookingrequest.GetSurveyNameByid(21);
            lblcomments.Text = objsurveybookingrequest.GetSurveyNameByid(22);
        }


    }
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        BindRatings();

        if (status == "Thank You For The Survey")
        {
            divsurvey.Visible = true;

            divmessage.Visible = true;
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = status;


        }
        else if (status == "Information could not be saved.")
        {
            divsurvey.Visible = true;
            divmessage.Visible = true;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;

        }
        else
        {
            divsurvey.Visible = true;
            divmessage.Visible = true;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;
        }
    }

    public void BindRatings()
    {
        int Userid = Convert.ToInt32(Request.QueryString["UserID"]);
        int venueid = Convert.ToInt32(Request.QueryString["venue"]);
        int bookingID = Convert.ToInt32(Request.QueryString["bookingID"]);
        Booking onjbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(bookingID));
        Booking onjbooking1 = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(bookingID));
        if (string.IsNullOrEmpty(onjbooking.RequestCollectionId))
            onjbooking.RequestCollectionId = "0";
        if (string.IsNullOrEmpty(onjbooking1.RequestCollectionId))
            onjbooking1.RequestCollectionId = "0";
        if ((onjbooking.IsSurveyDone == false) && ((onjbooking.HotelId == Convert.ToInt64(drphotel.SelectedItem.Value)) || (Convert.ToString(onjbooking.RequestCollectionId) == Convert.ToString(onjbooking1.RequestCollectionId))))
        {
            onjbooking.IsSurveyDone = true;
           

            if (string.IsNullOrEmpty(onjbooking.RequestCollectionId))
            {
                onjbooking.IsSurveyDone = true;
                DataRepository.BookingProvider.Update(onjbooking);
                status1 = DataRepository.BookingProvider.Update(onjbooking);
            }
            else
            {
                int totalcount = 0;
             string   where = " RequestCollectionID='" + onjbooking.RequestCollectionId + "'";
                TList<Booking> alreq = DataRepository.BookingProvider.GetPaged(where, "", 0, int.MaxValue, out totalcount);
                foreach (Booking b1 in alreq)
                {
                    b1.IsSurveyDone = true;
                    DataRepository.BookingProvider.Update(b1);
                    status1 = DataRepository.BookingProvider.Update(onjbooking);
                }
            }        



            if (status1 == true)
            {
                Serveyresult res = new Serveyresult();
                ServeyResponse response = new ServeyResponse();
                response.UserId = Userid;
                response.VanueId = Convert.ToInt64(drphotel.SelectedItem.Value);
                response.BookingId = bookingID;

                response.BookingType = Convert.ToInt32(onjbooking.BookType);
                if (commentsText.Text != "")
                {

                    response.AdditionalComment = commentsText.Text;
                    status = obj.insertSurvey(response);
                    //if (status == "Thank You For The Survey")
                    //{
                    //    res.QuestionId = 4;
                    //    res.AnswerId = 11;
                    //    res.ServeyId = response.ServeyId;
                    //    status = obj.insertSurvey(res);
                    //}
                    //else
                    //{
                    //    divsurvey.Visible = true;
                    //    divmessage.Visible = true;
                    //    divmessage.Attributes.Add("class", "error");
                    //    divmessage.Style.Add("display", "block");
                    //    divmessage.InnerHtml = status;
                    //}

                }
                else
                {
                    status = obj.insertSurvey(response);
                }
                if (status == "Thank You For The Survey")
                {
                    //TList<ServeyQuestion> objquesr = DataRepository.ServeyQuestionProvider.GetAll();

                    //foreach (ServeyQuestion sq in objquesr)
                    //{
                    //    TList<ServeyAnswer> objans = DataRepository.ServeyAnswerProvider.GetByQuestionId(sq.QuestionId);

                    //    foreach (ServeyAnswer sa in objans)
                    //    {
                    //        res.QuestionId = sa.QuestionId;
                    //        res.AnswerId = sa.AnswerId;
                    //        res.Rating = Convert.ToInt32(rbt_State_Venue.SelectedValue);
                    //        res.ServeyId = response.ServeyId;
                    //        status = obj.insertSurvey(res);

                    //    }
                    //}
                            res.QuestionId = 1;
                            res.AnswerId = 1;
                            res.Rating = Convert.ToInt32(rbt_State_Venue.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                     
                            res.QuestionId = 1;
                            res.AnswerId = 2;
                            res.Rating = Convert.ToInt32(rbt_expectations.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);


                            res.QuestionId = 1;
                            res.AnswerId = 3;
                            res.Rating = Convert.ToInt32(rbt_convenience.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);



                            res.QuestionId = 2;
                            res.AnswerId = 4;
                            res.Rating = Convert.ToInt32(rbt_Availability_staff.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                      

                            res.QuestionId = 2;
                            res.AnswerId = 5;
                            res.Rating = Convert.ToInt32(rbt_Staff_Kindness.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                   

                     
                            res.QuestionId = 2;
                            res.AnswerId = 6;
                            res.Rating = Convert.ToInt32(rbt_Staff_Professionalism.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                       

                            res.QuestionId = 3;
                            res.AnswerId = 7;
                            res.Rating = Convert.ToInt32(rbt_Quality_of_food.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                       

                            res.QuestionId = 3;
                            res.AnswerId = 8;
                            res.Rating = Convert.ToInt32(rbt_Quality_of_coffee_breaks.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        

                            res.QuestionId = 3;
                            res.AnswerId = 9;
                            res.Rating = Convert.ToInt32(rbt_Original_and_Creative.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        

                            res.QuestionId = 3;
                            res.AnswerId = 10;
                            res.Rating = Convert.ToInt32(rbt_service_speed.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                }
                else
                {
                    divsurvey.Visible = true;
                    divmessage.Visible = true;
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    divmessage.InnerHtml = status;
                    
                }
            }
            else
            {
                divsurvey.Visible = true;
                divmessage.Visible = true;
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                divmessage.InnerHtml = "Survey did not Save!";
              
            }
        }
        else
        {
            status = "Survey already done for the Hotel";
            divsurvey.Visible = true;
            divmessage.Visible = true;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;
        }


    }

    public void bindhotel()
    {
        int Userid = Convert.ToInt32(Request.QueryString["UserID"]);
        if (Userid != null)
        {
            TList<Booking> objbook = objViewBooking_Hotel.fetchrequestlink(Convert.ToInt32(Request.QueryString["bookingID"]));
            string id ="";
            int val = 0;
            foreach (Booking b1 in objbook)
            {
                if (string.IsNullOrEmpty(id))
                {
                    id = Convert.ToString(b1.Id);
                }
                else
                {
                    id += ","+ Convert.ToString(b1.Id);                
                }

                if(b1.RequestStatus == (int)BookingRequestStatus.Definite)
                {
                    val = 1;      
                }


            }

            if (objbook.Count == 1)
            {
                val = 1;                
            }

         

              string where = "id in ("+ id+")";

            VList<Viewbookingrequest> objview = objViewBooking_Hotel.fetchrequestlinkforDropdown(where);
            drphotel.DataValueField = "HotelId";
            drphotel.DataTextField = "HotelName";
            drphotel.DataSource = objview;
            drphotel.DataBind();

            if (val == 1)
            {
                tblhotel.Visible = false;
                tblhotel1.Visible = false;
            }
            
        
    }
}

    protected void Dropmetingheld_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Dropmetingheld.SelectedItem.Value == "1")
        {
            //divhoteldrop.Visible = true;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "alert('Thank you very much for your feedback, we hope to see you soon on www.lastminutemeetingroom.com'); window.close();", true);
           // divhoteldrop.Visible = false;            
        }
    }
}