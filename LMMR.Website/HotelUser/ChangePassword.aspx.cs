﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_ChangePassword : System.Web.UI.Page
{
    #region Variables and Properties
    PasswordManager objPassword = new PasswordManager();
    int intUserID = 0;
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_ChangePassword));
    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call at the time of Page load.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["CurrentUserID"] != null)
            {
                Session["LinkID"] = "lnkchangePassword";
                intUserID = Convert.ToInt32(Session["CurrentUserID"]);
                if (!IsPostBack)
                {
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                }
            }
            else
            {
                Response.Redirect("~/login.aspx",true);

            }
        }
        catch(Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Events
    /// <summary>
    /// This method is call on the click of save button.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //update pssword here.
            string strMsg = objPassword.UpdatePassword(intUserID, txtOldPassword.Text, txtNewPassword.Text.Trim().Replace("'",""));
            if (strMsg == "Password update Successfully")
            {
                divmessage.InnerHtml = "Password update Successfully.";
                divmessage.Attributes.Add("class", "succesfuly");
                divmessage.Style.Add("display", "block");
            }
            else if (strMsg == "Please enter correct old password.")
            {
                divmessage.InnerHtml = "Please enter correct old password.";
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
            }
            else
            {
                divmessage.InnerHtml = strMsg;
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

        }
    }

    /// <summary>
    /// This method is used to reset the all fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            txtOldPassword.Text = "";
            txtNewPassword.Text = "";
            txtConfirmNewpassword.Text = "";
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion
}