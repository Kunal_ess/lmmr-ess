﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
#endregion

public partial class HotelUser_CancellationPolicy : System.Web.UI.Page
{
    #region variable declaration
    public Hotel hotel;
    public CancelationPolicy policy;
    public CancelationPolicyLanguage policylanguage;
    int hotelPid = 0;
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    DashboardManager Objdash = new DashboardManager();
    HotelManager objHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    CancellationPolicy objCancellation = new CancellationPolicy();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_CancellationPolicy));
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        /// <summary>
        //Session for Hotelname
        /// </summary>

        try
        {
            if (Session["CurrentHotelID"] == null)
            {
                Response.Redirect("~/login.aspx", true);
            }
            hotelPid = Convert.ToInt32(Session["CurrentHotelID"]);

            if (!IsPostBack)
            {
                Session["LinkID"] = "lnkPropertyLevel";
                //Bind policy
                #region Leftmenu
                HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkCancelationPolicy");
                lnk.CssClass = "selected";
                #endregion

                //Call Function for Hotel policy
                BindPolicy();

                //Call function for Goonline
                Getgoonline();
            }
            //Get Hotelname on basis of hotelid
            lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Additional Methods
    /// <summary>
    /// For check go online for this hotel
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(hotelPid).Count > 0)
        {
            divNext.Visible = false;
        }
    }

    /// <summary>
    /// Bind cancellation policy as per country
    /// </summary>
    public void BindPolicy()
    {
        long? PolicyID = 0;
        PolicyHotelMapping objMapping = objCancellation.GetPolicyByHotelID(Convert.ToInt32(Session["CurrentHotelID"])).FirstOrDefault();
        if (objMapping != null)
        {
            PolicyID = objMapping.PolicyId;
        }
        else
        {
            int countryID = Convert.ToInt32(objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).CountryId);
            CancelationPolicy objPolicy = objCancellation.GetPolicyByCountry(countryID).Find(a => a.DefaultCountry == true);
            PolicyID = objPolicy.Id;
        }

        CancelationPolicy GetPolicyByID = objCancellation.GetPolicyByID(Convert.ToInt32(PolicyID));
        if (GetPolicyByID != null)
        {
            int languageID = Convert.ToInt32(objCancellation.GetLanguage().Find(a => a.Name == "English").Id);
            TList<CancelationPolicyLanguage> objCancellationPolicy = objCancellation.GetPolicySectionsByPolicyID(Convert.ToInt32(PolicyID));
            CancelationPolicyLanguage objLanguage = objCancellationPolicy.Find(a => a.LanguageId == languageID);
            lblName.Text = GetPolicyByID.PolicyName;
            lblCalcellationLimit.Text = GetPolicyByID.CancelationLimit.ToString();
            lblTimeFrameMax1.Text = GetPolicyByID.TimeFrameMax1.ToString();
            lblTimeFrameMax2.Text = GetPolicyByID.TimeFrameMax2.ToString();
            lblTimeFrameMax3.Text = GetPolicyByID.TimeFrameMax3.ToString();
            lblTimeFrameMax4.Text = GetPolicyByID.TimeFramMax4.ToString();
            lblTimeFrameMin1.Text = GetPolicyByID.TimeFrameMin1.ToString();
            lblTimeFrameMin2.Text = GetPolicyByID.TimeFrameMin2.ToString();
            lblTimeFrameMin3.Text = GetPolicyByID.TimeFrameMin3.ToString();
            lblTimeFrameMin4.Text = GetPolicyByID.TimeFrameMin4.ToString();

            lblSection1.Text = objLanguage.PolicySection1;
            lblSection2.Text = objLanguage.PolicySection2;
            lblSection3.Text = objLanguage.PolicySection3;
            lblSection4.Text = objLanguage.PolicySection4;

            
        }
        else
        {
            divPolicy.Style.Add("display", "none");
            divMessage.Style.Add("display", "block");
        }

        //hotel = ObjHotelinfo.GetHotelByHotelID(hotelPid);
        //if (objCancellation.GetCancellationPolicy(Convert.ToInt32(hotel.CountryId)).Count > 0)
        //{
        //    policy = objCancellation.GetCancellationPolicy(Convert.ToInt32(hotel.CountryId))[0];
        //    if (hotel.CountryId == policy.CountryId)
        //    {
        //        TList<CancelationPolicyLanguage> lsthPolicyLang = objCancellation.GetCancellationPolicylangByPolicyId(Convert.ToInt32(policy.Id));
        //        if (lsthPolicyLang.Count > 0)
        //        {
        //            divPolicy.Visible = true;
        //            policylanguage = lsthPolicyLang[0];
        //            lblName.Text = policy.PolicyName.ToString();
        //            lblCalcellationLimit.Text = policy.CancelationLimit.ToString();
        //            //lblTimeFrame1.Text = policy.TimeFrame1.ToString();
        //            //lblTime1.Text = policy.TimeFrame1.ToString();
        //            //lblTime2.Text = policy.TimeFrame2.ToString();
        //            //lblTf2.Text = policy.TimeFrame2.ToString();
        //            lblSection1.Text = policylanguage.PolicySection1;
        //            lblSection2.Text = policylanguage.PolicySection2;
        //            lblSection3.Text = policylanguage.PolicySection3;
        //        }
        //        else
        //        {
        //            lblMessage.Visible = true;
        //            lblMessage.Text = "Cancelation Policy does not exist for this hotel";
        //        }
        //    }
        //}
        //else
        //{
        //    lblMessage.Visible = true;
        //    lblMessage.Text = "Cancelation Policy does not exist for this hotel";
        //}
    }
    #endregion

    #region Move to Next page
    /// <summary>
    //For moving to next page
    /// </summary>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        //Objwizard.ThisPageIsDone(hotelPid,        
        Objwizard.ThisPageIsDone(hotelPid, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkCancelationPolicy));
        Objdash.DoneThisStep(hotelPid, Convert.ToInt32(PropertyLevelSection.lnkGeneralInfo));
        Response.Redirect("MeetingRoomDesc.aspx", true);
    }
    #endregion
}