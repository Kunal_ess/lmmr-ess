﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true"
    CodeFile="TestBingMap.aspx.cs" Inherits="HotelUser_TestBingMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">

<script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.3"></script>
    
    <script type="text/javascript">

        var myMap = null;

        var confidenceStrings = ["High", "Medium", "Low"];
        var precisionStrings = ["Interpolated", "Rooftop"];

        var pushpinUrls = ["../Images/PinGreen.png", "../Images/PinOrange.png", "../Images/PinRed.png"];

        function LoadMap() {
            myMap = new VEMap("mapDiv");
            myMap.LoadMap();
        }

        function UnloadMap() {
            if (myMap != null) {
                myMap.Dispose();
            }
        }


        function StartGeocoding(address) {
            myMap.Find(null,         // what
                  address,           // where
                  null,              // VEFindType (always VEFindType.Businesses)
                  null,              // VEShapeLayer (base by default)
                  null,              // start index for results (0 by default)
                  null,              // max number of results (default is 10)
                  null,              // show results? (default is true)
                  null,              // create pushpin for what results? (ignored since what is null)
                  null,              // use default disambiguation? (default is true)
                  null,              // set best map view? (default is true)
                  GeocodeCallback);  // call back function
        }

        function GeocodeCallback(shapeLayer, findResults, places, moreResults, errorMsg) {
            var resHtml = "";

            // if there are no results, display the error message and return
            if (places == null) {
                alert((errorMsg == null) ? "There were no results" : errorMsg);
                return;
            }

            // Create an entry for each VEPlace in the result set
            for (var p = 0; p < places.length; p++) {
                // Gather some info up front
                var place = places[p];
                var location = place.LatLong;
                var confString = confidenceStrings[place.MatchConfidence];
                var precString = precisionStrings[place.Precision];
                var mcVal = MatchCode(place.MatchCode);
                var latitude = location.Latitude;
                var longitude = location.Longitude;

                // create the info box description
                var desc = "Latitude: " + latitude + "<br/>" +
                          "Longitude: " + longitude;
                desc = desc + "<br>Confidence: " + confString;
                desc = desc + "<br>Precision: " + precString;
                desc = desc + "<br>Match Code: " + mcVal;

                // Create a pin at that location, list the latitude & longitude
                var pin = new VEShape(VEShapeType.Pushpin, location);
                pin.SetCustomIcon("<img src='" +
                                 pushpinUrls[place.MatchConfidence] +
                                 "'><span class='pinText'>" +
                                 (p + 1) +
                                 "</span>");
                pin.SetTitle(place.Name);
                pin.SetDescription(desc);
                myMap.AddShape(pin);

                // Add the information to the resultsDiv html, including a link 
                // that recenters the map over the pin
                resHtml = resHtml +
                         "<p><a href='javascript:myMap.SetCenter(new VELatLong(" +
                         latitude +
                         "," +
                         longitude +
                         "));'>";
                resHtml = resHtml +
                         "#" + (p + 1) +
                         ": " + place.Name +
                         " (" + precString +
                         ", " +
                         confString +
                         " confidence, " +
                         mcVal +
                         ")</a></p>";
            }

            // set the resultsDev html when we're done.
            document.getElementById("resultsDiv").innerHTML = resHtml;
        }

        function MatchCode(code) {
            if (code == VEMatchCode.None) {
                return "No match";
            }

            var codeDesc = "";
            var cVal;

            cVal = code & VEMatchCode.Good;
            if (cVal > 0) {
                codeDesc += "Good ";
            }

            cVal = code & VEMatchCode.Ambiguous;
            if (cVal > 0) {
                codeDesc += "Ambiguous ";
            }

            cVal = code & VEMatchCode.UpHierarchy;
            if (cVal > 0) {
                codeDesc += "UpHierarchy ";
            }

            cVal = code & VEMatchCode.Modified;
            if (cVal > 0) {
                codeDesc += "Modified ";
            }

            return (codeDesc + "Match");
        }

        function onGeocodeClick() {
            myMap.Clear();
            address = document.getElementById("txtWhere").value;
            StartGeocoding(address);
        }
    </script>
    <div style="font-family: Arial; font-size: 10pt">
        Address:
        <input id="txtWhere" type="text" style="width: 255px" />&nbsp;
        <input type="button" value="Geocode" onclick="onGeocodeClick()" />
    </div>
    <br />
    <div style="position: relative; width: 400px; height: 400px;" id="mapDiv">
    </div>
    <div id="resultsDiv" style="font-family: Arial; font-size: 8pt; position: relative;
        top: -400px; left: 425px; width: 400px">
    </div>
     <script language="javascript" type="text/javascript">
         jQuery(window).ready(function () {
             LoadMap();
         });

         jQuery(window).unload(function () {
             UnloadMap();
         });

    </script>
</asp:Content>
