﻿<%@ Page Title="Hotel User - Printfact Sheet" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" EnableEventValidation="false" CodeFile="PrintfactSheet.aspx.cs"
    Inherits="HotelUser_PrintfactSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
    </style>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .LinkPaging
        {
            width: 20;
            background-color: White;
            border: Solid 1 Black;
            text-align: center;
            margin-left: 8;
        }
    </style>
    <div id="divGrid" runat="server">
        <div id="divPrint">   
        <table id="tblParent" runat="server"><tr><td>
            <table width="100%" border="0" cellpadding="10" cellspacing="0" class="address" bgcolor="#2D9EEE">
                <tr>
                    <td align="center" width="15%" >
                        <img width="134" height="129" alt="" id="contentbody_imgMainimage" runat="server" />
                    </td>
                    <td align="left" width="67%" >
                        
                            <table cellpadding="1" border="0" cellspacing="0" width="100%" align="left" >
                                <tr>
                                    <th colspan="2" align="left"><h2>
                            <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></h2></th>
                                </tr>
                                <tr>
                                    <td width="14%" align="left">
                                        <b>Address :</b>
                                    </td>
                                    <td align="left" style="width:510px">
                                    <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="14%" align="left">
                                        <b>Telephone :</b>
                                    </td>
                                    <td align="left">
                                    <asp:Label ID="lblTel" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="14%" align="left">
                                        <b>Fax Number :</b>
                                    </td>
                                    <td align="left">
                                    <asp:Label ID="lblFax" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="14%" align="left">
                                        <b>Email :</b>
                                    </td>
                                    <td align="left">
                                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="14%" align="left" valign="top">
                                        <b>Description :</b>
                                    </td>
                                    <td align="left">
                                    <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>

                    </td>
                   <td valign="top" align="left" width="18%" style=" display:none;" ><img id="imgLogo" runat="server" align="left" alt="" /></td>
                </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#d8eefc">
                <tr>
                    <td align="left" class="description-print" valign="top" style="font-size: 22px; padding-bottom:9px;" >
                        Meeting room fact sheet : <b>
                            <asp:Label ID="lblHotelName" runat="server" Text=""></asp:Label></b><br /><br />
                            <b style="font-size: 20px">Size and Capacity</b><br />
                    </td>
                </tr>
            </table>                        
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblid" runat="server">
                <tr>
                    <td bgcolor="#98bcd6">
                        <table width="100%"  cellspacing="1" cellpadding="5" >
                            <tr bgcolor="#c4d6e2">
                                <td width="80" border="1">
                                    Meeting Room
                                </td>
                                <td border="1">
                                    Price
                                </td>
                                <td border="1">
                                    Description
                                </td>
                                <td align="center" width="55" border="1">
                                    Theatre Style
                                </td>
                                <td align="center" width="55" border="1">
                                    School
                                </td>
                                <td align="center" width="55" border="1">
                                    U-Shape
                                </td>
                                <td align="center" width="55" border="1">
                                    Boardroom
                                </td>
                                <td align="center" width="55" border="1">
                                    Cocktail
                                </td>
                            </tr>
                            <tr bgcolor="#c4d6e2">
                                <td border="1">
                                </td>
                                <td border="1">
                                </td>
                                <td border="1">
                                </td>
                                <td align="center" border="1">
                                    <img id="imgTheatre" runat="server" alt="" />
                                </td>
                                <td align="center" border="1">
                                    <img id="imgSchool" runat="server" alt="" />
                                </td>
                                <td align="center" border="1">
                                    <img id="imgShape" runat="server" alt="" />
                                </td>
                                <td align="center" border="1">
                                    <img id="imgBoardroom" runat="server" alt="" />
                                </td>
                                <td align="center" border="1">
                                    <img id="imgCocktail" runat="server" alt="" />
                                </td>
                            </tr>
                            <asp:ListView ID="lstViewMeetingRoomConfig" runat="server" ItemPlaceholderID="itemPlacehoderID"
                                AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewMeetingRoomConfig_ItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr bgcolor="#d8eefc" class="row">
                                        <td valign="top" align="left" border="1">
                                            <img id="imgHotelImage" runat="server" width="57" height="57" /><br />
                                            <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label><br />
                                        </td>
                                        <td border="1">
                                            <asp:Label ID="lblHalfday" runat="server" Text=""></asp:Label><br />
                                            <br>
                                            <asp:Label ID="lblFullday" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td border="1">
                                            <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>                                            
                                        </td>
                                        <td align="center" border="1">
                                            <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label>                                            
                                            <asp:Label ID="lblTheatreMax" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center" border="1">
                                            <asp:Label ID="lblSchoolMin" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblSchoolMax" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center" border="1">
                                            <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblUShapeMax" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center" border="1">
                                            <asp:Label ID="lblBoardroomMin" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblBoardroomMax" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center" border="1">
                                            <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblCocktailMax" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                 <EmptyDataTemplate>
                                <table width="100%" border="0" cellspacing="1" cellpadding="5">                           
                                    <tr><td colspan="8" align="center"><b>No Record found </b></td></tr>
                                </table>
                                  </EmptyDataTemplate>
                            </asp:ListView>
                        </table>
                    </td>
                </tr>
            </table> 
            </td></tr>
            </table>             
        </div>
    </div>
    <div class="booking-details" id="DivPrint" runat="server">
        <ul>
            <li class="value3">
                <div class="col9">
                    <img src="../Images/print.png" />&nbsp; <a id="uiLinkButtonPrintGrid" style="cursor: pointer;"
                        onclick="javascript:Button1_onclick('divPrint');">Print</a> &bull;
                    <img src="../Images/pdf.png" />&nbsp;<asp:LinkButton ID="uiLinkButtonSaveAsPdfGrid"
                        runat="server" OnClick="uiLinkButtonSaveAsPdfGrid_Click">Save as PDF</asp:LinkButton>
                </div>
            </li>
        </ul>
    </div>
    <div class="button_sectionNext" id="divNext" runat="server">
        <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;"
            OnClick="btnNext_Click" />
    </div>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br /><span>Loading...</span></div>
    <script language="javascript" type="text/javascript">
        function Button1_onclick(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
     <script language="javascript" type="text/javascript">
         jQuery(document).ready(function () {
             jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                 jQuery("body").scrollTop(0);
                 jQuery("html").scrollTop(0);
                 jQuery("#Loding_overlaySec span").html("Loading...");
                 jQuery("#Loding_overlaySec").show();
                 document.getElementsByTagName('html')[0].style.overflow = 'hidden';
             });
         });
    </script>
</asp:Content>
