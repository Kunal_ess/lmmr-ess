﻿<%@ Page Title="Hotel User - GDT" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true" CodeFile="GeneralDeleveryTerms.aspx.cs" Inherits="HotelUser_GeneralDelevryTerms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" Runat="Server">
<h1>General Delevry Terms</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" Runat="Server">
    <div class="contact-details">
        <div class="contact-details-inner1">
            <b>
                <asp:Literal ID="lnlInformation" runat="server">
                &#8220;Charges&#8221; means the charges for the Services;<br />
                &#8220;Client&#8221; means the person or entity that purchases the Services from LMMR and this term shall also be deemed to include all persons or entities on behalf of whom that person or entity purchases the Services from LEH;<br />
                &#8220;Contract&#8221; means any contract between LMMR and the Client which incorporates these terms and conditions;<br />
                &#8220;Event&#8221; means the function, match, event or occassion in respect of which the Services are provided;<br />
                &#8220;Reservation&#8221; means a booking, reservation or other similar request for the Services by the Client;<br />
                &#8220;Services&#8221; means the services and arrangements LMMR will provide to the Client under the Contract.<br />

                </asp:Literal></b>
            <br />
            <b>
                <asp:Label ID="Label1" runat="server" Text="For more details you can mail us on contact@lastminutemeetingroom.com"></asp:Label></b><br />
            <br />
            <br />
            <b>
                <asp:Label ID="Label3" runat="server" Text="Thanks, "></asp:Label></b><br />
            <b>
                <asp:Label ID="Label4" runat="server" Text="Lastminutemeetingroom Support"></asp:Label></b>
        </div>
    </div>
</asp:Content>

