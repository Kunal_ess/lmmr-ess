﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.HtmlControls;
using log4net;
using log4net.Config;
using System.Configuration;
using System.Xml;
#endregion
public partial class HotelUser_ViewDetailbooking : System.Web.UI.Page
{
    #region variables
    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_ViewDetailbooking));
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public string UserCurrency
    {
        get { return Convert.ToString(ViewState["UserCurrency"]); }
        set { ViewState["UserCurrency"] = 1; }
    }
    public string HotelCurrency
    {
        get { return Convert.ToString(ViewState["HotelCurrency"]); }
        set { ViewState["HotelCurrency"] = 1; }
    }
    public decimal CurrencyConvert
    {
        get { return Convert.ToDecimal(ViewState["CurrencyConvert"]); }
        set { ViewState["CurrencyConvert"] = 1; }
    }
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomID
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomConfigureID
    {
        get;
        set;
    }
    public int CurrentPackageType
    {
        get;
        set;
    }
    public TList<PackageItems> objHp
    {
        get;
        set;
    }
    public List<ManagePackageItem> CurrentPackageItem
    {
        get;
        set;
    }
    #endregion

    #region Pageload
    /// <summary>
    /// Method to manage the page load functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session check               
            if (Session["CurrentHotelID"] == null)
            {
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
            //Session check
            modalcheckcomm.Hide();

            if (IsPostBack)
            {
                ApplyPaging();

            }
            else if (!IsPostBack)
            {
                divmessage.Style.Add("display", "none");
                Typelink = Convert.ToString(Request.QueryString["type"]);
                //--- if querystring "type" = 0 then its for bookings, else for request
                if (Typelink == "0")
                {

                    Session["LinkID"] = "lnkBookings";
                    Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
                    string hotelname = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
                    lblbookingdetails.Text = "Booking details";
                    lblheadertext.Text = "Search Result list of bookings for the : " + "<b>" + hotelname + "</b>";
                }
                else if (Typelink == "1")
                {
                    Session["LinkID"] = "lnkRequests";
                    Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
                    string hotelname = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
                    lblbookingdetails.Text = "Request details";
                    lblheadertext.Text = "Search Result list of request for the : " + "<b>" + hotelname + "</b>";

                }
                ViewState["SearchAlpha"] = "all";
                bindgrid(Typelink);
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion


    #region BindGrid
    /// <summary>
    /// Method to bind the grid
    /// </summary>
    /// <param name="linktype"></param>
    /// linktype = 0 the its Booking else request   

    protected void bindgrid(string linktype)
    {
        try
        {
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
            int totalcount = 0;
            linktype = Request.QueryString["type"].ToString();
            string whereclaus="";
            if (string.IsNullOrEmpty(Convert.ToString(Session["whereclau"])))
            {
             whereclaus    = "hotelid in (" + Hotelid + ") and booktype = " + linktype;  // book type for request and booking
            }
            else
            {

                whereclaus = Convert.ToString(Session["whereclau"]);
            }
            if (ViewState["SearchAlpha"].ToString() != "all")
            {
                whereclaus += " and Contact like '" + ViewState["SearchAlpha"].ToString() + "%'";
            }
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
            grdViewBooking.DataSource = vlistreq;
            grdViewBooking.DataBind();
            ApplyPaging();

            #region Disableaction header
            if (Typelink == "1")
            {
                grdViewBooking.Columns[8].Visible = true;
               
            }
            else
            {
                grdViewBooking.Columns[8].Visible = false;
               
            }

            #endregion
            divbookingdetails.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Apply Paging
    /// <summary>
    /// Method to apply Paging in grid
    /// </summary>
    private void ApplyPaging()
    {
        try
        {
            if (grdViewBooking.Rows.Count <= 0)
            {
                //divprint.Visible = false;
                return;
            }

            GridViewRow row = grdViewBooking.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewBooking.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdViewBooking.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewBooking.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Search
    /// <summary>
    /// Method to search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        try
        {
            modalcheckcomm.Hide();
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
            Typelink = Convert.ToString(Request.QueryString["type"]);


            string whereclaus = " 1=1 ";

            if (!string.IsNullOrEmpty(txtRefNo.Text))
                whereclaus += " and Id ='" + txtRefNo.Text + "'";
            if (!string.IsNullOrEmpty(txtClientName.Text))
                whereclaus += " and Contact like '" + txtClientName.Text + "%'";
            if (!string.IsNullOrEmpty(txtArrivaldate.Text))
                whereclaus += " and arrivaldate = convert(DATETIME, '" + txtArrivaldate.Text + "',103)";
            if (!string.IsNullOrEmpty(txtFromdate.Text) && !string.IsNullOrEmpty(txtTodate.Text))
                whereclaus += " and arrivaldate between convert(DATETIME, '" + txtFromdate.Text + "',103) and convert(DATETIME , '" + txtTodate.Text + "',103) ";
            else
            {
                if (!string.IsNullOrEmpty(txtFromdate.Text))
                    whereclaus += " and  convert(DATETIME, '" + txtFromdate.Text + "',103) <= arrivaldate ";

                if (!string.IsNullOrEmpty(txtTodate.Text))
                    whereclaus += " and  convert(DATETIME, '" + txtTodate.Text + "',103) >= arrivaldate ";
            }


            whereclaus += " and hotelid in (" + Hotelid + ") and booktype = '" + Typelink + "'";

            Session["whereclau"] = whereclaus;
            int totalcount = 0;
            //  string whereclaus = "hotelid in (" + Hotelid + ") and booktype = 0 "; // book type for request and booking
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);// DataRepository.ViewbookingrequestProvider.GetPaged(whereclaus, "", 0, int.MaxValue, out totalcount);
            grdViewBooking.DataSource = vlistreq;
            grdViewBooking.DataBind();
            ApplyPaging();
            if (grdViewBooking.Rows.Count <= 0)
            {
                //  divAlphabeticPaging.Visible = false;
            }

            divbookingdetails.Visible = false;


        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region View Booking/Request Details
    /// <summary>
    /// Method to view details of request /Booking would be seen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lblRefNo_Click(object sender, EventArgs e)
    {
        try
        {
            //Changed The Code For Using Usercontrol:Pratik//

            LinkButton lnk = (LinkButton)sender;
            //-- viewstate object
            ViewState["BookingID"] = lnk.Text;
            bookingDetails.BindBooking(Convert.ToInt32(lnk.Text));
            divbookingdetails.Visible = true;
            bookingDetails.Visible = true;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }
    #endregion

    #region Bookingbind


    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }

    //Commented These Codes For Using UserControl:Pratik//
    //public void BindBooking()
    //{
    //    //lblArivalDate.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
    //    //lblDepartureDate.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
    //    //lblDuration.Text = objBooking.Duration == 1 ? objBooking.Duration + " Day" : objBooking.Duration + " Days";


    //    lblFromDt.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
    //    lblToDate.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
    //    lblBookedDays.Text = objBooking.Duration == 1 ? objBooking.Duration + " Day" : objBooking.Duration + " Days";

    //    // BindHotelDetails(objBooking.HotelID);

    //    rptMeetingRoom.DataSource = objBooking.MeetingroomList;
    //    rptMeetingRoom.DataBind();
    //    BindAccomodation();
    //    lblSpecialRequest.Text = objBooking.SpecialRequest;
    //    Calculate(objBooking);
    //    lblNetTotal.Text = Math.Round((objBooking.TotalBookingPrice - VatCalculation.Sum(a => a.CalculatedPrice)), 2).ToString();
    //    rptVatList.DataSource = VatCalculation;
    //    rptVatList.DataBind();
    //    lblFinalTotal.Text = Math.Round(objBooking.TotalBookingPrice, 2).ToString();
    //    //TimeSpan t = DateTime.Now.Subtract(DateTime.Now.AddDays(7));
    //    //if (t.Days < 7)
    //    //{
    //    //    lblCancelation.Visible = true;
    //    //    lblCancelation.Text = GetKeyResult("THISBOOKINGCANBECANCELED") + " <b>" + GetKeyResult("FREE") + "</b> " + GetKeyResult("OFCHANGEUNTIL") + " <b> " + DateTime.Now.AddDays(7).ToString("dd MMM yyyy") + "</b>";
    //    //}
    //    //else
    //    //{
    //    //    lblCancelation.Visible = false;
    //    //}
    //}

    //public void BindHotelDetails(Int64 Hotelid)
    //{
    //    Hotel objHotelDetails = objHotel.GetHotelDetailsById(Hotelid);
    //    lblHotelName.Text = objHotelDetails.Name;
    //    lblHotelAddress.Text = objHotelDetails.HotelAddress;
    //    lblHotelPhone.Text = objHotelDetails.PhoneExt + " - " + objHotelDetails.PhoneNumber;
    //    Currency objUserCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(Session["CurrencyID"]));
    //    UserCurrency = objUserCurrency.Currency;
    //    CurrencySign = objUserCurrency.CurrencySignature;
    //    Currency objcurrency = cm.GetCurrencyDetailsByID(objHotelDetails.CurrencyId);
    //    HotelCurrency = objcurrency.Currency;
    //    string currency = CurrencyManager.Currency(HotelCurrency, UserCurrency);
    //    CurrencyConvert = Convert.ToDecimal(currency == "N/A" ? "1" : currency);
    //    CurrentLat = Convert.ToDecimal(objHotelDetails.Latitude);
    //    CurrentLong = Convert.ToDecimal(objHotelDetails.Longitude);
    //    if (objHotelDetails.Stars == 1)
    //    {
    //        imgHotelStars.ImageUrl = "~/Images/1.png";
    //    }
    //    else if (objHotelDetails.Stars == 2)
    //    {
    //        imgHotelStars.ImageUrl = "~/Images/2.png";
    //    }
    //    else if (objHotelDetails.Stars == 3)
    //    {
    //        imgHotelStars.ImageUrl = "~/Images/3.png";
    //    }
    //    else if (objHotelDetails.Stars == 4)
    //    {
    //        imgHotelStars.ImageUrl = "~/Images/4.png";
    //    }
    //    else if (objHotelDetails.Stars == 5)
    //    {
    //        imgHotelStars.ImageUrl = "~/Images/5.png";
    //    }
    //    else if (objHotelDetails.Stars == 6)
    //    {
    //        imgHotelStars.ImageUrl = "~/Images/6.png";
    //    }
    //    else if (objHotelDetails.Stars == 7)
    //    {
    //        imgHotelStars.ImageUrl = "~/Images/7.png";
    //    }
    //}
    //public void BindAccomodation()
    //{
    //    if (objBooking.ManageAccomodationLst.Count > 0)
    //    {
    //        pnlAccomodation.Visible = false;
    //        rptAccomodation.DataSource = objBooking.ManageAccomodationLst;
    //        rptAccomodation.DataBind();
    //        lblAccomodationPrice.Text = Math.Round(objBooking.AccomodationPriceTotal, 2).ToString();
    //    }
    //    else
    //    {
    //        pnlAccomodation.Visible = false;
    //    }
    //}

    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFromTime = (Label)e.Item.FindControl("lblFromTime");
            Label lblToTime = (Label)e.Item.FindControl("lblToTime");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                ManagePackageItem mpi = CurrentPackageItem.Where(a => a.ItemId == p.Id).FirstOrDefault();
                if (mpi != null)
                {
                    lblPackageItem.Text = p.ItemName;
                    lblFromTime.Text = mpi.FromTime;
                    lblQuantity.Text += Convert.ToString(mpi.Quantity);
                    if (p.ItemName.ToLower().Contains("coffee break(s) morning and/or afternoon"))//|| p.ItemName.ToLower().Contains("morning / afternoon") || p.ItemName.ToLower().Contains("morning/ afternoon") || p.ItemName.ToLower().Contains("morning /afternoon") || p.ItemName.ToLower().Contains("morning / afternoon coffee break")
                    {
                        lblToTime.Text += mpi.ToTime;
                        lblToTime.Visible = true;
                    }
                    else
                    {
                        lblToTime.Visible = false;
                        lblQuantity.Visible = false;
                    }
                }


            }
        }
    }
    protected void rptVatList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblVatPrice = (Label)e.Item.FindControl("lblVatPrice");
            VatCollection v = e.Item.DataItem as VatCollection;
            if (v != null)
            {
                lblVatPrice.Text = Math.Round(v.CalculatedPrice, 2).ToString();
            }
        }
    }

    protected void rptAccomodation_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Accomodation objAccomodation = e.Item.DataItem as Accomodation;

            Label lblBedroomName = (Label)e.Item.FindControl("lblBedroomName");
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            Label lblBedroomPrice = (Label)e.Item.FindControl("lblBedroomPrice");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalBedroomPrice = (Label)e.Item.FindControl("lblTotalBedroomPrice");
            if (objAccomodation != null)
            {
                //BedRoom objBedroom = objHotel.GetBedRoomDetailsByBedroomID(objAccomodation.BedroomId);
                //lblBedroomName.Text = Enum.GetName(typeof(BedRoomType), objBedroom.Types);
                //lblBedroomType.Text = objAccomodation.RoomType.ToString();
                //lblBedroomPrice.Text = Math.Round(Convert.ToDecimal(objAccomodation.RoomType == "Single" ? objBedroom.PriceSingle : objBedroom.PriceDouble), 2).ToString();
                //lblQuantity.Text = objAccomodation.Quantity.ToString();
                //lblTotalBedroomPrice.Text = Math.Round(Convert.ToDecimal(objAccomodation.RoomType == "Single" ? objBedroom.PriceSingle : objBedroom.PriceDouble) * objAccomodation.Quantity, 2).ToString();
                //Repeater rptRoomManage = (Repeater)e.Item.FindControl("rptRoomManage");
                //rptRoomManage.DataSource = objAccomodation.RoomManageLst;
                //rptRoomManage.DataBind();
            }
        }
    }
    protected void rptRoomManage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            RoomManage objRoomManage = e.Item.DataItem as RoomManage;
            Label lblRoomIndex = (Label)e.Item.FindControl("lblRoomIndex");
            Label lblAllotmantName = (Label)e.Item.FindControl("lblAllotmantName");
            Label lblCheckIn = (Label)e.Item.FindControl("lblCheckIn");
            Label lblCheckOut = (Label)e.Item.FindControl("lblCheckOut");
            Label lblNote = (Label)e.Item.FindControl("lblNote");
            lblRoomIndex.Text = (e.Item.ItemIndex + 1).ToString();
            lblAllotmantName.Text = objRoomManage.RoomAssignedTo;
            lblCheckIn.Text = objRoomManage.CheckInTime;
            lblCheckOut.Text = objRoomManage.checkoutTime;
            lblNote.Text = objRoomManage.Notes;
        }
    }
    protected void rptMeetingRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMR objBookedMr = e.Item.DataItem as BookedMR;
            CurrentMeetingRoomID = objBookedMr.MRId;
            CurrentMeetingRoomConfigureID = objBookedMr.MrConfigId;

            Repeater rptMeetingRoomConfigure = (Repeater)e.Item.FindControl("rptMeetingRoomConfigure");
            rptMeetingRoomConfigure.DataSource = objBookedMr.MrDetails;
            rptMeetingRoomConfigure.DataBind();
        }
    }


    protected void rptMeetingRoomConfigure_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMrConfig objBookedMrConfig = e.Item.DataItem as BookedMrConfig;
            //Bind Meetingroom//
            MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(CurrentMeetingRoomID);
            MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
            MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == CurrentMeetingRoomConfigureID).FirstOrDefault();

            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblMeetingRoomType = (Label)e.Item.FindControl("lblMeetingRoomType");
            Label lblMinMaxCapacity = (Label)e.Item.FindControl("lblMinMaxCapacity");
            Label lblMeetingRoomActualPrice = (Label)e.Item.FindControl("lblMeetingRoomActualPrice");
            Label lblTotalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalMeetingRoomPrice");
            Label lblTotalFinalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalFinalMeetingRoomPrice");
            lblMeetingRoomName.Text = objMeetingroom.Name;
            lblMeetingRoomType.Text = Enum.GetName(typeof(RoomShape), objMrConfig.RoomShapeId);
            lblMinMaxCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
            lblMeetingRoomActualPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice, 2).ToString();
            lblTotalFinalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice, 2).ToString();
            decimal intDiscountMR = objBookedMrConfig.MeetingroomDiscount;
            lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice, 2).ToString();

            //Bind Other details//
            Label lblSelectedDay = (Label)e.Item.FindControl("lblSelectedDay");
            Label lblStart = (Label)e.Item.FindControl("lblStart");
            Label lblEnd = (Label)e.Item.FindControl("lblEnd");
            Label lblNumberOfParticepant = (Label)e.Item.FindControl("lblNumberOfParticepant");
            lblSelectedDay.Text = objBookedMrConfig.SelectedDay.ToString();
            //lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            lblStart.Text = objBookedMrConfig.FromTime;
            lblEnd.Text = objBookedMrConfig.ToTime;
            lblNumberOfParticepant.Text = objBookedMrConfig.NoOfParticepant.ToString();
            Panel pnlNotIsBreakdown = (Panel)e.Item.FindControl("pnlNotIsBreakdown");
            Panel pnlIsBreakdown = (Panel)e.Item.FindControl("pnlIsBreakdown");
            if (objBookedMrConfig.IsBreakdown == true)
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = true;
                    pnlNotIsBreakdown.Visible = false;
                }
            }
            else
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = false;
                    pnlNotIsBreakdown.Visible = true;
                }
            }
            #region Package and Equipment Collection
            //Bind Package//
            Panel pnlIsPackageSelected = (Panel)e.Item.FindControl("pnlIsPackageSelected");
            Panel pnlBuildMeeting = (Panel)e.Item.FindControl("pnlBuildMeeting");
            HtmlTableCell divpriceMeetingroom = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom");
            HtmlTableCell divpriceMeetingroom1 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom1");
            HtmlTableCell divpriceMeetingroom2 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom2");
            HtmlTableCell divpriceMeetingroom3 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom3");
            HtmlTableCell divpriceMeetingroom4 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom4");
            if (objBookedMrConfig.PackageID == 0)
            {
                pnlIsPackageSelected.Visible = false;
                if (objBookedMrConfig.BuildManageMRLst.Count > 0)
                {
                    pnlBuildMeeting.Visible = true;
                    Repeater rptBuildMeeting = (Repeater)e.Item.FindControl("rptBuildMeeting");
                    Label lblTotalBuildPackagePrice = (Label)e.Item.FindControl("lblTotalBuildPackagePrice");
                    CurrentPackageType = objBookedMrConfig.SelectedTime;
                    rptBuildMeeting.DataSource = objBookedMrConfig.BuildManageMRLst;
                    rptBuildMeeting.DataBind();
                    lblTotalBuildPackagePrice.Text = Math.Round((objBookedMrConfig.BuildPackagePriceTotal - (objBookedMrConfig.MeetingroomPrice)) * CurrencyConvert, 2).ToString();
                }
                else
                {
                    pnlBuildMeeting.Visible = false;
                }
                divpriceMeetingroom.Style.Add("display", "block");
                divpriceMeetingroom1.Style.Add("display", "block");
                divpriceMeetingroom2.Style.Add("display", "block");
                divpriceMeetingroom3.Style.Add("display", "block");
                divpriceMeetingroom4.Style.Add("display", "block");
            }
            else
            {
                pnlIsPackageSelected.Visible = true;
                pnlBuildMeeting.Visible = false;
                Label lblSelectedPackage = (Label)e.Item.FindControl("lblSelectedPackage");
                Label lblPackageDescription = (Label)e.Item.FindControl("lblPackageDescription");
                Label lblPackagePrice = (Label)e.Item.FindControl("lblPackagePrice");
                Label lblTotalPackagePrice = (Label)e.Item.FindControl("lblTotalPackagePrice");
                lblPackagePrice.Text = Math.Round(objBookedMrConfig.PackagePriceTotal * CurrencyConvert, 2).ToString();
                Repeater rptPackageItem = (Repeater)e.Item.FindControl("rptPackageItem");

                PackageByHotel p = objHotel.GetPackageDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageByHotelColumn.PackageId).Where(a => a.PackageId == objBookedMrConfig.PackageID).FirstOrDefault();
                if (p != null)
                {
                    CurrentPackageItem = objBookedMrConfig.ManagePackageLst;
                    rptPackageItem.DataSource = objHotel.GetPackageItemDetailsByPackageID(Convert.ToInt64(p.PackageId));
                    rptPackageItem.DataBind();

                    lblSelectedPackage.Text = p.PackageIdSource.PackageName;
                    if (p.PackageIdSource.PackageName.ToLower() == "favourite")
                    {
                        lblPackageDescription.Text = GetKeyResult("FAVOURITEDESCRIPTION");
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "elegant")
                    {
                        lblPackageDescription.Text = GetKeyResult("ELEGANTDESCRIPTION");
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "standard")
                    {
                        lblPackageDescription.Text = GetKeyResult("STANDARDDESCRIPTION");
                    }
                }
                Panel pnlIsExtra = (Panel)e.Item.FindControl("pnlIsExtra");
                Label lblextratitle = (Label)e.Item.FindControl("lblextratitle");
                Label lblExtraPrice = (Label)e.Item.FindControl("lblExtraPrice");
                if (objBookedMrConfig.ManageExtrasLst.Count > 0)
                {
                    Repeater rptExtra = (Repeater)e.Item.FindControl("rptExtra");
                    objHp = objHotel.GetIsExtraItemDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageItemsColumn.Id);
                    rptExtra.DataSource = objBookedMrConfig.ManageExtrasLst;
                    rptExtra.DataBind();
                    pnlIsExtra.Visible = true;

                    lblExtraPrice.Text = Math.Round(objBookedMrConfig.ExtraPriceTotal * CurrencyConvert, 2).ToString();
                }
                else
                {
                    lblExtraPrice.Visible = false;

                    lblextratitle.Visible = false;
                    pnlIsExtra.Visible = false;

                }
                divpriceMeetingroom.Visible = false;
                divpriceMeetingroom1.Visible = false;
                divpriceMeetingroom2.Visible = false;
                divpriceMeetingroom3.Visible = false;
                divpriceMeetingroom4.Visible = false;
                lblTotalPackagePrice.Text = Math.Round((objBookedMrConfig.PackagePriceTotal + objBookedMrConfig.ExtraPriceTotal) * CurrencyConvert, 2).ToString();
            }
            Panel pnlEquipment = (Panel)e.Item.FindControl("pnlEquipment");
            if (objBookedMrConfig.EquipmentLst.Count > 0)
            {
                pnlEquipment.Visible = true;
                Repeater rptEquipment = (Repeater)e.Item.FindControl("rptEquipment");
                rptEquipment.DataSource = objBookedMrConfig.EquipmentLst;
                rptEquipment.DataBind();
                Label lblTotalEquipmentPrice = (Label)e.Item.FindControl("lblTotalEquipmentPrice");
                lblTotalEquipmentPrice.Text = Math.Round(objBookedMrConfig.EquipmentPriceTotal * CurrencyConvert, 2).ToString();
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            #endregion
        }
    }

    protected void rptExtra_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFrom = (Label)e.Item.FindControl("lblFrom");
            Label lblTo = (Label)e.Item.FindControl("lblTo");
            Label lblQuntity = (Label)e.Item.FindControl("lblQuntity");
            ManageExtras mngExt = e.Item.DataItem as ManageExtras;
            if (mngExt != null)
            {
                PackageItems p = objHp.Where(a => a.Id == mngExt.ItemId).FirstOrDefault();
                if (p != null)
                {
                    lblPackageItem.Text = p.ItemName;
                    lblFrom.Text = mngExt.FromTime;
                    lblTo.Text = mngExt.ToTime;
                    lblQuntity.Text = mngExt.Quantity.ToString();
                }
            }
            else
            {
                lblQuntity.Text = "0";
            }
        }
    }
    protected void rptBuildMeeting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BuildYourMR objBuildYourMR = e.Item.DataItem as BuildYourMR;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalItemPrice = (Label)e.Item.FindControl("lblTotalItemPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.FoodBeverages).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objBuildYourMR.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objBuildYourMR.Quantity.ToString();
                lblTotalItemPrice.Text = Math.Round(objBuildYourMR.ItemPrice * objBuildYourMR.Quantity, 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if(objp!=null)
                //{
                //     = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objBuildYourMR.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptEquipment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageEquipment objManageEquipment = e.Item.DataItem as ManageEquipment;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Equipment).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity, 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if (objp != null)
                //{
                //    lblTotalPrice.Text = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objManageEquipment.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }



    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion

    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                        }
                    }
                    //Manage Package Item
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                        }
                    }

                    //if (PackageSelected)
                    //{
                    //    objBooking.TotalBookingPrice += TotalPackagePrice + TotalExtraPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                    //else
                    //{
                    //    objBooking.TotalBookingPrice += TotalMeetingroomPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                }
            }

        }
    }
    #endregion
    #endregion


    //#region fillbedroom details
    ///// <summary>
    ///// Method to get bedroom details
    ///// </summary>

    //protected void FillbedroomDetails()
    //{
    //    try
    //    {
    //        //BookedBedRoom object
    //        TList<BookedBedRoom> objBookedBR = objViewBooking_Hotel.getbookedBedroom(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

    //        foreach (BookedBedRoom br in objBookedBR)
    //        {
    //            lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), br.BedRoomIdSource.Types); // Enum.GetName();
    //            lblChekOut.Text = String.Format("{0:dd/MM/yyyy}", br.CheckOut);
    //            lblCheckIn.Text = String.Format("{0:dd/MM/yyyy}", br.CheckIn);
    //            lblpersonName.Text = br.PersonName;
    //            lblNote.Text = br.Note;

    //            lblbedroomtotalprice.Text = String.Format("{0:#,###}", br.Total);

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }

    //}
    //#endregion

    #region ItemDataBound
    /// <summary>
    /// ItemDataBound of repeater
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void rpmain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblbmrid = e.Item.FindControl("lblbmrid") as Label;
                Label lblPackageName = e.Item.FindControl("lblPackageName") as Label;
                Label lbltotalpackage = e.Item.FindControl("lbltotalpackage") as Label;
                Label lblextratotal = e.Item.FindControl("lblextratotal") as Label;
                GridView grdDetailspackage = e.Item.FindControl("grdDetailspackage") as GridView;
                GridView grdextra = e.Item.FindControl("grdextra") as GridView;
                GridView grdequipment = e.Item.FindControl("grdequipment") as GridView;

                // BuildPackageConfigure object
                TList<BuildPackageConfigure> obbuildpack = objViewBooking_Hotel.getPackageDetails(Convert.ToInt64(lblbmrid.Text));
                // BuildPackageConfigure object
                TList<BuildMeetingConfigure> objBuildMeetingConfigure = objViewBooking_Hotel.getextra(Convert.ToInt64(lblbmrid.Text));

                grdextra.DataSource = objBuildMeetingConfigure.FindAll(a => a.PackageIdSource.IsExtra == true);
                grdextra.DataBind();
                grdequipment.DataSource = objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment);
                grdequipment.DataBind();
                int cntextra = 0;
                foreach (BuildMeetingConfigure bmc in objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment))
                {
                    cntextra += Convert.ToInt32(bmc.TotalPrice);

                }
                lblextratotal.Text = Convert.ToString(cntextra);
                foreach (BuildPackageConfigure bpc in obbuildpack)
                {

                    lblPackageName.Text = bpc.PackageItemIdSource.PackageName;
                    lbltotalpackage.Text = String.Format("{0:#,###}", bpc.TotalPrice);

                    // BuildPackageConfigureDesc object
                    TList<BuildPackageConfigureDesc> obdesc = objViewBooking_Hotel.getpackagedetails(bpc.Id);
                    grdDetailspackage.DataSource = obdesc;
                    grdDetailspackage.DataBind();

                }


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region PageIndexChanging
    /// <summary>
    /// PageIndex Changing of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewBooking.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            bindgrid(Typelink);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region RowDataBound
    /// <summary>
    /// RowDataBound of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            foreach (GridViewRow gr in grdViewBooking.Rows)
            {

                LinkButton btnchkcommision = (LinkButton)gr.FindControl("btnchkcommision");

                Label lblArrivalDt = (Label)gr.FindControl("lblArrivalDt");
                Label lblBookingDt = (Label)gr.FindControl("lblBookingDt");
                LinkButton lblRefNo = (LinkButton)gr.FindControl("lblRefNo");
                TList<BookedMeetingRoom> bookedMR = objViewBooking_Hotel.getbookedmeetingroom(Convert.ToInt64(lblRefNo.Text));
                Label lblDepartureDt = (Label)gr.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)gr.FindControl("lblExpiryDt");
                Label lblstatus = (Label)gr.FindControl("lblstatus");
                string depdt = lblDepartureDt.Text;
                //#region ArrivalDate
                //foreach (BookedMeetingRoom br in bookedMR)
                //{
                //    DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                //    lblArrivalDt.Text = dtmeetingdt.ToString("dd/MM/yy") + "-" + br.StartTime.ToString();
                //    break;
                //}
                //#endregion

                #region Bindrequeststaus

                lblstatus.Text = Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(lblstatus.ToolTip));

                #endregion

                              DateTime startTime = DateTime.Now;
                DateTime endTime = new DateTime(Convert.ToInt32(depdt.Split('/')[2]), Convert.ToInt32(depdt.Split('/')[1]), Convert.ToInt32(depdt.Split('/')[0]));
                TimeSpan span = endTime.Subtract(startTime);
                int spn = span.Days;

              

                    //#region change button visile   //The Check revenue button will only been seen two days after the meeting. According to SRS v1.1 , Sharon
                    if (btnchkcommision.ToolTip.ToLower() == "false" || string.IsNullOrEmpty(btnchkcommision.ToolTip))
                    {

                        if (spn < 0)
                        {
                            btnchkcommision.Text = "Check commission";
                            btnchkcommision.Visible = true;
                            ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                        }
                        else
                        {
                            btnchkcommision.Visible = false;
                            ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;

                        }
                    }
                    else if (btnchkcommision.ToolTip.ToLower() == "true")
                    {
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = true;
                    }
                    else
                    {
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }


                
                if (lblstatus.Text.ToString() == BookingRequestStatus.Processed.ToString())
                {
                    btnchkcommision.Visible = false;
                }

                if (lblstatus.Text.ToString() == BookingRequestStatus.New.ToString())
                {
                    btnchkcommision.Visible = false;
                    ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                }
                #region frozen
                if (lblstatus.Text == BookingRequestStatus.Frozen.ToString())
                {
                    lblRefNo.Enabled = false;
                    gr.BackColor = System.Drawing.Color.LightGray;
                    btnchkcommision.Visible = false;
                    ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    btnchkcommision.Visible = false;
                }

                #endregion


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Check Commission
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;
            Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"]));
            lblrevenue.Text = Convert.ToString(bookcomm.RevenueAmount);
            txtrealvalue.Text = "";
            //txtrealvalue.ReadOnly = true;
            ddlreason.SelectedIndex = 0;
            //chkconfirmrevenue.Checked = true;
            
            div1.Style.Add("display", "none");
            modalcheckcomm.Show();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Confirm Commsion
    /// <summary>
    /// when check commsion was confirmed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //Booking object
            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (chkconfirmrevenue.Checked)
            {
                objbooking.ConfirmRevenueAmount = Convert.ToDecimal(lblrevenue.Text);
                objbooking.RevenueReason = "";

                #region check file upload
                string strPlanName = string.Empty;
                if (ulPlan.HasFile)
                {
                    string fileExtension = Path.GetExtension(ulPlan.PostedFile.FileName.ToString());

                    if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".doc" || fileExtension == ".docx" || fileExtension.ToLower() == ".pdf")
                    {
                        strPlanName = Path.GetFileName(ulPlan.FileName);
                    }
                    else
                    {
                        div1.InnerHtml = "Please select file in (word,excel,pdf) formats only";
                        div1.Attributes.Add("class", "error");
                        div1.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }

                    if (ulPlan.PostedFile.ContentLength > 1048576)
                    {
                        div1.InnerHtml = "Filesize of Supporting Document is too large. Maximum file size permitted is 1 MB.";
                        div1.Attributes.Add("class", "error");
                        div1.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }

                }
                else
                {

                    div1.InnerHtml = "Kindly upload the Supporting Document.";
                    div1.Attributes.Add("class", "error");
                    div1.Style.Add("display", "block");
                    modalcheckcomm.Show();
                    return;
                }

                #endregion

                ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "SupportDocNetto/") + strPlanName);
                objbooking.SupportingDocNetto = strPlanName;
            }
            else
            {
                #region check file upload
                string strPlanName = string.Empty;
                if (ulPlan.HasFile)
                {
                    string fileExtension = Path.GetExtension(ulPlan.PostedFile.FileName.ToString());

                    if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".doc" || fileExtension == ".docx" || fileExtension.ToLower() == ".pdf")
                    {
                        strPlanName = Path.GetFileName(ulPlan.FileName);
                    }
                    else
                    {
                        div1.InnerHtml = "Please select file in (word,excel,pdf) formats only";
                        div1.Attributes.Add("class", "error");
                        div1.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }

                    if (ulPlan.PostedFile.ContentLength > 1048576)
                    {
                        div1.InnerHtml = "Filesize of Supporting Document is too large. Maximum file size permitted is 1 MB.";
                        div1.Attributes.Add("class", "error");
                        div1.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtrealvalue.Text))
                    {
                        div1.InnerHtml = "Kindly enter the Netto value";
                        div1.Attributes.Add("class", "error");
                        div1.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(txtrealvalue.Text))
                        div1.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
                    else
                        div1.InnerHtml = "Kindly upload the Supporting Document.";
                    div1.Attributes.Add("class", "error");
                    div1.Style.Add("display", "block");
                    modalcheckcomm.Show();
                    return;
                }

                #endregion
                if (string.IsNullOrEmpty(txtrealvalue.Text))
                    txtrealvalue.Text = "0";
                ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "SupportDocNetto/") + strPlanName);
                decimal netvalue = Convert.ToDecimal(txtrealvalue.Text);
              //  decimal revenue = (Convert.ToDecimal(objbooking.FinalTotalPrice) / (netvalue - 1)) * 100;
                objbooking.ConfirmRevenueAmount = netvalue;
                objbooking.RevenueReason = ddlreason.SelectedItem.Text;
                objbooking.SupportingDocNetto = strPlanName;
            }

            if (objViewBooking_Hotel.updatecheckComm(objbooking))
            {
                //if (objViewBooking_Hotel.updaterequestStatus(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString())))
                //{
                    // FetchListofHotel();
                    bindgrid(Typelink);

               // }

            }
            txtrealvalue.Text = "";
            ddlreason.SelectedIndex = 0;
            chkconfirmrevenue.Checked = true;
          
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
     

    }
    #endregion

    #region CheckedChanged Confirm revenue
    /// <summary>
    /// when check commsion was checked/unchecked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void chkconfirmrevenue_CheckedChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    if (chkconfirmrevenue.Checked)
        //    {
        //        txtrealvalue.ReadOnly = true;
        //        txtrealvalue.Text = "";
        //        ddlreason.SelectedIndex = 0;
        //        btnSubmit.ValidationGroup = "novalidation";
        //    }
        //    else
        //    {
        //        txtrealvalue.ReadOnly = false;
        //        ddlreason.SelectedIndex = 0;
        //        btnSubmit.ValidationGroup = "popup";
        //    }
        //    divmessage.Style.Add("display", "none");

        //    modalcheckcomm.Show();
        //}
        //catch (Exception ex)
        //{
        //    logger.Error(ex);
        //}
    }
    #endregion

   

    #region Aplhabetic Paging
    /// <summary>
    /// alphabetic paging is done
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            HtmlAnchor anc = (sender as HtmlAnchor);
            Hotelid = Convert.ToString(Session["CurrentHotelID"]);
            int totalcount = 0;
            string whereclaus = "";
            var d = anc.InnerHtml;
         

            if (string.IsNullOrEmpty(Convert.ToString(Session["whereclau"])))
            {
                whereclaus = "hotelid in (" + Hotelid + ") and booktype = " + Convert.ToString(Request.QueryString["type"]);  // book type for request and booking
            }
            else
            {

                whereclaus = Convert.ToString(Session["whereclau"]);
            }
            
            if (d == "all")
            {
                whereclaus += "";
                ViewState["SearchAlpha"] = d;
            }
            else
            {
                whereclaus += " and Contact like '" + d + "%'";
                ViewState["SearchAlpha"] = d;

            }


            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);// DataRepository.ViewbookingrequestProvider.GetPaged(whereclaus, "", 0, int.MaxValue, out totalcount);
            grdViewBooking.DataSource = vlistreq;
            grdViewBooking.DataBind();
            if (vlistreq.Count > 0)
            {
                grdViewBooking.DataSource = vlistreq;
                grdViewBooking.DataBind();
                ApplyPaging();
               // Session["whereclau"] = null;
            }
            else
            {
                grdViewBooking.DataSource = null;
                grdViewBooking.DataBind();
            }
            //ViewState["SearchAlpha"] = "all";
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Cancel Button Click
    /// <summary>
    /// Modal popup cancel event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            div1.InnerHtml = "";
            div1.Style.Add("display", "none");
            modalcheckcomm.Hide();

            txtrealvalue.Text = "";
           chkconfirmrevenue.Checked = false;
            
            ddlreason.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    #region RowCreated
    /// <summary>
    /// method to RowCreated
    /// </summary>
    protected void grdViewBooking_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //string rowID = String.Empty;
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FF9'");
            //    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            //    ViewState["rowID"] = e.Row.RowIndex;

            //}
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    #region clear button
    /// <summary>
    /// method to clear search fields
    /// </summary>
    protected void lnkclear_Click(object sender, EventArgs e)
    {
        txtArrivaldate.Text = "";
        txtClientName.Text = "";
        txtFromdate.Text = "";
        txtRefNo.Text = "";
        txtTodate.Text = "";
        ViewState["SearchAlpha"] = "all";
        Session["whereclau"] = null;
        bindgrid(Convert.ToString(Request.QueryString["type"]));
    }
    #endregion
}