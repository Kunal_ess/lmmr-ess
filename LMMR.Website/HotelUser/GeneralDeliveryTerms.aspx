﻿<%@ Page Title="Hotel User - GDT" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true" CodeFile="GeneralDeliveryTerms.aspx.cs" Inherits="HotelUser_GeneralDelevryTerms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" Runat="Server">
<h1>General Delivery Terms</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" Runat="Server">
    <div class="contact-details">
        <div class="contact-details-inner1">
         <div align="center">
       <b style="font-size: medium">  General Delivery Terms (GDT) E-Hospitality BVBA </b>
         </div>





            <br />
            <br />
            A.&nbsp;&nbsp;&nbsp;&nbsp;	BVBA E-HOSPITALITY, whose registered office is at Waarbeek 16, 1730 Asse, 
VAT No.: BE0842 787 468

hereinafter referred to as the "intermediary"

            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and<br />
&nbsp;B.&nbsp;&nbsp;&nbsp;&nbsp; The applicant (see order form) 


have agreed as follows:
            <br />
            <br />
           <u> <strong>Parties</strong></u>

            <br />

            <br />
            A.	BVBA E-Hospitality
            <br />
            B.	The applicant

            <br />
            <br />
            <span style="text-decoration: underline"><strong>Definitions</strong></span>
            <br />
            <br />
            A.	The &#8220;intermediary&#8221; means BVBA E-Hospitality, the company that owns the www.lastminutemeetingroom.com website.<br />
            <br />

B.	The &#8220;applicant&#8221; means the company that enters into a non-exclusive intermediary service agreement with the intermediary in order to promote the facilities described below.<br />
            <br />

C.	The &#8220;visitor&#8221; means any visitor/user, both professional and consumer, of the website for the purpose of making bookings.

            <br />

            <br />
            D.	The &#8220;facilities&#8221; consist of the renting out of meeting rooms with or without ancillary services.

            <br />

            <br />
            E.	The &#8220;web module&#8221; is a software program based on internet technologies that should enable the intermediary to display meeting and accommodation products online and make them bookable. The program controls communication between the applicant and the consumer for processing online bookings and online proposals.


            <br />
            <br />
            <strong>1.&nbsp;&nbsp;&nbsp;&nbsp; User right </strong>
            <br />
            The user right that is granted to the applicant is limited to uploading the business data, availability, prices, special offers and images presented via the WEB MODULE. The applicant is not entitled to rent out, lease, sublicense or sell the user right to the WEB MODULE, grant limited rights to it, or make it available to third parties.
            <br />
            <strong> 

            <br />
            2.&nbsp;&nbsp;&nbsp;&nbsp; Registration and fee </strong>
            <br />
            After the applicant has successfully completed the registration procedure and paid the registration fee, E-Hospitality shall grant the applicant the right to use the web module. If the applicant fails to meet the payment deadline, E-Hospitality has the right to unilaterally suspend the contract until proof of payment is provided.
            <br />




            <br />
            <strong>3.&nbsp;&nbsp;&nbsp;&nbsp; Payment conditions</strong><br />

Any complaints concerning invoicing commission, contracting or services supplied shall be lodged by registered letter with BVBA E-Hospitality within eight days of the date on which the invoice was dispatched, subject to nullity.

            <br />
            <br />
            Invoices shall be paid by bank transfer to the following account:<br />
            <br />
&nbsp;BNP PARIBAS FORTIS &#8211; BE79 0016 5970 2433 

            <br />
            <br />
            specifying the reference number, within 30 calendar days. In case of failure to pay, interest will be payable, ipso jure and without the need for any notice of default, from the first calendar day after the abovementioned deadline. This interest shall be equal to the legal interest + 3 per cent. Moreover, in the event of a failure to pay within 60 days, the amount due shall, ipso jure and without notice of default, be increased by 10% because of failure to comply with the obligation to pay in time.<br />
            <strong>
            <br />

4.&nbsp;&nbsp;&nbsp;&nbsp; Term of the Agreement </strong>

            <br />
            The user right is valid for a period of one year and is renewable provided that the annual registration fee is paid. 

            <br />
            <br />
            At the end of this period, the contract shall be automatically renewed if the applicant has not served notice of termination of the agreement no later than one month before the end of the agreement (by sending a signed and dated letter).

            <br />
            <br />
            The agreement can at all times be terminated by E-Hospitality with immediate effect if the client fails to comply with the conditions. 

            <br />
            E-Hospitality may terminate the user right if the applicant violates one of the general conditions as described. In such case,<br />
&nbsp;E-Hospitality shall call upon the applicant by email and by registered letter to comply with the general conditions, subject to termination within 14 days.  
            <br />
            <br />
            <strong>5.&nbsp;&nbsp;&nbsp;&nbsp; Licence & Commission </strong>

            <br />
            The applicant shall pay for the use of this web module through an annual licence. If a commercial discount has been granted, it shall apply only in respect of the current year.
E-Hospitality reserves the right to apply the official prices, as indicated on the order form, when renewing the contract.  

            <br />
            The agreement is subject to indexation in accordance with the following formula:<br />
            <span style="text-decoration: underline"><br />

Licence price * new index (*)
  </span>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Starting index

            <br />
            <br />
            Licence price: the price is based on the official prices indicated on the order form.<br />
&nbsp;New index: health index of the month preceding the anniversary of the entry into force. 
            <br />
            Starting index: The index of the month preceding the month in which the agreement was concluded.  

In order to be able to use the web module, the applicant has to agree to the commission scheme connected with each booking or price proposal leading to a booking.  

            <br />
            <br />
            Booking or application between 60-30 days before date of arrival: 8% commission

            <br />
            Booking or application between 29-14 days before date of arrival: 9% commission

            <br />
            Booking or application between 13-7 days before date of arrival:   10% commission

            <br />
            Booking or application between 6-0 days before date of arrival:     12% commission

            <br />
            <br />
            All commission is calculated on the basis of the net totals of each booking (booking or confirmed applications) effectively executed.<br />
&nbsp;E-Hospitality has the right to adjust the commission proportionally if there is an increase in web hosting costs, API costs and other interface costs. Any adjustment of charges shall be notified to the applicant at the latest 14 days before the renewal of the contract. The applicant shall then have five working days to inform E-Hospitality that he wishes to terminate the contract by the date on which the change in charges takes effect.


            <br />
            <strong>
            <br />
            6.&nbsp;&nbsp;&nbsp;&nbsp; Protection of privacy </strong>

            <br />
            a)	The applicant grants the INTERMEDIARY the right to process personal data and other data it needs to manage the website. The applicant has the right to access his personal data and to rectify any corrections to his data if they are wrong. 
            <br />
            b)	The applicant shall immediately inform the INTERMEDIARY of any change of name, address, email, telephone and fax numbers. Any negligence or delay in this regard may lead to cancellation of the user right.  
            <br />
            c)	The applicant allows the INTERMEDIARY, in addition to a number of technical data, to also make the following personal data retrievable through its website in order to ensure public transparency:
            <br />
            o	Applicant's name, address, telephone and fax number;
            <br />
            o	Registration date + domain name status;<br />
o	Applicant's email address;
            <br />
            o	Language chosen 
            <br />
            <br />
            The applicant also allows the INTERMEDIARY to pass these data on to third parties for inclusion in a publically available guide. The applicant may at all times and without giving reasons inform the INTERMEDIARY by email at <a href="mailto:hotelsupport@lastminutemeetingroom.com">hotelsupport@lastminutemeetingroom.com</a> that his personal data shall not be passed on. The INTERMEDIARY shall in this case take all the measures necessary to halt the data being passed within five working days. However, such a request does not prevent the data from still remaining retrievable through the intermediary's website or the website of other companies under E-Hospitality.

            <br />
            <br />
            By derogation from the two preceding paragraphs, the applicant's telephone and fax number shall not be retrievable through the website and shall not be passed on to third parties if the applicant is a natural person.

            <br />
            <br />
            Any information concerning credit card numbers shall be used only for the following purposes:

            <br />
            By way of booking guarantee (if a booking is made in a period between 6 and 0 days prior to arrival), the applicant shall receive the visitor's credit card data. However, it is expressly agreed that the applicant may not record any pre-authorisation or payment without the visitor's written approval.<br />

The visitor's credit card data shall be sent to the applicant through a secure fax server. This will enable the applicant to debit a visitor's account on the basis of these data in case of failure to cancel, late cancelling or no show of a booking registered in the WEB MODULE which is not honoured because of the visitor. These data are clearly indicated with each booking and should be accepted by the visitor before a booking can be confirmed.<br />
            <strong>
            <br />
7.&nbsp;&nbsp;&nbsp;&nbsp; Limited liability </strong>
            <br />
            The INTERMEDIARY is not responsible for any damage, including direct or indirect damage, loss of profit, misuse of credit cards resulting from an agreement or from an unlawful act (including negligence), as a result of or relating to the registration or use of the software on his website, even if the INTERMEDIARY had been informed beforehand of the possibility of such damage, e.g. with regard to: 
            <br />
            o	registration or extension (or failure to register or extend) for an applicant as a result of an error concerning their identity;
            <br />
            o	technical problems or errors;
            <br />
            o	acts or negligence on the part of the intermediary regarding the application, booking or extension resulting in non-registration or deletion. 

            <br />
            <br />
            The INTERMEDIARY will make every endeavour to offer his services in accordance with the best practice standards that have been drawn up and approved at national or international level.

            <br />
            <br />
            The applicant shall hold the INTERMEDIARY harmless against any claim (and concomitant costs, including lawyers' fees) resulting from use or registration infringing the rights of a third party.

            <br />
            E-Hospitality is not liable for the contents of the website which the applicant puts on his computer installation. Nor is E-Hospitality liable for any failure of the service or the poor operation of the service due to third parties or external factors.

            <br />
            <br />
            <strong>8.&nbsp;&nbsp;&nbsp;&nbsp; Guarantees of E-Hospitality </strong>
            <br />
            E-Hospitality guarantees that the website is backed up every 24 hours so that it can be restored in emergencies.

            <br />
            E-Hospitality guarantees proper access to the website insofar as any mistakes or delays are not due to third parties or external causes.  




            <br />
            <strong>
            <br />
            9.&nbsp;&nbsp;&nbsp;&nbsp; E-Hospitality installation/maintenance </strong>
            <br />
            <em>Installation:</em>
            <br />
            The applicant compiles his web pages on his own machine and puts them directly on the  E-Hospitality server by gaining access, after logging in via FTP, to the area provided by E-Hospitality, using his user name and password.  

            <br />
            <em>Maintenance:<br />
            </em>The applicant himself is responsible for any back-end adaptations.

            <br />
            <strong>
            <br />
            10.&nbsp;&nbsp;&nbsp;&nbsp; Evidence </strong>
            <br />
            Electronic communication between E-Hospitality and the applicant has the same binding force as a written document and is accepted by the parties as evidence.  


            <br />
            <strong>
            <br />
            11.&nbsp;&nbsp;&nbsp;&nbsp;	Disputes </strong>
            <br />
            Any dispute between the applicant and the INTERMEDIARY shall be settled exclusively by the courts of Brussels, with application of Belgian law.  


            <br />
            <br />
            <strong>E-Hospitality </strong>  

        </div>
    </div>
</asp:Content>

