﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_GeneralDelevryTerms : System.Web.UI.Page
{
    #region Variables and Properties
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_GeneralDelevryTerms));
    #endregion

    #region Page Load
    /// <summary>
    /// Set the Initial Values on Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}