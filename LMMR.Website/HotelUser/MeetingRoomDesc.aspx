﻿<%@ Page Title="Hotel User - Meeting Room Desc" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="MeetingRoomDesc.aspx.cs" Inherits="HotelUser_MeetingRoomDesc" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        Description of meeting rooms : <b>
            <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetLanguageNameByID(languageID) {
            jQuery.ajax({
                type: "POST",
                url: "MeetingRoomDesc.aspx/GetNameByLanguageID",
                data: "{'languageID':'" + parseInt(languageID) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d == 'English') {
                            jQuery('#liImageUpload').show();
                            jQuery('#liPlanUpload').show();
                        }
                        else {
                            jQuery('#liImageUpload').hide();
                            jQuery('#liPlanUpload').hide();
                        }
                    }
                }
            });
        }
        //This function using for get language id by click on language tab.
        function GetLanguageID(languageID) {
            GetLanguageNameByID(languageID);
            document.getElementById("<%= hdnIDS.ClientID %>").value = languageID.toString();
            //var meetingroomID = document.getElementById("<%= hndMeetingRoomID.ClientID %>").value;           
            //alert(meetingroomID);
            jQuery("#Loding_overlaySec span").html("Loading...");
            jQuery("#Loding_overlaySec").show();
            jQuery.ajax({
                type: "POST",
                url: "MeetingRoomDesc.aspx/GetDescByLanguageID",
                data: "{'languageID':'" + parseInt(languageID) + "','meetingroomID':'" + parseInt(jQuery('#<%=hndMeetingRoomID.ClientID %>').val()) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    var result = jQuery.parseJSON(msg.d)
                    if (result != null) {

                        document.getElementById("<%= txtDesc.ClientID %>").value = result.Description;
                        document.getElementById("<%= hdnMeetingRoomDescID.ClientID %>").value = result.Id;

                    } else {

                        document.getElementById("<%= txtDesc.ClientID %>").value = "";
                        document.getElementById("<%= hdnMeetingRoomDescID.ClientID %>").value = "0";
                    }
                    jQuery("#Loding_overlaySec span").html("Loading...");
                    jQuery("#Loding_overlaySec").hide();
                }

            });
        }

        function ConfirmOnDelete() {
            return confirm("Are you sure you want to delete it?");
        }

        jQuery(document).ready(function () {

            CallMyMethod();

        });
        function CallMyMethod() {
            var SeletedRowID = jQuery('#<%=hdnSelectedRowID.ClientID %>').val();
            if (SeletedRowID != 0) {
                jQuery('#' + SeletedRowID).addClass("changerow");
            }
            jQuery('.rowElem').jqTransform({ imgPath: ' ' });
            LanguageTabJavascript();

            var MaxLength = 250;
            //            jQuery('#<%=txtDesc.ClientID %>').keypress(function (e) {
            //                var keycode = e.keyCode ? e.keyCode : e.which;
            //                if (jQuery(this).val().length >= MaxLength && keycode != 8) {
            //                    e.preventDefault();
            //                }
            //            });
            jQuery('#<%=txtDesc.ClientID %>').keyup(function (e) {
                var total = parseInt(jQuery(this).val().length);
                jQuery("#lblCount").html('Characters entered <b>' + total + '</b> out of 250.');
            });
        }
        function ChangeRowColor(rowID) {
            jQuery('#' + rowID).addClass("changerow");
            jQuery('#<%=hdnSelectedRowID.ClientID %>').val(rowID);
        }

        function LanguageTabJavascript() {
            var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
        }  
    </script>
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hdnIDS" runat="server" />
            <asp:HiddenField ID="hdnMeetingRoomName" Value="" runat="server" />
            <asp:HiddenField ID="hdnImageName" runat="server" Value="" />
            <asp:HiddenField ID="hdnPlanName" runat="server" Value="" />
            <asp:HiddenField ID="hndMeetingRoomID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnMeetingRoomDescID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnSelectedRowID" runat="server" Value="0" />
            <div style="float: left;">
                <div id="divmessage" runat="server">
                </div>
                <div id="addmeeting" class="meeting-descrip-btn" runat="server">
                    <asp:LinkButton ID="lnkAddNewMeetingRoom" runat="server" OnClick="lnkAddNewMeetingRoom_Click">Add new meeting room</asp:LinkButton>
                </div>
            </div>
            <asp:ListView ID="lstViewMeetingRoomDesc" runat="server" ItemPlaceholderID="itemPlacehoderID"
                AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewMeetingRoomDesc_ItemDataBound"
                OnItemCommand="lstViewMeetingRoomDesc_ItemCommand">
                <LayoutTemplate>
                    <div class="meeting-description-heading">
                        <div class="meeting-description-heading-box1">
                            &nbsp;
                        </div>
                        <div class="meeting-description-heading-box2">
                            Meeting Room
                        </div>
                        <div class="meeting-description-heading-box3">
                            Daylight
                        </div>
                        <div class="meeting-description-heading-box4">
                            Surf(sqm)
                        </div>
                        <div class="meeting-description-heading-box5">
                            Height
                        </div>
                        <div class="meeting-description-heading-box6">
                            Description
                        </div>
                        <div class="meeting-description-heading-box7">
                            Photo
                        </div>
                        <div class="meeting-description-heading-box8">
                            Plan
                        </div>
                        <div class="meeting-description-heading-box9">
                            Order
                        </div>
                    </div>
                    <div class="meeting-description-list">
                        <ul>
                            <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                        </ul>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <li id="li<%# Eval("Id") %>">
                        <div class="meeting-description-list-box1">
                            <asp:ImageButton ID="imgDeleteBtn" CommandName="deleteItem" CommandArgument='<%#Eval("Id") %>'
                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                ToolTip="Delete" />
                        </div>
                        <div class="meeting-description-list-box2">
                            <div class="newtext">
                                <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                            </div>
                            <span class="modify">
                                <asp:Button CssClass="modify-i" ID="lnkModify" CommandName="View" CommandArgument='<%#Eval("Id") %>'
                                    runat="server" Text="Modify"></asp:Button></span>
                        </div>
                        <div class="meeting-description-list-box3">
                            <asp:Label ID="lblDayLight" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="meeting-description-list-box4">
                            <asp:Label ID="lblSurf" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="meeting-description-list-box5">
                            <asp:Label ID="lblHeight" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="meeting-description-list-box6">
                            <asp:Label ID="lblDescription" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="meeting-description-list-box7">
                            <asp:Image ID="imgMeetingRoom" runat="server" />
                        </div>
                        <div class="meeting-description-list-box8">
                            <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank">
                                <asp:Image ID="imgPlan" runat="server" src="../images/pdf-icon.png" />
                            </asp:HyperLink>
                        </div>
                        <div class="meeting-description-list-box9">
                            <asp:ImageButton ID="btnMoveUp" CommandName="Swap" Style="float: left;" ImageUrl="../images/up.png"
                                runat="server" />
                            <asp:ImageButton ID="btnMoveDown" CommandName="Swap" ImageUrl="../images/down.png"
                                runat="server" />
                            <asp:HiddenField ID="hdnOrderNumber" runat="server" Value='<%# Eval("OrderNumber")%>' />
                        </div>
                    </li>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <li class="light" id="li<%# Eval("Id") %>">
                        <div class="meeting-description-list-box1">
                            <asp:ImageButton ID="imgDeleteBtn" CommandName="deleteItem" CommandArgument='<%#Eval("Id") %>'
                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                ToolTip="Delete" />
                        </div>
                        <div class="meeting-description-list-box2">
                            <div class="newtext">
                                <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                            </div>
                            <span class=" modify">
                                <asp:Button ID="lnkModify" CssClass="modify-i" CommandName="View" CommandArgument='<%#Eval("Id") %>'
                                    runat="server" Text="Modify"></asp:Button></span>
                        </div>
                        <div class="meeting-description-list-box3">
                            <asp:Label ID="lblDayLight" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="meeting-description-list-box4">
                            <asp:Label ID="lblSurf" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="meeting-description-list-box5">
                            <asp:Label ID="lblHeight" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="meeting-description-list-box6">
                            <asp:Label ID="lblDescription" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="meeting-description-list-box7">
                            <asp:Image ID="imgMeetingRoom" runat="server" />
                        </div>
                        <div class="meeting-description-list-box8">
                            <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank">
                                <asp:Image ID="imgPlan" runat="server" src="../images/pdf-icon.png" />
                            </asp:HyperLink>
                        </div>
                        <div class="meeting-description-list-box9">
                            <asp:ImageButton ID="btnMoveUp" CommandName="Swap" Style="float: left;" ImageUrl="../images/up.png"
                                runat="server" />
                            <asp:ImageButton ID="btnMoveDown" CommandName="Swap" ImageUrl="../images/down.png"
                                runat="server" />
                            <asp:HiddenField ID="hdnOrderNumber" runat="server" Value='<%# Eval("OrderNumber")%>' />
                        </div>
                    </li>
                </AlternatingItemTemplate>
                <EmptyDataTemplate>
                    <div class="meeting-description-heading">
                        <div class="meeting-description-heading-box1">
                            &nbsp;
                        </div>
                        <div class="meeting-description-heading-box2">
                            Meeting Room
                        </div>
                        <div class="meeting-description-heading-box3">
                            Daylight
                        </div>
                        <div class="meeting-description-heading-box4">
                            Surf(sqm)
                        </div>
                        <div class="meeting-description-heading-box5">
                            Height
                        </div>
                        <div class="meeting-description-heading-box6">
                            Description
                        </div>
                        <div class="meeting-description-heading-box7">
                            Photo
                        </div>
                        <div class="meeting-description-heading-box8">
                            Plan
                        </div>
                        <div class="meeting-description-heading-box9">
                            Order
                        </div>
                    </div>
                    <div class="meeting-description-list">
                        <ul>
                            <li style="text-align: center"><b>No record found</b></li>
                        </ul>
                    </div>
                </EmptyDataTemplate>
            </asp:ListView>
            <table style="float:left" id="trLagend" runat="server">
                <tr>
                    <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                        align="left">
                        <img src="../Images/help.jpg" />&nbsp;&nbsp;Please be sure you have filled description
                        in all the languages. To check click on modify.
                    </td>
                </tr>
            </table>
            <div class="booking-details" id="divMeetingRoomForm" runat="server">
                    <div class="error" style="display: none;">
                    </div>
                    <div id="TabbedPanels1" class="TabbedPanels">
                        <asp:Literal ID="litLangugeTab" runat="server"></asp:Literal>
                    </div>
                    <script type="text/javascript">
                        //var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
                        LanguageTabJavascript();
                    </script>
                    <ul>
                        <li class="value8">
                            <div class="col17">
                                <strong>Meeting Room <span style="color: Red">*</span></strong></div>
                            <div class="col18">
                                <asp:TextBox ID="txtName" runat="server" class="inputbox9" Width="200px" MaxLength="100"></asp:TextBox></div>
                        </li>
                        <li class="value9">
                            <div class="col17">
                                <strong>Daylight</strong></div>
                            <div class="col18">
                                    <asp:DropDownList ID="ddlDayLight" runat="server" class="NoClassApply">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">Partial</asp:ListItem>
                                    </asp:DropDownList>
                            </div>
                        </li>
                        <li class="value8">
                            <div class="col17">
                                <strong>Surf(sqm) <span style="color: Red">*</span></strong></div>
                            <div class="col18">
                                <asp:TextBox ID="txtSurf" runat="server" class="inputbox10" MaxLength="5"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtSurf"
                                    FilterType="Numbers" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </li>
                        <li class="value9">
                            <div class="col17">
                                <strong>Height <span style="color: Red">*</span></strong></div>
                            <div class="col18">
                                <asp:TextBox ID="txtheight" runat="server" class="inputbox10" MaxLength="5"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtheight"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </li>
                        <li class="value8">
                            <div class="col17">
                                <strong>Description <span style="color: Red">*</span></strong></div>
                            <div class="col18">
                                <asp:TextBox ID="txtDesc" runat="server" class="textarea-new" TextMode="MultiLine"
                                    MaxLength="250"></asp:TextBox>
                                <span id="lblCount" style="font-family: Arial; font-size: Smaller;">Description should
                                    be less than 250 characters.</span>
                            </div>
                        </li>
                        <li class="value8" id="liImageUpload">
                            <div class="col17">
                                <strong>Image <span style="color: Red">*</span></strong>
                            </div>
                            <div class="col18">
                                <asp:FileUpload ID="ulImage" runat="server" /><br />
                                <span style="font-family: Arial; font-size: Smaller;">Image format: JPG,JPEG,PNG,GIF
                                    & max size <%=System.Configuration.ConfigurationManager.AppSettings["MessageKB2"]%>. </span>
                            </div>
                        </li>
                        <li class="value8" id="liPlanUpload">
                            <div class="col17">
                                <strong>Plan </strong>
                            </div>
                            <div class="col18">
                                <asp:FileUpload ID="ulPlan" runat="server" /><br />
                                <span style="font-family: Arial; font-size: Smaller;">Plan file format: PDF & max size
                                    1MB. </span>
                            </div>
                        </li>
                        <li class="value10">
                            <div class="col21">
                                <div class="button_section">
                                    <asp:LinkButton ID="btnSave" CssClass="select" OnClick="btnSave_Click" runat="server"
                                        OnClientClick="return SaveButtonCall();" Text="Save" />
                                    <%-- <asp:LinkButton ID="btnReset" OnClick="btnReset_Click" runat="server" Text="Reset" />
                                    --%>
                                    <asp:LinkButton ID="btnUpdate" CssClass="select" OnClick="btnUpdate_Click" runat="server"
                                        OnClientClick="return UpdateButtonClick();" Text="Save" />
                                    <span>or</span>
                                    <asp:LinkButton ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" />
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="info-tab">
                        <table width="100%" border="0" cellpadding="2">
                            <tr>
                                <td width="3%" valign="top">
                                    <div class="info-tab-white">
                                    </div>
                                </td>
                                <td width="30%" valign="top">
                                    Active Tab
                                </td>
                                <td width="3%" valign="top">
                                    <div class="info-tab-blue">
                                    </div>
                                </td>
                                <td width="30%" valign="top">
                                    Tab with filled details
                                </td>
                                <td width="3%" valign="top">
                                    <div class="info-tab-red">
                                    </div>
                                </td>
                                <td width="30%" valign="top">
                                    Tab with unfilled details/New tab
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                                    align="left">
                                    <img src="../Images/help.jpg" />&nbsp;&nbsp;Save & Cancel button is not common for
                                    all the tabs. Please save information for a tab before switching to another tab.
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnUpdate" />
            <asp:PostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Saving...</span></div>
    <div class="button_sectionNext" id="divNext" runat="server" visible="false">
        <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;"
            OnClick="btnNext_Click" />
    </div>
    <script language="javascript" type="text/javascript">
        function SaveButtonCall() {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var name = jQuery("#<%= txtName.ClientID %>").val();
            if (name.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Meeting Room name is required.";
                isvalid = false;
            }
            var surf = jQuery("#<%= txtSurf.ClientID %>").val();
            if (surf.length <= 0 || parseInt(surf, 10) == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Surf(sqm) is required.";
                isvalid = false;
            }
            var height = jQuery("#<%= txtheight.ClientID %>").val();
            if (height.length <= 0 || parseFloat(height, 10) == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Height is required.";
                isvalid = false;
            }
            else if (isNaN(height)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Enter valid Height.";
                isvalid = false;
            }
            else {
                if (parseFloat(height, 10) > 20) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Height must be less than or equal to 20.";
                    isvalid = false;
                }
            }
            var desc = jQuery("#<%= txtDesc.ClientID %>").val();
            if (desc.length <= 0 || parseInt(desc) == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Description is required.";
                isvalid = false;
            }
            else if (desc.length > 250) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Description must have 250 character.";
                isvalid = false;
            }
            var file = jQuery('#<%= ulImage.ClientID %>').val();

            //Regular Expression for fileupload control.
            //var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.gif|.GIF|.Gif|.png|.Png|.PNG|.jpg|.Jpg|.JPG|.jpeg|.Jpeg|.JPEG)$/;
            if (file.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Image is required. Kindly add the details in English Tab first.";
                isvalid = false;
            }
            else {
                if (!validate_file_format(file, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select file in (Gif, JPG, JPEG and Png) formats only.";
                    isvalid = false;
                }
            }




            var filePlan = jQuery('#<%= ulPlan.ClientID %>').val();
            //Regular Expression for fileupload control.
            //var regplan = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.PDF|.Pdf)$/;
            if (filePlan.length > 0) {
                if (!validate_file_format(filePlan, "pdf,PDF,Pdf")) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select plan file in (PDF) formats only.";
                    isvalid = false;
                }
            }
            if (!isvalid) {
                jQuery("#<%= divMeetingRoomForm.ClientID %> .error").show();
                jQuery("#<%= divMeetingRoomForm.ClientID %> .error").html(errormessage);
                var offseterror = jQuery("#<%= divMeetingRoomForm.ClientID %> .error").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                jQuery("#<%=divmessage.ClientID %>").hide();
                return false;
            }
            jQuery("#<%= divMeetingRoomForm.ClientID %> .error").html("");
            jQuery("#<%= divMeetingRoomForm.ClientID %> .error").hide();
            jQuery("#Loding_overlaySec span").html("Loading...");
            jQuery("#Loding_overlaySec").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        }

        function UpdateButtonClick() {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var name = jQuery("#<%= txtName.ClientID %>").val();
            if (name.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Meeting Room name is required.";
                isvalid = false;
            }
            var surf = jQuery("#<%= txtSurf.ClientID %>").val();
            if (surf.length <= 0 || parseInt(surf, 10) == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Surf(sqm) is required.";
                isvalid = false;
            }
            var height = jQuery("#<%= txtheight.ClientID %>").val();
            if (height.length <= 0 || parseFloat(height, 10) == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Height is required.";
                isvalid = false;
            }
            else if (isNaN(height)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Enter valid Height.";
                isvalid = false;
            }
            else {
                if (parseFloat(height, 10) > 20) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Height must be less than or equal to 20.";
                    isvalid = false;
                }
            }
            var desc = jQuery("#<%= txtDesc.ClientID %>").val();
            if (desc.length <= 0 || parseInt(desc) == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Description is required.";
                isvalid = false;
            }
            else if (desc.length > 250) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Description must have 250 character.";
                isvalid = false;
            }
            var file = jQuery('#<%= ulImage.ClientID %>').val();
            //Regular Expression for fileupload control.
            //var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.gif|.GIF|.Gif|.png|.Png|.PNG|.jpg|.Jpg|.JPG|.jpeg|.Jpeg|.JPEG)$/;
            if (file.length <= 0) {
                //                        if (errormessage.length > 0) {
                //                            errormessage += "<br/>";
                //                        }
                //                        errormessage += "Image is required.";
                //                        isvalid = false;
            }
            else {
                if (!validate_file_format(file, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select file in (Gif, JPG, JPEG and Png) formats only.";
                    isvalid = false;
                }
            }

            var filePlan = jQuery('#<%= ulPlan.ClientID %>').val();
            //Regular Expression for fileupload control.
            //var regplan = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.PDF|.Pdf)$/;
            if (filePlan.length > 0) {
                if (!validate_file_format(filePlan, "pdf,PDF,Pdf")) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Plan file in (PDF) formats only.";
                    isvalid = false;
                }
            }
            if (!isvalid) {
                jQuery("#<%= divMeetingRoomForm.ClientID %> .error").show();
                jQuery("#<%= divMeetingRoomForm.ClientID %> .error").html(errormessage);
                var offseterror = jQuery("#<%= divMeetingRoomForm.ClientID %> .error").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                jQuery("#<%=divmessage.ClientID %>").hide();
                return false;
            }
            jQuery("#<%= divMeetingRoomForm.ClientID %> .error").html("");
            jQuery("#<%= divMeetingRoomForm.ClientID %> .error").hide();
            jQuery("#Loding_overlaySec span").html("Saving...");
            jQuery("#Loding_overlaySec").show();
            jQuery("html").scrollTop(0);
            jQuery("body").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        }
        jQuery(document).ready(function () {
            jQuery("#<%= divMeetingRoomForm.ClientID %> .error").hide();
            //            jQuery("#<%= btnSave.ClientID %>").bind("click", function () {
            //                
            //            })
            //            jQuery("#<%= btnUpdate.ClientID %>").bind("click", function () {
            //                
            //            })
        });
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
        function validate_file_format(file_name, allowed_ext) {
            //                obj1 = document.req_form;
            //                var temp_field = 'obj1.' + field_name + '.value';
            field_value = file_name;
            if (field_value != "") {
                var file_ext = (field_value.substring((field_value.lastIndexOf('.') + 1)).toLowerCase());
                ext = allowed_ext.split(',');
                var allow = 0;
                for (var i = 0; i < ext.length; i++) {
                    if (ext[i] == file_ext) {
                        allow = 1;
                    }
                }
                if (!allow) {
                    //                        alert('Invalid File format. Please upload file in ' + allowed_ext + ' format');
                    return false;
                }
                else {
                    return true;
                }
            }
            return false;
        } 
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
            }
        });
    </script>
    
</asp:Content>
