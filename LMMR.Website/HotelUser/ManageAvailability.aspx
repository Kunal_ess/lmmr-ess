﻿<%@ Page Title="Hotel User - Availability" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="ManageAvailability.aspx.cs" Inherits="HotelUser_ManageAvailability" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/AvailabilityMeeting.js" type="text/javascript"></script>
    
    <script src="../js/FormManage.js" type="text/javascript"></script>
    <style type="text/css">
        .hiddenButton
        {
            display: none;
        }
    </style>
    <script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#divAvailability").MyPlugin({
            HotelId: <%= Session["CurrentHotelID"] %>,
            MonthYearDayDate: <%=MonthYearDayDate %>,
            StartWeek: 6,
            DivNames: <%=DivNames %>,
            IsBedroomAvailable: <%= IsBedroomAvailable %>,
            BedRoomDivs: <%= DivNamesBR %>,
            StartAvailableDates: <%=StartAvailableDates %>,
            EndAvailableDate: <%=EndAvailableDate %>,
            Webservicepath: <%= ServiceUrl %>,
            CurrentPanel: <%=CurrentPanel %>
        });
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        Availability of meeting rooms for: <b>"<asp:Label ID="lblHotelName" runat="server"></asp:Label>"</b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <asp:HiddenField ID="hdnCurrentPanel" runat="server" />
    <div>
        <div id="msgEmpty" runat="server" class="warning" style="margin-top: 70px;">
            Dear Supplier,<br />
            <br />
            No meeting rooms have been set online. Please go to Property level – Pricing meeting<br />
            rooms and adjust the online setting of your Meeting rooms.
            <br />
            <br />
            Support team Lastminutemeetingroom
        </div>
    </div>
    <asp:Panel ID="pnlMeetingRoom" runat="server">
        <asp:Button ID="btnnothing" runat="server" CssClass="hiddenButton" Text="nothing"
            OnClientClick="return false;" /><div id="Loding_overlay">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...</div>
        <div class="top_header">
            <h2>
                Meeting rooms</h2>
            <p>
                <a href="javascript:;" onclick="window.scroll(0,0);document.getElementsByTagName('html')[0].style.overflow='hidden';document.getElementById('adjust-meeting').style.display='block';document.getElementById('adjust-meeting-overlay').style.display='block';" />
                Adjust meeting rooms to sell</a><div id="result">
                </div>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
            </p>
        </div>
        <div id="divAvailability">
            <div class="pager_header">
                <div style="float: left; width: 10%; height: 20px;">
                    <a href="javascript:void(0);" id="prevlink">Prev 30 days</a></div>
                <div class="page_text" style="width: 80%; text-align: center;">
                    <a href="javascript:void(0);" id="left">
                        <img src="../images/prev_bullet.png" alt="prev" border="0" /></a> <span class="spanwidth">
                            May 2011</span> <a href="javascript:void(0);" id="right">
                                <img src="../images/next_bullet.png" border="0" /></a></div>
                <div style="float: left; width: 10%; height: 20px;">
                    <a href="javascript:void(0);" id="nextlink">Next 30 days</a></div>
            </div>
        </div>
        <!-- start meeting room table-->
        <table width="750" border="0" cellspacing="0" cellpadding="0" bgcolor="#a3d4f7" class="room"
            id="tblMR" style="float: left;">
            <tr>
                <td valign="top">
                    <table width="100%" border="0" cellspacing="1" cellpadding="0">
                        <tr bgcolor="#FFFFFF" class="cal_header" id="Calander">
                        </tr>
                        <tr class="raw1">
                            <td colspan="32" height="22">
                                Discount applied on roomhire in the package:
                            </td>
                        </tr>
                        <tr class="raw2" id="discount">
                        </tr>
                        <tr class="raw1">
                            <td colspan="32" height="22">
                                Discount applied on standalone meetingroom:
                            </td>
                        </tr>
                        <tr class="raw2" id="variation">
                        </tr>
                        <tr class="raw3">
                            <td colspan="32" height="22">
                                Full property
                            </td>
                        </tr>
                        <tr class="raw4" id="fullproperty">
                        </tr>
                        <asp:Literal ID="ltrMeetingRoom" runat="server"></asp:Literal>
                        <tr class="raw3">
                            <td colspan="32" height="22">
                                Lead Time &bull; <a href="javascript:void(0);" id="modifyLeadTime">Modify</a><a href="javascript:void(0);"
                                    id="leadTimeCancel">Cancel</a>&nbsp;<a href="javascript:void(0);" id="leadTimeSave">Save</a>
                            </td>
                        </tr>
                        <tr class="raw6" id="leadtime">
                        </tr>
                        <tr class="raw6" id="leadtimeEdit">
                        </tr>
                    </table>
                    <asp:HiddenField ID="hdnLeadTime" runat="server" />
                    <asp:HiddenField ID="hdnStartingDate" runat="server" />
                    <asp:Button ID="btnSaveLeed" runat="server" Text="Save" OnClick="btnSaveLeed_Click"
                        CssClass="hiddenButton" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- end meeting room table-->
    <asp:Panel ID="pnlBedroom" runat="server">
        <div class="top_header">
            <h2>
                Bedrooms</h2>
            <p>
                <a href="javascript:void(0);" onclick="window.scroll(0,0);document.getElementsByTagName('html')[0].style.overflow='hidden';document.getElementById('adjust-bedroom').style.display='block';document.getElementById('adjust-bedroom-overlay').style.display='block';">
                    Adjust bedrooms to sell</a></p>
        </div>
        <div id="divAvailabilityBedroom">
            <div class="pager_header">
                <div style="float: left; width: 10%; height: 20px;">
                    <a href="javascript:void(0);" id="prevlink">Prev 30 days</a></div>
                <div class="page_text" style="width: 80%; text-align: center;">
                    <a href="javascript:void(0);" id="left">
                        <img src="../images/prev_bullet.png" alt="prev" /></a> <span class="spanwidth">May 2011</span>
                    <a href="javascript:void(0);" id="right">
                        <img src="../images/next_bullet.png" alt="next" /></a></div>
                <div style="float: left; width: 10%; height: 20px;">
                    <a href="javascript:void(0);" id="nextlink">Next 30 days</a></div>
            </div>
        </div>
        <!-- start bedroom table-->
        <table width="750" border="0" cellspacing="0" cellpadding="0" bgcolor="#a3d4f7" class="room"
            id="tblBR" runat="server" style="float: left;">
            <tr>
                <td valign="top">
                    <asp:HiddenField ID="hdn_AvailabileRooms" runat="server" />
                    <asp:HiddenField ID="hdn_Date" runat="server" />
                    <asp:HiddenField ID="hdnBedroomID" runat="server" />
                    <asp:Button ID="btnSubmit1" runat="server" Text="Save" CssClass="hiddenButton" OnClick="btnSubmit1_Click" />
                    <table width="100%" border="0" cellspacing="1" cellpadding="0">
                        <tr bgcolor="#FFFFFF" class="cal_header" id="calanderBR">
                            <td>
                                TU
                                <br />
                                <span>1</span>
                            </td>
                            <td>
                                WE<br />
                                <span>2</span>
                            </td>
                            <td>
                                TH
                                <br />
                                <span>3</span>
                            </td>
                            <td>
                                FR
                                <br />
                                <span>4</span>
                            </td>
                            <td>
                                SA<br />
                                <span>5</span>
                            </td>
                            <td>
                                SU
                                <br />
                                <span>6</span>
                            </td>
                            <td class="closed">
                                MO<br />
                                <span>7</span>
                            </td>
                            <td>
                                TU<br />
                                <span>8</span>
                            </td>
                            <td>
                                WE<br />
                                <span>9</span>
                            </td>
                            <td>
                                TH<br />
                                <span>10</span>
                            </td>
                            <td>
                                FR<br />
                                <span>11</span>
                            </td>
                            <td>
                                SA<br />
                                <span>12</span>
                            </td>
                            <td>
                                SU<br />
                                <span>13</span>
                            </td>
                            <td>
                                MO<br />
                                <span>14</span>
                            </td>
                            <td>
                                TU<br />
                                <span>15</span>
                            </td>
                            <td>
                                WE<br />
                                <span>16</span>
                            </td>
                            <td>
                                TH<br />
                                <span>17</span>
                            </td>
                            <td>
                                FR<br />
                                <span>18</span>
                            </td>
                            <td>
                                SA<br />
                                <span>19</span>
                            </td>
                            <td>
                                SU<br />
                                <span>20</span>
                            </td>
                            <td>
                                MO<br />
                                <span>21</span>
                            </td>
                            <td>
                                TU<br />
                                <span>22</span>
                            </td>
                            <td>
                                WE<br />
                                <span>23</span>
                            </td>
                            <td>
                                TH<br />
                                <span>24</span>
                            </td>
                            <td>
                                FR<br />
                                <span>25</span>
                            </td>
                            <td>
                                SA<br />
                                <span>26</span>
                            </td>
                            <td>
                                SU<br />
                                <span>27</span>
                            </td>
                            <td>
                                MO<br />
                                <span>28</span>
                            </td>
                            <td>
                                TU<br />
                                <span>29</span>
                            </td>
                            <td>
                                WE<br />
                                <span>30</span>
                            </td>
                            <td>
                                TH<br />
                                <span>31</span>
                            </td>
                        </tr>
                        <asp:Literal ID="ltrBedroomDetails" runat="server"></asp:Literal>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- end bedroom table-->
    <!--adjust-meeting-overlay LIGHT BOX-->
    <div id="adjust-meeting-overlay">
    </div>
    <div id="adjust-meeting">
        <div class="popup-top1">
        </div>
        <div class="popup-mid1">
            <div class="popup-mid-inner1">
                <div class="adjust-meeting-form">
                    <div class="adjust-meeting-date">
                        <div class="error">
                        </div>
                        <div class="adjust-meeting-date-box1">
                            From
                        </div>
                        <div class="adjust-meeting-date-box">
                            <asp:TextBox ID="txtFrom" runat="server" value="dd/mm/yy" class="inputbox1"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"
                                PopupButtonID="calFrom" Format="dd/MM/yy">
                            </asp:CalendarExtender>
                        </div>
                        <div class="adjust-meeting-date-box2">
                            <input type="image" src="../Images/cal-2.png" id="calFrom" />
                        </div>
                        <div class="adjust-meeting-date-box1">
                            To
                        </div>
                        <div class="adjust-meeting-date-box">
                            <asp:TextBox ID="txtTo" runat="server" value="dd/mm/yy" class="inputbox1"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
                                PopupButtonID="calTo" Format="dd/MM/yy">
                            </asp:CalendarExtender>
                        </div>
                        <div class="adjust-meeting-date-box2">
                            <input type="image" src="../Images/cal-2.png" id="calTo" />
                        </div>
                    </div>
                </div>
                <div class="adjust-meeting-form-body">
                    <div class="adjust-meeting-form-body-box1">
                        Meeting rooms
                    </div>
                    <div class="adjust-meeting-form-body-box1">
                        When
                    </div>
                    <div class="adjust-meeting-form-body-box1">
                        Close &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Open
                    </div>
                    <asp:HiddenField ID="hdnMeetingRoomIds" runat="server" />
                    <asp:HiddenField ID="hdnMeetingDays" runat="server" />
                    <asp:HiddenField ID="hdnClosingStatus" runat="server" />
                    <asp:HiddenField ID="hdnOpenStatus" runat="server" />
                    <div class="adjust-meeting-form-body-box" style="max-height:200px;overflow:auto;" >
                        <div class="adjust-meeting-form-body-box1left">
                            All</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <input type="checkbox" id="chkAllMeetingRoom" name="chbox" alt="0" class="NoClassApply">
                        </div>
                        <asp:Literal ID="ltrMeetingRoomCheckbox" runat="server"></asp:Literal>
                    </div>
                    <div class="adjust-meeting-form-body-box2">
                        <div class="adjust-meeting-form-body-box2left">
                            All day</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkAllDays" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Sunday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkSun" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Monday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkMon" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Tuesday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkTue" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Wednesday
                        </div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkWed" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Thursday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkThu" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Friday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkFri" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Saturday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkSat" name="chbox" class="NoClassApply">
                        </div>
                    </div>
                    <div class="adjust-meeting-form-body-box3">
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkAllClose" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkAllOpen" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseM" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenM" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseTu" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenTu" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseW" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenW" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseTh" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenTh" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseF" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenF" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseSa" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenSa" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseSu" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenSu" name="chbox" class="NoClassApply">
                        </div>
                    </div>
                </div>
                <div class="adjust-meeting-btn">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="ancSubmit" runat="server" CssClass="select" OnClick="ancSubmit_Click">Save</asp:LinkButton>
                        <%-- <a class="save-btn" id="ancSubmit" href="javascript:void(0);">Save</a>--%>
                        &nbsp;&nbsp; or &nbsp;&nbsp;<a title="Close" class="cancelbtn" href="javascript:;"
                            onclick="document.getElementsByTagName('html')[0].style.overflow='auto';document.getElementById('adjust-meeting').style.display='none';document.getElementById('adjust-meeting-overlay').style.display='none';">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom1">
        </div>
    </div>
    <!--adjust-meeting-overlay LIGHT BOX-->
    <div id="adjust-bedroom-overlay">
    </div>
    <div id="adjust-bedroom">
        <div class="popup-top1">
        </div>
        <div class="popup-mid1">
            <div class="popup-mid-inner1">
                <div class="adjust-meeting-form">
                    <div class="error">
                    </div>
                    <div class="adjust-meeting-date">
                        <div class="adjust-meeting-date-box1">
                            From
                        </div>
                        <div class="adjust-meeting-date-box">
                            <asp:TextBox ID="txtFromBr" runat="server" value="dd/mm/yy" class="inputbox1"></asp:TextBox>
                            <asp:CalendarExtender ID="calExtFromBr" runat="server" TargetControlID="txtFromBr"
                                PopupButtonID="calFromBr" Format="dd/MM/yy">
                            </asp:CalendarExtender>
                        </div>
                        <div class="adjust-meeting-date-box2">
                            <input type="image" src="../Images/cal-2.png" id="calFromBr" />
                        </div>
                        <div class="adjust-meeting-date-box1">
                            To
                        </div>
                        <div class="adjust-meeting-date-box">
                            <asp:TextBox ID="txtToBr" runat="server" value="dd/mm/yy" class="inputbox1"></asp:TextBox>
                            <asp:CalendarExtender ID="calExToBr" runat="server" TargetControlID="txtToBr" PopupButtonID="calToBr"
                                Format="dd/MM/yy">
                            </asp:CalendarExtender>
                        </div>
                        <div class="adjust-meeting-date-box2">
                            <input type="image" src="../Images/cal-2.png" id="calToBr" />
                        </div>
                    </div>
                </div>
                <div class="adjust-meeting-form-body">
                    <div class="adjust-meeting-form-body-box1">
                        Bedrooms
                    </div>
                    <div class="adjust-meeting-form-body-box1">
                        When
                    </div>
                    <div class="adjust-meeting-form-body-box1">
                        Close &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Open
                    </div>
                    <asp:HiddenField ID="hdnBedroomIds" runat="server" />
                    <asp:HiddenField ID="hdnBedroomDays" runat="server" />
                    <asp:HiddenField ID="hdnClosingStatusBr" runat="server" />
                    <asp:HiddenField ID="hdnOpeningstatusBr" runat="server" />
                    <div class="adjust-meeting-form-body-box" style="max-height:200px;overflow:auto;" >
                        <div class="adjust-meeting-form-body-box1left">
                            All</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <input type="checkbox" id="chkAllMeetingRoom" name="chbox" alt="0" class="NoClassApply">
                        </div>
                        <asp:Literal ID="ltrBedRoomCheckbox" runat="server"></asp:Literal>
                    </div>
                    <div class="adjust-meeting-form-body-box2">
                        <div class="adjust-meeting-form-body-box2left">
                            All day</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkAllDays" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Sunday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkSun" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Monday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkMon" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Tuesday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkTue" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Wednesday
                        </div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkWed" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Thursday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkThu" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Friday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkFri" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Saturday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <input type="checkbox" id="chkSat" name="chbox" class="NoClassApply">
                        </div>
                    </div>
                    <div class="adjust-meeting-form-body-box3">
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkAllClose" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkAllOpen" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseM" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenM" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseTu" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenTu" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseW" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenW" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseTh" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenTh" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseF" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenF" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseSa" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenSa" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3left">
                            <input type="checkbox" id="chkCloseSu" name="chbox" class="NoClassApply">
                        </div>
                        <div class="adjust-meeting-form-body-box3right">
                            <input type="checkbox" id="chkOpenSu" name="chbox" class="NoClassApply">
                        </div>
                    </div>
                </div>
                <div class="adjust-meeting-btn">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="ancSubmitBr" runat="server" CssClass="select" OnClick="ancSubmitBr_Click">Save</asp:LinkButton>
                        &nbsp;&nbsp; or &nbsp;&nbsp;<a title="Close" class="cancelbtn" href="javascript:;"
                            onclick="document.getElementsByTagName('html')[0].style.overflow='auto';document.getElementById('adjust-bedroom').style.display='none';document.getElementById('adjust-bedroom-overlay').style.display='none';">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom1">
        </div>
    </div>
</asp:Content>
