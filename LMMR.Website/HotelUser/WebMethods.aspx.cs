﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Text;
public partial class HotelUser_WebMethods : System.Web.UI.Page
{
    public static string linkvalue = "";

    protected void Page_Load(object sender, EventArgs e)
    {


    }

    [WebMethod]
    public static string LoadLeftMenu(string linkID)
    {
        StringBuilder ObjSB = new StringBuilder();
        ObjSB.Append(@"<div class=""leftside_inner_container"" runat=""server"" id=""divLeftMenu"">");

        if (linkID == "lnkPropertyLevel")
        {
            // start blue white box-->
            ObjSB.Append(@"<div class=""blue_white_box"">");
            ObjSB.Append(@"<h2>General info</h2>");
            ObjSB.Append(@"<div class=""content"">");
            ObjSB.Append(@"<ul class=""innerlist"">");

            ObjSB.Append(@"<li><a href=""#"" runat=""server"">Hotel/Conference info</a></li>");
            ObjSB.Append(@"<li><a href=""#"" runat=""server"">Contact details</a></li>");
            ObjSB.Append(@"<li><a href=""#"" runat=""server"">Facilities</a></li>");
            ObjSB.Append(@"<li><a href=""#"" runat=""server"">Cancelation policy</a></li>");

            ObjSB.Append(@"<li class=""h3"">Meeting rooms</li>");
            ObjSB.Append(@"<li><a href=""#"" runat=""server"">Description</a></li>");
            ObjSB.Append(@"<li><a href=""#"" runat=""server"">Configuration</a></li>");
            ObjSB.Append(@"<li><a href=""#"" runat=""server"">Preview/Print fact sheet</a></li>");
            ObjSB.Append(@"<li><a href=""#"">Preview on web</a></li>");

            ObjSB.Append(@"<li class=""h3"">Bedrooms</li>");
            ObjSB.Append(@"<li><a href=""#"">Description</a></li>");

            ObjSB.Append(@"<li class=""h3"">Pricing</li>");
            ObjSB.Append(@"<li><a href=""#"">Meeting rooms</a></li>");
            ObjSB.Append(@"<li><a href=""#"">Bedrooms</a></li>");

            ObjSB.Append(@"<li class=""h3"">Pictures/video</li>");
            ObjSB.Append(@"<li><a href=""#"">Picture Management</a></li>");
            ObjSB.Append(@"</ul>");
            ObjSB.Append(@"</div>");
            ObjSB.Append(@"</div>");
            // end blue white box-->
        }
        else if (linkID == "lnkAvailability")
        {

            // start blue white box-->
            ObjSB.Append(@"<div class=""blue_white_box"">");

            ObjSB.Append(@"<h2>General info</h2>");
            ObjSB.Append(@"<div class=""content"">");
            ObjSB.Append(@"<ul class=""innerlist"">");
            ObjSB.Append(@"<li><a href=""#"" >Availability</a></li>");
            ObjSB.Append(@"<li class=""selected""><a href=""#"">Contact details</a></li>");
            ObjSB.Append(@"<li><a href=""#"">Facilities</a></li>");
            ObjSB.Append(@"<li><a href=""#"">Cancelation policy</a></li>");

            ObjSB.Append(@"<li class=""h3"">Meeting rooms</li>");
            ObjSB.Append(@"<li><a href=""#"">Description</a></li>");
            ObjSB.Append(@"<li><a href=""#"">Configuration</a></li>");
            ObjSB.Append(@"<li><a href=""#"">Preview/Print fact sheet</a></li>");
            ObjSB.Append(@"<li><a href=""#"">Preview on web</a></li>");

            ObjSB.Append(@"<li class=""h3"">Bedrooms</li>");
            ObjSB.Append(@"<li><a href=""#"">Description</a></li>");

            ObjSB.Append(@"<li class=""h3"">Pricing</li>");
            ObjSB.Append(@"<li><a href=""#"">Meeting rooms</a></li>");
            ObjSB.Append(@"<li><a href=""#"">Bedrooms</a></li>");

            ObjSB.Append(@"<li class=""h3"">Pictures/video</li>");
            ObjSB.Append(@"<li><a href=""#"">Picture Management</a></li>");
            ObjSB.Append(@"</ul>");
            ObjSB.Append(@"</div>");
            ObjSB.Append(@"</div>");
            // end blue white box-->


        }

        else if (linkID == "lnkBookings")
        {



        }
        else if (linkID == "lnkFinance")
        {




        }
        else if (linkID == "lnkStatistics")
        {



        }
        else if (linkID == "lnkPromotions")
        {

        }

        else if (linkID == "lnkRequests")
        {



        }

        ObjSB.Append(@"</div>");

        return ObjSB.ToString(); 

    }

}