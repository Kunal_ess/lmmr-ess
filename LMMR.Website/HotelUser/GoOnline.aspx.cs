﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.IO;
using LMMR.Data;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_GoOnline : System.Web.UI.Page
{
    #region Variables and Properties
    int intHotelID = 0;
    public SendMails objSendmail = new SendMails();
    HotelManager ObjHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_GoOnline));
    EmailConfigManager em = new EmailConfigManager();
    #endregion

    #region Page Load
    /// <summary>
    /// Set the initial values of controls at page load.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session for Hotelname        
            if (Session["CurrentHotelID"] == null)
            {
                Response.Redirect("~/login.aspx",false);
                return;
            }
            Session["LinkID"] = "lnkPropertyLevel";
            if (Session["CurrentHotelID"] != null)
            {
                intHotelID = Convert.ToInt32(Session["CurrentHotelID"].ToString());
                lblHotel.Text = ObjHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
                lblHotel1.Text = ObjHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
                Getgoonline();
            }
            else
            {
                Response.Redirect("~/login.aspx",false);
                return;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// For check go online for this hotel
    /// </summary>
    public void Getgoonline()
    {
        try
        {
            if (ObjHotelinfo.GetHotelRequestGoOnline(intHotelID).Count > 0)
            {
                divcompleted.Visible = true;
                divPending.Visible = false;
                if (Convert.ToString(Session["Operator"]) == "1")
                {
                    divGoonline.Visible = true;
                    divcompleted.Visible = false;
                }
            }
            else
            {
                divcompleted.Visible = false;
                divPending.Visible = true;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Event
    /// <summary>
    /// Back button move to the Last page of Picture management Page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBack_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("~/HotelUser/PictureVideoManagement.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Button Confirm for sending 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            bool boolResult = false;
            HotelManager objHotelManager = new HotelManager();
            //update Hotel request GoOnline status//
            if (Convert.ToString(Session["Operator"]) == "1")
            {
                Hotel objHotel = objHotelManager.GetHotelDetailsById(intHotelID);
                objHotel.GoOnline = true;
                objHotel.GoOnlineDate = System.DateTime.Now;
                boolResult = objHotelManager.UpdateHotel(objHotel);
                //For allotment CR
                //if (boolResult)
                //{
                //    objHotelManager.FirstTimeOnline(objHotel.Id);
                    
                //}
                EmailConfig eConfig = em.GetByName("Goonline confirm");
                if (eConfig.IsActive)
                {
                    objSendmail.ToEmail = objHotel.Email;
                    objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                    string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                    EmailConfigMapping emap = new EmailConfigMapping();
                    emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                    bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                    EmailValueCollection objEmailValues = new EmailValueCollection();
                    Users objUser = (Users)Session["CurrentUser"];
                    objSendmail.Subject = "Confirmation for  (Hotel: " + lblHotel.Text + "going online)";
                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForGoOnlineHotelUser(lblHotel.Text, objUser.FirstName + " " + objUser.LastName))
                    {
                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                    }
                    objSendmail.Body = bodymsg;
                    if (objSendmail.SendMail() == "Send Mail")
                    {
                        boolResult = true;
                    }

                    if (boolResult)
                    {
                        divmessage.InnerHtml = "Go online successfully";
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "succesfuly");
                        Response.Redirect("HotelDashboard.aspx");
                    }
                    else
                    {
                        divmessage.InnerHtml = "Please try Again";
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "error");
                    }
                    Response.Redirect("HotelDashboard.aspx"); 
                }
                else
                {
                    divmessage.InnerHtml = "The email functionality is not activated yet.";
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "error");
                }
                               
            }
            else
            {
                Hotel objHotel = objHotelManager.GetHotelDetailsById(intHotelID);
                objHotel.RequestGoOnline = true;
                boolResult = objHotelManager.UpdateHotel(objHotel);
                EmailConfig eConfig = em.GetByName("Request for goonline");
                if (eConfig.IsActive)
                {
                    //Send To Admin//
                    objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                    //Send mail to the operator.
                    Users objoperator = new HotelManager().GetFirstOperatorInDataBase();
                    if (objoperator != null)
                    {
                        objSendmail.ToEmail = objoperator.EmailId;
                    }
                    else
                    {
                        throw new Exception("No operator email details found.", null);
                        //objSendmail.ToEmail = "gaurav-shrivastava@essindia.co.in";
                    }
                    string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                    EmailConfigMapping emap = new EmailConfigMapping();
                    emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                    bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);

                    EmailValueCollection objEmailValues = new EmailValueCollection();
                    Users objUser = (Users)Session["CurrentUser"];
                    objSendmail.Subject = "Confirmation for  (Hotel: " + lblHotel.Text + "going online)";
                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForGoOnline(lblHotel.Text, objUser.FirstName + " " + objUser.LastName, "Operator"))
                    {
                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                    }
                    objSendmail.Body = bodymsg;
                    if (objSendmail.SendMail() == "Send Mail")
                    {
                        boolResult = true;
                    }

                    //objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                    ////objSendmail.ToEmail = "pooja-verma@essindia.co.in";
                    //string bodymsg2 = File.ReadAllText(Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                    //EmailConfigMapping emap2 = new EmailConfigMapping();
                    //emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                    //bodymsg2 = bodymsg2.Replace("@CONTENTS", emap2.EmailContents);
                    //EmailValueCollection objEmailValues2 = new EmailValueCollection();
                    //Users objUser2 = (Users)Session["CurrentUser"];
                    //objSendmail.Subject = "Confirmation for  (Hotel: " + lblHotel.Text + "going online)";
                    //foreach (KeyValuePair<string, string> strKey in objEmailValues2.EmailForGoOnline(lblHotel.Text, objUser2.FirstName + " " + objUser2.LastName, "SupperAdmin"))
                    //{
                    //    bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                    //}
                    //objSendmail.Body = bodymsg2;
                    //if (objSendmail.SendMail() == "Send Mail")
                    //{
                    //    boolResult = true;
                    //}

                    if (boolResult)
                    {
                        divmessage.InnerHtml = "your request sent successfully";
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "succesfuly");
                        Response.Redirect("HotelDashboard.aspx");
                    }
                    else
                    {
                        divmessage.InnerHtml = "Please try Again";
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "error");
                    }
                }
                else
                {
                    divmessage.InnerHtml = "This mail functionality is disable right now.";
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "error");
                }
            }

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}