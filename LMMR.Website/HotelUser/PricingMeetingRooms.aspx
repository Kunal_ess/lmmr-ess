﻿<%@ Page Title="Hotel User - Pricing" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="PricingMeetingRooms.aspx.cs"
    Inherits="HotelUser_PricingMeetingRooms" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/PricingStyle.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        Pricing Meeting Rooms : <b>"<asp:Label ID="lblHotel" runat="server" Text=""></asp:Label>"</b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <div class="prices-container">
        <asp:HiddenField ID="hndPackageID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnWithOutChangeMeetingroom" runat="server" Value="" />
        <asp:HiddenField ID="hdnWithOutChangeFoodBeverages" runat="server" Value="" />
        <asp:HiddenField ID="hdnWithOutChangeEquipmentItem" runat="server" Value="" />
        <asp:HiddenField ID="hdnWithOutChangeOthersItem" runat="server" Value="" />
        <div style="display: none">
            <asp:Button ID="lnkSelectPackage" OnClick="GetPriceByPackage_Click" runat="server">
            </asp:Button>
            <asp:HiddenField ID="hdnPackage" Value="" runat="server" />
        </div>
        <div id="divmsgpackage" class="error" style="display: none;">
        </div>
        <div id="divmsgpackageByServer" class="error" style="display: none;" runat="server">
        </div>
        <asp:Literal ID="litPackageTab" runat="server"></asp:Literal>
        <div class="grid-main clearfix" style="float: left; width: 100%;" runat="server"
            id="divpackages">
            <div class="save-cancel-prices-container-btn-new" id="divPackageModify" runat="server">
                <div class="save-cancel-btn1">
                    <asp:LinkButton ID="lnkPackagePriceModify" runat="server" class="cancel-btn" OnClick="lnkPackagePriceModify_Click">modify</asp:LinkButton>
                </div>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="divViewPackagePrice"
                runat="server">
                <tr>
                    <td bgcolor="#98bcd6">
                        <table width="100%" border="0" cellspacing="1" cellpadding="5">
                            <tr bgcolor="#c4d6e2">
                                <td>
                                    Item Name
                                </td>
                                <td align="center">
                                    Vat%
                                </td>
                                <td align="center">
                                    Price per person 1/2 day &nbsp;<asp:Label ID="lblCSPackageHalf" runat="server" Text=""></asp:Label>
                                </td>
                                <td align="center">
                                    Price per person full day &nbsp;<asp:Label ID="lblCSPackageFull" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#c4d6e2">
                                <td>
                                    Online package price &nbsp;<asp:Label ID="lblCSPackageOnlien" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblActualHalfDayPrice" runat="server" Text=""></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblActualFullDayPrice" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <asp:ListView ID="lstViewPackagePrice" runat="server" ItemPlaceholderID="itemPlacehoderID"
                                AutomaticGenerateColumns="false" DataKeyNames="ItemId" OnItemDataBound="lstViewPackagePrice_ItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr bgcolor="#d8eefc" class=" row">
                                        <td>
                                            <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                            <asp:HiddenField ID="hdnPackageByHotelID" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblHalfDayPrice" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblFullDayPrice" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr bgcolor="#edf8fe" class=" row">
                                        <td>
                                            <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                            <asp:HiddenField ID="hdnPackageByHotelID" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblHalfDayPrice" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td align="center">
                                            <asp:Label ID="lblFullDayPrice" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                                <EmptyDataTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td bgcolor="#98bcd6">
                                                <table width="100%" border="0" cellspacing="1" cellpadding="5">
                                                    <tr bgcolor="#c4d6e2">
                                                        <td>
                                                            Item Name
                                                        </td>
                                                        <td align="center">
                                                            Vat%
                                                        </td>
                                                        <td align="center">
                                                            HalfDay Price &nbsp;<asp:Label ID="lblCSPackageHalf" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            FullDay Price &nbsp;<asp:Label ID="lblCSPackageFull" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#d8eefc" class=" row">
                                                        <td colspan="4" align="center">
                                                            <b>No Record found </b>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:ListView>
                            <tr bgcolor="#c4d6e2">
                                <td>
                                    Room rental
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblRentalVatView" runat="server" Text=""></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblActualHalfTotal" runat="server" Text=""></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblActualFullTotal" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="divViewPackagePriceEdit"
                runat="server">
                <tr>
                    <td bgcolor="#98bcd6">
                        <table width="100%" border="0" cellspacing="1" cellpadding="5" id="tblactual">
                            <tbody>
                                <tr bgcolor="#c4d6e2">
                                    <td>
                                        Item Name
                                    </td>
                                    <td align="center">
                                        Vat%
                                    </td>
                                    <td align="center">
                                        Price per person 1/2 day &nbsp;<asp:Label ID="lblCSPackageHalfEdit" runat="server"
                                            Text=""></asp:Label>
                                    </td>
                                    <td align="center">
                                        Price per person full day &nbsp;<asp:Label ID="lblCSPackageFullEdit" runat="server"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr bgcolor="#c4d6e2">
                                    <td>
                                        Online package price &nbsp;<asp:Label ID="lblCSPackageOnlineEdit" runat="server"
                                            Text=""></asp:Label>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtActualHalfPrice" runat="server" Text="" class="inputboxmid" MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtActualHalfPrice"
                                            ValidChars=".1234567890" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtActualFullPrice" runat="server" Text="" class="inputboxmid" MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtActualFullPrice"
                                            ValidChars=".1234567890" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <asp:ListView ID="lstViewPackagePriceEdit" runat="server" ItemPlaceholderID="itemPlacehoderID"
                                    AutomaticGenerateColumns="false" DataKeyNames="ItemId" OnItemDataBound="lstViewPackagePriceEdit_ItemDataBound">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#d8eefc" class=" row">
                                            <td>
                                                <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                                <asp:HiddenField ID="hdnPackageByHotelID" runat="server" Value="0" />
                                                <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtHalfDayPrice" runat="server" Text="" class="inputboxmid" MaxLength="10"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                                    ValidChars=".1234567890" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtFullDayPrice" runat="server" Text="" class="inputboxmid" MaxLength="10"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                                    ValidChars=".1234567890" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr bgcolor="#edf8fe" class=" row">
                                            <td>
                                                <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                                <asp:HiddenField ID="hdnPackageByHotelID" runat="server" Value="0" />
                                                <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtHalfDayPrice" runat="server" Text="" class="inputboxmid" MaxLength="10"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                                    ValidChars=".1234567890" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtFullDayPrice" runat="server" Text="" class="inputboxmid" MaxLength="10"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                                    ValidChars=".1234567890" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                    <EmptyDataTemplate>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td bgcolor="#98bcd6">
                                                    <table width="100%" border="0" cellspacing="1" cellpadding="5" id="package">
                                                        <tr bgcolor="#d8eefc" class=" row">
                                                            <td colspan="4" align="center">
                                                                <b>No Record found </b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                                <tr bgcolor="#c4d6e2">
                                    <td>
                                        Room rental
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="lblRentalVatEdit" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="center">
                                        <div id="calcHalfActual" runat="server">
                                        </div>
                                        <asp:HiddenField ID="hdnHaflCalValue" runat="server" Value="-1" />
                                    </td>
                                    <td align="center">
                                        <div id="calcFullActual" runat="server">
                                        </div>
                                        <asp:HiddenField ID="hdnFullCalValue" runat="server" Value="-1" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            <div class="save-cancel-prices-container-btn">
                <div class="save-cancel-prices-container-btn-left" id="divDeactiePackage" runat="server">
                    <div style="float: left">
                        <label>
                            <asp:CheckBox ID="chkActivePackage" Checked="false" runat="server" />
                            Deactivate package for online booking
                        </label>
                    </div>
                    <div style="float: left">
                    </div>
                </div>
                <div id="divDeactiePackageStandard" runat="server">
                    <div style="float: left">
                        <label>
                            Standard package is mandatory to sell online ( only made available as per min. 10
                            delegates)
                        </label>
                    </div>
                    <div style="float: left">
                    </div>
                </div>
                <div class="button_section" id="divSaveCancelPackage" runat="server">
                    <asp:LinkButton ID="lnkSavePackagePrice" runat="server" OnClick="lnkSavePackagePrice_Click"
                        CssClass="select">Save</asp:LinkButton>
                    <span>or</span>
                    <asp:LinkButton ID="lnkCancelPackagePrice" runat="server">Cancel</asp:LinkButton>
                </div>
            </div>
            <br />
            <div class="info-tab" id="divInfoTab" runat="server">
                <table width="100%" border="0" cellpadding="2">
                    <tr>
                        <td width="3%" valign="top">
                            <div class="info-tab-white">
                            </div>
                        </td>
                        <td width="30%" valign="top">
                            Active Tab
                        </td>
                        <td width="3%" valign="top">
                            <div class="info-tab-blue">
                            </div>
                        </td>
                        <td width="30%" valign="top">
                            Tab with filled details
                        </td>
                        <td width="3%" valign="top">
                            <div class="info-tab-red">
                            </div>
                        </td>
                        <td width="30%" valign="top">
                            Tab with unfilled details/New tab
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                            align="left">
                            <img src="../Images/help.jpg" />&nbsp;&nbsp;Save & Cancel button is not common for
                            all the tabs. Please save information for a tab before switching to another tab.
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="grid-main clearfix" style="float: left; width: 100%;" runat="server"
            id="divpackagesNot">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#98bcd6">
                        <table width="100%" border="0" cellspacing="1" cellpadding="5">
                            <tr bgcolor="#c4d6e2">
                                <td>
                                    <span style="color: Red">Warning ! Package does not exist for this country please contact
                                        the operator.</span>
                                </td>
                            </tr>
                        </table>
                </tr>
            </table>
        </div>
        <div class="grid-main clearfix">
            <div class="meeting-room-hire">
                <div class="save-cancel-prices-container-btn-new">
                    <div class="save-cancel-btn1">
                        <asp:LinkButton ID="lnkModifyMeetingRoomPrice" runat="server" class="cancel-btn"
                            OnClick="lnkModifyMeetingRoomPrice_Click">modify</asp:LinkButton>
                    </div>
                </div>
                <asp:ListView ID="lstViewMeetingRoomConfig" runat="server" ItemPlaceholderID="itemPlacehoderID"
                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewMeetingRoomConfig_ItemDataBound">
                    <LayoutTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="cofig">
                            <tr bgcolor="#c4d6e2">
                                <td width="17%">
                                    Meeting Room
                                </td>
                                <td width="4%" align="center">
                                    Vat%
                                </td>
                                <td width="11%" align="center">
                                    Theatre Style
                                </td>
                                <td width="9%" align="center">
                                    School
                                </td>
                                <td width="9%" align="center">
                                    U-Shape
                                </td>
                                <td width="9%" align="center">
                                    Boardroom
                                </td>
                                <td width="9%" align="center">
                                    Cocktail
                                </td>
                                <td align="center">
                                    Price 1/2 day &nbsp;<asp:Label ID="lblCSHalf" runat="server" Text=""></asp:Label>
                                </td>
                                <td align="center">
                                    Price full day &nbsp;<asp:Label ID="lblCSFull" runat="server" Text=""></asp:Label>
                                </td>
                                <td width="4%" align="center">
                                    Online
                                </td>
                            </tr>
                            <tr bgcolor="#c4d6e2">
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                    <img src="../Images/theatre.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/classroom.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/ushape.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/boardroom.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/cocktail.gif" alt="" />
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                            </tr>
                            <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr bgcolor="#d8eefc">
                            <td>
                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblMeetingRoomVat" runat="server"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblTheatreMin" runat="server" Text="0"></asp:Label>
                                &nbsp;-&nbsp;
                                <asp:Label ID="lblTheatreMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblSchoolMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblSchoolMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblUShapeMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblUShapeMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblBedroomMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblBedroomMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCocktailMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblCocktailMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblHalfDayPrice" runat="server"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblFullDayPrice" runat="server"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Image ID="imgIsOnline" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr bgcolor="#edf8fe">
                            <td>
                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblMeetingRoomVat" runat="server"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblTheatreMin" runat="server" Text="0"></asp:Label>
                                &nbsp;-&nbsp;
                                <asp:Label ID="lblTheatreMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblSchoolMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblSchoolMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblUShapeMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblUShapeMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblBedroomMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblBedroomMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCocktailMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblCocktailMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblHalfDayPrice" runat="server"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblFullDayPrice" runat="server"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Image ID="imgIsOnline" runat="server" />
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                    <EmptyDataTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="cofig">
                            <%--<tr bgcolor="#c4d6e2">
                                <td width="17%">
                                    Meeting Room
                                </td>
                                <td width="4%" align="center">
                                    Vat%
                                </td>
                                <td width="11%" align="center">
                                    Theatre Style
                                </td>
                                <td width="9%" align="center">
                                    School
                                </td>
                                <td width="9%" align="center">
                                    U-Shape
                                </td>
                                <td width="9%" align="center">
                                    Boardroom
                                </td>
                                <td width="9%" align="center">
                                    Cocktail
                                </td>
                                <td width="12%" align="center">
                                    Price 1/2 day &nbsp;<asp:Label ID="lblCSMeetingRoomHalf" runat="server" Text=""></asp:Label>
                                </td>
                                <td width="12%" align="center">
                                    Price full day &nbsp;<asp:Label ID="lblCSMeetingRoomFull" runat="server" Text=""></asp:Label>
                                </td>
                                <td width="4%" align="center">
                                    Online
                                </td>
                            </tr>--%>
                            <tr bgcolor="#c4d6e2">
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                    <img src="../Images/theatre.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/classroom.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/ushape.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/boardroom.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/cocktail.gif" alt="" />
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10" align="center" bgcolor="#d8eefc" class=" row">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:ListView>
                <div id="divmsgMeetingRoom" class="error" style="display: none;">
                </div>
                <asp:ListView ID="lstViewMeetingRoomConfigEdit" runat="server" ItemPlaceholderID="itemPlacehoderID"
                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewMeetingRoomConfigEdit_ItemDataBound">
                    <LayoutTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="meetingroomprice"
                            class="cofig">
                            <thead>
                                <tr bgcolor="#c4d6e2">
                                    <td>
                                        Meeting Room
                                    </td>
                                    <td align="center">
                                        Vat%
                                    </td>
                                    <td align="center">
                                        Theatre Style
                                    </td>
                                    <td align="center">
                                        School
                                    </td>
                                    <td align="center">
                                        U-Shape
                                    </td>
                                    <td align="center">
                                        Boardroom
                                    </td>
                                    <td align="center">
                                        Cocktail
                                    </td>
                                    <td align="center">
                                        Price 1/2 day &nbsp;<asp:Label ID="lblCSHalf" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="center">
                                        Price full day &nbsp;<asp:Label ID="lblCSFull" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align="center">
                                        Online
                                    </td>
                                </tr>
                                <tr bgcolor="#c4d6e2">
                                    <td align="center">
                                    </td>
                                    <td align="center">
                                    </td>
                                    <td align="center">
                                        <img src="../Images/theatre.gif" alt="" />
                                    </td>
                                    <td align="center">
                                        <img src="../Images/classroom.gif" alt="" />
                                    </td>
                                    <td align="center">
                                        <img src="../Images/ushape.gif" alt="" />
                                    </td>
                                    <td align="center">
                                        <img src="../Images/boardroom.gif" alt="" />
                                    </td>
                                    <td align="center">
                                        <img src="../Images/cocktail.gif" alt="" />
                                    </td>
                                    <td align="center">
                                    </td>
                                    <td align="center">
                                    </td>
                                    <td align="center">
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr bgcolor="#d8eefc">
                            <td>
                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                <asp:HiddenField ID="hdnMeetingRoomID" Value='<%#Eval("Id") %>' runat="server" />
                            </td>
                            <td align="center">
                                <asp:Label ID="lblMeetingRoomVat" runat="server"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblTheatreMin" runat="server" Text="0"></asp:Label>
                                &nbsp;-&nbsp;
                                <asp:Label ID="lblTheatreMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblSchoolMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblSchoolMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblUShapeMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblUShapeMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblBedroomMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblBedroomMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCocktailMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblCocktailMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtHalfDayPrice" runat="server" class="inputboxmid txtHalfDayPrice"
                                    Columns="8" Text="" MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtFullDayPrice" runat="server" class="inputboxmid txtFullDayPrice"
                                    Columns="8" Text="" MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </td>
                            <td align="center">
                                <asp:CheckBox ID="chkIsOnline" runat="server" CssClass="chkSelect"></asp:CheckBox>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr bgcolor="#edf8fe">
                            <td>
                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                <asp:HiddenField ID="hdnMeetingRoomID" Value='<%#Eval("Id") %>' runat="server" />
                            </td>
                            <td align="center">
                                <asp:Label ID="lblMeetingRoomVat" runat="server"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblTheatreMin" runat="server" Text="0"></asp:Label>
                                &nbsp;-&nbsp;
                                <asp:Label ID="lblTheatreMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblSchoolMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblSchoolMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center" valign="middle">
                                <asp:Label ID="lblUShapeMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblUShapeMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblBedroomMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblBedroomMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCocktailMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                                <asp:Label ID="lblCocktailMax" runat="server" Text="0"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtHalfDayPrice" runat="server" class="inputboxmid txtHalfDayPrice"
                                    Columns="8" Text="" MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtFullDayPrice" runat="server" class="inputboxmid txtFullDayPrice"
                                    Columns="8" Text="" MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </td>
                            <td align="center">
                                <asp:CheckBox ID="chkIsOnline" runat="server" CssClass="chkSelect"></asp:CheckBox>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                    <EmptyDataTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="cofig">
                            <%--<tr bgcolor="#c4d6e2">
                                <td>
                                    Meeting Room
                                </td>
                                <td align="center">
                                    Vat%
                                </td>
                                <td align="center">
                                    Theatre Style
                                </td>
                                <td align="center">
                                    School
                                </td>
                                <td align="center">
                                    U-Shape
                                </td>
                                <td align="center">
                                    Boardroom
                                </td>
                                <td align="center">
                                    Cocktail
                                </td>
                                <td align="center">
                                    Price 1/2 day &nbsp;<asp:Label ID="lblCSMeetingRoomHalfEdit" runat="server" Text=""></asp:Label>
                                </td>
                                <td align="center">
                                    Price full day &nbsp;<asp:Label ID="lblCSMeetingRoomFullEdit" runat="server" Text=""></asp:Label>
                                </td>
                                <td align="center">
                                    Online
                                </td>
                            </tr>--%>
                            <tr bgcolor="#c4d6e2">
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                    <img src="../Images/theatre.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/classroom.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/ushape.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/boardroom.gif" alt="" />
                                </td>
                                <td align="center">
                                    <img src="../Images/cocktail.gif" alt="" />
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                                <td align="center">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10" align="center" bgcolor="#d8eefc" class=" row">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:ListView>
                <div class="save-cancel-prices-container-btn" id="divSaveCancelMeetingRoom" runat="server"
                    visible="false">
                    <div class="button_section">
                        <asp:LinkButton ID="lnkSaveMeetingRoomPrice" OnClick="lnkSaveMeetingRoomPrice_Click"
                            class="select" runat="server">Save</asp:LinkButton>
                        <span>or</span>
                        <asp:LinkButton ID="lnkcancelMeetingRoomsPrice" class="cancel-btn" runat="server"
                            OnClick="lnkcancelMeetingRoomsPrice_Click">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-main clearfix">
            <div class="food-bever">
                <div class="save-cancel-prices-container-btn-new">
                    <div class="save-cancel-btn1">
                        <asp:LinkButton ID="lnkModifyBeverPrice" runat="server" class="cancel-btn" OnClick="lnkModifyBeverPrice_Click">modify</asp:LinkButton>
                    </div>
                </div>
                <asp:ListView ID="lstViewFoodBeveragesPrice" runat="server" ItemPlaceholderID="itemPlacehoderID"
                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewFoodBeveragesPrice_ItemDataBound">
                    <LayoutTemplate>
                        <div class="food-bever-heading">
                            <div class="food-bever-heading-box1">
                                Food & Beverages
                            </div>
                            <div class="food-bever-heading-box2">
                                Vat%
                            </div>
                            <div class="food-bever-heading-box3">
                                Description
                            </div>
                            <div class="food-bever-heading-box4">
                                Price 1/2 day &nbsp;<asp:Label ID="lblCSHalf" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="food-bever-heading-box5">
                                Price full day &nbsp;<asp:Label ID="lblCSFull" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="food-bever-heading-box6">
                                Online
                            </div>
                        </div>
                        <div class="food-bever-heading-list">
                            <ul>
                                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                            </ul>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="food-bever-heading-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox4">
                                <asp:Label ID="lblHalfDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox5">
                                <asp:Label ID="lblfullDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox6">
                                <asp:Image ID="imgIsOnline" runat="server" />
                            </div>
                        </li>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <li class="light">
                            <div class="food-bever-heading-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox4">
                                <asp:Label ID="lblHalfDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox5">
                                <asp:Label ID="lblfullDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox6">
                                <asp:Image ID="imgIsOnline" runat="server" />
                            </div>
                        </li>
                    </AlternatingItemTemplate>
                    <EmptyDataTemplate>
                        <%--<div class="food-bever-heading">
                            <div class="food-bever-heading-box1">
                                Food Beverages
                            </div>
                            <div class="food-bever-heading-box2">
                                Vat%
                            </div>
                            <div class="food-bever-heading-box3">
                                Description
                            </div>
                            <div class="food-bever-heading-box4">
                                Price 1/2 day (&#8364;)
                            </div>
                            <div class="food-bever-heading-box5">
                                Price full day (&#8364;)
                            </div>
                            <div class="food-bever-heading-box6">
                                Online
                            </div>
                        </div>--%>
                        <div class="food-bever-heading-list">
                            <ul>
                                <li style="text-align: center"><b>No record found !</b></li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>
                <div id="divmsgFoodandBravreges" class="error" style="display: none;">
                </div>
                <asp:ListView ID="lstViewFoodBeveragesPriceEdit" runat="server" ItemPlaceholderID="itemPlacehoderID"
                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewFoodBeveragesPriceEdit_ItemDataBound">
                    <LayoutTemplate>
                        <div class="food-bever-heading">
                            <div class="food-bever-heading-box1">
                                Food & Beverages
                            </div>
                            <div class="food-bever-heading-box2">
                                Vat%
                            </div>
                            <div class="food-bever-heading-box3">
                                Description
                            </div>
                            <div class="food-bever-heading-box4">
                                Price 1/2 day &nbsp;<asp:Label ID="lblCSHalf" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="food-bever-heading-box5">
                                Price full day &nbsp;<asp:Label ID="lblCSFull" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="food-bever-heading-box6">
                                Online
                            </div>
                        </div>
                        <div class="food-bever-heading-list" id="foodandbravrages">
                            <ul>
                                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                            </ul>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="food-bever-heading-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                                <asp:HiddenField ID="hdnPackageByHotelID" Value="0" runat="server" />
                                <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                            </div>
                            <div class="food-bever-heading-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox4">
                                <asp:TextBox ID="txtHalfDayPrice" runat="server" Columns="8" class="inputboxmid txtHalfDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="food-bever-heading-ulbox5">
                                <asp:TextBox ID="txtFullDayPrice" runat="server" Columns="8" class="inputboxmid txtFullDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="food-bever-heading-ulbox6">
                                <asp:CheckBox ID="chkIsOnline" runat="server" CssClass="chkSelect"></asp:CheckBox>
                            </div>
                        </li>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <li class="light">
                            <div class="food-bever-heading-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                                <asp:HiddenField ID="hdnPackageByHotelID" Value="0" runat="server" />
                                <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                            </div>
                            <div class="food-bever-heading-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="food-bever-heading-ulbox4">
                                <asp:TextBox ID="txtHalfDayPrice" runat="server" Columns="8" class="inputboxmid txtHalfDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="food-bever-heading-ulbox5">
                                <asp:TextBox ID="txtFullDayPrice" runat="server" Columns="8" class="inputboxmid txtFullDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="food-bever-heading-ulbox6">
                                <asp:CheckBox ID="chkIsOnline" runat="server" CssClass="chkSelect"></asp:CheckBox>
                            </div>
                        </li>
                    </AlternatingItemTemplate>
                    <EmptyDataTemplate>
                        <%--<div class="food-bever-heading">
                            <div class="food-bever-heading-box1">
                                Food Beverages
                            </div>
                            <div class="food-bever-heading-box2">
                                Vat%
                            </div>
                            <div class="food-bever-heading-box3">
                                Description
                            </div>
                            <div class="food-bever-heading-box4">
                                Price 1/2 day (&#8364;)
                            </div>
                            <div class="food-bever-heading-box5">
                                Price full day (&#8364;)
                            </div>
                            <div class="food-bever-heading-box6">
                                Online
                            </div>
                        </div>--%>
                        <div class="food-bever-heading-list">
                            <ul>
                                <li style="text-align: center"><b>No record found !</b></li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>
                <div class="save-cancel-prices-container-btn" id="divSaveCancelFoodBeverage" runat="server"
                    visible="false">
                    <div class="save-cancel-prices-container-btn-left">
                        <div style="float: left">
                        </div>
                        <div style="float: left">
                        </div>
                    </div>
                    <div class="button_section">
                        <asp:LinkButton ID="lnkSaveFoodBeveragesPrice" OnClick="lnkSaveFoodBeveragesPrice_Click"
                            class="select" runat="server">Save</asp:LinkButton>
                        <span>or</span>
                        <asp:LinkButton ID="lnkcancelFoodBeveragesPrice" class="cancel-btn" runat="server"
                            OnClick="lnkcancelFoodBeveragesPrice_Click">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-main clearfix">
            <div class="prices-container-equipment">
                <div class="save-cancel-prices-container-btn-new">
                    <div class="save-cancel-btn1">
                        <asp:LinkButton ID="lnkModifyEqipmentPrice" runat="server" class="cancel-btn" OnClick="lnkModifyEqipmentPrice_Click">modify</asp:LinkButton>
                    </div>
                </div>
                <asp:ListView ID="lstViewEquipmentPrice" runat="server" ItemPlaceholderID="itemPlacehoderID"
                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewEquipmentPrice_ItemDataBound">
                    <LayoutTemplate>
                        <div class="equipment-heading">
                            <div class="equipment-heading-box1">
                                Equipment
                            </div>
                            <div class="equipment-heading-box2">
                                Vat%
                            </div>
                            <div class="equipment-heading-box3">
                                Description
                            </div>
                            <div class="equipment-heading-box4">
                                Complimentary
                            </div>
                            <div class="equipment-heading-box5">
                                Price 1/2 day &nbsp;<asp:Label ID="lblCSHalf" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="equipment-heading-box6">
                                Price full day &nbsp;<asp:Label ID="lblCSFull" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="equipment-heading-box7">
                                Online
                            </div>
                        </div>
                        <div class="equipment-list">
                            <ul>
                                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                            </ul>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="equipment-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox4">
                                <asp:Image ID="imgIsEmentary" runat="server" />
                            </div>
                            <div class="equipment-ulbox5">
                                <asp:Label ID="lblHalfDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox6">
                                <asp:Label ID="lblFullDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox7">
                                <asp:Image ID="imgIsOnline" runat="server" />
                            </div>
                        </li>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <li class="light">
                            <div class="equipment-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox4">
                                <asp:Image ID="imgIsEmentary" runat="server" />
                            </div>
                            <div class="equipment-ulbox5">
                                <asp:Label ID="lblHalfDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox6">
                                <asp:Label ID="lblFullDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox7">
                                <asp:Image ID="imgIsOnline" runat="server" />
                            </div>
                        </li>
                    </AlternatingItemTemplate>
                    <EmptyDataTemplate>
                        <%--<div class="equipment-heading">
                            <div class="equipment-heading-box1">
                                Equipment
                            </div>
                            <div class="equipment-heading-box2">
                                Vat%
                            </div>
                            <div class="equipment-heading-box3">
                                Description
                            </div>
                            <div class="equipment-heading-box4">
                                Complimentary
                            </div>
                            <div class="equipment-heading-box5">
                                Price 1/2 day (&#8364;)
                            </div>
                            <div class="equipment-heading-box6">
                                Price full day (&#8364;)
                            </div>
                            <div class="equipment-heading-box7">
                                Online
                            </div>
                        </div>--%>
                        <div class="equipment-list">
                            <ul>
                                <li style="text-align: center"><b>No record found</b></li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>
                <div id="divmsgEquipment" class="error" style="display: none;">
                </div>
                <asp:ListView ID="lstViewEquipmentPriceEdit" runat="server" ItemPlaceholderID="itemPlacehoderID"
                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewEquipmentPriceEdit_ItemDataBound">
                    <LayoutTemplate>
                        <div class="equipment-heading">
                            <div class="equipment-heading-box1">
                                Equipment
                            </div>
                            <div class="equipment-heading-box2">
                                Vat%
                            </div>
                            <div class="equipment-heading-box3">
                                Description
                            </div>
                            <div class="equipment-heading-box4">
                                Complimentary
                            </div>
                            <div class="equipment-heading-box5">
                                Price 1/2 day &nbsp;<asp:Label ID="lblCSHalf" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="equipment-heading-box6">
                                Price full day &nbsp;<asp:Label ID="lblCSFull" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="equipment-heading-box7">
                                Online
                            </div>
                        </div>
                        <div class="equipment-list" id="equipment">
                            <ul>
                                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                            </ul>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="equipment-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                                <asp:HiddenField ID="hdnPackageByHotelID" Value="0" runat="server" />
                                <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                            </div>
                            <div class="equipment-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox4">
                                <asp:CheckBox ID="chkIsEmentary" AutoPostBack="true" OnCheckedChanged="check" runat="server" />
                            </div>
                            <div class="equipment-ulbox5">
                                <asp:TextBox ID="txtHalfDayPrice" Columns="8" runat="server" class="inputboxmid txtHalfDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="equipment-ulbox6">
                                <asp:TextBox ID="txtFullDayPrice" Columns="8" runat="server" class="inputboxmid txtFullDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="equipment-ulbox7">
                                <asp:CheckBox ID="chkIsOnline" runat="server" CssClass="chkSelect" />
                            </div>
                        </li>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <li class="light">
                            <div class="equipment-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                                <asp:HiddenField ID="hdnPackageByHotelID" Value="0" runat="server" />
                                <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                            </div>
                            <div class="equipment-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox4">
                                <asp:CheckBox ID="chkIsEmentary" AutoPostBack="true" OnCheckedChanged="check" runat="server" />
                            </div>
                            <div class="equipment-ulbox5">
                                <asp:TextBox ID="txtHalfDayPrice" Columns="8" runat="server" class="inputboxmid txtHalfDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="equipment-ulbox6">
                                <asp:TextBox ID="txtFullDayPrice" Columns="8" runat="server" class="inputboxmid txtFullDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="equipment-ulbox7">
                                <asp:CheckBox ID="chkIsOnline" runat="server" CssClass="chkSelect" />
                            </div>
                        </li>
                    </AlternatingItemTemplate>
                    <EmptyDataTemplate>
                        <%--<div class="equipment-heading">
                            <div class="equipment-heading-box1">
                                Equipment
                            </div>
                            <div class="equipment-heading-box2">
                                Vat%
                            </div>
                            <div class="equipment-heading-box3">
                                Description
                            </div>
                            <div class="equipment-heading-box4">
                                Complimentary
                            </div>
                            <div class="equipment-heading-box5">
                                Price 1/2 day (&#8364;)
                            </div>
                            <div class="equipment-heading-box6">
                                Price full day (&#8364;)
                            </div>
                            <div class="equipment-heading-box7">
                                Online
                            </div>
                        </div>--%>
                        <div class="food-bever-heading-list">
                            <ul>
                                <li style="text-align: center"><b>No record found</b></li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>
            </div>
            <div class="save-cancel-prices-container-btn" id="divSaveCancelEquipment" runat="server"
                visible="false" style="float: left">
                <div class="save-cancel-prices-container-btn-left">
                    <div style="float: left">
                    </div>
                    <div style="float: left">
                    </div>
                </div>
                <div class="button_section">
                    <asp:LinkButton ID="lnkSaveEqipmentPrice" OnClick="lnkSaveEquipmentPrice_Click" class="select"
                        runat="server">Save</asp:LinkButton>
                    <span>or</span>
                    <asp:LinkButton ID="lnkcancelEqipmentPrice" class="cancel-btn" runat="server" OnClick="lnkcancelEqipmentPrice_Click">Cancel</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="grid-main clearfix">
            <div class="prices-container-equipment">
                <div class="save-cancel-prices-container-btn-new">
                    <div class="save-cancel-btn1">
                        <asp:LinkButton ID="lnkModifyOthersPrice" runat="server" class="cancel-btn" OnClick="lnkModifyOthersPrice_Click">modify</asp:LinkButton>
                    </div>
                </div>
                <asp:ListView ID="lstViewOthersPrice" runat="server" ItemPlaceholderID="itemPlacehoderID"
                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewOthersPrice_ItemDataBound">
                    <LayoutTemplate>
                        <div class="equipment-heading">
                            <div class="equipment-heading-box1">
                                Others
                            </div>
                            <div class="equipment-heading-box2">
                                Vat%
                            </div>
                            <div class="equipment-heading-box3">
                                Description
                            </div>
                            <div class="equipment-heading-box4">
                                Complimentary
                            </div>
                            <div class="equipment-heading-box5">
                                Price 1/2 day &nbsp;<asp:Label ID="lblCSHalf" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="equipment-heading-box6">
                                Price full day &nbsp;<asp:Label ID="lblCSFull" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="equipment-heading-box7">
                                Online
                            </div>
                        </div>
                        <div class="equipment-list">
                            <ul>
                                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                            </ul>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="equipment-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox4">
                                <asp:Image ID="imgIsEmentary" runat="server" />
                            </div>
                            <div class="equipment-ulbox5">
                                <asp:Label ID="lblHalfDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox6">
                                <asp:Label ID="lblFullDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox7">
                                <asp:Image ID="imgIsOnline" runat="server" />
                            </div>
                        </li>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <li class="light">
                            <div class="equipment-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox4">
                                <asp:Image ID="imgIsEmentary" runat="server" />
                            </div>
                            <div class="equipment-ulbox5">
                                <asp:Label ID="lblHalfDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox6">
                                <asp:Label ID="lblFullDayPrice" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox7">
                                <asp:Image ID="imgIsOnline" runat="server" />
                            </div>
                        </li>
                    </AlternatingItemTemplate>
                    <EmptyDataTemplate>
                        <%--<div class="equipment-heading">
                            <div class="equipment-heading-box1">
                                Others
                            </div>
                            <div class="equipment-heading-box2">
                                Vat%
                            </div>
                            <div class="equipment-heading-box3">
                                Description
                            </div>
                            <div class="equipment-heading-box4">
                                Complimentary
                            </div>
                            <div class="equipment-heading-box5">
                                Price 1/2 day (&#8364;)
                            </div>
                            <div class="equipment-heading-box6">
                                Price full day (&#8364;)
                            </div>
                            <div class="equipment-heading-box7">
                                Online
                            </div>
                        </div>--%>
                        <div class="equipment-list">
                            <ul>
                                <li style="text-align: center"><b>No record found</b></li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>
                <div id="divmsgOthers" class="error" style="display: none;">
                </div>
                <asp:ListView ID="lstViewOthersPriceEdit" runat="server" ItemPlaceholderID="itemPlacehoderID"
                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewOthersPriceEdit_ItemDataBound">
                    <LayoutTemplate>
                        <div class="equipment-heading">
                            <div class="equipment-heading-box1">
                                Others
                            </div>
                            <div class="equipment-heading-box2">
                                Vat%
                            </div>
                            <div class="equipment-heading-box3">
                                Description
                            </div>
                            <div class="equipment-heading-box4">
                                Complimentary
                            </div>
                            <div class="equipment-heading-box5">
                                Price 1/2 day &nbsp;<asp:Label ID="lblCSHalf" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="equipment-heading-box6">
                                Price full day &nbsp;<asp:Label ID="lblCSFull" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="equipment-heading-box7">
                                Online
                            </div>
                        </div>
                        <div>
                            <div class="equipment-list" id="others">
                                <ul>
                                    <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                                </ul>
                            </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="equipment-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                                <asp:HiddenField ID="hdnPackageByHotelID" Value="0" runat="server" />
                                <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                            </div>
                            <div class="equipment-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox4">
                                <asp:CheckBox ID="chkIsEmentary" AutoPostBack="true" OnCheckedChanged="check" runat="server" />
                            </div>
                            <div class="equipment-ulbox5">
                                <asp:TextBox ID="txtHalfDayPrice" Columns="8" runat="server" class="inputboxmid txtHalfDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="equipment-ulbox6">
                                <asp:TextBox ID="txtFullDayPrice" Columns="8" runat="server" class="inputboxmid txtFullDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="equipment-ulbox7">
                                <asp:CheckBox ID="chkIsOnline" runat="server" CssClass="chkSelect" />
                            </div>
                        </li>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <li class="light">
                            <div class="equipment-ulbox1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                                <asp:HiddenField ID="hdnPackageByHotelID" Value="0" runat="server" />
                                <asp:HiddenField ID="hdnItemID" runat="server" Value="0" />
                            </div>
                            <div class="equipment-ulbox2">
                                <asp:Label ID="lblVat" runat="server" Text='<%# Eval("VAT")%>'></asp:Label>
                            </div>
                            <div class="equipment-ulbox3">
                                <asp:Label ID="lblDesc" runat="server"></asp:Label>
                            </div>
                            <div class="equipment-ulbox4">
                                <asp:CheckBox ID="chkIsEmentary" AutoPostBack="true" OnCheckedChanged="check" runat="server" />
                            </div>
                            <div class="equipment-ulbox5">
                                <asp:TextBox ID="txtHalfDayPrice" Columns="8" runat="server" class="inputboxmid txtHalfDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtHalfDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="equipment-ulbox6">
                                <asp:TextBox ID="txtFullDayPrice" Columns="8" runat="server" class="inputboxmid txtFullDayPrice"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFullDayPrice"
                                    ValidChars=".0123456789" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="equipment-ulbox7">
                                <asp:CheckBox ID="chkIsOnline" runat="server" CssClass="chkSelect" />
                            </div>
                        </li>
                    </AlternatingItemTemplate>
                    <EmptyDataTemplate>
                        <%--<div class="equipment-heading">
                            <div class="equipment-heading-box1">
                                Others
                            </div>
                            <div class="equipment-heading-box2">
                                Vat%
                            </div>
                            <div class="equipment-heading-box3">
                                Description
                            </div>
                            <div class="equipment-heading-box4">
                                Complimentary
                            </div>
                            <div class="equipment-heading-box5">
                                Price 1/2 day (&#8364;)
                            </div>
                            <div class="equipment-heading-box6">
                                Price full day (&#8364;)
                            </div>
                            <div class="equipment-heading-box7">
                                Online
                            </div>
                        </div>--%>
                        <div class="food-bever-heading-list">
                            <ul>
                                <li style="text-align: center"><b>No record found</b></li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>
            </div>
            <div class="save-cancel-prices-container-btn" id="divSaveCancelOthers" runat="server"
                visible="false">
                <div class="save-cancel-prices-container-btn-left">
                    <div style="float: left">
                    </div>
                    <div style="float: left">
                    </div>
                </div>
                <div class="button_section">
                    <asp:LinkButton ID="lnkSaveOthersPrice" OnClick="lnkSaveOthersPrice_Click" class="select"
                        runat="server">Save</asp:LinkButton>
                    <span>or</span>
                    <asp:LinkButton ID="lnkcancelOthersPrice" class="cancel-btn" runat="server" OnClick="lnkcancelOthersPrice_Click">Cancel</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="info-tab">
        <table width="100%" border="0" cellpadding="2">
            <tr>
                <td valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold" align="left">
                    <img src="../Images/help.jpg" />&nbsp;&nbsp;Note: "Leave blank if your hotel/venue
                    does not offer a specific item/service.
                </td>
            </tr>
        </table>
    </div>
    <div class="button_sectionNext" runat="server" id="divNext">
        <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;"
            OnClick="btnNext_Click" />
    </div>
    <div id="Loding_overlaySec">
        <img alt="" src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Saving...</span></div>
    <script type="text/javascript" language="javascript">
        jQuery(document).ready(function () {
            ///Package Price///
            jQuery("#divmsgpackage").hide();
            <% if(lnkSavePackagePrice.Visible== true) {%>
            jQuery("#<%= lnkSavePackagePrice.ClientID %>").bind("click", function () {
                var isValid = true;
                jQuery("#tblactual tr td input:text").each(function () {
                    if (!isNaN(jQuery(this).val().trim()) && jQuery(this).val().trim()!="") {
                        if (parseInt(jQuery(this).val(),10) >= 0) {

                        }
                        else {
                            jQuery(this).focus();
                            jQuery("#divmsgpackage").html("Please enter value greater then 0.");
                            jQuery("#divmsgpackage").show();
                            jQuery("#<%=divmsgpackageByServer.ClientID %>").hide();
                            isValid = false;
                        }
                    }
                    else {
                        jQuery(this).focus();
                        jQuery("#divmsgpackage").html("Please enter All fields required Halfday and Fullday prices.");
                        jQuery("#divmsgpackage").show();
                        jQuery("#<%=divmsgpackageByServer.ClientID %>").hide();
                        isValid = false;
                    }
                });
                if(!isValid){
                    var offseterror = jQuery("#divmsgpackage").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }
                else{
                    jQuery("#divmsgpackage").hide();
                    jQuery("#Loding_overlaySec span").html("Saving...");
                    jQuery("#Loding_overlaySec").show();
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                }
            });
            <%} %>
            ///Meeting room prices///
            <% if(lnkSaveMeetingRoomPrice.Visible == true) {%>
            jQuery("#<%= lnkSaveMeetingRoomPrice.ClientID %>").bind("click", function () {
                var isValid = true;
                
                if(jQuery("#meetingroomprice > tbody ").size() >0)
                {   
                    jQuery("#meetingroomprice tbody tr").each(function(index,row)
                    {
                        var chkSelect = jQuery(row).find("input[type='checkbox']").get(0);
                        if(chkSelect != null)
                        {
                            if(chkSelect.checked == true)
                            {
                                debugger;
                                var txtBox = jQuery(row).find("input[type='text']");
                                var txtHalfDayPrice = txtBox.filter(".txtHalfDayPrice").get(0);
                                var txtFullDayPrice = txtBox.filter(".txtFullDayPrice").get(0);
                                 if(jQuery(txtHalfDayPrice).val().length <= 0 || jQuery(txtFullDayPrice).val().length <= 0)
                                
                                {
                                    jQuery(this).focus();
                                    jQuery("#divmsgMeetingRoom").html("Please enter valid price for online meeting room.");
                                    jQuery("#divmsgMeetingRoom").show();
                                    isValid = false;
                                
                                }
                                else 
                                {
                                    if(parseInt(jQuery(txtHalfDayPrice).val()) <= 0 || parseInt(jQuery(txtFullDayPrice).val()) <= 0)
                                    {
                                        jQuery(this).focus();
                                        jQuery("#divmsgMeetingRoom").html("Please enter valid price for online meeting room.");
                                        jQuery("#divmsgMeetingRoom").show();
                                        isValid = false;
                                
                                    }
                                }
                            }
                        }
                    });
                }
                else{
                jQuery("#meetingroomprice tr").each(function(index,row)
                    {
                        var chkSelect = jQuery(row).find("input[type='checkbox']");
                        if(chkSelect != null)
                        {
                            if(chkSelect.checked == true)
                            {
                                debugger;
                                var txtBox = jQuery(row).find("input[type='text']");
                                var half = txtBox.get(0);
                                var txtHalfDayPrice = txtBox.filter(".txtHalfDayPrice").get(0);
                                var txtFullDayPrice = txtBox.filter(".txtFullDayPrice").get(0);
                                var m = jQuery(txtHalfDayPrice).val();
                                var n = jQuery(txtFullDayPrice).val();
                                if(jQuery(txtHalfDayPrice).val().length <= 0 || jQuery(txtFullDayPrice).val().length <= 0)
                                
                                {
                                    jQuery(this).focus();
                                    jQuery("#divmsgMeetingRoom").html("Please enter valid price for online meeting room.");
                                    jQuery("#divmsgMeetingRoom").show();
                                    isValid = false;
                                
                                }
                                else 
                                {
                                    if(parseInt(jQuery(txtHalfDayPrice).val()) <= 0 || parseInt(jQuery(txtFullDayPrice).val()) <= 0)
                                    {
                                        jQuery(this).focus();
                                        jQuery("#divmsgMeetingRoom").html("Please enter valid price for online meeting room.");
                                        jQuery("#divmsgMeetingRoom").show();
                                        isValid = false;
                                
                                    }
                                }
                            }
                        }
                  });
                }

                if(!isValid){
                    var offseterror = jQuery("#divmsgMeetingRoom").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }
                else{
                    jQuery("#divmsgMeetingRoom").hide();
                    jQuery("#Loding_overlaySec span").html("Saving...");                    
                    jQuery("#Loding_overlaySec").show();
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                }
            });
             <%} %>
            ///Food and bravrages///
//            jQuery("#divmsgFoodandBravreges").hide();
            <% if(lnkSaveFoodBeveragesPrice.Visible == true) {%>
            jQuery("#<%= lnkSaveFoodBeveragesPrice.ClientID %>").bind("click", function () {
               var isValid = true;
                    jQuery("#foodandbravrages ul li").each(function(index,row)
                    {
                        var chkSelect = jQuery(row).find("input[type='checkbox']").filter(".chkSelect").get(0);
                        if(typeof(chkSelect) == "undefined" || chkSelect == null)
                        {
                        chkSelect = jQuery(row).find("span").filter(".chkSelect").find("input[type='checkbox']").get(0);
                        }
;
                        if(chkSelect != null)
                        {
                            if(chkSelect.checked == true)
                            {
                                var txtBox = jQuery(row).find("input[type='text']");
                                var half = txtBox.get(0);
                                var txtHalfDayPrice = txtBox.filter(".txtHalfDayPrice").get(0);
                                var txtFullDayPrice = txtBox.filter(".txtFullDayPrice").get(0);
                                if(jQuery(txtHalfDayPrice).val().length <= 0 || jQuery(txtFullDayPrice).val().length <= 0)
                                {
                                    jQuery(this).focus();
                                    jQuery("#divmsgFoodandBravreges").html("Please enter valid price for online Item.");
                                    jQuery("#divmsgFoodandBravreges").show();
                                    isValid = false;
                                }
                                else
                                {
                                if(parseInt(jQuery(txtHalfDayPrice).val()) <= 0 || parseInt(jQuery(txtFullDayPrice).val()) <= 0)
                                {
                                    jQuery(this).focus();
                                    jQuery("#divmsgFoodandBravreges").html("Please enter valid price for online Item.");
                                    jQuery("#divmsgFoodandBravreges").show();
                                    isValid = false;
                                }
                                
                                }
                            }
                        }
                    });
                if(!isValid){
                    var offseterror = jQuery("#divmsgFoodandBravreges").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }
                else{
                   jQuery("#divmsgMeetingRoom").hide();
                   jQuery("#Loding_overlaySec span").html("Saving...");                    
                   jQuery("#Loding_overlaySec").show();
                   document.getElementsByTagName('html')[0].style.overflow = 'hidden';
              }
           });
           <% } %>
            ///Equipment///
           //jQuery("#divmsgEquipment").hide();
            <% if(lnkSaveEqipmentPrice.Visible== true) {%>
           jQuery("#<%= lnkSaveEqipmentPrice.ClientID %>").bind("click", function (e) {
               var isValid = true;
               debugger;
                jQuery("#equipment ul li").each(function(index,row)
                    { 
                        var chkSelect = jQuery(row).find("input[type='checkbox']").filter(".chkSelect").get(0);
                        if(typeof(chkSelect) == "undefined" || chkSelect == null)
                        {
                        chkSelect = jQuery(row).find("span").filter(".chkSelect").find("input[type='checkbox']").get(0);
                        }
                        if(chkSelect != null)
                        {debugger;
                            if(chkSelect.checked == true)
                            {debugger;
                                var txtBox = jQuery(row).find("input[type='text']");
                                var txtHalfDayPrice = txtBox.filter(".txtHalfDayPrice").get(0);
                                var txtFullDayPrice = txtBox.filter(".txtFullDayPrice").get(0);
                                if(txtHalfDayPrice == undefined || txtFullDayPrice == undefined)
                                {
                                }
                                else{
                                if(jQuery(txtHalfDayPrice).val().length <= 0 || jQuery(txtFullDayPrice).val().length <= 0)
                                {
                                    jQuery(this).focus();
                                    jQuery("#divmsgEquipment").html("Please enter valid price for online Item.");
                                    jQuery("#divmsgEquipment").show();
                                    isValid = false;

                                }
                                else
                                {
                                
                                 if(parseInt(jQuery(txtHalfDayPrice).val()) <= 0 || parseInt(jQuery(txtFullDayPrice).val()) <= 0)
                                 {
                                    jQuery(this).focus();
                                    jQuery("#divmsgEquipment").html("Please enter valid price for online Item.");
                                    jQuery("#divmsgEquipment").show();
                                    isValid = false;

                                 }
                                 }
                                
                                }
                            }
                        }
                    });

               if(isValid == false)
               {
               debugger;
                    var offseterror = jQuery("#divmsgEquipment").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    e.preventDefault();
                    return false;
                    
                }
                else{
                debugger;
                    jQuery("#divmsgMeetingRoom").hide();
                    jQuery("#Loding_overlaySec span").html("Saving...");                    
                    jQuery("#Loding_overlaySec").show();
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
               }
            });
           <%} %>


            <% if(lnkSaveOthersPrice.Visible== true) {%>
            
           jQuery("#<%= lnkSaveOthersPrice.ClientID %>").bind("click", function (e) {
               var isValid = true;
               debugger;
                jQuery("#others ul li").each(function(index,row)
                    { 
                    
                        var chkSelect = jQuery(row).find("input[type='checkbox']").filter(".chkSelect").get(0);
                        if(typeof(chkSelect) == "undefined" || chkSelect == null)
                        {
                        chkSelect = jQuery(row).find("span").filter(".chkSelect").find("input[type='checkbox']").get(0);
                        }
                        if(chkSelect != null)
                        {debugger;
                        
                            if(chkSelect.checked == true)
                            {debugger;
                            
                                var txtBox = jQuery(row).find("input[type='text']");
                                var txtHalfDayPrice = txtBox.filter(".txtHalfDayPrice").get(0);
                                var txtFullDayPrice = txtBox.filter(".txtFullDayPrice").get(0);
                                if(txtHalfDayPrice == undefined || txtFullDayPrice == undefined)
                                {
                                }
                                else{
                                if(jQuery(txtHalfDayPrice).val().length <= 0 || jQuery(txtFullDayPrice).val().length <= 0)
                                {
                                
                                    jQuery(this).focus();
                                    jQuery("#divmsgOthers").html("Please enter valid price for online Item.");
                                    jQuery("#divmsgOthers").show();
                                    isValid = false;

                                }
                                else
                                {
                                
                                 if(parseInt(jQuery(txtHalfDayPrice).val()) <= 0 || parseInt(jQuery(txtFullDayPrice).val()) <= 0)
                                 {
                                    jQuery(this).focus();
                                    jQuery("#divmsgOthers").html("Please enter valid price for online Item.");
                                    jQuery("#divmsgOthers").show();
                                    isValid = false;

                                 }
                                 }
                                
                                }
                            }
                            else{
                                
                            }
                        }
                    });
debugger;
               if(isValid == false)
               {
               debugger;
                    var offseterror = jQuery("#divmsgOthers").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    e.preventDefault();
                    return false;
                    
                }
                else{
                debugger;
                    jQuery("#divmsgMeetingRoom").hide();
                    jQuery("#Loding_overlaySec span").html("Saving...");                    
                    jQuery("#Loding_overlaySec").show();
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
               }
            });
           <%} %>

        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(function () {
                jQuery('#tblactual tr td:nth-child(3) input:text').change(function (e) {
                    calculateSum(3); // sum of 4th column
                });
                jQuery('#tblactual tr td:nth-child(4) input:text').change(function (e) {
                    calculateSum(4); // sum of 4th column
                });
            });
        });

        function calculateSum(colidx) {
            total = 0;
            var firstval = 0;
            var nextval = 0;
            if (jQuery("#tblactual > tbody ").size() > 0) {

                for (var p = 0; p < jQuery("#tblactual tbody tr td:nth-child(" + colidx + ") input:text").length; p++) {
                    if (p == 0) {
                        if (!isNaN(jQuery("#tblactual tbody tr td:nth-child(" + colidx + ") input:text:eq(" + (p) + ")").val()) && jQuery("#tblactual tbody tr td:nth-child(" + colidx + ") input:text:eq(" + (p) + ")").val() != '') {
                            firstval = parseFloat(jQuery("#tblactual tbody tr td:nth-child(" + colidx + ") input:text:eq(" + p + ")").val());

                        }
                    }
                    else {
                        if (!isNaN(jQuery("#tblactual tbody tr td:nth-child(" + colidx + ") input:text:eq(" + (p - 1) + ")").val()) && jQuery("#tblactual tbody tr td:nth-child(" + colidx + ") input:text:eq(" + (p - 1) + ")").val() != '') {
                            nextval += parseFloat(jQuery("#tblactual tbody tr td:nth-child(" + colidx + ") input:text:eq(" + p + ")").val());

                        }
                    }
                }
            }
            else {
                for (var p = 0; p < jQuery("#tblactual tr td:nth-child(" + colidx + ") input:text").length; p++) {
                    if (p == 0) {
                        if (!isNaN(jQuery("#tblactual tr td:nth-child(" + colidx + ") input:text:eq(" + (p) + ")").val()) && jQuery("#tblactual tr td:nth-child(" + colidx + ") input:text:eq(" + (p) + ")").val() != '') {
                            firstval = parseFloat(jQuery("#tblactual tr td:nth-child(" + colidx + ") input:text:eq(" + p + ")").val());

                        }
                    }
                    else {
                        if (!isNaN(jQuery("#tblactual tr td:nth-child(" + colidx + ") input:text:eq(" + (p - 1) + ")").val()) && jQuery("#tblactual tr td:nth-child(" + colidx + ") input:text:eq(" + (p - 1) + ")").val() != '') {
                            nextval += parseFloat(jQuery("#tblactual tr td:nth-child(" + colidx + ") input:text:eq(" + p + ")").val());

                        }
                    }
                }
            }
            total = firstval - nextval;
            if (total < 0) {

                if (parseInt(colidx) == 3) {
                    jQuery('#<%=calcHalfActual.ClientID %>').html(total + '&nbsp;(Invalid value)');
                    jQuery('#<%=hdnHaflCalValue.ClientID %>').val('-1');
                }
                else {
                    jQuery('#<%=calcFullActual.ClientID %>').html(total + '&nbsp;(Invalid value)');
                    jQuery('#<%=hdnFullCalValue.ClientID %>').val('-1');
                }
            }
            else {
                if (parseInt(colidx) == 3) {
                    if (!isNaN(total)) {

                        jQuery('#<%=calcHalfActual.ClientID %>').html(total.toFixed(2));
                        jQuery('#<%=hdnHaflCalValue.ClientID %>').val(total.toFixed(2));
                    }
                }
                else {
                    if (!isNaN(total)) {
                        jQuery('#<%=calcFullActual.ClientID %>').html(total.toFixed(2));
                        jQuery('#<%=hdnFullCalValue.ClientID %>').val(total.toFixed(2));
                    }
                }
            }
        }
   
    </script>
    <script type="text/javascript">
        function SelectPackage(Id) {
            jQuery('#<%= hdnPackage.ClientID %>').val(Id);
            jQuery('#<%= lnkSelectPackage.ClientID %>').click();
        }
        jQuery(document).ready(function () {

            var SeletedRowID = jQuery('#<%=hdnPackage.ClientID %>').val();
            //alert(SeletedRowID);
            if (SeletedRowID != 0) {
                jQuery('#' + SeletedRowID).removeClass().addClass("packagetabwhite");

            }
        });
        
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                var MeetingroomChanges = jQuery('#<%=hdnWithOutChangeMeetingroom.ClientID %>').val();
                var FoodBeveragesChanges = jQuery('#<%=hdnWithOutChangeFoodBeverages.ClientID %>').val();
                var EquipmentItemChanges = jQuery('#<%=hdnWithOutChangeEquipmentItem.ClientID %>').val();
                var result = true;
                if (MeetingroomChanges == "0" || FoodBeveragesChanges == "0" || EquipmentItemChanges == "0") {
                    result = confirm("Are you sure you want proceed to next without saving ?");
                    if (result) {
                        jQuery("body").scrollTop(0);
                        jQuery("html").scrollTop(0);
                        jQuery("#Loding_overlaySec span").html("Loading...");
                        jQuery("#Loding_overlaySec").show();
                        document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }
            });




        });
    </script>
    <script language="javascript" type="text/javascript">
        //added loading image for dynamic tab
        jQuery(document).ready(function () {
            jQuery("#<%= lnkSelectPackage.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
</asp:Content>
