﻿<%@ Page Title="Hotel User - Pricing" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true"
    CodeFile="SalesAndPromo.aspx.cs" Inherits="HotelUser_SalesAndPromo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .disable
        {
            background: none no-repeat scroll 1px center gray !important;
            margin: 0 0 0 2px;
            padding: 0 0 0 0;
            text-align: center;
        }
    </style>
    <script src="../js/SalesAndPromo.js" type="text/javascript"></script>
    <script src="../js/FormManagerSpecialPrice.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        Set special prices for meeting rooms</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server"><asp:HiddenField ID="hdnCurrentPanel" runat="server" />
<asp:HiddenField ID="hdnMinPercentage" runat="server" />
<asp:HiddenField ID="hdnMaxPercentage" runat="server" />
    <div class="new_right">
        <div class="pager_header" >
            <p>
                <a style="float: left; margin-bottom: 5px;" href="javascript:void(0);" onclick="window.scroll(0,0);document.getElementsByTagName('html')[0].style.overflow='hidden';document.getElementById('adjust-meeting').style.display='block';document.getElementById('adjust-meeting-overlay').style.display='block';" />
                Modify period % here.</a><div id="result"></div>
            </p>
        </div>
        <div class="pager_header"><div style="float:left;width:10%;height:20px;" ><a href="javascript:void(0);" id="prevlink">Prev 30 days</a></div>
            <div class="page_text" style="width:80%;text-align:center;" >
                <a href="javascript:void(0);" id="left">
                    <img src="../images/prev_bullet.png" alt="prev" border="0" /></a> <span id="spnDateYear">
                        May 2011</span> <a href="javascript:void(0)" id="right">
                            <img src="../images/next_bullet.png" alt="Next" /></a></div>
        <div style="float:left;width:10%;height:20px;"><a href="javascript:void(0);" id="nextlink" >Next 30 days</a></div>
        </div>
        <div class="specials-screen-main">
            <table  width="100%" border="0" cellspacing="0" cellpadding="0" class="specials-screen-table">
                <tr id="calander"></tr>
                <tr ><th colspan="31" class="head1" height="22" align="left">Increase / decrease % room hire in packages:</th></tr>
                <tr  id="packages" bgcolor="#d9eefc"></tr>
                <tr ><th colspan="31" class="head1" height="22" align="left">Increase / decrease % standalone meeting room:</th></tr>
                <tr  id="meetingRoomPrice" bgcolor="#d9eefc">
                </tr>
                <tr >
                    <th colspan="31" height="22" >
                        <div class="save-cancel">
                            <div class="button_section">
                                <asp:LinkButton ID="lnkSave" runat="server" CssClass="select" OnClick="lnkSave_Click">Save</asp:LinkButton>
                                <span>or</span> <a href="SalesAndPromo.aspx">Cancel</a><asp:HiddenField ID="hdnPackageDiscount"
                                    runat="server" />
                                <asp:HiddenField ID="hdnMeetingRoomDiscount" runat="server" />
                                <asp:HiddenField ID="hdnStartingDate" runat="server" />
                            </div>
                        </div>
                    </th>
                </tr>
                <%--<tr >
                    <td colspan="32">&nbsp;</td>
                </tr>--%>
                <asp:Literal ID="ltrPackage" runat="server"></asp:Literal>
                <tr >
                    <th colspan="31" style="height:20px;" >&nbsp;</th>
                </tr>
                <tr >
                    <th class="head1" colspan="32" height="22" style="display:none;">
                        Availability 
                    </th>
                </tr>
                <tr  id="divAvailability" style="display:none;">
                </tr>
                <tr >
                    <th colspan="31" style="display:none;" >&nbsp;</th>
                </tr>
                <% if (count > 0)
                   { %>
                <tr>
                    <td  colspan="31" class="BorderTOP">
                        <div class="pager_header" style="width:auto;border:none;" >
                            <p>
                                <a style="float: left; margin-bottom: 5px;" href="javascript:void(0);" onclick="window.scroll(0,0);document.getElementsByTagName('html')[0].style.overflow='hidden';document.getElementById('adjust-bedroom').style.display='block';document.getElementById('adjust-meeting-overlay').style.display='block';" />
                                Modify bedrooms Price here.</a>
                            </p>
                        </div>
                    </td>
                </tr>
                <tr >
                    <th class="head1"  colspan="31" height="22">
                        Bedroom Pricing 
                    </th>
                </tr>
                <asp:Literal ID="ltrBedroom" runat="server"></asp:Literal>
                <tr id="savebedroom" runat="server" >
                    <th colspan="31" height="22">
                        <div class="save-cancel">
                            <div class="button_section">
                                <asp:LinkButton ID="lnkSaveBedroom" runat="server" CssClass="select" OnClick="lnkSaveBedroom_Click">Save</asp:LinkButton>
                                <span>or</span> <a href="SalesAndPromo.aspx">Cancel</a>
                                <asp:HiddenField ID="hdnBedroom1ID" runat="server" />
                                <asp:HiddenField ID="hdnBedroom2ID" runat="server" />
                                <asp:HiddenField ID="hdnBedroom1single" runat="server" />
                                <asp:HiddenField ID="hdnBedroom2single" runat="server" />
                                <asp:HiddenField ID="hdnBedroom1double" runat="server" />
                                <asp:HiddenField ID="hdnBedroom2double" runat="server" />
                            </div>
                        </div>
                    </th>
                </tr>
                <%} %>
            </table>
        </div>
        <div class="specials-screen-main">

            <table id="trLegend" style="float: left" runat="server">
                <tr>
                    <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                        align="left">
                        <img src="../Images/help.jpg" />&nbsp;&nbsp;The prices shown may be hidden behind. Kindly hover over the particlaur cell to view the Full value/Price.<br />
                     
                    </td>
                </tr>
            </table>
            <%--<div class="specials-screen">
                <ul class="top" id="calander">
                </ul>
            </div>--%>
            <%--<div class="increase_decreas">
                <h3>
                    Increase / decrease % room hire in packages:</h3>
                <ul class="ddr" id="packages">
                </ul>
                <h3>
                    Increase / decrease % standalone meeting room:</h3>
                <ul class="ddr" id="meetingRoomPrice">
                </ul>
            </div>
            <div class="save-cancel">
                <div class="button_section">
                    <asp:LinkButton ID="lnkSave" runat="server" CssClass="select" OnClick="lnkSave_Click">Save</asp:LinkButton>
                    <span>or</span> <a href="SalesAndPromo.aspx">Cancel</a><asp:HiddenField ID="hdnPackageDiscount"
                        runat="server" />
                    <asp:HiddenField ID="hdnMeetingRoomDiscount" runat="server" />
                    <asp:HiddenField ID="hdnStartingDate" runat="server" />
                </div>
            </div>--%>
        </div>
        <%--<div class="specials-screen-main" style="display:none;">
            <div class="blank">
            </div>
            <asp:Literal ID="ltrPackage" runat="server"></asp:Literal>
            <div class="button_section">
            </div>
        </div>
        <div class="blank"  style="display:none;">
        </div>
        <div class="specials-screen-main" style="display:none;">
            <asp:Literal ID="ltrMeetingRoom" runat="server"></asp:Literal>
            <div class="blank">
            </div>
            <h4 class="ddr1">
                Availability</h4>
            <ul class="ddr3" id="divAvailability">
                
            </ul>
        </div>--%>
    </div>
    <div id="adjust-meeting-overlay">
    </div>
    <div id="adjust-meeting">
        <div class="popup-top1">
        </div>
        <div class="popup-mid1">
            <div class="popup-mid-inner1">
                <div class="adjust-meeting-form">
                    <div class="adjust-meeting-date">
                        <div class="error">
                        </div>
                        <div class="adjust-meeting-date-box1">
                            From
                        </div>
                        <div class="adjust-meeting-date-box">
                            <asp:TextBox ID="txtFrom" runat="server" value="dd/mm/yy" class="inputbox1"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"
                                PopupButtonID="calFrom" Format="dd/MM/yy">
                            </asp:CalendarExtender>
                            
                        </div>
                        <div class="adjust-meeting-date-box2">
                            <input type="image" src="../Images/cal-2.png" id="calFrom" />
                        </div>
                        <div class="adjust-meeting-date-box1">
                            To
                        </div>
                        <div class="adjust-meeting-date-box">
                            <asp:TextBox ID="txtTo" runat="server" value="dd/mm/yy" class="inputbox1"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
                                PopupButtonID="calTo" Format="dd/MM/yy">
                            </asp:CalendarExtender>
                            
                        </div>
                        <div class="adjust-meeting-date-box2">
                            <input type="image" src="../Images/cal-2.png" id="calTo" />
                        </div>
                    </div>
                </div>
                <div class="adjust-meeting-form-body">
                    <div class="adjust-meeting-form-body-box1">
                        Applies On
                    </div>
                    <div class="adjust-meeting-form-body-box1">
                        When
                    </div>
                    <div class="adjust-meeting-form-body-box1">
                        %</div>
                    <div class="adjust-meeting-form-body-box">
                        <div class="adjust-meeting-form-body-box1left">
                            All</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <asp:RadioButton ID="rbtnAll" runat="server" GroupName="applied" Checked="true" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box1left">
                            Packages</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <asp:RadioButton ID="rbtnDDRs" runat="server" GroupName="applied" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box1left">
                            Meeting Rooms</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <asp:RadioButton ID="rbtnMeetingRoom" runat="server" GroupName="applied" class="NoClassApply" />
                        </div>
                        <%--<asp:Literal ID="ltrMeetingRoomCheckbox" runat="server"></asp:Literal>--%>
                    </div>
                    <div class="adjust-meeting-form-body-box2">
                        <div class="adjust-meeting-form-body-box2left">
                            All day</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkAll" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Sunday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkSunday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Monday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkMonday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Tuesday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkTuesday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Wednesday
                        </div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkWednesday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Thursday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkThursday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Friday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkFriday" runat="server" class="NoClassApply"  />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Saturday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkSaturday" runat="server" class="NoClassApply" />
                        </div>
                    </div>
                    <div class="adjust-meeting-form-body-box3">
                        <div class="adjust-meeting-form-body-box3left" style="width: 100%;">
                            <table cellpadding="0" cellspacing="2">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtAll" runat="server" Text="0" CssClass="textfield" MaxLength="4" 
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                            TargetControlID="txtAll" ValidChars="0123456789-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSunday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                            TargetControlID="txtSunday" ValidChars="0123456789-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtMonday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                            TargetControlID="txtMonday" ValidChars="0123456789-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtTuesday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" 
                                            TargetControlID="txtTuesday" ValidChars="0123456789-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtWednesday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" 
                                            TargetControlID="txtWednesday" ValidChars="0123456789-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtThursday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" 
                                            TargetControlID="txtThursday" ValidChars="0123456789-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtFriday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7"
                                            TargetControlID="txtFriday" ValidChars="0123456789-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSaturday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8"
                                            TargetControlID="txtSaturday" ValidChars="0123456789-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="adjust-meeting-btn">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="ancSubmit" runat="server" CssClass="select" OnClick="ancSubmit_Click">Save</asp:LinkButton>
                        
                        &nbsp;&nbsp; or &nbsp;&nbsp;<a title="Close" class="cancelbtn" href="javascript:;" onclick="document.getElementsByTagName('html')[0].style.overflow='auto';document.getElementById('adjust-meeting').style.display='none';document.getElementById('adjust-meeting-overlay').style.display='none';">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom1">
        </div>
    </div>

    <!-- For manage Bedroom this div is open-->
    <div id="adjust-bedroom">
        <div class="popup-top1">
        </div>
        <div class="popup-mid1">
            <div class="popup-mid-inner1">
                <div class="adjust-meeting-form">
                    <div class="adjust-meeting-date">
                        <div class="error">
                        </div>
                        <div class="adjust-meeting-date-box1">
                            From
                        </div>
                        <div class="adjust-meeting-date-box">
                            <asp:TextBox ID="txtBRFrom" runat="server" value="dd/mm/yy" class="inputbox1"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtBRFrom"
                                PopupButtonID="calBrFrom" Format="dd/MM/yy">
                            </asp:CalendarExtender>
                            
                        </div>
                        <div class="adjust-meeting-date-box2">
                            <input type="image" src="../Images/cal-2.png" id="calBrFrom" />
                        </div>
                        <div class="adjust-meeting-date-box1">
                            To
                        </div>
                        <div class="adjust-meeting-date-box">
                            <asp:TextBox ID="txtBRTo" runat="server" value="dd/mm/yy" class="inputbox1"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtBRTo"
                                PopupButtonID="calBRTo" Format="dd/MM/yy">
                            </asp:CalendarExtender>
                            
                        </div>
                        <div class="adjust-meeting-date-box2">
                            <input type="image" src="../Images/cal-2.png" id="calBRTo" />
                        </div>
                    </div>
                </div>
                <div class="adjust-meeting-form-body">
                    <div class="adjust-meeting-form-body-box1">
                        Applies On
                    </div>
                    <div class="adjust-meeting-form-body-box1">
                        When
                    </div>
                    <div class="adjust-meeting-form-body-box1">
                        Price (in Euro)</div>
                    <div class="adjust-meeting-form-body-box" style="max-height:200px;overflow:auto;" >
                        <div class="adjust-meeting-form-body-box1left">
                            All</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <asp:RadioButton ID="rbtnBRAll" runat="server" GroupName="applied" Checked="true" class="NoClassApply" />
                        </div>
                        <% if (Bedroom1Available > 0)
                           { %>
                        <div class="adjust-meeting-form-body-box1left">
                            <%=Bedroom1Type %> Single</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <asp:RadioButton ID="rbtnBrBedroom1" runat="server" GroupName="applied" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box1left">
                            <%=Bedroom1Type %> Double</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <asp:RadioButton ID="rbtnBrBedroom1Double" runat="server" GroupName="applied" class="NoClassApply" />
                        </div>
                        <%} %>
                        <% if (Bedroom2Available > 0){ %>
                        <div class="adjust-meeting-form-body-box1left">
                            <%=Bedroom2Type %> Single</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <asp:RadioButton ID="rbtnBrBedroom2" runat="server" GroupName="applied" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box1left">
                            <%=Bedroom2Type %> Double</div>
                        <div class="adjust-meeting-form-body-box1right">
                            <asp:RadioButton ID="rbtnBrBedroom2Double" runat="server" GroupName="applied" class="NoClassApply" />
                        </div>
                        <%} %>
                    </div>
                    <div class="adjust-meeting-form-body-box2">
                        <div class="adjust-meeting-form-body-box2left">
                            All day</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkBRAll" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Sunday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkBRSunday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Monday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkBRMonday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Tuesday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkBRTuesday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Wednesday
                        </div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkBRWednesday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Thursday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkBRThursday" runat="server" class="NoClassApply" />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Friday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkBRFriday" runat="server" class="NoClassApply"  />
                        </div>
                        <div class="adjust-meeting-form-body-box2left">
                            Saturday</div>
                        <div class="adjust-meeting-form-body-box2right">
                            <asp:CheckBox ID="chkBRSaturday" runat="server" class="NoClassApply" />
                        </div>
                    </div>
                    <div class="adjust-meeting-form-body-box3">
                        <div class="adjust-meeting-form-body-box3left" style="width: 100%;">
                            <table cellpadding="0" cellspacing="2">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBRALL" runat="server" Text="0" CssClass="textfield" MaxLength="4" 
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" 
                                            TargetControlID="txtBRALL" ValidChars="0123456789." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBRSunday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" 
                                            TargetControlID="txtBRSunday" ValidChars="0123456789." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBRMonday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" 
                                            TargetControlID="txtBRMonday" ValidChars="0123456789." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBRTuesday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" 
                                            TargetControlID="txtBRTuesday" ValidChars="0123456789." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBRWednesday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" 
                                            TargetControlID="txtBRWednesday" ValidChars="0123456789." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBRThursday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" 
                                            TargetControlID="txtBRThursday" ValidChars="0123456789." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBRFriday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15"
                                            TargetControlID="txtBRFriday" ValidChars="0123456789." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBRSaturday" runat="server" Text="0" CssClass="textfield" MaxLength="4"
                                            TabIndex="1"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16"
                                            TargetControlID="txtBRSaturday" ValidChars="0123456789." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="adjust-meeting-btn">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="ancBRSubmit" runat="server" CssClass="select" OnClick="ancBRSubmit_Click">Save</asp:LinkButton>
                        
                        &nbsp;&nbsp; or &nbsp;&nbsp;<a title="Close" class="cancelbtn" href="javascript:;" onclick="document.getElementsByTagName('html')[0].style.overflow='auto';document.getElementById('adjust-bedroom').style.display='none';document.getElementById('adjust-meeting-overlay').style.display='none';">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom1">
        </div>
    </div>
    <div id="Loding_overlay">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />Loading...</div>
        <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />Saving...</div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(".new_right").SalesPromo({
            HotelId:<%= Session["CurrentHotelID"] %>,
            MonthYearDayDate: <%= MonthYearDayDate %>,
            PackageDivNames: <%= PackageDivNames %>,
            MeetingRoomDivNames: <%= MeetingRoomDivNames %>,
            BedroomDivNames: <%= BedroomDivs %>,
            StartSalesandPromo:<%= StartSalesandPromo %>,
            EndSalesandPromo: <%= EndSalesandPromo %>,
            Webservicepath: <%= ServiceUrl %>,
            CurrentPanel: <%=CurrentPanel %>
            });
        });
    </script>
</asp:Content>
