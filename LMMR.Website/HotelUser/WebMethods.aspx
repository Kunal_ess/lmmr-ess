﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true"
    CodeFile="WebMethods.aspx.cs" Inherits="HotelUser_WebMethods" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <asp:ListView ID="lstViewFoodBeveragesPriceEdit" runat="server" ItemPlaceholderID="itemPlacehoderID"
        AutomaticGenerateColumns="false" DataKeyNames="Id">
        <LayoutTemplate>
            <div class="meeting-description-list">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 15px;">
                    <tr>
                        <td bgcolor="#98bcd6">
                            <table width="100%" border="0" cellspacing="1" cellpadding="5">
                                <tr bgcolor="#c4d6e2">
                                    <td>
                                        Food Beverages
                                    </td>
                                    <td>
                                        Vat%
                                    </td>
                                    <td>
                                        Description
                                    </td>
                                    <td>
                                        Price 1/2 day
                                    </td>
                                    <td>
                                        Price Full day
                                    </td>
                                    <td>
                                        Online
                                    </td>
                                </tr>
                                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <tr bgcolor="#d8eefc" class=" row">
                <td>
                    <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblVat" runat="server" Text='<%#Eval("VAT")%>'></asp:Label>%
                </td>
                <td>
                    <asp:Label ID="lblDesc" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtHalfDayPrice" runat="server" class="inputbox"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="lblfullDayPrice" runat="server" class="inputbox"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkIsOnline" runat="server"></asp:CheckBox>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr bgcolor="#edf8fe" class=" row">
                <td>
                    <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("ItemName")%>'></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblVat" runat="server" Text='<%#Eval("VAT")%>'></asp:Label>%
                </td>
                <td>
                    <asp:Label ID="lblDesc" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtHalfDayPrice" runat="server" class="inputbox"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="lblfullDayPrice" runat="server" class="inputbox"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkIsOnline" runat="server"></asp:CheckBox>
                </td>
            </tr>
        </AlternatingItemTemplate>
    </asp:ListView>
</asp:Content>
