﻿#region Using
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using LMMR.Business;
using LMMR.Entities;
using System.Web.UI.HtmlControls;
using System.Xml;
using LMMR;
using LMMR.Data;
using log4net;
using log4net.Config;
#endregion


public partial class HotelMain : System.Web.UI.MasterPage
{
    #region Variables and Properties
    public int getValue = 0;
    public int intHotelID = 0;
    public bool boolGoOnlineStatus = false;
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelMain));
    public ScriptManager MasterPageScriptManager
    {
        get
        {
            return scpmgr;
        }
    }

    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    public string TimeoutTime
    {
        get
        {
            return System.Web.Security.FormsAuthentication.Timeout.Minutes.ToString();
        }
    }
    HttpContext CurrContext = HttpContext.Current;
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    HotelManager objHotel = new HotelManager();
    ViewBooking_Hotel objviewbooking = new ViewBooking_Hotel();
    #endregion

    #region Page Load
    /// <summary>
    /// Set all the Initial values of controls at page load.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentUserID"] == null || Session["CurrentHotelID"] == null || Session["CurrentUser"] == null)
        {
            //Response.Redirect("~/login.aspx");
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        else
        {
            Users objUsers = (Users)Session["CurrentUser"];
            if (objUsers.Usertype != (int)Usertype.HotelUser && objUsers.Usertype != (int)Usertype.Operator && objUsers.Usertype != (int)Usertype.SuperMan && objUsers.Usertype != (int)Usertype.HotelGroupUser && objUsers.Usertype != (int)Usertype.Superadmin)
            {
                Session.Remove("CurrentUser");
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            else
            {
                lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;// +" | UserID=" + objUsers.UserId + " | HotelId=" + Convert.ToString(Session["CurrentHotelID"]);
                objUsers.LastLogin = DateTime.Now;
                lstLoginTime.Text = DateTime.Now.ToLongDateString();
            }
            intHotelID = Convert.ToInt32(Session["CurrentHotelID"] == null ? 0 : Session["CurrentHotelID"]);
            ObjWizardManager.hotelid = intHotelID;
            Hotel objHotel = DataRepository.HotelProvider.GetById(intHotelID);
            boolGoOnlineStatus = Convert.ToBoolean(objHotel.GoOnline);

            //check for hide login details for Operator console
            if (Convert.ToString(Session["Operator"]) == "1")
            {
                divLogin.Visible = false;
            }

            if (!boolGoOnlineStatus)
            {
                if (Convert.ToString(Session["LinkID"]) != "lnkNews" && Convert.ToString(Session["LinkID"]) != "lnkGdt" && Convert.ToString(Session["LinkID"]) != "lnkchangePassword")
                {
                    if (Session["LinkID"] != null)
                    {


                        if (Session["LinkID"].ToString() == "lnkPropertyLevel")
                        {
                            DisableLeftMenuPropertySection();
                            SelectLeftMenu();


                            string getTopLinkId = Session["LinkID"].ToString();
                            LinkButton TopMenuLink = (LinkButton)Page.Master.FindControl(getTopLinkId);
                            TopMenuLink.CssClass = "select";
                        }
                        else
                        {
                            divLeftMenuPL.Visible = false;
                            divLeftMenuALL.Visible = true;
                            divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu(Session["LinkID"].ToString(), intHotelID);
                        }

                    }
                    else
                    {
                        Session["LinkID"] = "lnkPropertyLevel";
                        DisableLeftMenuPropertySection();
                        SelectLeftMenu();
                        string getTopLinkId = Session["LinkID"].ToString();
                        LinkButton TopMenuLink = (LinkButton)Page.Master.FindControl(getTopLinkId);
                        TopMenuLink.CssClass = "select";
                    }
                }
                else
                {
                    divLeftMenuPL.Visible = false;
                    divLeftMenuALL.Visible = true;
                    divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu(Session["LinkID"].ToString(), intHotelID);
                }
                DisableTopMenu(intHotelID);
            }
            else
            {
                if (Session["LinkID"] != null)
                {
                    if (Session["LinkID"].ToString() != "lnkNews" && Session["LinkID"].ToString() != "lnkGdt" && Session["LinkID"].ToString() != "lnkchangePassword")
                    {
                        string getTopLinkId = Session["LinkID"].ToString();
                        LinkButton TopMenuLink = (LinkButton)Page.Master.FindControl(getTopLinkId);
                        TopMenuLink.CssClass = "select";

                        if (Session["LinkID"].ToString() != "lnkPropertyLevel")
                        {
                            divLeftMenuPL.Visible = false;
                            divLeftMenuALL.Visible = true;
                            divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu(Session["LinkID"].ToString(), intHotelID);
                            SelectLeftMenu();
                        }
                        else
                        {
                            divLeftMenuPL.Visible = true;
                            divLeftMenuALL.Visible = false;
                            liGoOnlineLink.Visible = false;

                        }
                    }
                    else
                    {
                        divLeftMenuPL.Visible = false;
                        divLeftMenuALL.Visible = true;
                        divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu(Session["LinkID"].ToString(), intHotelID);
                        SelectLeftMenu();
                    }

                }
                else
                {
                    Session["LinkID"] = "lnkPropertyLevel";
                    DisableLeftMenuPropertySection();
                    SelectLeftMenu();
                    string getTopLinkId = Session["LinkID"].ToString();
                    LinkButton TopMenuLink = (LinkButton)Page.Master.FindControl(getTopLinkId);
                    TopMenuLink.CssClass = "select";
                }

            }
        }
    }
    #endregion

    #region methods
    /// <summary>
    /// Select left menu of the page.
    /// </summary>
    public void SelectLeftMenu()
    {
        try
        {
            string currenturl = HttpContext.Current.Request.Url.ToString();

            switch (currenturl.Split('/')[currenturl.Split('/').Length - 1].ToLower())
            {
                case "generalinfo.aspx":
                    lnkConferenceInfo.Attributes.Add("class", "seltd selected");
                    break;
                case "contactdetails.aspx":
                    lnkContactDetails.Attributes.Add("class", "seltd selected");
                    break;
                case "facilities.aspx":
                    lnkFacilities.Attributes.Add("class", "seltd selected");
                    break;
                case "cancellationpolicy.aspx":
                    lnkCancelationPolicy.Attributes.Add("class", "seltd selected");
                    break;
                case "meetingroomdesc.aspx":
                    lnkDescriptionMeetingRoom.Attributes.Add("class", "seltd selected");
                    break;
                case "meetingroomconfig.aspx":
                    lnkConfiguration.Attributes.Add("class", "seltd selected");
                    break;
                case "printfactsheet.aspx":
                    lnkPrintFactSheet.Attributes.Add("class", "seltd selected");
                    break;
                case "previewonweb.aspx":
                    lnkPreviewOnWeb.Attributes.Add("class", "seltd selected");
                    break;
                case "bedroomdesc.aspx":
                    lnkDescriptionBedRoom.Attributes.Add("class", "seltd selected");
                    break;
                case "pricingmeetingrooms.aspx":
                    lnkMeetingRooms.Attributes.Add("class", "seltd selected");
                    break;
                case "bedroompricing.aspx":
                    lnkBedrooms.Attributes.Add("class", "seltd selected");
                    break;
                case "picturevideomanagement.aspx":
                    lnkPictureManagement.Attributes.Add("class", "seltd selected");
                    break;
                case "goonline.aspx":
                    lnkGoOnline.Attributes.Add("class", "seltd selected");
                    break;
                default:
                    lnkConferenceInfo.Attributes.Add("class", "seltd selected");
                    break;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    //function to disable top menu.
    void DisableTopMenu(int hotelID)
    {
        try
        {
            if (!Convert.ToBoolean(ObjWizardManager.IsGoOnline(hotelID)))
            {
                lnkAvailability.Enabled = false;
                lnkBookings.Enabled = false;
                lnkFinance.Enabled = false;
                lnkStatistics.Enabled = false;
                lnkPromotions.Enabled = false;
                lnkRequests.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    //function leftmenu of property section.
    void DisableLeftMenuPropertySection()
    {
        divLeftMenuPL.Visible = true;
        divLeftMenuALL.Visible = false;
        TList<LeftMenuLink> ObjList = new TList<LeftMenuLink>();
        ObjList = ObjWizardManager.GetLinkByHotelID(intHotelID);
        if (ObjList.Count > 0)
        {
            int minID = ObjWizardManager.GetMinPropertyLevelSectionID();
            int maxID = ObjWizardManager.GetMaxPropertyLevelSectionID();
            int Index = 0;

            for (Index = minID; Index <= maxID; Index++)
            {
                var IsExist = ObjList.Find(a => a.LeftMenuLinkId == Index);
                if (IsExist != null)
                {
                    ViewState["getValue"] = Convert.ToInt32(IsExist.LeftMenuLinkId);
                    //DisableLink(getValue);


                }

            }
            getValue = Convert.ToInt32(ViewState["getValue"]);
            DisableLink(getValue);
            ChangeLinkColor(getValue);
        }
        else
        {
            getValue = ObjWizardManager.GetMinPropertyLevelSectionID() - 1;
            DisableLink(getValue);
            ChangeLinkColor(getValue);
        }

    }

    //function to change color of enabled link. 
    void ChangeLinkColor(int changeColorLinkID)
    {
        string linkID = Enum.GetName(typeof(PropertyLevelSectionLeftMenu), changeColorLinkID + 1);
        HyperLink anchorControl = (HyperLink)Page.Master.FindControl(linkID);
        anchorControl.Enabled = true;
        anchorControl.Style.Add("color", "red");

    }

    // This Function Use for disable links of dashboard.
    void DisableLink(int lnkValue)
    {
        lnkValue = lnkValue + 1;
        int linkMax = ObjWizardManager.GetMaxPropertyLevelSectionID();
        int linkMin = ObjWizardManager.GetMinPropertyLevelSectionID();
        int i = 0;
        for (i = linkMin; i <= linkMax; i++)
        {
            if (lnkValue < i)
            {
                string linkID = Enum.GetName(typeof(PropertyLevelSectionLeftMenu), i);
                HyperLink anchorControl = (HyperLink)Page.Master.FindControl(linkID);
                anchorControl.Enabled = false;
                anchorControl.Style.Add("color", "gray");
            }

            else
            {
                string linkID = Enum.GetName(typeof(PropertyLevelSectionLeftMenu), i);
                HyperLink anchorControl = (HyperLink)Page.Master.FindControl(linkID);
                anchorControl.Enabled = true;
                anchorControl.Style.Add("color", "#1B7AC2");
            }
        }
    }
    #endregion

    #region Events
    /// <summary>
    /// Link Last meeting room click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkLastMinutMeeting_Click(object sender, EventArgs e)
    {
        Response.Redirect("HotelDashboard.aspx", false);
        return;
    }

    /// <summary>
    /// Link Property level click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkPropertyLevel_Click(object sender, EventArgs e)
    {
        Session["LinkID"] = "lnkPropertyLevel";
        //DisableLeftMenuPropertySection();
        Response.Redirect("GeneralInfo.aspx", false);
        return;
    }

    /// <summary>
    /// Link Availability click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkAvailability_Click(object sender, EventArgs e)
    {
        divLeftMenuPL.Visible = false;
        divLeftMenuALL.Visible = true;
        divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu("lnkAvailability", intHotelID);
        Response.Redirect("ManageAvailability.aspx", false);
        return;
    }

    /// <summary>
    /// Link Booking click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkBookings_Click(object sender, EventArgs e)
    {
        divLeftMenuPL.Visible = false;
        divLeftMenuALL.Visible = true;
        divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu("lnkBookings", intHotelID);
        Response.Redirect("ViewBookings.aspx?Type=1", false);
        return;
    }

    /// <summary>
    /// Link Finance click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkFinance_Click(object sender, EventArgs e)
    {
        divLeftMenuPL.Visible = false;
        divLeftMenuALL.Visible = true;
        divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu("lnkFinance", intHotelID);
        Response.Redirect("FinanceInformation.aspx", false);
        return;
    }

    /// <summary>
    /// link Statistics click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkStatistics_Click(object sender, EventArgs e)
    {
        //divLeftMenuPL.Visible = false;
        //divLeftMenuALL.Visible = true;
        //divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu("lnkPropertyLevel", intHotelID);
        Session["LinkID"] = "lnkPropertyLevel";
        Response.Redirect("Statistics.aspx?type=1", false);
        return;
    }

    /// <summary>
    /// link promotion click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkPromotions_Click(object sender, EventArgs e)
    {
        divLeftMenuPL.Visible = false;
        divLeftMenuALL.Visible = true;
        divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu("lnkPromotions", intHotelID);
        Response.Redirect("SalesAndPromo.aspx", false);
        return;
    }

    /// <summary>
    /// Link request click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkRequests_Click(object sender, EventArgs e)
    {
        divLeftMenuPL.Visible = false;
        divLeftMenuALL.Visible = true;
        divLeftMenuALL.InnerHtml = ObjWizardManager.LoadLeftMenu("lnkRequests", intHotelID);
        Response.Redirect("ViewRequest.aspx?type=1", false);
        return;
    }

    /// <summary>
    /// Link logout click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        logger.Debug("Log out User ID:" + Session["CurrentUserID"]);
        Session.Remove("CurrentUserID");
        Session.Remove("CurrentUser");
        Session.Remove("CurrentHotelID");
        Session.Remove("LinkID");
        Session.Remove("masterInput");
        //Response.Redirect("~/login.aspx");
        Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
        return;
    }
    #endregion
}
