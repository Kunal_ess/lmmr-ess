﻿<%@ Page Title="Hotel User - Go Online" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true"
    CodeFile="GoOnline.aspx.cs" Inherits="HotelUser_GoOnline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <div class="picture-video">
        <div class="heading-body">
            <div class="heading-left">
                <h2>
                    Go Online with the hotel <b>"<asp:Label ID="lblHotel" runat="server" Text=""></asp:Label>"</b></h2>
            </div>
        </div>
        <div id="divmessage" runat="server">
        </div>
        <div id="divPending" runat="server">
        <div class="images-main-body">
            Dear &#8220; <asp:Label ID="lblHotel1" runat="server" Text=""></asp:Label>&#8221;
            <br />
            Your meeting facilitie(s) are now ready to go live. Push the
&#8220;go online&#8221; button and allow 72h in order to verify the 
conformity by our hotel support team. You will receive a mail 
when the hotel is ready to go live. When receiving the mail, 
please update Availability, Pricing / Hotel promo.
            <br />
<br />
Many thanks,
            <br />
            Hotel support team
        </div>
        <div class="picture-video-form-btn">
            <div class="save-cancel-btn6">
                <asp:LinkButton ID="btnBack" runat="server" class="cancel-btn" Text="Cancel" OnClick="btnBack_Click" />
                &nbsp; &nbsp;&nbsp;
                <asp:LinkButton ID="btnConfirm" class="goonline" runat="server" Text="Go Online"
                    OnClick="btnConfirm_Click" />
            </div>
        </div>
        </div>
         <div class="picture-video-form-btn" id="divGoonline" runat="server" visible="false">
            <div class="save-cancel-btn6">                
                &nbsp; &nbsp;&nbsp;
                <asp:LinkButton ID="btnGoonline" class="approvegoonline" runat="server" Text="Approve Go Online"
                    OnClick="btnConfirm_Click" />
            </div>
        </div>
        <div id="divcompleted" runat="server">
        <div class="images-main-body">
     Your request to go online is currently in process &#8211;&nbsp; if
You have any questions please contact our hotel support
Team at <a href="mailto:hotelsupport@lastminutemeetingroom.com " >hotelsupport@lastminutemeetingroom.com </a>or call
+32 2 344 25 50
 </div>
        </div>
    </div>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />Saving...</div>
        <script language="javascript" type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#<%= btnConfirm.ClientID %>").bind("click", function () {
                    jQuery("#Loding_overlaySec").show();
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                });
            });
        </script>
    </a>
</asp:Content>
