﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftHotelDetails.ascx.cs"
    Inherits="HotelUser_usercontrols_LeftHotelDetails" %>
<div class="hotel_logo">
    <asp:Image ID="imgHotelLogo" runat="server" CssClass="imagelogo" Height="168px" Width="168px" />
</div>
<div class="search">
    <p>
        See dashboard for other hotels:</p>
    <!-- start drop down search -->
    <div class="rowElem">
        <asp:DropDownList ID="drpHotelList" runat="server" Width="180" AutoPostBack="True" CssClass="NoClassApply"
            OnSelectedIndexChanged="drpHotelList_SelectedIndexChanged">
        </asp:DropDownList>
    </div>
    <!-- end drop down search -->
</div>
