﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
#endregion

public partial class HotelUser_Facilities : System.Web.UI.Page
{
    #region variable declaration
    Facilities objFaclt = new Facilities();
    int hotelPid=0;
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    DashboardManager Objdash = new DashboardManager();
    HotelManager objHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_Facilities));
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session for Hotelname
            if (Session["CurrentHotelID"] == null)
            {
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            hotelPid = Convert.ToInt32(Session["CurrentHotelID"]);
            //Session for Hotelnames

            if (!IsPostBack)
            {
                                            #region Leftmenu
            HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
            lnk.CssClass = "";
            lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
            lnk.CssClass = "";
            lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
            lnk.CssClass = "selected";
            #endregion

                //For hide div message
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");
                //if (Session["LinkID"] != null)
                //{
                Session["LinkID"] = "lnkPropertyLevel";
                //}
                //Bind all facility types

                //Call function to bind all selected facilities
                BindAllStaticFacilities();

                //Call function for Goonline
                Getgoonline();

            }

            //Display Hotelname on header of the page
            lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Additional Methods
    /// <summary>
    /// For check go online for this hotel
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(hotelPid).Count > 0)
        {
            divNext.Visible = false;
            ViewState["Count"] = ObjHotelinfo.GetHotelGoOnline(hotelPid).Count;  
        }
    }

    /// <summary>
    //Bind all facilities by FacilityTypeId
    /// </summary>   
    public void BindAllStaticFacilities()
    {
        try
        {
            chkGeneral.DataValueField = "Id";
            chkGeneral.DataTextField = "FacilityName";
            chkGeneral.DataSource = objFaclt.GetFacilityGnl();
            chkGeneral.DataBind();
            for (int i = 0; i < chkGeneral.Items.Count; i++)
            {
                TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkGeneral.Items[i].Value), hotelPid);
                if (htlfac.Count > 0)
                {
                    chkGeneral.Items[i].Selected = true;
                }

            }
            chkActivities.DataValueField = "Id";
            chkActivities.DataTextField = "FacilityName";
            chkActivities.DataSource = objFaclt.GetFacilityAct();
            chkActivities.DataBind();
            for (int i = 0; i < chkActivities.Items.Count; i++)
            {
                TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkActivities.Items[i].Value), hotelPid);
                if (htlfac.Count > 0)
                {
                    chkActivities.Items[i].Selected = true;
                }

            }

            chkServices.DataValueField = "Id";
            chkServices.DataTextField = "FacilityName";
            chkServices.DataSource = objFaclt.GetFacilitySer();
            chkServices.DataBind();
            for (int i = 0; i < chkServices.Items.Count; i++)
            {
                TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkServices.Items[i].Value), hotelPid);
                if (htlfac.Count > 0)
                {
                    chkServices.Items[i].Selected = true;
                }
            }


            chkMeeting.DataValueField = "Id";
            chkMeeting.DataTextField = "FacilityName";
            chkMeeting.DataSource = objFaclt.GetFacilityMeeting();
            chkMeeting.DataBind();
            for (int i = 0; i < chkMeeting.Items.Count; i++)
            {
                TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkMeeting.Items[i].Value), hotelPid);
                if (htlfac.Count > 0)
                {
                    chkMeeting.Items[i].Selected = true;
                }
            }


            chkBedrooms.DataValueField = "Id";
            chkBedrooms.DataTextField = "FacilityName";
            chkBedrooms.DataSource = objFaclt.GetFacilityBedroom();
            chkBedrooms.DataBind();
            for (int i = 0; i < chkBedrooms.Items.Count; i++)
            {
                TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkBedrooms.Items[i].Value), hotelPid);
                if (htlfac.Count > 0)
                {
                    chkBedrooms.Items[i].Selected = true;
                }
            }

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    /// <summary>
    //Function for CheckBox Clear for Cancel button
    /// </summary>    
    public void ClearChkList()
    {
        chkGeneral.SelectedIndex = -1;
        chkActivities.SelectedIndex = -1;
        chkServices.SelectedIndex = -1;
        chkMeeting.SelectedIndex = -1;
        chkBedrooms.SelectedIndex = -1;
    }

    #endregion    

    #region Insert Facilities
    /// <summary>
    //Insert all selected facilities by hotel id
    /// </summary>   
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //Update changes on facilities
            objFaclt.DeleteFacilities(hotelPid);
                
            for (int i = 0; i < chkGeneral.Items.Count; i++)
            {
                if (chkGeneral.Items[i].Selected)
                {
                    objFaclt.AddNewFacilities(hotelPid, Convert.ToInt32(chkGeneral.Items[i].Value));
                }
            }
            for (int i = 0; i < chkActivities.Items.Count; i++)
            {
                if (chkActivities.Items[i].Selected)
                {
                    objFaclt.AddNewFacilities(hotelPid, Convert.ToInt32(chkActivities.Items[i].Value));
                }
            }
            for (int i = 0; i < chkServices.Items.Count; i++)
            {
                if (chkServices.Items[i].Selected)
                {
                    objFaclt.AddNewFacilities(hotelPid, Convert.ToInt32(chkServices.Items[i].Value));
                }
            }
            for (int i = 0; i < chkMeeting.Items.Count; i++)
            {
                if (chkMeeting.Items[i].Selected)
                {
                    objFaclt.AddNewFacilities(hotelPid, Convert.ToInt32(chkMeeting.Items[i].Value));
                }
            }
            for (int i = 0; i < chkBedrooms.Items.Count; i++)
            {
                if (chkBedrooms.Items[i].Selected)
                {
                    objFaclt.AddNewFacilities(hotelPid, Convert.ToInt32(chkBedrooms.Items[i].Value));
                }
            }
            //lblMessage.Text= "Facilities have been successfully submitted";
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "Facilities updated successfully!";
            if (Convert.ToString(ViewState["Count"]) != "1")
            {
                divmessage.InnerHtml += "&nbsp; Please click on next button to move ahead.";
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Reset Information
    /// <summary>
    //Call function for clearing checkboxlist
    /// </summary>    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearChkList();
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
    }
    #endregion

    #region Move to Next page
    /// <summary>
    //Function for moving next page
    /// </summary>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        //Objwizard.ThisPageIsDone(hotelPid,        
        Objwizard.ThisPageIsDone(hotelPid, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkFacilities));
        Response.Redirect("CancellationPolicy.aspx");
    }
    #endregion
}