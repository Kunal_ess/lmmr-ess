﻿<%@ Page Title="Hotel User - Request" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="ViewRequest.aspx.cs" Inherits="HotelUser_ViewBookings"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControl/HotelUser/RequestDetails.ascx" TagName="requestDetails1" TagPrefix="uc1" %>
<%@ Register src="../UserControl/HotelUser/BookingDetails.ascx" tagname="BookingDetails" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        <asp:Label ID="lblheadertext" runat="server" Text="New Requests for:"></asp:Label></h1>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
        .style1
        {
            height: 84px;
        }
    </style>
    <link href="../css/style234.css" rel="stylesheet" media="all" type="text/css" />
    <div class="search-bookings-date" id="divsearch" runat="server" align="left">
        <div class="search-bookings-date-box1">
            From
        </div>
        <div class="search-bookings-date-box">
            <asp:TextBox ID="txtFromdate" runat="server" CssClass="inputbox1"></asp:TextBox>
            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
        <div class="search-bookings-date-box1">
            To
        </div>
        <div class="search-bookings-date-box">
            <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox1"></asp:TextBox>
            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
        <div>
            <asp:LinkButton ID="lbtSearch" runat="server" Visible="false" ForeColor="White" OnClick="lbtSearch_Click"></asp:LinkButton>
        </div>
    </div>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updTest">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="booking-details">
        <asp:UpdatePanel runat="server" ID="updTest">
            <ContentTemplate>
                <ul>
                    <li class="value14">
                        <div id="divwarning" runat="server" visible="false" class="col14_new">
                            <b>Warning!</b> You have meeting dates in less than 48h</div>
                    </li>
                </ul>
                <ul class="message">
                    <li>
                        <div id="divGrid">
                            <asp:GridView ID="grdViewBooking" runat="server" Width="100%" AutoGenerateColumns="False"
                                RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" CellPadding="6"
                                OnRowDataBound="grdViewBooking_RowDataBound" OnPageIndexChanging="grdViewBooking_PageIndexChanging"
                                EmptyDataText="No record Found!" EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                EditRowStyle-VerticalAlign="Top" AllowPaging="true" PageSize="10" GridLines="None"
                                CssClass="cofig" BackColor="#ffffff" OnRowCreated="grdViewBooking_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Ref No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblRefNo" runat="server" ToolTip='<%# Eval("HotelID") %>' Text='<%# Eval("Id") %>'
                                                CommandArgument='<%# ((GridViewRow) Container).RowIndex %>' OnClick="lblRefNo_Click"></asp:LinkButton>
                                        </ItemTemplate>

                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meeting Room">
                                        <ItemTemplate>
                                            <asp:Label ID="lblmeetingroom" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBookingDt" runat="server" ToolTip='<%# Eval("BookingDate") %>'
                                                Text='<%# Eval("BookingDate","{0:dd/MM/yy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Channel">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Usertype ") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Eval("TypeUser ") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblConctName" runat="server" Text='<%# Eval("Contact ") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Arrival Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblArrivalDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}", Eval("ArrivalDate"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Departure Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartureDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("DepartureDate"))%>'
                                                ToolTip='<%# Eval("DepartureDate")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Expiry Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblExpiryDt" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFinalTotal" runat="server" Text='<%# String.Format("{0:###,###,###}",Eval("Finaltotalprice")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <HeaderTemplate>
                                            <asp:DropDownList ID="ddlstatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstatus_SelectedIndexChanged"
                                                CssClass="NoClassApply">
                                                <asp:ListItem Value="-1">All</asp:ListItem>
                                                <asp:ListItem Value="5">Tentative</asp:ListItem>
                                                <asp:ListItem Value="7">Definitive</asp:ListItem>
                                                <asp:ListItem Value="2">Cancel</asp:ListItem>
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" ToolTip='<%# Eval("requeststatus") %>'></asp:Label>
                                            <br />
                                             <asp:LinkButton ID="btnchkcommision1" runat="server" Text="Change Status" CommandArgument='<%# Eval("Id") %>'   ToolTip='<%# Eval("IsComissionDone") %>'
                                                OnClick="btnchkcommision_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commission">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnchkcommision" runat="server" Text="Check Commission" CommandArgument='<%# Eval("Id") %>'   ToolTip='<%# Eval("IsComissionDone") %>'
                                                OnClick="btnchkcommision_Click"></asp:LinkButton>
                                                 <asp:Image ID="imgCheck" runat="server" ImageUrl="~/Images/select-check-bx.png" />
                                                </td>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" BackColor="White" />
                                <PagerTemplate>
                                    <table cellpadding="0" border="0" align="right">
                                        <tr>
                                            <td style="vertical-align=middle; height: 22px;">
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <HeaderStyle BackColor="#CCD8D8" />
                                <PagerSettings Mode="NumericFirstLast" Position="Top" />
                                <PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                                    Font-Strikeout="False" />
                                <RowStyle BackColor="#E3F0F1" />
                                <AlternatingRowStyle BackColor="#ffffff" />
                            </asp:GridView>
                        </div>
                    </li>
                </ul>
                <div class="booking-details" id="divprintpdfllink" runat="server">
                    <ul>
                        <li class="value3">
                            <div class="col9">
                                <img src="../Images/print.png" />&nbsp; <a id="uiLinkButtonPrintGrid" style="cursor: pointer;"
                                    onclick="javascript:Button1_onclick('divGrid');">Print</a> &bull;
                                <img src="../Images/pdf.png" />&nbsp;<asp:LinkButton ID="uiLinkButtonSaveAsPdfGrid"
                                    runat="server" OnClick="uiLinkButtonSaveAsPdfGrid_Click">Save as PDF</asp:LinkButton>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="uiLinkButtonSaveAsPdfGrid" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div id="divbookingdetails" runat="server" visible="false">
                <h1 class="new">
                    Request details</h1>
                <br />
                <div class="booking-details" id="divprint" runat="server">
                    <ul>
                        <li >
                            <div id="divwarningdetails" runat="server" class="col14" style="background-color:#ff9933" >
                              
                                <asp:Label ID="lblwarning" runat="server"  Font-Bold ="true" Text="Please note that you have 48 hours to send an offer or decline"></asp:Label></div>
                        </li>
                    </ul>
                    <ul>
                        <li class="value3">
                            <div class="col9">
                                <img src="../Images/print.png" />&nbsp; <a id="ADetails" style="cursor: pointer;"
                                    onclick="javascript:Button1_onclick('<%= Divdetails.ClientID%>');">Print</a>
                                &bull;
                                <img src="../Images/pdf.png" />&nbsp;<asp:LinkButton ID="lnkSavePDF" runat="server"
                                    OnClick="lnkSavePDF_Click">Save as PDF</asp:LinkButton>
                            </div>
                        </li>
                    </ul>
                </div>
                <div id="Divdetails" runat="server">
                    <uc1:requestDetails1 ID="requestDetails1" runat="server">
                    </uc1:requestDetails1>
                    <uc2:BookingDetails ID="BookingDetails1" runat="server" />
                </div>
                <br />
                 <table width="99%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="8">
                        <tr bgcolor="#E3F0F1">
                            <td>
                                <img src="../Images/help.jpg" />&nbsp;&nbsp;Red elements have no pricing in your
                                venue backend system. In order to also present them online, fill out pricing on
                                venue backend system
                            </td>
                        </tr>
                    </table>
                <div class="col9">
                    <div class="booking-details" style="width: 720px;" id="divbutton" runat="server">
                        <div style="width: 300px; margin: 20px auto">
                            <table width="100%">
                                <tr>
                                    <td align="right" width="25%">
                                        <div>
                                            <div class="n-btn5">
                                                <asp:LinkButton ID="Lnkmovetoprocessed" runat="server" OnClick="Lnkmovetoprocessed_Click">
                        <div class="n-btn-left"></div>                    
                                     
                     <div  class="n-btn-mid">Send an Offer</div>                    
                     <div class="n-btn-right"></div>
                                      </asp:LinkButton>
                                            </div>
                                        </div>
                                    </td>
                                    <td align="center" width="10%">
                                        <span>or</span>
                                    </td>
                                    <td align="left" width="45%">
                                        <asp:LinkButton ID="lnkdecline" runat="server" Text="Decline" OnClick="lnkdecline_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkSavePDF" />
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <div>
        <asp:LinkButton ID="lnkbtn" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="modalcheckcomm" TargetControlID="lnkbtn" BackgroundCssClass="modalBackground"
            PopupControlID="pnlchkCommission" runat="server">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlchkCommission" BorderColor="#999999" Width="757px" BorderWidth="5"
            Style="display: none; padding-top: 7px;" runat="server" BackColor="White">
            <div class="popup-mid-inner-body" align="center">
               Revenue validation for commission
            </div>
            <div id="divmessage" runat="server">
            </div>
            <div class="popup-mid-inner-body1">
                <asp:UpdatePanel ID="upcheckcomm" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellspacing="0" cellpadding="3" align="center">
                            <tr>
                                <td class="style1">
                                    <table width="100%">
                                        <tr>
                                            <td align="center" colspan="2">
                                                <b>Total Revenue</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Insert Netto (VAT excluded) :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtrealvalue" runat="server" MaxLength="7"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderrealvalue" runat="server" TargetControlID="txtrealvalue"
                                                    ValidChars="." FilterMode="ValidChars" FilterType="Numbers,Custom">
                                                </asp:FilteredTextBoxExtender>
                                                
                                            </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td align="right">
                                                Reason :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlreason" runat="server" CssClass="NoClassApply">
                                                    <asp:ListItem>Increase number of delegates</asp:ListItem>
                                                    <asp:ListItem>Decrease number of delegates</asp:ListItem>
                                                    <asp:ListItem>Changes in package</asp:ListItem>
                                                    <asp:ListItem>Changes in F&B</asp:ListItem>
                                                    <asp:ListItem>Changes in Duration</asp:ListItem>
                                                    <asp:ListItem>Changes in room nights</asp:ListItem>
                                                    <asp:ListItem>No-Show</asp:ListItem>
                                                    <asp:ListItem>Cancelled</asp:ListItem>
                                                </asp:DropDownList>
                                                <%--<asp:TextBox ID="txtreason" runat="server"></asp:TextBox>--%>
                                                <%-- <asp:RequiredFieldValidator
                            ID="reqreason" runat="server" ControlToValidate="ddlreason" ValidationGroup="popup" InitialValue="0" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td align="right" valign="top">
                                            Supporting document :
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="ulPlan" runat="server" />&nbsp;<br />*
                                             <span style="font-family: Arial; font-size: Smaller; color: Gray">The supporting document is mandatory to process commission</span>
                                            
<br />
                                            <span style="font-family: Arial; font-size: Smaller; color: Gray">eg: Invoice, File format: PDF/Word/Excel & max size 1MB. </span>
                                        </td>
                                    </tr>  
                                        <tr valign="bottom" >
                                <td align="left"  valign="bottom">
                                <br />
                                    <asp:CheckBox ID="chkmeetingNotheld" runat="server" CssClass="NoClassApply" onclick="callme()"
                                        Text="The meeting was not held"  />
                                </td><td></td>
                            </tr>                 
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div>
                <div class="popup-mid-inner-body2_i">
                </div>
                <div class="booking-details" style="width: 760px;">
                    <ul>
                        <li class="value10">
                            <div class="col21" style="width: 752px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="btnSubmit" runat="server" Text="Save"  CssClass="select" OnClick="btnSubmit_Click" />
                                    <span>or</span>
                                    <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:LinkButton ID="lnkcheck" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupCheck" TargetControlID="lnkcheck" BackgroundCssClass="modalBackground"
            PopupControlID="pnlcheck" runat="server">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlcheck" BorderColor="Black" Width="800px" BorderWidth="1" Style="display: none;"
            runat="server" BackColor="White">
            <div class="popup-mid-inner-body" align="center">
            </div>
            <div class="popup-mid-inner-body1">
                <table width="100%" cellspacing="0" cellpadding="3" align="center">
                    <tr>
                        <td colspan="2">
                            <div class="warning">
                               Please change to DEFINITE when client has confirmed the request. Afterwards, you will be able to
adjust and mark the net revenue for the confirmed request. If the request did not materialise, 
please turn to CANCEL.
</div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlstatus" Width="200px" runat="server">
                                
                                <asp:ListItem Value="7">Definite</asp:ListItem>
                                <asp:ListItem Value="2">Cancel</asp:ListItem>
                                <asp:ListItem Value="5">Tentative</asp:ListItem>
                                  
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <div class="popup-mid-inner-body2_i">
                </div>
                <div class="booking-details" style="width: 800px;" id="div5">
                    <ul>
                        <li class="value10">
                            <div class="col21" style="width: 795px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="lnkchecksubmit" runat="server" Text="Save" CssClass="select"
                                        OnClick="btnSubmit_Click" />
                                    <span>or</span>
                                    <asp:LinkButton ID="lnkcheckcancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </asp:Panel>
    </div>
    <script language="javascript" type="text/javascript">

        function Button1_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }


        function callme() {
            if (jQuery("#contentbody_chkmeetingNotheld").is(":checked")) {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", true);
                jQuery("#contentbody_ddlreason").attr("disabled", true);
                jQuery("#contentbody_ulPlan").val('');
                jQuery("#contentbody_ulPlan").attr("disabled", true);

            }
            else {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", false);
                jQuery("#contentbody_ddlreason").attr("disabled", false);
                jQuery("#contentbody_ulPlan").val('');
                jQuery("#contentbody_ulPlan").attr("disabled", false);
            }
        }

        

    </script>
</asp:Content>
