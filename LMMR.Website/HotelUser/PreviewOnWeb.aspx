﻿<%@ Page Title="Hotel User - Preview on Web" Language="C#" AutoEventWireup="true"
    CodeFile="PreviewOnWeb.aspx.cs" Inherits="HotelUser_PreviewOnWeb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../css/style.css" rel="stylesheet" media="all" type="text/css" />
    <link href="../css/style-front.css" rel="stylesheet" media="all" type="text/css" />
    <script type="text/javascript" src="../js/custom-form-elements.js"></script>
    <script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
    <link href="../css/tutorsty.css" rel="stylesheet" type="text/css" />
    <link href="../css/flexcrollstyles.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src="../js/flexcroll.js"></script>
    <script type="text/javascript" src="../js/mootools.js"></script>
    <script type="text/javascript" src="../js/elSelect.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    
    <script type="text/javascript">
        function LoadMapLatLong() {
          
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: new google.maps.LatLng(document.getElementById("hf_latitude").value, document.getElementById("hf_longtitude").value),
                mapTypeId: google.maps.MapTypeId.ROADMAP

            });

          var marker = new google.maps.Marker({
              position: new google.maps.LatLng(document.getElementById("hf_latitude").value, document.getElementById("hf_longtitude").value),
              map: map
            });

        }
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
</head>
<body onload="LoadMapLatLong()" onunload="UnloadMap()">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hf_address" runat="server" />
    <asp:HiddenField ID="hf_latitude" runat="server" />
    <asp:HiddenField ID="hf_longtitude" runat="server" />
    <div class="wapper-main">
        <!--wapper-inner START HERE-->
        <div class="wapper-inner">
            <div class="wapper">
                <div class="nav-body-big">
                    <div class="nav-big">
                        <!--Main navigation start -->
                        <div id="subnavbar">
                            <ul id="subnav">
                                <li class="cat-item"><a href="#">Home</a></li>
                                <li class="cat-item"><a href="#">About us</a></li>
                                <li class="cat-item"><a href="#">Contact us</a></li>
                                <li class="cat-item"><a href="#">Join today</a></li>
                                <li class="cat-item"><a href="#">Login</a></li>
                            </ul>
                        </div>
                        <!--main navigation end -->
                    </div>
                    <div class="nav-form">
                        <div id="English">
                            <asp:DropDownList ID="drplanguage" runat="server" Width="120px" AutoPostBack="True"
                                OnSelectedIndexChanged="drplanguage_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!--mainbody START HERE-->
                <div class="mainbody-big">
                    <table width="100%" border="0" cellpadding="0">
                        <tr>
                            <td>
                                <!--banner START HERE-->
                                <div class="banner">
                                    <div id="map_action">
                                        <div id="map" style="width: 700px; height: 350px;">
                                        </div>
                                    </div>
                                </div>
                                <!--banner ENDS HERE-->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ul>
                                    <li class="first">
                                        <!--all-results START HERE-->
                                        <div class="all-results-i">
                                            <!--all-results-left START HERE-->
                                            <div class="all-results-left">
                                                <!--all-results-left-left START HERE-->
                                                <div class="all-results-left-left">
                                                    <asp:Image ID="imgHotelMainImage" runat="server" Width="100" Height="100" />
                                                </div>
                                                <!--all-results-left-left START HERE-->
                                                <!--all-results-left-right END HERE-->
                                                <div class="all-results-left-right">
                                                    <div class="star">
                                                        <asp:Image ID="imgStars" runat="server" />
                                                    </div>
                                                    <h3>
                                                        <asp:Label ID="lblHotelName" runat="server" Text=""></asp:Label>
                                                    </h3>
                                                    <h4>
                                                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label></h4>
                                                    <h4>
                                                        <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                                                    </h4>
                                                    <div class="watch">
                                                        <img src="../images/r-video-icon.png" align="absmiddle" />
                                                        <span>Watch Video</span>
                                                    </div>
                                                </div>
                                                <!--all-results-left-right END HERE-->
                                            </div>
                                            <!--all-results-left END HERE-->
                                            <!--all-results-right START HERE-->
                                            <div class="all-results-right">
                                                <div class="results-top">
                                                    <div class="results-review">
                                                        <p>
                                                            Review</p>
                                                        <span>N/A</span>
                                                    </div>
                                                    <div class="results-from-airport">
                                                        <span>
                                                                        <asp:Label ID="lblAirport" runat="server" Text=""></asp:Label></span>
                                                    </div>
                                                    <div class="results-price-dis" style="display: none" id="divDiscount" runat="server">
                                                        <div class="results-price-dis-left">
                                                            <img src="../images/euro-red.png" />
                                                        </div>
                                                         <div class="results-price-dis-right">
                                                                        <span class="euro">€</span> <span><span class="dis-bg">
                                                                            <asp:Label ID="lblActPkgPrice" runat="server" Text=""></asp:Label>                                                                            
                                                                        </span></span><b>
                                                                            p.p</b>
                                                                        <p>
                                                                            <span>€
                                                                                <asp:Label ID="lblDiscountPrice" runat="server" Text=""></asp:Label>
                                                                            </span>
                                                                        </p>                                                                       
                                                                    </div>
                                                    </div>
                                                    <div class="results-price" style="display: none" id="divPkg" runat="server">
                                                                    <span class="euro">€</span> <span>
                                                                        <asp:Label ID="lblActPAckagePrice" runat="server" Text=""></asp:Label></span>
                                                                    <b>
                                                                        p.p</b>
                                                                </div>
                                                </div>
                                                <div class="results-bottom">
                                                    <a href="#" class="book-online-btn">Book online</a> <span>or</span> <a href="#" class="send-request-btn">
                                                        Send a request</a>
                                                </div>
                                            </div>
                                            <!--all-results-right  END HERE-->
                                        </div>
                                        <!--all-results END HERE-->
                                        <!--all-results-bottom START HERE-->
                                        <div class="all-results-bottom">
                                            <div class="all-results-bottom-left">
                                                <div class="all-results-bottom-left-online">
                                                    <span>2</span> online to book
                                                </div>
                                                <div class="all-results-bottom-left-icon">
                                                    <table width="50%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="left" id="img1" runat="server" visible="false">
                                                                <asp:Image ID="imgRestaurant" runat="server" ImageUrl="~/HotelUser/Uploads/FacilityIcon/bar-resto.png"
                                                                    ToolTip="Restaurant" />
                                                            </td>
                                                            <td align="left" id="img2" runat="server" visible="false">
                                                                <asp:Image ID="imgDaylight" runat="server" ImageUrl="~/HotelUser/Uploads/FacilityIcon/daylight-in-all-meeting-rooms.png"
                                                                    ToolTip="All meeting rooms have natural daylight" />
                                                            </td>
                                                            <td align="left" id="img3" runat="server" visible="false">
                                                                <asp:Image ID="imgMeetingroom" runat="server" ImageUrl="~/HotelUser/Uploads/FacilityIcon/meeting.png"
                                                                    ToolTip="Dedicated meeting coordinator" />
                                                            </td>
                                                            <td align="left" id="img4" runat="server" visible="false">
                                                                <asp:Image ID="imgParking" runat="server" ImageUrl="~/HotelUser/Uploads/FacilityIcon/private-parking.png"
                                                                    ToolTip="Free parking" />
                                                            </td>
                                                            <td align="left" id="img5" runat="server" visible="false">
                                                                <asp:Image ID="imgWireless" runat="server" ImageUrl="~/HotelUser/Uploads/FacilityIcon/wifi.png"
                                                                    ToolTip="Complimenteary wireless internet" />
                                                            </td>
                                                             <td align="left" id="img6" runat="server" visible="false">
                                                                <asp:Image ID="imgbedroom" runat="server" ImageUrl="~/HotelUser/Uploads/FacilityIcon/bedroom-icon.png"
                                                                    ToolTip="Bedroom Available" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="all-results-bottom-left-map">
                                                    <a href="#">
                                                        <img src="../images/map-icon.png" align="absmiddle" /></a> Show map
                                                </div>
                                            </div>
                                            <div class="all-results-bottom-right">
                                                <a href="#">See all</a></div>
                                        </div>
                                        <!--all-results-bottom END HERE-->
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="booking-big">
                                    <!--booking-online START HERE-->
                                    <div class="booking-online-big">
                                        <h2>
                                            Book online</h2>
                                        <!--booking-online-body START HERE-->
                                        <div class="booking-online-body-big">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="green-preview">
                                                <asp:ListView ID="lstViewBooking" runat="server" ItemPlaceholderID="itemPlacehoderID"
                                                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewBooking_ItemDataBound">
                                                    <LayoutTemplate>
                                                        <tr>
                                                            <th>
                                                            </th>
                                                            <th>
                                                                Meeting room name
                                                            </th>
                                                            <th bgcolor="#D7E4C8">
                                                                Room Size(meters)
                                                            </th>
                                                            <th>
                                                                Height(Sq m)
                                                            </th>
                                                            <th bgcolor="#D7E4C8">
                                                                <img src="../Images/theatre.gif" alt="" />
                                                            </th>
                                                            <th>
                                                                <img src="../Images/classroom.gif" alt="" />
                                                            </th>
                                                            <th bgcolor="#D7E4C8">
                                                                <img src="../Images/ushape.gif" alt="" />
                                                            </th>
                                                            <th>
                                                                <img src="../Images/boardroom.gif" alt="" />
                                                            </th>
                                                            <th bgcolor="#D7E4C8">
                                                                <img src="../Images/cocktail.gif" alt="" />
                                                            </th>
                                                        </tr>
                                                        <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                                                    </LayoutTemplate>
                                                    <ItemTemplate>
                                                        <!--booking-online-body-inner END HERE-->
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="imgHotelImage" runat="server" Width="57" Height="57" /><br />
                                                                <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank" Text="Floor Plan"
                                                                    ForeColor="#1b7ac2" Font-Italic="true">                                            
                                                                </asp:HyperLink>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                            </td>
                                                            <td bgcolor="#D7E4C8">
                                                                <asp:Label ID="lblRoomSize" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblHeight" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td bgcolor="#D7E4C8" valign="top">
                                                                <p>
                                                                    Theatre Style</p>
                                                                <p>
                                                                    <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                        runat="server" Text=""></asp:Label></p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkTheatre" runat="server" Checked="true" /></p>
                                                            </td>
                                                            <td valign="top">
                                                                <p>
                                                                    School</p>
                                                                <p>
                                                                    <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                        runat="server" Text=""></asp:Label>
                                                                </p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkSchool" runat="server" Checked="true" /></p>
                                                            </td>
                                                            <td bgcolor="#D7E4C8" valign="top">
                                                                <p>
                                                                    U-Shape</p>
                                                                <p>
                                                                    <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblUShapeMax" runat="server" Text=""></asp:Label></p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkUshape" runat="server" Checked="true" /></p>
                                                            </td>
                                                            <td valign="top">
                                                                <p>
                                                                    Boardroom</p>
                                                                <p>
                                                                    <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblBanquetMax" runat="server" Text=""></asp:Label></p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkBoardroom" runat="server" Checked="true" /></p>
                                                            </td>
                                                            <td bgcolor="#D7E4C8" valign="top">
                                                                <p>
                                                                    Cocktail</p>
                                                                <p>
                                                                    <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblCocktailMax" runat="server" Text=""></asp:Label></p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkCocktail" runat="server" Checked="true" /></p>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </table>
                                        </div>
                                        <div class="booking-online-btn-i">
                                            <a href="#" class="add-shopping-cart-btn">Add to shopping cart</a> or <a href="#">Cancel</a>
                                        </div>
                                    </div>
                                    <div class="booking-request-big">
                                        <h2>
                                            Book on request
                                        </h2>
                                        <!--booking-request-body START HERE-->
                                        <div class="booking-request-body-big">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="green-preview">
                                                <asp:ListView ID="lstViewRequest" runat="server" ItemPlaceholderID="itemPlacehoderID"
                                                    AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewRequest_ItemDataBound">
                                                    <LayoutTemplate>
                                                        <tr>
                                                            <th>
                                                            </th>
                                                            <th>
                                                                Meeting room name
                                                            </th>
                                                            <th bgcolor="#D7E4C8">
                                                                Room Size(meters)
                                                            </th>
                                                            <th>
                                                                Height(Sq m)
                                                            </th>
                                                            <th bgcolor="#D7E4C8">
                                                                <img src="../Images/theatre.gif" alt="" />
                                                            </th>
                                                            <th>
                                                                <img src="../Images/classroom.gif" alt="" />
                                                            </th>
                                                            <th bgcolor="#D7E4C8">
                                                                <img src="../Images/ushape.gif" alt="" />
                                                            </th>
                                                            <th>
                                                                <img src="../Images/boardroom.gif" alt="" />
                                                            </th>
                                                            <th bgcolor="#D7E4C8">
                                                                <img src="../Images/cocktail.gif" alt="" />
                                                            </th>
                                                        </tr>
                                                        <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                                                    </LayoutTemplate>
                                                    <ItemTemplate>
                                                        <!--booking-online-body-inner END HERE-->
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="imgHotelImage" runat="server" Width="57" Height="57" /><br />
                                                                <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank" Text="Floor Plan"
                                                                    ForeColor="#1b7ac2" Font-Italic="true">                                            
                                                                </asp:HyperLink>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                            </td>
                                                            <td bgcolor="#D7E4C8">
                                                                <asp:Label ID="lblRoomSize" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblHeight" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td bgcolor="#D7E4C8" valign="top">
                                                                <p>
                                                                    Theatre Style</p>
                                                                <p>
                                                                    <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                        runat="server" Text=""></asp:Label></p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkTheatre" runat="server" Checked="true" /></p>
                                                            </td>
                                                            <td valign="top">
                                                                <p>
                                                                    School</p>
                                                                <p>
                                                                    <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                        runat="server" Text=""></asp:Label>
                                                                </p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkSchool" runat="server" Checked="true" /></p>
                                                            </td>
                                                            <td bgcolor="#D7E4C8" valign="top">
                                                                <p>
                                                                    U-Shape</p>
                                                                <p>
                                                                    <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblUShapeMax" runat="server" Text=""></asp:Label></p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkUshape" runat="server" Checked="true" /></p>
                                                            </td>
                                                            <td valign="top">
                                                                <p>
                                                                    Boardroom</p>
                                                                <p>
                                                                    <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblBanquetMax" runat="server" Text=""></asp:Label></p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkBoardroom" runat="server" Checked="true" /></p>
                                                            </td>
                                                            <td bgcolor="#D7E4C8" valign="top">
                                                                <p>
                                                                    Cocktail</p>
                                                                <p>
                                                                    <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblCocktailMax" runat="server" Text=""></asp:Label></p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkCocktail" runat="server" Checked="true" /></p>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </table>
                                        </div>
                                        <div class="booking-online-btn-i">
                                            <a href="#" class="add-basket-btn-big">Add to shopping cart</a> or <a href="#">Cancel</a>
                                        </div>
                                        <!--booking-request-body END HERE-->
                                        <!--booking-request-btn START HERE-->
                                        <!--booking-request-btn END HERE-->
                                    </div>
                                    <!--booking-request END HERE-->
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col21" id="divNext" runat="server">
                    <div class="button_sectionNext">
                        <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;"
                            OnClick="btnNext_Click" />
                    </div>
                </div>
                <div id="Loding_overlaySec">
                    <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                    <span>Loading...</span></div>
            </div>
            <!--main navigation end -->
        </div>
    </div>
    <!--wapper-inner ENDS HERE-->
    </form>
</body>
</html>
