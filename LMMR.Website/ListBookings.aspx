﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="ListBookings.aspx.cs" Inherits="ListBookings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControl/HotelUser/BookingDetails.ascx" TagName="BookingDetails" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
    </style>
    <link href="../css/stylew423.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .LinkPaging
        {
            width: 20;
            background-color: White;
            border: Solid 1 Black;
            text-align: center;
            margin-left: 8;
        }
    </style>
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="updTest">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="<%= SiteRootPath %>Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                <%= GetKeyResult("LOADING")%>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="<%= SiteRootPath %>Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                <%= GetKeyResult("LOADING")%>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="contract-list11">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <%--<td width="5%"><a href="#" class="pro">Profile</a></td>--%>
                <td width="14%">
                    <b><%= GetKeyResult("TORETEIEVEYOURBOOKING")%></b>
                </td>
                <td width="11%">
                    <asp:TextBox ID="txtBookingID" runat="server" MaxLength="10" CssClass="dateinputf"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="txtBookingIDExtender" runat="server" TargetControlID="txtBookingID"
                        FilterType="Numbers">
                    </asp:FilteredTextBoxExtender>
                </td>
                <td width="67%">
                    <asp:LinkButton ID="lbtGo" runat="server" CssClass="gobtn" OnClick="lbtGo_Click"><%= GetKeyResult("GO")%></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtReset"
                    runat="server" CssClass="gobtn" onclick="lbtReset_Click"><%= GetKeyResult("RESET")%></asp:LinkButton>
                </td>
                <%--<td width="62%"><a href="#" class="pro">Future booking</a></td>--%>
            </tr>
        </table>
    </div>
    <div class="contract-list11">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <%--<td width="12%">
                    <img src="Images/pdf-icon.png" align="absmiddle" />
                    save as pdf
                </td>--%>
                <td width="9%">
                    <b><%= GetKeyResult("ARRIVALDATE")%> :</b>
                </td>
                <td width="4%">
                    <%= GetKeyResult("FROM")%>
                </td>
                <td width="11%">
                    <asp:TextBox ID="txtFrom" runat="server" MaxLength="20" CssClass="dateinputf"></asp:TextBox>
                </td>
                <td width="4%">
                    <input type="image" src="<%= SiteRootPath %>Images/dateicon.png" id="calFrom2" tabindex="1" /><asp:CalendarExtender
                        ID="calExTO" runat="server" TargetControlID="txtFrom" PopupButtonID="calFrom2"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </td>
                <td width="2%">
                    <%= GetKeyResult("TO")%>
                </td>
                <td width="11%">
                    <asp:TextBox ID="txtTo" runat="server" MaxLength="20" CssClass="dateinputf"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
                        PopupButtonID="calTo" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </td>
                <td width="47%">
                    <input type="image" src="<%= SiteRootPath %>Images/dateicon.png" id="calTo" />
                </td>
            </tr>
        </table>
    </div>
    <div class="contract-list01">
        <div class="contract-list-right" style="width: 676px;">
            <ul runat="server" id="AlphaList">
                <li id="Li1" runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">all</a></li>
                <li id="Li2" runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">a</a></li>
                <li id="Li3" runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">b</a></li>
                <li id="Li4" runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">c</a></li>
                <li id="Li5" runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">d</a></li>
                <li id="Li6" runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">e</a></li>
                <li id="Li7" runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">f</a></li>
                <li id="Li8" runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">g</a></li>
                <li id="Li9" runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">h</a></li>
                <li id="Li10" runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">i</a></li>
                <li id="Li11" runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">j</a></li>
                <li id="Li12" runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">k</a></li>
                <li id="Li13" runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">l</a></li>
                <li id="Li14" runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">m</a></li>
                <li id="Li15" runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">n</a></li>
                <li id="Li16" runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">o</a></li>
                <li id="Li17" runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">p</a></li>
                <li id="Li18" runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">q</a></li>
                <li id="Li19" runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">r</a></li>
                <li id="Li20" runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">s</a></li>
                <li id="Li21" runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">t</a></li>
                <li id="Li22" runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">u</a></li>
                <li id="Li23" runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">v</a></li>
                <li id="Li24" runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">w</a></li>
                <li id="Li25" runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">x</a></li>
                <li id="Li26" runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">y</a></li>
                <li id="Li27" runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">z</a></li>
            </ul>
        </div>
    </div>
    <div id="booking-details" class="booking-detailsFE">
        <asp:UpdatePanel runat="server" ID="updTest">
            <ContentTemplate>
                <asp:HiddenField ID="hdnSelectedRowID" runat="server" Value="0" />
                <div id="divGrid">
                                <asp:GridView ID="grdViewBooking" runat="server" Width="973px" AutoGenerateColumns="False"
                                    RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" CellPadding="0"
                                    CellSpacing="1" OnRowDataBound="grdViewBooking_RowDataBound" EmptyDataText="No record Found!"
                                    EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                    EditRowStyle-VerticalAlign="Top" OnPageIndexChanging="grdViewBooking_PageIndexChanging"
                                    AllowPaging="true" GridLines="None" PageSize="10" CssClass="cofig" BackColor="#ffffff"
                                    OnRowCreated="grdViewBooking_RowCreated" ShowHeader="true" ShowHeaderWhenEmpty="true">
                                    <Columns>
                                        <asp:TemplateField>
                                        <HeaderStyle cssClass="heading-earned-row"  HorizontalAlign="Center"/>
                                        <ItemStyle CssClass="con-earned" />
                                            <HeaderTemplate>
                                                <asp:Label ID="lblRefNo" runat="server" Text="No."></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRefNo" runat="server" ToolTip='<%# Eval("HotelID") %>' CommandName="high"
                                                    Text='<%# Eval("Id") %>' CommandArgument='<%# ((GridViewRow) Container).RowIndex %>'
                                                    ForeColor="#339966" OnClick="lblRefNo_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                            <asp:LinkButton ID="lblRefNo" runat="server" ToolTip='<%# Eval("HotelID") %>' CommandName="high"
                                                    Text='<%# Eval("Id") %>' CommandArgument='<%# ((GridViewRow) Container).RowIndex %>'
                                                    ForeColor="#339966" OnClick="lblRefNo_Click"></asp:LinkButton>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center"/>
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="cityDDL" runat="server" CssClass="smallselect" AutoPostBack="true"
                                                    OnSelectedIndexChanged="cityDDL_selectedIndexChanged">
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCityName" runat="server" ToolTip='<%# Eval("HotelId") %>'></asp:Label>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                            <asp:Label ID="lblCityName" runat="server" ToolTip='<%# Eval("HotelId") %>'></asp:Label>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center" />
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="hotelDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                                    OnSelectedIndexChanged="hotelDDL_selectedIndexChanged">
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblHotelName" runat="server" ToolTip='<%# Eval("HotelId") %>' Text='<%# Eval("HotelName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <asp:Label ID="lblHotelName" runat="server" ToolTip='<%# Eval("HotelId") %>' Text='<%# Eval("HotelName") %>'></asp:Label>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row2"  HorizontalAlign="Center" />
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="meetingRoomDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                                    OnSelectedIndexChanged="meetingRoomDDL_selectedIndexChanged">
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblMeetingRoom" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <asp:Label ID="lblMeetingRoom" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center" />
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="bookingDateDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                                    OnSelectedIndexChanged="bookingDateDDL_selectedIndexChanged">
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBookingDt" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <asp:Label ID="lblBookingDt" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center" />
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                            <asp:DropDownList ID="arrivalDateDDL" runat="server" OnSelectedIndexChanged="arrivalDateDDL_selectedIndexChanged" CssClass="midselect" AutoPostBack="true"></asp:DropDownList>
                                                <%--<asp:Label ID="lblArrival" runat="server" Text="Arrival"></asp:Label>--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrivalDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("ArrivalDate"))%>'></asp:Label>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <asp:Label ID="lblArrivalDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("ArrivalDate"))%>'></asp:Label>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center" />
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                                <asp:Label ID="lblDeparture" runat="server" Text="Departure"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepartureDt" runat="server" Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("DepartureDate"))%>'
                                                    ToolTip='<%# Eval("DepartureDate")%>'></asp:Label>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <asp:Label ID="lblDepartureDt" runat="server" Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("DepartureDate"))%>'
                                                    ToolTip='<%# Eval("DepartureDate")%>'></asp:Label>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center" />
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                                <asp:DropDownList ID="valueDDL" runat="server" CssClass="smallselect" OnSelectedIndexChanged="valueDDL_selectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                &#8364;&nbsp;<asp:Label ID="lblFinalTotal" runat="server" Text='<%# String.Format("{0:###,###,###}",Eval("FinalTotalPrice")) %>'></asp:Label>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                &#8364;&nbsp;<asp:Label ID="lblFinalTotal" runat="server" Text='<%# String.Format("{0:###,###,###}",Eval("FinalTotalPrice")) %>'></asp:Label>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center" />
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                                <asp:Label ID="lblCancellation" runat="server" Text="Free cancellation until"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCancellationLimit" runat="server" ToolTip='<%# Eval("HotelId") %>'></asp:Label>
                                            </ItemTemplate>

                                            <AlternatingItemTemplate>
                                                <asp:Label ID="lblCancellationLimit" runat="server" ToolTip='<%# Eval("HotelId") %>'></asp:Label>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                        <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center" />
                                        <ItemStyle CssClass="con1-earned" />
                                            <HeaderTemplate>
                                                <asp:Label ID="lblCancel" runat="server" Text="Cancel"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtCancel" runat="server"  Visible="false" ToolTip='<%# Eval("Id") %>' CssClass="select"
                                                OnClick="lbtCancel_Click" OnClientClick="return confirm('Are you sure you want to cancel.');">Cancel</asp:LinkButton>
                                                <asp:Label ID="lblCancelled" runat="server" Text="" Visible="false"></asp:Label>
                                                <asp:LinkButton ID="btnchkcommision" runat="server" ForeColor="#339966" CommandArgument='<%# Eval("Id") %>'
                                                    OnClick="btnchkcommision_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <asp:LinkButton ID="lbtCancel" runat="server" Visible="false" ToolTip='<%# Eval("Id") %>' CssClass="select"
                                                OnClick="lbtCancel_Click"  OnClientClick="return confirm('Are you sure you want to cancel.');">Cancel</asp:LinkButton>
                                                <asp:Label ID="lblCancelled" runat="server" Text="" Visible="false"></asp:Label>
                                                <asp:LinkButton ID="btnchkcommision" runat="server" ForeColor="#339966" CommandArgument='<%# Eval("Id") %>'
                                                    OnClick="btnchkcommision_Click"></asp:LinkButton>
                                            </AlternatingItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <tr>
                                            <td colspan="11" align="center">
                                                "No record Found!"
                                            </td>
                                        </tr>
                                    </EmptyDataTemplate>
                                    <AlternatingRowStyle CssClass="con-earned-dark1" BackColor="#D9EEFC" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True"></EmptyDataRowStyle>
                                    <PagerSettings Mode="NumericFirstLast" Position="Top" />
                                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                                    <PagerTemplate>
                                                <div style="float: right; width: auto;" >
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </div>
                                    </PagerTemplate>
                                </asp:GridView>
                </div>
                <div class="booking-detailsFE" id="divprintpdfllink" runat="server">
                    <ul>
                        <li class="value3" style="width: 975px">
                            <div class="col9" style="width: 975px">
                                <img src="Images/print.png" />&nbsp; <a id="uiLinkButtonPrintGrid" style="cursor: pointer;"
                                    onclick="javascript:Button1_onclick('divGrid');"><%= GetKeyResult("PRINT")%></a> &bull;
                                <img src="Images/pdf.png" />&nbsp;<asp:LinkButton ID="uiLinkButtonSaveAsPdfGrid"
                                    runat="server" OnClick="uiLinkButtonSaveAsPdfGrid_Click"><%= GetKeyResult("SAVEASPDF")%></asp:LinkButton>
                            </div>
                        </li>
                    </ul>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="uiLinkButtonSaveAsPdfGrid" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div id="divbookingdetails" align="left" runat="server">
                    <br />
                    <h1 class="new">
                        <%= GetKeyResult("BOOKINGDETAILS")%></h1>
                    <div class="booking-detailsFE" id="divprint" runat="server">
                        <ul>
                            <li class="value3">
                                <div class="col9" style="width: 978px;">
                                    <img src="Images/print.png" />&nbsp; <a id="ADetails" style="cursor: pointer;"
                                        onclick="javascript:Button1_onclick('<%= Divdetails.ClientID%>');"><%= GetKeyResult("PRINT")%></a>
                                    &bull;
                                    <img src="Images/pdf.png" />&nbsp;
                                    <asp:LinkButton ID="lnkSavePDF" runat="server" OnClick="lnkSavePDF_Click"><%= GetKeyResult("SAVEASPDF")%></asp:LinkButton>
                                </div>
                            </li>
                        </ul>
                    </div>
                
                <div id="Divdetails" runat="server" class="meinnewbodybig">

                        <uc1:BookingDetails ID="bookingDetails" runat="server"></uc1:BookingDetails>
                    </div>
                    
                    

                    
                         
                   <%-- <div class="button-center">
                        <div class="blue-button" id="divlinkpro" runat="server" align="center">
                            <asp:LinkButton ID="Lnkmovetoprocessed" runat="server" CssClass="link" ForeColor="White"
                                Font-Underline="false" OnClick="Lnkmovetoprocessed_Click">Process</asp:LinkButton>
                        </div>
                    </div>--%>
                </div>
                <br />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkSavePDF" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:LinkButton ID="lnkbtn" OnClientClick="javascript:document.getElementById('contentbody_pnlchkCommission').style.display='block';"
        runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="modalcheckcomm" TargetControlID="lnkbtn" BackgroundCssClass="modalBackground"
        PopupControlID="pnlchkCommission" runat="server">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlchkCommission" BorderColor="#999999" Width="757px" BorderWidth="5"
        Style="display: none; padding-top: 7px;" runat="server" BackColor="White">
        <div class="popup-mid-inner-body">
            Here you can confirm Booking revenue or adjust it.
        </div>
        <div id="divmessage" runat="server">
        </div>
        <div class="popup-mid-inner-body1">
            <asp:UpdatePanel ID="upcheckcomm" runat="server">
                <ContentTemplate>
                    <table width="100%" cellspacing="0" cellpadding="3" align="center">
                        <tr>
                            <td width="30%">
                                <table>
                                    <tr>
                                        <td align="right">
                                            Revenue :
                                        </td>
                                        <td>
                                            <asp:Label ID="lblrevenue" runat="server" Text="6.820"></asp:Label>
                                            &nbsp;Euro
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkconfirmrevenue" AutoPostBack="true" Checked="true" Text="Confirm the revenue"
                                                OnCheckedChanged="chkconfirmrevenue_CheckedChanged" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td align="center" colspan="2">
                                            <b>Adjust Revenue (if needed)</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Insert Netto (VAT excluded) :
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtrealvalue" runat="server" MaxLength="7"></asp:TextBox><asp:FilteredTextBoxExtender
                                                ID="FilteredTextBoxExtenderrealvalue" runat="server" TargetControlID="txtrealvalue"
                                                ValidChars="." FilterMode="ValidChars" FilterType="Numbers,Custom">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Reason :
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlreason" runat="server" CssClass="NoClassApply">
                                                <asp:ListItem>Cancelled and agreed between hotel and client</asp:ListItem>
                                                <asp:ListItem>Changes in Duration</asp:ListItem>
                                                <asp:ListItem>Increased number of participants</asp:ListItem>
                                                <asp:ListItem>No-Show</asp:ListItem>
                                                <asp:ListItem>Reduced number of participants</asp:ListItem>
                                                <asp:ListItem>Variation on package price</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            Supporting document :
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="ulPlan" runat="server" /><br />
                                            <span style="font-family: Arial; font-size: Smaller; color: Gray">Supporting document
                                                file format: PDF/Word/Excel & max size 1MB. </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <table width="100%">
            </table>
        </div>
        <div>
            <div class="popup-mid-inner-body2_i">
            </div>
            <div class="booking-details" style="width: 760px;">
                <ul>
                    <li class="value10">
                        <div class="col21" style="width: 752px;">
                            <div class="button_section">
                                <asp:LinkButton ID="btnSubmit" runat="server" Text="Save" CssClass="select" OnClick="btnSubmit_Click" />
                                <span>or</span>
                                <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" />
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <div class="mainbody-left">
        <div class="banner">
            <iframe src="<%= SiteRootPath %>SlideShow.aspx" width="100%" height="100%"></iframe>
        </div>
    </div>
    <div class="mainbody-right">
        <div id="beforeLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
            </div>
            <div class="mainbody-right-join" id="divjoinus" runat="server">
               <div class="join">
                    <a runat="server" id="joinToday">
                        <%= GetKeyResult("JOINUSTODAY")%></a></div>
                <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
            </div>
            <div class="mainbody-right-you">
                <a href="http://www.youtube.com/watch?v=rkt3NIWecG8&feature=youtu.be" target="_blank">
                    <img src="<%= SiteRootPath %>images/youtube-icon.png" /></a>&nbsp;&nbsp;<a href="http://www.youtube.com/watch?v=U3y7d7yxmvE&feature=relmfu"
                        target="_blank"><img src="<%= SiteRootPath %>images/small-vedio.png" /></a>
            </div>
        </div>
        <div style="width: 216px; height: auto; overflow: hidden; margin: 0px auto; display: none"
            id="afterLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
            </div>
            <!--start mainbody-right-afterlogin-body -->
            <div class="mainbody-right-afterlogin-body">
                <div class="mainbody-right-afterlogin-top">
                    <div class="mainbody-right-afterlogin-inner">
                        <b><%= GetKeyResult("TODAY")%> :</b>
                        <asp:Label ID="lstLoginTime" runat="server"></asp:Label>
                        <div class="afterlogin-welcome">
                            <span><%= GetKeyResult("WELCOME")%></span>
                            <br>
                            <asp:Label ID="lblLoginUserName" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="mainbody-right-afterlogin-bottom">
                    <div class="mainbody-right-afterlogin-bottom-inner">
                        <asp:LinkButton ID="hypManageProfile" runat="server" OnClick="hypManageProfile_Click"><%= GetKeyResult("MANAGEPROFILE")%></asp:LinkButton> <br/>
			            <asp:LinkButton ID="hypChangepassword" runat="server" OnClick="hypChangepassword_Click"><%= GetKeyResult("CHANGEPASSWORD")%></asp:LinkButton><br/>
                        <asp:LinkButton ID="hypListBookings" runat="server" OnClick="hypListBookings_Click" Visible="true"><%= GetKeyResult("MYBOOKINGS")%></asp:LinkButton><br/>
                        <asp:LinkButton ID="hypListRequests" runat="server" OnClick="hypListRequests_Click" Visible="true"><%= GetKeyResult("MYREQUESTS")%></asp:LinkButton>
                    </div>
                </div>
            </div>
            <!--end mainbody-right-afterlogin-body -->
        </div>
    </div>
    <script language="javascript" type="text/javascript">

        function Button1_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }


    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= txtFrom.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtTo.ClientID %>").attr("disabled", true);
            jQuery("#<%= lbtGo.ClientID%>").bind("click", function () {
                jQuery(".error").html("");
                jQuery(".error").hide();
                jQuery("#<%= txtFrom.ClientID %>").attr("disabled", false);
                jQuery("#<%= txtTo.ClientID %>").attr("disabled", false);
                jQuery("#Loding_overlay").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                var fromdate = jQuery("#<%= txtFrom.ClientID %>").val();
                var todate = jQuery("#<%= txtTo.ClientID %>").val();
                if (fromdate.length != 0 && todate.length != 0) {
                    var todayArr = todate.split('/');
                    var formdayArr = fromdate.split('/');
                    var fromdatecheck = new Date();
                    var todatecheck = new Date();
                    fromdatecheck.setFullYear(parseInt("20" + formdayArr[2]), (parseInt(formdayArr[1]) - 1), formdayArr[0]);
                    todatecheck.setFullYear(parseInt("20" + todayArr[2]), (parseInt(todayArr[1]) - 1), todayArr[0]);
                    if (fromdatecheck > todatecheck) {
                        alert('<%= GetKeyResultForJavaScript("FROMDATEMUSTBEEARLIEROREQUALTOTODATE")%>');
                        return false;
                    } 
                }
            });
        });
    </script>
    <script language="javascript" type="text/javascript">
                        jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');
                                jQuery('#cntLeftBottom_cntLeft_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                        });
    </script>

    <script language="javascript" type="text/javascript">

    function open_win2(pageurl) {

            var url = pageurl;
            var winName = 'myWindow';
            var w = '700';
            var h = '500';
            var scroll = 'no';
            var popupWindow = null;
            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
            settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=yes,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
            popupWindow = window.open(url, winName, settings)
            return false;

        }

        function uuu() {

            var r = confirm('<%= GetKeyResultForJavaScript("AREYOUSUREYOUWANTTODELETE")%>');
            if (r == true) {
                return true;
            }
            else {
                return false;
            }
        }

   </script>

    
</asp:Content>
