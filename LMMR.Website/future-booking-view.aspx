﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="future-booking-view.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <div class="future-booking-view">
        <div class="search-booking">
            <div class="contract-list1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="5%">
                            <a href="#" class="pro">Profile</a>
                        </td>
                        <td width="17%">
                            <b>To retrive your booking</b>
                        </td>
                        <td width="11%">
                            <input class="dateinputf">
                        </td>
                        <td width="5%">
                            <a href="#" class="gobtn">Go</a>
                        </td>
                        <td width="62%">
                            <a href="#" class="pro">Future booking</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="contract-list1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="12%">
                            <img src="../images/pdf-icon.png" align="absmiddle" />
                            save as pdf
                        </td>
                        <td width="9%">
                            <b>arrival date :</b>
                        </td>
                        <td width="4%">
                            from
                        </td>
                        <td width="11%">
                            <input class="dateinputf">
                        </td>
                        <td width="4%">
                            <img src="../images/dateicon.png" align="absmiddle" />
                        </td>
                        <td width="2%">
                            to
                        </td>
                        <td width="11%">
                            <input class="dateinputf">
                        </td>
                        <td width="47%">
                            <img src="../images/dateicon.png" align="absmiddle" />
                        </td>
                    </tr>
                </table>
            </div>
            <!--contract-list start -->
            <div class="contract-list">
                <div class="contract-list-right">
                    <ul>
                        <li><a href="#">a</a></li><li><a href="#" class="select">b</a></li><li><a href="#">c</a></li><li>
                            <a href="#">d</a></li><li><a href="#">e</a></li><li><a href="#">f</a></li><li><a
                                href="#">g</a></li><li><a href="#">h</a></li><li><a href="#">i</a></li><li><a href="#">j</a></li><li><a href="#">k</a></li><li><a href="#">l</a></li><li><a href="#">m</a></li><li>
                                        <a href="#">n</a></li><li><a href="#">o</a></li><li><a href="#">p</a></li><li><a
                                            href="#">q</a></li><li><a href="#">r</a></li><li><a href="#">s</a></li><li><a href="#">t</a></li><li><a href="#">u</a></li><li><a href="#">v</a></li><li><a href="#">w</a></li><li>
                                                    <a href="#">x</a></li><li><a href="#">y</a></li><li><a href="#">z</a></li>
                    </ul>
                </div>
            </div>
            <!-- end contract-list-->
            <div class="pageing-operator">
                <div class="pageing">
                    <a class="arrow-right select" href="#"></a>
                    <div class="pageing-mid">
                        <a href="#">Prevous</a> <a class="no" href="#">1</a><a class="no select" href="#">2</a><a
                            class="no" href="#">3</a> ... <a class="no" href="#">8</a> <a href="#">next</a></div>
                    <a class="arrow-left" href="#"></a>
                </div>
            </div>
            <div class="future-booking-viewmain">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="heading-earned-row">
                            No.
                        </td>
                        <td class="heading-earned-row1">
                            <div id="City4">
                                <select name="test">
                                    <option>City</option>
                                    <option value="City1">City1</option>
                                    <option value="City2">City2</option>
                                    <option value="City3">City3</option>
                                </select>
                            </div>
                        </td>
                        <td class="heading-earned-row1">
                            <div id="FacilityHotalname">
                                <select name="test">
                                    <option>F/H name</option>
                                    <option value="F/H name1">F/H name1</option>
                                    <option value="F/H name2">F/H name2</option>
                                    <option value="F/H name3">F/H name3</option>
                                </select>
                            </div>
                        </td>
                        <td class="heading-earned-row2">
                            <div id="Meetingroom">
                                <select name="test">
                                    <option>Meeting room</option>
                                    <option value="Meeting room1">Meeting room2</option>
                                    <option value="Meeting room2">Meeting room2</option>
                                    <option value="Meeting room3">Meeting room3</option>
                                </select>
                            </div>
                        </td>
                        <td class="heading-earned-row1">
                            <div id="Bookingdate">
                                <select name="test">
                                    <option>Booking date</option>
                                    <option value="Booking date1">Booking date1</option>
                                    <option value="Booking date2">Booking date2</option>
                                    <option value="Booking date3">Booking date3</option>
                                </select>
                            </div>
                        </td>
                        <td class="heading-earned-row1">
                            <div id="Arrival">
                                <select name="test">
                                    <option>Arrival</option>
                                    <option value="Arrival1">Arrival1</option>
                                    <option value="Arrival2">Arrival2</option>
                                    <option value="Arrival3">Arrival3</option>
                                </select>
                            </div>
                        </td>
                        <td class="heading-earned-row1">
                            Departure
                        </td>
                        <td class="heading-earned-row1">
                            <div id="Value">
                                <select name="test">
                                    <option>Value</option>
                                    <option value="Value1">Value1</option>
                                    <option value="Value2">Value2</option>
                                    <option value="Value3">Value3</option>
                                </select>
                            </div>
                        </td>
                        <td class="heading-earned-row1">
                            Free cancetion until
                        </td>
                        <td class="heading-earned-row1">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="con-earned">
                            1245
                        </td>
                        <td class="con1-earned">
                            Antw
                        </td>
                        <td class="con1-earned">
                            Bapart
                        </td>
                        <td class="con1-earned">
                            Leanardo
                        </td>
                        <td class="con1-earned">
                            28.Aug 2011
                        </td>
                        <td class="con1-earned">
                            30.Oct 2011
                        </td>
                        <td class="con1-earned">
                            30.Oct 2011
                        </td>
                        <td class="con1-earned">
                            3.450.00E
                        </td>
                        <td class="con1-earned">
                            30sep 2011 12.37
                        </td>
                        <td class="con1-earned">
                            Cancel
                        </td>
                    </tr>
                    <tr>
                        <td class="con-earned-dark">
                            124
                        </td>
                        <td class="con-earned-dark1">
                            Dserr
                        </td>
                        <td class="con-earned-dark1">
                            Bapart
                        </td>
                        <td class="con-earned-dark1">
                            Meanardo
                        </td>
                        <td class="con-earned-dark1">
                            28.Aug 2011
                        </td>
                        <td class="con-earned-dark1">
                            30.Oct 2011
                        </td>
                        <td class="con-earned-dark1">
                            30.Oct 2011
                        </td>
                        <td class="con-earned-dark1">
                            3.450.00E
                        </td>
                        <td class="con-earned-dark1">
                            30sep 2011 12.37
                        </td>
                        <td class="con-earned-dark1">
                            Cancel
                        </td>
                    </tr>
                    <tr>
                        <td class="con-earned">
                            1245
                        </td>
                        <td class="con1-earned">
                            Antw
                        </td>
                        <td class="con1-earned">
                            Bapart
                        </td>
                        <td class="con1-earned">
                            Leanardo
                        </td>
                        <td class="con1-earned">
                            28.Aug 2011
                        </td>
                        <td class="con1-earned">
                            30.Oct 2011
                        </td>
                        <td class="con1-earned">
                            30.Oct 2011
                        </td>
                        <td class="con1-earned">
                            3.450.00E
                        </td>
                        <td class="con1-earned">
                            30sep 2011 12.37
                        </td>
                        <td class="con1-earned">
                            Cancel
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
</asp:Content>
