﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;
using LMMR.Data;
using System.Globalization;



#endregion

public partial class SurveyRequest : System.Web.UI.Page
{
    #region variable declaration
    Surveydata obj = new Surveydata();
    surveybookingrequest objsurveybookingrequest = new surveybookingrequest();
    ServeyAnswer insertsurvey = new ServeyAnswer();
    TList<ServeyAnswer> objsurvey = new TList<ServeyAnswer>();
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    TList<ServeyQuestion> objsurveydesc = new TList<ServeyQuestion>();
    string status;
    string substatus;

    bool status1;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divmessage.Visible = false;
            BindHeadrepeter();
            bindhotel();
            int venueId = Convert.ToInt32(Request.QueryString["venue"]);
            int bookingId = Convert.ToInt32(Request.QueryString["bookingid"]);
            HotelInfo info = new HotelInfo();
            lblVenueName.Text = "Venue Name : " + (info.GetHotelByHotelID(venueId) == null ? "" : info.GetHotelByHotelID(venueId).Name);
            lblDate.Text = "Meeting Date : " + (info.GetBookingByBookingID(bookingId) == null ? "" : Convert.ToDateTime(info.GetBookingByBookingID(bookingId).ArrivalDate).ToString("dd/MM/yy"));            
        }
        
    }
    public void bindhotel()
    {
        int Userid = Convert.ToInt32(Request.QueryString["UserID"]);
        if (Userid != null)
        {
            TList<Booking> objbook = objViewBooking_Hotel.fetchrequestlink(Convert.ToInt32(Request.QueryString["bookingID"]));
            string id = "";
            int val = 0;
            foreach (Booking b1 in objbook)
            {
                if (string.IsNullOrEmpty(id))
                {
                    id = Convert.ToString(b1.Id);
                }
                else
                {
                    id += "," + Convert.ToString(b1.Id);
                }

                if (b1.RequestStatus == (int)BookingRequestStatus.Definite)
                {
                    val = 1;
                }


            }

            if (objbook.Count == 1)
            {
                val = 1;
            }



            string where = "id in (" + id + ")";

            VList<Viewbookingrequest> objview = objViewBooking_Hotel.fetchrequestlinkforDropdown(where);
            drphotel.DataValueField = "HotelId";
            drphotel.DataTextField = "HotelName";
            drphotel.DataSource = objview;
            drphotel.DataBind();
            if (objview.Count > 1)
            {
                lblVenueName.Visible = false;
            }
            else
            {
                lblVenueName.Visible = true;
            }

            if (val == 1)
            {
                tblhotel.Visible = false;
                tblhotel1.Visible = false;
            }


        }
    }

    protected void Dropmetingheld_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Dropmetingheld.SelectedItem.Value == "1")
        {
            drphotel.Visible = true;
            //tblhotel.Visible = true;
            pnlRequest.Visible = true;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "alert('Thank you very much for your feedback, we hope to see you soon on www.lastminutemeetingroom.com'); window.close();", true);
            pnlRequest.Visible = false;
            //tblhotel.Visible = false;
            drphotel.Visible = false;            
        }
    }

    public void BindHeadrepeter()
    {
        objsurveydesc = objsurveybookingrequest.GetAllsurveyDescription();
        rptheadQuestion.DataSource = objsurveydesc;
        rptheadQuestion.DataBind();
    }

    protected void rptheadQuestion_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServeyQuestion surveydesc = e.Item.DataItem as ServeyQuestion;
            Label lblHeader = (Label)e.Item.FindControl("lblHeader");
            HiddenField hdfsurveyId = (HiddenField)e.Item.FindControl("hdfsurveyId");
            Repeater rptsubquestions = (Repeater)e.Item.FindControl("rptsubquestions");
            if (surveydesc != null)
            {
                lblHeader.Text = surveydesc.QuestionName;
                hdfsurveyId.Value = surveydesc.QuestionId.ToString();
                rptsubquestions.DataSource = objsurveybookingrequest.Getsurveyrequest().Where(a => a.QuestionId == surveydesc.QuestionId).ToList();
                rptsubquestions.DataBind();
            }
        }
    }

    protected void rptsubquestions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServeyAnswer surveyanswer = e.Item.DataItem as ServeyAnswer;
            Label lblsubquestions = (Label)e.Item.FindControl("lblsubquestions");
            HiddenField hdfSubQsurveyId = (HiddenField)e.Item.FindControl("hdfSubQsurveyId");
            RadioButtonList rbtsurvey = (RadioButtonList)e.Item.FindControl("rbtsurvey");
            TextBox commentsText = (TextBox)e.Item.FindControl("commentsText");
            if (surveyanswer != null)
            {
                lblsubquestions.Text = surveyanswer.Answer;
                hdfSubQsurveyId.Value = surveyanswer.AnswerId.ToString();
                if (hdfSubQsurveyId.Value=="4")
                {
                    rbtsurvey.Visible = false;
                    commentsText.Visible = true;
                    hdfcommentstxt.Value = commentsText.Text;
                }
                else
                {
                    rbtsurvey.Visible = true;
                    commentsText.Visible = false;
                    hdfcommentstxt.Value = "";
                }
                
            }
        }
    }

    protected void rptsubquestionscreated(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void btn_submit_Click(object sender, EventArgs e)
    {
        int Userid = Convert.ToInt32(Request.QueryString["UserID"]);
        int venueid = Convert.ToInt32(Request.QueryString["venue"]);
        int bookingID = Convert.ToInt32(Request.QueryString["bookingID"]);
        Booking onjbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(bookingID));
        Booking onjbooking1 = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(bookingID));
        if (string.IsNullOrEmpty(onjbooking.RequestCollectionId))
            onjbooking.RequestCollectionId = "0";
        if (string.IsNullOrEmpty(onjbooking1.RequestCollectionId))
            onjbooking1.RequestCollectionId = "0";
        if ((onjbooking.IsSurveyDone == false) && ((onjbooking.HotelId == Convert.ToInt64(drphotel.SelectedItem.Value)) || (Convert.ToString(onjbooking.RequestCollectionId) == Convert.ToString(onjbooking1.RequestCollectionId))))
        {
            onjbooking.IsSurveyDone = true;
            if (onjbooking.RequestCollectionId == "0")
            {
                onjbooking.IsSurveyDone = true;
                DataRepository.BookingProvider.Update(onjbooking);
                status1 = DataRepository.BookingProvider.Update(onjbooking);
            }
            else
            {
                int totalcount = 0;
                string where = " RequestCollectionID='" + onjbooking.RequestCollectionId + "'";
                TList<Booking> alreq = DataRepository.BookingProvider.GetPaged(where, "", 0, int.MaxValue, out totalcount);
                foreach (Booking b1 in alreq)
                {
                    b1.IsSurveyDone = true;
                    DataRepository.BookingProvider.Update(b1);
                    status1 = DataRepository.BookingProvider.Update(onjbooking);
                }
            }
            if (status1 == true)
            {
                Serveyresult res = new Serveyresult();
                ServeyResponse response = new ServeyResponse();
                response.UserId = Userid;
                response.VanueId = Convert.ToInt64(drphotel.SelectedItem.Value);
                response.BookingId = bookingID;
                response.BookingType = Convert.ToInt32(onjbooking.BookType);
                if (hdfcommentstxt.Value != "")
                {
                    response.AdditionalComment = hdfcommentstxt.Value;
                    status = obj.insertSurvey(response);
                }
                else
                {
                    status = obj.insertSurvey(response);
                }

                if (status == "Thank You For The Survey")
                {
                    foreach (RepeaterItem item in rptheadQuestion.Items)
                    {
                        Repeater rptsubquestions = (Repeater)item.FindControl("rptsubquestions");
                        HiddenField hdfsurveyId = (HiddenField)item.FindControl("hdfsurveyId");
                        //TextBox commentsText = (TextBox)rptsubquestions.FindControl("commentsText");
                        foreach (RepeaterItem childitem in rptsubquestions.Items)
                        {
                            HiddenField hdfSubQsurveyId = (HiddenField)childitem.FindControl("hdfSubQsurveyId");
                            RadioButtonList rbtsurvey = (RadioButtonList)childitem.FindControl("rbtsurvey");
                            TextBox commentsText = (TextBox)childitem.FindControl("commentsText");
                            ServeyResponse s = DataRepository.ServeyResponseProvider.GetByServeyId(Convert.ToInt32(response.ServeyId));
                            s.AdditionalComment = commentsText.Text;
                            DataRepository.ServeyResponseProvider.Update(s);

                            TList<ServeyAnswer> objans = objsurveybookingrequest.GetbyQuestionIdRequest(Convert.ToInt32(hdfSubQsurveyId.Value));
                            foreach (ServeyAnswer sa in objans)
                            {
                                if (Convert.ToInt32(hdfsurveyId.Value) != 4)
                                {

                                    res.QuestionId = Convert.ToInt32(hdfsurveyId.Value);
                                    res.AnswerId = sa.AnswerId;
                                    res.Rating = Convert.ToInt32(rbtsurvey.SelectedValue);
                                    res.ServeyId = response.ServeyId;
                                    substatus = obj.insertSurvey(res);
                                }

                            }
                            if (substatus == "Thank You For The Survey")
                            {

                                divsurvey.Visible = true;
                                divmessage.Visible = true;
                                divmessage.Style.Add("display", "block");
                                divmessage.Attributes.Add("class", "succesfuly");
                                divmessage.InnerHtml = status;

                            }
                        }
                    }
                }
                else
                {
                    divsurvey.Visible = true;
                    divmessage.Visible = true;
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    divmessage.InnerHtml = "Survey did not Save!";

                }
            }
            else
            {
                divsurvey.Visible = true;
                divmessage.Visible = true;
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                divmessage.InnerHtml = "Survey did not Save!";

            }
        }
        else
        {
            status = "Survey already done for the Hotel";
            divsurvey.Visible = true;
            divmessage.Visible = true;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;
        }


    }
}