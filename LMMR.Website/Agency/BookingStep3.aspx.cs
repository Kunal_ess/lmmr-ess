﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Configuration;
using log4net;
using log4net.Config;
using AjaxControlToolkit;
using System.Xml;

public partial class Agency_BookingStep3 : BasePage
{
    #region Variables and Properties
    Createbooking objBooking = null;
    HotelManager objHotel = new HotelManager();
    Hotel objBookedHotel = new Hotel();
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public string UserCurrency
    {
        get { return Convert.ToString(ViewState["UserCurrency"]); }
        set { ViewState["UserCurrency"] = value; }
    }
    public string HotelCurrency
    {
        get { return Convert.ToString(ViewState["HotelCurrency"]); }
        set { ViewState["HotelCurrency"] = value; }
    }
    public decimal CurrencyConvert
    {
        get { return Convert.ToDecimal(ViewState["CurrencyConvert"]); }
        set { ViewState["CurrencyConvert"] = value; }
    }
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomID
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomConfigureID
    {
        get;
        set;
    }
    public int CurrentPackageType
    {
        get;
        set;
    }
    public UserControl_Agency_LeftSearchPanel u
    {
        get
        {
            return Page.Master.FindControl("cntLeftSearch").FindControl("LeftSearchPanel1") as UserControl_Agency_LeftSearchPanel;
        }
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUserID"] == null)
        {
            Response.Redirect("~/Agency/SearchBookingRequest.aspx");
        }
        u.SearchButton += new EventHandler(u_SearchButton);
        u.MapSearch += new EventHandler(u_MapSearch);
        if (!Page.IsPostBack)
        {
            u.usercontroltype = "Basic";
            Users objUsers = Session["CurrentAgencyUser"] as Users;
            //Bind Login users details
            //lblLoginTime.Text = DateTime.Now.ToString("dd MMM yyyy");
            //lblLoginUser.Text = objUsers.FirstName + " " + objUsers.LastName;
            lblclientName.Text = objUsers.FirstName + " " + objUsers.LastName;
            UserDetails ud = new NewUser().getDetailsByID(objUsers.UserId);
            if (objUsers.Usertype == (int)Usertype.Agency || objUsers.Usertype == (int)Usertype.Company)
            {
                if (ud != null)
                {
                    pnlIsCompOrAgency.Visible = true;

                    lblCompany.Text = ud.CompanyName;
                    //if (objUsers.Usertype == (int)Usertype.Agency)
                    //{
                    //    lnkChangeProfile.Visible = true;
                    //}
                    //else
                    //{
                    //    lnkChangeProfile.Visible = false;
                    //}
                }
            }
            else
            {
                pnlIsCompOrAgency.Visible = false;
            }
            //Check Availability of Search.
            CheckSearchAvailable();
            Cms cmsObj = new SuperAdminTaskManager().getCmsEntityOnType("TermsAndConditions");
            if (cmsObj != null)
            {
                TList<CmsDescription> list = new ManageCMSContent().GetCMSContent((Int32)cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
                CmsDescription obj1 = new CmsDescription();
                if (list.Count != 0)
                {
                    hypTermsandCondition.NavigateUrl = ConfigurationManager.AppSettings["FilePath"] + "MediaFile/" + list[0].PageUrl.ToString();
                    hypTermsandCondition.Target = "_blank";
                    hypTermsandCondition.Attributes.Add("onclick", "return open_win('" + SiteRootPath + hypTermsandCondition.NavigateUrl.Replace("~/", "") + "');");
                }
                else
                {
                    list = new ManageCMSContent().GetCMSContent((Int32)cmsObj.Id, Convert.ToInt64("1"));
                    if (list.Count != 0)
                    {
                        hypTermsandCondition.NavigateUrl = ConfigurationManager.AppSettings["FilePath"] + "MediaFile/" + list[0].PageUrl.ToString();
                        hypTermsandCondition.Target = "_blank";
                        hypTermsandCondition.Attributes.Add("onclick", "return open_win('" + SiteRootPath + hypTermsandCondition.NavigateUrl.Replace("~/", "") + "');");
                    }
                    else
                    {
                        hypTermsandCondition.Visible = false;
                    }
                }
            }

            if (objBooking != null)
            {
                tAndCPopUp.OnClientClick = "return open_win2('" + SiteRootPath + "Policy.aspx?hid=" + objBooking.HotelID + "')";
            }
        }
    }
    void u_SearchButton(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Booking";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void u_MapSearch(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Booking";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    BookingRequest objBookingRequest = new BookingRequest();
    void ManageSession()
    {
        string WhereClause = string.Empty;
        string WhereClauseday2 = string.Empty;
        if (u.propCountryID != "0")
        {
            if (u.propCountryID != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + u.propCountryID;
                WhereClauseday2 += HotelColumn.CountryId + "=" + u.propCountryID;
            }
        }

        if (Convert.ToInt32(u.propCity) != 0)
        {
            if (u.propCity != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + u.propCity;
                WhereClauseday2 += HotelColumn.CityId + "=" + u.propCity;
            }
        }
        if (u.propDuration == "1")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }
        if (u.propDuration == "2")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }


                if (u.propDay2 == "0")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay2 == "1")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay2 == "2")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }

        if (u.propDate != "")
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + u.propDate.Split('/')[2]), Convert.ToInt32(u.propDate.Split('/')[1]), Convert.ToInt32(u.propDate.Split('/')[0]));

            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }


        if (u.propParticipant != "" && u.propParticipant != "0")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            WhereClauseday2 += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        }

        //Send all search session value
        Session["Where"] = WhereClause;
        if (u.propDuration == "2")
        {
            Session["Where2"] = WhereClauseday2;
        }

        //Maintain session value for all controls 
        objBookingRequest = new BookingRequest();
        objBookingRequest.propCountry = u.propCountryID;
        objBookingRequest.propCity = u.propCity;
        objBookingRequest.propDate = Convert.ToString(string.IsNullOrEmpty(u.propDate) ? (u.propDate == "dd/mm/yy" ? DateTime.Now.ToString("dd/MM/yy") : u.propDate) : u.propDate);
        objBookingRequest.propDuration = u.propDuration;
        objBookingRequest.propDays = u.propDay1;
        objBookingRequest.propDay2 = u.propDay2;
        if (u.propParticipant != "")
        {
            objBookingRequest.propParticipants = u.propParticipant;
        }
        else
        {
            objBookingRequest.propParticipants = "0";
        }
        Session["masterInput"] = objBookingRequest;
        Session["CurrencyID"] = "2";
        //Dictionary<long, string> participants = new Dictionary<long, string>();
        //participants.Add(1, objBookingRequest.propParticipants);
        //Session["participants"] = participants;
    }
    #endregion

    /// <summary>
    /// Check Availability of Serach according To Serach ID.
    /// </summary>
    public void CheckSearchAvailable()
    {
        if (Session["SerachID"] != null)
        {
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
            if (st != null)
            {
                if ((st.IsSentAsRequest == null ? false : st.IsSentAsRequest) == true)
                {
                    ViewState["isssent"] = false;
                    divWarnning.InnerHtml = GetKeyResult("YOURBOOKINGISCONVERTINTOREQUEST");// "Your booking is now converted into request.";
                    divWarnning.Attributes.Add("style", "display:block;");
                    divCancelation.Visible = false;
                    lblCancelation.Visible = false;
                }
                else
                {
                    ViewState["isssent"] = true;
                    lblCancelation.Visible = true;
                    divCancelation.Visible = true;
                }
                if (st.CurrentStep == 1)
                {
                    Response.Redirect("~/Agency/BookingStep1.aspx");
                }
                else if (st.CurrentStep == 2)
                {
                    Response.Redirect("~/Agency/BookingStep2.aspx");
                }
                else if (st.CurrentStep == 3)
                {
                    objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                    //Bind Booking Details
                    BindBooking();

                }
                else if (st.CurrentStep == 4)
                {
                    Response.Redirect("~/Agency/BookingStep4.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Agency/SearchBookingRequest.aspx");
            }
        }
        else
        {
            Response.Redirect("~/Agency/SearchBookingRequest.aspx");
        }
    }

    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }

    public void BindBooking()
    {

        lblArivalDate.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
        lblDepartureDate.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
        lblDuration.Text = objBooking.Duration == 1 ? objBooking.Duration + GetKeyResult("DAY") : objBooking.Duration + GetKeyResult("DAY");

        BindHotelDetails(objBooking.HotelID);

        rptMeetingRoom.DataSource = objBooking.MeetingroomList;
        rptMeetingRoom.DataBind();
        BindAccomodation();
        lblSpecialRequest.Text = objBooking.SpecialRequest;
        Calculate(objBooking);
        lblNetTotal.Text = Math.Round((objBooking.TotalBookingPrice - VatCalculation.Sum(a => a.CalculatedPrice)) * CurrencyConvert, 2).ToString("#,##,##0.00");
        rptVatList.DataSource = VatCalculation;
        rptVatList.DataBind();
        lblFinalTotal.Text = Math.Round(objBooking.TotalBookingPrice * CurrencyConvert, 2).ToString("#,##,##0.00");

    }

    public void BindHotelDetails(Int64 Hotelid)
    {
        Hotel objHotelDetails = objHotel.GetHotelDetailsById(Hotelid);
        lblHotelName.Text = objHotelDetails.Name;
        lblHotelAddress.Text = objHotelDetails.HotelAddress;
        lblHotelPhone.Text = objHotelDetails.PhoneExt + " - " + objHotelDetails.PhoneNumber;
        //Currency objUserCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(Session["CurrencyID"]));
        //UserCurrency = objUserCurrency.Currency;
        //CurrencySign = objUserCurrency.CurrencySignature;
        Currency objcurrency = cm.GetCurrencyDetailsByID(objHotelDetails.CurrencyId);
        HotelCurrency = objcurrency.Currency;
        UserCurrency = objcurrency.Currency;
        CurrencySign = objcurrency.CurrencySignature;
        //string currency = CurrencyManager.Currency(HotelCurrency, UserCurrency);
        //CurrencyConvert = Convert.ToDecimal(currency == "N/A" ? "1" : currency);
        CurrencyConvert = 1;//Math.Round(Convert.ToDecimal(currency == "N/A" || currency == "</HTML>" ? "1" : currency), 2);
        CurrentLat = Convert.ToDecimal(objHotelDetails.Latitude);
        CurrentLong = Convert.ToDecimal(objHotelDetails.Longitude);
        if (objHotelDetails.Stars == 1)
        {
            imgHotelStars.ImageUrl = "~/Images/1.png";
        }
        else if (objHotelDetails.Stars == 2)
        {
            imgHotelStars.ImageUrl = "~/Images/2.png";
        }
        else if (objHotelDetails.Stars == 3)
        {
            imgHotelStars.ImageUrl = "~/Images/3.png";
        }
        else if (objHotelDetails.Stars == 4)
        {
            imgHotelStars.ImageUrl = "~/Images/4.png";
        }
        else if (objHotelDetails.Stars == 5)
        {
            imgHotelStars.ImageUrl = "~/Images/5.png";
        }
        else if (objHotelDetails.Stars == 6)
        {
            imgHotelStars.ImageUrl = "~/Images/6.png";
        }
        else if (objHotelDetails.Stars == 7)
        {
            imgHotelStars.ImageUrl = "~/Images/7.png";
        }
        CancelationPolicy c = bm.CheckCancelationPolicy(objHotelDetails);
        if (c != null)
        {
            TimeSpan t = objBooking.ArivalDate.Subtract(DateTime.Now);
            //TimeSpan td = objBooking.ArivalDate.Subtract(DateTime.Now);
            if (t.Days >= c.CancelationLimit)
            {
                
                lblCancelation.Visible = true;
                lblCancelation.Text = GetKeyResult("THISBOOKINGCANBECANCELED") + " <b>" + GetKeyResult("FREE") + "</b> " + GetKeyResult("OFCHANGEUNTIL") + " <b> " + objBooking.ArivalDate.AddDays(Convert.ToInt32(c.CancelationLimit) * -1).ToString("dd MMM yyyy") + "</b>";

                if (Convert.ToBoolean(ViewState["isssent"]))
                {
                    lblCancelation.Visible = true;
                }
                else
                {
                    lblCancelation.Visible = false;
                }
            }
            else
            {
                lblCancelation.Visible = false;
            }
        }
        else
        {
            lblCancelation.Visible = false;
        }
        BindPolicy(Convert.ToInt32(Hotelid));
    }
    public void BindAccomodation()
    {
        if (objBooking.NoAccomodation)
        {
            pnlAccomodation.Visible = false;
        }
        else
        {
            if (objBooking.ManageAccomodationLst.Count > 0)
            {
                List<BedroomManage> distinctNames = (from d in objBooking.ManageAccomodationLst select new BedroomManage { BedroomID = d.BedroomId, bedroomType = d.BedroomType }).ToList();
                List<BedroomManage> disListNew = distinctNames.Distinct(new DistinctItemComparer3()).ToList();
                lblTotalAccomodation.Text = string.Format("{0:#,##,##0.00}", objBooking.AccomodationPriceTotal);
                rptAccomodationDaysManager.DataSource = disListNew;
                rptAccomodationDaysManager.DataBind();
                pnlAccomodation.Visible = true;
                lblNoteBedroom.Text = objBooking.BedroomNote;
            }
            else
            {
                if (string.IsNullOrEmpty(objBooking.BedroomNote))
                {
                    pnlAccomodation.Visible = false;
                }
                else
                {
                    pnlAccomodation.Visible = true;
                    lblNoteBedroom.Text = objBooking.BedroomNote;
                }
            }
        }
    }

    protected void rptVatList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblVatPrice = (Label)e.Item.FindControl("lblVatPrice");
            VatCollection v = e.Item.DataItem as VatCollection;
            if (v != null)
            {
                lblVatPrice.Text = Math.Round(v.CalculatedPrice * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
        }
    }
    public List<ManagePackageItem> CurrentPackageItem
    {
        get;
        set;
    }
    public int CurrentDayTime
    {
        get;
        set;
    }
    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFromTime = (Label)e.Item.FindControl("lblFromTime");
            Label lblToTime = (Label)e.Item.FindControl("lblToTime");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            PackageItems p = e.Item.DataItem as PackageItems;
            PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
            if (p != null)
            {
                ManagePackageItem mpi = CurrentPackageItem.Where(a => a.ItemId == p.Id).FirstOrDefault();
                if (mpi != null)
                {
                    if (pdesc != null)
                    {
                        lblPackageItem.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblPackageItem.Text = p.ItemName;
                    }

                    if (p.ItemName.ToLower().Contains("coffee break(s) morning and/or afternoon"))//|| p.ItemName.ToLower().Contains("morning / afternoon") || p.ItemName.ToLower().Contains("morning/ afternoon") || p.ItemName.ToLower().Contains("morning /afternoon") || p.ItemName.ToLower().Contains("morning / afternoon coffee break")
                    {
                        if (CurrentDayTime == 0 || CurrentDayTime == 1)
                        {
                            lblFromTime.Text = mpi.FromTime;
                            lblFromTime.Visible = true;
                        }
                        else
                        {
                            lblFromTime.Text = "NA";
                            //lblFromTime.Visible = false;
                        }
                        if (CurrentDayTime == 0 || CurrentDayTime == 2)
                        {
                            lblToTime.Text = mpi.ToTime;
                            lblToTime.Visible = true;
                        }
                        else
                        {
                            lblToTime.Text = "NA";
                            //lblToTime.Visible = false;
                        }
                    }
                    else
                    {
                        lblFromTime.Text = mpi.FromTime;
                        lblToTime.Visible = false;
                    }
                    lblQuantity.Text = Convert.ToString(mpi.Quantity);
                }
            }
        }
    }
    #region New Accomodation
    protected void rptAccomodationDaysManager_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater rptDays2 = (Repeater)e.Item.FindControl("rptDays2");
            Literal ltrExtendDays = (Literal)e.Item.FindControl("ltrExtendDays");
            long bedid = (from d in objBooking.ManageAccomodationLst select d.BedroomId).FirstOrDefault();
            List<Accomodation> distinctNames = (from d in objBooking.ManageAccomodationLst where d.BedroomId == bedid select d).ToList();
            rptDays2.DataSource = distinctNames;
            rptDays2.DataBind();
            if (distinctNames.Count < 6)
            {
                ltrExtendDays.Text = "<td colspan='" + (6 - distinctNames.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            HiddenField hdnBedroomId = (HiddenField)e.Item.FindControl("hdnBedroomId");
            Repeater rptPrice = (Repeater)e.Item.FindControl("rptPrice");
            Repeater rptNoOfDays = (Repeater)e.Item.FindControl("rptNoOfDays");
            Repeater rptSingleQuantity = (Repeater)e.Item.FindControl("rptSingleQuantity");
            Repeater rptDoubleQuantity = (Repeater)e.Item.FindControl("rptDoubleQuantity");
            Literal ltrExtendPrice = (Literal)e.Item.FindControl("ltrExtendPrice");
            Literal ltrExtendNoDays = (Literal)e.Item.FindControl("ltrExtendNoDays");
            Literal ltrExtendSQuantity = (Literal)e.Item.FindControl("ltrExtendSQuantity");
            Literal ltrExtendDQuantity = (Literal)e.Item.FindControl("ltrExtendDQuantity");
            Label lblTotalAccomodation = (Label)e.Item.FindControl("lblTotalAccomodation");
            BedroomManage bedm = e.Item.DataItem as BedroomManage;
            lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), bedm.bedroomType);
            hdnBedroomId.Value = bedm.BedroomID.ToString();
            List<Accomodation> lstaccomo = (from d in objBooking.ManageAccomodationLst where d.BedroomId == bedm.BedroomID select d).ToList();
            rptPrice.DataSource = lstaccomo;
            rptPrice.DataBind();
            rptNoOfDays.DataSource = lstaccomo;
            rptNoOfDays.DataBind();
            rptSingleQuantity.DataSource = lstaccomo;
            rptSingleQuantity.DataBind();
            rptDoubleQuantity.DataSource = lstaccomo;
            rptDoubleQuantity.DataBind();
            if (lstaccomo.Count < 6)
            {
                ltrExtendNoDays.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendPrice.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendSQuantity.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendDQuantity.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
            }
            lblTotalAccomodation.Text = String.Format("{0:#,##,#0.00}", Math.Round((lstaccomo.Sum(a => a.QuantityDouble > 0 ? a.QuantityDouble * a.RoomPriceDouble * CurrencyConvert : 0) + lstaccomo.Sum(a => a.QuantitySingle > 0 ? a.QuantitySingle * a.RoomPriceSingle * CurrencyConvert : 0)), 2));
        }
    }

    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblDay.Text = a.DateRequest.ToString("dd/MM/yyyy");
        }
    }

    protected void rptPrice_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPriceDay = (Label)e.Item.FindControl("lblPriceDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblPriceDay.Text = Math.Round(a.RoomPriceSingle * CurrencyConvert, 2).ToString("#,##,##0.00") + "</BR>" + "<span class='currencyClass'></span>&nbsp;" + Math.Round(a.RoomPriceDouble * CurrencyConvert, 2).ToString("#,##,##0.00");
        }
    }

    protected void rptNoOfDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblAvailableRoomDay = (Label)e.Item.FindControl("lblAvailableRoomDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblAvailableRoomDay.Text = a.RoomAvailable.ToString();
        }
    }

    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblQuantitySDay = (Label)e.Item.FindControl("lblQuantitySDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            Accomodation a = e.Item.DataItem as Accomodation;
            if (a != null)
            {
                lblQuantitySDay.Text = a.QuantitySingle.ToString();
                lblQuantitySDay.Attributes.Add("roomtype", Enum.GetName(typeof(BedRoomType), a.BedroomType));
                lblQuantitySDay.Attributes.Add("maxvalue", a.RoomAvailable.ToString());
                lblQuantitySDay.Attributes.Add("dateavailable", a.DateRequest.ToString("dd/MM/yyyy"));
                lblQuantitySDay.Attributes.Add("price", Math.Round(a.RoomPriceSingle * CurrencyConvert, 2).ToString());
                hdnDate.Value = a.DateRequest.ToString();
                hdnPrice.Value = a.RoomPriceSingle.ToString();
            }
        }
    }

    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblQuantityDDay = (Label)e.Item.FindControl("lblQuantityDDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            Accomodation a = e.Item.DataItem as Accomodation;
            if (a != null)
            {
                if (lblQuantityDDay != null)
                {
                    lblQuantityDDay.Text = a.QuantityDouble.ToString();
                    lblQuantityDDay.Attributes.Add("roomtype", Enum.GetName(typeof(BedRoomType), a.BedroomType));
                    lblQuantityDDay.Attributes.Add("maxvalue", a.RoomAvailable.ToString());
                    lblQuantityDDay.Attributes.Add("dateavailable", a.DateRequest.ToString("dd/MM/yyyy"));
                    lblQuantityDDay.Attributes.Add("price", Math.Round(a.RoomPriceDouble * CurrencyConvert, 2).ToString());
                    hdnDate.Value = a.DateRequest.ToString();
                    hdnPrice.Value = a.RoomPriceDouble.ToString();
                }
            }
        }
    }
    #endregion
    protected void rptMeetingRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMR objBookedMr = e.Item.DataItem as BookedMR;
            CurrentMeetingRoomID = objBookedMr.MRId;
            CurrentMeetingRoomConfigureID = objBookedMr.MrConfigId;

            Repeater rptMeetingRoomConfigure = (Repeater)e.Item.FindControl("rptMeetingRoomConfigure");
            rptMeetingRoomConfigure.DataSource = objBookedMr.MrDetails;
            rptMeetingRoomConfigure.DataBind();
        }
    }


    protected void rptMeetingRoomConfigure_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMrConfig objBookedMrConfig = e.Item.DataItem as BookedMrConfig;
            //Bind Meetingroom//
            MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(CurrentMeetingRoomID);
            MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
            MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == CurrentMeetingRoomConfigureID).FirstOrDefault();

            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblMeetingRoomType = (Label)e.Item.FindControl("lblMeetingRoomType");
            Label lblMinMaxCapacity = (Label)e.Item.FindControl("lblMinMaxCapacity");
            Label lblMeetingRoomActualPrice = (Label)e.Item.FindControl("lblMeetingRoomActualPrice");
            Label lblTotalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalMeetingRoomPrice");
            Label lblTotalFinalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalFinalMeetingRoomPrice");
            lblMeetingRoomName.Text = objMeetingroom.Name;
            lblMeetingRoomType.Text = Enum.GetName(typeof(RoomShape), objMrConfig.RoomShapeId);
            lblMinMaxCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
            lblMeetingRoomActualPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice * CurrencyConvert, 2).ToString("#,##,##0.00");
            lblTotalFinalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice * CurrencyConvert, 2).ToString("#,##,##0.00");
            decimal intDiscountMR = objBookedMrConfig.MeetingroomDiscount;
            lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice * CurrencyConvert, 2).ToString("#,##,##0.00");

            //Bind Other details//
            Label lblSelectedDay = (Label)e.Item.FindControl("lblSelectedDay");
            Label lblStart = (Label)e.Item.FindControl("lblStart");
            Label lblEnd = (Label)e.Item.FindControl("lblEnd");
            Label lblNumberOfParticepant = (Label)e.Item.FindControl("lblNumberOfParticepant");
            lblSelectedDay.Text = objBookedMrConfig.SelectedDay.ToString();
            //lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            lblStart.Text = objBookedMrConfig.FromTime;
            lblEnd.Text = objBookedMrConfig.ToTime;
            lblNumberOfParticepant.Text = objBookedMrConfig.NoOfParticepant.ToString();
            CurrentDayTime = objBookedMrConfig.SelectedTime;
            Panel pnlNotIsBreakdown = (Panel)e.Item.FindControl("pnlNotIsBreakdown");
            Panel pnlIsBreakdown = (Panel)e.Item.FindControl("pnlIsBreakdown");
            if (objBookedMrConfig.IsBreakdown == true)
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = true;
                    pnlNotIsBreakdown.Visible = false;
                }
            }
            else
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = false;
                    pnlNotIsBreakdown.Visible = true;
                }
            }
            #region Package and Equipment Collection
            //Bind Package//
            Panel pnlIsPackageSelected = (Panel)e.Item.FindControl("pnlIsPackageSelected");
            Panel pnlBuildMeeting = (Panel)e.Item.FindControl("pnlBuildMeeting");
            HtmlControl divpriceMeetingroom = (HtmlControl)e.Item.FindControl("divpriceMeetingroom");
            HtmlControl divpriceMeetingroom1 = (HtmlControl)e.Item.FindControl("divpriceMeetingroom1");
            HtmlControl divpriceMeetingroom2 = (HtmlControl)e.Item.FindControl("divpriceMeetingroom2");
            HtmlControl divpriceMeetingroom3 = (HtmlControl)e.Item.FindControl("divpriceMeetingroom3");
            HtmlControl divpriceMeetingroom4 = (HtmlControl)e.Item.FindControl("divpriceMeetingroom4");

            if (objBookedMrConfig.PackageID == 0)
            {
                pnlIsPackageSelected.Visible = false;
                if (objBookedMrConfig.BuildManageMRLst.Count > 0)
                {
                    pnlBuildMeeting.Visible = true;
                    Repeater rptBuildMeeting = (Repeater)e.Item.FindControl("rptBuildMeeting");
                    Label lblTotalBuildPackagePrice = (Label)e.Item.FindControl("lblTotalBuildPackagePrice");
                    CurrentPackageType = objBookedMrConfig.SelectedTime;
                    rptBuildMeeting.DataSource = objBookedMrConfig.BuildManageMRLst;
                    rptBuildMeeting.DataBind();
                    lblTotalBuildPackagePrice.Text = Math.Round((objBookedMrConfig.BuildPackagePriceTotal - (objBookedMrConfig.MeetingroomPrice)) * CurrencyConvert, 2).ToString("#,##,##0.00");
                }
                else
                {
                    pnlBuildMeeting.Visible = false;
                }
                divpriceMeetingroom.Style.Add("display", "block");
                divpriceMeetingroom1.Style.Add("display", "block");
                divpriceMeetingroom2.Style.Add("display", "block");
                divpriceMeetingroom3.Style.Add("display", "block");
                divpriceMeetingroom4.Style.Add("display", "block");
            }
            else
            {
                pnlIsPackageSelected.Visible = true;
                pnlBuildMeeting.Visible = false;
                Label lblSelectedPackage = (Label)e.Item.FindControl("lblSelectedPackage");
                Label lblPackageDescription = (Label)e.Item.FindControl("lblPackageDescription");
                Label lblPackagePrice = (Label)e.Item.FindControl("lblPackagePrice");
                Label lblTotalPackagePrice = (Label)e.Item.FindControl("lblTotalPackagePrice");
                lblPackagePrice.Text = Math.Round(objBookedMrConfig.PackagePriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
                Repeater rptPackageItem = (Repeater)e.Item.FindControl("rptPackageItem");

                PackageByHotel p = objHotel.GetPackageDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageByHotelColumn.PackageId).Where(a => a.PackageId == objBookedMrConfig.PackageID).FirstOrDefault();
                if (p != null)
                {
                    CurrentPackageItem = objBookedMrConfig.ManagePackageLst;
                    rptPackageItem.DataSource = objHotel.GetPackageItemDetailsByPackageID(Convert.ToInt64(p.PackageId));
                    rptPackageItem.DataBind();
                    lblSelectedPackage.Text = p.PackageIdSource.PackageName;
                    objHotel.GetDescriptionofPackageByPackage(p.PackageIdSource);
                    PackageMasterDescription des = p.PackageIdSource.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                    if (des == null)
                    {
                        des = p.PackageIdSource.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64("1")).FirstOrDefault();
                    }
                    if (des != null)
                    {
                        lblPackageDescription.Text = des.Description;
                    }
                }
                Panel pnlIsExtra = (Panel)e.Item.FindControl("pnlIsExtra");
                if (objBookedMrConfig.ManageExtrasLst.Count > 0)
                {
                    Repeater rptExtra = (Repeater)e.Item.FindControl("rptExtra");
                    objHp = objHotel.GetIsExtraItemDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageItemsColumn.Id);
                    rptExtra.DataSource = objBookedMrConfig.ManageExtrasLst;
                    rptExtra.DataBind();
                    pnlIsExtra.Visible = true;
                    Label lblExtraPrice = (Label)e.Item.FindControl("lblExtraPrice");
                    lblExtraPrice.Text = Math.Round(objBookedMrConfig.ExtraPriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
                }
                else
                {
                    pnlIsExtra.Visible = false;
                }
                divpriceMeetingroom.Style.Add("display", "none");
                divpriceMeetingroom1.Style.Add("display", "none");
                divpriceMeetingroom2.Style.Add("display", "none");
                divpriceMeetingroom3.Style.Add("display", "none");
                divpriceMeetingroom4.Style.Add("display", "none");

                lblTotalPackagePrice.Text = Math.Round((objBookedMrConfig.PackagePriceTotal + objBookedMrConfig.ExtraPriceTotal) * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
            Panel pnlEquipment = (Panel)e.Item.FindControl("pnlEquipment");
            if (objBookedMrConfig.EquipmentLst.Count > 0)
            {
                pnlEquipment.Visible = true;
                Repeater rptEquipment = (Repeater)e.Item.FindControl("rptEquipment");
                rptEquipment.DataSource = objBookedMrConfig.EquipmentLst;
                rptEquipment.DataBind();
                Label lblTotalEquipmentPrice = (Label)e.Item.FindControl("lblTotalEquipmentPrice");
                lblTotalEquipmentPrice.Text = Math.Round(objBookedMrConfig.EquipmentPriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
            else
            {
                pnlEquipment.Visible = false;
            }

            Panel pnlOthers = (Panel)e.Item.FindControl("pnlOthers");
            if (objBookedMrConfig.BuildOthers.Count > 0)
            {
                pnlOthers.Visible = true;
                Repeater rptOthers = (Repeater)e.Item.FindControl("rptOthers");
                rptOthers.DataSource = objBookedMrConfig.BuildOthers;
                rptOthers.DataBind();
                Label lblOtherTotals = (Label)e.Item.FindControl("lblOtherTotals");
                lblOtherTotals.Text = Math.Round(objBookedMrConfig.OtherPriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
            else
            {
                pnlOthers.Visible = false;
            }
            #endregion
        }
    }
    public TList<PackageItems> objHp
    {
        get;
        set;
    }
    protected void rptExtra_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFrom = (Label)e.Item.FindControl("lblFrom");
            //Label lblTo = (Label)e.Item.FindControl("lblTo");
            Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            Label lblQuntity = (Label)e.Item.FindControl("lblQuntity");
            ManageExtras mngExt = e.Item.DataItem as ManageExtras;
            if (mngExt != null)
            {
                PackageItems p = objHp.Where(a => a.Id == mngExt.ItemId).FirstOrDefault();
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (p != null)
                {
                    if (pdesc != null)
                    {
                        lblPackageItem.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblPackageItem.Text = p.ItemName;
                    }
                    lblFrom.Text = mngExt.FromTime;
                    //lblTo.Text = mngExt.ToTime;
                    lblTotal.Text = Math.Round(mngExt.Quantity * mngExt.ItemPrice, 2).ToString("#,##,##0.00");
                    lblQuntity.Text = mngExt.Quantity.ToString();
                }
            }
            else
            {
                lblQuntity.Text = "0";
            }
        }
    }
    protected void rptBuildMeeting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BuildYourMR objBuildYourMR = e.Item.DataItem as BuildYourMR;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalItemPrice = (Label)e.Item.FindControl("lblTotalItemPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.FoodBeverages).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objBuildYourMR.ItemId).FirstOrDefault();
            if (p != null)
            {
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                if (pdesc != null)
                {
                    lblItemName.Text = pdesc.ItemName;
                    lblItemDescription.Text = pdesc.ItemDescription;
                }
                else
                {
                    lblItemName.Text = p.ItemName;
                    lblItemDescription.Text = "";
                }
                lblQuantity.Text = objBuildYourMR.Quantity.ToString();
                lblTotalItemPrice.Text = Math.Round(objBuildYourMR.ItemPrice * objBuildYourMR.Quantity * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
        }
    }

    protected void rptEquipment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageEquipment objManageEquipment = e.Item.DataItem as ManageEquipment;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Equipment).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                if (pdesc != null)
                {
                    lblItemName.Text = pdesc.ItemName;
                    lblItemDescription.Text = pdesc.ItemDescription;
                }
                else
                {
                    lblItemName.Text = p.ItemName;
                    lblItemDescription.Text = "";
                }
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
        }
    }

    protected void rptOthers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageOtherItems objManageEquipment = e.Item.DataItem as ManageOtherItems;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Others).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                if (pdesc != null)
                {
                    lblItemName.Text = pdesc.ItemName;
                    lblItemDescription.Text = pdesc.ItemDescription;
                }
                else
                {
                    lblItemName.Text = p.ItemName;
                    lblItemDescription.Text = "";
                }
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
        }
    }

    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        st.CurrentStep = 2;
        st.CurrentDay = objBooking.Duration;
        st.SearchObject = TrailManager.XmlSerialize(objBooking);
        st.IsSentAsRequest = false;
        if (bm.SaveSearch(st))
        {
            Response.Redirect("~/Agency/BookingStep2.aspx");
        }
    }

    protected void btnNext_Click1(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        st.CurrentDay = objBooking.Duration;
        st.CurrentStep = 4;
        if (bm.SaveSearch(st))
        {
            Response.Redirect("~/Agency/BookingStep4.aspx");
        }
    }

    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        decimal TotalOtherPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    //Others
                    foreach (ManageOtherItems eqp in objconfig.BuildOthers)
                    {
                        TotalOtherPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                        }
                    }
                    //Manage Package Item
                    decimal restmeetingroomprice = 0;
                    decimal itemtotalprice = 0;
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        itemtotalprice += pck.ItemPrice;
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                        }
                    }
                    restmeetingroomprice = (objconfig.PackagePricePerPerson - itemtotalprice) * objconfig.NoOfParticepant;
                    if (PackageSelected)
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = restmeetingroomprice * objconfig.MeetingroomVAT / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += restmeetingroomprice * objconfig.MeetingroomVAT / 100;
                        }
                    }
                    else
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                        }
                    }
                }
            }

        }
    }
    #endregion

    /// <summary>
    /// Bind cancellation policy as per country
    /// </summary>
    CancellationPolicy objCancellation = new CancellationPolicy();
    HotelManager objHotelManager = new HotelManager();
    public void BindPolicy(Int32 HotelID)
    {
        long? PolicyID = 0;
        PolicyHotelMapping objMapping = objCancellation.GetPolicyByHotelID(Convert.ToInt32(HotelID)).FirstOrDefault();
        if (objMapping != null)
        {
            PolicyID = objMapping.PolicyId;
        }
        else
        {
            int countryID = Convert.ToInt32(objHotelManager.GetHotelDetailsById(Convert.ToInt32(HotelID)).CountryId);
            CancelationPolicy objPolicy = objCancellation.GetPolicyByCountry(countryID).Find(a => a.DefaultCountry == true);
            PolicyID = objPolicy.Id;
        }

        CancelationPolicy GetPolicyByID = objCancellation.GetPolicyByID(Convert.ToInt32(PolicyID));
        if (GetPolicyByID != null)
        {
            int languageID = Convert.ToInt32(objCancellation.GetLanguage().Find(a => a.Name == "English").Id);
            TList<CancelationPolicyLanguage> objCancellationPolicy = objCancellation.GetPolicySectionsByPolicyID(Convert.ToInt32(PolicyID));
            CancelationPolicyLanguage objLanguage = objCancellationPolicy.Find(a => a.LanguageId == languageID);
            lblName.Text = GetPolicyByID.PolicyName;
            lblCalcellationLimit.Text = GetPolicyByID.CancelationLimit.ToString();
            lblTimeFrameMax1.Text = GetPolicyByID.TimeFrameMax1.ToString();
            lblTimeFrameMax2.Text = GetPolicyByID.TimeFrameMax2.ToString();
            lblTimeFrameMax3.Text = GetPolicyByID.TimeFrameMax3.ToString();
            lblTimeFrameMax4.Text = GetPolicyByID.TimeFramMax4.ToString();
            lblTimeFrameMin1.Text = GetPolicyByID.TimeFrameMin1.ToString();
            lblTimeFrameMin2.Text = GetPolicyByID.TimeFrameMin2.ToString();
            lblTimeFrameMin3.Text = GetPolicyByID.TimeFrameMin3.ToString();
            lblTimeFrameMin4.Text = GetPolicyByID.TimeFrameMin4.ToString();

            lblSection1.Text = objLanguage.PolicySection1;
            lblSection2.Text = objLanguage.PolicySection2;
            lblSection3.Text = objLanguage.PolicySection3;
            lblSection4.Text = objLanguage.PolicySection4;


        }
        else
        {
            // divpolicy.Style.Add("display", "none");
            //divMessage.Style.Add("display", "block");
        }

    }
}
class DistinctItemComparer3 : IEqualityComparer<BedroomManage>
{

    public bool Equals(BedroomManage x, BedroomManage y)
    {
        return x.BedroomID == y.BedroomID &&
            x.bedroomType == y.bedroomType;
    }

    public int GetHashCode(BedroomManage obj)
    {
        return obj.BedroomID.GetHashCode() ^
            obj.bedroomType.GetHashCode();
    }
}