﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.Web.UI.DataVisualization.Charting;

public partial class Agency_Statistics : System.Web.UI.Page
{
    #region Variables and properties
   
    Viewstatistics objViewstatistics = new Viewstatistics();
   
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUserID"] == null)
        {
            Response.Redirect("~/login.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
        {
            //Session check               
            if (Session["CurrentAgencyUserID"] == null)
            {
                Response.Redirect("~/login.aspx", false);
                return;
            }
            fillagents();
            //objViewstatistics.HotelId = (Session["CurrentHotelID"].ToString());
            //Session check
            Users objUsers = (Users)Session["CurrentAgencyUser"];
            txtFromdate.Text = System.DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");
            txtTodate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            //lblBooking.Text = "Statistics Booking made by :" + objUsers.FirstName + " " + objUsers.LastName;
            //lblhead.Text = "Statistics Request made by :" + objUsers.FirstName + " " + objUsers.LastName;
            //  Bindlist(txtFromdate.Text, txtTodate.Text, "days");
            divmessage.Style.Add("display", "none");

        }
        else
        {
            if (string.IsNullOrEmpty(txtFromdate.Text))
            {
                
                txtFromdate.Text = System.DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");
                txtTodate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
               
                divmessage.Style.Add("display", "none");
            }
        
        
        }
   
    }
    #region fliter by Days
    /// <summary>
    /// method to fliter by Days     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkDays_Click(object sender, EventArgs e)
    {

        ViewState["SelectedBtn"] = "days";
        ViewState["Selectedtype"] = rdbBooking.SelectedValue;
        fillgridwithagents();
    }
    #endregion
    #region fliter by Month
    /// <summary>
    /// method to fliter by Month     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkMonths_Click(object sender, EventArgs e)
    {

        ViewState["SelectedBtn"] = "months";
        ViewState["Selectedtype"] = rdbBooking.SelectedValue;
        fillgridwithagents();

    }
    #endregion

    #region fliter by Year
    /// <summary>
    /// method to fliter by Year     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkYear_Click(object sender, EventArgs e)
    {
        ViewState["SelectedBtn"] = "years";
        ViewState["Selectedtype"] = rdbBooking.SelectedValue;
        fillgridwithagents();

    }
    #endregion


    public void fillagents()
    {
     var dtfillagents=   objViewstatistics.GetAllAgencyUser(Convert.ToInt32(Session["CurrentAgencyUserID"].ToString()));

     chkAgency.DataSource = dtfillagents;
     chkAgency.DataValueField = "UserId";
     chkAgency.DataTextField = "FirstName";
     chkAgency.DataBind();
   //  chkall.Checked = true;
     //chkAgency.Attributes.Add("disabled", false);
    // chkAgency.Items.Insert(0,new ListItem("Select All","0"));

     //chkAgency.SelectedIndex = 0;
    }

    public void fillgridwithagents()
    {
        string fetchagency = string.Empty;

        if(chkall.Checked)
        {
        foreach (ListItem li in chkAgency.Items)
        {
                if (!string.IsNullOrEmpty(fetchagency))
                {
                    fetchagency += "," + li.Value.ToString();
                }
                else
                {
                    fetchagency = li.Value.ToString();
                }
            }
        }
            else
            {
                foreach (ListItem li in chkAgency.Items)
                {
             
                if (li.Selected)
                {
                    if (!string.IsNullOrEmpty(fetchagency))
                    {
                        fetchagency += "," + li.Value.ToString();
                    }
                    else
                    {
                        fetchagency = li.Value.ToString();
                    }
                }
                }
            
        }  
      


        rptBooking.DataSource = objViewstatistics.GetAllAgencyUser(fetchagency);
        rptBooking.DataBind();

    }
    
    protected void rptBooking_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Users objUsers = (Users)Session["CurrentAgencyUser"];
        

     
        // string quertype = "1";// Convert.ToString(Request.QueryString["type"]);
        DateTime fromdate = new DateTime(Convert.ToInt32(txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
        DateTime todate = new DateTime(Convert.ToInt32(txtTodate.Text.Split('/')[2]), Convert.ToInt32(txtTodate.Text.Split('/')[1]), Convert.ToInt32(txtTodate.Text.Split('/')[0]));


        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblusername = (Label)e.Item.FindControl("lblusername");
            Label lblBooking = (Label)e.Item.FindControl("lblBooking");
            Label lblhead = (Label)e.Item.FindControl("lblhead");
            DataList dtlstConversion = (DataList)e.Item.FindControl("dtlstConversion");
            DataList DtlReq = (DataList)e.Item.FindControl("DtlReq");

            if (ViewState["Selectedtype"].ToString() == "0")
            {
                dtlstConversion.Visible = true;
                lblhead.Visible = false;
                lblBooking.Visible = true;
                DtlReq.Visible = false;
            }
            else if (ViewState["Selectedtype"].ToString() == "1")
            {
                dtlstConversion.Visible = false;
                lblhead.Visible = true;
                lblBooking.Visible = false;
                DtlReq.Visible = true;
            }
            else
            {
                dtlstConversion.Visible = true;
                lblhead.Visible = true;
                lblBooking.Visible = true ;
                DtlReq.Visible = true;
            }
            lblBooking.Text = "Statistics Booking made by :" + lblusername.Text;// objUsers.FirstName + " " + objUsers.LastName;
            lblhead.Text = "Statistics Request made by :" + lblusername.Text;//objUsers.FirstName + " " + objUsers.LastName;

            //int user = Convert.ToInt32(lblusername.ToolTip);
            if (ViewState["SelectedBtn"].ToString() == "days")
            {
                //Bindlist(txtFromdate.Text, txtTodate.Text, "days", Convert.ToInt32(lblusername.ToolTip));
                lnkMonths.Attributes.Add("class", "btn");
                lnkDays.Attributes.Add("class", "btn-active");
                lnkYear.Attributes.Add("class", "btn");
                //chrtReq.ChartAreas[0].AxisY.Title = " No of Request ";
                var datasourcedays = objViewstatistics.Getagencyuser(fromdate, todate, "1", Convert.ToInt32(lblusername.ToolTip));
                dtlstConversion.DataSource = datasourcedays;
                dtlstConversion.DataBind();
                DtlReq.DataSource = datasourcedays;
                DtlReq.DataBind();
                //DtlReqcon.DataSource = datasourcedays;
                //DtlReqcon.DataBind();

            }
            else if (ViewState["SelectedBtn"].ToString() == "months")
            {
              //  Bindlist(txtFromdate.Text, txtTodate.Text, "months", Convert.ToInt32(lblusername.ToolTip));

                lnkMonths.Attributes.Add("class", "btn-active");
                lnkDays.Attributes.Add("class", "btn");
                lnkYear.Attributes.Add("class", "btn");

                var datasourcedays = objViewstatistics.Getagencyuser(fromdate, todate, "2", Convert.ToInt32(lblusername.ToolTip));
                dtlstConversion.DataSource = datasourcedays;
                dtlstConversion.DataBind();
                DtlReq.DataSource = datasourcedays;
                DtlReq.DataBind();
            }
            else if (ViewState["SelectedBtn"].ToString() == "years")
            {
                lnkYear.Attributes.Add("class", "btn-active");
                lnkMonths.Attributes.Add("class", "btn");
                lnkDays.Attributes.Add("class", "btn");

                var datasourcedays = objViewstatistics.Getagencyuser(fromdate, todate, "3", Convert.ToInt32(lblusername.ToolTip));
                dtlstConversion.DataSource = datasourcedays;
                dtlstConversion.DataBind();
                DtlReq.DataSource = datasourcedays;
                DtlReq.DataBind();
            }




           
        }
    }

    protected void lnkClear_Click(object sender, EventArgs e)
    {
        Response.Redirect("StatisticsMaster.aspx");
       
    
    }

}