﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/AgencyMaster.master" AutoEventWireup="true" CodeFile="SendRfp.aspx.cs" Inherits="Agency_SendRfp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ctnTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ctnMain" runat="Server">
    <div id="divmessage" runat="server" class="error">
    </div>
    <div class="wrapper">
		<div class="innerwrapper">
			<div class="main" id="divMain" runat="server">
				
					<fieldset>
						<div class="line">
							<asp:Label ID="lblAddress" CssClass="textbox" runat="server">Event / RFP Name</asp:Label>
							<asp:TextBox ID="txtEventRfp" runat="server" TextMode="MultiLine" 
                                    CssClass="textbox" oncopy="return false" onpaste="return false"></asp:TextBox>
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="Label1" runat="server" CssClass="textbox" >Response due date</asp:Label>
							<asp:TextBox ID="txtRspduedate" runat="server" Text="dd/mm/yy" Width="80px" CssClass="textbox"></asp:TextBox>
                                <asp:HiddenField ID="hdntodate" runat="server" />
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtRspduedate"
                                    PopupButtonID="calTo" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo1">
                                </asp:CalendarExtender>
                                <asp:Image ID="calTo" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="Label2" runat="server" CssClass="textbox">Decision date</asp:Label>
							<asp:TextBox ID="txtdecisiondate" runat="server" Text="dd/mm/yy" Width="80px" CssClass="textbox"></asp:TextBox>
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtdecisiondate"
                                    PopupButtonID="calTo1" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo2">
                                </asp:CalendarExtender>
                                <asp:Image ID="calTo1" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="lblCountry" CssClass="textbox" runat="server"><%= GetKeyResult("COUNTRY")%></asp:Label>
							<asp:DropDownList ID="CountryDDL" Width="165px" CssClass="textbox" runat="server">
                                    </asp:DropDownList>
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="lblCity" CssClass="textbox" runat="server"><%= GetKeyResult("CITY")%>/Region</asp:Label>
							<asp:TextBox ID="txtCity" runat="server" CssClass="textbox" MaxLength="100"></asp:TextBox>
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="Label3" runat="server" CssClass="textbox">Type of venue</asp:Label>
							<asp:TextBox ID="txtvenuetype" runat="server" CssClass="textbox" MaxLength="100"></asp:TextBox><br />
                                    <span style=" margin-left:413px">eg: hotel,conference centre,luxury</span>
							<br clear="all" />
						</div>
					</fieldset>
					<fieldset>
						<div class="line">
							<asp:Label ID="Label11" runat="server" CssClass="textbox">Do you have any venue(s) in mind?</asp:Label>
							<asp:TextBox ID="txtnamevenues" runat="server" CssClass="textbox" ></asp:TextBox>
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="Label12" runat="server"  CssClass="textbox">Event type</asp:Label>
							<asp:DropDownList ID="drpevent" Width="165px" runat="server" CssClass="textbox">
                                        <asp:ListItem Value="0">Board meeting</asp:ListItem>
                                        <asp:ListItem Value="1">Corporate event</asp:ListItem>
                                        <asp:ListItem Value="2">Incentive travel</asp:ListItem>
                                        <asp:ListItem Value="3">Product launch</asp:ListItem>
                                        <asp:ListItem Value="4"> Team building</asp:ListItem>
                                        <asp:ListItem Value="5"> Other</asp:ListItem>
                                    </asp:DropDownList>
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="Label5" runat="server" CssClass="textbox">Total Attendees</asp:Label>
							<asp:TextBox ID="txttotalattandes" runat="server" CssClass="textbox"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="filteredExtender3" runat="server" TargetControlID="txttotalattandes"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
							<br clear="all" />
						</div>
						<div class="line">						
							<asp:Label ID="Label6" runat="server" CssClass="textbox">Event Start date</asp:Label>
							<asp:TextBox ID="txteventstartdate" runat="server" Text="dd/mm/yy" Width="80px" CssClass="textbox"></asp:TextBox>
                                <asp:HiddenField ID="HiddenField2" runat="server" />
                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txteventstartdate"
                                    PopupButtonID="calTo2" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo4">
                                </asp:CalendarExtender>
                                <asp:Image ID="calTo2" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="Label7" runat="server" CssClass="textbox">Event End date</asp:Label>
							<asp:TextBox ID="txteventenddate" runat="server"  Width="80px" Text="dd/mm/yy" CssClass="textbox"></asp:TextBox>
                                <asp:HiddenField ID="HiddenField3" runat="server" />
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txteventenddate"
                                    PopupButtonID="calTo3" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo3">
                                </asp:CalendarExtender>
                                <asp:Image ID="calTo3" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
							<br clear="all" />
						</div>
						<div class="line">
							<asp:Label ID="Label9" runat="server" CssClass="textbox">Are your dates flexible</asp:Label>
                            
							<asp:RadioButtonList ID="rbtcientvenue" runat="server" RepeatDirection="Horizontal" CssClass="textboxradio" >
                                    <asp:ListItem Text="Yes" Value="2" Selected="True" />
                                    <asp:ListItem Text="No" Value="1" />
                                </asp:RadioButtonList>
							<br clear="all" />
                            
						</div>
						<div class="line">
							<asp:Label ID="Label10" runat="server" CssClass="textbox">Do you require bedrooms?</asp:Label>
							<asp:RadioButtonList ID="rbtbedroom" runat="server" CssClass="textboxradio" AutoPostBack="true" RepeatDirection="Horizontal"
                                    OnSelectedIndexChanged="rbtbedroom_SelectedIndexChanged">
                                    <asp:ListItem Text="Yes" Value="2" Selected="True" />
                                    <asp:ListItem Text="No" Value="1" />
                                </asp:RadioButtonList>
							<br clear="all" />
						</div>
					</fieldset>
					<fieldset>
                    <div id="divbedroom" runat="server">
						<div class="line">
                        
							<asp:Label ID="Label4" runat="server" CssClass="textbox">If Yes, How many Bedrooms?</asp:Label>
							<asp:TextBox ID="txtbedroomtext" runat="server" CssClass="textbox" MaxLength="100"  ></asp:TextBox>
                           
                              <%--  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtbedroomtext"
                                    FilterType="Numbers">
                                </asp:FilteredTextBoxExtender>--%>
							<br clear="all" />
                             </div>
						</div>
						<div class="line">
							<asp:Label ID="Label8" runat="server" CssClass="textbox">Additional information</asp:Label>
							<asp:TextBox ID="txtaddinfo" runat="server" TextMode="MultiLine" 
                                    CssClass="textbox" oncopy="return false" onpaste="return false" Height="90px" ></asp:TextBox>
                                    <br clear="all" />
						</div>
						
					</fieldset>					
					<input type="hidden" name="postback" value="postback" />
					<div class="buttonbar">
                        <asp:LinkButton ID="lbtSave" CssClass="submit" runat="server"
                OnClick="lbtSave_Click" OnClientClick="return DataValidation()">Send RFP</asp:LinkButton>
					</div>
			</div>
			<div class="push"></div>
		</div>
	</div>
    
    <script type="text/javascript" language="javascript">
        function dateChangeTo1(s, e) {
            var todatechange = jQuery("#<%= txtRspduedate.ClientID %>").val();
            jQuery("#<%= txtRspduedate.ClientID %>").val(todatechange);

        }
        function dateChangeTo2(s, e) {

            var todatechange1 = jQuery("#<%= txtdecisiondate.ClientID %>").val();
            jQuery("#<%= txtdecisiondate.ClientID %>").val(todatechange1);

        }
        function dateChangeTo3(s, e) {

            var todatechange3 = jQuery("#<%= txteventenddate.ClientID %>").val();
            jQuery("#<%= txteventenddate.ClientID %>").val(todatechange3);

        }
        function dateChangeTo4(s, e) {

            var todatechange4 = jQuery("#<%= txteventstartdate.ClientID %>").val();
            jQuery("#<%= txteventstartdate.ClientID %>").val(todatechange4);
        }

                

    
    </script>
    <script type="text/javascript" language="javascript">
        function dateChangeTo1(s, e) {
            var todatechange = jQuery("#<%= txtRspduedate.ClientID %>").val();
            jQuery("#<%= txtRspduedate.ClientID %>").val(todatechange);

        }
        function dateChangeTo2(s, e) {

            var todatechange1 = jQuery("#<%= txtdecisiondate.ClientID %>").val();
            jQuery("#<%= txtdecisiondate.ClientID %>").val(todatechange1);

        }
        function dateChangeTo3(s, e) {

            var todatechange3 = jQuery("#<%= txteventenddate.ClientID %>").val();
            jQuery("#<%= txteventenddate.ClientID %>").val(todatechange3);

        }
        function dateChangeTo4(s, e) {

            var todatechange4 = jQuery("#<%= txteventstartdate.ClientID %>").val();
            jQuery("#<%= txteventstartdate.ClientID %>").val(todatechange4);
        }

        function DataValidation() {
            var fromdatechange = jQuery("#<%= txteventstartdate.ClientID %>").val();
            var todatechange = jQuery("#<%= txteventenddate.ClientID %>").val();
            var todayArr = todatechange.split('/');
            var formdayArr = fromdatechange.split('/');
            var fromdatecheck = new Date();
            var todatecheck = new Date();

            fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
            todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0]);
            if (fromdatecheck > todatecheck) {
                alert("Event From date must be less than Event End date.");
                return false;
            }
        }

    
</script>
<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#<%= txtRspduedate.ClientID %>").attr("disabled", true);
        jQuery("#<%= txtdecisiondate.ClientID %>").attr("disabled", true);
        jQuery("#<%= txteventenddate.ClientID %>").attr("disabled", true);
        jQuery("#<%= txteventstartdate.ClientID %>").attr("disabled", true);
        jQuery("#<%= lbtSave.ClientID %>").bind("click", function () {
            Query("#<%= txtRspduedate.ClientID %>").attr("disabled", false);
            jQuery("#<%= txtdecisiondate.ClientID %>").attr("disabled", false);
            jQuery("#<%= txteventenddate.ClientID %>").attr("disabled", false);
            jQuery("#<%= txteventstartdate.ClientID %>").attr("disabled", false);
        });
    });
     
</script>
</asp:Content>

