﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/AgencyMaster.master" AutoEventWireup="true"
    CodeFile="Users.aspx.cs" Inherits="Agency_Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctnTop" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ctnMain" runat="Server">
    <asp:HiddenField ID="hdnAgentId" runat="server" Value="0" />
    <table width="100%" border="0" cellspacing="0">
        <tr>
            <td>
                <div id="divmsg" runat="server" class="error" style="display: none">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%;" style="background: #ffffff; border: 1px #A3D4F7 solid;">
                    <tr>
                        <td>
                            <div style="margin-right: 10px; float: left">
                                <div class="n-btn1">
                                    <asp:LinkButton ID="lnkButtonAddnew" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnkButtonAddnew_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Add new</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div style="margin-right: 10px; float: left;" runat="server" id="ModifyButton">
                                <div class="n-btn1">
                                    <asp:LinkButton ID="lnkButtonModify" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnkButtonModify_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Modify</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div style="margin-right: 10px; float: left;" runat="server" id="DeleteButton">
                                <div class="n-btn1" style="margin-right: 10px;">
                                    <asp:LinkButton ID="lnkButtonDelete" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnkButtonDelete_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Delete</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </td>
                        <td align="right">
                            <div style="float: right;" id="pageing">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdViewAgentUser" runat="server" Width="100%" CellPadding="8" border="0"
                    HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                    AutoGenerateColumns="False" GridLines="None" CellSpacing="1" BackColor="#A3D4F7"
                    DataKeyNames="UserId" AllowPaging="true" PageSize="10" 
                    OnRowDataBound="grdViewAgentUser_RowDataBound" 
                    onpageindexchanging="grdViewAgentUser_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="" ItemStyle-Width="1%">
                            <ItemTemplate>
                                <asp:CheckBox ID="rbselect" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:CheckBox ID="rbselect" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="UserId" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUserID" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblUserID" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email" ItemStyle-Width="30%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IsActive" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblIsActive" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblIsActive" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Link with" ItemStyle-Width="29%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblLinkWith" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblLinkWith" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <tr>
                            <td colspan="4" align="center">
                                <b>No record found</b>
                            </td>
                        </tr>
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <div class="superadmin-example-mainbody" id="DivAddUpdateAgentUser" runat="server"
        visible="false">
        <div id="divmessage" runat="server" class="error" style="display: none">
        </div>
        <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
            <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                <h2>
                    <asp:Label ID="lblHeader" runat="server" Text="Add new user"></asp:Label></h2>
            </div>
        </div>
        <table style="margin-left: -1px;" width="100%" bgcolor="#92bddd" cellspacing="1"
            cellpadding="0">
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>UserID</strong>
                </td>
                <td>
                    <asp:Label ID="lblUserID" runat="server" TabIndex="1" CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>First Name</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="25" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Last Name:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="25" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Email</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="200" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Re-type Email:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtReEmail" runat="server" MaxLength="200" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Password</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="200" TabIndex="1" Width="200px"
                        CssClass="txtpadding" TextMode="Password" />
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Re-type Password</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtrePassword" runat="server" MaxLength="200" TabIndex="1" Width="200px"
                        CssClass="txtpadding" TextMode="Password" />
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Link with:</strong>
                </td>
                <td>
                    <asp:Label ID="lblNoUser" runat="server" Text="No user exist" Font-Bold="true" CssClass="txtpadding"></asp:Label>
                    <asp:CheckBoxList ID="checkboxlistAgent" runat="server" Style="margin-left: 10px;">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Is Active</strong>
                </td>
                <td>
                    <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="txtpadding" />
                </td>
            </tr>
        </table>
        <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
            padding-bottom: 10px;">
            <div class="button_section">
                <asp:LinkButton ID="lnkSave" runat="server" CssClass="select" OnClick="lnkSave_Click">Save</asp:LinkButton>
                &nbsp;or&nbsp;
                <asp:LinkButton ID="lnkCancel" runat="server" CssClass="cancelpop" OnClick="lnkCancel_Click">Cancel</asp:LinkButton>
            </div>
        </div>
    </div>
    <script type="text/javascript" language="javascript">

        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });

        jQuery(document).ready(function () {
            jQuery("#<%= lnkButtonModify.ClientID %>").bind("click", function () {
                if (jQuery("#ctnMain_grdViewAgentUser").find("input:checkbox:checked").length <= 0) {
                    alert('Please select user to modify.');
                    return false;
                }
            });

            jQuery("#<%= lnkButtonDelete.ClientID %>").bind("click", function () {
                if (jQuery("#ctnMain_grdViewAgentUser").find("input:checkbox:checked").length <= 0) {
                    alert('Please select user to delete.');
                    return false;
                }
                else {
                    return confirm("Are you sure you want to delete it?");
                }

            });
        });


        jQuery("#<%= lnkSave.ClientID %>").bind("click", function () {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var firstName = jQuery("#<%= txtFirstName.ClientID %>").val();
            if (firstName.length <= 0 || firstName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter first name';
                isvalid = false;
            }

            var lastName = jQuery("#<%= txtLastName.ClientID %>").val();
            if (lastName.length <= 0 || lastName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter last name';
                isvalid = false;
            }


            var emailp1 = jQuery("#<%= txtEmail.ClientID %>").val();
            if (emailp1.length <= 0 || emailp1 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter email';
                isvalid = false;
            }

            else {
                if (!validateEmail(emailp1)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += 'Enter valid email';
                    isvalid = false;
                }
            }

            var email = jQuery("#<%=txtEmail.ClientID %>").val();
            var reemail = jQuery("#<%=txtReEmail.ClientID %>").val();
            if (email != reemail) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Re-type Email does not match.';
                isvalid = false;
            }

            var password = jQuery("#<%= txtPassword.ClientID %>").val();
            if (password.length <= 0 || password == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter Password';
                isvalid = false;
            }

            var password = jQuery("#<%= txtPassword.ClientID %>").val();
            if (password.length <= 5) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Password must be at least 6 characters';
                isvalid = false;
            }


            var password = jQuery("#<%=txtPassword.ClientID %>").val();
            var repassword = jQuery("#<%=txtrePassword.ClientID %>").val();
            if (password != repassword) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Re-type password does not match';
                isvalid = false;
            }

            if (!isvalid) {
                jQuery("#<%= divmessage.ClientID %>").show();
                jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                return false;
            }
            jQuery("#<%= divmessage.ClientID %>").html("");
            jQuery("#<%= divmessage.ClientID %>").hide();
            jQuery("#Loding_overlay").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#divJoinToday").show();
            jQuery("#divJoinToday-overlay").show();
        });

        function CheckOtherIsCheckedByGVID(spanChk) {
            var IsChecked = spanChk.checked;
            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById('ctnMain_grdViewAgentUser');
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {

                if (items[i].id != CurrentRdbID && items[i].type == "checkbox") {

                    if (items[i].checked) {

                        items[i].checked = false;

                        items[i].parentElement.parentElement.style.backgroundColor = '';

                        //items[i].parentElement.parentElement.style.color = 'black';
                    }
                    else {

                        items[i].parentElement.parentElement.style.backgroundColor = '';
                    }
                }
                else {

                    items[i].parentElement.parentElement.style.backgroundColor = '#feff99';
                }
            }
            if (IsChecked != true) {
                spanChk.parentElement.parentElement.style.backgroundColor = '';
            }
        }
    </script>
    <div id="divDeletePopUp" style="display: none">
        <div id="divAgentDelete-overlay">
        </div>
        <div id="divAgentDelete">
            <div class="popup-top">
            </div>
            <div class="popup-mid">
                <div class="popup-mid-inner">
                    <div class="error" id="errorPopup" runat="server">
                    </div>
                    <table cellspacing="10">
                        <tr>
                            <td>
                                <b>Choose user to transfer data:</b>
                            </td>
                            <td>
                                <asp:CheckBoxList ID="CheckBoxListLinkWith" runat="server">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <div class="subscribe-btn">
                        <div class="button_section">
                            <asp:LinkButton ID="btnConfirm" runat="server" CssClass="select" OnClick="btnConfirm_Click"
                                OnClientClick="return LinkValidate();">Confirm</asp:LinkButton>
                            &nbsp;or&nbsp;
                            <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return CloseDeletePopUp();">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup-bottom">
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function CloseDeletePopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('divAgentDelete').style.display = 'none';
            document.getElementById('divAgentDelete-overlay').style.display = 'none';
            jQuery("#divDeletePopUp").hide();
            jQuery("#<%=CheckBoxListLinkWith.ClientID %>").find("input:checkbox:checked").each(function () {
                jQuery("#<%=CheckBoxListLinkWith.ClientID %>").find("input:checkbox:checked").attr("checked", false);
            });
            return false;
        }

        function OpenDeletePopUp() {
            if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            jQuery("#<%= errorPopup.ClientID %>").html("");
            jQuery("#<%= errorPopup.ClientID %>").hide();
            jQuery("#divDeletePopUp").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#divAgentDelete").show();
            jQuery("#divAgentDelete-overlay").show();
            return false;
        }

        function LinkValidate() {
            if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var ischekced = "0";

            jQuery("#<%=CheckBoxListLinkWith.ClientID %>").find("input:checkbox:checked").each(function () {

                ischekced = "1";
            });
            if (ischekced == "0") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Select at least one user";
                isvalid = false;
            }

            if (!isvalid) {
                jQuery("#<%= errorPopup.ClientID %>").show();
                jQuery("#<%= errorPopup.ClientID %>").html(errormessage);
                return false;
            }
            jQuery("#<%= errorPopup.ClientID %>").html("");
            jQuery("#<%= errorPopup.ClientID %>").hide();
            jQuery("#Loding_overlay").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#divAgentDelete").show();
            jQuery("#divAgentDelete-overlay").show();
        }

        function validateEmail(txtEmail) {
            var a = txtEmail;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        }

        function SetFocusBottom(val) {

            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
        }

        function ConfirmOnDelete() {
            return confirm("Are you sure you want to delete it?");
        }
    </script>
</asp:Content>
