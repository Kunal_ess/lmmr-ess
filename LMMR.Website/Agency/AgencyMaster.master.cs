﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;

public partial class Agency_AgencyMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CurrentAgencyUserID"] != null)
            {
                Users objUsers = (Users)Session["CurrentAgencyUser"];
                if (objUsers.Usertype == (int)Usertype.Agency || objUsers.Usertype == (int)Usertype.agencyuser)
                {
                    lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                    objUsers.LastLogin = DateTime.Now;
                    lstLoginTime.Text = DateTime.Now.ToLongDateString();
                    if (objUsers.Usertype == (int)Usertype.Agency)
                    {
                        lnkChangeUser.Visible = true;
                        switchUser();
                    }
                    else
                    {
                        lnkChangeUser.Visible = false;
                    }
                }
            }
            else
            {
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
        }
    }

    /// <summary>
    /// This envet used for logout for agency.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        if (Session["masterInput"] == null || Session["masterInput"] != null)
        {
            Session.Remove("CurrentAgencyUserID");
            Session.Remove("CurrentAgency");
            Session.Remove("SerachID");
            Session.Remove("RequestID");
            Session.Remove("masterInput");
            Session.Remove("CurrencyID");
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
    }

    void switchUser()
    {
        Users u = Session["CurrentAgencyUser"] as Users;
        if (u != null)
        {
            if (!u.WorkAsAgentUser)
            {
                lnkChangeUser.Text = "Work as user";
            }
            else
            {
                lnkChangeUser.Text = "Work as admin";
            }
        }
    }
    protected void lnkChangeUser_OnClick(object sender, EventArgs e)
    {
        Users u = Session["CurrentAgencyUser"] as Users;
        if (u != null)
        {
            if (!u.WorkAsAgentUser)
            {
                u.WorkAsAgentUser = true;
                lnkChangeUser.Text = "Work as admin";
                Response.Redirect("ControlPanelUser.aspx");
            }
            else
            {
                u.WorkAsAgentUser = false;
                lnkChangeUser.Text = "Work as user";
                Response.Redirect("ControlPanel.aspx");
            }
            Session["CurrentAgentUser"] = u;
        }
    }
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
}
