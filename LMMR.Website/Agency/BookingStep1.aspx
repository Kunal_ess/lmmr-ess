﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/Main.master" AutoEventWireup="true" CodeFile="BookingStep1.aspx.cs" Inherits="Agency_BookingStep1" %>
<%@ MasterType VirtualPath="~/Agency/Main.master" %> 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../UserControl/Agency/LeftSearchPanel.ascx" tagname="LeftSearchPanel" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntMainBody" Runat="Server">
    <!--booking-heading START HERE-->
    <div class="booking-stepbody">
        <div class="booking-step1">
            <span><%= GetKeyResult("STEP1")%>.</span> <%= GetKeyResult("STEP1SELECTMEETINGROOM")%>
        </div>
        <div class="booking-step2">
            <span><%= GetKeyResult("STEP2")%>.</span> <%= GetKeyResult("STEP2EXTRAOPTIONSBEDROOMS")%>
        </div>
        <div class="booking-step3">
            <span><%= GetKeyResult("STEP3")%>.</span> <%= GetKeyResult("STEP3SENDIT")%>
        </div>
        <div class="booking-step4">
            <span><%= GetKeyResult("STEPFINILIZED")%></span>
        </div>
    </div>
    <!--booking-heading ENDS HERE-->
    <!--booking-heading START HERE-->
    <div class="booking-heading">
        <span><%= GetKeyResult("YOURBOOKING")%>:</span> <%= GetKeyResult("SELECTEDMEETINGROOMS")%>
    </div>
    <!--booking-heading ENDS HERE-->
    <!--booking date body START HERE-->
    <div class="booking-date-body">
        <div class="arrival-date">
            <span><%= GetKeyResult("ARRIVALDATE")%>:</span><asp:Label ID="lblArrival" runat="server"></asp:Label>
        </div>
        <div class="departure-date">
            <span><%= GetKeyResult("DEPARTUREDATE")%>:</span><asp:Label ID="lblDeparture" runat="server"></asp:Label>
        </div>
        <div class="duration-day">
            <span><%= GetKeyResult("DURATION")%>:</span> <span class="red"><asp:Label ID="lblDuration" runat="server"></asp:Label></span>
        </div>
    </div>
    <!--booking date body ENDS HERE-->
    <!--profile-body START HERE-->
    <div class="profile-body">
        <div class="profile-body-left">
            <div class="profile-body-left-left">
                <asp:Image ID="imgHotel" runat="server" align="absmiddle" Width="100px" Height="100px" />
            </div>
            <div class="profile-body-left-right">
                <div class="star3">
                    <asp:Image ID="imgstars" runat="server" />
                </div>
                <h2>
                    <asp:Label ID="lblHotelname" runat="server"></asp:Label></h2>
                <p class="heading">
                    <asp:Label ID="lblAddress" runat="server"></asp:Label></p>
                <p>
                    <asp:Label runat="server" ID="lblDescription"></asp:Label></p>
                    <table cellpadding="0" cellspacing="0" style="  margin:10px 0px;  border: #D7D1D1 solid 1px;">
                        <tr>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><b><%= GetKeyResult("TIMING")%></b></td>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><b><%= GetKeyResult("FROM")%></b></td>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><b><%= GetKeyResult("TO")%></b></td>
                        </tr>
                        <tr>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><%= GetKeyResult("FULLDAY")%></td>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><asp:Label ID="lblFullFrom" runat="server" ></asp:Label></td>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><asp:Label ID="lblFullTo" runat="server" ></asp:Label></td>
                        </tr>
                        <tr>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><%= GetKeyResult("MORNING")%></td>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><asp:Label ID="lblMorningFrom" runat="server" ></asp:Label></td>
                            <td style=" padding:5px;border-bottom: #D7D1D1 solid 1px;"><asp:Label ID="lblMorningTo" runat="server" ></asp:Label></td>
                        </tr>
                        <tr>
                            <td style=" padding:5px;"><%= GetKeyResult("AFTERNOON")%></td>
                            <td style=" padding:5px;"><asp:Label ID="lblAfternoonFrom" runat="server" ></asp:Label></td>
                            <td style=" padding:5px;"><asp:Label ID="lblAfternoonTo" runat="server" ></asp:Label></td>
                        </tr>
                    </table>
            </div>
        </div>
        <div class="profile-body-right">
            <a href="javascript:void(0);" id="showMap" >
                <img src="<%= SiteRootPath %>images/map-icon.png" align="absmiddle" /> <%= GetKeyResult("SHOWMAP")%></a> 
        </div>
        <div id="maploc" style="background-color: #E5E3DF;border: 2px solid green;float: right;height: 176px;left: 923px;overflow: hidden;position: absolute;top: 242px;width: 249px;z-index: 50;display:none;" ></div>
        <input type="image" src="<%= SiteRootPath %>Images/close.png" style="float: right;position: absolute;z-index: 55;display:none;" class="close"/>
        <script language="javascript" type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#showMap").bind("click", function () {var offsetshowmap = jQuery("#showMap").offset();
                    jQuery("#maploc").css({"top":(offsetshowmap.top+20) + "px","left":(offsetshowmap.left-10)+"px"});
                    jQuery(".close").css({"top":(offsetshowmap.top+10) + "px","left":(offsetshowmap.left + 230)+"px"})
                    jQuery("#maploc").show();jQuery(".close").show();LoadMapLatLong();});
                jQuery(".close").bind("click",function(){ jQuery("#maploc").hide();jQuery(".close").hide();return false;});
            });
            function LoadMapLatLong() {

                var map = new google.maps.Map(document.getElementById('maploc'), {
                    zoom: 14,
                    center: new google.maps.LatLng(<%= CurrentLat %>, <%= CurrentLong %>),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<%= CurrentLat %>, <%= CurrentLong %>),
                    map: map
                });

            }
        </script>
    </div>
    <!--profile-body ENDS HERE-->
    <div id="divWarnning" runat="server" class="warning" style="display:none;"></div>
    <div class="error" id="divError" runat="server" style="display:none;" ></div>
    <!--meeting room body START HERE-->
    <div class="meeting-room-body">
        <h1>
            <%= GetKeyResult("MEETINGROOMS")%></h1><span style="margin-left:10px;" ><i><%= GetKeyResult("FORALLMEETINGROOMSYOUHAVETOSPECIFIED")%>.</i></span>
        <div class="meeting-room-body-heading">
            <div class="meeting-room-body-heading1">
                <%= GetKeyResult("DESCRIPTION")%>
            </div>
            <div class="meeting-room-body-heading2">
                <%= GetKeyResult("ARRIVALTIME")%> &nbsp;&nbsp; <%= GetKeyResult("DEPARTURETIME")%>
            </div>
            <div class="meeting-room-body-heading3">
                <%= GetKeyResult("QTY")%>
            </div>
        </div>
        <ul>
            <asp:Repeater ID="dtLstMeetingroom" runat="server" OnItemDataBound="dtLstMeetingroom_ItemDataBound"
                OnItemCommand="dtLstMeetingroom_ItemCommand">
                <ItemTemplate>
                    <li>
                        <div class="meeting-room-body-ulli-heading">
                            <h2>
                                1.
                                <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label></h2>
                            <asp:ImageButton ID="btnDelete" runat="server" 
                                ImageUrl="~/images/delete-ol-btn.png" CssClass="ImageFloat" CommandName="DeleteMeetingroom"
                                CommandArgument='<%# Eval("MRId") %>' />
                        </div>
                        <div class="meeting-room-body-ulli-body" id="mydiv" runat="server">
                            <div class="meeting-room-body-ulli-body-image">
                                <asp:Image ID="imgMrImage" runat="server" Width="69px" Height="69px" />
                            </div>
                            <div class="meeting-room-body-ulli-body-description">
                                <p>
                                    <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                <p class="last">
                                    <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                <div>
                                    <asp:HyperLink ID="hypPlan" runat="server"></asp:HyperLink></div>
                            </div>
                            <div class="meeting-room-body-ulli-right">
                                <asp:Repeater ID="rptDays" runat="server" OnItemDataBound="rptDays_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="meeting-room-body-ulli-right-inner">
                                            <div class="right-inner1">
                                                <%= GetKeyResult("DAY")%> 1
                                            </div>
                                            <div class="right-inner2">
                                                <asp:Label ID="lblDayDuration" runat="server"></asp:Label>
                                            </div>
                                            <div class="right-inner3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left"><asp:HiddenField ID="hdnHotelArivalTime" runat="server" />
                                                            <asp:DropDownList ID="drpFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="right"><asp:HiddenField ID="hdnHotelDepartureTime" runat="server" />
                                                            <asp:DropDownList ID="drpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-inner4">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div class="meeting-room-body-ulli-right-inner-bottom">
                                            <div class="right-inner1">
                                                <%= GetKeyResult("DAY")%> 2
                                            </div>
                                            <div class="right-inner2">
                                                <asp:Label ID="lblDayDuration" runat="server"></asp:Label>
                                            </div>
                                            <div class="right-inner3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left"><asp:HiddenField ID="hdnHotelArivalTime" runat="server" />
                                                            <asp:DropDownList ID="drpFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="right"><asp:HiddenField ID="hdnHotelDepartureTime" runat="server" />
                                                            <asp:DropDownList ID="drpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-inner4">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <li>
                        <div class="meeting-room-body-ulli-heading-last">
                            <h2>
                                2.
                                <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label></h2>
                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete-ol-btn.png" CommandName="DeleteMeetingroom"
                                CommandArgument='<%# Eval("MRId") %>' CssClass="ImageFloat" />
                        </div>
                        <div class="meeting-room-body-ulli-body" id="mydiv" runat="server">
                            <div class="meeting-room-body-ulli-body-image">
                                <asp:Image ID="imgMrImage" runat="server" Width="69px" Height="69px" />
                            </div>
                            <div class="meeting-room-body-ulli-body-description">
                                <p>
                                    <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                <p class="last">
                                    <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                <div>
                                    <asp:HyperLink ID="hypPlan" runat="server"></asp:HyperLink></div>
                            </div>
                            <div class="meeting-room-body-ulli-right">
                                <asp:Repeater ID="rptDays" runat="server" OnItemDataBound="rptDays_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="meeting-room-body-ulli-right-inner">
                                            <div class="right-inner1">
                                                <%= GetKeyResult("DAY")%> 1
                                            </div>
                                            <div class="right-inner2">
                                                <asp:Label ID="lblDayDuration" runat="server"></asp:Label>
                                            </div>
                                            <div class="right-inner3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left"><asp:HiddenField ID="hdnHotelArivalTime" runat="server" />
                                                            <asp:DropDownList ID="drpFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="right"><asp:HiddenField ID="hdnHotelDepartureTime" runat="server" />
                                                            <asp:DropDownList ID="drpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-inner4">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div class="meeting-room-body-ulli-right-inner-bottom">
                                            <div class="right-inner1">
                                                <%= GetKeyResult("DAY")%> 2
                                            </div>
                                            <div class="right-inner2">
                                                <asp:Label ID="lblDayDuration" runat="server"></asp:Label>
                                            </div>
                                            <div class="right-inner3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left"><asp:HiddenField ID="hdnHotelArivalTime" runat="server" />
                                                            <asp:DropDownList ID="drpFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="right"><asp:HiddenField ID="hdnHotelDepartureTime" runat="server" />
                                                            <asp:DropDownList ID="drpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-inner4">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </li>
                </AlternatingItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <!--meeting room body ENDS HERE-->
    <!--next button START HERE-->
    <div class="next-button">
&nbsp;&nbsp;<asp:LinkButton ID="lnkNext"
            runat="server" CssClass="step-next-btn" OnClick="lnkNext_Click"><%= GetKeyResult("NEXTSTEP")%></asp:LinkButton>
    </div>
    <!--next button ENDS HERE-->
    <script language="javascript" type="text/javascript">
        var loginUser = '<%= Session["CurrentAgencyUserID"] %>';
        jQuery(document).ready(function () {
            jQuery("#<%= divError.ClientID %>").hide();
            jQuery("#<%= lnkNext.ClientID %>").bind("click", function () {
                var errorMessage = "";
                var isValid = true;
                var countdivs = jQuery(".meeting-room-body-ulli-body").length;
                for (i = 0; i < parseInt(countdivs, 10); i++) {
                    var maxandmin = jQuery("#cntMainBody_dtLstMeetingroom_mydiv_" + i + " .meeting-room-body-ulli-body-description .last span").html();
                    jQuery("#cntMainBody_dtLstMeetingroom_mydiv_" + i + " .meeting-room-body-ulli-right .right-inner4 input:text").each(function (e) {
                        var val = jQuery(this).val();
                        if (parseInt(val) < parseInt(maxandmin.split('-')[0]) || parseInt(val) > parseInt(maxandmin.split('-')[1])) {
                            jQuery(this).focus();
                            if (errorMessage.length > 0) {
                                errorMessage += "<br>";
                            }
                            errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERVALUEBETWEEN")%>' + maxandmin.split('-')[0] + ' <%= GetKeyResult("AND")%> ' + maxandmin.split('-')[1] + '.';
                            isValid = false;
                        }
                        else if (isNaN(val) || val.length <= 0) {
                            //jQuery(this).addClass("errorQuantity");
                            if (errorMessage.length > 0) {
                                errorMessage += "<br>";
                            }
                            errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERQUANTITY")%>.';
                            isValid = false;
                        }
                    });
                }
                if (!isValid) {
                    jQuery("#<%= divError.ClientID %>").html(errorMessage);
                    jQuery("#<%= divError.ClientID %>").show();
                    var offseterror = jQuery("#<%= divError.ClientID %>").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }
                else {
                    if (loginUser == '') {
                        alert('<%= GetKeyResultForJavaScript("LOGINFORBOOKING")%>.');
                        jQuery("#<%= divError.ClientID %>").hide();
                    }
                    else {
                        jQuery("#<%= divError.ClientID %>").hide();
                    }
                }
            });
        });
    </script>
</asp:Content>

<asp:Content ID="ContentLeftSearch" ContentPlaceHolderID="cntLeftSearch" runat="server" >
<uc2:LeftSearchPanel ID="LeftSearchPanel1" runat="server" />
</asp:Content>