﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using System.Configuration;

public partial class Agency_RequestStep3 : BasePage
{
    #region Variables and Properties
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Hotel objRequestHotel = new Hotel();
    BookingManager bm = new BookingManager();
    int countMr = 0;
    CreateRequest objRequest = null;
    public List<PackageItemDetails> SelectedPackage
    {
        get;
        set;
    }
    public List<RequestExtra> SelectedExtra
    {
        get;
        set;
    }
    public TList<PackageItems> AllPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> AllFoodAndBravrages
    {
        get;
        set;
    }
    public TList<PackageItems> AllEquipments
    {
        get;
        set;
    }
    public TList<PackageItems> AllOthersItem
    {
        get;
        set;
    }
    public TList<PackageItemMapping> AllPackageItemMapping
    {
        get;
        set;
    }
    public UserControl_Agency_LeftSearchPanel u
    {
        get
        {
            return Page.Master.FindControl("cntLeftSearch").FindControl("LeftSearchPanel1") as UserControl_Agency_LeftSearchPanel;
        }
    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["CurrentAgencyUserID"] == null)
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
            u.SearchButton += new EventHandler(u_SearchButton);
            u.MapSearch += new EventHandler(u_MapSearch);
            if (!Page.IsPostBack)
            {
                u.usercontroltype = "Basic";
                Users objUsers = Session["CurrentAgencyUser"] as Users;
                //Bind Login users details
                //lblDateLogin.Text = DateTime.Now.ToString("dd MMM yyyy");
                //lblUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                CheckSearchAvailable();
                hypTermsandCondition.NavigateUrl = ConfigurationManager.AppSettings["FilePath"] + "MediaFile/TermsAndConditions.pdf";
                hypTermsandCondition.Target = "_blank";
                hypTermsandCondition.Attributes.Add("onclick", "return open_win('" + SiteRootPath + hypTermsandCondition.NavigateUrl.Replace("~/", "") + "');");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    BookingRequest objBookingRequest = new BookingRequest();
    void u_SearchButton(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Request";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void u_MapSearch(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Request";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void ManageSession()
    {
        string WhereClause = string.Empty;
        string WhereClauseday2 = string.Empty;
        if (u.propCountryID != "0")
        {
            if (u.propCountryID != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + u.propCountryID;
                WhereClauseday2 += HotelColumn.CountryId + "=" + u.propCountryID;
            }
        }

        if (Convert.ToInt32(u.propCity) != 0)
        {
            if (u.propCity != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + u.propCity;
                WhereClauseday2 += HotelColumn.CityId + "=" + u.propCity;
            }
        }
        if (u.propDuration == "1")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }
        if (u.propDuration == "2")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }


                if (u.propDay2 == "0")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay2 == "1")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay2 == "2")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }

        if (u.propDate != "")
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + u.propDate.Split('/')[2]), Convert.ToInt32(u.propDate.Split('/')[1]), Convert.ToInt32(u.propDate.Split('/')[0]));

            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }


        if (u.propParticipant != "" && u.propParticipant != "0")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            WhereClauseday2 += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        }

        //Send all search session value
        Session["Where"] = WhereClause;
        if (u.propDuration == "2")
        {
            Session["Where2"] = WhereClauseday2;
        }

        //Maintain session value for all controls 
        objBookingRequest = new BookingRequest();
        objBookingRequest.propCountry = u.propCountryID;
        objBookingRequest.propCity = u.propCity;
        objBookingRequest.propDate = Convert.ToString(string.IsNullOrEmpty(u.propDate) ? (u.propDate == "dd/mm/yy" ? DateTime.Now.ToString("dd/MM/yy") : u.propDate) : u.propDate);
        objBookingRequest.propDuration = u.propDuration;
        objBookingRequest.propDays = u.propDay1;
        objBookingRequest.propDay2 = u.propDay2;
        if (u.propParticipant != "")
        {
            objBookingRequest.propParticipants = u.propParticipant;
        }
        else
        {
            objBookingRequest.propParticipants = "0";
        }
        Session["masterInput"] = objBookingRequest;
        Session["CurrencyID"] = "2";
        //Dictionary<long, string> participants = new Dictionary<long, string>();
        //participants.Add(1, objBookingRequest.propParticipants);
        //Session["participants"] = participants;
    }
    #endregion

    #region Additional methods
    /// <summary>
    /// Check Availability of Serach according To Serach ID.
    /// </summary>
    public void CheckSearchAvailable()
    {
        if (Session["RequestID"] != null)
        {
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
            if (st != null)
            {
                if (st.CurrentStep == 3)
                {
                    objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
                    //Bind Booking Details
                    BindHotel();
                }
                else if (st.CurrentStep == 4)
                {
                    Response.Redirect("~/Agency/request.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("~/Agency/controlpaneluser.aspx");
        }
    }
    public void BindHotel()
    {
        try
        {
            rptHotel.DataSource = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0);
            rptHotel.DataBind();
            lblArrival.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");
            lblDeparture.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
            Int64 intHotelID = objRequest.HotelList[0].HotelID;
            if (objRequest.PackageID != 0)
            {
                pnlPackage.Visible = true;
                PackageMaster packagedetails = objPackagePricingManager.GetAllPackageName().Where(a => a.Id == objRequest.PackageID).FirstOrDefault();
                if (packagedetails != null)
                {
                    lblPackageName.Text = packagedetails.PackageName;
                    
                    objHotel.GetDescriptionofPackageByPackage(packagedetails);
                    PackageMasterDescription des = packagedetails.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                    if (des == null)
                    {
                        des = packagedetails.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64("1")).FirstOrDefault();
                    }
                    if (des != null)
                    {
                        lblpackageDescription.Text = des.Description;
                    }
                }
                AllPackageItem = objPackagePricingManager.GetAllPackageItems();
                rptPackageItem.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(objRequest.PackageID);
                rptPackageItem.DataBind();
                if (objRequest.ExtraList.Count > 0)
                {
                    //Package Selection.

                    pnlExtra.Visible = true;
                    rptExtras.DataSource = objRequest.ExtraList;
                    rptExtras.DataBind();//check AllPackageItem.Where(a => a.IsExtra == true)
                }
                else
                {
                    pnlExtra.Visible = false;
                }
                pnlFoodAndBravrages.Visible = false;
            }
            else
            {
                pnlPackage.Visible = false;

                if (objRequest.BuildYourMeetingroomList.Count > 0)
                {
                    AllFoodAndBravrages = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
                    pnlFoodAndBravrages.Visible = true;
                    rptFoodandBravragesDay.DataSource = objRequest.DaysList;
                    rptFoodandBravragesDay.DataBind();
                }
                else
                {
                    pnlFoodAndBravrages.Visible = false;
                }
            }

            lblDuration.Text = Convert.ToString(objRequest.Duration == 1 ? objRequest.Duration + GetKeyResult("DAY") : objRequest.Duration + GetKeyResult("DAY"));
            if (objRequest.EquipmentList.Count > 0)
            {
                AllEquipments = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
                pnlEquipment.Visible = true;
                rptEquipmentDay.DataSource = objRequest.DaysList;
                rptEquipmentDay.DataBind();
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            if (objRequest.OthersList.Count > 0)
            {
                AllOthersItem = objPackagePricingManager.GetAllOtherItems(intHotelID);
                pnlOthers.Visible = true;
                rptOthersDay.DataSource = objRequest.DaysList;
                rptOthersDay.DataBind();
            }
            else
            {
                pnlOthers.Visible = false;
            }
            if (objRequest.IsAccomodation)
            {
                pnlAccomodation.Visible = true;
                rptDays2.DataSource = objRequest.RequestAccomodationList;
                rptDays2.DataBind();
                rptSingleQuantity.DataSource = objRequest.RequestAccomodationList;
                rptSingleQuantity.DataBind();
                rptDoubleQuantity.DataSource = objRequest.RequestAccomodationList;
                rptDoubleQuantity.DataBind();
                if (objRequest.RequestAccomodationList.Count < 6)
                {
                    ltrExtendDays.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
                    ltrExtendSQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                    ltrExtendDQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                }
                //lblaccomodationQun.Text = Convert.ToString(objRequest.RequestAccomodationList[0].QuantityDouble);
            }
            else
            {
                pnlAccomodation.Visible = false;
            }
            lblSpecialRequest.Text = objRequest.SpecialRequest;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }


    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label txtQuantity = (Label)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            //AllPackageItem
            PackageItemMapping pim = e.Item.DataItem as PackageItemMapping;
            if (pim != null)
            {
                PackageItems p = AllPackageItem.Where(a => a.Id == pim.ItemId).FirstOrDefault();
                if (p != null)
                {
                    hdnItemId.Value = Convert.ToString(p.Id);

                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                    PackageItemDetails pid = objRequest.PackageItemList.Where(a => a.ItemID == p.Id).FirstOrDefault();
                    if (pid != null)
                    {
                        txtQuantity.Text = Convert.ToString(pid.Quantity);
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
            }
        }
    }
    #endregion
    #region New Accommodation
    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            lblDay.Text = a.Checkin.ToString("dd/MM/yyyy");
        }
    }
    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label txtQuantitySDay = (Label)e.Item.FindControl("txtQuantitySDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantitySDay.Text = a.QuantitySingle.ToString();
            hdnDate.Value = a.Checkin.ToString();
        }
    }
    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label txtQuantityDDay = (Label)e.Item.FindControl("txtQuantityDDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantityDDay.Text = a.QuantityDouble.ToString();
            hdnDate.Value = a.Checkin.ToString();
        }
    }
    #endregion
    protected void rptHotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
            Repeater rptMeetingroom = (Repeater)e.Item.FindControl("rptMeetingroom");
            BuildHotelsRequest b = e.Item.DataItem as BuildHotelsRequest;
            if (b != null)
            {
                lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                Hotel objHtl = objHotel.GetHotelDetailsById(b.HotelID);
                lblHotelName.Text = objHtl.Name;
                rptMeetingroom.DataSource = b.MeetingroomList;
                rptMeetingroom.DataBind();
            }
        }
    }

    protected void rptMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
            Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblIsMain = (Label)e.Item.FindControl("lblIsMain");
            BuildMeetingRoomRequest brm = e.Item.DataItem as BuildMeetingRoomRequest;
            if (brm != null)
            {
                lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(brm.MeetingRoomID);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == brm.ConfigurationID).FirstOrDefault();
                lblMeetingRoomName.Text = objMeetingroom.Name;
                lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                lblQuantity.Text = brm.Quantity.ToString();
                lblIsMain.Text = brm.IsMain == true ? GetKeyResult("MAIN") : GetKeyResult("BREAKOUT");
            }
        }
    }

    protected void rptExtras_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestExtra r = e.Item.DataItem as RequestExtra;
            if (r != null)
            {
                PackageItems objPackage = AllPackageItem.Where(a => a.IsExtra == true && a.Id == r.ItemID).FirstOrDefault();
                if (objPackage != null)
                {

                    PackageDescription pdesc = objPackage.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = objPackage.ItemName;
                    }
                    lblQuantity.Text = Convert.ToString(r.Quantity);
                }
            }
        }
    }

    protected void rptFoodandBravragesDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptFoodandBravragesItem = (Repeater)e.Item.FindControl("rptFoodandBravragesItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptFoodandBravragesItem.DataSource = objRequest.BuildYourMeetingroomList.Where(a => a.SelectDay == nod.SelectedDay);
                rptFoodandBravragesItem.DataBind();
            }
        }
    }

    protected void rptFoodandBravragesItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            BuildYourMeetingroomRequest bm = e.Item.DataItem as BuildYourMeetingroomRequest;
            if (bm != null)
            {
                PackageItems p = AllFoodAndBravrages.Where(a => a.Id == bm.ItemID).FirstOrDefault();
                if (p != null)
                {

                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                    lblQuantity.Text = Convert.ToString(bm.Quantity);
                }
            }
        }
    }

    protected void rptEquipmentDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptEquipmentItem = (Repeater)e.Item.FindControl("rptEquipmentItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay == 1 ? GetKeyResult("DAY") + nod.SelectedDay : GetKeyResult("DAY") + nod.SelectedDay;
                rptEquipmentItem.DataSource = objRequest.EquipmentList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptEquipmentItem.DataBind();
            }
        }
    }

    protected void rptEquipmentItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestEquipment re = e.Item.DataItem as RequestEquipment;
            if (re != null)
            {
                PackageItems p = AllEquipments.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {

                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }

    protected void rptOthersDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptOthersItem = (Repeater)e.Item.FindControl("rptOthersItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay == 1 ? GetKeyResult("DAY") + nod.SelectedDay : GetKeyResult("DAY") + nod.SelectedDay;
                rptOthersItem.DataSource = objRequest.OthersList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptOthersItem.DataBind();
            }
        }
    }

    protected void rptOthersItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestOthers re = e.Item.DataItem as RequestOthers;
            if (re != null)
            {
                PackageItems p = AllOthersItem.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {

                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }

    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
        objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
        st.CurrentStep = 2;
        st.CurrentDay = 1;
        st.SearchObject = TrailManager.XmlSerialize(objRequest);
        if (bm.SaveSearch(st))
        {
            Response.Redirect("~/Agency/request.aspx");
        }
    }

    protected void lnkConfirm_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
        objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
        st.CurrentStep = 4;
        st.CurrentDay = 1;
        st.SearchObject = TrailManager.XmlSerialize(objRequest);
        if (bm.SaveSearch(st))
        {
            Response.Redirect("~/Agency/Request.aspx");
        }
    }
}