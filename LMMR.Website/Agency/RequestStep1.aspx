﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/Main.master" AutoEventWireup="true" CodeFile="RequestStep1.aspx.cs" Inherits="Agency_RequestStep1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../UserControl/Agency/LeftSearchPanel.ascx" tagname="LeftSearchPanel" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntMainBody" Runat="Server">
<!--today-date START HERE-->
    <%-- <div class="today-date">
        <asp:Panel ID="pnlLogin" runat="server" >
        <div class="today-date-left">
            <b><%= GetKeyResult("TODAY")%>:</b> <asp:Label ID="lblDateLogin" runat="server"></asp:Label>
        </div>
        <div class="today-date-right">
            <%= GetKeyResult("YOUARELOGGEDINAS")%>: <b><asp:Label ID="lblUserName" runat="server"></asp:Label></b>
        </div>
        </asp:Panel>
    </div>--%>
    <!--today-date ENDS HERE-->
    <!--request-step-mainbody START HERE-->
    <div class="request-step-mainbody">
        <!--request-stepbody START HERE-->
        <div class="request-stepbody">
            <div class="request-step1">
                <span><%= GetKeyResult("STEP1")%>. </span><%= GetKeyResult("BASKETREQUEST")%>
            </div>
            <div class="request-step2">
                <span><%= GetKeyResult("STEP2")%>.</span> <%= GetKeyResult("EXTRAINFO")%>
            </div>
            <div class="request-step3">
                <span><%= GetKeyResult("STEP3")%>.</span> <%= GetKeyResult("RECAP")%>
            </div>
            <div class="request-step4">
                <span><%= GetKeyResult("STEP4")%>.</span> <%= GetKeyResult("SENDING")%> 
            </div>
        </div>
        <!--request-stepbody ENDS HERE-->
        <!--request-heading1 START HERE-->
        <div class="request-heading1">
            <h1>
                <span><%= GetKeyResult("YOURBASKETREQUEST")%></span></h1>
        </div>
        <!--request-heading1 ENDS HERE-->
        <div id="errormessage" class="error" style="display:none;" ></div>
        <!--request-date-body START HERE-->
        <div class="request-date-body">
            <div class="request-arrival-date">
                <span><%= GetKeyResult("ARRIVALDATE")%>:</span>
                <asp:Label ID="lblArrival" runat="server"></asp:Label>
            </div>
            <div class="request-departure-date">
                <span><%= GetKeyResult("DEPARTUREDATE")%>:</span> <asp:Label ID="lblDeparture" runat="server"></asp:Label>
            </div>
            <div class="request-participants">
                <%--<span>Participants:</span> 45--%>
            </div>
        </div>
        <asp:Repeater ID="rptDays" runat="server" OnItemDataBound="rptDays_ItemDataBound">
            <ItemTemplate>
                <div class="request-day-body">
                    <div class="request-day-box1">
                        <span><%= GetKeyResult("DURATION")%>:</span>&nbsp;&nbsp;
                        <asp:Label ID="lblDuration" runat="server"></asp:Label>
                    </div>
                    <div class="request-day-box2">
                        <div class="request-day-box2-box1">
                            <b><%= GetKeyResult("DAY")%> <asp:Label ID="lblDay" runat="server"></asp:Label>:</b></div>
                        <div id="Fullday">
                            <asp:DropDownList ID="drpSelectedTime" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div id="Fulldaytime">
                            <asp:DropDownList ID="drpStart" runat="server" ></asp:DropDownList>
                        </div>
                        <div id="Fulldaytime1">
                            <asp:DropDownList ID="drpEnd" runat="server" ></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--request-date-body ENDS HERE-->
        <!--request-hotal-body START HERE-->
        <div class="request-hotal-body">
            <ul>
                <asp:Repeater ID="dtLstHotel" runat="server" 
                    OnItemDataBound="dtLstHotel_ItemDataBound" >
                    <ItemTemplate><asp:HiddenField ID="hdnHotelID" runat="server" />
                        <li>
                            <div class="request-hotal-body-inner">
                                <!--request-hotal-body-inner-top START HERE-->
                                <div class="request-hotal-body-inner-top">
                                    <div class="request-hotal-body-inner-top-left">
                                        <asp:Label ID="lblIndex" runat="server"></asp:Label>.</div>
                                    <div class="request-hotal-body-inner-top-mid">
                                        <h2>
                                            <asp:Label ID="lblHotelname" runat="server"></asp:Label></h2>
                                        <div class="h2star3">
                                            <%--<img src="<%= SiteRootPath %>images/star3.png">--%><asp:Image ID="imgStars" runat="server" /></div>
                                        <div class="small-heading">
                                            <asp:Label ID="lblAddress" runat="server"></asp:Label></div>
                                    </div>
                                    <div class="request-hotal-body-inner-top-right">
                                        <span class="long"><asp:HiddenField ID="hdnLong" runat="server" /></span>
                                        <span class="lat"><asp:HiddenField ID="hdnLat" runat="server" /></span>
                                        <a href="javascript:void(0);" class="showMap">
                                            <img align="absmiddle" src="<%= SiteRootPath %>images/map-icon.png"> <%= GetKeyResult("SHOWMAP")%></a></div>
                                </div>
                                <!--request-hotal-body-inner-top ENDS HERE-->
                                <!--request-hotal-inner-headingSTART HERE-->
                                <div class="request-hotal-inner-heading">
                                    <div class="request-hotal-inner-heading1">
                                        <%= GetKeyResult("MEETINGROOMS")%>
                                    </div>
                                    <div class="request-hotal-inner-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="request-hotal-inner-heading3">
                                        <%= GetKeyResult("QTY")%>
                                    </div>
                                    <div class="request-hotal-inner-heading4">
                                        <%= GetKeyResult("CHOOSECATEGORY")%>
                                    </div>
                                </div>
                                <!--request-hotal-inner-heading ENDS HERE-->
                                <!--request-hotal-inner-detail START HERE-->
                                <div class="request-hotal-inner-detail">
                                    <ul>
                                        <asp:Repeater ID="dtLstMeetingroom" runat="server" OnItemDataBound="dtLstMeetingroom_ItemDataBound" onitemcommand="dtLstMeetingroom_ItemCommand" >
                                            <ItemTemplate><asp:HiddenField ID="hdnMeetingRoomID" runat="server" />
                                                <li>
                                                    <div class="request-hotal-inner-detail-heading">
                                                        <div class="request-hotal-inner-detail-heading-left">
                                                            <asp:Label ID="lblIndex" runat="server" ></asp:Label>. <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="request-hotal-inner-detail-heading-right">
                                                            <asp:ImageButton ID="btnDelete" runat="server" OnClientClick="return confirm('Do you want to delete this meeting room?');"
                                                                ImageUrl="~/images/delete-ol-btn.png" CssClass="ImageFloat" CommandName="DeleteMeetingroom"/>
                                                        </div>
                                                    </div>
                                                    <!--request-hotal-inner-detail-room START HERE-->
                                                    <div class="request-hotal-inner-detail-room">
                                                        <div class="request-hotal-inner-detail-room-inner">
                                                            <div class="request-hotal-inner-detail-room1">
                                                                <asp:Image ID="imgMrImage" runat="server" Width="69px" Height="69px" />
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room2">
                                                                <p>
                                                                    <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                                                <p class="last">
                                                                    <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                                                <div>
                                                                    <asp:HyperLink ID="hypPlan" runat="server"><%= GetKeyResult("SEEPLAN")%></asp:HyperLink>
                                                                </div>
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room3">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room4">
                                                                <p >
                                                                    <asp:RadioButton ID="rbtnMain" runat="server" GroupName="cat" onclick="return onMain(this);" />
                                                                    <asp:Label ID="lblMain" runat="server" ><%= GetKeyResult("MAIN")%>&nbsp;<img id="imgmain" src="Images/help-2.png" class="information" title='<%= GetKeyResult("MAINMEETINGROOMINFO")%>' /></asp:Label></p>
                                                                <p >
                                                                    <asp:RadioButton ID="rbtnBreakDown" runat="server" GroupName="cat" onclick="return onMain(this);" />
                                                                    <asp:Label ID="lblBreakDown" runat="server" ><%= GetKeyResult("BREAKOUT")%>&nbsp;<img  id="imgbreakout"  src="Images/help-2.png" class="information" title='<%= GetKeyResult("BREAKOUTMEETINGROOMINFO")%>' /></asp:Label></p>
                                                                    
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--request-hotal-inner-detail-room ENDS HERE-->
                                                </li>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate><asp:HiddenField ID="hdnMeetingRoomID" runat="server" />
                                                <li>
                                                    <div class="request-hotal-inner-detail-heading1">
                                                        <div class="request-hotal-inner-detail-heading-left">
                                                            <asp:Label ID="lblIndex" runat="server" ></asp:Label>. <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="request-hotal-inner-detail-heading-right">
                                                            <asp:ImageButton ID="btnDelete" runat="server" OnClientClick="return confirm('Do you want to delete this meeting room?');"
                                                                ImageUrl="~/images/delete-ol-btn.png" CssClass="ImageFloat" CommandName="DeleteMeetingroom"/>
                                                        </div>
                                                    </div>
                                                    <!--request-hotal-inner-detail-room START HERE-->
                                                    <div class="request-hotal-inner-detail-room">
                                                        <div class="request-hotal-inner-detail-room-inner">
                                                            <div class="request-hotal-inner-detail-room1">
                                                                <asp:Image ID="imgMrImage" runat="server" Width="69px" Height="69px" />
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room2">
                                                                <p>
                                                                    <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                                                <p class="last">
                                                                    <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                                                <div>
                                                                    <asp:HyperLink ID="hypPlan" runat="server"><%= GetKeyResult("SEEPLAN")%></asp:HyperLink>
                                                                </div>
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room3">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room4">
                                                                <p >
                                                                    <asp:RadioButton ID="rbtnMain" runat="server" GroupName="cat2" onclick="return onMain(this);" />
                                                                    <asp:Label ID="lblMain" runat="server"><%= GetKeyResult("MAIN")%>&nbsp;<img  id="imgmain2" src="Images/help-2.png" class="information" title='<%= GetKeyResult("MAINMEETINGROOMINFO")%>' /></asp:Label></p>
                                                                <p>
                                                                    <asp:RadioButton ID="rbtnBreakDown" runat="server" GroupName="cat2" onclick="return onMain(this);" />
                                                                    <asp:Label ID="lblBreakDown" runat="server" ><%= GetKeyResult("BREAKOUT")%>&nbsp;<img  id="imgbreakout2" src="Images/help-2.png" class="information" title='<%= GetKeyResult("BREAKOUTMEETINGROOMINFO")%>' /></asp:Label></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--request-hotal-inner-detail-room ENDS HERE-->
                                                </li>
                                            </AlternatingItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                                <!--request-hotal-inner-detail ENDS HERE-->
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!--request-hotal-body ENDS HERE-->
    </div>
    <!--request-step-mainbody ENDS HERE-->
    <!--booking-heading START HERE-->
    <div class="xyz">
    </div>
    <!--booking-heading ENDS HERE-->
    <!--next button START HERE-->
    <div class="next-button">
        <%--<a href="#" class="step-pre-blue-btn">Previous step</a>--%> &nbsp;<asp:LinkButton 
            ID="lnkNext" runat="server" CssClass="step-next-blue-btn" 
            onclick="lnkNext_Click" ><%= GetKeyResult("NEXTSTEP")%></asp:LinkButton><%--<a href="#" class="step-next-blue-btn">Next
            step</a>--%>
    </div>
    <!--next button ENDS HERE-->
    <div id="maploc" style="background-color: #E5E3DF;border: 2px solid green;float: right;height: 176px;left: 923px;overflow: hidden;position: absolute;top: 242px;width: 249px;z-index: 50;display:none;" ></div>
    <input type="image" src="../Images/close.png" style="float: right;position: absolute;z-index: 55;display:none;" class="close"/>
    <script language="javascript" type="text/javascript">
        var loginUser = '<%= Session["CurrentAgencyUserID"] %>';
        jQuery(document).ready(function () {

            jQuery("input:text").bind("focus", function () {
                var p = jQuery(this).val();
                if (parseInt(p) == 0) {
                    jQuery(this).val('');
                }
            });
            jQuery("#<%= lnkNext.ClientID %>").bind("click", function () {
                var IsValid = true;
                var errorMessage = "";
                jQuery(".request-hotal-body").find("input:text").each(function () {
                    var checkminmax = jQuery(this).parent().parent().find(".last span").html();
                    var p = jQuery(this).val();
                    if (p == '' || p.length <= 0 || parseInt(p, 2) == 0 || isNaN(p)) {
                        if (errorMessage.length > 0) {
                            errorMessage += "<br/>";
                        }
                        errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERVALIDVALUES")%>';
                        IsValid = false;
                    }
                    //                    else if (parseInt(p, 10) < parseInt(checkminmax.split('-')[0], 10) || parseInt(p,10) > parseInt(checkminmax.split('-')[1], 10)) {
                    //                        if (errorMessage.length > 0) {
                    //                            errorMessage += "<br/>";
                    //                        }
                    //                        errorMessage += '<%= GetKeyResult("PLEASEENTERVALUEBETWEEN")%>' + checkminmax.split('-')[0] + ' <%= GetKeyResult("AND")%> ' + checkminmax.split('-')[1] + '.';
                    //                        IsValid = false;
                    //                    }
                });
                if (!IsValid) {
                    //alert(errorMessage);
                    jQuery(".error").show();
                    jQuery(".error").html(errorMessage);
                    var offseterror = jQuery(".error").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }

                if (loginUser == '') {
                    alert('<%= GetKeyResultForJavaScript("LOGINFORREQUEST")%>.');
                    jQuery(".error").hide();
                    jQuery(".error").html('');
                    jQuery("body").scrollTop(0);
                    jQuery("html").scrollTop(0);
                }
                else {
                    jQuery(".error").hide();
                    jQuery(".error").html('');
                    jQuery("body").scrollTop(0);
                    jQuery("html").scrollTop(0);
                }
                return true;
            });
        });

        function onMain(sender) {
            var senderid = sender.id;
            var check = senderid.split('_');
            var last = check[check.length - 1];
            if (parseInt(last, 10) == 0) {
                if (check[check.length - 2] == "rbtnMain") {
                    if (jQuery("#" + senderid.replace("rbtnMain_0", "rbtnBreakDown_1")) != undefined) {
                        var mystringB = "#" + senderid.replace("rbtnMain_0", "rbtnBreakDown_1");
                        jQuery("#" + senderid.replace("rbtnMain_0", "rbtnMain_1")).attr("checked", false);
                        jQuery(mystringB).attr("checked", true);
                        //alert(jQuery(mystringB).attr("checked"));

                    }
                }
                else {
                    if (jQuery("#" + senderid.replace("rbtnBreakDown_0", "rbtnMain_1")) != undefined) {
                        //alert('hello');
                        jQuery("#" + senderid.replace("rbtnBreakDown_0", "rbtnBreakDown_1")).attr("checked", false);
                        jQuery("#" + senderid.replace("rbtnBreakDown_0", "rbtnMain_1")).attr("checked", true);

                    }
                }
            }
            else {
                if (check[check.length - 2] == "rbtnMain") {
                    if (jQuery("#" + senderid.replace("rbtnMain_1", "rbtnBreakDown_0")) != undefined) {
                        var mystringB = "#" + senderid.replace("rbtnMain_1", "rbtnBreakDown_0");
                        jQuery("#" + senderid.replace("rbtnMain_1", "rbtnMain_0")).attr("checked", false);
                        jQuery(mystringB).attr("checked", true);
                        //alert(jQuery(mystringB).attr("checked"));

                    }
                }
                else {
                    if (jQuery("#" + senderid.replace("rbtnBreakDown_1", "rbtnMain_0")) != undefined) {
                        jQuery("#" + senderid.replace("rbtnBreakDown_1", "rbtnBreakDown_0")).attr("checked", false);
                        jQuery("#" + senderid.replace("rbtnBreakDown_1", "rbtnMain_0")).attr("checked", true);

                    }
                }
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(".showMap").each(function () {
                jQuery(this).bind("click", function () {
                    var offsetshowmap = jQuery(this).offset();
                    var long = jQuery(this).parent().find(".long").find("input:hidden").val();
                    var lat = jQuery(this).parent().find(".lat").find("input:hidden").val();
                    jQuery("#maploc").css({ "top": (offsetshowmap.top + 20) + "px", "left": (offsetshowmap.left - 10) + "px" });
                    jQuery("#maploc").show();
                    jQuery(".close").css({ "top": (offsetshowmap.top + 10) + "px", "left": (offsetshowmap.left + 230) + "px" });
                    jQuery(".close").show();
                    LoadMapLatLong(long, lat);
                });
            });
            jQuery(".close").bind("click", function () { jQuery("#maploc").hide(); jQuery(".close").hide(); return false; });
        });
        function LoadMapLatLong(long, lat) {

            var map = new google.maps.Map(document.getElementById('maploc'), {
                zoom: 14,
                center: new google.maps.LatLng(lat, long),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, long),
                map: map
            });

        }

        //////            jQuery(document).ready(function () {
        //////                //Select all anchor tag with rel set to tooltip
        //////                jQuery('.tooltip').mouseover(function (e) {

        //////                    //Grab the title attribute's value and assign it to a variable
        //////                    var tip = jQuery(this).attr('alt');
        //////                    //alert(tip);
        //////                    //Remove the title attribute's to avoid the native tooltip from the browser
        //////                    //jQuery(this).attr('alt', '');

        //////                    //Append the tooltip template and its value
        //////                    jQuery(this).append('<div id="tooltip"><div class="tipHeader"></div><div class="tipBody">' + tip + '</div><div class="tipFooter"></div></div>');

        //////                    //Set the X and Y axis of the tooltip
        //////                    jQuery('#tooltip').css('top', e.pageY + 10);
        //////                    jQuery('#tooltip').css('left', e.pageX + 20);

        //////                    //Show the tooltip with faceIn effect
        //////                    jQuery('#tooltip').fadeIn('500');
        //////                    jQuery('#tooltip').fadeTo('10', 0.8);

        //////                }).mousemove(function (e) {

        //////                    //Keep changing the X and Y axis for the tooltip, thus, the tooltip move along with the mouse
        //////                    jQuery('#tooltip').css('top', e.pageY + 10);
        //////                    jQuery('#tooltip').css('left', e.pageX + 20);

        //////                }).mouseout(function () {

        //////                    //Put back the title attribute's value
        //////                    //jQuery(this).attr('alt', jQuery('.tipBody').html());

        //////                    //Remove the appended tooltip template
        //////                    jQuery(this).children('div#tooltip').remove();

        //////                });
        //////            });
        </script>
</asp:Content>

<asp:Content ID="ContentLeftSearch" ContentPlaceHolderID="cntLeftSearch" runat="server" >
<uc2:LeftSearchPanel ID="LeftSearchPanel1" runat="server" />
</asp:Content>