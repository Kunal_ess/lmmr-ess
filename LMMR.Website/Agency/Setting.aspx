<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/AgencyMaster.master" AutoEventWireup="true"
    CodeFile="Setting.aspx.cs" Inherits="Agency_Setting" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctnTop" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ctnMain" runat="Server">
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="updatePanel">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" alt="" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                <%= GetKeyResult("LOADING")%></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatePanel" runat="server">
        <ContentTemplate>
            <div id="divmessage" runat="server" class="error">
            </div>
            <div class="agency-setting">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div class="register-main">
                                <div class="register-row4 clearfix">
                                    <span style="font-size: 18px; padding-right: 38px;">
                                        <asp:Label ID="lblAgencyName" runat="server"><%= GetKeyResult("AGENCYNAME")%>:</asp:Label></span>
                                    <asp:Label ID="lblAgencyNameText" runat="server"></asp:Label>
                                </div>
                                <div class="register-row4 clearfix">
                                    <span style="font-size: 14px; padding-right: 96px;"><b>
                                        <asp:Label ID="lblIataNo" runat="server"><%= GetKeyResult("IATANO")%>:</asp:Label></b></span>
                                    <asp:Label ID="lblIataNoText" runat="server"></asp:Label>
                                </div>
                                <div class="register-row4 clearfix">
                                    <b>
                                        <asp:Label ID="lblMasterAccount" runat="server"><%= GetKeyResult("MASTERACCOUNT")%>:</asp:Label></b>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblLogin" runat="server"><%= GetKeyResult("LOGIN")%></asp:Label>
                                    </div>
                                    <div class="res-input">
                                        <asp:Label ID="lblLoginText" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblPassword" runat="server"><%= GetKeyResult("PASSWORD")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtPassword" runat="server" CssClass="inputregister" TextMode="Password"
                                            MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblReTypePassword" runat="server"><%= GetKeyResult("RETYPEPWD")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtReTypePassword" runat="server" CssClass="inputregister" TextMode="Password"
                                            MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="agency-user">
                            <fieldset class="reg">
                                <legend>
                                    <%= GetKeyResult("ORGANISATIONINFO")%></legend>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblAddress" runat="server"><%= GetKeyResult("ADDRESS")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" MaxLength="100"
                                            CssClass="textareareg" oncopy="return false" onpaste="return false"></asp:TextBox>
                                        <div class="sep">
                                        </div>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblCountry" runat="server"><%= GetKeyResult("COUNTRY")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <div class="rowElem">
                                            <asp:DropDownList ID="organisationCountryDDL" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblPostalCode" runat="server"><%= GetKeyResult("POSTALCODE")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtPostalCode" runat="server" CssClass="inputregister2" MaxLength="15"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="filteredExtender3" runat="server" TargetControlID="txtPostalCode"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblCity" runat="server"><%= GetKeyResult("CITY")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <div class="rowElem">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="inputregister" MaxLength="30"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <br />
                            <fieldset class="reg">
                                <legend>
                                    <%= GetKeyResult("CONTACTPERSON")%></legend>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblFirstName" runat="server"><%= GetKeyResult("FIRSTNAME")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="inputregister" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblLastName" runat="server"><%= GetKeyResult("LASTNAME")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtLastName" runat="server" CssClass="inputregister" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblEmail" runat="server"><%= GetKeyResult("EMAILONLY")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <asp:Label ID="lblEmailTxt" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblPhoneContactPerson" runat="server"><%= GetKeyResult("PHONE")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <table width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <div class="rowElem">
                                                        <asp:DropDownList ID="cPCountryCodeDDL" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>7</asp:ListItem>
                                                            <asp:ListItem>20</asp:ListItem>
                                                            <asp:ListItem>27</asp:ListItem>
                                                            <asp:ListItem>30</asp:ListItem>
                                                            <asp:ListItem>31</asp:ListItem>
                                                            <asp:ListItem>32</asp:ListItem>
                                                            <asp:ListItem>33</asp:ListItem>
                                                            <asp:ListItem>34</asp:ListItem>
                                                            <asp:ListItem>36</asp:ListItem>
                                                            <asp:ListItem>39</asp:ListItem>
                                                            <asp:ListItem>40</asp:ListItem>
                                                            <asp:ListItem>41</asp:ListItem>
                                                            <asp:ListItem>43</asp:ListItem>
                                                            <asp:ListItem>44</asp:ListItem>
                                                            <asp:ListItem>45</asp:ListItem>
                                                            <asp:ListItem>46</asp:ListItem>
                                                            <asp:ListItem>47</asp:ListItem>
                                                            <asp:ListItem>48</asp:ListItem>
                                                            <asp:ListItem>49</asp:ListItem>
                                                            <asp:ListItem>51</asp:ListItem>
                                                            <asp:ListItem>52</asp:ListItem>
                                                            <asp:ListItem>53</asp:ListItem>
                                                            <asp:ListItem>54</asp:ListItem>
                                                            <asp:ListItem>55</asp:ListItem>
                                                            <asp:ListItem>56</asp:ListItem>
                                                            <asp:ListItem>57</asp:ListItem>
                                                            <asp:ListItem>58</asp:ListItem>
                                                            <asp:ListItem>60</asp:ListItem>
                                                            <asp:ListItem>61</asp:ListItem>
                                                            <asp:ListItem>62</asp:ListItem>
                                                            <asp:ListItem>63</asp:ListItem>
                                                            <asp:ListItem>64</asp:ListItem>
                                                            <asp:ListItem>65</asp:ListItem>
                                                            <asp:ListItem>66</asp:ListItem>
                                                            <asp:ListItem>81</asp:ListItem>
                                                            <asp:ListItem>82</asp:ListItem>
                                                            <asp:ListItem>84</asp:ListItem>
                                                            <asp:ListItem>86</asp:ListItem>
                                                            <asp:ListItem>90</asp:ListItem>
                                                            <asp:ListItem>91</asp:ListItem>
                                                            <asp:ListItem>92</asp:ListItem>
                                                            <asp:ListItem>93</asp:ListItem>
                                                            <asp:ListItem>94</asp:ListItem>
                                                            <asp:ListItem>95</asp:ListItem>
                                                            <asp:ListItem>98</asp:ListItem>
                                                            <asp:ListItem>212</asp:ListItem>
                                                            <asp:ListItem>213</asp:ListItem>
                                                            <asp:ListItem>216</asp:ListItem>
                                                            <asp:ListItem>218</asp:ListItem>
                                                            <asp:ListItem>220</asp:ListItem>
                                                            <asp:ListItem>221</asp:ListItem>
                                                            <asp:ListItem>222</asp:ListItem>
                                                            <asp:ListItem>223</asp:ListItem>
                                                            <asp:ListItem>224</asp:ListItem>
                                                            <asp:ListItem>225</asp:ListItem>
                                                            <asp:ListItem>226</asp:ListItem>
                                                            <asp:ListItem>227</asp:ListItem>
                                                            <asp:ListItem>228</asp:ListItem>
                                                            <asp:ListItem>229</asp:ListItem>
                                                            <asp:ListItem>230</asp:ListItem>
                                                            <asp:ListItem>231</asp:ListItem>
                                                            <asp:ListItem>232</asp:ListItem>
                                                            <asp:ListItem>233</asp:ListItem>
                                                            <asp:ListItem>234</asp:ListItem>
                                                            <asp:ListItem>235</asp:ListItem>
                                                            <asp:ListItem>236</asp:ListItem>
                                                            <asp:ListItem>237</asp:ListItem>
                                                            <asp:ListItem>238</asp:ListItem>
                                                            <asp:ListItem>239</asp:ListItem>
                                                            <asp:ListItem>240</asp:ListItem>
                                                            <asp:ListItem>241</asp:ListItem>
                                                            <asp:ListItem>242</asp:ListItem>
                                                            <asp:ListItem>243</asp:ListItem>
                                                            <asp:ListItem>244</asp:ListItem>
                                                            <asp:ListItem>245</asp:ListItem>
                                                            <asp:ListItem>248</asp:ListItem>
                                                            <asp:ListItem>249</asp:ListItem>
                                                            <asp:ListItem>250</asp:ListItem>
                                                            <asp:ListItem>251</asp:ListItem>
                                                            <asp:ListItem>252</asp:ListItem>
                                                            <asp:ListItem>253</asp:ListItem>
                                                            <asp:ListItem>254</asp:ListItem>
                                                            <asp:ListItem>255</asp:ListItem>
                                                            <asp:ListItem>256</asp:ListItem>
                                                            <asp:ListItem>257</asp:ListItem>
                                                            <asp:ListItem>258</asp:ListItem>
                                                            <asp:ListItem>260</asp:ListItem>
                                                            <asp:ListItem>261</asp:ListItem>
                                                            <asp:ListItem>262</asp:ListItem>
                                                            <asp:ListItem>263</asp:ListItem>
                                                            <asp:ListItem>264</asp:ListItem>
                                                            <asp:ListItem>265</asp:ListItem>
                                                            <asp:ListItem>266</asp:ListItem>
                                                            <asp:ListItem>267</asp:ListItem>
                                                            <asp:ListItem>268</asp:ListItem>
                                                            <asp:ListItem>269</asp:ListItem>
                                                            <asp:ListItem>290</asp:ListItem>
                                                            <asp:ListItem>291</asp:ListItem>
                                                            <asp:ListItem>297</asp:ListItem>
                                                            <asp:ListItem>298</asp:ListItem>
                                                            <asp:ListItem>299</asp:ListItem>
                                                            <asp:ListItem>350</asp:ListItem>
                                                            <asp:ListItem>351</asp:ListItem>
                                                            <asp:ListItem>352</asp:ListItem>
                                                            <asp:ListItem>353</asp:ListItem>
                                                            <asp:ListItem>354</asp:ListItem>
                                                            <asp:ListItem>355</asp:ListItem>
                                                            <asp:ListItem>356</asp:ListItem>
                                                            <asp:ListItem>357</asp:ListItem>
                                                            <asp:ListItem>358</asp:ListItem>
                                                            <asp:ListItem>359</asp:ListItem>
                                                            <asp:ListItem>370</asp:ListItem>
                                                            <asp:ListItem>371</asp:ListItem>
                                                            <asp:ListItem>372</asp:ListItem>
                                                            <asp:ListItem>373</asp:ListItem>
                                                            <asp:ListItem>374</asp:ListItem>
                                                            <asp:ListItem>375</asp:ListItem>
                                                            <asp:ListItem>376</asp:ListItem>
                                                            <asp:ListItem>377</asp:ListItem>
                                                            <asp:ListItem>378</asp:ListItem>
                                                            <asp:ListItem>380</asp:ListItem>
                                                            <asp:ListItem>381</asp:ListItem>
                                                            <asp:ListItem>382</asp:ListItem>
                                                            <asp:ListItem>385</asp:ListItem>
                                                            <asp:ListItem>386</asp:ListItem>
                                                            <asp:ListItem>387</asp:ListItem>
                                                            <asp:ListItem>389</asp:ListItem>
                                                            <asp:ListItem>420</asp:ListItem>
                                                            <asp:ListItem>421</asp:ListItem>
                                                            <asp:ListItem>423</asp:ListItem>
                                                            <asp:ListItem>500</asp:ListItem>
                                                            <asp:ListItem>501</asp:ListItem>
                                                            <asp:ListItem>502</asp:ListItem>
                                                            <asp:ListItem>503</asp:ListItem>
                                                            <asp:ListItem>504</asp:ListItem>
                                                            <asp:ListItem>505</asp:ListItem>
                                                            <asp:ListItem>506</asp:ListItem>
                                                            <asp:ListItem>507</asp:ListItem>
                                                            <asp:ListItem>508</asp:ListItem>
                                                            <asp:ListItem>509</asp:ListItem>
                                                            <asp:ListItem>590</asp:ListItem>
                                                            <asp:ListItem>591</asp:ListItem>
                                                            <asp:ListItem>592</asp:ListItem>
                                                            <asp:ListItem>593</asp:ListItem>
                                                            <asp:ListItem>595</asp:ListItem>
                                                            <asp:ListItem>597</asp:ListItem>
                                                            <asp:ListItem>598</asp:ListItem>
                                                            <asp:ListItem>599</asp:ListItem>
                                                            <asp:ListItem>670</asp:ListItem>
                                                            <asp:ListItem>672</asp:ListItem>
                                                            <asp:ListItem>673</asp:ListItem>
                                                            <asp:ListItem>674</asp:ListItem>
                                                            <asp:ListItem>675</asp:ListItem>
                                                            <asp:ListItem>676</asp:ListItem>
                                                            <asp:ListItem>677</asp:ListItem>
                                                            <asp:ListItem>678</asp:ListItem>
                                                            <asp:ListItem>679</asp:ListItem>
                                                            <asp:ListItem>680</asp:ListItem>
                                                            <asp:ListItem>681</asp:ListItem>
                                                            <asp:ListItem>682</asp:ListItem>
                                                            <asp:ListItem>683</asp:ListItem>
                                                            <asp:ListItem>685</asp:ListItem>
                                                            <asp:ListItem>686</asp:ListItem>
                                                            <asp:ListItem>687</asp:ListItem>
                                                            <asp:ListItem>688</asp:ListItem>
                                                            <asp:ListItem>689</asp:ListItem>
                                                            <asp:ListItem>690</asp:ListItem>
                                                            <asp:ListItem>691</asp:ListItem>
                                                            <asp:ListItem>692</asp:ListItem>
                                                            <asp:ListItem>850</asp:ListItem>
                                                            <asp:ListItem>852</asp:ListItem>
                                                            <asp:ListItem>853</asp:ListItem>
                                                            <asp:ListItem>855</asp:ListItem>
                                                            <asp:ListItem>856</asp:ListItem>
                                                            <asp:ListItem>870</asp:ListItem>
                                                            <asp:ListItem>880</asp:ListItem>
                                                            <asp:ListItem>886</asp:ListItem>
                                                            <asp:ListItem>960</asp:ListItem>
                                                            <asp:ListItem>961</asp:ListItem>
                                                            <asp:ListItem>962</asp:ListItem>
                                                            <asp:ListItem>963</asp:ListItem>
                                                            <asp:ListItem>964</asp:ListItem>
                                                            <asp:ListItem>965</asp:ListItem>
                                                            <asp:ListItem>966</asp:ListItem>
                                                            <asp:ListItem>967</asp:ListItem>
                                                            <asp:ListItem>968</asp:ListItem>
                                                            <asp:ListItem>970</asp:ListItem>
                                                            <asp:ListItem>971</asp:ListItem>
                                                            <asp:ListItem>972</asp:ListItem>
                                                            <asp:ListItem>973</asp:ListItem>
                                                            <asp:ListItem>974</asp:ListItem>
                                                            <asp:ListItem>975</asp:ListItem>
                                                            <asp:ListItem>976</asp:ListItem>
                                                            <asp:ListItem>977</asp:ListItem>
                                                            <asp:ListItem>992</asp:ListItem>
                                                            <asp:ListItem>993</asp:ListItem>
                                                            <asp:ListItem>994</asp:ListItem>
                                                            <asp:ListItem>995</asp:ListItem>
                                                            <asp:ListItem>996</asp:ListItem>
                                                            <asp:ListItem>998</asp:ListItem>
                                                            <asp:ListItem>1242</asp:ListItem>
                                                            <asp:ListItem>1246</asp:ListItem>
                                                            <asp:ListItem>1264</asp:ListItem>
                                                            <asp:ListItem>1268</asp:ListItem>
                                                            <asp:ListItem>1284</asp:ListItem>
                                                            <asp:ListItem>1340</asp:ListItem>
                                                            <asp:ListItem>1345</asp:ListItem>
                                                            <asp:ListItem>1441</asp:ListItem>
                                                            <asp:ListItem>1473</asp:ListItem>
                                                            <asp:ListItem>1599</asp:ListItem>
                                                            <asp:ListItem>1649</asp:ListItem>
                                                            <asp:ListItem>1664</asp:ListItem>
                                                            <asp:ListItem>1670</asp:ListItem>
                                                            <asp:ListItem>1671</asp:ListItem>
                                                            <asp:ListItem>1684</asp:ListItem>
                                                            <asp:ListItem>1758</asp:ListItem>
                                                            <asp:ListItem>1767</asp:ListItem>
                                                            <asp:ListItem>1784</asp:ListItem>
                                                            <asp:ListItem>1809</asp:ListItem>
                                                            <asp:ListItem>1868</asp:ListItem>
                                                            <asp:ListItem>1869</asp:ListItem>
                                                            <asp:ListItem>1876</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td style="padding-left: 10px;">
                                                    <asp:TextBox ID="txtCPPhone" runat="server" CssClass="inputregister2" MaxLength="15"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="filteredExtender1" runat="server" TargetControlID="txtCPPhone"
                                                        FilterType="Numbers">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                            <br />
                            <fieldset class="reg">
                                <legend>
                                    <%= GetKeyResult("FINANCIALDATA")%></legend>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblVatNo" runat="server"><%= GetKeyResult("VATNO")%></asp:Label><span
                                            style="color: Red">*</span>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtVatNo" runat="server" CssClass="inputregister" MaxLength="30"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="filteredExtender4" TargetControlID="txtVatNo" FilterType="Numbers,LowercaseLetters,UppercaseLetters"
                                            runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblVatValue" runat="server"><%= GetKeyResult("VATVALUE")%></asp:Label>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtVatValue" runat="server" CssClass="inputregister2" MaxLength="20"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="filteredExtender5" TargetControlID="txtVatValue"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblName" runat="server"><%= GetKeyResult("NAME")%></asp:Label>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="inputregister" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblDepartment" runat="server"><%= GetKeyResult("DEPARTMENT")%></asp:Label>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="inputregister" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblCommunicatonEmail" runat="server"><%= GetKeyResult("EMAILFORCOMM")%></asp:Label>
                                    </div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtCommunicationEmail" runat="server" CssClass="inputregister" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="register-row clearfix">
                                        <div class="res-level">
                                            <asp:Label ID="lblFinancialPhone" runat="server"><%= GetKeyResult("PHONE")%></asp:Label>
                                        </div>
                                        <div class="res-input">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <div class="rowElem">
                                                            <asp:DropDownList ID="CountryCodeFinancialDDL" runat="server">
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>7</asp:ListItem>
                                                                <asp:ListItem>20</asp:ListItem>
                                                                <asp:ListItem>27</asp:ListItem>
                                                                <asp:ListItem>30</asp:ListItem>
                                                                <asp:ListItem>31</asp:ListItem>
                                                                <asp:ListItem>32</asp:ListItem>
                                                                <asp:ListItem>33</asp:ListItem>
                                                                <asp:ListItem>34</asp:ListItem>
                                                                <asp:ListItem>36</asp:ListItem>
                                                                <asp:ListItem>39</asp:ListItem>
                                                                <asp:ListItem>40</asp:ListItem>
                                                                <asp:ListItem>41</asp:ListItem>
                                                                <asp:ListItem>43</asp:ListItem>
                                                                <asp:ListItem>44</asp:ListItem>
                                                                <asp:ListItem>45</asp:ListItem>
                                                                <asp:ListItem>46</asp:ListItem>
                                                                <asp:ListItem>47</asp:ListItem>
                                                                <asp:ListItem>48</asp:ListItem>
                                                                <asp:ListItem>49</asp:ListItem>
                                                                <asp:ListItem>51</asp:ListItem>
                                                                <asp:ListItem>52</asp:ListItem>
                                                                <asp:ListItem>53</asp:ListItem>
                                                                <asp:ListItem>54</asp:ListItem>
                                                                <asp:ListItem>55</asp:ListItem>
                                                                <asp:ListItem>56</asp:ListItem>
                                                                <asp:ListItem>57</asp:ListItem>
                                                                <asp:ListItem>58</asp:ListItem>
                                                                <asp:ListItem>60</asp:ListItem>
                                                                <asp:ListItem>61</asp:ListItem>
                                                                <asp:ListItem>62</asp:ListItem>
                                                                <asp:ListItem>63</asp:ListItem>
                                                                <asp:ListItem>64</asp:ListItem>
                                                                <asp:ListItem>65</asp:ListItem>
                                                                <asp:ListItem>66</asp:ListItem>
                                                                <asp:ListItem>81</asp:ListItem>
                                                                <asp:ListItem>82</asp:ListItem>
                                                                <asp:ListItem>84</asp:ListItem>
                                                                <asp:ListItem>86</asp:ListItem>
                                                                <asp:ListItem>90</asp:ListItem>
                                                                <asp:ListItem>91</asp:ListItem>
                                                                <asp:ListItem>92</asp:ListItem>
                                                                <asp:ListItem>93</asp:ListItem>
                                                                <asp:ListItem>94</asp:ListItem>
                                                                <asp:ListItem>95</asp:ListItem>
                                                                <asp:ListItem>98</asp:ListItem>
                                                                <asp:ListItem>212</asp:ListItem>
                                                                <asp:ListItem>213</asp:ListItem>
                                                                <asp:ListItem>216</asp:ListItem>
                                                                <asp:ListItem>218</asp:ListItem>
                                                                <asp:ListItem>220</asp:ListItem>
                                                                <asp:ListItem>221</asp:ListItem>
                                                                <asp:ListItem>222</asp:ListItem>
                                                                <asp:ListItem>223</asp:ListItem>
                                                                <asp:ListItem>224</asp:ListItem>
                                                                <asp:ListItem>225</asp:ListItem>
                                                                <asp:ListItem>226</asp:ListItem>
                                                                <asp:ListItem>227</asp:ListItem>
                                                                <asp:ListItem>228</asp:ListItem>
                                                                <asp:ListItem>229</asp:ListItem>
                                                                <asp:ListItem>230</asp:ListItem>
                                                                <asp:ListItem>231</asp:ListItem>
                                                                <asp:ListItem>232</asp:ListItem>
                                                                <asp:ListItem>233</asp:ListItem>
                                                                <asp:ListItem>234</asp:ListItem>
                                                                <asp:ListItem>235</asp:ListItem>
                                                                <asp:ListItem>236</asp:ListItem>
                                                                <asp:ListItem>237</asp:ListItem>
                                                                <asp:ListItem>238</asp:ListItem>
                                                                <asp:ListItem>239</asp:ListItem>
                                                                <asp:ListItem>240</asp:ListItem>
                                                                <asp:ListItem>241</asp:ListItem>
                                                                <asp:ListItem>242</asp:ListItem>
                                                                <asp:ListItem>243</asp:ListItem>
                                                                <asp:ListItem>244</asp:ListItem>
                                                                <asp:ListItem>245</asp:ListItem>
                                                                <asp:ListItem>248</asp:ListItem>
                                                                <asp:ListItem>249</asp:ListItem>
                                                                <asp:ListItem>250</asp:ListItem>
                                                                <asp:ListItem>251</asp:ListItem>
                                                                <asp:ListItem>252</asp:ListItem>
                                                                <asp:ListItem>253</asp:ListItem>
                                                                <asp:ListItem>254</asp:ListItem>
                                                                <asp:ListItem>255</asp:ListItem>
                                                                <asp:ListItem>256</asp:ListItem>
                                                                <asp:ListItem>257</asp:ListItem>
                                                                <asp:ListItem>258</asp:ListItem>
                                                                <asp:ListItem>260</asp:ListItem>
                                                                <asp:ListItem>261</asp:ListItem>
                                                                <asp:ListItem>262</asp:ListItem>
                                                                <asp:ListItem>263</asp:ListItem>
                                                                <asp:ListItem>264</asp:ListItem>
                                                                <asp:ListItem>265</asp:ListItem>
                                                                <asp:ListItem>266</asp:ListItem>
                                                                <asp:ListItem>267</asp:ListItem>
                                                                <asp:ListItem>268</asp:ListItem>
                                                                <asp:ListItem>269</asp:ListItem>
                                                                <asp:ListItem>290</asp:ListItem>
                                                                <asp:ListItem>291</asp:ListItem>
                                                                <asp:ListItem>297</asp:ListItem>
                                                                <asp:ListItem>298</asp:ListItem>
                                                                <asp:ListItem>299</asp:ListItem>
                                                                <asp:ListItem>350</asp:ListItem>
                                                                <asp:ListItem>351</asp:ListItem>
                                                                <asp:ListItem>352</asp:ListItem>
                                                                <asp:ListItem>353</asp:ListItem>
                                                                <asp:ListItem>354</asp:ListItem>
                                                                <asp:ListItem>355</asp:ListItem>
                                                                <asp:ListItem>356</asp:ListItem>
                                                                <asp:ListItem>357</asp:ListItem>
                                                                <asp:ListItem>358</asp:ListItem>
                                                                <asp:ListItem>359</asp:ListItem>
                                                                <asp:ListItem>370</asp:ListItem>
                                                                <asp:ListItem>371</asp:ListItem>
                                                                <asp:ListItem>372</asp:ListItem>
                                                                <asp:ListItem>373</asp:ListItem>
                                                                <asp:ListItem>374</asp:ListItem>
                                                                <asp:ListItem>375</asp:ListItem>
                                                                <asp:ListItem>376</asp:ListItem>
                                                                <asp:ListItem>377</asp:ListItem>
                                                                <asp:ListItem>378</asp:ListItem>
                                                                <asp:ListItem>380</asp:ListItem>
                                                                <asp:ListItem>381</asp:ListItem>
                                                                <asp:ListItem>382</asp:ListItem>
                                                                <asp:ListItem>385</asp:ListItem>
                                                                <asp:ListItem>386</asp:ListItem>
                                                                <asp:ListItem>387</asp:ListItem>
                                                                <asp:ListItem>389</asp:ListItem>
                                                                <asp:ListItem>420</asp:ListItem>
                                                                <asp:ListItem>421</asp:ListItem>
                                                                <asp:ListItem>423</asp:ListItem>
                                                                <asp:ListItem>500</asp:ListItem>
                                                                <asp:ListItem>501</asp:ListItem>
                                                                <asp:ListItem>502</asp:ListItem>
                                                                <asp:ListItem>503</asp:ListItem>
                                                                <asp:ListItem>504</asp:ListItem>
                                                                <asp:ListItem>505</asp:ListItem>
                                                                <asp:ListItem>506</asp:ListItem>
                                                                <asp:ListItem>507</asp:ListItem>
                                                                <asp:ListItem>508</asp:ListItem>
                                                                <asp:ListItem>509</asp:ListItem>
                                                                <asp:ListItem>590</asp:ListItem>
                                                                <asp:ListItem>591</asp:ListItem>
                                                                <asp:ListItem>592</asp:ListItem>
                                                                <asp:ListItem>593</asp:ListItem>
                                                                <asp:ListItem>595</asp:ListItem>
                                                                <asp:ListItem>597</asp:ListItem>
                                                                <asp:ListItem>598</asp:ListItem>
                                                                <asp:ListItem>599</asp:ListItem>
                                                                <asp:ListItem>670</asp:ListItem>
                                                                <asp:ListItem>672</asp:ListItem>
                                                                <asp:ListItem>673</asp:ListItem>
                                                                <asp:ListItem>674</asp:ListItem>
                                                                <asp:ListItem>675</asp:ListItem>
                                                                <asp:ListItem>676</asp:ListItem>
                                                                <asp:ListItem>677</asp:ListItem>
                                                                <asp:ListItem>678</asp:ListItem>
                                                                <asp:ListItem>679</asp:ListItem>
                                                                <asp:ListItem>680</asp:ListItem>
                                                                <asp:ListItem>681</asp:ListItem>
                                                                <asp:ListItem>682</asp:ListItem>
                                                                <asp:ListItem>683</asp:ListItem>
                                                                <asp:ListItem>685</asp:ListItem>
                                                                <asp:ListItem>686</asp:ListItem>
                                                                <asp:ListItem>687</asp:ListItem>
                                                                <asp:ListItem>688</asp:ListItem>
                                                                <asp:ListItem>689</asp:ListItem>
                                                                <asp:ListItem>690</asp:ListItem>
                                                                <asp:ListItem>691</asp:ListItem>
                                                                <asp:ListItem>692</asp:ListItem>
                                                                <asp:ListItem>850</asp:ListItem>
                                                                <asp:ListItem>852</asp:ListItem>
                                                                <asp:ListItem>853</asp:ListItem>
                                                                <asp:ListItem>855</asp:ListItem>
                                                                <asp:ListItem>856</asp:ListItem>
                                                                <asp:ListItem>870</asp:ListItem>
                                                                <asp:ListItem>880</asp:ListItem>
                                                                <asp:ListItem>886</asp:ListItem>
                                                                <asp:ListItem>960</asp:ListItem>
                                                                <asp:ListItem>961</asp:ListItem>
                                                                <asp:ListItem>962</asp:ListItem>
                                                                <asp:ListItem>963</asp:ListItem>
                                                                <asp:ListItem>964</asp:ListItem>
                                                                <asp:ListItem>965</asp:ListItem>
                                                                <asp:ListItem>966</asp:ListItem>
                                                                <asp:ListItem>967</asp:ListItem>
                                                                <asp:ListItem>968</asp:ListItem>
                                                                <asp:ListItem>970</asp:ListItem>
                                                                <asp:ListItem>971</asp:ListItem>
                                                                <asp:ListItem>972</asp:ListItem>
                                                                <asp:ListItem>973</asp:ListItem>
                                                                <asp:ListItem>974</asp:ListItem>
                                                                <asp:ListItem>975</asp:ListItem>
                                                                <asp:ListItem>976</asp:ListItem>
                                                                <asp:ListItem>977</asp:ListItem>
                                                                <asp:ListItem>992</asp:ListItem>
                                                                <asp:ListItem>993</asp:ListItem>
                                                                <asp:ListItem>994</asp:ListItem>
                                                                <asp:ListItem>995</asp:ListItem>
                                                                <asp:ListItem>996</asp:ListItem>
                                                                <asp:ListItem>998</asp:ListItem>
                                                                <asp:ListItem>1242</asp:ListItem>
                                                                <asp:ListItem>1246</asp:ListItem>
                                                                <asp:ListItem>1264</asp:ListItem>
                                                                <asp:ListItem>1268</asp:ListItem>
                                                                <asp:ListItem>1284</asp:ListItem>
                                                                <asp:ListItem>1340</asp:ListItem>
                                                                <asp:ListItem>1345</asp:ListItem>
                                                                <asp:ListItem>1441</asp:ListItem>
                                                                <asp:ListItem>1473</asp:ListItem>
                                                                <asp:ListItem>1599</asp:ListItem>
                                                                <asp:ListItem>1649</asp:ListItem>
                                                                <asp:ListItem>1664</asp:ListItem>
                                                                <asp:ListItem>1670</asp:ListItem>
                                                                <asp:ListItem>1671</asp:ListItem>
                                                                <asp:ListItem>1684</asp:ListItem>
                                                                <asp:ListItem>1758</asp:ListItem>
                                                                <asp:ListItem>1767</asp:ListItem>
                                                                <asp:ListItem>1784</asp:ListItem>
                                                                <asp:ListItem>1809</asp:ListItem>
                                                                <asp:ListItem>1868</asp:ListItem>
                                                                <asp:ListItem>1869</asp:ListItem>
                                                                <asp:ListItem>1876</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <asp:TextBox ID="txtFinancialPhone" runat="server" CssClass="inputregister2" MaxLength="15"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="filteredExtender6" TargetControlID="txtFinancialPhone"
                                                            FilterType="Numbers" runat="server">
                                                        </asp:FilteredTextBoxExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="register-row clearfix">
                                        <div class="res-level">
                                            <asp:Label ID="lblFinancialAddress" runat="server"><%= GetKeyResult("ADDRESS")%></asp:Label>
                                        </div>
                                        <div class="res-input">
                                            <asp:TextBox ID="txtFinancialAddress" runat="server" TextMode="MultiLine" MaxLength="100"
                                                CssClass="textareareg" oncopy="return false" onpaste="return false"></asp:TextBox>
                                            <div class="sep">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="register-row clearfix">
                                        <div class="res-level">
                                            <asp:Label ID="lblFinancialCountry" runat="server"><%= GetKeyResult("COUNTRY")%></asp:Label>
                                        </div>
                                        <div class="res-input">
                                            <div class="rowElem">
                                                <asp:DropDownList ID="financialCountryDDL" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="register-row clearfix">
                                        <div class="res-level">
                                            <asp:Label ID="lblFinancialPostalCode" runat="server"><%= GetKeyResult("POSTALCODE")%></asp:Label>
                                        </div>
                                        <div class="res-input">
                                            <asp:TextBox ID="txtFinancialPostalCode" runat="server" CssClass="inputregister2"
                                                MaxLength="15"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="filteredExtender7" TargetControlID="txtFinancialPostalCode"
                                                FilterType="Numbers" runat="server">
                                            </asp:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                    <div class="register-row clearfix">
                                        <div class="res-level">
                                            <asp:Label ID="lblFinancialCity" runat="server"><%= GetKeyResult("CITY")%></asp:Label>
                                        </div>
                                        <div class="res-input">
                                            <div class="rowElem">
                                                <asp:TextBox ID="txtFinancialCity" runat="server" CssClass="inputregister" MaxLength="30"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="register-row4 clearfix">
                                        <div class="res-level">
                                            <asp:Label ID="lblInvoicesFormat" runat="server"><%= GetKeyResult("INVOICESERIESFORMAT")%></asp:Label>
                                        </div>
                                        <div class="res-input">
                                            <asp:TextBox ID="txtInvoicesFormat" runat="server" CssClass="inputregister"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="register1-form-btn">
                <div class="save-cancel-btn1">
                    <asp:LinkButton ID="lbtCancel" runat="server" CssClass="cancel-btn" OnClick="lbtCancel_Click"><%= GetKeyResult("CANCEL")%></asp:LinkButton>
                    &nbsp; &nbsp;&nbsp;<asp:LinkButton ID="lbtSave" runat="server" CssClass="save-btn"
                        OnClientClick="return validate();" OnClick="lbtSave_Click"><%= GetKeyResult("SAVE")%></asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- end register-body-->
    <script language="javascript" type="text/javascript">
        function validate() {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";

            var password = jQuery("#<%= txtPassword.ClientID %>").val();
            if (password.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("PASSWORDERRORMESSAGE")%>';
                isvalid = false;
            }
            else {
                if (password.length < 6) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResult("PASSWORDLENGTHERRORMESSAGE")%>';
                    isvalid = false;
                }
            }

            var reTypedPassword = jQuery("#<%= txtReTypePassword.ClientID %>").val();
            if (reTypedPassword.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("REENTERPASSWORDMESSAGE")%>';
                isvalid = false;
            }

            if (password != "" && reTypedPassword != "") {
                if (password != reTypedPassword) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResult("PWDNOTMATCHINGMESSAGE")%>';
                    isvalid = false;
                }
            }

            var txtAddress = jQuery("#<%= txtAddress.ClientID %>").val();
            if (txtAddress.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("ADDRESSERRORMESSAGE")%>';
                isvalid = false;
            }

            var organisationCountryDDLValue = jQuery("#<%= organisationCountryDDL.ClientID %>").val();
            if (organisationCountryDDLValue == '<%=GetKeyResult("EXTENSION")%>' || organisationCountryDDLValue == "0") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("COUNTRYERRORMESSAGE")%>';
                isvalid = false;
            }

            var txtPostalCode = jQuery("#<%= txtPostalCode.ClientID %>").val();
            if (txtPostalCode.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("POSTALCODEERRORMESSAGE")%>';
                isvalid = false;
            }

            var txtCity = jQuery("#<%= txtCity.ClientID %>").val();
            if (txtCity == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("CITYERRORMESSAGE")%>';
                isvalid = false;
            }

            var txtFirstName = jQuery("#<%= txtFirstName.ClientID %>").val();
            if (txtFirstName.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("FIRSTNAMEERRORMESSAGE") %>';
                isvalid = false;
            }

            var txtLastName = jQuery("#<%= txtLastName.ClientID %>").val();
            if (txtLastName.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("LASTNAMEERRORMESSAGE") %>';
                isvalid = false;
            }

            var cPCountryCodeDDLValue = jQuery("#<%= cPCountryCodeDDL.ClientID %>").val();
            if (cPCountryCodeDDLValue == '<%=GetKeyResult("EXTENSION")%>' || cPCountryCodeDDLValue == "0") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("EXTENSIONCODEERRORMESSAGE")%>';
                isvalid = false;
            }

            var txtCPPhone = jQuery("#<%= txtCPPhone.ClientID %>").val();
            if (txtCPPhone.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("PHONEERRORMESSAGE")%>';
                isvalid = false;
            }

            var txtVatNo = jQuery("#<%= txtVatNo.ClientID %>").val();
            if (txtVatNo.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%=GetKeyResult("VATNOERRORMESSAGE")%>';
                isvalid = false;
            }

            var txtCommunicationEmail = jQuery("#<%= txtCommunicationEmail.ClientID %>").val();
            if (txtCommunicationEmail != null && txtCommunicationEmail != "") {
                if (!validateEmail(txtCommunicationEmail)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResult("EMAILVALADITYERRORMESSAGE")%>';
                    isvalid = false;
                }
            }
            if (!isvalid) {
                jQuery(".error").show();
                jQuery(".error").html(errormessage);
                var offseterror = jQuery(".error").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }


            jQuery(".error").hide();
            jQuery(".error").html('');
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            jQuery("#Loding_overlaySec span").html("Saving...");
            jQuery("#Loding_overlaySec").show();
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';


        }

        function validateEmail(email) {
            var a = email;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }

        }



    </script>
</asp:Content>
