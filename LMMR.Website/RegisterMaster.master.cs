﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;


public partial class RegisterMaster : BaseMasterPage
{
    HotelManager objHotelMgr = new HotelManager();
    CurrencyManager cm = new CurrencyManager();

    public void bindURls()
    {
        lnkBtnHome.PostBackUrl = SiteRootPath + "default/" + drpLanguage.SelectedItem.Text.ToLower();
        lnkBtnAbout.PostBackUrl = SiteRootPath + "about-us/" + drpLanguage.SelectedItem.Text.ToLower();
        lnkBtnContactUs.PostBackUrl = SiteRootPath + "contact-us/" + drpLanguage.SelectedItem.Text.ToLower();
        lnkBtnJoinToday.PostBackUrl = SiteRootPath + "join-today/" + drpLanguage.SelectedItem.Text.ToLower();
        lnkBtnLogin.PostBackUrl = SiteRootPath + "login/" + drpLanguage.SelectedItem.Text.ToLower();
    }
    public void BindCurrency()
    {
        drpCurrency.DataTextField = "Currency";
        drpCurrency.DataValueField = "Id";
        drpCurrency.DataSource = cm.GetAllCurrency();
        drpCurrency.DataBind();
        if (Session["CurrencyID"] == null)
        {
            Session["CurrencyID"] = drpCurrency.Items[1].Value;
            drpCurrency.SelectedIndex = drpCurrency.Items.IndexOf(drpCurrency.Items[1]);
        }

        drpCurrency.SelectedValue = Convert.ToString(Session["CurrencyID"]);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
    
        //if (!HttpContext.Current.Request.IsSecureConnection)
        //{
        //    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"), true);
        //    return;
        //}
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            imgLogo.HRef = SiteRootPath + "default/" + l.Name.ToLower();
        }
        else
        {
            imgLogo.HRef = SiteRootPath + "default/english";
        }
        BindCurrency();
        if (!Page.IsPostBack)
        {
            if (Session["LanguageID"] == null)
            {
                Session["LanguageID"] = 1;
            }
            
            drpLanguage.DataTextField = "Name";
            drpLanguage.DataValueField = "ID";
            drpLanguage.DataSource = getdata();//.Where(a => a.Id == 1)
            drpLanguage.DataBind();
            drpLanguage.SelectedValue = Convert.ToString(Session["LanguageID"]);
            if (drpLanguage.SelectedItem.Text.ToLower() == "english")
            {
                ltrtestscript.Text = @"<script language='javascript' type='text/javascript'>
       
        var google_conversion_id = 1018259257;
        var google_conversion_language = 'en';
        var google_conversion_format = '3';
        var google_conversion_color = 'ffffff';
        var google_conversion_label = 'nZ0rCKej1QIQuc7F5QM';
        var google_conversion_value = 0;
        </script>
        <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
        </script>
        <noscript>
        <div style='display: inline;'>
        <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=nZ0rCKej1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
        </div>
        </noscript>";
            }
            else if (drpLanguage.SelectedItem.Text.ToLower() == "french")
            {
                ltrtestscript.Text = @"<script language='javascript' type='text/javascript'>

        var google_conversion_id = 1018259257;
        var google_conversion_language = 'en';
        var google_conversion_format = '3';
        var google_conversion_color = 'ffffff';
        var google_conversion_label = 'XAV3CP-n1QIQuc7F5QM';
        var google_conversion_value = 0;
        </script>
        <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
        </script>
        <noscript>
        <div style='display: inline;'>
        <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=XAV3CP-n1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
        </div>
        </noscript>";
            }
            else if (drpLanguage.SelectedItem.Text.ToLower() == "dutch")
            {
                ltrtestscript.Text = @"<script language='javascript' type='text/javascript'>

        var google_conversion_id = 1018259257;
        var google_conversion_language = 'en';
        var google_conversion_format = '3';
        var google_conversion_color = 'ffffff';
        var google_conversion_label = '-b9KCIen1QIQuc7F5QM';
        var google_conversion_value = 0;
        </script>
        <script type='text/javascript' src='http://www.googleadservices.com/pagead/conversion.js'>
        </script>
        <noscript>
        <div style='display: inline;'>
        <img height='1' width='1' style='border-style: none;' alt='' src='http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=-b9KCIen1QIQuc7F5QM&amp;guid=ON&amp;script=0' />
        </div>
        </noscript>";
            }
            else
            {
                ltrtestscript.Text = "<script language='javascript' type='text/javascript'></script>";
            }
            bindURls();
        }

        if (Session["CurrentRestUserID"] == null)
        {
            lnkBtnLogout.Visible = false;
            lnkBtnLogin.Visible = true;
        }
        else
        {
            lnkBtnLogout.Visible = true;
            lnkBtnLogin.Visible = false;
        }
    }
    public TList<Language> getdata()
    {
        TList<Language> Obj = new LanguageManager().GetActiveLanguage();
        return Obj;
    }
    protected void drpCurrency_SelectedIndexChanged(object sender, EventArgs e)
    {

        Session["CurrencyID"] = drpCurrency.SelectedValue;

        Response.Redirect(Request.RawUrl);
    }
    /// <summary>
    /// This method is passing the key value
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    //public string GetKeyResult(string key)
    //{
    //    return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}

    protected void lnkBtnLogout_Click(object sender, EventArgs e)
    {
        Session.Remove("CurrentRestUserID");
        Session.Remove("CurrentRestUser");
        Session.Remove("Search");
        Session.Remove("SerachID");
        Session.Remove("CurrentUser");
        //Response.Redirect("Default.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "default/english");
        }
    }

    protected void drpLanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        //Session["LanguageID"] = DDLlanguage.SelectedValue;
        //Session["Language"] = l;
        string urlbind = Request.RawUrl;
        if (l != null)
        {
            urlbind = urlbind.Replace(l.Name.ToLower(), drpLanguage.SelectedItem.Text.ToLower());
        }
        else
        {
            urlbind = urlbind.Replace("english", drpLanguage.SelectedItem.Text.ToLower());
        }
        Session["PostBack"] = null;
        Response.Redirect(urlbind);
    }

}
