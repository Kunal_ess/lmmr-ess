﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SurveyDetailsRequest.aspx.cs"
    Inherits="SurveyDetailsRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="css/Survey.css" rel="stylesheet" type="text/css" />
     <link href="css/style.css" rel="stylesheet" type="text/css" />
    <%-- <link href="css/jqtransform.css" rel="stylesheet" type="text/css" />--%>
    <title>Survey Request</title>
    <script type="text/javascript">
        //window.opener = window
        function close() {
            window.close();
        }
        
    </script>
    <style type="text/css">
        .style1
        {
            font-size: medium;
        }
    </style>
</head>
<body>
    <div>
        <form id="form1" runat="server">
        <div id="divsurvey" runat="server" style="vertical-align:top">
            <table class="table" style="width: 1000px;" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr class="tableHeaderRow" align="center" valign="top">
                        <td>
                            <img src="Images/mail-logo.png" />
                        </td>
                       
                        <td>
                            <h1 >
                              Please take a moment to rate the following product and service elements.
                            </h1>
                        </td>
                    </tr>                 
                    <tr align="center">
                        <td colspan="2">
                            <div id="divmessage" align="center" runat="server" class="error">
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table"  style="width: 950px; padding-left: 150px;"
                cellspacing="1">
                <tbody>
                    <tr class="tableHeaderRow">
                        <td width="25%" class="tableHeaderCell">
                            Please tell us if the meeting was held
                        </td>
                        <td width="100px" class="tableHeaderCell" height="20px"  style="border-left: #F6F6F6 solid 1px;
                            padding-left: 20px;">
                            <asp:DropDownList ID="Dropmetingheld" runat="server" OnSelectedIndexChanged="Dropmetingheld_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <%-- <div id="divhoteldrop" runat="server">--%>
                        <td width="25%" class="tableHeaderCell" id="tblhotel" runat="server">
                            If,Yes, Please Specify the hotel
                        </td>
                        <td width="100px" class="tableHeaderCell" height="20px" id="tblhotel1" runat="server" style="border-left: #F6F6F6 solid 1px;
                            padding-left: 10px;">
                            <asp:DropDownList ID="drphotel" runat="server">
                            </asp:DropDownList>
                        </td>
                        <%--</div>--%>
                    </tr>
                </tbody>
            </table>
            <table class="table" style="width: 950px; padding-left: 150px;" cellspacing="1" border="5px">
                <tbody>
                    <tr class="tableHeaderRow">
                        <td class="tableHeaderCell" colspan="2">
                            1) Your overall satisfaction regarding the venue
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Even">
                        <td width="50%" class="tableFormCell Even">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A)&nbsp;<asp:Label
                                ID="lbl1" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td width="50%" class="tableFormCell Even" style="border-left: #fff solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_State_Venue" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="lbl_poor" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lbl_excellent" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Odd">
                        <td class="tableFormCell Odd">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B)&nbsp;<asp:Label
                                ID="lbl2" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td class="tableFormCell Odd" style="border-left: #F6F6F6 solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_expectations" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="lbl_hotel_poor" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lbl_hotel_excellent" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Even">
                        <td width="50%" class="tableFormCell Even">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C)&nbsp;<asp:Label
                                ID="lbl3" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td width="50%" class="tableFormCell Even" style="border-left: #fff solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_convenience" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="Label3" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label4" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableHeaderRow">
                        <td class="tableHeaderCell" colspan="2">
                            2) Your satisfaction regarding service
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Even">
                        <td width="50%" class="tableFormCell Even">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A)&nbsp;<asp:Label
                                ID="lbl4" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td width="50%" class="tableFormCell Even" style="border-left: #fff solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_Availability_staff" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="Label1" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label2" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Odd">
                        <td class="tableFormCell Odd">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B)&nbsp;<asp:Label
                                ID="lbl5" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td class="tableFormCell Odd" style="border-left: #F6F6F6 solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_Staff_Kindness" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="Label5" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label6" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Even">
                        <td width="50%" class="tableFormCell Even">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C)&nbsp;<asp:Label
                                ID="lbl6" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td width="50%" class="tableFormCell Even" style="border-left: #fff solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_Staff_Professionalism" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="Label7" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label8" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableHeaderRow">
                        <td class="tableHeaderCell" colspan="2">
                            3) Your Satisfaction regarding Food & Beverage
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Even">
                        <td width="50%" class="tableFormCell Even">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A)&nbsp;<asp:Label
                                ID="lbl7" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td width="50%" class="tableFormCell Even" style="border-left: #fff solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_Quality_of_food" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="Label9" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label10" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Odd">
                        <td class="tableFormCell Odd">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B)&nbsp;<asp:Label
                                ID="lbl8" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td class="tableFormCell Odd" style="border-left: #F6F6F6 solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_Quality_of_coffee_breaks" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="Label11" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label12" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Even">
                        <td width="50%" class="tableFormCell Even">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C)&nbsp;<asp:Label
                                ID="lbl9" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td width="50%" class="tableFormCell Even" style="border-left: #fff solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_Original_and_Creative" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="Label13" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label14" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Odd">
                        <td width="50%" class="tableFormCell Odd">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D)&nbsp;<asp:Label
                                ID="lbl10" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td width="50%" class="tableFormCell Odd" style="border-left: #fff solid 1px;">
                            <table style="padding: 4px; width: 400px;" class="formInput">
                                <tbody>
                                    <tr style="height: 50%; vertical-align: bottom;">
                                        <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                            <asp:RadioButtonList ID="rbt_service_speed" runat="server" RepeatDirection="Horizontal"
                                                CssClass="formRadio">
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                                <asp:ListItem Value="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Selected></asp:ListItem>
                                                <asp:ListItem Value="6"></asp:ListItem>
                                                <asp:ListItem Value="7"></asp:ListItem>
                                                <asp:ListItem Value="8"></asp:ListItem>
                                                <asp:ListItem Value="9"></asp:ListItem>
                                                <asp:ListItem Value="10"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="height: 50%; vertical-align: top;">
                                        <td style="padding-left: 2px;" colspan="3">
                                            <asp:Label ID="Label15" runat="server" Text="Poor" />
                                            &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label16" runat="server" Text="Excellent" />
                                            <%--<label for="survey_answer_1">Poor</label>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table" style="width: 950px; padding-left: 150px;" cellspacing="1">
                <tbody>
                    <tr class="tableHeaderRow">
                        <td class="tableHeaderCell" colspan="3">
                           <span class="style1"> 4) Additional comments </span>
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Odd">
                        <td class="tableFormCell Odd" width="50%">
                            <asp:Label ID="lblcomments" runat="server"></asp:Label>
                        </td>
                        <td class="tableFormCellEditor Odd" width="175px" height="20px" style="border-left: #F6F6F6 solid 1px;
                            padding-left: 20px;">
                            <asp:TextBox ID="commentsText" runat="server" TextMode="MultiLine" Width="447px" />
                        </td>
                    </tr>
                    <tr class="tableFormRowEditor Odd">
                        <td class="tableFormCell Odd" width="50%" colspan="2">
                            <p>
                                <b>
                                    <asp:Label ID="lbl_appreciate" runat="server" Text="We highly appreciate your comments and feedback."
                                        Style="margin-left: 350px;" />
                                </b>
                            </p>
                            <br />
                            <p>
                                <b>
                                    <asp:Label ID="Label17" runat="server" Text="Lastminutemeetingroom support team"
                                        Style="margin-left: 350px;" /></b></p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table" style="width: 950px; padding-left: 150px" cellspacing="1">
                <tbody>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btn_submit" runat="server" Text="submit" Style="margin-left: 450px;"
                                OnClick="btn_submit_Click" />
                            <%--<input name="btnValid" value="Submit"  class="formButton formButtonMiddle"  style="margin-left:450px;" type="submit">--%>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        </form>
    </div>
</body>
</html>
