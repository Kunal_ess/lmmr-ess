﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WLCode.aspx.cs" Inherits="Test_WLCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .searchboxstyle #mainbody
        {
            min-width: 260px;
            background: pink;
            border: #000 solid 1px;
        }
        .searchboxstyle label
        {
            color: #000;
        }
        .searchboxstyle .button
        {
            background: #9fce3f;
            font-weight: bold;
            color: #fff;
            border: none;
            cursor: pointer;
            padding: 3px 10px;
        }
    </style>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        .searchboxstyle
        {
            /* background: #1386d6; 
            border: #999 solid 1px;*/ /*background: pink;
            border: #000 solid 1px;*/
        }
        .searchboxstyle #mainbody
        {
            min-width: 260px;
            background: pink;
            border: #000 solid 1px;
        }
        .searchboxstyle label
        {
            /*color: #fff;*/
            color: #000;
        }
        .searchboxstyle .button
        {
            background: #9fce3f;
            font-weight: bold;
            color: #fff;
            border: none;
            cursor: pointer;
            padding: 3px 10px;
        }
    </style>
    <script src="http://107.21.215.144/LMMR.Website/js/jquery-1.3.2.min.js" type="text/javascript"></script>
    <%--<script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="http://107.21.215.144/LMMR.Website/js/jMsAjax.js" type="text/javascript"></script>
    <script src="http://107.21.215.144/LMMR.Website/js/WS.js" type="text/javascript"></script>--%>
    <script src="Scripts/Test.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <button id="Calc" onclick="return testing();">
        Calc</button>
    <script type="text/javascript">
        function TestingByNew() {
            var key = 2;
            var jsonp = true;
            var service = new WS("http://107.21.215.144/LMMR.Website/WL" + (jsonp ? "" : "") + "/WLCommunication.asmx", jsonp ? WSDataType.jsonp : WSDataType.json);
            service.call("VerifiedClient", { WLChannelID: parseInt(key) }, function (response) {
                alert(response);
                if (response == 'verified') {

                }
                else if (response == 'deactivate') {
                    var errorHtml = '<table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;"><tr><td><b>Sorry ! your service is deactivated, please contact to LMMR support.</b></td></tr>';
                    jQuery("#search-canvas").html(errorHtml);
                }
                else {
                    var errorHtml = '<table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;"><tr><td><b>Sorry ! you are not authorised to use this service, please contact to LMMR support.</b></td></tr>';
                    jQuery("#search-canvas").html(errorHtml);
                }
            });
            return false;
        }
        function testing() {

            AjaxGet('http://107.21.215.144/LMMR.Website/WL/WLCommunication.asmx/VerifiedClient', 'WLChannelID": 2', 'mycallback', 'errocallback');
            return false;
        }
    </script>
    <%--<div class="searchboxstyle" id="search-canvas">
        <table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;">
            <tr>
                <td>
                    <select id="DropDownWLCountry" style="width: 210px" onchange="return getCiy();">
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <select id="DropDownWLCity" style="width: 210px">
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <select id="DropDownWLDate" style="width: 40px">
                    </select>
                    <select id="DropDownWLMonth" style="width: 105px">
                    </select>
                    <select id="DropDownWLYear" style="width: 60px">
                    </select>
                    <input id="TextBoxWLDate" type="text" value="12/03/2012" style="width: 80px" />
                    <img src="date-icon.png" align="absmiddle" id="datepicker" onclick="ShowCalander();">
                </td>
            </tr>
            <tr>
                <td>
                    <table width="98%" height="27" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td width="43%">
                                <label>
                                    Duration</label>
                                <select id="DropDownWLDuration" style="width: 35px" onchange="return ShowHideWLDayDropDown();">
                                    <option value="1" selected="selected">1</option>
                                    <option value="2">2</option>
                                </select>
                            </td>
                            <td width="57%">
                                <label>
                                    Day 1</label>
                                <select id="DropDownWLDay1" style="width: 80px">
                                    <option value="0">Full day</option>
                                    <option value="1">Morning</option>
                                    <option value="2">Afternoon</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trWLDay2" style="display: none">
                <td>
                    <table width="98%" height="27" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td width="43%">
                                &nbsp;
                            </td>
                            <td width="57%">
                                <label>
                                    Day 2</label>
                                <select id="DropDownWLDay2" style="width: 80px">
                                    <option value="0">Full day</option>
                                    <option value="1">Morning</option>
                                    <option value="2">Afternoon</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        Meeting delegates</label>
                    <input type="text" style="width: 40px" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="submit" class="button" value="Find your meeting room !" onclick="return WLValidation();" />
                </td>
            </tr>
        </table>
    </div>--%>
    <div class="searchboxstyle" id="search-canvas">
    </div>
    </form>
</body>
</html>
