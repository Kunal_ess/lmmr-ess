﻿
//jQuery(document).ready(function(){CrossDomainTesting();});

var WLTokenKey;
var scripts = document.getElementsByTagName("script");
var script = scripts[scripts.length - 1];

jQuery(document).ready(function () {
    for (var i = 0, len = scripts.length; i < len; i++) {

        var src = jQuery(scripts[i]).attr('src');
        if (src != undefined) {

            var arr = src.split('?');
            var url = arr[0];
            var args = arr[1];
            if (args == undefined) {
                continue;
            }
            else {

                var argv = args.split("&");
                for (var j = 0, argc = argv.length; j < argc; j++) {
                    var pair = argv[j].split("=");
                    var argName = pair[0];
                    var argValue = pair[1];

                    if (argName == "w") {
                        WLTokenKey = argValue;
                        LoadWL();
                        break;
                        //alert(argValue);
                    }
                }
            }
        }
    } 
});

function LoadWL() {
    CheckPermission();
}

function TestingByNew()
{
    var key = WLTokenKey;
    var jsonp = true;
    var service = new WS("Services" + (jsonp ? "/" : "") + "/WLCommunication.asmx", jsonp ? WSDataType.jsonp : WSDataType.json);
    service.call("VerifiedClient", { WLChannelID: parseInt(key) }, function (response) {
        if (response == 'verified') {
            WLSearchCanvasHTML();
        }
        else if (response == 'deactivate') {
            var errorHtml = '<table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;"><tr><td><b>Sorry ! your service is deactivated, please contact to LMMR support.</b></td></tr>';
            jQuery("#search-canvas").html(errorHtml);
        }
        else {
            var errorHtml = '<table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;"><tr><td><b>Sorry ! you are not authorised to use this service, please contact to LMMR support.</b></td></tr>';
            jQuery("#search-canvas").html(errorHtml);
        }
    });

}


function CheckPermission() {
    var key = WLTokenKey;

    var url = $.jmsajaxurl({
        url: "http://107.21.215.144/LMMR.Website/WL/WLCommunication.asmx",
        method: "VerifiedClient",
        data: { WLChannelID: parseInt(key) }
    });

    $.ajax({
        cache: false,
        dataType: "jsonp",
        success: function (d) {
            var response = d.d;
            if (response == 'verified') {
                WLSearchCanvasHTML();
            }
            else if (response == 'deactivate') {
                var errorHtml = '<table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;"><tr><td><b>Sorry ! your service is deactivated, please contact to LMMR support.</b></td></tr>';
                jQuery("#search-canvas").html(errorHtml);
            }
            else {
                var errorHtml = '<table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;"><tr><td><b>Sorry ! you are not authorised to use this service, please contact to LMMR support.</b></td></tr>';
                jQuery("#search-canvas").html(errorHtml);
            }
        },
        url: url + "&format=json"
    });
}

function WLSearchCanvasHTML() {

    var url = $.jmsajaxurl({
        url: "http://107.21.215.144/LMMR.Website/WL/WLCommunication.asmx",
        method: "SendSearchHTML",
        data: {}
    });

    $.ajax({
        cache: false,
        dataType: "jsonp",
        success: function (d) {
            var response = d.d;
            jQuery('#search-canvas').html(response);

            jQuery('#DropDownWLCountry').append($("<option></option>").val('0').html('--Select country--'));
            jQuery('#DropDownWLCity').append($("<option></option>").val('0').html('--Select city--'));
            BindWLDateDropdown();
            BindWLMonthDropDown();
            BindWLYearDropDown();
            WLSetTodayDate();
            getCountry();

        },
        url: url + "&format=json"
    });
}



function getCountry() {
    var key = WLTokenKey;
    var url = $.jmsajaxurl({
        url: "http://107.21.215.144/LMMR.Website/WL/WLCommunication.asmx",
        method: "GetCountryData",
        data: { WLChannelID: parseInt(key) }
    });

    $.ajax({
        cache: false,
        dataType: "jsonp",
        success: function (d) {
            var result = JSON.parse(msg.d);
            jQuery.each(result, function () {
                jQuery('#DropDownWLCountry').append(jQuery("<option></option>").val(this['Id']).html(this['CountryName']));
            });
        },
        url: url + "&format=json"
    });
}

function getCiy() {

    var key = WLTokenKey;
    var countryID = jQuery('#DropDownWLCountry').val();
    var url = $.jmsajaxurl({
        url: "http://107.21.215.144/LMMR.Website/WL/WLCommunication.asmx",
        method: "GetCityData",
        data: { WLChannelID: parseInt(key), WLCountryID: parseInt(countryID) }
    });

    $.ajax({
        cache: false,
        dataType: "jsonp",
        success: function (d) {
            var result = JSON.parse(msg.d);
            if (result != null) {
                jQuery.each(result, function () {
                    jQuery('#DropDownWLCity').append(jQuery("<option></option>").val(this['Id']).html(this['City']));
                });
            }
            else {
                jQuery('#DropDownWLCity').empty();
                jQuery('#DropDownWLCity').append($("<option></option>").val('0').html('--Select city--'));
            }
        },
        url: url + "&format=json"
    });
}





function ShowHideWLDayDropDown() {

    var selectedDay = jQuery('#DropDownWLDuration').val();
    if (selectedDay == '2') {
        jQuery('#DropDownWLDay2').val('0');
        jQuery('#trWLDay2').show();

    }
    else {
        jQuery('#trWLDay2').hide();
    }

}

function CrossDomainTesting() {
    alert('I');


    jQuery.getJSON("http://107.21.215.144/LMMR.Website/WL/WLCommunication.asmx/Foo?jsoncallback=?",
    function (data) {
        alert(data);
    });

    jQuery("<iframe src='http://107.21.215.144/LMMR.Website/WL/Communication.aspx?id=2' />").appendTo('body');
}

function CodeProject() {
    var url = $.jmsajaxurl({
        url: "http://107.21.215.144/LMMR.Website/WL/WLCommunication.asmx",
        method: "SendSearchHTML",
        data: {}
    });

    $.ajax({
        cache: false,
        dataType: "jsonp",
        success: function (d) { alert(d.d); },
        url: url + "&format=json"
    });

}
function WLValidation() {
    var dropWLcountry = jQuery('#DropDownWLCountry').val();
    if (dropWLcountry == "0" || dropWLcountry == "") {
        alert('Please select country');
        return false;
    }
    var dropWLcity = jQuery('#DropDownWLCity').val();
    if (dropWLcity == "0" || dropWLcity == "") {
        alert('Please select city');
        return false;
    }


    var txtDelegate = jQuery('#TextBoxDelegate').val();
    if (txtDelegate == "0" || txtDelegate == "") {
        alert('Please enter valid number of delegate');
        return false;
    }
}
function WLSetTodayDate() {
    var d = new Date();
    jQuery('#DropDownWLDate').val(d.getDate());
    jQuery('#DropDownWLMonth').val(d.getMonth());
    jQuery('#DropDownWLYear').val(d.getFullYear());
}

function BindWLDateDropdown() {
    var firstDate = 1;
    var lastDate = 31;
    for (firstDate = 1; firstDate <= 31; firstDate++) {
        jQuery('#DropDownWLDate').append($("<option></option>").val(firstDate).html(firstDate));
    }

}

function BindWLYearDropDown() {
    var d = new Date();
    var firstYear = d.getFullYear();
    var ToYear = parseInt(firstYear) + 20;
    for (firstYear = parseInt(firstYear); firstYear <= ToYear; firstYear++) {
        jQuery('#DropDownWLYear').append($("<option></option>").val(firstYear).html(firstYear));
    }
}

function BindWLMonthDropDown() {
    var m_names = new Array("January", "February", "March",
"April", "May", "June", "July", "August", "September",
"October", "November", "December");
    var firstMonth = 0;
    for (firstMonth = 0; firstMonth <= m_names.length - 1; firstMonth++) {
        jQuery('#DropDownWLMonth').append($("<option></option>").val(firstMonth).html(m_names[firstMonth]));
    }
}



