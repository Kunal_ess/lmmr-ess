﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Test_WLCode : System.Web.UI.Page
{
    //System.Web.HttpRequest Request;

    protected void Page_Load(object sender, EventArgs e)
    {
        //GetDom("pr");
        
    }

    public static string BaseSiteUrl
    {
        get
        {
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + '/';
            return baseUrl;
        }
    }
    public string  GetDom(string name)
    {
        //if (Uri.CheckHostName(name) != UriHostNameType.Unknown)
        //{
        //    return true;
        //}
        //else
        //{
        //    return false;
        //}
        //return FullyQualifiedApplicationPath;


        return BaseSiteUrl;
    }

    public string FullyQualifiedApplicationPath
    {
        get
        {
            //Return variable declaration
            var appPath = string.Empty;

            //Getting the current context of HTTP request
            var context = HttpContext.Current;

            //Checking the current context content
            if (context != null)
            {
                //Formatting the fully qualified website url/name
                appPath = string.Format("{0}://{1}{2}{3}",
                                        context.Request.Url.Scheme,
                                        context.Request.Url.Host,
                                        context.Request.Url.Port == 80
                                            ? string.Empty
                                            : ":" + context.Request.Url.Port,
                                        context.Request.ApplicationPath);
            }

            if (!appPath.EndsWith("/"))
                appPath += "/";

            return appPath;
        }
    }

}