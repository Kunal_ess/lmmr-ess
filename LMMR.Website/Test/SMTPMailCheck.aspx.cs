﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Sockets;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using LMMR.Business;
using System.Configuration;
public partial class Test_SMTPMailCheck : System.Web.UI.Page
{
    SendMails obj = new SendMails();
    protected void Page_Load(object sender, EventArgs e)
    {
        //string myMail = "facebook.com";
        //// ValidSMTP(myMail);
        //obj.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
        //obj.ToEmail = "shantakaram1@essindia.co.in";
        //string bodymsg2 = "Me ek test mail hu.me aaya hu aapka mail check karne.";

        //obj.Subject = "test mail";

        //obj.Body = bodymsg2;
        //if (obj.SendMail() == "Send Mail")
        //{
        //    //bool boolResult = true;
        //}


        //CreateTestMessage4();
        //string[] host = (address.Split('@'));
        //string hostname = host[1];
        //Uri uri = (Uri)myMail;//HttpContext.Current.Request.Url;
        //string host = uri.Host;
        //int port = uri.Port;

        //var server = Dns.GetHostEntry(myMail);
        //IdnMapping idn = new IdnMapping();
        //try
        //{
        //    string domainName = idn.GetAscii(myMail);
        //}
        //catch
        //{

        //}

        //IPHostEntry IPhst = Dns.GetHostEntry(myMail); ;
        //IPEndPoint endPt = new IPEndPoint(IPhst.AddressList[0], 25);
        //Socket s = new Socket(endPt.AddressFamily,
        //        SocketType.Stream, ProtocolType.Tcp);
        //s.Connect(endPt);

        double x1 = 4.356677;
        double y1 = 50.845739;
        double x2 = 4.344317380859366;
        double y2 = 50.91074854525164;

       double result =  distance(x1, y1, x2, y2);
    }
    public static void CreateTestMessage4(string server)
    {
        MailAddress from = new MailAddress("pranayesh-pathak@essindia.co.in");
        MailAddress to = new MailAddress("pranayeshpathak2009@gmail.com");
        MailMessage message = new MailMessage(from, to);
        message.Subject = "Using the SmtpClient class.";
        message.Body = @"Using this feature, you can send an e-mail message from an application very easily.";
        SmtpClient client = new SmtpClient(server);
        Console.WriteLine("Sending an e-mail message to {0} by using SMTP host {1} port {2}.",
             to.ToString(), client.Host, client.Port);

        try
        {
            client.Send(message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught in CreateTestMessage4(): {0}",
                  ex.ToString());
        }
    }
    private bool ValidSMTP(string hostName)
    {
        var server = Dns.GetHostEntry(hostName);
        bool valid = false;
        try
        {
            TcpClient smtpTest = new TcpClient();
            for (int i = 0; i <= server.AddressList.Length - 1; i++)
            {
                smtpTest.Connect(hostName, 25);
            }
            if (smtpTest.Connected)
            {
                NetworkStream ns = smtpTest.GetStream();
                StreamReader sr = new StreamReader(ns);
                if (sr.ReadLine().Contains("220"))
                {
                    valid = true;
                }
                smtpTest.Close();
            }
        }
        catch
        {

        }
        return valid;
    }



    public double distance(double X1, double Y1, double X2, double Y2)
    {
        double R = 6371;
        double dLat = this.toRadian(Y2 - Y1);
        double dLon = this.toRadian(X2 - X1);
        double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(this.toRadian(Y1)) * Math.Cos(this.toRadian(Y2)) * Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
        double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));
        double d = R * c;

        return d;
    }

    private double toRadian(double val)
    {
        return (Math.PI / 180) * val;
    }


}