﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GoogleMapSearchRadius.aspx.cs"
    Inherits="Test_GoogleMapSearchRadius" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript">
        function initialize() {

            var mapDiv = document.getElementById('map-canvas');
            philly = new google.maps.LatLng(39.95, -75.16)

            map = new google.maps.Map(mapDiv, {
                center: philly,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                navigationControl: true,
                navigationControlOptions: {
                style: google.maps.NavigationControlStyle.SMALL
                }
            });

            google.maps.event.addListenerOnce(map, 'idle', addObjects);

        }

        function addObjects() {
            var circle = {
                strokeColor: "#ff0000",
                strokeOpacity: 0.8,
                strokeWeight: 1,
                fillColor: "#ff0000",
                fillOpacity: 0.20,
                map: map,
                center: philly,
                radius: 16090 * 2
            };

            var drawCirle = new google.maps.Circle(circle);
            var icon = 'http://kahimyang.info/resources/funny/marker.png';

            var phillyMarker = new google.maps.Marker({
                position: philly,
                map: map,
                title: 'Click here to open info window',
                icon: icon
            });

            var phillyContent = '<span style="font-size:10px;">Philadelphia, PA</span>';
            var phillyInfo = new google.maps.InfoWindow({
                content: phillyContent,
                maxWidth: 180
            });

            google.maps.event.addListener(phillyMarker, 'click', function () {
              phillyInfo.open(map, this);
          });

            google.maps.event.addListener(phillyInfo, 'closeclick', function () {
               reCenterMap();
           });

            phillyInfo.open(map, phillyMarker);

        }
        function reCenterMap() {
            map.setCenter(philly);
            map.setZoom(8);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="map-canvas" style="width: 500px; height: 350px">
    </div>
    </form>
</body>
</html>
