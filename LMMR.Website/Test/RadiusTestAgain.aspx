﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RadiusTestAgain.aspx.cs"
    Inherits="Test_RadiusTestAgain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Google Map Radius</title>
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="map" style="width: 700px; height: 600px;">
    </div>
    <script type="text/javascript">
        var marker, i;
        var markersArray = [];
        var circleArray;
        var infowindow = new google.maps.InfoWindow();
        var locations = [
      ['Taj Palace',27.1807734,78.0180529, 1],
      ['Hyatt Regency',28.635308,77.22496, 2],
      ['Le Meridien',28.63531,77.22496, 3],
      ['Centaur Hotel',28.5499902,77.1045895, 4],
      ['The Lalit',28.6289271,77.2205061, 5],
      ['The Park Hotel Parliament Street',28.6211415,77.2111296, 6],
      ['Radisson Hotel National Highway',28.52907,77.28827, 7]

    ];
            var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        codeAddress();

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }

        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
        });

        function clearOverlays() {
            if (markersArray && circleArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
            }
        }

        function clearCircle() {
            if (circleArray) {
                for (i in circleArray) {
                    circleArray.setMap(null);
                }
            }
        }

        function placeMarker(location) {
            var geocoder;
            var marker;
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': location }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        clearOverlays();
                        clearCircle();
                        marker = new google.maps.Marker({
                            position: location,
                            title: results[1].formatted_address,
                            map: map,
                            //animation: google.maps.Animation.DROP
                        });
                        infowindow.setContent(results[1].formatted_address);
                        infowindow.open(map, marker);

                        markersArray.push(marker);
                        map.setCenter(location);

                        //mouseover
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.setContent(results[1].formatted_address);
                            infowindow.open(map, this);
                        });

                        circleArray = new google.maps.Circle({
                            map: map,
                            radius: 2*1609,    // 1 miles in metres
                            fillColor: '#AA0000'
                        });

                        circleArray.bindTo('center', marker, 'position');
                        
                    }
                } else {
                    alert("Geocoder failed due to: " + status);
                }
            });

        }
        function codeAddress() {
        var geocoder;
            var marker;
            geocoder = new google.maps.Geocoder();
            var address = "Delhi India";
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    clearOverlays();

                    map.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: map,
                        title: results[0]['formatted_address'],
                        position: results[0].geometry.location
                        //animation: google.maps.Animation.DROP
                    });

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);

                    markersArray.push(marker);

                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

    </script>
    </form>
</body>
</html>
