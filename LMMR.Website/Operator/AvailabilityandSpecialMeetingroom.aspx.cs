﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

public partial class Operator_AvailabilityandSpecialMeetingroom : System.Web.UI.Page
{
    public AvailabilityAndSpecialManager am = new AvailabilityAndSpecialManager();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Check session exist or not
        if (Session["CurrentOperatorID"] == null)
        {
            Response.Redirect("~/login.aspx", true);
        }
        search.SearchButtonClick += new EventHandler(buttonSearch_Click);
        search.CancelButtonClick += new EventHandler(buttonCancel_Click);
        if (!Page.IsPostBack)
        {
            search.MinFromDate = DateTime.Now;
            search.MaxFromDate = DateTime.Now.AddDays(60);
            search.MinToDate = DateTime.Now;
            search.MaxToDate = DateTime.Now.AddDays(60);
            lstSort.Items.Add(new ListItem("discount", "1"));
            lstSort.Items.Add(new ListItem("no. of meetingroom", "2"));
            lstSort.Items.Add(new ListItem("no. of bedroom", "3"));
            lstSort.Items.Add(new ListItem("% occupancy", "4"));
            hdnOrder.Value = "1,2,3,4";
            ViewState["Where"] = "";
            BindCalanderWithList(string.Empty, string.Empty);
            //BindCalander();
            Page.RegisterStartupScript("aa", "<script language='javascript' >CallMealways();</script>");
        }
        else
        {
            Page.RegisterStartupScript("aa", "<script language='javascript' >CallMealways();</script>");
        }
    }
    public void buttonCancel_Click(object sender, EventArgs e)
    {
        ViewState["Where"] = "";
        BindCalanderWithList(string.Empty, string.Empty);
        Page.RegisterStartupScript("aa", "<script language='javascript' >CallMealways();</script>");
    }

    public void buttonSearch_Click(object sender, EventArgs e)
    {
        string where = string.Empty;
        if (search.propCountryID != 0)
        {
            if (where.Length > 0)
            {
                where += " and ";
            }
            where += ViewAvailabilityAndSpecialManagerColumn.CountryId + "='" + search.propCountryID + "' ";
        }
        if (search.propCityID != 0)
        {
            if (where.Length > 0)
            {
                where += " and ";
            }
            where += ViewAvailabilityAndSpecialManagerColumn.CityId + "='" + search.propCityID + "' ";
        }
        if (search.propFromDate != DateTime.MinValue && search.propToDate != DateTime.MinValue)
        {
            if (where.Length > 0)
            {
                where += " and ";
            }
            where += ViewAvailabilityAndSpecialManagerColumn.SpecialPriceDate + " between '" + search.propFromDate + "' and '" + search.propToDate + "' ";
            startDate = search.propFromDate;
            endDate = search.propToDate;
        }
        if (!string.IsNullOrEmpty(search.Venus))
        {
            if (where.Length > 0)
            {
                where += " and ";
            }
            where += ViewAvailabilityAndSpecialManagerColumn.Name + " like '" + search.Venus + "%' ";
        }
        if (where.Length > 0)
        {
            where += " and ";
        }
        where += ViewAvailabilityAndSpecialManagerColumn.GoOnline + "=1 AND " + ViewAvailabilityAndSpecialManagerColumn.IsRemoved + "=0 ";
        ViewState["Where"] = where;
        BindCalanderWithList(where, string.Empty);
        Page.RegisterStartupScript("aa", "<script language='javascript' >CallMealways();</script>");
    }
    public void BindCalander()
    {
        int currentMonth = 0;
        int previousMonth = DateTime.Now.Month;
        int currentColSpan = 0;
        ltrTableCalander.Text = "<table border='0' cellspacing='1' cellpadding='0' id='myTable' bgcolor='#a3d4f7'>";
        for (int j = 0; j < 100; j++)
        {
            if (j == 0)
            {
                ltrTableCalander.Text += "<tr style='background-color:#fff;border:1px solid #A3D4F7;' >";
            }
            currentMonth = DateTime.Now.AddDays(j).Month;
            currentColSpan++;
            if (previousMonth != currentMonth)
            {
                ltrTableCalander.Text += "<td colspan=" + (currentColSpan - 1) + " class='DateSelection'>" + Enum.GetName(typeof(MonthName), previousMonth) + " " + DateTime.Now.AddDays(j).Year + "</td>";
                currentColSpan = 1;
                previousMonth = currentMonth;
            }
            if (j == 99)
            {
                ltrTableCalander.Text += "<td colspan=" + (currentColSpan) + " class='DateSelection'>" + Enum.GetName(typeof(MonthName), previousMonth) + " " + DateTime.Now.AddDays(j).Year + "</td>";
                currentColSpan = 0;
                ltrTableCalander.Text += "</tr>";
            }
        }
        ltrTableCalander.Text += "<tr bgcolor='#fff' class='cal_header'>";
        for (int i = 0; i < 100; i++)
        {
            ltrTableCalander.Text += "<td>"+Enum.GetName(typeof(DayName),(int)DateTime.Now.AddDays(i).DayOfWeek)+"<br/>"+DateTime.Now.AddDays(i).Day+"</td>";
        }
        ltrTableCalander.Text += "</tr>";
        for (int h = 0; h < 20; h++)
        {
            ltrTableCalander.Text += "<tr class='raw1'><td colspan='"+100+"' height='22'><span style='position:relative;'>hotal name " + (h + 1) + "</span></td></tr>";
            ltrTableCalander.Text += "<tr class='raw2'>";
            for (int k = 0; k < 100; k++)
            {
                ltrTableCalander.Text += "<td height='35'>" + k + "</td>";
            }
            ltrTableCalander.Text += "</tr>";
            ltrTableCalander.Text += "<tr class='raw4'>";
            for (int k = 0; k < 100; k++)
            {
                int discountcount = (int)(40 - (40 * k / 100.0));
                ltrTableCalander.Text += "<td valign='top' bgcolor='#FF0000'><div class='adjust-main'><div class='adjust-first' style='height:"+discountcount+"px;'><div class='adjust-inner'>"+(k+1)+"%</div></div></div></td>";
            }
            ltrTableCalander.Text += "</tr>";
        }
        ltrTableCalander.Text += "</table>";
        //ltrTableCalander.Text = "";
    }
    public string Arraylst
    {
        get
        {
            return ViewState["Arraylst"]==null?"":Convert.ToString(ViewState["Arraylst"]);
        }
        set
        {
            ViewState["Arraylst"] = value;
        }
    }
    public string WhereClauses
    {
        get
        {
            return ViewState["WhereClauses"] == null ? "" : Convert.ToString(ViewState["WhereClauses"]);
        }
        set
        {
            ViewState["WhereClauses"] = value;
        }
    }
    public DateTime startDate
    {
        get
        {
            return ViewState["startDate"] == null ? DateTime.Now : Convert.ToDateTime(ViewState["startDate"]);
        }
        set
        {
            ViewState["startDate"] = value;
        }
    }
    public DateTime endDate
    {
        get
        {
            return ViewState["endDate"] == null ? DateTime.Now.AddDays(59) : Convert.ToDateTime(ViewState["endDate"]);
        }
        set
        {
            ViewState["endDate"] = value;
        }
    }
    public void BindCalanderWithList(string whereClause, string OrderBy)
    {
        Arraylst = "";
        VList<ViewAvailabilityAndSpecialManager> vAvailabilitySpecial = new VList<ViewAvailabilityAndSpecialManager>();
        if (!Page.IsPostBack)
        {
            if (string.IsNullOrEmpty(OrderBy))
            {
                OrderBy += "Name Asc,";
            }
        }
        foreach (string s in hdnOrder.Value.Split(','))
        {
            switch (hdnOrder.Value.Split(',')[0])
            {
                case "1": OrderBy += "p.OccopanctMr DESC,";
                    break;
                case "2": OrderBy += "p.BookedMr DESC,";
                    break;
                case "3": OrderBy += "p.bookedBr DESC,";
                    break;
                case "4": OrderBy += "p.percentDDR ASC,";
                    break;
                default: OrderBy += "p.SpecialPriceDate ASC,";
                    break;
            }
            break;
        }
        if (string.IsNullOrEmpty(whereClause))
        {
            vAvailabilitySpecial = am.GetByPaged(ViewAvailabilityAndSpecialManagerColumn.GoOnline + "=1 AND " + ViewAvailabilityAndSpecialManagerColumn.IsRemoved + "=0 and " + ViewAvailabilityAndSpecialManagerColumn.SpecialPriceDate + " between '" + search.propFromDate + "' and  '" + search.propToDate + "'", OrderBy + "SpecialPriceDate ASC", 0, int.MaxValue);
            WhereClauses = ViewAvailabilityAndSpecialManagerColumn.GoOnline + "=1 AND " + ViewAvailabilityAndSpecialManagerColumn.IsRemoved + "=0 and " + ViewAvailabilityAndSpecialManagerColumn.SpecialPriceDate + " between '" + search.propFromDate + "' and  '" + search.propToDate + "'";
            startDate = search.propFromDate;
            endDate = search.propToDate;
        } 
        else
        {
            vAvailabilitySpecial = am.GetByPaged(whereClause, OrderBy + "p.Id, SpecialPriceDate ASC", 0, int.MaxValue);
            WhereClauses = whereClause;
            
        }
        var Vfirst = vAvailabilitySpecial.Select(a => a.SpecialPriceDate).FirstOrDefault();
        List<DateTime?> ListDays = vAvailabilitySpecial.Select(a => a.SpecialPriceDate).Distinct().ToList();
        List<HotelAvailable> ListHotel = vAvailabilitySpecial.Select(a => new HotelAvailable { IDs = a.Id, HName = a.Name }).Distinct(new DistinctItemComparerAvail()).ToList();
        if (Vfirst != null)
        {
            int currentMonth = 0;
            int previousMonth = Vfirst.Value.Month;
            int currentColSpan = 0;
            ltrTableCalander.Text = "<table border='0' cellspacing='1' cellpadding='0' id='myTable' bgcolor='#a3d4f7' title='On the click appears a sorting window.' onclick='openDiv();' class='information'>";
            int count = 0;
            //var listOfDays = vAvailabilitySpecial.FindAllDistinct(ViewAvailabilityAndSpecialManagerColumn.SpecialPriceDate);
            //var listOfHotels = vAvailabilitySpecial.FindAllDistinct(ViewAvailabilityAndSpecialManagerColumn.Id);
            
            string DateCollection = string.Empty;
            for(int vdate = 0; vdate < ListDays.Count(); vdate++)
            {
                if (count == 0)
                {
                    ltrTableCalander.Text += "<tr style='background-color:#fff;border:1px solid #A3D4F7;' >";
                }

                currentMonth = ListDays[vdate].Value.Month;
                currentColSpan++;
                if (previousMonth != currentMonth)
                {
                    ltrTableCalander.Text += "<td colspan=" + (currentColSpan - 1) + " class='DateSelection'>" + Enum.GetName(typeof(MonthName), previousMonth) + " " + ListDays[vdate].Value.Year + "</td>";
                    currentColSpan = 1;
                    previousMonth = currentMonth;
                }
                if (count == ListDays.Count() - 1)
                {
                    ltrTableCalander.Text += "<td colspan=" + (currentColSpan) + " class='DateSelection'>" + Enum.GetName(typeof(MonthName), previousMonth) + " " + ListDays[vdate].Value.Year + "</td>";
                    currentColSpan = 0;
                    ltrTableCalander.Text += "</tr>";
                }
                count++;
                DateCollection += "<td>" + Enum.GetName(typeof(DayName), (int)ListDays[vdate].Value.DayOfWeek) + "<br/>" + ListDays[vdate].Value.Day + "</td>"; ;
            }
            ltrTableCalander.Text += "<tr bgcolor='#fff' class='cal_header'>";
            if (DateCollection.Length > 0)
            {
                ltrTableCalander.Text += DateCollection;
            }
            ltrTableCalander.Text += "</tr>";
            //int counter = 0;
            Arraylst = "[";
            for(int v=0;v<ListHotel.Count;v++)
            {
                //var myList = vAvailabilitySpecial.Where(a => a.Id == v.Id);
                ltrTableCalander.Text += "<tr class='raw1' id='tblrow"+ListHotel[v].IDs+"'><td colspan='" + ListDays.Count() + "' height='22'><span style='position:relative;'>" + ListHotel[v].HName + "</span></td></tr>";
                Arraylst += ListHotel[v].IDs + ",";
                //ltrTableCalander.Text += "<tr class='raw2'>";
                //foreach (DateTime vcontent in ListDays)
                //{
                //    ViewAvailabilityAndSpecialManager vm = vAvailabilitySpecial.Where(a => a.Id == ListHotel[v].IDs && a.SpecialPriceDate == vcontent).FirstOrDefault();
                //    if (vm != null)
                //    {
                //        ltrTableCalander.Text += "<td height='35'>" + (vm.DdrPercent == 0 ? "" : Math.Round(vm.DdrPercent.Value, 2).ToString()) + "<br/>" + Math.Round(vm.BookedMeetingRoom.Value, 1) + "/" + vm.MeetingroomCount + "<br/>" + vm.BookedBedroom.Value + "/" + vm.BedroomCount + "</td>";
                //    }
                //    else
                //    {
                //        ltrTableCalander.Text += "<td height='35'>N/A</td>";
                //    }
                //}
                //ltrTableCalander.Text += "</tr>";
                //ltrTableCalander.Text += "<tr class='raw4'>";

                //for (int vdate = 0; vdate < ListDays.Count(); vdate++)
                //{
                //    ViewAvailabilityAndSpecialManager vm = vAvailabilitySpecial.Where(a => a.Id == ListHotel[v].IDs && a.SpecialPriceDate == ListDays[vdate].Value).FirstOrDefault();
                //    if (vm != null)
                //    {
                //        int discountcount = (int)(40 - (40 * (vm.BookedMeetingRoom.Value / (vm.MeetingroomCount.Value == 0 ? 1 : vm.MeetingroomCount.Value))));
                //        ltrTableCalander.Text += "<td valign='top' bgcolor='#FF0000'><div class='adjust-main'><div class='adjust-first' style='height:" + discountcount + "px;'><div class='adjust-inner'>" + Math.Round(((vm.BookedMeetingRoom.Value / (vm.MeetingroomCount.Value == 0 ? 1 : vm.MeetingroomCount.Value)) * 100), 2) + "%</div></div></div></td>";
                //    }
                //    else
                //    {
                //        ltrTableCalander.Text += "<td valign='top' bgcolor='#FF0000'><div class='adjust-main'><div class='adjust-first-disable' style='height:40px;'><div class='adjust-inner'></div></div></div></td>";
                //    }
                //}
                //ltrTableCalander.Text += "</tr>";
                //counter++;
                //if (counter == 10)
                //{
                //    break;
                //}
            }
            if (Arraylst.Length > 1)
            {
                Arraylst = Arraylst.Substring(0, Arraylst.Length - 1);
            }
            Arraylst += "]";
            ltrTableCalander.Text += "</table>";
        }
        else
        {
            ltrTableCalander.Text = "<div class='warning'>No record found!</div>";
        }
    }

    public void lnkSort_Click(object sender, EventArgs e)
    {
        lstSort.Items.Clear();
        foreach (string s in hdnOrder.Value.Split(','))
        {
            switch (s)
            {
                case "4": lstSort.Items.Add(new ListItem("% occupancy", "4"));
                    break;
                case "2": lstSort.Items.Add(new ListItem("no. of meetingroom", "2"));
                    break;
                case "3": lstSort.Items.Add(new ListItem("no. of bedroom", "3"));
                    break;
                case "1": lstSort.Items.Add(new ListItem("discount", "1"));
                    break;
            }
        }
        BindCalanderWithList(Convert.ToString(ViewState["Where"]), string.Empty);
        Page.RegisterStartupScript("aa", "<script language='javascript' >CallMealways();</script>");
    }
}

public enum DayName
{
    SU =0,
    MO=1,
    TU=2,
    WE=3,
    TH=4,
    FR=5,
    ST=6
}

public enum MonthName
{
    January = 1,
    February = 2,
    March = 3,
    April = 4,
    May = 5,
    June = 6,
    July = 7,
    August = 8,
    September = 9,
    October = 10,
    November = 11,
    December = 12
}

public class HotelAvailable
{
    public long IDs
    {
        get;
        set;
    }
    public string HName
    {
        get;
        set;
    }
}
class DistinctItemComparerAvail : IEqualityComparer<HotelAvailable>
{

    public bool Equals(HotelAvailable x, HotelAvailable y)
    {
        return x.IDs == y.IDs &&
            x.HName == y.HName;
    }

    public int GetHashCode(HotelAvailable obj)
    {
        return obj.IDs.GetHashCode() ^
            obj.HName.GetHashCode();
    }
}