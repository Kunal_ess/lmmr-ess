﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true" CodeFile="NewAdminLink.aspx.cs" Inherits="Operator_NewAdminLink" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="contract-list" id="divAlphabeticPaging" runat="server" style="margin-top: 20px;">
                <div class="contract-list-left" style="width: 278px;">
                    <h2>
                        Add Admin</h2>
                </div>                
            </div><br />
 <div class="operator-mainbody-1"  >
<div id="divmessage" runat="server" class="error">
            </div>     
<table width="600px" border="0" cellspacing="0"  style="border: 1px solid #A3D4F7; background:#ECF7FE; padding:10px" cellpadding="5" >
  <tr>
    <td width="50%">Enter the Admin contact person</td>
    <td width="50%">
        <asp:TextBox ID="txtContactPerson" runat="server" class="inputbox13" MaxLength="50"></asp:TextBox></td>
  </tr>
  <tr>
    <td>Enter the Admin email account</td>
    <td><asp:TextBox ID="txtEmailAccount" runat="server" class="inputbox13" MaxLength="50"></asp:TextBox></td>
  </tr>
  <tr>
    <td>Enter the Admin phone no</td>
    <td><asp:TextBox ID="txtPhoneNumber" runat="server" class="inputbox13" MaxLength="20"></asp:TextBox>
    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" TargetControlID="txtPhoneNumber"
                                ValidChars="0123456789" runat="server">
                            </asp:FilteredTextBoxExtender></td>
  </tr>
  
    <tr>
    <td valign="top">List of all hotels for that client</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="100%" style="text-align:left">
      <asp:Panel ID="Panel1" runat="server" Height="200px" ScrollBars="Vertical">    
    <asp:CheckBoxList ID="chkHotelName" runat="server" CellPadding="5">                              
    </asp:CheckBoxList>
      </asp:Panel>  
       
    </td>
  </tr>  
</table>
</td>
  </tr>  
 <tr>
    <td>&nbsp;</td>
    <td> <div class="button_section">
                            <asp:LinkButton ID="btnSubmit" CssClass="select" runat="server" Text="Save"
                                ValidationGroup="LOGIN" onclick="btnSubmit_Click" />
                            <span>or</span>
                            <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" 
                                onclick="btnCancel_Click" />
                        </div></td>
    <td>
    </td>
  </tr>
</table>







          
          </div>
          <!-- end search-operator-layout-->
    
      
          <!--main-body start -->


                
        
		   
		  
                  
 <div class="operator-mainbody">
 <div id="divMessageOnGridHeader" runat="server">
                </div>
<table width="100%" cellspacing="1" cellpadding="5" border="0" bgcolor="#98BCD6">
  <tbody>
  <tr bgcolor="#CCD8D8">
  <td valign="top" width="25%">Admin Name</td>
    <td valign="top" width="25%">EmailId</td>
    <td valign="top" width="25%">Phone Number</td>    
    <td valign="top" width="15%">List of Hotels assigned</td>
    <td valign="top" width="10%">Active/Deactive</td>
  </tr> 
  <asp:GridView ID="grvAdminLink" runat="server" Width="100%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="false" 
          DataKeyNames="UserId" GridLines="None"
                    ShowHeader="false" ShowFooter="false" BackColor="#98BCD6" 
          RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"                    
                    EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                    HeaderStyle-BackColor="#98BCD6" 
          onrowdatabound="grvAdminLink_RowDataBound">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="25%">
                            <ItemTemplate>                                
                                <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                            </ItemTemplate>     
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:Label ID="lblPhoneNumber" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>                                
                                 <asp:LinkButton ID="lnbHotelName" runat="server" Text="Assigned Hotels" CommandArgument='<%# Eval("UserId") %>'
                                 OnClick="lnbHotelName_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>  
                         <asp:TemplateField ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:CheckBox ID="uiCheckBoxIsActive" runat="server" AutoPostBack="true" OnCheckedChanged="uiCheckBoxIsActive_CheckedChanged" />
                            </ItemTemplate>
                        </asp:TemplateField>                       
                    </Columns>       
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>             
                </asp:GridView>
</tbody></table>
<asp:LinkButton ID="lnkcheck" runat="server"></asp:LinkButton>
<asp:ModalPopupExtender ID="ModalPopupCheck" TargetControlID="lnkcheck" BackgroundCssClass="modalBackground"
                PopupControlID="pnlcheck" runat="server">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlcheck" BorderColor="Black" Width="700px" BorderWidth="1" Style="display: none;"
                runat="server" BackColor="White">
                <div class="popup-mid-inner-body" align="center">
                </div>
                <div class="popup-mid-inner-body1">
                    <table width="100%" cellpadding="5" cellspacing="1">
                        <tr bgcolor="#CCD8D8">
                          <td valign="top" width="50%">Client Name</td>
                            <td valign="top" width="50%">Hotel Name</td>                            
                          </tr>
                        <asp:GridView ID="grvHotelowner" runat="server" Width="100%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="false" DataKeyNames="Id" GridLines="None"
                    ShowHeader="false" ShowFooter="false" BackColor="#98BCD6" RowStyle-BackColor="#FFFFFF" AlternatingRowStyle-BackColor="#E3F0F1"                    
                    EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                    HeaderStyle-BackColor="#98BCD6">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="50%">
                            <ItemTemplate>                                
                                <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("UserIdSource.FirstName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="50%">
                            <ItemTemplate>
                                <asp:Label ID="lblHotelID" runat="server" Text='<%# Eval("HotelIDSource.Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                                               
                    </Columns>
                      <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>                       
                </asp:GridView>                       
                    </table>
                     <ul>
                            <li class="value10">
                                <div class="col21" style="width: 700px;">
                                    <div class="button_section">                                       
                                        <asp:LinkButton ID="lnkCheckcancel" runat="server" Text="Close" OnClick="lnkCheckcancel_Click" />
                                    </div>
                                </div>
                            </li>
                        </ul>
                </div>    
                      
            </asp:Panel>
</div>

<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#<%= btnSubmit.ClientID %>").bind("click", function () {
            var isvalid = true;
            var errormessage = "";
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }

            var txtContactPerson = jQuery("#<%= txtContactPerson.ClientID %>").val();
            if (txtContactPerson.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Contact person is required.";
                isvalid = false;
            }
            var txtEmailAccount = jQuery("#<%= txtEmailAccount.ClientID %>").val();
            if (txtEmailAccount.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Emailid is required.";
                isvalid = false;
            }
            else {
                if (!validateEmail(txtEmailAccount)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Enter valid email id.";
                    isvalid = false;
                }
            }

            var hotelPhone = jQuery("#<%= txtPhoneNumber.ClientID %>").val();
            if (hotelPhone.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Phone number is required.";
                isvalid = false;
            }


            var chkHotelName = jQuery("#<%= chkHotelName.ClientID %>").find("input:checkbox:checked").length;
            if (chkHotelName == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Please select atleast one hotel. ";
                isvalid = false;
            }

            if (!isvalid) {
                jQuery("#<%= divmessage.ClientID %>").show();
                jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                jQuery("#<%= divMessageOnGridHeader.ClientID %>").html("");
                jQuery("#<%= divMessageOnGridHeader.ClientID %>").hide();
                var offseterror = jQuery("#<%= divmessage.ClientID %>").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }
            jQuery("#<%= divmessage.ClientID %>").html("");
            jQuery("#<%= divmessage.ClientID %>").hide();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        });
    });

    function validateEmail(txtEmail) {
        var a = txtEmail;
        var filter = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{1,4}$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }

    function validate_file_format(file_name, allowed_ext) {
        field_value = file_name;
        if (field_value != "") {
            var file_ext = (field_value.substring((field_value.lastIndexOf('.') + 1)).toLowerCase());
            ext = allowed_ext.split(',');
            var allow = 0;
            for (var i = 0; i < ext.length; i++) {
                if (ext[i] == file_ext) {
                    allow = 1;
                }
            }
            if (!allow) {
                return false;
            }
            else {
                return true;
            }
        }
        return false;
    } 
    </script>
</asp:Content>

