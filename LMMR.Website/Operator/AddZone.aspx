﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true"
    CodeFile="AddZone.aspx.cs" Inherits="Operator_AddCity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_container1">
        <!-- start left section-->
        <div class="left_container2">
            <div class="search">
                <%--<div class="hotel_logo"><img src="../images/hotel_logo.png" alt="Hotel Logo"></div>	--%>
                <!-- start drop down search -->
                <div class="rowElem">
                    <asp:DropDownList ID="drpCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCountry_SelectedIndexChanged"
                        CssClass="NoClassApply" Width="160">
                    </asp:DropDownList>
                </div>
                <!-- end drop down search -->
            </div>
        </div>
        <!-- end left section-->
        <!-- start right section-->
        <div class="right_container1">
            <div class="superadmin-mainbody">
                <!-- end logout-today-->
                <div class="superadmin-mainbody-inner">
                    <div class="commisions-top-main clearfix">
                        <div class="commisions-top ">
                            <div id="divmessage" runat="server">
                            </div>
                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                <tr>
                                    <td width="42%">
                                        Select City
                                    </td>
                                    <td width="58%">
                                        <asp:DropDownList ID="drpCity" runat="server"
                                            CssClass="NoClassApply" Width="180">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Enter Zone Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtZoneName" runat="server" MaxLength="20"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <br />
                            <br />
                            <asp:LinkButton ID="lbtSave" runat="server" CssClass="select" OnClick="lbtSave_Click"
                                OnClientClick="return validate();">Save</asp:LinkButton>
                            <asp:LinkButton ID="lbtReset" runat="server" CssClass="select" OnClick="lbtReset_Click">Reset</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end right section-->
    </div>
<script language="javascript" type="text/javascript">
        function validate() {
           
            var CountryDropdownList = document.getElementById('<%=drpCountry.ClientID %>');
            var SelectedText = CountryDropdownList.options[CountryDropdownList.selectedIndex].text;
            if (SelectedText == "Select Country") {
                alert("Please select country.");
                return false;
            }
            var CityDropdownList = document.getElementById('<%=drpCity.ClientID %>');
            var SelectedText = CityDropdownList.options[CityDropdownList.selectedIndex].text;
            if (SelectedText == "Select City" ) {
                alert("Please select city.");
                return false;
            }
            var zoneName = jQuery("#<%= txtZoneName.ClientID %>").val();
            if (zoneName.length <= 0) {
                alert("Please enter zone.");
                return false;
            }
        }

</script>
</asp:Content>

