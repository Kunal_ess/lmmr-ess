<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true"
    MaintainScrollPositionOnPostback="true" EnableEventValidation="false" CodeFile="newHotelInvoice.aspx.cs"
    Inherits="Operator_newHotelInvoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title>checkinvoice</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .jqTransformSelectWrapper
        {
            width: 139px !important;
        }
        .jqTransformSelectWrapper ul
        {
            width: 139px !important;
        }
        .LinkPaging
        {
            width: 20px;
            background-color: White;
            border: Solid 1px Black;
            text-align: center;
            margin-left: 8px;
        }
    </style>
    <div class="search-operator-layout1">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="630px">
                    <div class="search-operator-layout-left1">
                        <div class="search-operator-from1">
                            <div class="commisions-top-new1 ">
                                <table width="100%" border="0" cellspacing="0" cellpadding="8">
                                    <tr>
                                        <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                            <div class="search-form1-left-new">
                                                Select country : &nbsp;&nbsp;&nbsp;
                                            </div>
                                            <asp:DropDownList ID="countryDDL" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                            <div class="search-form1-left-new">
                                                Invoice Number : &nbsp;&nbsp;&nbsp;
                                            </div>
                                            <asp:TextBox ID="txtInvoiceNumber" runat="server" Text="" class="inputbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                            <div class="search-form1-left">
                                                Month &nbsp;&nbsp; From
                                            </div>
                                            <div class="search-form1-right1">
                                                <asp:TextBox ID="txtFromdate" runat="server" CssClass="inputbox1"></asp:TextBox><asp:CalendarExtender
                                                    ID="CalendarExtender3" runat="server" TargetControlID="txtFromdate" PopupButtonID="calFrom"
                                                    Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                            </div>
                                            <div class="search-form1-right2">
                                                <input type="image" src="../images/date-icon.png" id="calFrom" />
                                            </div>
                                            <div class="search-form1-right3">
                                                To
                                            </div>
                                            <div class="search-form1-right1">
                                                <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox1"></asp:TextBox><asp:CalendarExtender
                                                    ID="CalendarExtender1" runat="server" TargetControlID="txtTodate" PopupButtonID="calTo"
                                                    Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                            </div>
                                            <div class="search-form1-right2">
                                                <input type="image" src="../images/date-icon.png" id="calTo" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </td>
                <td align="left" valign="bottom">
                    <div style="float: left; margin-left: 20px;" class="n-commisions">
                        <div class="n-btn">
                            <asp:LinkButton ID="lbtSearch" runat="server" OnClick="lbtSearch_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Search</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 10px;" class="n-commisions">
                        <div class="n-btn">
                            <asp:LinkButton ID="clearBtn" runat="server" OnClick="clearBtn_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Clear</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="contract-list" id="divAlphabeticPaging" runat="server" style="margin-top: 20px;">
        <div class="contract-list-left" style="width: 278px;">
            <h2>
                Invoice</h2>
        </div>
    </div>
    <div class="search-booking">
        <!--pageing-operator start -->
        <div class="pageing-operator">
            <asp:LinkButton ID="exportToExcel" runat="server" OnClick="exportToExcel_Click"><img id="imeexcel" src="../Images/excel.jpg" /></asp:LinkButton>
            <asp:LinkButton ID="lbtSendInvoices" runat="server" class="sendinvoice-btn" OnClick="lbtSendInvoices_Click"
                OnClientClick="javascript:return TestCheckBox();">Send Invoice</asp:LinkButton>
        </div>
        <!-- end pageing-operator-->
        <div class="search-booking-rowmain clearfix" style="width: 950px">
            <div style="width: 940px; overflow: hidden;">
                <div id="divmessage" runat="server">
                </div>
            </div>
            <table class="teblewidth" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#98bcd6">
                        <asp:GridView runat="server" ID="grvFinanceInvoice" AutoGenerateColumns="false" DataKeyNames="Id"
                            ShowHeaderWhenEmpty="True" AllowSorting="True" GridLines="None" Width="100%"
                            CellPadding="0" CellSpacing="1" EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center"
                            OnRowDataBound="grvFinanceInvoice_RowDataBound" AllowPaging="True" PageSize="10"
                            OnPageIndexChanging="grvFinanceInvoice_PageIndexChanging" ShowHeader="true" OnSorting="grvFinanceInvoice_Sorting"
                            EmptyDataText="No record Found!" OnRowCommand="grvFinanceInvoice_RowCommand">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <tr bgcolor="#c4d6e2">
                                            <td align="center">
                                                View
                                            </td>
                                            <td align="center">
                                                Invoice Number
                                            </td>
                                            <td align="center">
                                                Channel
                                            </td>
                                            <td align="center">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Sort" CommandArgument="DueDate"
                                                    Text="Invoice Date"></asp:LinkButton>
                                            </td>
                                            <td align="center">
                                                <asp:DropDownList ID="drpPeriod" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpPeriod_SelectedIndexChanged"
                                                    CssClass="NoClassApply" Visible="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left">
                                                Country
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpHotelName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpHotelName_SelectedIndexChanged"
                                                    CssClass="NoClassApply">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center">
                                                Amount
                                            </td>
                                            <td align="center">
                                                Status
                                            </td>
                                            <td align="center">
                                                Credit Note
                                            </td>
                                            <td align="center">
                                                Verified
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#d8eefc">
                                            <td bgcolor="#d8eefc" align="center">
                                                <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank">
                                                    <asp:Image ID="imgPlan" runat="server" src="../images/pdf-icon.png" AlternateText='<%# Eval("InvoiceFile") %>' />
                                                </asp:HyperLink>
                                                <asp:HiddenField ID="hdfImage" runat="server" Value='<%# Eval("InvoiceFile") %>' />
                                            </td>
                                            <td align="center">
                                                <asp:LinkButton ID="lblRefNo" runat="server" Text='<%# Eval("Id") %>' ToolTip='<%# Eval("BookedId") %>'
                                                    ForeColor="#339966" OnClick="lblRefNo_Click" Visible="false"></asp:LinkButton>
                                                <asp:LinkButton ID="lnbInvoiceNo" runat="server" Text='<%# Eval("InvoiceNumber") %>'
                                                    ToolTip='<%# Eval("Id") %>' CommandName='<%# Eval("HotelId") %>' OnClick="lnbInvoiceNo_Click"></asp:LinkButton>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblChannel" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblInvoiceDate" runat="server" Text='<%# Bind("DueDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblDatefrom" runat="server" Text='<%# Bind("DateFrom", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                -
                                                <asp:Label ID="lblDateto" runat="server" Text='<%# Bind("DateTo", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblCountryName" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblHotelId" runat="server" Text='<%# Eval("HotelId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblHotel" runat="server" Text='<%# Eval("HotelIdSource.Name") %>'></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# String.Format("{0:#,##,##0.00}",Eval("TotalAmount")) %>'></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:LinkButton ID="lblPaid" CommandName="StatusChange" CommandArgument='<%#Eval("Id") %>'
                                                    runat="server" OnClientClick="return ConfirmStatus();" Text='<%# Eval("IsPaid") %>'
                                                    Visible="false" />
                                                <asp:Image ID="imgStatus" runat="server" ImageUrl="../images/done.png" Visible="false" />
                                            </td>
                                            <td bgcolor="#d8eefc" align="center">
                                                <asp:HyperLink ID="linkCreditnote" runat="server" Target="_blank" ToolTip='<%# Eval("CreditNote") %>'
                                                    Visible="false">
                                                    <asp:Image ID="imgCheck" runat="server" src="../images/pdf-icon.png" />
                                                </asp:HyperLink>
                                                <asp:HiddenField ID="hdfCreditnote" runat="server" Value='<%# Eval("CreditNote") %>' />
                                                <asp:CheckBox ID="chkCreditnote" runat="server" />
                                                <asp:LinkButton ID="lnbCreditnote" runat="server" CommandName='<%# Eval("Id") %>'
                                                    OnClick="lnbCreditnote_Click" Text="Attach Credit Note"></asp:LinkButton>
                                            </td>
                                            <td bgcolor="#d8eefc" align="center">
                                                <asp:CheckBox ID="chkInvoiceSent" runat="server" Visible="false" />
                                                <asp:Image ID="verifiedIMG" ImageUrl="~/Images/v-image.png" AlternateText='<%# Eval("InvoiceSent") %>'
                                                    runat="server" Visible="false" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr bgcolor="#d8eefc">
                                            <td bgcolor="#d8eefc" align="center">
                                                <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank">
                                                    <asp:Image ID="imgPlan" runat="server" src="../images/pdf-icon.png" />
                                                </asp:HyperLink>
                                                <asp:HiddenField ID="hdfImage" runat="server" Value='<%# Eval("InvoiceFile") %>' />
                                            </td>
                                            <td align="center">
                                                <asp:LinkButton ID="lblRefNo" runat="server" Text='<%# Eval("Id") %>' ToolTip='<%# Eval("BookedId") %>'
                                                    ForeColor="#339966" OnClick="lblRefNo_Click" Visible="false"></asp:LinkButton>
                                                <asp:LinkButton ID="lnbInvoiceNo" runat="server" Text='<%# Eval("InvoiceNumber") %>'
                                                    ToolTip='<%# Eval("Id") %>' CommandName='<%# Eval("HotelId") %>' OnClick="lnbInvoiceNo_Click"></asp:LinkButton>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblChannel" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblInvoiceDate" runat="server" Text='<%# Bind("DueDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblDatefrom" runat="server" Text='<%# Bind("DateFrom", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                -
                                                <asp:Label ID="lblDateto" runat="server" Text='<%# Bind("DateTo", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblCountryName" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblHotelId" runat="server" Text='<%# Eval("HotelId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblHotel" runat="server" Text='<%# Eval("HotelIdSource.Name") %>'></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# String.Format("{0:#,##,##0.00}",Eval("TotalAmount")) %>'></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:LinkButton ID="lblPaid" CommandName="StatusChange" CommandArgument='<%#Eval("Id") %>'
                                                    runat="server" OnClientClick="return ConfirmStatus();" Text='<%# Eval("IsPaid") %>'
                                                    Visible="false" />
                                                <asp:Image ID="imgStatus" runat="server" ImageUrl="../images/done.png" Visible="false" />
                                            </td>
                                            <td bgcolor="#d8eefc" align="center">
                                                <asp:HyperLink ID="linkCreditnote" runat="server" Target="_blank" ToolTip='<%# Eval("CreditNote") %>'
                                                    Visible="false">
                                                    <asp:Image ID="imgCheck" runat="server" src="../images/pdf-icon.png" />
                                                </asp:HyperLink>
                                                <asp:HiddenField ID="hdfCreditnote" runat="server" Value='<%# Eval("CreditNote") %>' />
                                                <asp:CheckBox ID="chkCreditnote" runat="server" />
                                                <asp:LinkButton ID="lnbCreditnote" runat="server" CommandName='<%# Eval("Id") %>'
                                                    OnClick="lnbCreditnote_Click" Text="Attach Credit Note"></asp:LinkButton>
                                            </td>
                                            <td bgcolor="#d8eefc" align="center">
                                                <asp:CheckBox ID="chkInvoiceSent" runat="server" Visible="false" />
                                                <asp:Image ID="verifiedIMG" ImageUrl="~/Images/v-image.png" AlternateText='<%# Eval("InvoiceSent") %>'
                                                    runat="server" Visible="false" />
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <tr>
                                    <td colspan="11" align="center">
                                        "No record Found!"
                                    </td>
                                </tr>
                            </EmptyDataTemplate>
                            <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True"></EmptyDataRowStyle>
                            <PagerSettings Mode="NumericFirstLast" Position="Top" />
                            <PagerStyle HorizontalAlign="Right" BackColor="White" />
                            <PagerTemplate>
                                <tr>
                                    <td colspan="11" bgcolor="#ffffff">
                                        <div style="float: right; width: 200px;">
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </div>
                                    </td>
                                </tr>
                            </PagerTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:LinkButton ID="lnkbtn" OnClientClick="javascript:document.getElementById('contentbody_pnlchkCommission').style.display='block';"
                runat="server"></asp:LinkButton>
            <asp:ModalPopupExtender ID="modalcheckcomm" TargetControlID="lnkbtn" BackgroundCssClass="modalBackground"
                PopupControlID="pnlchkCommission" runat="server">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlchkCommission" BorderColor="#999999" Width="757px" Height="250px"
                BorderWidth="5" Style="display: none; padding-top: 7px;" runat="server" BackColor="White">
                <div id="divPopup" runat="server">
                </div>
                <div class="popup-mid-inner-body1">
                    <table width="100%" cellspacing="0" cellpadding="3" align="center">
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td align="center" colspan="2">
                                            <b>Upload Credit Note file(Pdf format)</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            Upload Document :
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="fileUpload" runat="server" /><br />
                                            <span style="font-family: Arial; font-size: Smaller; color: Gray">Supporting document
                                                file format: PDF & max size 1MB. </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <div class="popup-mid-inner-body2_i">
                    </div>
                    <div class="booking-details" style="width: 760px;">
                        <ul>
                            <li class="value10">
                                <div class="col21" style="width: 752px;">
                                    <div class="button_section">
                                        <asp:LinkButton ID="UploadBtn" runat="server" Text="Save" CssClass="select" OnClick="UploadBtn_Click" />
                                        <span>or</span>
                                        <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" />
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </asp:Panel>
            <div style="margin-top: 20px;" runat="server" id="Divdetails" visible="false">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <%--<tr bgcolor="#FFFFFF">
                                <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 12px; line-height: 25px"
                                    align="left" valign="top">
                                    <asp:Label ID="Label3" runat="server" Text="From : " Font-Bold="true"></asp:Label>
                                    <asp:Label ID="lblFromHos" runat="server" Text="E-Hospitality"></asp:Label><br />
                                    <asp:Label ID="Label5" runat="server" Text="VAT No : " Font-Bold="true"></asp:Label><asp:Label
                                        ID="lblAdminVAT" runat="server" Text="BE 9029922"></asp:Label><br />
                                    <asp:Label ID="Label11" runat="server" Text="Address : " Font-Bold="true"></asp:Label><asp:Label
                                        ID="lblClientAddress" runat="server" Text=""></asp:Label><br />
                                    <asp:Label ID="Label13" runat="server" Text="Phone : " Font-Bold="true"></asp:Label><asp:Label
                                        ID="lblClientPhone" runat="server" Text=""></asp:Label><br />
                                </td>
                                <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 12px; line-height: 25px"
                                    align="left" valign="top">
                                    <asp:Label ID="Label7" runat="server" Text="To :" Font-Bold="true"></asp:Label>
                                    <asp:Label ID="lblCTo" runat="server" Text=""></asp:Label><br />                                   
                                </td>
                            </tr>--%>
                    <tr bgcolor="#FFFFFF">
                        <td colspan="3">
                            <table width="100%">
                                <tr bgcolor="#FFFFFF">
                                    <td width="60%">
                                        &nbsp;
                                    </td>
                                    <td width="40%" style="padding: 10px 10px; font-size: 12px; line-height: 25px" align="left"
                                        valign="top">
                                        <asp:Label ID="Label3" runat="server" Text="Hotel Name : " Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblVenueName" runat="server" Text=""></asp:Label><br />
                                        <asp:Label ID="Label11" runat="server" Text="Hotel Address : " Font-Bold="true"></asp:Label><asp:Label
                                            ID="lblClientAddress" runat="server" Text=""></asp:Label><br />
                                        <asp:Label ID="Label13" runat="server" Text="Phone : " Font-Bold="true"></asp:Label><asp:Label
                                            ID="lblClientPhone" runat="server" Text=""></asp:Label><br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td style="padding: 10px 10px; font-size: 25px; font-weight: bold; font-family: Times New Roman;
                            font-style: italic;" align="left" colspan="9">
                            <asp:Label ID="Label" runat="server" Text="Statement number : " Font-Bold="true"></asp:Label>&nbsp;&nbsp;<asp:Label
                                ID="lblInvoiceNumber" runat="server" Text=""></asp:Label>
                            <%--<asp:Label ID="lblReferenceNumber" runat="server" Text="125/0455/62409"></asp:Label>--%>
                        </td>
                    </tr>
                    <tr bgcolor="#CCD8D8">
                        <td colspan="9" style="border-top: #A3D4F7 solid 1px;">
                            <table width="100%" cellpadding="10">
                                <tr>
                                    <td style="font-size: 14px;" align="left">
                                        <asp:Label ID="Label4" runat="server" Text="Client Number: " Font-Bold="true"></asp:Label>&nbsp;&nbsp;<asp:Label
                                            ID="lblClientCode" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td style="font-size: 14px;" align="left">
                                        <asp:Label ID="Label6" runat="server" Text="VAT Number: " Font-Bold="true"></asp:Label>&nbsp;&nbsp;<asp:Label
                                            ID="lblClientVat" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td style="font-size: 14px;" align="left">
                                        <asp:Label ID="Label8" runat="server" Text=" Date: " Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                                        <asp:Label ID="lblInvoiceDate" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td colspan="9" style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-weight: bold;
                            font-size: 14px;" align="center">
                            Period :
                            <asp:Label ID="lblFrom" runat="server" Text=""></asp:Label>
                            -
                            <asp:Label ID="lblTo" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td colspan="9" style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-weight: bold">
                            Booking
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <asp:GridView ID="grvBooking" runat="server" Width="100%" AutoGenerateColumns="False"
                                RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" CellPadding="6"
                                OnRowDataBound="grvBooking_RowDataBound" EmptyDataText="No record Found!" EmptyDataRowStyle-Font-Bold="true"
                                EmptyDataRowStyle-HorizontalAlign="Center" EditRowStyle-VerticalAlign="Top" GridLines="None"
                                PageSize="10" BackColor="#ffffff">
                                <Columns>
                                    <asp:TemplateField HeaderText="#">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Channel">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Usertype ") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Person">
                                        <ItemTemplate>
                                            <asp:Label ID="lblContact" runat="server" Text='<%# Eval("Contact ") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Booking Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBookingDate" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Departure Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartureDt" runat="server" Text='<%# Eval("DepartureDate","{0:dd/MM/yy}")%>'></asp:Label>
                                            <asp:HiddenField ID="hdnArrivalDate" runat="server" Value='<%# Eval("ArrivalDate","{0:dd/MM/yy}")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Net Value">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRevenueAmount" runat="server" Text='<%# String.Format("{0:###,###,###}",Eval("ConfirmRevenueAmount")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commission %">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCommision" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value(Euro)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFinalValue" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#CCD8D8" />
                                <RowStyle BackColor="#E3F0F1" />
                                <SelectedRowStyle BackColor="Yellow" />
                                <AlternatingRowStyle BackColor="#ffffff" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td colspan="9" style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-weight: bold">
                            Requests
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <asp:GridView ID="grvRequest" runat="server" Width="100%" AutoGenerateColumns="False"
                                RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" CellPadding="6"
                                OnRowDataBound="grvRequest_RowDataBound" EmptyDataText="No record Found!" EmptyDataRowStyle-Font-Bold="true"
                                EmptyDataRowStyle-HorizontalAlign="Center" EditRowStyle-VerticalAlign="Top" GridLines="None"
                                PageSize="10" BackColor="#ffffff">
                                <Columns>
                                    <asp:TemplateField HeaderText="#">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Channel">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Usertype ") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Person">
                                        <ItemTemplate>
                                            <asp:Label ID="lblContact" runat="server" Text='<%# Eval("Contact ") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Booking Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBookingDate" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Departure Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartureDt" runat="server" Text='<%# Eval("DepartureDate","{0:dd/MM/yy}")%>'></asp:Label>
                                            <asp:HiddenField ID="hdnArrivalDate" runat="server" Value='<%# Eval("ArrivalDate","{0:dd/MM/yy}")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Net Value">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRevenueAmount" runat="server" Text='<%# String.Format("{0:###,###,###}",Eval("ConfirmRevenueAmount")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commission %">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCommision" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value(Euro)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFinalValue" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#CCD8D8" />
                                <RowStyle BackColor="#E3F0F1" />
                                <SelectedRowStyle BackColor="Yellow" />
                                <AlternatingRowStyle BackColor="#ffffff" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <table class="teblewidth" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"
                    style="margin-top: 20px" id="divTotal" runat="server" visible="false">
                    <tr bgcolor="#FFFFFF">
                        <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 14px;" align="left"
                            width="25%">
                            <asp:Label ID="Label1" runat="server" Text="Total VAT Excl " Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                        </td>
                        <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 14px;" align="left"
                            width="25%">
                            <asp:Label ID="Label5" runat="server" Text="VAT %" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                        </td>
                        <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 14px;" align="left"
                            width="25%">
                            <asp:Label ID="Label9" runat="server" Text="Total VAT" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                        </td>
                        <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 14px;" align="right"
                            width="25%">
                            <asp:Label ID="Label2" runat="server" Text="Total Eur " Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 13px;" align="left">
                            <asp:Label ID="lblTotalValue" runat="server" Text="0.00"></asp:Label>&nbsp;&nbsp;
                        </td>
                        <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 13px;" align="left">
                            <asp:Label ID="Label10" runat="server" Text="21.0" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                        </td>
                        <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 13px;" align="left">
                            <asp:Label ID="lblTotalVat" runat="server" Text="0.00" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                        </td>
                        <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 13px;" align="right">
                            <asp:Label ID="lblFinalTotal" runat="server" Text="0.00" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td colspan="4" style="border-bottom: #A3D4F7 solid 1px; padding-top: 30px;">
                            <table width="100%">
                                <tr bgcolor="#FFFFFF">
                                    <td style="padding: 10px 10px; font-size: 16px;" align="left" width="60%">
                                   <font style="font-size: 14px; font-family:Arial Narrow">    This is not an invoice. Invoice will be send by post.<br /> The above is a financial statement. <br />
                                        We thank you for your support.<br />
                                        Hotelsupport Team.
                                        </font>
 &nbsp;&nbsp;
                                    </td>
                                    <td style="padding: 10px 10px; font-size: 16px;" align="right" width="40%">
                                        <%--***<asp:Label ID="lblUniqueNumber" runat="server" Text="003/6611/66077" Font-Bold="true"></asp:Label>***--%>
                                        <asp:Label ID="lblUniqueNumber" runat="server" Text="003/6611/66077" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTotalRequest" runat="server" Text="0.00" Visible="false"></asp:Label>
                            <asp:Label ID="lblTotalBooking" runat="server" Text="0.00" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div align="left" style="display: none">
            <br />
            <h1 class="new">
                Booking details</h1>
            <div class="booking-details">
                <div class="booking-step3-detail-body" style="width: 100%;">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="one">
                        <tr>
                            <td>
                                <b>Contact name:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <b>Phone No:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblContactPhone" runat="server" Text=""></asp:Label>
                            </td>
                            <td colspan="2">
                                <b>Email :</b> <a id="lblConatctpersonEmail" runat="server" href="#"></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Date from:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblFromDt" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <b>To:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                            </td>
                            <td colspan="2">
                                <b>Duration:</b>
                                <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label>
                                <asp:Label ID="lblBookedDays1" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Contact Address</b>
                            </td>
                            <td colspan="5">
                                <asp:Label ID="lblContactAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:Repeater ID="rpmain" runat="server" OnItemDataBound="rpmain_ItemDataBound">
                    <ItemTemplate>
                        <!--booking-step3-meeting-room-mainbody START HERE-->
                        <div class="booking-step3-meeting-room" style="width: 100%;">
                            <h1>
                                Meeting room
                            </h1>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="one">
                                <tr bgcolor="#d4d9cc">
                                    <th>
                                    </th>
                                    <th align="left">
                                        Description
                                    </th>
                                    <th align="center">
                                        Price
                                    </th>
                                    <th align="center">
                                        Start
                                    </th>
                                    <th align="center">
                                        End
                                    </th>
                                    <th align="center">
                                        Qty
                                    </th>
                                    <th align="center">
                                        Total
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblmeetingroomname" runat="server" Text='<%# Eval("MeetingroomName") %>'></asp:Label>
                                        <asp:Label ID="lblbmrid" runat="server" Text='<%# Eval("MeetingroomDesc") %>' Visible="true"></asp:Label>
                                    </td>
                                    <td>
                                        <p>
                                            <asp:Label ID="lblMeetingroomtype" runat="server" Text='<%# Eval("MeetingRoomType") %>'></asp:Label></p>
                                        <p>
                                            <asp:Label ID="lblmeetingroomconfig" runat="server" Text='<%# Eval("MeetingRoomConfig") %>'></asp:Label></p>
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="lblmeetingroomprice" runat="server" Text='<%# String.Format("{0:#,##,##0.00}",Eval("MeetingRoomPrice")) %>'></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="lblmrstarttime" runat="server" Text='<%# Eval("MeetingRoomST") %>'></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="lblmrendtime" runat="server" Text='<%# Eval("MeetingRoomET") %>'></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="lblqty" runat="server" Text='<%# Eval("MeetingRoomQTY") %>'></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="lbltotal" runat="server" Text='<%# String.Format("{0:#,##,##0.00}",Eval("MeetingRoomTotal")) %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--booking-step3-meeting-room ENDS HERE-->
                        <!--booking-step3-packages-mainbody START HERE-->
                        <div class="booking-step3-packages" style="width: 100%;">
                            <h1>
                                Packages</h1>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="one">
                                <tr bgcolor="#d4d9cc">
                                    <th>
                                    </th>
                                    <th align="left">
                                        Description
                                    </th>
                                    <th align="center">
                                        Total
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPackageName" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:GridView ID="grdDetailspackage" runat="server" ShowHeader="false" BorderWidth="0"
                                            GridLines="None" AutoGenerateColumns="false" Width="100%">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <table align="left" width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <b>Item Name:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblItemname" runat="server" Text='<%# Eval("SectionDescIdSource.ItemName") %>'></asp:Label>
                                                                </td>
                                                                <td align="left">
                                                                    <b>Serve Time:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblfromtime" runat="server" Text='<%# Eval("FromTime") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="lbltotalpackage" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Extra's
                                    </td>
                                    <td align="center">
                                        <asp:GridView ID="grdextra" runat="server" ShowHeader="false" BorderWidth="0" GridLines="None"
                                            AutoGenerateColumns="false" Width="100%" EmptyDataText="No Records">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <table width="100%" align="left">
                                                            <tr>
                                                                <td align="left">
                                                                    <b>Item Name:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblItemname" runat="server" Text='<%# Eval("PackageIdSource.ItemName") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <b>Serve Time:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblfromtime" runat="server" Text='<%# Eval("ServeTime") %>'></asp:Label></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="lblextratotal" runat="server" Text='<%# String.Format("{0:#,##,##0.00}",Eval("MeetingRoomTotal")) %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="booking-step3-equipment" style="width: 100%;">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <h1>
                                            Equipment</h1>
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView ID="grdequipment" runat="server" AutoGenerateColumns="false" BorderWidth="0"
                                BorderStyle="None" CssClass="one" Width="100%" EmptyDataText="No Records">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="booking-step3-equipment-heading1">
                                                &nbsp;
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemname" runat="server" Text='<%# Eval("PackageIdSource.ItemName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Qty
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblequipqty" runat="server" Text='<%# Eval("Quntity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Total
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblequipTotal" runat="server" Text='<%# String.Format("{0:#,##,##0.00}",Eval("TotalPrice")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <!--booking-step3-accomodation-body START HERE-->
                <div class="booking-step3-accomodation-body" style="width: 100%;">
                    <table width="100%">
                        <tr>
                            <td>
                                <h1>
                                    Bedrooms</h1>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="one">
                        <tr bgcolor="#d4d9cc">
                            <th>
                                Description
                            </th>
                            <th>
                                Type
                            </th>
                            <th align="center">
                                Price
                            </th>
                            <th align="center">
                                Qty
                            </th>
                            <th align="center">
                                Total
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblBedroomType" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbloccupancy" runat="server" Text="Single"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblbedroomtyoeprice" runat="server" Text="2,000"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblQty" runat="server" Text="1"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblbedroomtotalprice" runat="server" Text='<%# String.Format("{0:#,##,##0.00}",Eval("Total")) %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="one">
                        <tr bgcolor="#d4d9cc">
                            <th>
                            </th>
                            <th>
                            </th>
                            <th align="center">
                                Check in
                            </th>
                            <th align="center">
                                Checkout
                            </th>
                            <th>
                                Notes
                            </th>
                        </tr>
                        <tr>
                            <td>
                                Room 1
                            </td>
                            <td>
                                <asp:Label ID="lblpersonName" runat="server" Text='<%# Eval("PersonName") %>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblCheckIn" runat="server" Text='<%# Eval("CheckIn") %>'></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblChekOut" runat="server" Text='<%# Eval("CheckOut") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblNote" runat="server" Text='<%# Eval("Note") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--booking-step3-accomodation ENDS HERE-->
                <div class="booking-step3-meeting-room-total">
                    Final Price:&nbsp; &nbsp; <b>
                        <asp:Label ID="lblfinalprice" runat="server"></asp:Label></b>
                </div>
                <!--special-request-step3-content START HERE-->
                <div class="special-request-step3" style="width: 100%;">
                    <table width="100%">
                        <tr>
                            <td>
                                <h1 style="width: 100%;">
                                    Special request.</h1>
                            </td>
                        </tr>
                    </table>
                    <div class="special-request-step3-inner">
                        <asp:Label ID="lblsplcomments" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
                <!--special-request-step3-detail-body ENDS HERE-->
            </div>
        </div>
    </div>

    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);
            jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
                jQuery("#ContentPlaceHolder1_txtFromdate").attr("disabled", false);
                jQuery("#ContentPlaceHolder1_txtTodate").attr("disabled", false);
            });
        });
     
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
                //                jQuery("body").scrollTop(0);
                //                jQuery("html").scrollTop(0);
                //                jQuery("#Loding_overlaySec span").html("Loading...");
                //                jQuery("#Loding_overlaySec").show();
                //                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
            jQuery("#<%= UploadBtn.ClientID %>").bind("click", function () {
                //                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                //                jQuery("#Loding_overlaySec").show();
            });
        });

        function checkuncheck(obj, val) {
            if (obj.checked) {
                document.getElementById(val).style.display = 'block';
            }
            else {
                document.getElementById(val).style.display = 'none';
            }
        }

        //        var TargetBaseControl = null;

        //        window.onload = function () {
        //            try {
        //                //get target base control.
        //                TargetBaseControl =
        //           document.getElementById('<%= this.grvFinanceInvoice.ClientID %>');
        //            }
        //            catch (err) {
        //                TargetBaseControl = null;
        //            }
        //        }

        function TestCheckBox() {
            if (jQuery("#ContentPlaceHolder1_grvFinanceInvoice tr").find("td:last").find("input:checkbox:checked").length > 0) {
                return true;
            }
            else {
                alert("Please select at least one checkbox!");
                return false;
            }
        }
        function ConfirmStatus() {
            return confirm("Are you sure you want to change the status?");
        }
        function SetFocusBottom(val) {

            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
        }
    </script>
</asp:Content>
