﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true"
    CodeFile="ClientContract.aspx.cs" Inherits="Operator_ClientContract" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .floatleft
        {
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Literal ID="scriptRunTime" runat="server"></asp:Literal>
    <div id="searchmessage" style="display: none;" class="error">
    </div>
    <div class="search-operator-layout">
        <div class="search-operator-layout-left">
            <asp:UpdateProgress ID="updPro" runat="server" AssociatedUpdatePanelID="upSearch">
                <ProgressTemplate>
                    <div style="position: absolute; top: 0px; left: 0; width: 100%; height: 1000%; background-color: #FFF;
                        z-index: 1001; -moz-opacity: 0.7; opacity: .70; filter: alpha(opacity=70); text-align: center;">
                        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                        <span>Loading...</span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upSearch" runat="server">
                <ContentTemplate>
                    <div class="search-operator-from">
                        <div class="search-operator-from-left">
                            <div class="search-form-left">
                                <div class="rowElem" id="country">
                                    <asp:DropDownList ID="uiDropDownListCountryforLoad" runat="server" AutoPostBack="true"
                                        OnSelectedIndexChanged="uiDropDownListCountryforLoad_SelectedIndexChanged" CssClass="NoClassApply">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="search-form-left">
                                <div class="rowElem" id="city">
                                    <asp:DropDownList ID="uiDropDownListCityForLoad" runat="server" AutoPostBack="false"
                                        CssClass="NoClassApply" Enabled="false">
                                        <asp:ListItem Text="Select City" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="search-form-left" id="chkclients">
                                <asp:CheckBox ID="chkClient" runat="server" AutoPostBack="True" OnCheckedChanged="chkClient_CheckedChanged" /><label>Clients</label>
                            </div>
                            <div class="search-form-left" id="chkcontracts">
                                <asp:CheckBox ID="chkContracts" runat="server" AutoPostBack="True" OnCheckedChanged="chkContracts_CheckedChanged" /><label>Contracts</label>
                            </div>
                        </div>
                        <div class="search-operator-from-right">
                            <div class="search-form1" style="height: 30px; padding: 10px 0px 0px 10px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #FFF">
                                    <tr>
                                        <td>
                                            <b>Online Status:</b>
                                        </td>
                                        <td colspan="3" id="radioKM">
                                            <asp:RadioButtonList ID="rdbStatus" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem Value="0">Not Requested Yet</asp:ListItem>
                                                <asp:ListItem Value="1">Requested</asp:ListItem>
                                                <asp:ListItem Value="2">Online</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <%-- <td>
                                    <asp:RadioButton ID="RadioButton1" runat="server" Checked="true" /></td>
                                <td>Not Requested Yet</td>
                                <td>Requested</td>
                                <td><input type="radio" name="1"/></td>
                                <td>Online</td>
                                <td><input type="radio" name="1"/></td>--%>
                                    </tr>
                                </table>
                            </div>
                            <div class="search-form1">
                                <div class="search-form1-left">
                                    Contract date: &nbsp; From
                                </div>
                                <div class="search-form1-right1">
                                    <asp:TextBox ID="txtFromdate" runat="server" CssClass="inputbox1"></asp:TextBox><asp:HiddenField
                                        ID="hdnfromdate" runat="server" />
                                </div>
                                <div class="search-form1-right2">
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate"
                                        PopupButtonID="calFrom" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeFrom">
                                    </asp:CalendarExtender>
                                    <input type="image" src="../images/date-icon.png" id="calFrom" />
                                </div>
                                <div class="search-form1-right3">
                                    To
                                </div>
                                <div class="search-form1-right1">
                                    <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox1"></asp:TextBox><asp:HiddenField
                                        ID="hdntodate" runat="server" />
                                </div>
                                <div class="search-form1-right2">
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate"
                                        PopupButtonID="calTo" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo">
                                    </asp:CalendarExtender>
                                    <input type="image" src="../images/date-icon.png" id="calTo" />
                                </div>
                            </div>
                            <div class="search-form1">
                                <div class="search-form1-left">
                                    Company client:
                                </div>
                                <div class="search-form1-right" style="padding-top: 3px;">
                                    <asp:TextBox ID="uiTextBoxCompanyClient" runat="server" class="inputbox"></asp:TextBox>
                                </div>
                            </div>
                            <div class="search-form1">
                                <div class="search-form1-left">
                                    Facility (hotel):
                                </div>
                                <div class="search-form1-right">
                                    <asp:TextBox ID="uiTextBoxFacility" runat="server" class="inputbox"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divpagedetailsM" runat="server">
            <div>
                <div style="padding-left: 715px;">
                    <b>
                        <asp:Label ID="lblRequestonline" runat="server" Text=""></asp:Label></b>
                </div>
                <div class="search-btn-body" style="margin-left: 716px;">
                    <asp:LinkButton ID="lbtSearch" runat="server" CssClass="Search-btn" OnClick="lbtSearch_Click">Search</asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                        ID="lnkClear" runat="server" CssClass="Search-btn" OnClick="lnkClear_Click">&nbsp;&nbsp;Clear&nbsp;&nbsp;</asp:LinkButton>
                </div>
            </div>
            <!-- end search-operator-layout-->
            <!--contract-list start -->
            <div class="contract-list" id="divAlphabeticPaging" runat="server" style="margin-top: 50px;">
                <div class="contract-list-left" style="width: 278px;">
                    <h2>
                        Contract list</h2>
                </div>
                <div class="contract-list-right" style="width: 676px;">
                    <ul runat="server" id="AlphaList">
                        <li runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">all</a></li>
                        <li runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">a</a></li>
                        <li runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">b</a></li>
                        <li runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">c</a></li>
                        <li runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">d</a></li>
                        <li runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">e</a></li>
                        <li runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">f</a></li>
                        <li runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">g</a></li>
                        <li runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">h</a></li>
                        <li runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">i</a></li>
                        <li runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">j</a></li>
                        <li runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">k</a></li>
                        <li runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">l</a></li>
                        <li runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">m</a></li>
                        <li runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">n</a></li>
                        <li runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">o</a></li>
                        <li runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">p</a></li>
                        <li runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">q</a></li>
                        <li runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">r</a></li>
                        <li runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">s</a></li>
                        <li runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">t</a></li>
                        <li runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">u</a></li>
                        <li runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">v</a></li>
                        <li runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">w</a></li>
                        <li runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">x</a></li>
                        <li runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">y</a></li>
                        <li runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">z</a></li>
                    </ul>
                </div>
            </div>
            <div class="pageing-operator" style="border: none;">
                <div class="pageing-operator-left" id="divAdd" runat="server">
                    <asp:LinkButton ID="uiButtonNewContract" runat="server" CssClass="Add-contract-btn"
                        Text="Add New Contract" OnClick="uiButtonNewContract_Click" />
                </div>
                <div style="float: left; width: 55%" id="pageing">
                </div>
            </div>
            <!-- end contract-list-->
            <!--Contract-body start -->
            <div class="contract-body-new" class="clearfix">
                <div id="divMessageOnGridHeader" runat="server">
                </div>
                <div class="contract-body-heading">
                    <div class="contract-heading-box1">
                        <span>Contract nr.</span>
                    </div>
                    <div class="contract-heading-box2" style="padding-top: 15px; min-height: 0px;">
                        <span>Country</span><br>
                        City
                        <%--<div class="contract-heading-box2-bottom">
                            <div class="rowElem">
                                <asp:DropDownList ID="uiDropDownListCountryGrid" runat="server" AutoPostBack="True" CssClass="NoClassApply" Width="90px"
                                    OnSelectedIndexChanged="uiDropDownListCountryGrid_SelectedIndexChanged">
                                    <asp:ListItem Text="Country" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>--%>
                        <%--<div class="contract-heading-box2-bottom">
                            <div class="rowElem">
                                <asp:DropDownList ID="drpCityGrid" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCityGrid_SelectedIndexChanged" CssClass="NoClassApply" Width="90px" Enabled="false">
                                    <asp:ListItem Text="City" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>--%>
                    </div>
                    <div class="contract-heading-box3">
                        <div class="rowElem">
                            <asp:DropDownList ID="drpHotelName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpHotelName_SelectedIndexChanged"
                                CssClass="NoClassApply" Width="125px">
                                <asp:ListItem Text="Select Hotel" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="contract-heading-box4">
                        <span>Client (company)</span><br>
                        Contact person
                    </div>
                    <div class="contract-heading-box5">
                        <span>Contact phone</span><br>
                        Contact email
                    </div>
                    <div class="contract-heading-box6">
                        <span>Sales agent</span><br>
                        <asp:LinkButton ID="creationdate" Style="color: #1b7ac2;" runat="server" OnClick="creationdate_Click">
                            Insert Date<asp:Image ImageUrl="~/Images/sortdescending.gif" ID="orderimg" runat="server" /></asp:LinkButton>
                    </div>
                    <div class="contract-heading-box7">
                        <span>Value</span>
                    </div>
                    <div class="contract-heading-box8">
                        <span>Scan</span>
                    </div>
                    <div class="contract-heading-box9">
                        <div class="rowElem">
                            <asp:DropDownList ID="drpActive" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpActive_SelectedIndexChanged"
                                CssClass="NoClassApply" Width="80px">
                                <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <asp:GridView runat="server" ID="grvClientContract" AutoGenerateColumns="false" Width="100%"
                    OnRowDataBound="grvClientContract_RowDataBound" AllowPaging="True" EmptyDataRowStyle-HorizontalAlign="Center"
                    PageSize="10" OnPageIndexChanging="grvClientContract_PageIndexChanging" OnRowCommand="grvClientContract_RowCommand"
                    BorderWidth="0" GridLines="None" CellPadding="0" CellSpacing="0" ShowHeader="false"
                    CssClass="floatleft">
                    <PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                        Font-Strikeout="False" BorderWidth="0" />
                    <EmptyDataTemplate>
                        <div class="booking-details" style="width: 960px;" id="divLastDetails" runat="server">
                            <ul>
                                <li class="value10">
                                    <div class="col21" style="width: 960px;">
                                        <div class="button_section">
                                            Search result not found.
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <div class="contract-body-box" class="clearfix">
                                    <ul>
                                        <li>
                                            <div class="contract-li-box1">
                                                <span><b>
                                                    <asp:Label ID="lblContractId" runat="server"></asp:Label></b></span>
                                            </div>
                                            <div class="contract-li-box2">
                                                <span><b>
                                                    <asp:Label ID="lblCountryName" runat="server"></asp:Label></b></span><br>
                                                <asp:Label ID="lblCity" runat="server"></asp:Label>
                                            </div>
                                            <div class="contract-li-box3">
                                                <asp:LinkButton ID="lnbName" runat="server" CommandArgument='<%#Eval("Id")%>' CommandName="View"></asp:LinkButton>
                                            </div>
                                            <div class="contract-li-box4">
                                                <span><b>
                                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label></b></span><br>
                                                <asp:Label ID="lblContactPerson" runat="server"></asp:Label><br>
                                                <%if (Session["CurrentSalesPersonID"] == null)
                                                  { %>
                                                <asp:LinkButton ID="hypEye" runat="server" Visible="false" OnClick="hypEye_Click"
                                                    CommandName='<%#Eval("Id")%>'>
                                                    <asp:Image ID="imgEye" runat="server" align="right" Style="margin: 0px 5px 1px 0px" /></asp:LinkButton>
                                                    <%} %>
                                            </div>
                                            <div class="contract-li-box5">
                                                <span><b>
                                                    <asp:Label ID="lblPhone" runat="server"></asp:Label></b></span><br>
                                                <asp:Label ID="uiLabelEmail" runat="server"></asp:Label><br>
                                            </div>
                                            <div class="contract-li-box6">
                                                <span><b>
                                                    <asp:Label ID="lblStaffName" runat="server"></asp:Label></b></span><br>
                                                <%# Eval("CreationDate","{0:dd/MM/yy}") %>
                                            </div>
                                            <div class="contract-li-box7">
                                                <span>
                                                    <%# String.Format("{0:##,####.##}",Eval("ContractValue")) %></span>
                                                <%--<asp:Label ID="lblContractValue" runat="server" Text='<%# Eval("ContractValue") %>'></asp:Label>--%>
                                            </div>
                                            <div class="contract-li-box8">
                                                <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><img src="../images/pdf-icon.png" border="0" alt="<%# Eval("HotelPlan") %>" title="<%# Eval("HotelPlan") %>"/></asp:HyperLink>
                                            </div>
                                            <div class="contract-li-box9">
                                                <asp:CheckBox ID="uiCheckBoxIsActive" runat="server" AutoPostBack="true" OnCheckedChanged="uiCheckBoxIsActive_CheckedChanged" />
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align=middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
                
                <asp:Panel ID="pnlFrame" runat="server" Width="100%" Visible="false">
                    <div style="width: 1000px; position: absolute; left: auto; top: 112px; z-index: 100;
                        overflow: hidden; border: 15px solid #333; background: #999;">
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" Text="Hotel Name : "
                            ForeColor="White" Font-Bold="true"></asp:Label><asp:Label ID="lblFacilityName" runat="server"
                                Text="" ForeColor="White" Font-Bold="true"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label3" runat="server" Text="Contract Number : " ForeColor="White"
                            Font-Bold="true"></asp:Label><asp:Label ID="lblContractNumber" runat="server" Text=""
                                ForeColor="White" Font-Bold="true"></asp:Label>
                        <asp:LinkButton runat="server" ID="lnb" Style="display: block; clear: both; float: right;
                            color: #000; font-size: 16px; font-weight: bold; margin-right: 4px; padding: 2px;
                            text-decoration: none;" OnClick="lnb_Click">X</asp:LinkButton>
                            <asp:UpdatePanel ID="frameupdate" runat="server" ><ContentTemplate>
                        <iframe scrolling="auto" src="../HotelUser/HotelDashboard.aspx" width="998px" height="870px"
                            frameborder="1" title="Hotel Details" scrolling="yes">Your browser does not support
                            IFRAMEs. </iframe></ContentTemplate></asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <!-- end search-operator-layout-->
        <!--contract hotal start -->
        <!-- end contract-hotal-->
        <!--contract hotal body start -->
        <asp:UpdateProgress ID="upDateProgress" runat="server" AssociatedUpdatePanelID="upDate">
            <ProgressTemplate>
                <div id="Loding_overlaySec" style="display: block;">
                    <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                    Loading...</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div class="contract-hotal-body" runat="server" id="divcontract">
            <div id="divmessage" runat="server" class="error">
            </div>
            <div class="contract-hotal-body-left">
                <asp:UpdatePanel ID="upDate" runat="server">
                    <ContentTemplate>
                        <div class="myclass">
                            <!--contract hotal body start -->
                            <div class="contract-form">
                                <div class="contract-form-left">
                                    Country<span style="color: Red"> *</span>
                                </div>
                                <div class="contract-form-right">
                                    <div id="divCountryV" class="rowElem">
                                        <asp:DropDownList ID="drpCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCountry_SelectedIndexChanged"
                                            CssClass="NoClassApply">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="contract-form-dark">
                                <div class="contract-form-left">
                                    Currency<span style="color: Red"> *</span>
                                </div>
                                <div class="contract-form-right">
                                    <div class="rowElem" id="divCurrency">
                                        <asp:DropDownList ID="drpCurrency" runat="server" CssClass="NoClassApply" Enabled="false">
                                            <asp:ListItem Text="Select Currency" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="contract-form">
                                <div class="contract-form-left">
                                    City<span style="color: Red"> *</span>
                                </div>
                                <div class="contract-form-right">
                                    <div class="rowElem" id="divCityV">
                                        <asp:DropDownList ID="drpCity" runat="server" CssClass="NoClassApply" AutoPostBack="True"
                                            Enabled="false" OnSelectedIndexChanged="drpCity_SelectedIndexChanged">
                                            <asp:ListItem Text="Select City" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="contract-form-dark">
                                <div class="contract-form-left">
                                    Zone<span style="color: Red"> *</span>
                                </div>
                                <div class="contract-form-right">
                                    <div class="rowElem" id="divZoneV">
                                        <asp:DropDownList ID="drpZone" runat="server" onchange="javacript:setAddressValue(this.id);"
                                            Enabled="false" CssClass="NoClassApply">
                                            <asp:ListItem Text="Select Zone" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="contract-form" id="divClientNameV" runat="server">
                                <div class="contract-form-left">
                                    Client name (company)<span style="color: Red"> *</span>
                                </div>
                                <div class="contract-form-right">
                                    <div class="rowElem" id="clientname">
                                        <asp:DropDownList ID="drpClientName" runat="server" CssClass="NoClassApply" AutoPostBack="true"
                                            OnSelectedIndexChanged="drpClientName_SelectedIndexChanged">
                                            <asp:ListItem Text="Select Client" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="contract-form" id="DivUserFirstName" runat="server">
                                <div class="contract-form-left">
                                    Name<span style="color: Red"> *</span>
                                </div>
                                <div class="contract-form-right">
                                    <div class="rowElem" id="div4">
                                        <asp:TextBox ID="uiTextBoxUserFirstName" runat="server" CssClass="inputbox" MaxLength="100"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="contract-form" id="DivVatNumber" runat="server">
                                <div class="contract-form-left">
                                    VAT Number<span style="color: Red"> *</span>
                                </div>
                                <div class="contract-form-right">
                                    <div class="rowElem" id="div3">
                                        <asp:TextBox ID="uiTextBoxVATNumber" runat="server" CssClass="inputbox" MaxLength="20"></asp:TextBox><asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender4" TargetControlID="uiTextBoxVATNumber" FilterType="Numbers,LowercaseLetters,UppercaseLetters"
                                            runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="contract-form-dark">
                                <div class="contract-form-left">
                                    Sales Person<span style="color: Red"> *</span>
                                </div>
                                <div class="contract-form-right">
                                    <div class="rowElem" id="divsalespersonV">
                                        <asp:DropDownList ID="drpSalesperson" runat="server" CssClass="NoClassApply">
                                            <asp:ListItem Text="Select Sales Person" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="contract-form">
                    <div class="contract-form-left">
                        Hotel name<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <asp:TextBox ID="txtHotelName" runat="server" CssClass="inputbox" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="contract-form-dark">
                    <div class="contract-form-left">
                        Hotel Address<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <asp:TextBox ID="txtHotelAddress" runat="server" CssClass="inputbox" MaxLength="500"
                            onblur="javacript:setAddressValue(this.id);"></asp:TextBox>
                    </div>
                </div>
                <div class="contract-form">
                    <div class="contract-form-left">
                        Star
                    </div>
                    <div class="contract-form-right">
                        <asp:Rating ID="Rating1" runat="server" MaxRating="7" CurrentRating="0" CssClass="ratingStar"
                            Style="float: left;" StarCssClass="ratingItem" WaitingStarCssClass="Saved" FilledStarCssClass="Filled"
                            EmptyStarCssClass="Empty">
                        </asp:Rating>
                        <span id="clearRating" style="display: none; float: left; padding-top: 10px;"><a
                            href="javascript:void(0)">
                            <img src="../Images/delete-ol-btn.png" alt="clear ranking" border="0" /></a></span>
                        <asp:HiddenField ID="uiHiddenFieldRatingStar" runat="server" />
                    </div>
                </div>
                <div class="contract-form-dark">
                    <div class="contract-form-left">
                        Latitude/Longitude<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right" style="padding: 9px 0px 10px 5px;">
                        <asp:TextBox ID="lblLatitude" runat="server" MaxLength="50" CssClass="inputbox"></asp:TextBox><asp:FilteredTextBoxExtender
                            ID="FilteredTextBoxExtender2" TargetControlID="lblLatitude" ValidChars="-0123456789."
                            runat="server">
                        </asp:FilteredTextBoxExtender>
                        /<asp:TextBox ID="lblLongitude" runat="server" CssClass="inputbox" MaxLength="50"></asp:TextBox><asp:FilteredTextBoxExtender
                            ID="FilteredTextBoxExtender3" TargetControlID="lblLongitude" ValidChars="-0123456789."
                            runat="server">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="contract-form">
                    <div class="contract-form-left">
                        Hotel Contact person<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <asp:TextBox ID="txtContactPerson" runat="server" CssClass="inputbox" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="contract-form-dark">
                    <div class="contract-form-left">
                        Hotel email account<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <asp:TextBox ID="txtEmailAccount" runat="server" CssClass="inputbox" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="contract-form-dark" id="confirmEmail" runat="server" style="border-top: none;">
                    <div class="contract-form-left">
                        Confirm email account<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="inputbox" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="contract-form">
                    <div class="contract-form-left">
                        Accounting Officer<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <asp:TextBox ID="txtAccountingOfficer" runat="server" CssClass="inputbox" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="contract-form-dark">
                    <div class="contract-form-left">
                        Hotel Phone<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right3">
                        <div class="contract-form-right3-left">
                            <div class="rowElem" id="phoneextension">
                                <asp:DropDownList ID="drpExtPhone" runat="server" CssClass="NoClassApply" Width="80px">
                                    <asp:ListItem Value="">Extension</asp:ListItem>
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>27</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>31</asp:ListItem>
                                    <asp:ListItem>32</asp:ListItem>
                                    <asp:ListItem>33</asp:ListItem>
                                    <asp:ListItem>34</asp:ListItem>
                                    <asp:ListItem>36</asp:ListItem>
                                    <asp:ListItem>39</asp:ListItem>
                                    <asp:ListItem>40</asp:ListItem>
                                    <asp:ListItem>41</asp:ListItem>
                                    <asp:ListItem>43</asp:ListItem>
                                    <asp:ListItem>44</asp:ListItem>
                                    <asp:ListItem>45</asp:ListItem>
                                    <asp:ListItem>46</asp:ListItem>
                                    <asp:ListItem>47</asp:ListItem>
                                    <asp:ListItem>48</asp:ListItem>
                                    <asp:ListItem>49</asp:ListItem>
                                    <asp:ListItem>51</asp:ListItem>
                                    <asp:ListItem>52</asp:ListItem>
                                    <asp:ListItem>53</asp:ListItem>
                                    <asp:ListItem>54</asp:ListItem>
                                    <asp:ListItem>55</asp:ListItem>
                                    <asp:ListItem>56</asp:ListItem>
                                    <asp:ListItem>57</asp:ListItem>
                                    <asp:ListItem>58</asp:ListItem>
                                    <asp:ListItem>60</asp:ListItem>
                                    <asp:ListItem>61</asp:ListItem>
                                    <asp:ListItem>62</asp:ListItem>
                                    <asp:ListItem>63</asp:ListItem>
                                    <asp:ListItem>64</asp:ListItem>
                                    <asp:ListItem>65</asp:ListItem>
                                    <asp:ListItem>66</asp:ListItem>
                                    <asp:ListItem>81</asp:ListItem>
                                    <asp:ListItem>82</asp:ListItem>
                                    <asp:ListItem>84</asp:ListItem>
                                    <asp:ListItem>86</asp:ListItem>
                                    <asp:ListItem>90</asp:ListItem>
                                    <asp:ListItem>91</asp:ListItem>
                                    <asp:ListItem>92</asp:ListItem>
                                    <asp:ListItem>93</asp:ListItem>
                                    <asp:ListItem>94</asp:ListItem>
                                    <asp:ListItem>95</asp:ListItem>
                                    <asp:ListItem>98</asp:ListItem>
                                    <asp:ListItem>212</asp:ListItem>
                                    <asp:ListItem>213</asp:ListItem>
                                    <asp:ListItem>216</asp:ListItem>
                                    <asp:ListItem>218</asp:ListItem>
                                    <asp:ListItem>220</asp:ListItem>
                                    <asp:ListItem>221</asp:ListItem>
                                    <asp:ListItem>222</asp:ListItem>
                                    <asp:ListItem>223</asp:ListItem>
                                    <asp:ListItem>224</asp:ListItem>
                                    <asp:ListItem>225</asp:ListItem>
                                    <asp:ListItem>226</asp:ListItem>
                                    <asp:ListItem>227</asp:ListItem>
                                    <asp:ListItem>228</asp:ListItem>
                                    <asp:ListItem>229</asp:ListItem>
                                    <asp:ListItem>230</asp:ListItem>
                                    <asp:ListItem>231</asp:ListItem>
                                    <asp:ListItem>232</asp:ListItem>
                                    <asp:ListItem>233</asp:ListItem>
                                    <asp:ListItem>234</asp:ListItem>
                                    <asp:ListItem>235</asp:ListItem>
                                    <asp:ListItem>236</asp:ListItem>
                                    <asp:ListItem>237</asp:ListItem>
                                    <asp:ListItem>238</asp:ListItem>
                                    <asp:ListItem>239</asp:ListItem>
                                    <asp:ListItem>240</asp:ListItem>
                                    <asp:ListItem>241</asp:ListItem>
                                    <asp:ListItem>242</asp:ListItem>
                                    <asp:ListItem>243</asp:ListItem>
                                    <asp:ListItem>244</asp:ListItem>
                                    <asp:ListItem>245</asp:ListItem>
                                    <asp:ListItem>248</asp:ListItem>
                                    <asp:ListItem>249</asp:ListItem>
                                    <asp:ListItem>250</asp:ListItem>
                                    <asp:ListItem>251</asp:ListItem>
                                    <asp:ListItem>252</asp:ListItem>
                                    <asp:ListItem>253</asp:ListItem>
                                    <asp:ListItem>254</asp:ListItem>
                                    <asp:ListItem>255</asp:ListItem>
                                    <asp:ListItem>256</asp:ListItem>
                                    <asp:ListItem>257</asp:ListItem>
                                    <asp:ListItem>258</asp:ListItem>
                                    <asp:ListItem>260</asp:ListItem>
                                    <asp:ListItem>261</asp:ListItem>
                                    <asp:ListItem>262</asp:ListItem>
                                    <asp:ListItem>263</asp:ListItem>
                                    <asp:ListItem>264</asp:ListItem>
                                    <asp:ListItem>265</asp:ListItem>
                                    <asp:ListItem>266</asp:ListItem>
                                    <asp:ListItem>267</asp:ListItem>
                                    <asp:ListItem>268</asp:ListItem>
                                    <asp:ListItem>269</asp:ListItem>
                                    <asp:ListItem>290</asp:ListItem>
                                    <asp:ListItem>291</asp:ListItem>
                                    <asp:ListItem>297</asp:ListItem>
                                    <asp:ListItem>298</asp:ListItem>
                                    <asp:ListItem>299</asp:ListItem>
                                    <asp:ListItem>350</asp:ListItem>
                                    <asp:ListItem>351</asp:ListItem>
                                    <asp:ListItem>352</asp:ListItem>
                                    <asp:ListItem>353</asp:ListItem>
                                    <asp:ListItem>354</asp:ListItem>
                                    <asp:ListItem>355</asp:ListItem>
                                    <asp:ListItem>356</asp:ListItem>
                                    <asp:ListItem>357</asp:ListItem>
                                    <asp:ListItem>358</asp:ListItem>
                                    <asp:ListItem>359</asp:ListItem>
                                    <asp:ListItem>370</asp:ListItem>
                                    <asp:ListItem>371</asp:ListItem>
                                    <asp:ListItem>372</asp:ListItem>
                                    <asp:ListItem>373</asp:ListItem>
                                    <asp:ListItem>374</asp:ListItem>
                                    <asp:ListItem>375</asp:ListItem>
                                    <asp:ListItem>376</asp:ListItem>
                                    <asp:ListItem>377</asp:ListItem>
                                    <asp:ListItem>378</asp:ListItem>
                                    <asp:ListItem>380</asp:ListItem>
                                    <asp:ListItem>381</asp:ListItem>
                                    <asp:ListItem>382</asp:ListItem>
                                    <asp:ListItem>385</asp:ListItem>
                                    <asp:ListItem>386</asp:ListItem>
                                    <asp:ListItem>387</asp:ListItem>
                                    <asp:ListItem>389</asp:ListItem>
                                    <asp:ListItem>420</asp:ListItem>
                                    <asp:ListItem>421</asp:ListItem>
                                    <asp:ListItem>423</asp:ListItem>
                                    <asp:ListItem>500</asp:ListItem>
                                    <asp:ListItem>501</asp:ListItem>
                                    <asp:ListItem>502</asp:ListItem>
                                    <asp:ListItem>503</asp:ListItem>
                                    <asp:ListItem>504</asp:ListItem>
                                    <asp:ListItem>505</asp:ListItem>
                                    <asp:ListItem>506</asp:ListItem>
                                    <asp:ListItem>507</asp:ListItem>
                                    <asp:ListItem>508</asp:ListItem>
                                    <asp:ListItem>509</asp:ListItem>
                                    <asp:ListItem>590</asp:ListItem>
                                    <asp:ListItem>591</asp:ListItem>
                                    <asp:ListItem>592</asp:ListItem>
                                    <asp:ListItem>593</asp:ListItem>
                                    <asp:ListItem>595</asp:ListItem>
                                    <asp:ListItem>597</asp:ListItem>
                                    <asp:ListItem>598</asp:ListItem>
                                    <asp:ListItem>599</asp:ListItem>
                                    <asp:ListItem>670</asp:ListItem>
                                    <asp:ListItem>672</asp:ListItem>
                                    <asp:ListItem>673</asp:ListItem>
                                    <asp:ListItem>674</asp:ListItem>
                                    <asp:ListItem>675</asp:ListItem>
                                    <asp:ListItem>676</asp:ListItem>
                                    <asp:ListItem>677</asp:ListItem>
                                    <asp:ListItem>678</asp:ListItem>
                                    <asp:ListItem>679</asp:ListItem>
                                    <asp:ListItem>680</asp:ListItem>
                                    <asp:ListItem>681</asp:ListItem>
                                    <asp:ListItem>682</asp:ListItem>
                                    <asp:ListItem>683</asp:ListItem>
                                    <asp:ListItem>685</asp:ListItem>
                                    <asp:ListItem>686</asp:ListItem>
                                    <asp:ListItem>687</asp:ListItem>
                                    <asp:ListItem>688</asp:ListItem>
                                    <asp:ListItem>689</asp:ListItem>
                                    <asp:ListItem>690</asp:ListItem>
                                    <asp:ListItem>691</asp:ListItem>
                                    <asp:ListItem>692</asp:ListItem>
                                    <asp:ListItem>850</asp:ListItem>
                                    <asp:ListItem>852</asp:ListItem>
                                    <asp:ListItem>853</asp:ListItem>
                                    <asp:ListItem>855</asp:ListItem>
                                    <asp:ListItem>856</asp:ListItem>
                                    <asp:ListItem>870</asp:ListItem>
                                    <asp:ListItem>880</asp:ListItem>
                                    <asp:ListItem>886</asp:ListItem>
                                    <asp:ListItem>960</asp:ListItem>
                                    <asp:ListItem>961</asp:ListItem>
                                    <asp:ListItem>962</asp:ListItem>
                                    <asp:ListItem>963</asp:ListItem>
                                    <asp:ListItem>964</asp:ListItem>
                                    <asp:ListItem>965</asp:ListItem>
                                    <asp:ListItem>966</asp:ListItem>
                                    <asp:ListItem>967</asp:ListItem>
                                    <asp:ListItem>968</asp:ListItem>
                                    <asp:ListItem>970</asp:ListItem>
                                    <asp:ListItem>971</asp:ListItem>
                                    <asp:ListItem>972</asp:ListItem>
                                    <asp:ListItem>973</asp:ListItem>
                                    <asp:ListItem>974</asp:ListItem>
                                    <asp:ListItem>975</asp:ListItem>
                                    <asp:ListItem>976</asp:ListItem>
                                    <asp:ListItem>977</asp:ListItem>
                                    <asp:ListItem>992</asp:ListItem>
                                    <asp:ListItem>993</asp:ListItem>
                                    <asp:ListItem>994</asp:ListItem>
                                    <asp:ListItem>995</asp:ListItem>
                                    <asp:ListItem>996</asp:ListItem>
                                    <asp:ListItem>998</asp:ListItem>
                                    <asp:ListItem>1242</asp:ListItem>
                                    <asp:ListItem>1246</asp:ListItem>
                                    <asp:ListItem>1264</asp:ListItem>
                                    <asp:ListItem>1268</asp:ListItem>
                                    <asp:ListItem>1284</asp:ListItem>
                                    <asp:ListItem>1340</asp:ListItem>
                                    <asp:ListItem>1345</asp:ListItem>
                                    <asp:ListItem>1441</asp:ListItem>
                                    <asp:ListItem>1473</asp:ListItem>
                                    <asp:ListItem>1599</asp:ListItem>
                                    <asp:ListItem>1649</asp:ListItem>
                                    <asp:ListItem>1664</asp:ListItem>
                                    <asp:ListItem>1670</asp:ListItem>
                                    <asp:ListItem>1671</asp:ListItem>
                                    <asp:ListItem>1684</asp:ListItem>
                                    <asp:ListItem>1758</asp:ListItem>
                                    <asp:ListItem>1767</asp:ListItem>
                                    <asp:ListItem>1784</asp:ListItem>
                                    <asp:ListItem>1809</asp:ListItem>
                                    <asp:ListItem>1868</asp:ListItem>
                                    <asp:ListItem>1869</asp:ListItem>
                                    <asp:ListItem>1876</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="contract-form-right3-right">
                            <asp:TextBox ID="txtHotelPhone" runat="server" CssClass="inputbox" MaxLength="20"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" TargetControlID="txtHotelPhone"
                                ValidChars="0123456789" runat="server">
                            </asp:FilteredTextBoxExtender>
                        </div>
                    </div>
                </div>
                <div class="contract-form">
                    <div class="contract-form-left">
                        Contract value in euro<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <asp:TextBox ID="txtContractValue" runat="server" CssClass="inputbox" MaxLength="10"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtContractValue"
                            ValidChars="0123456789." runat="server">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="contract-form-dark">
                    <div class="contract-form-left">
                        Operator Choice<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <div class="rowElem" id="divOperatorCoiceV">
                            <asp:DropDownList ID="drpOperatorChoice" runat="server" CssClass="NoClassApply">
                                <asp:ListItem Value=""> Select Choice</asp:ListItem>
                                <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                                <asp:ListItem Value="Good">Good</asp:ListItem>
                                <asp:ListItem Value="Average">Average</asp:ListItem>
                                <asp:ListItem Value="Bad">Bad</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="contract-form">
                    <div class="contract-form-left">
                        Group Name
                    </div>
                    <div class="contract-form-right">
                        <div class="rowElem">
                            <asp:DropDownList ID="drpGroup" runat="server" CssClass="NoClassApply">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="contract-form-dark">
                    <div class="contract-form-left">
                        Scanned file<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <div class="browse-cancel-btn">
                            <asp:FileUpload ID="ScanFile" runat="server" Width="200px" /><br />
                            <span style="font-family: Arial; font-size: Smaller;">File format: Pdf & max size 1MB.
                            </span>
                        </div>
                    </div>
                </div>
                <div class="contract-form">
                    <div class="contract-form-left">
                        Logo file<span style="color: Red"> *</span>
                    </div>
                    <div class="contract-form-right">
                        <div class="browse-cancel-btn">
                            <asp:FileUpload ID="uiFileUploadLogo" runat="server" Width="200px" /><br />
                            <span style="font-family: Arial; font-size: Smaller;">Image format: JPG,JPEG,PNG,GIF
                                & max size <%=System.Configuration.ConfigurationManager.AppSettings["MessageKB2"]%>. Ideal image dimension: 168x168 </span>
                        </div>
                    </div>
                </div>
                <div class="booking-details" style="width: 960px;">
                    <ul>
                        <li class="value10">
                            <div class="col21" style="width: 960px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="uiButtonModify" runat="server" CssClass="select" Text="Modify"
                                        OnClick="uiButtonModify_Click" />&nbsp;&nbsp;
                                    <asp:LinkButton ID="uiButtonDelete" runat="server" CssClass="select" Text="Delete"
                                        OnClientClick="javascript:return confirm('Are you confirm for delete this contract?');"
                                        OnClick="uiButtonDelete_Click" />
                                    <asp:LinkButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="select" />
                                    <span>or</span>
                                    <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end save-cancel-btn-body-->
            <asp:HiddenField ID="uiHiddenFieldPrimaryKeyId" runat="server" />
            <asp:HiddenField ID="uiHiddenFieldFileUpload" runat="server" />
            <asp:HiddenField ID="uiHiddenFieldlogo" runat="server" />
            <!-- end contract hotal body-->
            <!--contract hotal body start -->
            <div class="contract-map-body" style="width: 525px; height: 840px; float: right;
                margin: 5px;" id="map_canvas">
            </div>
            <!-- end contract hotal body-->
        </div>
    </div>
    <div id="Loding_overlay">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Saving...</span></div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }
        });
    </script>
    <script language="javascript" type="text/javascript">
                        jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');
                                jQuery('#ContentPlaceHolder1_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                        });
    </script>
    <script language="javascript" type="text/javascript">
        function dateChangeFrom(s, e) {
            var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
            jQuery("#<%= hdnfromdate.ClientID %>").val(fromdatechange);
        }
        function dateChangeTo(s, e) {
            var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
            jQuery("#<%= hdntodate.ClientID %>").val(todatechange);
        }
        jQuery(document).ready(function () {
            jQuery("#searchmessage").hide();
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);
            jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
                var isValid = true;
                var errormsg = "";
                var totalchecked = jQuery("#chkclients .jqTransformChecked").length;
                var totalcontracts = jQuery("#chkcontracts .jqTransformChecked").length;
                if (totalchecked > 0) {
                    var elemetClient = jQuery("#<%= uiTextBoxCompanyClient.ClientID %>").val();
                    if (elemetClient == "" && elemetClient.length <= 0) {
                        if (errormsg.length > 0) {
                            errormsg += "<br/>";
                        }
                        errormsg += "Please select Company client";
                        isValid = false;
                    }
                }
                if (totalcontracts > 0) {
                    var elemetClient = jQuery("#<%= uiTextBoxFacility.ClientID %>").val();
                    if (elemetClient == "" && elemetClient.length <= 0) {
                        if (errormsg.length > 0) {
                            errormsg += "<br/>";
                        }
                        errormsg += "Please enter Facility(Hotel).";
                        isValid = false;
                    }
                }
                var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
                var todate = jQuery("#<%= txtTodate.ClientID %>").val();
                if (fromdate != "dd/mm/yy" && todate == "dd/mm/yy") {
                    if (errormsg.length > 0) {
                        errormsg += "<br/>";
                    }
                    errormsg += "Please select to date.";
                    isValid = false;
                }
                else if (fromdate == "dd/mm/yy" && todate != "dd/mm/yy") {
                    if (errormsg.length > 0) {
                        errormsg += "<br/>";
                    }
                    errormsg += "Please select from date.";
                    isValid = false;
                }
                else {
                    var todayArr = todate.split('/');
                    var formdayArr = fromdate.split('/');
                    var fromdatecheck = new Date();
                    var todatecheck = new Date();
                    fromdatecheck.setFullYear(parseInt("20" + formdayArr[2]), (parseInt(formdayArr[1]) - 1), formdayArr[0]);
                    todatecheck.setFullYear(parseInt("20" + todayArr[2]), (parseInt(todayArr[1]) - 1), todayArr[0]);
                    if (fromdatecheck > todatecheck) {
                        if (errormsg.length > 0) {
                            errormsg += "<br/>";
                        }
                        errormsg += "From date must be less than To date.";
                        isValid = false;
                    }
                }

                jQuery("#<%= hdnfromdate.ClientID %>").val(fromdate);
                jQuery("#<%= hdntodate.ClientID %>").val(todate);
                if (!isValid) {
                    jQuery("#searchmessage").show();
                    jQuery("#searchmessage").html(errormsg);
                    return false;
                }
                jQuery("#searchmessage").html("");
                jQuery("#searchmessage").hide();
                jQuery("#contentbody_txtFrom").attr("disabled", false);
                jQuery("#contentbody_txtTo").attr("disabled", false);
                jQuery("#Loding_overlay").show();
                jQuery("#Loding_overlay span").html("Loading...");
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
    <script type="text/javascript">
        var myMap = null;
        var count = 1;
        var confidenceStrings = ["High", "Medium", "Low"];
        var precisionStrings = ["Interpolated", "Rooftop"];

        var pushpinUrls = ["../Images/PinGreen.png", "../Images/PinOrange.png", "../Images/PinRed.png"];

        function LoadMap() {

            try {
                myMap = new VEMap("map");
            }
            catch (d) {
            }
            if (myMap != null) {
                myMap.LoadMap();
                StartGeocoding("Belgium");
            }

        }

        function UnloadMap() {
            if (myMap != null) {
                myMap.Dispose();
            }
        }

        function CallWhenYouWant(val, height) {
            jQuery('.myclass').jqTransform({ imgPath: ' ' });
            jQuery('.search-operator-from').jqTransform({ imgPath: ' ' });
            setAddressValue(val);
            var heightdiv = jQuery(".contract-hotal-body-left").height();
            if (height == '1') {
                jQuery("#map_canvas").height(heightdiv - 60);
            }
            else {
                jQuery("#map_canvas").height(heightdiv - 60);
            }
        }

        function CallOnchangeCountry() {
            jQuery('#radioKM input:radio').addClass('NoClassApply');
            jQuery('#radioKM input:radio').width(20);
            jQuery('.myclass').jqTransform({ imgPath: ' ' });
            jQuery('.search-operator-from').jqTransform({ imgPath: ' ' });

            //jQuery('.search-form-left').jqTransform({ imgPath: ' ' });
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);
        }

        function StartGeocoding(address) {
            myMap.Find(null,         // what
                  address,           // where
                  null,              // VEFindType (always VEFindType.Businesses)
                  null,              // VEShapeLayer (base by default)
                  null,              // start index for results (0 by default)
                  null,              // max number of results (default is 10)
                  null,              // show results? (default is true)
                  null,              // create pushpin for what results? (ignored since what is null)
                  null,              // use default disambiguation? (default is true)
                  null,              // set best map view? (default is true)
                  GeocodeCallback);  // call back function
        }
        var mycordinate = '<%=ViewState["mode"] %>';
        function GeocodeCallback(shapeLayer, findResults, places, moreResults, errorMsg) {

            var resHtml = "";

            // if there are no results, display the error message and return
            if (places == null) {
                //alert((errorMsg == null) ? "There were no results" : errorMsg);
                return;
            }

            // Create an entry for each VEPlace in the result set
            for (var p = 0; p < places.length; p++) {
                // Gather some info up front
                var place = places[p];
                var location = place.LatLong;
                var confString = confidenceStrings[place.MatchConfidence];
                var precString = precisionStrings[place.Precision];
                var mcVal = MatchCode(place.MatchCode);
                var latitude = location.Latitude;
                var longitude = location.Longitude;

                // create the info box description
                var desc = "Latitude: " + latitude + "<br/>" +
                          "Longitude: " + longitude;
                desc = desc + "<br>Confidence: " + confString;
                desc = desc + "<br>Precision: " + precString;
                desc = desc + "<br>Match Code: " + mcVal;

                // Create a pin at that location, list the latitude & longitude
                var pin = new VEShape(VEShapeType.Pushpin, location);
                pin.SetCustomIcon("<img src='" +
                                 pushpinUrls[place.MatchConfidence] +
                                 "'><span class='pinText'>" +
                                 (p + 1) +
                                 "</span>");
                pin.SetTitle(place.Name);
                pin.SetDescription(desc);
                myMap.AddShape(pin);

                // Add the information to the resultsDiv html, including a link 
                // that recenters the map over the pin
                resHtml = resHtml +
                         "<p><a href='javascript:myMap.SetCenter(new VELatLong(" +
                         latitude +
                         "," +
                         longitude +
                         "));'>";
                resHtml = resHtml +
                         "#" + (p + 1) +
                         ": " + place.Name +
                         " (" + precString +
                         ", " +
                         confString +
                         " confidence, " +
                         mcVal +
                         ")</a></p>";
            }

            // set the resultsDev html when we're done.
            document.getElementById("resultsDiv").innerHTML = resHtml;
            if (mycordinate != 'U') {
                jQuery("#<%=lblLongitude.ClientID %>").val(longitude);
                jQuery("#<%=lblLatitude.ClientID %>").val(latitude);
            }
            mycordinate = 'A';
        }

        function MatchCode(code) {
            if (code == VEMatchCode.None) {
                return "No match";
            }

            var codeDesc = "";
            var cVal;

            cVal = code & VEMatchCode.Good;
            if (cVal > 0) {
                codeDesc += "Good ";
            }

            cVal = code & VEMatchCode.Ambiguous;
            if (cVal > 0) {
                codeDesc += "Ambiguous ";
            }

            cVal = code & VEMatchCode.UpHierarchy;
            if (cVal > 0) {
                codeDesc += "UpHierarchy ";
            }

            cVal = code & VEMatchCode.Modified;
            if (cVal > 0) {
                codeDesc += "Modified ";
            }

            return (codeDesc + "Match");
        }



        //        function setAddressValue(elementID) {
        //            //jQuery(document).ready(function () {
        //            if (myMap == null) {
        //                myMap = new VEMap("map");
        //            }
        //            else {
        //                myMap.Clear();
        //            }
        //            if (elementID != null) {
        //                address = jQuery("#" + elementID + " option:selected").text();
        //                myMap.LoadMap();
        //                StartGeocoding(address);
        //            }
        //            else {
        //                myMap.LoadMap();
        //                StartGeocoding("Belgium");
        //            }
        //            //});
        //        }
        //        jQuery(document).ready(function () {
        //            LoadMap();
        //        });

        jQuery(window).unload(function () {
            UnloadMap();
        });
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnSave.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var hotelName = jQuery("#<%= txtHotelName.ClientID %>").val();
                var isvalid = true;
                var errormessage = "";
                var country = jQuery("#<%= drpCountry.ClientID %>").val();
                if (country == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Country.";
                    isvalid = false;
                }

                var Currency = jQuery("#<%= drpCurrency.ClientID %>").val();
                if (Currency == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Currency.";
                    isvalid = false;
                }

                var city = jQuery("#<%= drpCity.ClientID %>").val();
                if (city == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select City.";
                    isvalid = false;
                }

                var zone = jQuery("#<%= drpZone.ClientID %>").val();
                if (zone == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Zone.";
                    isvalid = false;
                }
                var clientNameV = jQuery("#<%= drpClientName.ClientID %>").val();
                if (clientNameV == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select client name.";
                    isvalid = false;
                }
                else if (clientNameV == "Others") {
                    var username = jQuery("#<%= uiTextBoxUserFirstName.ClientID %>").val();
                    if (username.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Name is required.";
                        isvalid = false;
                    }
                    var username = jQuery("#<%= uiTextBoxVATNumber.ClientID %>").val();
                    if (username.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "VAT Number is required.";
                        isvalid = false;
                    }
                }

                var salespersonV = jQuery("#<%= drpSalesperson.ClientID %>").val();
                if (salespersonV == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select sales person.";
                    isvalid = false;
                }



                if (hotelName.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "HotelName is required.";
                    isvalid = false;
                }
                var hotelAddress = jQuery("#<%= txtHotelAddress.ClientID %>").val();
                if (hotelAddress.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "HotelAddress is required.";
                    isvalid = false;
                }
                var latitude = jQuery("#<%= lblLatitude.ClientID %>").val();
                if (latitude.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Latitude is required.";
                    isvalid = false;
                }

                var longitude = jQuery("#<%= lblLongitude.ClientID %>").val();
                if (longitude.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Longitude is required.";
                    isvalid = false;
                }

                var contactPerson = jQuery("#<%= txtContactPerson.ClientID %>").val();
                if (contactPerson.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "ContactPerson is required.";
                    isvalid = false;
                }
                var emailAccount = jQuery("#<%= txtEmailAccount.ClientID %>").val();
                if (emailAccount.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Email is required.";
                    isvalid = false;
                }
                else {
                    if (!validateEmail(emailAccount)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Enter valid email.";
                        isvalid = false;
                    }
                    else {
                        var confirmemailAccount = jQuery("#<%= txtConfirmEmail.ClientID %>").val();
                        if (emailAccount != confirmemailAccount) {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += "Email not matched with confirm email.";
                            isvalid = false;
                        }
                    }
                }

                var emailOfficer = jQuery("#<%= txtAccountingOfficer.ClientID %>").val();
                if (emailOfficer.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "AccountingOfficer Email is required.";
                    isvalid = false;
                }
                else {
                    if (!validateEmail(emailOfficer)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Enter valid email for AccountingOfficer.";
                        isvalid = false;
                    }
                }

                var phoneext = jQuery("#<%= drpExtPhone.ClientID %>").val();
                if (phoneext == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select phone extension.";
                    isvalid = false;
                }

                var hotelPhone = jQuery("#<%= txtHotelPhone.ClientID %>").val();
                if (hotelPhone.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Hotel Phone is required.";
                    isvalid = false;
                }
                var contractValue = jQuery("#<%= txtContractValue.ClientID %>").val();
                if (contractValue.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "ContractValue is required.";
                    isvalid = false;
                }
                else if (parseFloat(contractValue) <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Enter valid value in ContractValue and value must be greater than zero also.";
                    isvalid = false;
                }
                else if (isNaN(contractValue)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Enter valid value in ContractValue.";
                    isvalid = false;
                }
                var operatorchoice = jQuery("#<%= drpOperatorChoice.ClientID %>").val();
                if (operatorchoice == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Operator Choice.";
                    isvalid = false;
                }
                var file = jQuery('#<%= ScanFile.ClientID %>').val();
                if (file.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Scanned pdf file is required.";
                    isvalid = false;
                }
                else {
                    if (!validate_file_format(file, "pdf,Pdf,PDF,pDF")) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Please select scanned file in (PDF) formats only.";
                        isvalid = false;
                    }
                }

                var fileLogo = jQuery('#<%= uiFileUploadLogo.ClientID %>').val();
                if (fileLogo.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Logo image file is required.";
                    isvalid = false;
                }
                else {
                    if (!validate_file_format(fileLogo, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Please select logo image file in (Gif, JPG, JPEG and PNG) formats only.";
                        isvalid = false;
                    }
                }

                if (!isvalid) {
                    jQuery("#<%= divmessage.ClientID %>").show();
                    jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                    var offseterror = jQuery("#<%= divmessage.ClientID %>").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }
                jQuery("#<%= divmessage.ClientID %>").html("");
                jQuery("#<%= divmessage.ClientID %>").hide();
                jQuery("#<%= lblLatitude.ClientID %>").attr("disabled", false);
                jQuery("#<%= lblLongitude.ClientID %>").attr("disabled", false);
                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });

        function validate_file_format(file_name, allowed_ext) {
            field_value = file_name;
            if (field_value != "") {
                var file_ext = (field_value.substring((field_value.lastIndexOf('.') + 1)).toLowerCase());
                ext = allowed_ext.split(',');
                var allow = 0;
                for (var i = 0; i < ext.length; i++) {
                    if (ext[i] == file_ext) {
                        allow = 1;
                    }
                }
                if (!allow) {
                    return false;
                }
                else {
                    return true;
                }
            }
            return false;
        } 
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {

            var currentVal2 = jQuery("#<%= Rating1.ClientID %>_RatingExtender_ClientState").val();
            if (parseInt(currentVal2, 10) > 0) {
                jQuery("#clearRating").show();
            }
            else {
                jQuery("#clearRating").hide();
            }

            jQuery("#<%= Rating1.ClientID %>_A").bind("click", function () {
                jQuery("#<%= Rating1.ClientID %>_RatingExtender_ClientState").val(jQuery("#<%= Rating1.ClientID %>_A").attr("title"));
                var valuerate = jQuery("#<%= Rating1.ClientID %>_RatingExtender_ClientState").val();
                if (parseInt(valuerate, 10) > 0) {
                    jQuery("#clearRating").show();
                }
            });
            jQuery("#clearRating a").bind("click", function () {
                jQuery("#<%= Rating1.ClientID %>_RatingExtender_ClientState").val("0");
                jQuery("#<%= Rating1.ClientID %>_A").attr("title", "0");
                jQuery("#<%= Rating1.ClientID %> a span").removeClass("ratingItem  Filled").addClass("ratingItem Empty");
                jQuery("#clearRating").hide();
            });

            jQuery("#<%= uiButtonModify.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }

                var isvalid = true;
                var errormessage = "";
                var country = jQuery("#<%= drpCountry.ClientID %>").val();
                if (country == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select country.";
                    isvalid = false;
                }

                var Currency = jQuery("#<%= drpCurrency.ClientID %>").val();
                if (Currency == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Currency.";
                    isvalid = false;
                }

                var city = jQuery("#<%= drpCity.ClientID %>").val();
                if (city == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select city.";
                    isvalid = false;
                }

                var zone = jQuery("#<%= drpZone.ClientID %>").val();
                if (zone == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Zone.";
                    isvalid = false;
                }
                var clientNameV = jQuery("#<%= drpClientName.ClientID %>").val();
                if (clientNameV == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select client name.";
                    isvalid = false;
                }

                var salespersonV = jQuery("#<%= drpSalesperson.ClientID %>").val();
                if (salespersonV == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select sales person.";
                    isvalid = false;
                }


                var hotelName = jQuery("#<%= txtHotelName.ClientID %>").val();
                if (hotelName.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "HotelName is required.";
                    isvalid = false;
                }
                var hotelAddress = jQuery("#<%= txtHotelAddress.ClientID %>").val();
                if (hotelAddress.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "HotelAddress is required.";
                    isvalid = false;
                }
                var latitude = jQuery("#<%= lblLatitude.ClientID %>").val();
                if (latitude.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Latitude is required.";
                    isvalid = false;
                }

                var longitude = jQuery("#<%= lblLongitude.ClientID %>").val();
                if (longitude.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Longitude is required.";
                    isvalid = false;
                }

                var contactPerson = jQuery("#<%= txtContactPerson.ClientID %>").val();
                if (contactPerson.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "ContactPerson is required.";
                    isvalid = false;
                }
                var emailAccount = jQuery("#<%= txtEmailAccount.ClientID %>").val();
                if (emailAccount.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Email is required.";
                    isvalid = false;
                }
                else {
                    if (!validateEmail(emailAccount)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Enter valid email.";
                        isvalid = false;
                    }
                }

                var emailOfficer = jQuery("#<%= txtAccountingOfficer.ClientID %>").val();
                if (emailOfficer.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "AccountingOfficer Email is required.";
                    isvalid = false;
                }
                else {
                    if (!validateEmail(emailOfficer)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Enter valid email for AccountingOfficer.";
                        isvalid = false;
                    }
                }

                var phoneext = jQuery("#<%= drpExtPhone.ClientID %>").val();
                if (phoneext == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select phone extension.";
                    isvalid = false;
                }

                var hotelPhone = jQuery("#<%= txtHotelPhone.ClientID %>").val();
                if (hotelPhone.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Hotel Phone is required.";
                    isvalid = false;
                }
                var contractValue = jQuery("#<%= txtContractValue.ClientID %>").val();
                if (contractValue.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "ContractValue is required.";
                    isvalid = false;
                }
                else if (parseFloat(contractValue) <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Enter valid value in ContractValue and value must be greater than zero also.";
                    isvalid = false;
                }
                else if (isNaN(contractValue)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Enter valid value in ContractValue.";
                    isvalid = false;
                }

                var operatorchoice = jQuery("#<%= drpOperatorChoice.ClientID %>").val();
                if (operatorchoice == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Operator Choice.";
                    isvalid = false;
                }
                var file = jQuery('#<%= ScanFile.ClientID %>').val();
                if (file.length <= 0) {

                }
                else {
                    if (!validate_file_format(file, "pdf,Pdf,PDF,pDF")) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Please select scanned file in (PDF) formats only.";
                        isvalid = false;
                    }
                }

                var fileLogo = jQuery('#<%= uiFileUploadLogo.ClientID %>').val();
                if (fileLogo.length <= 0) {

                }
                else {
                    if (!validate_file_format(fileLogo, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Please select logo image file in (Gif, JPG, JPEG and PNG) formats only.";
                        isvalid = false;
                    }
                }

                if (!isvalid) {
                    jQuery("#<%= divmessage.ClientID %>").show();
                    jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                    var offseterror = jQuery("#<%= divmessage.ClientID %>").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                    return false;
                }
                jQuery("#<%= divmessage.ClientID %>").html("");
                jQuery("#<%= divmessage.ClientID %>").hide();
                jQuery("#Loding_overlay").show();
                jQuery("#<%= lblLatitude.ClientID %>").attr("disabled", false);
                jQuery("#<%= lblLongitude.ClientID %>").attr("disabled", false);
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });

        function validateEmail(txtEmail) {
            var a = txtEmail;
            var filter = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{1,4}$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        var geocoder;
        var map;
        var markersArray = [];
        var marker;
        var address;
        var addressByUser;
        var Mylocation = "";
        var infowindow = new google.maps.InfoWindow();

        function setAddressValue(elementID) {

            if (elementID != null && elementID != 'Modify') {

                if (jQuery("#" + elementID).attr("type") == 'text') {

                    var Currency = jQuery("#<%= drpCurrency.ClientID %>").val();
                    var city = jQuery("#<%= drpCity.ClientID %>").val();
                    var zone = jQuery("#<%= drpZone.ClientID %>").val();
                    if (Currency == "") {
                        alert('Please select country.');
                    }


                    else if (city == "") {
                        alert('Please select city.');
                    }


                    else if (zone == "") {
                        alert('Please select zone');
                    }
                    else {
                        addressByUser = jQuery("#" + elementID).val() + "," + jQuery("#<%=drpZone.ClientID %> option:selected").text() + "," + jQuery("#<%=drpCity.ClientID %> option:selected").text(); +"," + jQuery("#<%=drpCountry.ClientID %> option:selected").text();

                        address = addressByUser;
                        initialize(16);
                    }

                }
                else {
                    addressByUser = jQuery("#" + elementID + " option:selected").text();
                    address = addressByUser;
                    initialize(10);

                }

            }
            else if (elementID != null && elementID == 'Modify') {
                addressByUser = jQuery("#<%=txtHotelAddress.ClientID %>").val() + "," + jQuery("#<%=drpZone.ClientID %> option:selected").text() + "," + jQuery("#<%=drpCity.ClientID %> option:selected").text(); +"," + jQuery("#<%=drpCountry.ClientID %> option:selected").text();

                address = addressByUser;
                initialize(16);
            }
            else {

                address = "europe";
                initialize(4);
            }
        }
        function initialize(zoomlevel) {
            geocoder = new google.maps.Geocoder();
            var myOptions = {
                zoom: zoomlevel,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            codeAddress();

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
            });

        }


        function codeAddress() {
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    clearOverlays();

                    var cordinate = results[0].geometry.location;
                    if (cordinate != null) {
                        var splitCordinate = cordinate.toString().split(',');
                        if (mycordinate != 'U') {
                            jQuery("#<%=lblLatitude.ClientID %>").val(splitCordinate[0]);
                            jQuery("#<%=lblLongitude.ClientID %>").val(splitCordinate[1]);
                            
                        }
                    }
                    map.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: map,
                        title: results[0]['formatted_address'],
                        position: results[0].geometry.location
                    });

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);

                    markersArray.push(marker);

                } else {

                    alert("Geocode was not successful for the following reason: " + status);

                }
            });
        }

        function placeMarker(location) {
            geocoder.geocode({ 'latLng': location }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        clearOverlays();
                        var clickplaceAddress = results[1].formatted_address;
                        if (clickplaceAddress != null) {
                            var splitAddress = clickplaceAddress.toString().split(',');
                            var selectedCountry = jQuery("#<%= drpCountry.ClientID %> option:selected").text();

                            if (clickplaceAddress.toLowerCase().indexOf(selectedCountry.toLowerCase()) != -1) {

                                var cordinate = results[0].geometry.location;
                                if (cordinate != null) {
                                    var splitCordinate = cordinate.toString().split(',');
                                    jQuery("#<%=lblLatitude.ClientID %>").val(splitCordinate[0]);
                                    jQuery("#<%=lblLongitude.ClientID %>").val(splitCordinate[1]);
                                }
                                marker = new google.maps.Marker({
                                    position: location,
                                    title: results[1].formatted_address,
                                    map: map
                                });

                                infowindow.setContent(results[1].formatted_address);
                                infowindow.open(map, marker);

                                markersArray.push(marker);
                                map.setCenter(location);

                                //mouseover
                                google.maps.event.addListener(marker, 'click', function () {
                                    infowindow.setContent(results[1].formatted_address);
                                    infowindow.open(map, this);
                                    //document.getElementById("latlong").innerText = results[0].geometry.location;
                                    //document.getElementById("address").value = results[0]['formatted_address'];
                                });
                            }
                            else {
                                alert('Please select your choice under selected country.');
                            }
                        } else {

                        }
                    }
                } else {
                    initialize(10);
                    alert("Geocoder failed due to: " + status);
                }
            });

        }


        function clearOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
            }
        }

        function toggleBounce() {

            if (marker.getAnimation() != null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }
        
    </script>
</asp:Content>
