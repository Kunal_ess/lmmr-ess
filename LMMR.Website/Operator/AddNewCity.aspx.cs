﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using LMMR.Data;

public partial class Operator_AddNewCity : System.Web.UI.Page
{
    HotelManager objHotelManager = new HotelManager();
    ClientContract objClient = new ClientContract();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_AddNewCity));  

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divmessage.Visible = false;
            BindAllCountry();
        }
    }

    public void BindAllCountry()
    {
        TList<Country> objCountry = objClient.GetByAllCountry();
        drpCountry.DataValueField = "Id";
        drpCountry.DataTextField = "CountryName";
        drpCountry.DataSource = objCountry.OrderBy(a => a.CountryName);
        drpCountry.DataBind();
        drpCountry.Items.Insert(0, new ListItem("Select Country", ""));
    }

    protected void lbtSave_Click(object sender, EventArgs e)
    {
        string cityName = txtCityName.Text;
        long countryId=Convert.ToInt64(drpCountry.SelectedItem.Value);
        TList<City> all = DataRepository.CityProvider.GetByCountryId(countryId);
        foreach (City city in all)
        {
            if (cityName == city.City)
            {
                Page.RegisterStartupScript("msg", "<script language='javascript'>alert('City already exists for the selected country.');</script>");
                return;
            }
        }
        City newCity = new City();
        newCity.CountryId = countryId;
        newCity.City = cityName;
        DataRepository.CityProvider.Insert(newCity);
        Page.RegisterStartupScript("msg", "<script language='javascript'>alert('City added successfully.');</script>");
    }
    protected void lbtReset_Click(object sender, EventArgs e)
    {
        drpCountry.SelectedIndex = 0;
        txtCityName.Text = "";
    }
}