﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using LMMR.Data;

public partial class Operator_AddCity : System.Web.UI.Page
{

    HotelManager objHotelManager = new HotelManager();
    ClientContract objClient = new ClientContract();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_AddCity));  

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divmessage.Visible = false;
            BindAllCountry();
            bindCityFirstTime();
        }
    }

    public void bindCityFirstTime()
    {
        drpCity.Items.Insert(0, new ListItem("Select City", "0"));
        drpCity.Enabled = false;
    }

    public void BindAllCountry()
    {
        TList<Country> objCountry = objClient.GetByAllCountry();
        drpCountry.DataValueField = "Id";
        drpCountry.DataTextField = "CountryName";
        drpCountry.DataSource = objCountry.OrderBy(a => a.CountryName);
        drpCountry.DataBind();
        drpCountry.Items.Insert(0, new ListItem("Select Country", ""));
    }


    protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpCountry.SelectedIndex > 0)
            {
                //Get Currency id on the basis of countryid
                Country cunt = objHotelManager.GetByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
                drpCity.DataValueField = "Id";
                drpCity.DataTextField = "City";
                drpCity.DataSource = objClient.GetCityByCountry(Convert.ToInt32(drpCountry.SelectedItem.Value)).OrderBy(a => a.City);
                drpCity.DataBind();
                drpCity.Items.Insert(0, new ListItem("Select City", "0"));
                drpCity.Enabled = true;
            }
            else
            {
                drpCity.Items.Clear();
                drpCity.Items.Insert(0, new ListItem("Select City", "0"));
                drpCity.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void lbtReset_Click(object sender, EventArgs e)
    {
        drpCountry.SelectedIndex = 0;
        drpCountry_SelectedIndexChanged(null, null);
        txtZoneName.Text = "";
    }

    protected void lbtSave_Click(object sender, EventArgs e)
    {
        long cityId=Convert.ToInt64(drpCity.SelectedItem.Value);
        string zoneName=txtZoneName.Text;
        TList<Zone> all = DataRepository.ZoneProvider.GetByCityId(cityId);
        foreach (Zone zone in all)
        {
            if (zoneName == zone.Zone)
            {
                Page.RegisterStartupScript("msg", "<script language='javascript'>alert('Zone already exists for the selected city.');</script>");
                return;
            }
        }
        Zone newZone=new Zone();
        newZone.Zone=zoneName;
        newZone.CityId=cityId;
        DataRepository.ZoneProvider.Insert(newZone);
        Page.RegisterStartupScript("msg", "<script language='javascript'>alert('Zone added successfully.');</script>");
    }
  
}