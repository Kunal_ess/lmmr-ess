﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text.RegularExpressions;
using log4net;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Xml;

public partial class Operator_CancelTransfer : System.Web.UI.Page
{
    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_CancelTransfer));  
    VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
    ViewBooking_Hotel objViewBooking = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    MeetingRoomDescManager objMeeting = new MeetingRoomDescManager();
    AvalibalityManager objAvailability = new AvalibalityManager();
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentOperatorID"] == null)
        {
            Response.Redirect("~/login.aspx", true);
        }

        SearchPanel1.SearchButtonClick += new EventHandler(Search_Button);
        SearchPanel1.CancelButtonClick += new EventHandler(Cancel_Button);
        SearchPanel1.rdbBookingClick += new EventHandler(Booking_SelectedIndex);
        if (!IsPostBack)
        {
            ViewState["SearchAlpha"] = "all";
            BindBookingDetails();
            
        }
        ApplyPaging();        
    }
    #endregion

    #region search
    protected void Cancel_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindBookingDetails();
        ViewState["CurrentPage"] = null;
        ApplyPaging();
    }
    protected void Search_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindBookingDetails();
        ViewState["CurrentPage"] = null;
        ApplyPaging();        
    }
    protected void Booking_SelectedIndex(object sender, EventArgs e)
    {
        string whereclaus = string.Empty;
        string orderby = "Id DESC";
        if (SearchPanel1.propBookRefType == 0)
        {
            lblType.Text = "Booking#";
            lblDate.Text = "Booking date";
            whereclaus += "booktype = 0 and DepartureDate >= GETDATE() -1";
            grvBookingDetails.Columns[10].Visible = true;
            tdTransfer.Visible = true;
        }
        else
        {
            lblType.Text = "Request#";
            lblDate.Text = "Request date";
            whereclaus += "requeststatus in (5,7,2) and booktype in ( 1,2) and DepartureDate >= GETDATE() -1";
            grvBookingDetails.Columns[10].Visible = false;
            tdTransfer.Visible = false;
        }
        Vlistreq = objViewBooking.Bindgrid(whereclaus, orderby);
        lblTotalBooking.Text = Convert.ToString(Vlistreq.Count);
        grvBookingDetails.DataSource = Vlistreq;
        grvBookingDetails.DataBind();        
        Session["WhereClause"] = Vlistreq;
        Session["WhereClauseTemp"] = Vlistreq;
        ViewState["SearchAlpha"] = "all";
        ApplyPaging();
        DivrequestDetail.Visible = false;
        divbookingdetails.Visible = false;
    }

    private void ApplyPaging()
    {
        try
        {            
            GridViewRow row = grvBookingDetails.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grvBookingDetails.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grvBookingDetails.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grvBookingDetails.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grvBookingDetails.PageIndex == grvBookingDetails.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void grvBookingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvBookingDetails.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            grvBookingDetails.DataSource = Session["WhereClauseTemp"];
            grvBookingDetails.DataBind();
            ApplyPaging();
            DivrequestDetail.Visible = false;
            divbookingdetails.Visible = false;
        }
        catch (Exception ex)
        {

        }
    }
    #region PageAlpha
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            HtmlAnchor anc = (sender as HtmlAnchor);
            anc.Style.Add("class", "select");
            var alpha = anc.InnerHtml;
            string get = alpha.Trim();
            ViewState["SearchAlpha"] = get;
            VList<Viewbookingrequest> objBooking = new VList<Viewbookingrequest>();
            objBooking = (VList<Viewbookingrequest>)Session["WhereClause"];
            ViewState["CurrentPage"] = null;
            if (get != "all")
            {
                Session["WhereClauseTemp"] = objBooking.FindAll(a => a.HotelName.ToLower().StartsWith(get.ToLower()));
                grvBookingDetails.DataSource = Session["WhereClauseTemp"];
                grvBookingDetails.DataBind();
                lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
                ApplyPaging();
            }
            else
            {
                BindBookingDetails();
                ApplyPaging();
            }
            DivrequestDetail.Visible = false;
            divbookingdetails.Visible = false;

        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    #endregion
    #endregion

    #region BindGrid for Booking
    /// <summary>
    /// Method to bind the grid
    /// </summary>    
    protected void BindBookingDetails()
    {
        try
        {
            string whereclaus=string.Empty;
            string orderby = "Id DESC";
            if (!string.IsNullOrEmpty(SearchPanel1.propBookRefNumber))
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.Id + "=" + SearchPanel1.propBookRefNumber + " ";
            }
            if (SearchPanel1.propHotelId != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.HotelId + "=" + SearchPanel1.propHotelId + " ";
            }
            if (SearchPanel1.propMeetingRoomId != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.MainMeetingRoomId + "=" + SearchPanel1.propMeetingRoomId + " ";
            }
            if (SearchPanel1.propCountryID != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.CountryId + "=" + SearchPanel1.propCountryID + " ";
            }
            if (SearchPanel1.propCityID != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.CityId + "=" + SearchPanel1.propCityID + " ";
            }

            if (SearchPanel1.propBookRefType == 0)
            {                
                lblType.Text="Booking#";
                lblDate.Text = "Booking date";
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "booktype = 0 and DepartureDate >= GETDATE() -1";                              
                grvBookingDetails.Columns[10].Visible = true;
                tdTransfer.Visible = true;
            }
            else
            {
                lblType.Text="Request#";
                lblDate.Text = "Request date";
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "requeststatus in (5,7,2) and booktype in ( 1,2) and DepartureDate >= GETDATE() -1";
                grvBookingDetails.Columns[10].Visible = false;
                tdTransfer.Visible = false;
            }
            Vlistreq = objViewBooking.Bindgrid(whereclaus, orderby);
            grvBookingDetails.DataSource = Vlistreq;//.OrderBy(a => a.Id);
            grvBookingDetails.DataBind();
            lblTotalBooking.Text = Convert.ToString(Vlistreq.Count);
            Session["WhereClause"] = Vlistreq;
            Session["WhereClauseTemp"] = Vlistreq;
            DivrequestDetail.Visible = false;
            divbookingdetails.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    //#region Reset for Cancel button
    //public void ResetCancel()
    //{
    //    BindBookingDetails();
    //}
    //#endregion

    #region Open popup for Cancel/Transfer
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;
            divCancel.Style.Add("display", "block");
            divTransfer.Style.Add("display", "none");
            if (btnchkcomm.Text.ToLower() == "change status")
            {                
                ModalPopupCheck.Show();
                if (SearchPanel1.propBookRefType == 1)
                {
                    drpStatus.Visible = false;
                    ddlstatus.Visible = true;
                    ViewState["popup"] = "1";
                }
                else
                {
                    ViewState["popup"] = "0";
                    drpStatus.Visible = true;
                    ddlstatus.Visible = false;
                }
            }            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    //Open popup for transfer booking
    protected void btntransfer_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btnTransfer = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnTransfer.CommandArgument;
            divTransfer.Style.Add("display", "block");
            divCancel.Style.Add("display", "none");
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            if (btnTransfer.Text.ToLower() == "transfer")
            {
                ModalPopupCheck.Show();
                if (SearchPanel1.propBookRefType == 0)
                {
                    drpInventory.DataSource = objMeeting.GetAllMeetingRoom(Convert.ToInt32(btnTransfer.CommandName)).OrderBy(a => a.Name);
                    drpInventory.DataTextField = "Name";
                    drpInventory.DataValueField = "Id";
                    drpInventory.DataBind();                    
                    drpInventory.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Inventory--", ""));
                    txtFromdate.Text = "dd/mm/yy";
                    CalendarExtender1.StartDate = DateTime.Now.Date;
                    Booking objbooking = objViewBooking.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
                    txtFromdate.Text = String.Format("{0:dd/MM/yyyy}", objbooking.ArrivalDate);                    
                    drpInventory.SelectedIndex = drpInventory.Items.IndexOf(drpInventory.Items.FindByValue(objbooking.MainMeetingRoomId.ToString()));
                }               
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #endregion

    #region Gridview Events
    protected void grvBookingDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                Label lblCity = (Label)e.Row.FindControl("lblCity");
                HiddenField hdnCityId = (HiddenField)e.Row.FindControl("hdnCityId");
                City objCity = objHotelManager.GetByCityID(Convert.ToInt32(hdnCityId.Value));
                lblCity.Text = objCity.City;



                Label lblArrivalDt = (Label)e.Row.FindControl("lblArrivalDt");
                LinkButton lblRefNo = (LinkButton)e.Row.FindControl("lblRefNo");
                Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)e.Row.FindControl("lblExpiryDt");
                LinkButton btnchkcommision = (LinkButton)e.Row.FindControl("btnchkcommision");
                Label lblBookingDt = (Label)e.Row.FindControl("lblBookingDt");
                Label lblstatus = (Label)e.Row.FindControl("lblstatus");
                DateTime startTime = DateTime.Now;
                string depdt = lblDepartureDt.Text;
                LinkButton btnchkcommision1 = (LinkButton)e.Row.FindControl("btnchkcommision1");
                LinkButton btntransfer = (LinkButton)e.Row.FindControl("btntransfer");

                #region Bindrequeststaus
                lblstatus.Text = Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(lblstatus.ToolTip));
                #endregion

                DateTime endTime = new DateTime(Convert.ToInt32(depdt.Split('/')[2]), Convert.ToInt32(depdt.Split('/')[1]), Convert.ToInt32(depdt.Split('/')[0]));
                TimeSpan span = endTime.Subtract(startTime);
                int spn = span.Days;

                if (SearchPanel1.propBookRefType == 1)
                {
                    #region show change Status button for request
                    if (spn < 0)
                    {
                        if (lblstatus.Text.ToString() == BookingRequestStatus.Cancel.ToString())
                        {
                            btnchkcommision1.Visible = false;
                        }
                        if (lblstatus.Text.ToString() == BookingRequestStatus.New.ToString())
                        {
                            btnchkcommision1.Visible = false;
                        }
                        if (lblstatus.Text.ToString() == BookingRequestStatus.Definite.ToString())
                        {
                            btnchkcommision1.Visible = false;
                        }
                        if (lblstatus.Text.ToString() == BookingRequestStatus.Tentative.ToString())
                        {
                            btnchkcommision1.Visible = true;
                        }
                    }
                    else
                    {
                        if (lblstatus.Text.ToString() == BookingRequestStatus.Cancel.ToString())
                        {
                            btnchkcommision1.Visible = true;
                        }
                        if (lblstatus.Text.ToString() == BookingRequestStatus.New.ToString())
                        {
                            btnchkcommision1.Visible = true;
                        }
                        if (lblstatus.Text.ToString() == BookingRequestStatus.Definite.ToString())
                        {
                            btnchkcommision1.Visible = true;
                        }
                        if (lblstatus.Text.ToString() == BookingRequestStatus.Tentative.ToString())
                        {
                            btnchkcommision1.Visible = true;
                        }
                    }
                }
                else
                {
                    if ((lblstatus.Text.ToString() == BookingRequestStatus.Processed.ToString()) || (lblstatus.Text.ToString() == BookingRequestStatus.Cancel.ToString()))
                    {
                        btnchkcommision1.Visible = false;
                        btntransfer.Visible = false;
                    }
                    else
                    {
                        btnchkcommision1.Visible = true;
                        btntransfer.Visible = true;
                    }
                }
                    #endregion


                //if (Convert.ToDateTime(endTime.ToString("dd/MM/yyyy")) > Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy")))
                //{
                //    btnchkcommision1.Visible = false;
                //}

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void grvBookingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (Convert.ToString(e.CommandName) == "ViewBookingDetail")
        {
            if (SearchPanel1.propBookRefType == 0)
            {
                divbookingdetails.Visible = true;
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + divbookingdetails.ClientID + "');});", true);
                DivrequestDetail.Visible = false;
                ucViewBookingDetail.BindBooking(Convert.ToInt32(e.CommandArgument));
            }
            else
            {
                ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
                  Booking obj=        objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(e.CommandArgument));
                  if (obj.BookType == 1)
                  {
                      requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
                      DivrequestDetail.Visible = true;
                      divbookingdetails.Visible = false;
                      BookingDetails1.Visible = false;
                      requestDetails.Visible = true;
                      ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                  }
                  else if (obj.BookType == 2)
                  {
                      BookingDetails1.Visible = true;
                      BookingDetails1.BindBooking(Convert.ToInt32(e.CommandArgument));
                      requestDetails.Visible = false;
                      DivrequestDetail.Visible = true;
                      divbookingdetails.Visible = false;
                      ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                  }
                
            }
        }
    }
    #endregion

    #region Change Status for booking/request
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (Convert.ToString(ViewState["popup"]) == "1")
            {
                objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);
                if (objViewBooking.updatecheckComm(objbooking, Convert.ToInt64(Session["CurrentOperatorID"])))
                {
                    ModalPopupCheck.Hide();
                    BindBookingDetails();
                }
            }
            else if (Convert.ToString(ViewState["popup"]) == "0")
            {
                objViewBooking.CancelBooking(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()), Convert.ToInt64(Session["CurrentOperatorID"]));
                ModalPopupCheck.Hide();
                BindBookingDetails();             
            }
           

        }
        catch (Exception ex)
        {
            logger.Error(ex);           
        }

    }
   
    protected void lnkTransfer_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime fromDate = new DateTime(Convert.ToInt32(txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
            Booking objbooking = objViewBooking.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));

            string[] SelectedDay = new string[4];
            string[] MRNo = new string[2];
            string[] Selectedtime = new string[4];
            int day = 0;
            int ct = 0;
            int a = 0;

            #region Xml
            string xml = objbooking.BookingXml;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNode node;
            node = doc.DocumentElement;
            foreach (XmlNode node1 in node.ChildNodes)
            {
                foreach (XmlNode node2 in node1.ChildNodes) //BookedMR
                {
                    foreach (XmlNode node3 in node2.ChildNodes) //MRid,MrDetails
                    {
                        if (node3.Name == "MRId")
                        {
                            MRNo[ct] = node3.InnerText;
                            ct++;
                            node3.InnerText = Convert.ToString(drpInventory.SelectedValue);
                        }
                        if (node3.Name == "MrDetails")
                        {
                            #region to get selected date

                            foreach (XmlNode node4 in node3.ChildNodes) //BookedMRconfig
                            {
                                foreach (XmlNode node5 in node4.ChildNodes)
                                {
                                    if (node5.Name == "SelectedDay")
                                    {
                                        SelectedDay[a] = node5.InnerText;
                                    }
                                    if (node5.Name == "SelectedTime")
                                    {
                                        Selectedtime[a] = node5.InnerText;
                                    }
                                }
                                a++;
                            }
                            #endregion
                        }
                    }
                }

                #region modify arrval and depdate

                if (node1.Name == "Duration")
                {
                    day = Convert.ToInt32(node1.InnerText);
                }
                if (node1.Name == "ArivalDate")
                {
                    node1.InnerText = fromDate.ToString("yyyy-MM-dd");

                }
                if (node1.Name == "DepartureDate")
                {
                    node1.InnerText = fromDate.AddDays(day).ToString("yyyy-MM-dd");

                }
            }
                #endregion

            string BOOKINxml = "";
            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                doc.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                BOOKINxml = stringWriter.GetStringBuilder().ToString();
            }

            #endregion

            #region Insert new booking
            Booking newBoking = new Booking();
            newBoking.ArrivalDate = fromDate;
            if (day == 1)
            {
                newBoking.DepartureDate = fromDate;
            }
            else
            {
                newBoking.DepartureDate = fromDate.AddDays(1);
            }
            newBoking.CreatorId = objbooking.CreatorId;
            newBoking.Duration = objbooking.Duration;//Convert.ToInt64( SelectedDay);
            newBoking.HotelId = objbooking.HotelId;
            newBoking.MainMeetingRoomId = Convert.ToInt32(drpInventory.SelectedValue);
            newBoking.BookingDate = objbooking.BookingDate;
            newBoking.SpecialRequest = objbooking.SpecialRequest;
            newBoking.IsBedroom = objbooking.IsBedroom;
            newBoking.BookType = 0;
            newBoking.IsUserBookingProcessDone = true;
            newBoking.BedRoomTotalPrice = objbooking.BedRoomTotalPrice;
            newBoking.AgencyClientId = 1;
            newBoking.AgencyUserId = 1;
            newBoking.BookingXml = BOOKINxml; //objbooking.BookingXml
            newBoking.RequestStatus = (int)BookingRequestStatus.Processed;
            newBoking.FinalTotalPrice = objbooking.FinalTotalPrice;
            newBoking.RevenueAmount = objbooking.RevenueAmount;
            objViewBooking.InsertBookingAfterTransfer(newBoking);
            #endregion

            #region Get days
            string propDay = "";
            string propDay2 = "";
            string propDay3 = "";
            string propDay4 = "";
            if (Convert.ToInt32(Selectedtime[0]) == 0)
            {
                propDay = "Fullday";
            }
            else if (Convert.ToInt32(Selectedtime[0]) == 1)
            {
                propDay = "Morning";
            }
            else
            {
                propDay = "Afternoon";
            }
            if (!string.IsNullOrEmpty(Selectedtime[1]))
            {
                if (Convert.ToInt32(Selectedtime[1]) == 0)
                {
                    propDay2 = "Fullday";
                }
                else if (Convert.ToInt32(Selectedtime[1]) == 1)
                {
                    propDay2 = "Morning";
                }
                else
                {
                    propDay2 = "Afternoon";
                }
            }
            if (day == 2)
            {
                if (!string.IsNullOrEmpty(Selectedtime[2]))
                {
                    if (Convert.ToInt32(Selectedtime[2]) == 0)
                    {
                        propDay3 = "Fullday";
                    }
                    else if (Convert.ToInt32(Selectedtime[2]) == 1)
                    {
                        propDay3 = "Morning";
                    }
                    else
                    {
                        propDay3 = "Afternoon";
                    }
                }
                if (!string.IsNullOrEmpty(Selectedtime[3]))
                {
                    if (Convert.ToInt32(Selectedtime[3]) == 0)
                    {
                        propDay4 = "Fullday";
                    }
                    else if (Convert.ToInt32(Selectedtime[3]) == 1)
                    {
                        propDay4 = "Morning";
                    }
                    else
                    {
                        propDay4 = "Afternoon";
                    }
                }
            }            
            #endregion
            
            #region book new date
            VList<ViewAvailabilityOfRooms> vlist;
            vlist = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(newBoking.HotelId), newBoking.ArrivalDate.ToString(), newBoking.MainMeetingRoomId);
            if (vlist.Count > 0)
            {
                string result = "";
                result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlist[0].Id), Convert.ToDateTime(newBoking.ArrivalDate), propDay, "booked");
            }
            if (day == 2)
            {
                VList<ViewAvailabilityOfRooms> vlist1;
                vlist1 = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(newBoking.HotelId), newBoking.ArrivalDate.ToString(), newBoking.MainMeetingRoomId);
                if (vlist1.Count > 0)
                {
                    string result = "";
                    result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlist1[0].Id), Convert.ToDateTime(newBoking.ArrivalDate), propDay, "booked");
                }

                VList<ViewAvailabilityOfRooms> vlist2;
                vlist2 = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(newBoking.HotelId), newBoking.DepartureDate.ToString(), newBoking.MainMeetingRoomId);
                if (vlist2.Count > 0)
                {
                    string result = "";
                    result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlist2[0].Id), Convert.ToDateTime(newBoking.DepartureDate), propDay2, "booked");
                }
            }

            objbooking.RequestStatus = Convert.ToInt32(BookingRequestStatus.Cancel);
            if (objViewBooking.UpdateTransferBooking(objbooking))
            {

            }
            #endregion

            #region Open/Close status for previous booking id for 1 Day
            if (day == 1)
            {
                VList<ViewAvailabilityOfRooms> vlistOpen;
                vlistOpen = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(objbooking.HotelId), objbooking.ArrivalDate.ToString(), Convert.ToInt32(MRNo[0].ToString()));
                if (vlistOpen.Count > 0)
                {
                    if (rdbStatus.SelectedValue == "0")
                    {
                        string result = "";
                        result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistOpen[0].Id), Convert.ToDateTime(objbooking.ArrivalDate), propDay, "opened");
                        objAvailability.UpdateForFullDay((propDay == "morning" ? 1 : (propDay == "afternoon" ? 2 : 0)), objbooking.ArrivalDate.Value, objbooking.DepartureDate.Value, objbooking.HotelId, Convert.ToInt32(MRNo[0].ToString()));
                    }
                    else
                    {
                        string result = "";
                        result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistOpen[0].Id), Convert.ToDateTime(objbooking.ArrivalDate), propDay, "closed");
                        //objAvailability.UpdateForFullDay((propDay == "morning" ? 1 : (propDay == "afternoon" ? 2 : 0)), objbooking.ArrivalDate.Value, objbooking.DepartureDate.Value, objbooking.HotelId, Convert.ToInt32(MRNo[0].ToString()));
                    }
                }

                if (!string.IsNullOrEmpty(MRNo[1]))
                {
                    VList<ViewAvailabilityOfRooms> vlistOpen1;
                    vlistOpen1 = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(objbooking.HotelId), objbooking.ArrivalDate.ToString(), Convert.ToInt32(MRNo[1].ToString()));
                    if (vlistOpen1.Count > 0)
                    {
                        if (rdbStatus.SelectedValue == "0")
                        {
                            string result = "";
                            result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistOpen1[0].Id), Convert.ToDateTime(objbooking.ArrivalDate), propDay2, "opened");
                            objAvailability.UpdateForFullDay((propDay2 == "morning" ? 1 : (propDay2 == "afternoon" ? 2 : 0)), objbooking.ArrivalDate.Value, objbooking.DepartureDate.Value, objbooking.HotelId, Convert.ToInt32(MRNo[1].ToString()));
                        }
                        else
                        {
                            string result = "";
                            result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistOpen1[0].Id), Convert.ToDateTime(objbooking.ArrivalDate), propDay2, "closed");
                            //objAvailability.UpdateForFullDay((propDay2 == "morning" ? 1 : (propDay2 == "afternoon" ? 2 : 0)), objbooking.ArrivalDate.Value, objbooking.DepartureDate.Value, objbooking.HotelId, Convert.ToInt32(MRNo[1].ToString()));
                        }
                    }
                }
            }
            #endregion

            #region Open/Close status for previous booking id for 2 Day
            if (day == 2)
            {
                if (!string.IsNullOrEmpty(MRNo[0]))
                {
                    VList<ViewAvailabilityOfRooms> vlistDay1;
                    vlistDay1 = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(objbooking.HotelId), objbooking.ArrivalDate.ToString(), Convert.ToInt32(MRNo[0].ToString()));
                    VList<ViewAvailabilityOfRooms> vlistDay2;
                    vlistDay2 = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(objbooking.HotelId), objbooking.DepartureDate.ToString(), Convert.ToInt32(MRNo[0].ToString()));
                    if (vlistDay1.Count > 0 && vlistDay2.Count > 0)
                    {
                        if (rdbStatus.SelectedValue == "0")
                        {
                            string result = "";
                            result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistDay1[0].Id), Convert.ToDateTime(objbooking.ArrivalDate), propDay, "opened");
                            //objAvailability.UpdateForFullDay((propDay == "morning" ? 1 : (propDay == "afternoon" ? 2 : 0)), objbooking.ArrivalDate.Value, objbooking.DepartureDate.Value, objbooking.HotelId, Convert.ToInt32(MRNo[0].ToString()));

                            string result1 = "";
                            result1 = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistDay2[0].Id), Convert.ToDateTime(objbooking.DepartureDate), propDay2, "opened");
                            //objAvailability.UpdateForFullDay((propDay2 == "morning" ? 1 : (propDay2 == "afternoon" ? 2 : 0)), objbooking.ArrivalDate.Value, objbooking.DepartureDate.Value, objbooking.HotelId, Convert.ToInt32(MRNo[0].ToString()));
                        }
                        else
                        {
                            string result = "";
                            result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistDay1[0].Id), Convert.ToDateTime(objbooking.ArrivalDate), propDay, "closed");
                            string result1 = "";
                            result1 = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistDay2[0].Id), Convert.ToDateTime(objbooking.DepartureDate), propDay2, "closed");
                        }
                    }
                }


                if (!string.IsNullOrEmpty(MRNo[1]))
                {
                    VList<ViewAvailabilityOfRooms> vlistDay1;
                    vlistDay1 = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(objbooking.HotelId), objbooking.ArrivalDate.ToString(), Convert.ToInt32(MRNo[1].ToString()));
                    VList<ViewAvailabilityOfRooms> vlistDay2;
                    vlistDay2 = objAvailability.GetMeetingRoomByHotelIDForTransfer(Convert.ToInt32(objbooking.HotelId), objbooking.DepartureDate.ToString(), Convert.ToInt32(MRNo[1].ToString()));
                    if (vlistDay1.Count > 0 && vlistDay2.Count > 0)
                    {
                        if (rdbStatus.SelectedValue == "0")
                        {
                            string result = "";
                            result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistDay1[0].Id), Convert.ToDateTime(objbooking.ArrivalDate), propDay3, "opened");
                            objAvailability.UpdateForFullDay((propDay3 == "morning" ? 1 : (propDay3 == "afternoon" ? 2 : 0)), objbooking.ArrivalDate.Value, objbooking.DepartureDate.Value, objbooking.HotelId, Convert.ToInt32(MRNo[1].ToString()));

                            string result1 = "";
                            result1 = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistDay2[0].Id), Convert.ToDateTime(objbooking.DepartureDate), propDay4, "opened");
                            objAvailability.UpdateForFullDay((propDay4 == "morning" ? 1 : (propDay4 == "afternoon" ? 2 : 0)), objbooking.ArrivalDate.Value, objbooking.DepartureDate.Value, objbooking.HotelId, Convert.ToInt32(MRNo[1].ToString()));
                        }
                        else
                        {
                            string result = "";
                            result = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistDay1[0].Id), Convert.ToDateTime(objbooking.ArrivalDate), propDay3, "closed");
                            string result1 = "";
                            result1 = objAvailability.UpdateCurrentAvailability(Convert.ToInt64(vlistDay2[0].Id), Convert.ToDateTime(objbooking.DepartureDate), propDay4, "closed");
                        }
                    }
                }
            }
            #endregion

            ModalPopupCheck.Hide();
            BindBookingDetails();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion

    #region Cancel Button Click
    /// <summary>
    /// Modal popup cancel event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {            
            ModalPopupCheck.Hide();
            ddlstatus.SelectedIndex = 0;           
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #endregion
    

    #region PDF
    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }




    }

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {
        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    protected void lnkSaveRequestPdf_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(DivdetailsRequest);
            DivdetailsRequest.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
}