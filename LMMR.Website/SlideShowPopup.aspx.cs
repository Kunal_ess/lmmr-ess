﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Configuration;
public partial class SlideShowPopup : System.Web.UI.Page
{
    PictureVideoManagementManager pvm = new PictureVideoManagementManager();
    HotelManager hm = new HotelManager();
    public string imagesCollection
    {
        get;
        set;
    }
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            Hotel h = hm.GetHotelDetailsById(Convert.ToInt64(Request.QueryString["id"]));
            //imgHotellogo.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "HotelImage/" + h.Logo;
            //lblHotelName.Text = h.Name;
            var pp = pvm.GetAllHotelPictureVideo(Convert.ToInt32(Request.QueryString["id"])).ToList().Where(a=>a.FileType==0);
            imagesCollection = "";
            foreach (HotelPhotoVideoGallary hp in pp)
            {
                //string imagepath = LMMREnvironment.SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Split('~')[1].Remove(0,1) + "HotelImage/" + hp.ImageName;
                string imagepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Remove(0, 2) + "HotelImage/" + hp.ImageName;// vaibhav
                imagesCollection += "{image: '" + imagepath + "', title: '" + (hp.AlterText == null ? "" : hp.AlterText) + "', thumb: '" + imagepath + "', url: '' },";
            }
            if (imagesCollection.Length > 0)
            {
                imagesCollection = imagesCollection.Substring(0, imagesCollection.Length - 1);
            }
            InsertStatistics(Convert.ToInt32(Request.QueryString["id"]));
        }
        else if (Request.QueryString["Mid"] != null)
        {
            TList<MeetingRoomPictureVideo> p = pvm.GetPictureVideoByMeetingRoomID(Convert.ToInt32(Request.QueryString["Mid"]));
            imagesCollection = "";
            foreach (MeetingRoomPictureVideo hp in p)
            {                
                string imagepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Remove(0, 2) + "MeetingRoomImage/" + hp.ImageName;
                imagesCollection += "{image: '" + imagepath + "', title: '" + (hp.AlterText == null ? "" : hp.AlterText) + "', thumb: '" + imagepath + "', url: '' },";
            }
            if (imagesCollection.Length > 0)
            {
                imagesCollection = imagesCollection.Substring(0, imagesCollection.Length - 1);
            }
        }
        else if (Request.QueryString["Bid"] != null)
        {
            TList<BedRoomPictureImage> p = pvm.GetPictureVideoByBedRoomID(Convert.ToInt32(Request.QueryString["Bid"]));
            imagesCollection = "";
            foreach (BedRoomPictureImage hp in p)
            {
                string imagepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Remove(0, 2) + "BedRoomImage/" + hp.ImageName;
                imagesCollection += "{image: '" + imagepath + "', title: '" + (hp.AlterText == null ? "" : hp.AlterText) + "', thumb: '" + imagepath + "', url: '' },";
            }
            if (imagesCollection.Length > 0)
            {
                imagesCollection = imagesCollection.Substring(0, imagesCollection.Length - 1);
            }
        }
        
    }

    #region statistics
    public void InsertStatistics(int HotelId)
    {
        Viewstatistics objview = new Viewstatistics();
        Statistics ST = new Statistics();
        ST.HotelId = HotelId;
        ST.Views = 1;
        ST.StatDate = System.DateTime.Now;
        objview.InsertVisitwithBookingid(ST);
    }
    #endregion
}