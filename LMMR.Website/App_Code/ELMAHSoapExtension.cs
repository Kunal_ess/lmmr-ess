﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

//namespace LMMR
//{
    /// <summary>
    /// Elmah extension
    /// </summary>
    class ELMAHExtension : SoapExtension
    {
        
        public override object GetInitializer(Type serviceType)
        { return null; }

        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        { return null; }

        public override void Initialize(object initializer)
        { }
        /// <summary>
        /// process the message
        /// </summary>
        /// <param name="message"></param>
        public override void ProcessMessage(SoapMessage message)
        {
            if (message.Stage == SoapMessageStage.AfterSerialize &&
                message.Exception != null)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(message.Exception, HttpContext.Current));
            }
        }
    }

//}