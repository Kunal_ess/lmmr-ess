﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.Xml;
/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : System.Web.UI.Page
{
    public ILog logger
    {
        get { return log4net.LogManager.GetLogger(typeof(BasePage)); }
    }
    public XmlDocument xmlLanguage
    {
        get
        {
            if (Session["xmlLanguage"] != null)
            {
                return (Session["xmlLanguage"] as XmlDocument);
            }
            else
            {
                return null;
            }
        }
        set
        {
            Session["xmlLanguage"] = value;
        }
    }
    //Get Site Root path
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    public Language l
    {
        get;
        set;
    }
	public BasePage()
	{
		//
		// TODO: Add constructor logic here
		//
        base.Init += new EventHandler(BasePage_Init);
	}

    void BasePage_Init(object sender, EventArgs e)
    {
        if (Session["LanguageID"] == null)
        {
            Language newl = new LanguageManager().GetActiveLanguage().Where(a => a.Name.ToLower() == Request.RawUrl.ToLower().Split('/')[Request.RawUrl.ToLower().Split('/').Length - 1]).FirstOrDefault();
            if (newl != null)
            {
                Session["LanguageID"] = newl.Id;
                Session["Language"] = newl;
                Session["LanguageChange"] = true;
            }
            else
            {
                Session["LanguageID"] = 1;
                Session["Language"] = new LanguageManager().GetLanguageByID(1);
                Session["LanguageChange"] = true;
            }
        }
        else
        {
            Language newl = new LanguageManager().GetActiveLanguage().Where(a => a.Name.ToLower() == Request.RawUrl.ToLower().Split('/')[Request.RawUrl.ToLower().Split('/').Length - 1]).FirstOrDefault();
            Language Myl = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (newl != null)
            {
                if (newl.Name.ToLower() != Myl.Name.ToLower())
                {
                    Session["LanguageID"] = newl.Id;
                    Session["Language"] = newl;
                    Session["LanguageChange"] = true;
                }
                else
                {
                    Session["LanguageID"] = Myl.Id;
                    Session["Language"] = Myl;
                }
            }
        }
        l = Session["Language"] as Language;
        if (Request.RawUrl.ToLower().Contains("search-results"))
        {
            if (Session["Date"] == null)
            {
                Session["Date"] = DateTime.Now.ToString("dd/MM/yy");
            }
            if (Session["NumberOfParticipant"] == null)
            {
                Session["NumberOfParticipant"] = "20";
            }
            string[] urlArray = Request.RawUrl.ToLower().Substring(Request.RawUrl.ToLower().LastIndexOf("search-results/") + "search-results/".Length).Split('/');
            int countryID = 0;
            if (urlArray.Length > 0)
            {
                try
                {
                    countryID = Convert.ToInt32(objManageCMSContent.GetCountryIdByName(urlArray[0]).Id);
                }
                catch
                {
                    countryID = 0;
                }
            }
            Session["Mycountry"] = countryID == 0 ? null : countryID.ToString();
            int cityID = 0;
            if (urlArray.Length > 1)
            {
                try
                {
                    cityID = Convert.ToInt32(objManageCMSContent.GetCityIdByName(urlArray[1]).Id);
                }
                catch
                {
                    cityID = 0;
                }
            }
            Session["Mycity"] = cityID == 0 ? null : cityID.ToString();
            int zoneID = 0;
            if (urlArray.Length > 2)
            {
                try
                {
                    zoneID = Convert.ToInt32(objManageCMSContent.GetZoneIdByName(Convert.ToInt64(Session["Mycity"]), urlArray[2]).Id);
                }
                catch
                {
                    zoneID = 0;
                }
            }
            Session["Myzone"] = zoneID == 0 ? null : zoneID.ToString();
        }
    }

    public string GetKeyResult(string key)
    {
        if (Session["LanguageChange"] != null)
        {
            xmlLanguage = null;
            Session["LanguageChange"] = null;
            Session["CMS"] = null;
            Session["CMSDESCRIPTION"] = null;
        }
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
            }
        }
        else 
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    public string GetKeyResultForJavaScript(string key)
    {
        if (Session["LanguageChange"] != null)
        {
            xmlLanguage = null;
            Session["LanguageChange"] = null;
            Session["CMS"] = null;
            Session["CMSDESCRIPTION"] = null;
        }
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key)).Replace("'", "\\'").Replace("\"", "\\\"");
    }
}