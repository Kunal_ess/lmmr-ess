﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.Xml;
/// <summary>
/// Summary description for BaseUserControl
/// </summary>
public class BaseUserControl : System.Web.UI.UserControl
{
    public ILog logger
    {
        get { return log4net.LogManager.GetLogger(typeof(BaseUserControl)); }
    }
    public SuperAdminTaskManager manager = new SuperAdminTaskManager();
    public XmlDocument xmlLanguage
    {
        get
        {
            if (Session["xmlLanguage"] != null)
            {
                return (Session["xmlLanguage"] as XmlDocument);
            }
            else
            {
                return null;
            }
        }
        set
        {
            Session["xmlLanguage"] = value;
        }
    }

    public TList<Cms> PropCMS
    {
        get 
        {
            if (Session["CMS"] != null)
            {
                return (Session["CMS"] as TList<Cms>);
            }
            else
            {
                TList<Cms> lstCms = manager.getAllCmsDetails();
                Session["CMS"] = lstCms;
                return (Session["CMS"] as TList<Cms>);
            }
        }
    }
    public TList<CmsDescription> PropCMSDESCRIPTION
    {
        get
        {
            if (Session["CMSDESCRIPTION"] != null)
            {
                return (Session["CMSDESCRIPTION"] as TList<CmsDescription>);
            }
            else
            {
                TList<CmsDescription> lstCms = manager.getAllCmsDescDetails();
                Session["CMSDESCRIPTION"] = lstCms;
                return (Session["CMSDESCRIPTION"] as TList<CmsDescription>);
            }
        }
    }
    public TList<Language> PropLangauge
    {
        get
        {
            if (Session["LANGAUGESACTIVE"] != null)
            {
                return (Session["LANGAUGESACTIVE"] as TList<Language>);
            }
            else
            {
                TList<Language> lstCms = new LanguageManager().GetActiveLanguage();
                Session["LANGAUGESACTIVE"] = lstCms;
                return (Session["LANGAUGESACTIVE"] as TList<Language>);
            }
        }
    }
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    public Language l
    {
        get;
        set;
    }
	public BaseUserControl()
	{
		//
		// TODO: Add constructor logic here
		//
        base.Init += new EventHandler(BaseUserControl_Init);
	}

    void BaseUserControl_Init(object sender, EventArgs e)
    {
        if (Session["LanguageID"] == null)
        {
            Language newl = new Language();
            newl = PropLangauge.Where(a => a.Name.ToLower() == Request.RawUrl.ToLower().Split('/')[Request.RawUrl.ToLower().Split('/').Length - 1]).FirstOrDefault();
            if(newl==null)
            {
                newl = new LanguageManager().GetActiveLanguage().Where(a => a.Name.ToLower() == Request.RawUrl.ToLower().Split('/')[Request.RawUrl.ToLower().Split('/').Length - 1]).FirstOrDefault();
                Session["LANGAUGESACTIVE"] = new LanguageManager().GetActiveLanguage();
            }
            if (newl != null)
            {
                Session["LanguageID"] = newl.Id;
                Session["Language"] = newl;
                Session["LanguageChange"] = true;
            }
            else
            {
                Session["LanguageID"] = 1;
                Session["Language"] = new LanguageManager().GetLanguageByID(1);
                Session["LanguageChange"] = true;
            }
        }
        else
        {
            Language newl = new Language();
            newl = PropLangauge.Where(a => a.Name.ToLower() == Request.RawUrl.ToLower().Split('/')[Request.RawUrl.ToLower().Split('/').Length - 1]).FirstOrDefault();
            if (newl == null)
            {
                newl = new LanguageManager().GetActiveLanguage().Where(a => a.Name.ToLower() == Request.RawUrl.ToLower().Split('/')[Request.RawUrl.ToLower().Split('/').Length - 1]).FirstOrDefault();
                Session["LANGAUGESACTIVE"] = new LanguageManager().GetActiveLanguage();
            }
            Language Myl = new Language();
            if (PropLangauge == null)
            {
                Myl = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            }
            else
            {
                Myl = PropLangauge.Where(a => a.Id == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
            }
            if (newl != null)
            {
                if (newl.Name.ToLower() != Myl.Name.ToLower())
                {
                    Session["LanguageID"] = newl.Id;
                    Session["Language"] = newl;
                    Session["LanguageChange"] = true;
                }
                else
                {
                    Session["LanguageID"] = Myl.Id;
                    Session["Language"] = Myl;
                }
            }
        }
        l = Session["Language"] as Language;
    }

    public string GetKeyResult(string key)
    {
        if (Session["LanguageChange"] != null)
        {
            xmlLanguage = null;
            Session["LanguageChange"] = null;
            Session["CMS"] = null;
            Session["CMSDESCRIPTION"] = null;
        }
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    public string GetKeyResultForJavaScript(string key)
    {
        if (Session["LanguageChange"] != null)
        {
            xmlLanguage = null;
            Session["LanguageChange"] = null;
            Session["CMS"] = null;
            Session["CMSDESCRIPTION"] = null;
        }
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key)).Replace("'", "\\'").Replace("\"", "\\\"");
    }
}