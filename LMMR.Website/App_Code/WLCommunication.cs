﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LMMR.Business;
using LMMR.Entities;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for WLCommunication
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WLCommunication : System.Web.Services.WebService
{
    [WebMethod]
    public string Test()
    {
        return "Hello World";
    }


    [WebMethod]
    public string GetCountryData(long WLChannelID)
    {

        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        ManageWhiteLabel objWLManage = new ManageWhiteLabel();
        WhiteLabel ObjWL = objWLManage.GetWhiteLabelByID(WLChannelID);
        TList<Hotel> objHotel = objWLManage.GetHotelInfo();
        TList<Country> objCountry = objWLManage.GetCountry();
        List<Country> FinalListOfCountry = new List<Country>();
        if (ObjWL != null)
        {
            if (Convert.ToBoolean(ObjWL.IsActive))
            {
                TList<WhiteLabelMappingWithHotel> objMapHotel = objWLManage.GetHotelMappingByWhiteLabelId(WLChannelID);
                if (objMapHotel.Count > 0)
                {
                    int intIndex = 0;
                    for (intIndex = 0; intIndex <= objMapHotel.Count - 1; intIndex++)
                    {
                        var Hotelrow = objHotel.Find(a => a.Id == objMapHotel[intIndex].HotelId);
                        if (Hotelrow != null)
                        {
                            Country Countryrow = objCountry.Find(a => a.Id == Hotelrow.CountryId);
                            if (Countryrow != null)
                            {
                                if (!FinalListOfCountry.Contains(Countryrow))
                                {
                                    FinalListOfCountry.Add(Countryrow);
                                }
                            }
                        }
                    }
                }
            }
        }

        //return FinalListOfCountry.OrderBy(a=>a.CountryName).ToList();
        FinalListOfCountry = FinalListOfCountry.OrderBy(a => a.CountryName).ToList();
        if (FinalListOfCountry.Count > 0)
        {
            var data = FinalListOfCountry.AsQueryable().Select(u => new
            {
                u.Id,
                u.CountryName
            });

            return serializer.Serialize(data);


        }
        else
        {
            return null;
        }

    }

    [WebMethod]
    public string GetCityData(long WLChannelID, long WLCountryID)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        ManageWhiteLabel objWLManage = new ManageWhiteLabel();
        WhiteLabel ObjWL = objWLManage.GetWhiteLabelByID(WLChannelID);
        TList<Hotel> objHotel = objWLManage.GetHotelInfo().FindAll(a => a.CountryId == WLCountryID);
        TList<City> objCity = objWLManage.GetCity();
        List<City> FinalListOfCity = new List<City>();
        if (ObjWL != null)
        {
            if (Convert.ToBoolean(ObjWL.IsActive))
            {
                TList<WhiteLabelMappingWithHotel> objMapHotel = objWLManage.GetHotelMappingByWhiteLabelId(WLChannelID);
                if (objMapHotel.Count > 0)
                {
                    int intIndex = 0;
                    for (intIndex = 0; intIndex <= objMapHotel.Count - 1; intIndex++)
                    {
                        var Hotelrow = objHotel.Find(a => a.Id == objMapHotel[intIndex].HotelId);
                        if (Hotelrow != null)
                        {
                            City Countryrow = objCity.Find(a => a.Id == Hotelrow.CityId);
                            if (Countryrow != null)
                            {
                                if (!FinalListOfCity.Contains(Countryrow))
                                {
                                    FinalListOfCity.Add(Countryrow);
                                }
                            }
                        }
                    }
                }
            }
        }

        //return FinalListOfCity.OrderBy(a=>a.City).ToList();
        FinalListOfCity = FinalListOfCity.OrderBy(a => a.City).ToList();
        if (FinalListOfCity.Count > 0)
        {
            var data = FinalListOfCity.AsQueryable().Select(u => new
            {
                u.Id,
                u.City
            });

            return serializer.Serialize(data);
        }
        else
        {
            return null;
        }

    }
    [WebMethod]
    public string VerifiedClient(long WLChannelID, string clientUrl)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        ManageWhiteLabel objWLManage = new ManageWhiteLabel();
        string result = string.Empty;
        WhiteLabel objwl = objWLManage.GetWhiteLabelByID(WLChannelID);
        if (objwl != null)
        {
          if (clientUrl.Contains(objwl.TargetUrl.Replace("http://www.", "").Replace("https://www.", "").Replace("http:", "").Replace("http:/", "").Replace("http://", "").Replace("https:", "").Replace("https:/", "").Replace("https://", "")))
            {
                if (Convert.ToBoolean(objwl.IsActive))
                {
                    result = "verified";
                }
                else
                {
                    result = "deactivate";
                }
            }
            else
            {
                result = "unverified";
            }
        }

        return result;
    }
    [WebMethod]
    public string SendSearchHTML()
    {
        StringBuilder objSb = new StringBuilder();
        objSb.Append("<table border='0' id='mainbody' style='width: 265px; margin: 0px auto; padding: 10px;'>");
        objSb.Append("<tr>");
        objSb.Append("<td>");
        objSb.Append("<select id='DropDownWLCountry' style='width: 220px' onchange='return getCiy();'>");
        objSb.Append("</select>");
        objSb.Append("<span style='color:Red; display:none;' id='validCountry'>*</span>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("<tr>");
        objSb.Append("<td>");
        objSb.Append("<select id='DropDownWLCity' style='width: 220px' tabindex='1'>");
        objSb.Append("</select>");
        objSb.Append("<span style='color:Red; display:none;' id='validcity'>*</span>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("<tr>");
        objSb.Append("<td>");
        objSb.Append("<select id='DropDownWLDate' style='width: 40px'>");
        objSb.Append("</select>");
        objSb.Append("&nbsp;");
        objSb.Append("<select id='DropDownWLMonth' style='width: 113px'>");
        objSb.Append("</select>");
        objSb.Append("&nbsp;");
        objSb.Append("<select id='DropDownWLYear' style='width: 60px'>");
        objSb.Append("</select>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("<tr>");
        objSb.Append("<td>");
        objSb.Append("<span style='color:Red; display:none;' id='validdate'>Please enter valid date</span>");
        objSb.Append("<span style='color:Red; display:none;' id='validdateBack'>Previous date not allowed</span>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("<tr>");
        objSb.Append("<td>");
        objSb.Append("<table width='98%' height='27' border='0' cellpadding='1' cellspacing='1'>");
        objSb.Append("<tr>");
        objSb.Append("<td width='43%'>");
        objSb.Append("<label>");
        objSb.Append("Duration ");
        objSb.Append("</label>");
        objSb.Append("<select id='DropDownWLDuration' style='width: 35px' onchange='return ShowHideWLDayDropDown();'>");
        objSb.Append("<option value='1' selected='selected'>1</option>");
        objSb.Append("<option value='2'>2</option>");
        objSb.Append("</select>");
        objSb.Append("</td>");
        objSb.Append("<td width='57%'>");
        objSb.Append("<label>");
        objSb.Append("Day 1");
        objSb.Append("</label>");
        objSb.Append("<select id='DropDownWLDay1' style='width: 80px'>");
        objSb.Append("<option value='0'>Full day</option>");
        objSb.Append("<option value='1'>Morning</option>");
        objSb.Append("<option value='2'>Afternoon</option>");
        objSb.Append("</select>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("</table>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("<tr id='trWLDay2' style='display: none'>");
        objSb.Append("<td>");
        objSb.Append("<table width='98%' height='27' border='0' cellpadding='1' cellspacing='1'>");
        objSb.Append("<tr>");
        objSb.Append("<td width='43%'>");
        objSb.Append("&nbsp;");
        objSb.Append("</td>");
        objSb.Append("<td width='57%'>");
        objSb.Append("<label>");
        objSb.Append("Day 2");
        objSb.Append("</label>");
        objSb.Append("<select id='DropDownWLDay2' style='width: 80px'>");
        objSb.Append("<option value='0'>Full day</option>");
        objSb.Append("<option value='1'>Morning</option>");
        objSb.Append("<option value='2'>Afternoon</option>");
        objSb.Append("</select>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("</table>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("<tr>");
        objSb.Append("<td>");
        objSb.Append("<label>");
        objSb.Append("Meeting delegates ");
        objSb.Append("</label>");
        objSb.Append("<input type='text' id='TextBoxDelegate' style='width: 40px' onkeydown='javascript:return onlyNumeric(event);' maxlength='5'/>");
        objSb.Append("<span style='color:Red; display:none;' id='validDelegate'>*</span>");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("<tr>");
        objSb.Append("<td>");
        objSb.Append("&nbsp;");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("<tr>");
        objSb.Append("<td align='center'>");
        objSb.Append("<input type='submit' class='button' value='Find your meeting room !' onclick='return WLValidation();' />");
        objSb.Append("</td>");
        objSb.Append("</tr>");
        objSb.Append("</table>");

        return objSb.ToString();
    }

}
