﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LMMR.Business;
using LMMR.Entities;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
using System.Xml;
/// <summary>
/// Summary description for BaseMasterPage
/// </summary>
public class BaseMasterPage : System.Web.UI.MasterPage
{
    private string _keywords;
    private string _description;
    private string _pagetitel;
    public ILog logger
    {
        get { return log4net.LogManager.GetLogger(typeof(BaseMasterPage)); }
    }
    public XmlDocument xmlLanguage
    {
        get
        {
            if (Session["xmlLanguage"] != null)
            {
                return (Session["xmlLanguage"] as XmlDocument);
            }
            else
            {
                return null;
            }
        }
        set
        {
            Session["xmlLanguage"] = value;
        }
    }
    //Get Site Root path
    public Language l
    {
        get;
        set;
    }
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    /// <SUMMARY>
    /// Gets or sets the Meta Keywords tag for the page
    /// </SUMMARY>
    public string Meta_Keywords
    {
        get
        {
            return _keywords;
        }
        set
        {
            // strip out any excessive white-space, newlines and linefeeds
            _keywords = Regex.Replace(value, "\\s+", " ");
        }
    }

    /// <SUMMARY>
    /// Gets or sets the Meta Description tag for the page
    /// </SUMMARY>
    public string Meta_Description
    {
        get
        {
            return _description;
        }
        set
        {
            // strip out any excessive white-space, newlines and linefeeds
            _description = Regex.Replace(value, "\\s+", " ");
        }
    }

    /// <SUMMARY>
    /// Gets or sets the Page Titel tag for the page
    /// </SUMMARY>
    public string Page_Titel
    {
        get
        {
            return _pagetitel;
        }
        set
        {
            // strip out any excessive white-space, newlines and linefeeds
            _pagetitel = Regex.Replace(value, "\\s+", " ");
        }
    }
    int counter = 0;
    protected void Page_Init(object sender, EventArgs e)
    {
        //if (counter == 0)
        //{
            XmlDocument seoXml = new XmlDocument();
            seoXml.Load(Server.MapPath("~/SEO.xml"));
            XmlNode xnod = null;
            l = Session["Language"] as Language;
            string search = "";
            if (l != null)
            {
                search += "/links/link[@language='" + l.Name.ToLower() + "']";
            }
            else
            {
                search += "/links/link[@language='english']";
            }
            string url = Request.RawUrl.Split('/')[Request.RawUrl.Split('/').Length - 2];
            search += "[@linkName='" + url + "']";
            xnod = seoXml.SelectSingleNode(search);
            XmlNode xnodKey = null;
            if (xnod != null)
            {
                if (xnod.HasChildNodes)
                {
                    xnodKey = xnod.FirstChild;
                    if (xnodKey != null)
                    {
                        Meta_Keywords = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
                    }
                    xnodKey = xnodKey.NextSibling;
                    if (xnodKey != null)
                    {
                        Meta_Description = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
                    }
                    xnodKey = xnodKey.NextSibling;
                    if (xnodKey != null)
                    {
                        Page_Titel = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
                    }
                }
            }
            if (string.IsNullOrEmpty(Page_Titel) && string.IsNullOrEmpty(Meta_Description) && string.IsNullOrEmpty(Meta_Keywords))
            {
                search = "";
                search += "/links/link[@language='english']";
                //url = Request.RawUrl.Split('/')[Request.RawUrl.Split('/').Length - 2];
                search += "[@linkName='" + url + "']";
                xnod = seoXml.SelectSingleNode(search);
                xnodKey = null;
                if (xnod != null)
                {
                    if (xnod.HasChildNodes)
                    {
                        xnodKey = xnod.FirstChild;
                        if (xnodKey != null)
                        {
                            Meta_Keywords = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
                        }
                        xnodKey = xnodKey.NextSibling;
                        if (xnodKey != null)
                        {
                            Meta_Description = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
                        }
                        xnodKey = xnodKey.NextSibling;
                        if (xnodKey != null)
                        {
                            Page_Titel = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
                        }
                    }
                }
            }
            seoXml = null;
            //Meta_Description = "My Decsription";
            if (!String.IsNullOrEmpty(Meta_Description))
            {
                HtmlMeta tag = new HtmlMeta();
                tag.Name = "description";
                tag.Content = Meta_Description;
                Page.Header.Controls.AddAt(0, tag);
            }
            //Meta_Keywords = "My Keywords";
            if (!String.IsNullOrEmpty(Meta_Keywords))
            {
                HtmlMeta tag = new HtmlMeta();
                tag.Name = "keywords";
                tag.Content = Meta_Keywords;
                Page.Header.Controls.AddAt(0, tag);
            }

            Cms cmsObj = new SuperAdminTaskManager().getCmsEntityOnType("BottomPart");
            if (cmsObj != null)
            {
                TList<CmsDescription> lstDesc = new ManageCMSContent().GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
                foreach (CmsDescription cd in lstDesc)
                {
                    if (Request.RawUrl.ToLower().Contains(cd.PageUrl.ToLower()))
                    {
                        Meta_Description = cd.MetaDescription == null ? "" : cd.MetaDescription;
                        if (!String.IsNullOrEmpty(Meta_Description))
                        {
                            HtmlMeta tag = new HtmlMeta();
                            tag.Name = "description";
                            tag.Content = Meta_Description;
                            Page.Header.Controls.AddAt(0, tag);
                        }
                        Meta_Keywords = cd.MetaKeyword == null ? "" : cd.MetaKeyword;
                        if (!String.IsNullOrEmpty(Meta_Keywords))
                        {
                            HtmlMeta tag = new HtmlMeta();
                            tag.Name = "keywords";
                            tag.Content = Meta_Keywords;
                            Page.Header.Controls.AddAt(0, tag);
                        }
                        Page_Titel = cd.ContentShortDesc == null ? "" : cd.ContentShortDesc;
                        
                        break;
                    }
                }
            }
            if (!string.IsNullOrEmpty(Page_Titel))
            {
                HtmlTitle ttl = new HtmlTitle();
                ttl.Text = Page_Titel;
                Page.Header.Controls.AddAt(0, ttl);
            }
            else
            {
                Page.Title = "Last Minute Meeting Room";
                HtmlTitle ttl = new HtmlTitle();
                ttl.Text = Page_Titel;
                Page.Header.Controls.AddAt(0, ttl);
            }
        //    counter++;
        //}
        //else
        //{
        //    counter = 0;
        //}
    }
	public BaseMasterPage()
	{
        base.Init += new EventHandler(Page_Init);
	}

    public string GetKeyResult(string key)
    {
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    public string GetKeyResultForJavaScript(string key)
    {
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key)).Replace("'", "\\'").Replace("\"", "\\\"");
    }
}