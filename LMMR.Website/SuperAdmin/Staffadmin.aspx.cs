﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text.RegularExpressions;
using log4net;
using LMMR.Data;

public partial class SuperAdmin_Staffadmin : System.Web.UI.Page
{
    ILog logger = log4net.LogManager.GetLogger(typeof(SuperAdmin_Staffadmin));
    TList<Users> objstafftable = new TList<Users>();
    Staffaccount objstaff = new Staffaccount();
    Users stfuser = new Users();
    UserDetails stfuserdetails = new UserDetails();
    string status;
    #region Page_load
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchPanel1.SearchButtonClick += new EventHandler(Search_Button);
        SearchPanel1.CancelButtonClick += new EventHandler(Cancel_Button);
        
        if (!IsPostBack)
        {
            ViewState["SearchAlpha"] = "all";
            ViewState["Where"] = string.Empty;
            ViewState["Order"] = string.Empty;
            bindrole();

       
            bindstaff();
            DivAddStaff.Visible = false;
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divvalidstf.Style.Add("display", "none");
            divvalidstf.Attributes.Add("class", "error");
            divvalidstf.InnerHtml = "";
            
        }
        else
        {
            ApplyPaging();

        }

       
    }
#endregion

    #region Buttons Usercontrol
    protected void Search_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        bindstaff();
        
    }

    protected void Cancel_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        bindstaff();
        
        divvalidstf.Visible = false;
    }
    #endregion

    public void bindstaff()
    {
        DivAddStaff.Visible = false;
        string whereclaus1 = string.Empty;
        //string orderby = "CreationDate desc";
        ViewState["Order"] = UsersColumn.CreatedDate + " desc";
        
        if (SearchPanel1.stfname != "")
        {
            whereclaus1 = UsersColumn.FirstName + " Like '" + SearchPanel1.stfname + "%' and " + UsersColumn.Usertype +" in (1,2,3)";
            ViewState["ClientName"] = SearchPanel1.stfname.Replace("'", "");

        }
        else
        {
            whereclaus1 = UsersColumn.Usertype + " in (1,2,3)";
            ViewState["ClientName"] = null;
        }

        ViewState["Where"] = whereclaus1;
        ViewState["Where2"] = null;
        ViewState["CurrentPage"] = 0;


        FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
    }

    #region Grid Page IndexChange
    protected void grdstaff_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdstaff.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");


        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    #endregion

    #region Lnk AddNew
    protected void lnkAddNew_Click(object sender, EventArgs e)
    {  
        DivAddStaff.Visible = true;
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddStaff.ClientID + "');});", true);
        
        Session["task"] = "register";
        divmessage.Visible = false;
        //Response.Redirect("~/SuperAdmin/StaffRegistration.aspx");


        txtpassword.Enabled = true;
        txtpasswordconfirm.Enabled = true;
        txtEmail.ReadOnly = false;
        divvalidstf.Style.Add("display", "none");
        divvalidstf.Attributes.Add("class", "error");
        passDIV.Visible = true;
        divvalidstf.InnerHtml = "";
        drprole.Enabled = true;
        
        lblHeader.Text = "Add New Staff";
        bindrole();
        txtEmail.Text = "";
        txtEmail.Enabled = true;
        txtpassword.Text = "";
        txtpasswordconfirm.Text = "";
        txtstaffname.Text = "";
      
        
        

       
    }
    #endregion

    #region Lnk Modify
    protected void lnkmodify_Click(object sender, EventArgs e)
    {

        foreach (GridViewRow gvr in grdstaff.Rows)
        {
            CheckBox chk = (CheckBox)gvr.FindControl("chkField");
            if (chk.Checked == true)
            {
                //LinkButton lbt = (LinkButton)gvr.FindControl("lblRefNo");
                HiddenField hdf = (HiddenField)gvr.FindControl("hdfImage");
                string userid = hdf.Value.ToString(); ;
                Session["Userid"] = userid;
                break;

            }
        }
        
        Session["task"] = "update";
        lblHeader.Text = "Update Staff";
        passDIV.Visible = false;
        DivAddStaff.Visible = true;
        divvalidstf.Style.Add("display", "none");
        divvalidstf.Attributes.Add("class", "error");
        divvalidstf.InnerHtml = "";
        divmessage.Visible = false;
        string userids = Session["Userid"].ToString();
        Users displayuser = new Users();
        displayuser = objstaff.GetStaffUserByID(userids);
        txtstaffname.Text = displayuser.FirstName.Trim().Replace("'", "");
        txtEmail.Text = displayuser.EmailId.Trim().Replace("'", "");
        txtEmail.ReadOnly = true;
        txtpassword.Text = PasswordManager.Decrypt(displayuser.Password.Trim().Replace("'", ""), true);
        txtpassword.Enabled = false;
        txtpasswordconfirm.Text = txtpassword.Text;
        txtpasswordconfirm.Enabled=false;
        bindrole();
        drprole.SelectedValue = displayuser.Usertype.ToString();
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddStaff.ClientID + "');});", true);
        
        

    }
    #endregion

    #region Checkbox select Ony Select One
    public static int iVal = -1;
    public static int jVal = -2;
    protected void chkField_CheckedChanged(object sender, EventArgs e)
    {
        int i;
        CheckBox chk;
        for (i = 0; i <= grdstaff.Rows.Count - 1; i++)
        {
            chk = (CheckBox)grdstaff.Rows[i].FindControl("chkField");
            if (chk.Checked == true)
            {
                if (jVal != i)
                {
                    iVal = i;
                    jVal = i;
                    if (jVal == i)
                    {
                        break;
                    }
                }
            }
        }
        for (i = 0; i <= grdstaff.Rows.Count - 1; i++)
        {
            chk = (CheckBox)grdstaff.Rows[i].FindControl("chkField");
            if (i == iVal)
            {
                chk.Checked = true;
            }
            else
            {
                chk.Checked = false;
            }
        }
        
    }
    #endregion

    #region Row DataBound
    protected void grdstaff_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            try
            {
                GridViewRow gr=e.Row;
                Label lblrole = (Label)gr.FindControl("lblrole");
               int role=Convert.ToInt32(lblrole.Text);
               switch (role)
               {
                   case 1: lblrole.Text = "Superadmin";
                       break;
                   case 2: lblrole.Text = "Operator";
                       break;
                   case 3: lblrole.Text = "Sales";
                       break;
               }
               


            }
            catch(Exception ex)
            {
 
            }
        }
    }
    #endregion

    #region BindGrid
    /// <summary>
    /// Bind grid with filter condition
    /// </summary>
    /// <param name="wherec">This is the string value which contains value of Viewstate Where condition</param>
    /// <param name="order">This is the string value which contains value of Viewstate Order condition</param>
    public void FilterGridWithHeader(string wherec, string order, string clientNameStarts)
    {
        try
        {
            int totalcount = 0;
            string whereclaus = wherec;
            string whereclauseforHotel = wherec;
            string orderby = order;
            if (ViewState["Where2"] != null)
            {
                if (!string.IsNullOrEmpty(ViewState["Where2"].ToString().Trim()))
                {
                    if (!string.IsNullOrEmpty(whereclauseforHotel.Trim()))
                    {
                        whereclauseforHotel += " and ";
                    }
                    whereclauseforHotel += ViewState["Where2"];
                }
            }


            objstafftable = objstaff.GetStaffUsers(whereclauseforHotel, orderby);
            grdstaff.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
            lblTotalBooking.Text = objstafftable.Where(a => a.IsActive == true).Count().ToString();
            grdstaff.DataSource = objstafftable.Where(a => a.IsActive == true).ToList();
            grdstaff.DataBind();
            ApplyPaging();
            //BindAllHotel(THotel);
            //drpHotelName.SelectedValue = selectedHotelName;

            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// Alphabat paging applied using this methods
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void PageChange(object sender, EventArgs e)
    {
        
        try
        {
            int totalcount = 0;
            string whereclaus = String.Empty;
            string orderby = String.Empty;

            HtmlAnchor anc = (sender as HtmlAnchor);
            var d = anc.InnerHtml;
            //ViewState["SearchAlpha"] = d;
            if (SearchPanel1.stfname == "")
            {
                if (d.Trim() == "all")
                {
                    whereclaus = UsersColumn.FirstName + " LIKE '%' and "  + UsersColumn.Usertype  + " in (1,2,3)";
                    ViewState["SearchAlpha"] = d;
                }
                else
                {
                    whereclaus = UsersColumn.FirstName + " LIKE '" + d + "%' and "  + UsersColumn.Usertype + " in (1,2,3) ";
                    ViewState["SearchAlpha"] = d;
                }
            }
            else
            {
                if (d.Trim() == "all")
                {
                    whereclaus = UsersColumn.FirstName + " LIKE '" + SearchPanel1.stfname + "%' and " + UsersColumn.Usertype + " in (1,2,3)";
                    ViewState["SearchAlpha"] = d;
                }
                else
                {
                    whereclaus = UsersColumn.FirstName + " LIKE '"  + d + "%'  and "  + UsersColumn.FirstName +  " LIKE '" + SearchPanel1.stfname + "%' and  "  +  UsersColumn.Usertype  +  " in (1,2,3)";
                    ViewState["SearchAlpha"] = d;
                }
            }
            ViewState["Where2"] = whereclaus;
            ViewState["CurrentPage"] = 0;

            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            //divMessageOnGridHeader.Style.Add("display", "none");
            //divMessageOnGridHeader.Attributes.Add("class", "error");
            if (ViewState["Where"] == null)
            {
                FilterGridWithHeader(ViewState["Where"].ToString(), ViewState["Order"].ToString(), string.Empty);
            }
            else
            {
                FilterGridWithHeader(ViewState["Where2"].ToString(), ViewState["Order"].ToString(), string.Empty);
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
        //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddStaff.ClientID + "');});", false);
        DivAddStaff.Visible = false;
    }


    

    /// <summary>
    /// Apply Numaric paging to the grid
    /// </summary>
    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grdstaff.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdstaff.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdstaff.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdstaff.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdstaff.PageIndex == grdstaff.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    #endregion

    #region DeleteStaff
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow gvr in grdstaff.Rows)
        {
            CheckBox chk = (CheckBox)gvr.FindControl("chkField");
            if (chk.Checked == true)
            {
                //LinkButton lbt = (LinkButton)gvr.FindControl("lblRefNo");
                HiddenField hdf = (HiddenField)gvr.FindControl("hdfImage");
                string userid = hdf.Value.ToString(); ;
                Users staffuserdelete = new Users();
                staffuserdelete = objstaff.GetStaffUserByID(userid);
                status = objstaff.DeleteStaffUser(staffuserdelete);
                divmessage.Visible = true;
                if (status == "Information Deleted successfully.")
                {
                    bindstaff();
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "succesfuly");
                    divmessage.InnerHtml = "Staff Account deleted successfully";

                }
                else
                {
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    divmessage.InnerHtml = status;

                }


                break;

            }

            

        }
    }
    #endregion
    
    #region Registerbutton
    protected void registerBtn_Click(object sender, EventArgs e)
    {

        InsertUpdateusers();
        //drprole.SelectedValue = "0";



    }
    #endregion


    #region Insert Or Upadate User
    private void InsertUpdateusers()
    {
        string staffname = txtstaffname.Text.Trim().Replace("'", "");
        string emailid = txtEmail.Text.Trim().Replace("'", "");
        string password = txtpassword.Text.Trim().Replace("'", "");
        long role = Convert.ToInt64(drprole.SelectedValue);
        if (Session["task"] == "register")
        {
            DateTime Currenttime = DateTime.Now;
            StaffAccount StaffUsers = new StaffAccount();
            stfuser.FirstName = staffname;
            stfuser.EmailId = emailid;
            stfuser.Password = PasswordManager.Encrypt(password, true);
            if (drprole.SelectedItem.Text == "Superadmin")
            {
                stfuser.Usertype = Convert.ToInt32(Usertype.Superadmin); 
            }
            else if (drprole.SelectedItem.Text == "Operator")
            {
                stfuser.Usertype = Convert.ToInt32(Usertype.Operator); 
            }
            else if (drprole.SelectedItem.Text == "Sales")
            {
                stfuser.Usertype = Convert.ToInt32(Usertype.Salesperson); 
            }
            stfuser.CreatedDate = Currenttime;
            //StaffUsers.StaffName = staffname;
            //StaffUsers.EmailId = emailid;
            //StaffUsers.Password = password;
            //StaffUsers.Role = role;
            //StaffUsers.CreationDate = Currenttime;
            status = objstaff.insertstaffdata(stfuser);
            divmessage.Visible = true;
            if (status == "Information Insert successfully.")
            {
                
                
                DivAddStaff.Visible = false;
                bindstaff();
                divmessage.Style.Add("display", "block");
                divmessage.Attributes.Add("class", "succesfuly");
                divmessage.InnerHtml = "Staff account added successfully";
                txtstaffname.Text = "";
                txtEmail.Text = "";
                txtpassword.Text = "";
                txtpasswordconfirm.Text = "";
               
                //drprole.SelectedValue = "0";
                //btnRegisterCancel.Visible = false;
            }

            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddStaff.ClientID + "');});", true);
                divvalidstf.Attributes.Add("class", "error");
                divvalidstf.Style.Add("display", "block");
                divvalidstf.InnerHtml = status;

            }

        }
        else if (Session["task"] == "update")
        {
            string userids = Session["Userid"].ToString();
            Users displayuser = new Users();
            displayuser = objstaff.GetStaffUserByID(userids);
            Users staffusersupdate = new Users();
            string userid = Session["Userid"].ToString();
            staffusersupdate = objstaff.GetStaffUserByID(userid);
            staffusersupdate.FirstName = staffname;
            staffusersupdate.EmailId = emailid;
            //staffusersupdate.Password = PasswordManager.Encrypt(password, true);
            staffusersupdate.Password = displayuser.Password;
            staffusersupdate.Usertype = Convert.ToInt32(drprole.SelectedValue);
            status = objstaff.staffupdate(staffusersupdate, emailid);
            divmessage.Visible = true;
            if (status == "Information updated successfully.")
        {
            bindstaff();
            divmessage.Visible = true;
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "Staff account updated successfully";
            DivAddStaff.Visible = false;
                

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddStaff.ClientID + "');});", true);
            divvalidstf.Attributes.Add("class", "error");
            divvalidstf.Style.Add("display", "block");
            divvalidstf.InnerHtml = status;
        }
            

        }
    }
    #endregion
    public void bindrole()
    {
        
        TList<Roles> userrole = objstaff.GetByAllRole();
        drprole.DataSource = userrole;
        drprole.DataTextField = "RoleName";
        drprole.DataValueField = "Id";
        drprole.DataBind();
        drprole.Items.Insert(0, new ListItem("--Select a Role--", "0"));
    }
    #region Cancel Button Click
    protected void cancelBtn_Click(object sender, EventArgs e)
    {
        //if (txtstaffname.Text != "" || txtEmail.Text != "" || txtpassword.Text != "" || txtpasswordconfirm.Text != "")
        //{
        //    txtstaffname.Text = "";
        //    txtpasswordconfirm.Text = "";
        //    txtpassword.Text = "";
        //    txtEmail.Text = "";
        //}
        //else
        //{
        Response.Redirect("~/SuperAdmin/Staffadmin.aspx");
        //}
    }
    #endregion
}