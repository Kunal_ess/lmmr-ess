﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SuperAdmin_ContentManagementSystem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region Event
    protected void lbtAboutUs_Click(object sender, EventArgs e)
    {

    }

    protected void lbtWhyBookOnline_Click(object sender, EventArgs e)
    {

    }

    protected void lbtLegalAspects_Click(object sender, EventArgs e)
    {

    }

    protected void lbtContactUs_Click(object sender, EventArgs e)
    {

    }
    protected void lbtWhySendRequest_Click(object sender, EventArgs e)
    {

    }
    protected void lbtTermsNConditions_Click(object sender, EventArgs e)
    {

    }
    protected void lbtJoinToday_Click(object sender, EventArgs e)
    {

    }
    protected void lbtCompanyInfo_Click(object sender, EventArgs e)
    {

    }
    protected void lbtHotelsOfWeek_Click(object sender, EventArgs e)
    {

    }
    protected void lbtForInvestors_Click(object sender, EventArgs e)
    {

    }
    protected void lbtPrivacyStatement_Click(object sender, EventArgs e)
    {

    }

    protected void lbtImageGalleryFrontPage_Click(object sender, EventArgs e)
    {

    }
    protected void lbtNews_Click(object sender, EventArgs e)
    {

    }
    protected void lbtBenefits_Click(object sender, EventArgs e)
    {

    }
    protected void lbtSocialLinks_Click(object sender, EventArgs e)
    {

    }
    protected void lbtFrontEndBottomLink_Click(object sender, EventArgs e)
    {

    }
    protected void lnkStaticPages_Click(object sender, EventArgs e)
    {

    }
    protected void lbtNewsBanner_Click(object sender, EventArgs e)
    {

    }
    protected void lbtPersonalizedMessage_Click(object sender, EventArgs e)
    {

    }
    #endregion

}