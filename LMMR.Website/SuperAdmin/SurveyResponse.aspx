﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="SurveyResponse.aspx.cs" Inherits="SuperAdmin_SurveyResponse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="search-operator-layout1" style="margin-bottom: 20px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <div class="search-operator-layout-left1" style="width: 358px;">
                        <div class="search-operator-from1">
                            <div class="commisions-top-new1 ">
                                <table>
                                    <tr>
                                        <td align="right">
                                            Type :
                                        </td>
                                        <td style="height: 30px;">
                                            <asp:RadioButtonList ID="rdbType" runat="server" Style="width: 215px" RepeatDirection="Horizontal"
                                                AutoPostBack="True" OnSelectedIndexChanged="rdbType_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Selected="True">Booking</asp:ListItem>
                                                <asp:ListItem Value="1">Request</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 30px;" colspan="2">
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblVenue" runat="server" Text="Select Venue : "></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:DropDownList ID="drpHotelName" runat="server" CssClass="NoClassApply"
                                                Style="width: 215px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </td>
                <td align="left" valign="bottom">
                    <div style="float: left; margin-left: 20px;" class="n-commisions">
                        <div class="n-btn">
                            <asp:LinkButton ID="lbtSearch" runat="server" OnClick="lbtSearch_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Search</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 10px;" class="n-commisions">
                        <div class="n-btn">
                            <asp:LinkButton ID="lnkClear" runat="server" OnClick="lnkClear_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Clear</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="operator-mainbody">
        <div id="divMessage" runat="server">
        </div>
        <div class="pageing-operator" id="divDelete" runat="server" visible="false">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="bottom">
                        <asp:Panel ID="pnlActivate" runat="server">
                            <div style="float: left; margin-left: 10px;" class="n-commisions">
                                <div class="n-btn">
                                    <asp:LinkButton ID="lnkdelete" runat="server" OnClientClick="javascript:return TestCheckBox(); "
                                        OnClick="lnkdelete_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Delete</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <div style="float: right;" id="pageing">
            </div>
        </div>
        <div runat="server" id="tableReviewGrid">
            <table width="100%" cellspacing="1" cellpadding="5" border="0" bgcolor="#98BCD6">
                <tbody>
                    <tr bgcolor="#CCD8D8" id="trGrid" runat="server" visible="false">
                        <td valign="top" width="10%">
                        </td>
                        <td valign="top" width="15%">
                            Ref No
                        </td>
                        <td valign="top" width="20%">
                            Client Name
                        </td>
                        <td valign="top" width="25%">
                            Company Name
                        </td>
                        <td valign="top" width="15%">
                            Submitted Date
                        </td>
                        <td valign="top" width="15%">
                            Active/Deactive
                        </td>
                    </tr>
                    <asp:GridView ID="grvReview" runat="server" Width="100%" border="0" CellPadding="5"
                        CellSpacing="1" AutoGenerateColumns="false" DataKeyNames="ServeyID" GridLines="None"
                        ShowHeader="false" ShowFooter="false" BackColor="#98BCD6" RowStyle-CssClass="con"
                        AlternatingRowStyle-CssClass="con-dark" EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                        HeaderStyle-BackColor="#98BCD6" OnRowDataBound="grvReview_RowDataBound">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkField" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                    <asp:HiddenField ID="hdnServeyId" runat="server" Value='<%# Eval("ServeyID") %>' />
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkField" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                    <asp:HiddenField ID="hdnServeyId" runat="server" Value='<%# Eval("ServeyID") %>' />
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkRefNo" runat="server" Text='<%# Eval("BookingId") %>' CommandArgument='<%# Eval("ServeyId") %>'
                                        OnClick="lnkRefNo_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:LinkButton ID="lnkRefNo" runat="server" Text='<%# Eval("BookingId") %>' CommandArgument='<%# Eval("ServeyId") %>'
                                        OnClick="lnkRefNo_Click"></asp:LinkButton>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserId" runat="server" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnUserId" runat="server" Value='<%# Eval("UserId") %>' />
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblUserId" runat="server" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnUserId" runat="server" Value='<%# Eval("UserId") %>' />
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="25%">
                                <ItemTemplate>
                                    <asp:Label ID="lblVenueId" runat="server" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnVenueId" runat="server" Value='<%# Eval("VanueId") %>' />
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblVenueId" runat="server" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnVenueId" runat="server" Value='<%# Eval("VanueId") %>' />
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubmittedDate" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblSubmittedDate" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="15%">
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="lblIsOnline" runat="server" Text=""></asp:Label>
                                    &nbsp;
                                    <asp:CheckBox ID="uiCheckBoxIsActive" runat="server" OnCheckedChanged="uiCheckBoxIsActive_CheckChanged"
                                        AutoPostBack="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <table>
                                <tr>
                                    <td colspan="3" align="center">
                                        <b>No record found !</b>
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </tbody>
            </table>
        </div>
        <div style="padding-top: 50px;" runat="server" id="divHeadRep">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <asp:Repeater ID="rptheadQuestion" runat="server" OnItemDataBound="rptheadQuestion_ItemDataBound">
                    <ItemTemplate>
                        <tr class="tableHeaderRow">
                            <td class="tableHeaderCell" colspan="2">
                                <%#Container.ItemIndex +1 %>
                                .
                                <asp:Label ID="lblHeader" runat="server"></asp:Label>
                                <asp:HiddenField ID="hdfsurveyId" runat="server" />
                            </td>
                        </tr>
                        <asp:Repeater ID="rptsubquestions" runat="server" OnItemDataBound="rptsubquestions_ItemDataBound">
                            <ItemTemplate>
                                <tr class="tableFormRowEditor Even">
                                    <td width="60%" class="tableFormCell Even">
                                        <%#Container.ItemIndex +1 %>
                                        ).
                                        <asp:Label ID="lblsubquestions" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdfSubQsurveyId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "QuestionID") %>' />
                                        &nbsp;
                                    </td>
                                    <td width="40%" class="tableFormCell Even" style="border-left: #fff solid 1px;">
                                        <table class="formInput">
                                            <tbody>
                                                <tr style="height: 50%; vertical-align: bottom;">
                                                    <td>
                                                        <asp:Literal ID="ltrGraph" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                                        <asp:HiddenField ID="hdnAnswerID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "AnswerID") %>' />
                                                        <asp:Label ID="lblPercentage" runat="server" Text=""></asp:Label>
                                                        <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="447px" Visible="false" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <div class="button_section" style="padding-top: 50px;" runat="server" id="divButtonSave">
            <asp:LinkButton ID="btnSubmit" CssClass="select" runat="server" Text="Save" OnClick="btnSubmit_Click" />
            <span>or</span>
            <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function TestCheckBox() {
            if (jQuery("#ContentPlaceHolder1_grvReview").find("input:checkbox:checked").length > 0) {

            }
            else {
                alert("Please select at least one row");
                return false;
            }


            return confirm('Are you sure you want to delete this User?');



        }   
                       
    </script>
</asp:Content>
