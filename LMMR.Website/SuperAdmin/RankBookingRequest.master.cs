﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Web.UI.HtmlControls;
using System.Xml;
using LMMR;

public partial class SuperAdmin_RankBookingRequest : System.Web.UI.MasterPage
{
    public CancellationPolicy objCancellationManager = new CancellationPolicy();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CurrentSuperAdminID"] != null)
            {
                Users objUsers = (Users)Session["CurrentSuperAdmin"];
                if (objUsers.Usertype == (int)Usertype.Superadmin)
                {
                    lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                    objUsers.LastLogin = DateTime.Now;
                    lstLoginTime.Text = DateTime.Now.ToLongDateString();
                }
                else
                {
                    Session.Remove("CurrentSuperAdmin");
                    //Response.Redirect("../Login.aspx");
                    Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "login/english");
                    }
                }
            }
        }
    }


    protected void lnklogout_Click(object sender, EventArgs e)
    {
        Session.Remove("CurrentSuperAdmin");
        Session.Remove("CurrentSuperAdminID");
        Session.Remove("CurrentUserID");
        Session.Remove("CurrentUser");
        Session.Remove("CurrentHotelID");
        //Session.Abandon();
        //Response.Redirect("~/login.aspx");
        Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
    }
}
