﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;
using LMMR.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;

public partial class SuperAdmin_SurveyResponse : System.Web.UI.Page
{
    #region variable declaration
    surveybookingrequest objsurveybookingrequest = new surveybookingrequest();
    HotelInfo ObjHotelinfo = new HotelInfo();
    UserManager objUsers = new UserManager();
    HotelManager objHotelManager = new HotelManager();
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSurveyHotels();
            divMessage.Style.Add("display", "none");
            divMessage.Attributes.Add("class", "succesfuly");
            divButtonSave.Visible = false;
            divHeadRep.Visible = false;
        }
    }
    #endregion

    #region Functions
    public void BindSurveyHotels()
    {
        string whereclaus = string.Empty;
        string hotelid = "0";
        if (rdbType.SelectedValue == "0")
        {
            whereclaus = "Bookingtype = 0";
        }
        else
        {
            whereclaus = "Bookingtype in ( 1,2)";
        }

        TList<ServeyResponse> objResponse = new TList<ServeyResponse>();
        objResponse = objsurveybookingrequest.GetSurveyHotelName(whereclaus).FindAllDistinct(ServeyResponseColumn.VanueId);
        foreach (ServeyResponse obj in objResponse)
        {
            hotelid += "," + +obj.VanueId;
        }
        string whereclausHotel = "Id in (" + hotelid + ")";
        TList<Hotel> lstHotel = ObjHotelinfo.GetHotelbyCondition(whereclausHotel, String.Empty);
        //if (lstHotel.Count > 0)
        //{
            drpHotelName.DataSource = lstHotel;
            drpHotelName.DataTextField = "Name";
            drpHotelName.DataValueField = "Id";
            drpHotelName.DataBind();
            drpHotelName.Items.Insert(0, new ListItem("Select Venue", "0"));
        //}
    }

    public void BindAllServey()
    {
        tableReviewGrid.Visible = true;
        string whereclaus = string.Empty;

        if (rdbType.SelectedValue == "0")
        {
            whereclaus = "Bookingtype = 0 AND VanueID=" + drpHotelName.SelectedValue + "";
            //if (drpHotelName.SelectedValue != "0" && drpHotelName.SelectedValue != "")
            //{
            //    whereclaus = "Bookingtype = 0 AND VanueID=" + drpHotelName.SelectedValue + "";
            //}
            //else
            //{
            //    whereclaus = "Bookingtype = 0";
            //}
        }
        else
        {
            whereclaus = "Bookingtype in ( 1,2) AND VanueID=" + drpHotelName.SelectedValue + "";
            //if (drpHotelName.SelectedValue != "0" && drpHotelName.SelectedValue != "")
            //{
            //    whereclaus = "Bookingtype in ( 1,2) AND VanueID=" + drpHotelName.SelectedValue + "";
            //}
            //else
            //{
            //    whereclaus = "Bookingtype in ( 1,2)";
            //}

        }

        TList<ServeyResponse> objResponse = new TList<ServeyResponse>();
        objResponse = objsurveybookingrequest.GetSurveyHotelName(whereclaus);
        grvReview.DataSource = objResponse;
        grvReview.DataBind();
        trGrid.Visible = true;
        divDelete.Visible = true;

    }
    #endregion

    #region Events
    protected void rdbType_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSurveyHotels();
        divButtonSave.Visible = false;
        divHeadRep.Visible = false;
        divDelete.Visible = false;
        tableReviewGrid.Visible = false;
        divMessage.Style.Add("display", "none");
        divMessage.Attributes.Add("class", "error");   
    }

    protected void lbtSearch_Click(object sender, EventArgs e)
    {        
        if (drpHotelName.SelectedValue != "0" && drpHotelName.SelectedValue != "")
        {
            BindAllServey();
            divButtonSave.Visible = false;
            divHeadRep.Visible = false;
            divMessage.Style.Add("display", "none");
            divMessage.Attributes.Add("class", "error");            
        }
        else
        {
            divButtonSave.Visible = false;
            divHeadRep.Visible = false;
            divDelete.Visible = false;
            tableReviewGrid.Visible = false;
            divMessage.Style.Add("display", "block");
            divMessage.Attributes.Add("class", "error");
            divMessage.InnerHtml = "Please select venue";            
        }
    }

    protected void lnkRefNo_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lbnValue = (LinkButton)sender;
            ViewState["SurveyId"] = lbnValue.CommandArgument;
            divButtonSave.Visible = true;
            divHeadRep.Visible = true;
            BindHeadrepeter();
        }
        catch (Exception ex)
        {

        }
    }

    protected void lnkClear_Click(object sender, EventArgs e)
    {
        BindSurveyHotels();
        divMessage.Style.Add("display", "none");
        divMessage.Attributes.Add("class", "error");
        divButtonSave.Visible = false;
        divHeadRep.Visible = false;
        tableReviewGrid.Visible = false;
        divDelete.Visible = false;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        divMessage.Style.Add("display", "none");
        divMessage.Attributes.Add("class", "error");
        divButtonSave.Visible = false;
        divHeadRep.Visible = false;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        bool isStatus = false;
        foreach (RepeaterItem item in rptheadQuestion.Items)
        {
            Repeater rptsubquestions = (Repeater)item.FindControl("rptsubquestions");
            foreach (RepeaterItem childitem in rptsubquestions.Items)
            {
                TextBox txtComment = (TextBox)childitem.FindControl("txtComment");
                if (txtComment != null)
                {
                    if (ViewState["SurveyId"] != null)
                    {
                        ServeyResponse objSR = objsurveybookingrequest.GeyDetailsBySurveyId(Convert.ToInt32(ViewState["SurveyId"]));
                        objSR.AdditionalComment = txtComment.Text;
                        if (objsurveybookingrequest.UpdateServeyResponse(objSR))
                        {
                            isStatus = true;
                        }
                        else
                        {
                            isStatus = false;
                        }
                    }
                }
            }
        }
        if (isStatus)
        {
            divMessage.Style.Add("display", "block");
            divMessage.Attributes.Add("class", "succesfuly");
            divMessage.InnerHtml = "Comment has been updated successfully";
        }
        else
        {
            divMessage.Style.Add("display", "block");
            divMessage.Attributes.Add("class", "error");
            divMessage.InnerHtml = "Error please contact to technical support";
        }
        divButtonSave.Visible = false;
        divHeadRep.Visible = false;
        BindAllServey();
    }

    protected void uiCheckBoxIsActive_CheckChanged(object sender, EventArgs e)
    {
        bool isStatus = false;
        string statusMsg = string.Empty;
        CheckBox ch = (CheckBox)sender;
        GridViewRow item = (GridViewRow)ch.NamingContainer;
        GridViewRow dataItem = (GridViewRow)item;
        int ServeyID = 0;
        string email = string.Empty;
        if (grvReview.DataKeys[dataItem.RowIndex] != null)
        {
            string datakey = grvReview.DataKeys[dataItem.RowIndex].Value.ToString();
            if (datakey != string.Empty)
            {
                ServeyID = Convert.ToInt32(datakey);

                ServeyResponse objServayResponse = objsurveybookingrequest.GeyDetailsBySurveyId(ServeyID);
                if (objServayResponse != null)
                {
                    if (ch.Checked == true)
                    {
                        objServayResponse.ReviewStatus = true;
                        statusMsg = "Activated successfully";
                    }
                    else
                    {
                        objServayResponse.ReviewStatus = false;
                        statusMsg = "Deactivated successfully";
                    }

                    if (objsurveybookingrequest.UpdateServeyResponse(objServayResponse))
                    {
                        isStatus = true;
                    }
                    else
                    {
                        isStatus = false;
                    }
                }
            }
        }
        if (isStatus)
        {
            divMessage.Style.Add("display", "block");
            divMessage.Attributes.Add("class", "succesfuly");
            divMessage.InnerHtml = statusMsg;
            divButtonSave.Visible = false;
            divHeadRep.Visible = false;
        }
        BindAllServey();
    }
    #endregion

    #region
    protected void grvReview_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Label lblUserId = (Label)e.Row.FindControl("lblUserId");
            Label lblVenueId = (Label)e.Row.FindControl("lblVenueId");
            HiddenField hdnUserId = (HiddenField)e.Row.FindControl("hdnUserId");
            HiddenField hdnVenueId = (HiddenField)e.Row.FindControl("hdnVenueId");
            Label lblSubmittedDate = (Label)e.Row.FindControl("lblSubmittedDate");
            ServeyResponse obj = (ServeyResponse)e.Row.DataItem;
            Users objusrs = objUsers.GetUserByID(Convert.ToInt32(hdnUserId.Value));
            lblUserId.Text = objusrs.FirstName + " " + objusrs.LastName;
            Hotel objHotel = objHotelManager.GetHotelDetailsById(Convert.ToInt64(hdnVenueId.Value));
            lblVenueId.Text = objHotel.Name;
            CheckBox uiCheckBoxIsActive = (CheckBox)e.Row.FindControl("uiCheckBoxIsActive");
            Label lblIsOnline = (Label)e.Row.FindControl("lblIsOnline");
            lblSubmittedDate.Text = string.Format("{0 : MM/dd/yyyy}", obj.ReviewSubmitted);
            if (obj.ReviewStatus)
            {
                uiCheckBoxIsActive.Checked = true;
                lblIsOnline.Text = "Online";
            }
            else
            {
                uiCheckBoxIsActive.Checked = false;
                lblIsOnline.Text = "Offline";
            }
        }
    }
    #endregion

    

    #region Delete User
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        bool isStatus = false;
        foreach (GridViewRow gvr in grvReview.Rows)
        {
            CheckBox chk = (CheckBox)gvr.FindControl("chkField");
            if (chk.Checked == true)
            {
                HiddenField hdnId = (HiddenField)gvr.FindControl("hdnServeyId");
                if (objsurveybookingrequest.DeleteServey(Convert.ToInt32(hdnId.Value)))
                {
                    isStatus = true;
                }
                else
                {
                    isStatus = false;
                }
            }
        }
        if (isStatus)
        {
            divMessage.Style.Add("display", "block");
            divMessage.Attributes.Add("class", "succesfuly");
            divMessage.InnerHtml = "Survey deleted successfully";
        }
        else
        {
            divMessage.Style.Add("display", "block");
            divMessage.Attributes.Add("class", "error");
            divMessage.InnerHtml = "Error please contact to technical support";
        }
        BindAllServey();
        BindSurveyHotels();
    }
    #endregion

    #region For Survey booking after clicking on reference number
    public void BindHeadrepeter()
    {
        TList<ServeyQuestion> objsurveydesc = new TList<ServeyQuestion>();
        objsurveydesc = objsurveybookingrequest.GetAllsurveyDescription();
        rptheadQuestion.DataSource = objsurveydesc;
        rptheadQuestion.DataBind();
    }
    protected void rptheadQuestion_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServeyQuestion surveydesc = e.Item.DataItem as ServeyQuestion;
            Label lblHeader = (Label)e.Item.FindControl("lblHeader");
            HiddenField hdfsurveyId = (HiddenField)e.Item.FindControl("hdfsurveyId");
            Repeater rptsubquestions = (Repeater)e.Item.FindControl("rptsubquestions");
            if (surveydesc != null)
            {
                lblHeader.Text = surveydesc.QuestionName;
                hdfsurveyId.Value = surveydesc.QuestionId.ToString();
                if (rdbType.SelectedValue == "0")
                {
                    rptsubquestions.DataSource = objsurveybookingrequest.Getsurveybooking().Where(a => a.QuestionId == surveydesc.QuestionId).ToList();
                    rptsubquestions.DataBind();
                }
                else
                {
                    rptsubquestions.DataSource = objsurveybookingrequest.Getsurveyrequest().Where(a => a.QuestionId == surveydesc.QuestionId).ToList();
                    rptsubquestions.DataBind();
                }
            }
        }
    }

    protected void rptsubquestions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServeyAnswer surveyanswer = e.Item.DataItem as ServeyAnswer;
            Label lblsubquestions = (Label)e.Item.FindControl("lblsubquestions");
            HiddenField hdfSubQsurveyId = (HiddenField)e.Item.FindControl("hdfSubQsurveyId");
            HiddenField hdnAnswerID = (HiddenField)e.Item.FindControl("hdnAnswerID");
            Label lblPercentage = (Label)e.Item.FindControl("lblPercentage");
            TextBox txtComment = (TextBox)e.Item.FindControl("txtComment");
            Literal ltrGraph = (Literal)e.Item.FindControl("ltrGraph");
            if (surveyanswer != null)
            {
                lblsubquestions.Text = surveyanswer.Answer;
                if (hdfSubQsurveyId.Value == "4")
                {
                    lblPercentage.Visible = false;
                    txtComment.Visible = true;
                }
                else
                {
                    string where = "AnswerId = '" + Convert.ToInt32(hdnAnswerID.Value) + "' and ServeyId=" + Convert.ToString(ViewState["SurveyId"]) + " ";
                    TList<Serveyresult> objResult = objsurveybookingrequest.GetServeyresult(where);
                    if (objResult.Count > 0)
                    {
                        lblPercentage.Text = Convert.ToString(objResult[0].Rating);
                        ltrGraph.Text = "<div style='width:100px;background-color:#FE2E2E;height:18px;'><div style='width:" + Convert.ToString(objResult[0].Rating) + "0px;background-color:#2EFE2E;height:18px;'></div></div>";
                    }
                    else
                    {
                        lblPercentage.Text = "0";
                        ltrGraph.Text = "<div style='width:100px;background-color:#FE2E2E;height:18px;'></div>";
                    }
                    lblPercentage.Visible = true;
                    txtComment.Visible = false;
                    // hdfcommentstxt.Value = "";                    
                }
                hdfSubQsurveyId.Value = surveyanswer.AnswerId.ToString();
            }
            ServeyResponse srv = objsurveybookingrequest.GeyDetailsBySurveyId(Convert.ToInt32(ViewState["SurveyId"]));
            txtComment.Text = srv.AdditionalComment;
        }
    }
    #endregion
}