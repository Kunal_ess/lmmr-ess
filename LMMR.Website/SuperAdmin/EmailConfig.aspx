﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="EmailConfig.aspx.cs" Inherits="SuperAdmin_EmailConfig" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--superadmin-example-layout start -->
    <%--<asp:ScriptManager ID="scriptupd" runat="server" ></asp:ScriptManager>--%>
    <div style="float: left;">
        <asp:UpdatePanel ID="upd" runat="server">
            <ContentTemplate>
                <div class="superadmin-example-layout">
                    <div id="divMessage" runat="server">
                    </div>
                    <div class="contract-list" style="border-bottom: none; border-right: none;">
                        <div style="width: 900px;" class="contract-list-left">
                            <h2>
                                Email Config</h2>
                        </div>
                    </div>
                    <div class="superadmin-cms">
                        <div class="superadmin-cms-box1" style="min-height: 400px; border-right: 1px solid #92BEDE;">
                            <h3>
                                Event Driven Mails</h3>
                            <ul>
                                <asp:Repeater ID="rptEventDriven" runat="server" OnItemCommand="rptEventDriven_ItemCommand"
                                    OnItemDataBound="rptEventDriven_ItemDataBound">
                                    <ItemTemplate>
                                        <li style="width: 215px;">
                                            <asp:LinkButton ID="lnkEmail" runat="server"></asp:LinkButton></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <div class="superadmin-cms-box1" style="min-height: 400px;">
                            <h3>
                                Periodic Mails</h3>
                            <ul>
                                <asp:Repeater ID="rptPeriodic" runat="server" OnItemCommand="rptPeriodic_ItemCommand"
                                    OnItemDataBound="rptPeriodic_ItemDataBound">
                                    <ItemTemplate>
                                        <li style="width: 215px;">
                                            <asp:LinkButton ID="lnkEmail" runat="server"></asp:LinkButton></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </div>
                </div>
                <br />
                <asp:Panel ID="pnlEmailDetails" runat="server">
                    <div class="superadmin-example-mainbody">
                        <div id="divLowerMessage" runat="server">
                        </div>
                        <div class="superadmin-mainbody-sub1">
                            <div class="superadmin-mainbody-sub1-left">
                                <h2>
                                    Email Config:
                                    <asp:Label ID="lblEmailName" runat="server"></asp:Label>
                                    - (<asp:Label ID="lblEmailType" runat="server"></asp:Label>)</h2>
                            </div>
                        </div>
                        <div class="superadmin-tabbody1">
                            <div>
                            </div>
                            <div id="TabbedPanels2" class="TabbedPanels">
                                <asp:HiddenField ID="hdnItemId" runat="server" />
                                <asp:HiddenField ID="hdnLanguageID" runat="server" />
                                <asp:HiddenField ID="hdnOldLanguageID" runat="server" />
                                <asp:Button ID="btnSetLanguage" runat="server" CssClass="displayNone" Text="ChangeLanguage"
                                    OnClick="btnSetLanguage_Click" />
                                <div class="superadmin-mainbody-sub6" style="width: 100%;">
                                    <ul>
                                        <asp:Literal ID="ltrLanguage" runat="server"></asp:Literal>
                                    </ul>
                                </div>
                                <div class="TabbedPanelsContentGroup">
                                    <div class="TabbedPanelsContent">
                                        <table width="960px;" cellpadding="5" cellspacing="1" bgcolor="#92bddd" style="margin-left: -1px;">
                                            <tr bgcolor="#E3F0F1">
                                                <td width="15%" style="line-height: 31px;">
                                                    <b>List of Parameters:</b>
                                                </td>
                                                <td>
                                                    <asp:Literal ID="ltrParameters" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#F1F8F8">
                                                <td width="15%" style="line-height: 31px;">
                                                    <b>Is Active:</b>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpIsActive" runat="server">
                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <asp:Panel ID="pnlPeriodic" runat="server">
                                                <tr bgcolor="#E3F0F1">
                                                    <td width="15%" style="line-height: 31px;">
                                                        <b>Period of calls:</b>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPeriodOfCall" runat="server" Width="100px" MaxLength="3" CssClass="textfield"></asp:TextBox>
                                                        <span style="font-family: Arial; font-size: Smaller;">( For duration based specify the No of days after which mail has to be sent, For date based specify the Date on which mail has to be sent)</span>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#F1F8F8">
                                                    <td width="15%" style="line-height: 31px;">
                                                        <b>Type of Period:</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPeriodType" runat="server" Text="" style="display:none"></asp:Label>
                                                        <asp:DropDownList ID="drpPeriodType" runat="server">
                                                            <asp:ListItem Text="Duration Based" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Date Based" Value="1"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </asp:Panel>
                                            <tr bgcolor="#E3F0F1">
                                                <td width="15%" valign="top">
                                                    <b>Email Contents :</b>
                                                </td>
                                                <td>
                                                    <CKEditor:CKEditorControl ID="CKEditorContents" runat="server" Height="200" Toolbar="Full"></CKEditor:CKEditorControl><br />
                                                    <img src="../Images/help.jpg" />&nbsp;&nbsp;<strong>Please do not modify the parameters
                                                        text from the mail contents.</strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
                                padding-bottom: 10px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="lnkSave" runat="server" OnClick="lnkSave_Click" CssClass="select">Save</asp:LinkButton>
                                    &nbsp;or&nbsp;
                                    <asp:LinkButton ID="lnkCancel" runat="server" OnClick="lnkCancel_Click" CssClass="cancelpop">Cancel</asp:LinkButton>
                                </div>
                            </div>
                            <script type="text/javascript">
                                //var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels2");
                                function SetLanguage(val) {
                                    document.getElementById('<%= hdnOldLanguageID.ClientID %>').value = document.getElementById('<%= hdnLanguageID.ClientID %>').value;
                                    document.getElementById('<%= hdnLanguageID.ClientID %>').value = val;
                                    document.getElementById('<%= btnSetLanguage.ClientID %>').click();
                                    return true;
                                    //                            if (confirm('Are you want to store this change in temp. storage. On Final "Save" click it\'ll be stored in database.')) {
                                    //                                
                                    //                                return true;
                                    //                            }
                                    //                            else {
                                    //                                return false;
                                    //                            }
                                }
                            </script>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <script language="javascript" type="text/javascript">
            function movePage(val) {
                document.getElementsByTagName('html')[0].style.overflow = 'auto';
                jQuery('#Loding_overlay').hide();
                var ofset = jQuery("#" + val).offset();

                jQuery('body').scrollTop(ofset.top);
                jQuery('html').scrollTop(ofset.top);


            }
        </script>
    </div>
</asp:Content>
