﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using CKEditor;
using System.Linq.Expressions;
public partial class SuperAdmin_CancellationPolicy : System.Web.UI.Page
{
    public CancellationPolicy objCancellationManager = new CancellationPolicy();
    public ManageOthers objOthers = new ManageOthers();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCountryDropDown();
            BindPolicylstView();
            pnlAddModify.Style.Add("display", "none");
            HotelAssignTab.Style.Add("display", "none");
            pnlAssignHotel.Style.Add("display", "none");
            divmsg.Style.Add("display", "none");
            divmsg.Attributes.Add("class", "error");
        }

        ApplyPaging();
    }

    private void ApplyPaging()
    {
        try
        
        {
            GridViewRow row = lstViewPolicy.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (lstViewPolicy.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= lstViewPolicy.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == lstViewPolicy.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (lstViewPolicy.PageIndex == lstViewPolicy.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    /// <summary>
    /// This function used for bind policy to listview.
    /// </summary>
    public void BindPolicylstView()
    {
        if (ddlCountry.SelectedValue != "0")
        {
            TList<CancelationPolicy> obj = objCancellationManager.GetPolicyByCountry(Convert.ToInt32(ddlCountry.SelectedValue));
            if (obj.Count > 0)
            {
                lstViewPolicy.DataSource = obj;
                lstViewPolicy.DataBind();
                ModifyButton.Style.Add("display", "block");
                DeleteButton.Style.Add("display", "block");
            }
            else
            {
                lstViewPolicy.DataSource = obj;
                lstViewPolicy.DataBind();
                ModifyButton.Style.Add("display", "none");
                DeleteButton.Style.Add("display", "none");
            }
        }
        else
        {
            lstViewPolicy.DataSource = objCancellationManager.GetAllPolicy();
            lstViewPolicy.DataBind();
        }
    }
    /// <summary>
    /// This function used for bind cancellation policy.
    /// </summary>
    public void BindCancellationPolicyDropDown()
    {
        //ddlPolicy.DataSource = objCancellationManager.GetAllPolicy();
        //ddlPolicy.DataTextField = "PolicyName";
        //ddlPolicy.DataValueField = "Id";
        //ddlPolicy.DataBind();
        //ddlPolicy.Items.Insert(0, new ListItem("--Select cancellation policy--", "0"));
    }
    /// <summary>
    /// This function used for clear time frame form.
    /// </summary>
    public void clearForm()
    {
        //ddlPolicy.SelectedValue = "0";
        txtCancellationLimit.Text = "";
        txtName.Text = "";
        //txtTimeFrameMax1.Text = "";
        txtTimeFrameMin1.Text = "";
        txtTimeFrameMax2.Text = "";
        txtTimeFrameMin2.Text = "";
        txtTimeFrameMax3.Text = "";
        txtTimeFrameMin3.Text = "";
        txtTimeFrameMax4.Text = "";
        //txtTimeFrameMin4.Text = "";
        CKEditorSection1.Text = "";
        CKEditorSection2.Text = "";
        CKEditorSection3.Text = "";
        CKEditorSection4.Text = "";
        foreach (GridViewRow l in lstViewPolicy.Rows)
        {
            CheckBox r = (CheckBox)l.FindControl("rbselectpolicy");
            if (r.Checked)
            {
                r.Checked = false;
            }
        }
    }
    /// <summary>
    /// This function used for bind checkbox list of hotel.
    /// </summary>
    /// <param name="countryID"></param>
    /// <param name="policyID"></param>
    public void BindHotelCheckBoxList(int countryID, int policyID)
    {
        TList<Hotel> objhotel = objCancellationManager.GetHotelByCountryID(countryID);
        chklstHotel.DataSource = objhotel;
        chklstHotel.DataTextField = "Name";
        chklstHotel.DataValueField = "Id";
        chklstHotel.DataBind();

        TList<PolicyHotelMapping> objMappingHotel = objCancellationManager.GetHotelMapping(policyID);
        if (objMappingHotel.Count > 0)
        {

            foreach (ListItem obj in chklstHotel.Items)
            {
                if (objMappingHotel.Find(a => a.HotelId == Convert.ToInt32(obj.Value)) != null)
                {
                    obj.Selected = true;
                    obj.Attributes.Add("style", "background-color: green;");


                }
            }

            TList<CancelationPolicy> objPolicy = objCancellationManager.GetPolicyByCountry(countryID);
            TList<PolicyHotelMapping> objMapping = new TList<PolicyHotelMapping>();
            int i = 0;
            for (i = 0; i <= objPolicy.Count - 1; i++)
            {
                if (Convert.ToInt32(objPolicy[i].Id) != policyID)
                {
                    objMapping = objCancellationManager.GetHotelMapping(Convert.ToInt32(objPolicy[i].Id));
                }

                foreach (ListItem obj in chklstHotel.Items)
                {
                    if (objMapping.Find(a => a.HotelId == Convert.ToInt32(obj.Value)) != null)
                    {
                        obj.Enabled = false;
                        obj.Attributes.Add("style", "background-color: yellow;");

                    }
                }
            }
        }
        else
        {
            TList<CancelationPolicy> objPolicy = objCancellationManager.GetPolicyByCountry(countryID);
            int i = 0;
            for (i = 0; i <= objPolicy.Count - 1; i++)
            {
                TList<PolicyHotelMapping> objMapping = objCancellationManager.GetHotelMapping(Convert.ToInt32(objPolicy[i].Id));

                foreach (ListItem obj in chklstHotel.Items)
                {
                    if (objMapping.Find(a => a.HotelId == Convert.ToInt32(obj.Value)) != null)
                    {
                        obj.Enabled = false;
                        obj.Attributes.Add("style", "background-color: yellow;");

                    }
                }
            }

        }
    }

    protected void lstViewPolicy_ItemDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridViewRow currentItem = (GridViewRow)e.Row;
            DataKey currentDataKey = this.lstViewPolicy.DataKeys[currentItem.RowIndex];
            CancelationPolicy policy = e.Row.DataItem as CancelationPolicy;
            Label lblCountryName = (Label)currentItem.FindControl("lblCountryName");
            Country co = objCancellationManager.GetCountry().Find(a => a.Id == policy.CountryId);
            if (co != null)
            {
                lblCountryName.Text = co.CountryName;
            }
            Label lblPolicyName = (Label)currentItem.FindControl("lblPolicyName");
            lblPolicyName.Text = policy.PolicyName;
            Label lblCreationDate = (Label)currentItem.FindControl("lblCreationDate");
            lblCreationDate.Text = String.Format("{0 :MM/dd/yyyy}", policy.CreationDate);
            Label lblTimeFrame = (Label)currentItem.FindControl("lblTimeFrame");
            lblTimeFrame.Text = policy.TimeFrameMax1 + "-" + policy.TimeFrameMax2 + "-" + policy.TimeFrameMax3 + "-" + policy.TimeFramMax4 + "-" + policy.TimeFrameMin4;
            Label lblAppliedTo = (Label)currentItem.FindControl("lblAppliedTo");
            TList<PolicyHotelMapping> objMapping = objCancellationManager.GetHotelMapping(Convert.ToInt32(currentDataKey.Value));
            if (objMapping.Count > 0)
            {
                int Index = 0;
                for (Index = 0; Index <= objMapping.Count - 1; Index++)
                {
                    Hotel objHotel = objCancellationManager.GetHotelById(Convert.ToInt32(objMapping[Index].HotelId));
                    if (objHotel != null)
                    {
                        if (string.IsNullOrEmpty(lblAppliedTo.Text))
                        {
                            lblAppliedTo.Text = objHotel.Name.ToString();
                        }
                        else
                        {
                            lblAppliedTo.Text = lblAppliedTo.Text + "," + objHotel.Name.ToString();
                        }
                    }

                }
                if (lblAppliedTo.Text.Split(',').Length > 2)
                {
                    lblAppliedTo.Text = lblAppliedTo.Text.Split(',')[0] + lblAppliedTo.Text.Split(',')[1] + "...";
                }
            }
            else
            {
                lblAppliedTo.Text = "All";

            }

            Label lblUserCanCancel = (Label)currentItem.FindControl("lblUserCanCancel");
            CheckBox chkSetDefault = (CheckBox)e.Row.FindControl("chkSetDefault");
            lblUserCanCancel.Text = policy.CancelationLimit.ToString();
            Image imgIsDefault = (Image)currentItem.FindControl("imgIsDefault");
            if (policy.DefaultCountry)
            {
                imgIsDefault.ImageUrl = "../images/tick.png";
                chkSetDefault.Visible = false;
            }
            else
            {
                imgIsDefault.ImageUrl = "../images/tick.png";
                imgIsDefault.Visible = false;
                chkSetDefault.Visible = true;
            }

        }
    }

    public void SetDefaultPolicy(object sender, EventArgs e)
    {
        int key = Convert.ToInt32(((CheckBox)sender).Attributes["key"]);
        objCancellationManager.SetDefaultPolicyOfCountry(key);
        Others objOthersInfo = objOthers.GetInfoByCountryID(Convert.ToInt32(ddlCountry.SelectedValue)).FirstOrDefault();
        if (objOthersInfo != null)
        {
            objOthersInfo.CancellationPolicyId = key;
            objOthers.UpdateOthersInfo(objOthersInfo);
        }
        else
        { 
            Others objo = new Others();
            objo.CountryId = Convert.ToInt32(ddlCountry.SelectedValue);
            objo.CancellationPolicyId =key;
            objOthers.InserOtherInfo(objo);
        }
        BindPolicylstView();
        ApplyPaging();
        pnlAddModify.Style.Add("display", "none");
        pnlAssignHotel.Style.Add("display", "none");
        HotelAssignTab.Style.Add("display", "none");

        divmsg.Style.Add("display", "block");
        divmsg.Attributes.Add("class", "succesfuly");
        divmsg.InnerHtml = "successfully set as default.";

    }

    protected void lnkButtonAddnew_Click(object sender, EventArgs e)
    {
        //DropDownList ddlCountry = (DropDownList)Master.FindControl("ddlCountry");
        hdnLanguageID.Value = "0";
        hdnPolicyID.Value = "0";
        pnlAddModify.Style.Add("display", "block");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + pnlAddModify.ClientID + "');});", true);
        HotelAssignTab.Style.Add("display", "none");
        pnlAssignHotel.Style.Add("display", "none");
        btnUpdate.Visible = false;
        btnSave.Visible = true;
        LanguageTab1.CreateLanguageTab(Convert.ToInt32(ddlCountry.SelectedValue));
        clearForm();

        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");
    }
    protected void lnkButtonModify_Click(object sender, EventArgs e)
    {
        hdnLanguageID.Value = "0";
        hdnPolicyID.Value = "0";
        //DropDownList ddlCountry = (DropDownList)Master.FindControl("ddlCountry");

        foreach (GridViewRow l in lstViewPolicy.Rows)
        {
            CheckBox r = (CheckBox)l.FindControl("rbselectpolicy");
            if (r.Checked)
            {
                //DataKey currentDataKey = this.lstViewPolicy.DataKeys["Id"] as DataKey;
                DataKey currentDataKey = this.lstViewPolicy.DataKeys[l.RowIndex];
                hdnPolicyID.Value = currentDataKey.Value.ToString();
            }
        }

        if (hdnPolicyID.Value != "0")
        {
            CancelationPolicy obj = objCancellationManager.GetPolicyByID(Convert.ToInt32(hdnPolicyID.Value));

            ddlCountry.SelectedValue = obj.CountryId.ToString();
            txtName.Text = obj.PolicyName;
            lblPolicyName.Text = obj.PolicyName;
            txtCancellationLimit.Text = obj.CancelationLimit.ToString();
            txtTimeFrameMax1.Text = obj.TimeFrameMax1.ToString();
            txtTimeFrameMin1.Text = obj.TimeFrameMin1.ToString();
            txtTimeFrameMax2.Text = obj.TimeFrameMax2.ToString();
            txtTimeFrameMin2.Text = obj.TimeFrameMin2.ToString();
            txtTimeFrameMax3.Text = obj.TimeFrameMax3.ToString();
            txtTimeFrameMin3.Text = obj.TimeFrameMin3.ToString();
            txtTimeFrameMax4.Text = obj.TimeFramMax4.ToString();
            txtTimeFrameMin4.Text = obj.TimeFrameMin4.ToString();
            TList<CancelationPolicyLanguage> objCancellationSection = objCancellationManager.GetPolicySectionsByPolicyID(Convert.ToInt32(hdnPolicyID.Value));
            long intLanguageID = objCancellationManager.GetLanguage().Find(a => a.Name == "English").Id;
            if (objCancellationSection.Find(a => a.LanguageId == intLanguageID) != null)
            {
                CancelationPolicyLanguage objCancellation = objCancellationSection.Find(a => a.LanguageId == intLanguageID);
                CKEditorSection1.Text = objCancellation.PolicySection1;
                CKEditorSection2.Text = objCancellation.PolicySection2;
                CKEditorSection3.Text = objCancellation.PolicySection3;
                CKEditorSection4.Text = objCancellation.PolicySection4;
            }

            BindHotelCheckBoxList(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(hdnPolicyID.Value));
            pnlAddModify.Style.Add("display", "block");
            HotelAssignTab.Style.Add("display", "block");
            pnlAssignHotel.Style.Add("display", "none");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + HotelAssignTab.ClientID + "');});", true);
            btnUpdate.Visible = true;
            btnSave.Visible = false;
            LanguageTab1.CreateLanguageTab(Convert.ToInt32(ddlCountry.SelectedValue));

            divmsg.Style.Add("display", "none");
            divmsg.Attributes.Add("class", "error");
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        long intLanguageID = Convert.ToInt32(hdnLanguageID.Value);
        if (intLanguageID == 0)
        {
            intLanguageID = objCancellationManager.GetLanguage().Find(a => a.Name == "English").Id;
        }
        CancelationPolicy obj = new CancelationPolicy();
        //DropDownList ddlCountry = (DropDownList)Master.FindControl("ddlCountry");
        obj.CountryId = Convert.ToInt32(ddlCountry.SelectedValue);
        obj.PolicyName = Convert.ToString(txtName.Text);
        obj.CreationDate = DateTime.Now;
        obj.TimeFrameMin1 = Convert.ToInt32(txtTimeFrameMin1.Text);
        obj.TimeFrameMax1 = Convert.ToInt32(txtTimeFrameMax1.Text);
        obj.TimeFrameMin2 = Convert.ToInt32(txtTimeFrameMin2.Text);
        obj.TimeFrameMax2 = Convert.ToInt32(txtTimeFrameMax2.Text);
        obj.TimeFrameMin3 = Convert.ToInt32(txtTimeFrameMin3.Text);
        obj.TimeFrameMax3 = Convert.ToInt32(txtTimeFrameMax3.Text);
        obj.TimeFrameMin4 = Convert.ToInt32(txtTimeFrameMin4.Text);
        obj.TimeFramMax4 = Convert.ToInt32(txtTimeFrameMax4.Text);
        obj.CancelationLimit = Convert.ToInt32(txtCancellationLimit.Text);
        
            TList<CancelationPolicy> objPolicyByCountry = objCancellationManager.GetPolicyByCountry(Convert.ToInt32(ddlCountry.SelectedValue));
            if (objPolicyByCountry.Count > 0)
            {
                if (objCancellationManager.IsDefaultpolicyExist(Convert.ToInt32(ddlCountry.SelectedValue)))
                {
                    obj.DefaultCountry = false;
                }
                else
                {

                    obj.DefaultCountry = true;
                }
            }
            else
            {
                obj.DefaultCountry = true;
            }

        CancelationPolicyLanguage objCancellationLanguage = new CancelationPolicyLanguage();
        objCancellationLanguage.PolicySection1 = CKEditorSection1.Text;
        objCancellationLanguage.PolicySection2 = CKEditorSection2.Text;
        objCancellationLanguage.PolicySection3 = CKEditorSection3.Text;
        objCancellationLanguage.PolicySection4 = CKEditorSection4.Text;
        objCancellationLanguage.LanguageId = intLanguageID;
        if (objCancellationManager.AddNewPolicy(obj, objCancellationLanguage))
        {
            if (obj.DefaultCountry == true)
            {
                Others objOthersInfo = objOthers.GetInfoByCountryID(Convert.ToInt32(ddlCountry.SelectedValue)).FirstOrDefault();
                if (objOthersInfo != null)
                {
                    objOthersInfo.CancellationPolicyId = obj.Id;
                    objOthers.UpdateOthersInfo(objOthersInfo);
                }
                else
                {
                    Others objo = new Others();
                    objo.CountryId = Convert.ToInt32(ddlCountry.SelectedValue);
                    objo.CancellationPolicyId = obj.Id;
                    objOthers.InserOtherInfo(objo);
                }
            }
            pnlAddModify.Style.Add("display", "none");
            pnlAssignHotel.Style.Add("display", "none");
            HotelAssignTab.Style.Add("display", "none");

            divmsg.Style.Add("display", "block");
            divmsg.Attributes.Add("class", "succesfuly");
            divmsg.InnerHtml = "Policy added successfully";

            BindPolicylstView();
            ApplyPaging();
        }

    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        long intLanguageID = Convert.ToInt32(hdnLanguageID.Value);
        int intpolicyID = Convert.ToInt32(hdnPolicyID.Value);
        bool result = false;
        if (intpolicyID != 0)
        {
            if (intLanguageID == 0)
            {
                intLanguageID = objCancellationManager.GetLanguage().Find(a => a.Name == "English").Id;
            }

            CancelationPolicy obj = objCancellationManager.GetPolicyByID(intpolicyID);
            //DropDownList ddlCountry = (DropDownList)Master.FindControl("ddlCountry");
            obj.CountryId = Convert.ToInt32(ddlCountry.SelectedValue);
            obj.PolicyName = Convert.ToString(txtName.Text);
            obj.CreationDate = DateTime.Now;
            obj.TimeFrameMin1 = Convert.ToInt32(txtTimeFrameMin1.Text);
            obj.TimeFrameMax1 = Convert.ToInt32(txtTimeFrameMax1.Text);
            obj.TimeFrameMin2 = Convert.ToInt32(txtTimeFrameMin2.Text);
            obj.TimeFrameMax2 = Convert.ToInt32(txtTimeFrameMax2.Text);
            obj.TimeFrameMin3 = Convert.ToInt32(txtTimeFrameMin3.Text);
            obj.TimeFrameMax3 = Convert.ToInt32(txtTimeFrameMax3.Text);
            obj.TimeFrameMin4 = Convert.ToInt32(txtTimeFrameMin4.Text);
            obj.TimeFramMax4 = Convert.ToInt32(txtTimeFrameMax4.Text);
            obj.CancelationLimit = Convert.ToInt32(txtCancellationLimit.Text);
            //CancelationPolicy ObjCan = obj.Copy();
            //if (!obj.DefaultCountry)
            //{
                
            //    TList<CancelationPolicy> objPolicyByCountry = objCancellationManager.GetPolicyByCountry(Convert.ToInt32(ddlCountry.SelectedValue));
            //    if (objPolicyByCountry.Count > 0)
            //    {
            //        if (objCancellationManager.IsDefaultpolicyExist(Convert.ToInt32(ddlCountry.SelectedValue)))
            //        {
            //            ObjCan.DefaultCountry = false;
            //        }
            //        else
            //        {
            //            ObjCan.DefaultCountry = true;
            //        }
            //    }
            //    else
            //    {
            //        ObjCan.DefaultCountry = true;

            //    }
            //}
            
            TList<CancelationPolicyLanguage> objCancellationSections = objCancellationManager.GetPolicySectionsByPolicyID(intpolicyID);
            if (objCancellationSections.Find(a => a.LanguageId == intLanguageID) != null)
            {
                CancelationPolicyLanguage objcancellation = objCancellationSections.Find(a => a.LanguageId == intLanguageID);
                objcancellation.PolicySection1 = CKEditorSection1.Text;
                objcancellation.PolicySection2 = CKEditorSection2.Text;
                objcancellation.PolicySection3 = CKEditorSection3.Text;
                objcancellation.PolicySection4 = CKEditorSection4.Text;
                objcancellation.LanguageId = intLanguageID;
                result = objCancellationManager.UpdateCancellationPolicy(obj, objcancellation);
            }
            else
            {
                CancelationPolicyLanguage objCancellationLanguage = new CancelationPolicyLanguage();
                objCancellationLanguage.CancelationPolicyId = intpolicyID;
                objCancellationLanguage.PolicySection1 = CKEditorSection1.Text;
                objCancellationLanguage.PolicySection2 = CKEditorSection2.Text;
                objCancellationLanguage.PolicySection3 = CKEditorSection3.Text;
                objCancellationLanguage.PolicySection4 = CKEditorSection4.Text;
                objCancellationLanguage.LanguageId = intLanguageID;
                result = objCancellationManager.AddSectionInNewlanguage(obj, objCancellationLanguage);
            }
            if (result)
            {
                if (obj.DefaultCountry == true)
                {
                    Others objOthersInfo = objOthers.GetInfoByCountryID(Convert.ToInt32(ddlCountry.SelectedValue)).FirstOrDefault();
                    if (objOthersInfo != null)
                    {
                        objOthersInfo.CancellationPolicyId = obj.Id;
                        objOthers.UpdateOthersInfo(objOthersInfo);
                    }
                    else
                    {
                        Others objo = new Others();
                        objo.CountryId = Convert.ToInt32(ddlCountry.SelectedValue);
                        objo.CancellationPolicyId = obj.Id;
                        objOthers.InserOtherInfo(objo);
                    }
                }

                pnlAddModify.Style.Add("display", "none");
                pnlAssignHotel.Style.Add("display", "none");
                HotelAssignTab.Style.Add("display", "none");

                divmsg.Style.Add("display", "block");
                divmsg.Attributes.Add("class", "succesfuly");
                divmsg.InnerHtml = "Policy modified successfully";
                BindPolicylstView();
                ApplyPaging();
            }

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        pnlAddModify.Style.Add("display", "none");
        pnlAssignHotel.Style.Add("display", "none");
        HotelAssignTab.Style.Add("display", "none");
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");
        clearForm();
    }
    protected void btnLanguage_Click(object sender, EventArgs e)
    {
        int PolicyId = Convert.ToInt32(hdnPolicyID.Value);
        if (PolicyId != 0)
        {
            TList<CancelationPolicyLanguage> objCancellationSection = objCancellationManager.GetPolicySectionsByPolicyID(PolicyId);
            long intLanguageID = Convert.ToInt64(hdnLanguageID.Value);
            if (objCancellationSection.Find(a => a.LanguageId == intLanguageID) != null)
            {
                CancelationPolicyLanguage objCancellation = objCancellationSection.Find(a => a.LanguageId == intLanguageID);
                CKEditorSection1.Text = objCancellation.PolicySection1;
                CKEditorSection2.Text = objCancellation.PolicySection2;
                CKEditorSection3.Text = objCancellation.PolicySection3;
                CKEditorSection4.Text = objCancellation.PolicySection4;
            }
            else
            {
                CKEditorSection1.Text = "";
                CKEditorSection2.Text = "";
                CKEditorSection3.Text = "";
                CKEditorSection4.Text = "";

            }
        }

    }
    protected void lnkSaveAssingHotel_Click(object sender, EventArgs e)
    {
        bool result = false;
        objCancellationManager.DeleteMapping(Convert.ToInt32(hdnPolicyID.Value));
        foreach (ListItem obj in chklstHotel.Items)
        {
            if (obj.Selected == true)
            {
                PolicyHotelMapping objMapping = new PolicyHotelMapping();
                objMapping.HotelId = Convert.ToInt64(obj.Value);
                objMapping.PolicyId = Convert.ToInt32(hdnPolicyID.Value);
                objCancellationManager.InsertMapping(objMapping);
                result = true;
            }
        }
        CancelationPolicy objCancellation = objCancellationManager.GetPolicyByID(Convert.ToInt32(hdnPolicyID.Value));

        objCancellation.DefaultCountry = false;
        objCancellationManager.UpdateIsDefault(objCancellation);
        if (result)
        {
            divmsg.Style.Add("display", "block");
            divmsg.Attributes.Add("class", "succesfuly");
            divmsg.InnerHtml = "Policy assigned successfully";
        }
        pnlAddModify.Style.Add("display", "none");
        pnlAssignHotel.Style.Add("display", "none");
        HotelAssignTab.Style.Add("display", "none");
        BindPolicylstView();
    }
    protected void lnkCancelAssingHotel_Click(object sender, EventArgs e)
    {
        pnlAddModify.Style.Add("display", "none");
        pnlAssignHotel.Style.Add("display", "none");
        HotelAssignTab.Style.Add("display", "none");
        clearForm();
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");
    }
    protected void lnkButtonDelete_Click(object sender, EventArgs e)
    {
        try
        {
            int policyId = 0;
            foreach (GridViewRow l in lstViewPolicy.Rows)
            {
                CheckBox r = (CheckBox)l.FindControl("rbselectpolicy");
                if (r.Checked)
                {
                    DataKey currentDataKey = this.lstViewPolicy.DataKeys[l.RowIndex];
                    policyId = Convert.ToInt32(currentDataKey.Value);
                }
            }

            CancelationPolicy obj = objCancellationManager.GetPolicyByID(policyId);
            obj.IsDeleted = true;
            objCancellationManager.DeletePolicy(obj);

            
            BindPolicylstView();
            ApplyPaging();
            divmsg.Style.Add("display", "block");
            divmsg.Attributes.Add("class", "succesfuly");
            divmsg.InnerHtml = "Policy deleted successfully";

            clearForm();
            pnlAddModify.Style.Add("display", "none");
            pnlAssignHotel.Style.Add("display", "none");
            HotelAssignTab.Style.Add("display", "none");
        }
        catch
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "Please try again";
        }
    }

    public void BindCountryDropDown()
    {
        ddlCountry.DataSource = objCancellationManager.GetCountry();
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataValueField = "Id";
        ddlCountry.DataBind();
        ddlCountry.Items.FindByText("Belgium").Selected = true;
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnlAddModify.Style.Add("display", "none");
        HotelAssignTab.Style.Add("display", "none");
        pnlAssignHotel.Style.Add("display", "none");
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");
       
        BindPolicylstView();
        ApplyPaging();
        Session["SelectCountryID"] = ddlCountry.SelectedValue;
    }
    protected void lstViewPolicy_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            lstViewPolicy.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            BindPolicylstView();
            //lstViewPolicy.DataSource = Session["ListOfHotelTemp"];
            //lstViewPolicy.DataBind();
            ApplyPaging();
        }
        catch (Exception ex)
        {

        }
    }
}
