﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="ManageCountryOthers.aspx.cs" Inherits="SuperAdmin_ManageCountry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnCountryID" Value="0" runat="server" />
    <div class="cancelation-heading-body3" id="HotelAssignTab" runat="server">
        <ul>
            <li><a id="liTimeFrame" class="select" href="ManageCountryOthers.aspx">Country</a></li>
            <li><a id="liApplication" href="ApplicationOthers.aspx">Application</a></li>
            <li><a id="liManageLanaguage" href="ManageLanguage.aspx">Manage Language</a></li>
            <li><a id="liSetinvoice" href="SetInvoice.aspx">Set Invoice</a></li>
        </ul>
    </div>
    <table width="100%" border="0" cellspacing="0">
        <tr>
            <td bgcolor="#ffffff" style="padding: 10px;" colspan="2">
                <table width="100%;" style="border-top: 1px red solid; border-top-color: #A3D4F7;">
                    <tr>
                        <td>
                            <h3 style="font-size: 21px; border-bottom: #A3D4F7 solid 1px; font-weight: bold;
                                margin: 0px; padding: 0px 0px 0px 0px;">
                                Manage Others
                            </h3>
                        </td>
                        <td align="right" style="border-bottom: #A3D4F7 solid 1px; margin: 0px; padding: 0px 0px 0px 0px;">
                            <div style="float: right;" id="pageing">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" width="100%">
                            <div id="divmessage" runat="server">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdViewCountryOthers" runat="server" Width="100%" CellPadding="8"
                    border="0" HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                    AutoGenerateColumns="False" GridLines="None" CellSpacing="1" BackColor="#A3D4F7"
                    OnRowCommand="grdViewCountryOthers_RowCommand" OnRowDataBound="grdViewCountryOthers_RowDataBound"
                    DataKeyNames="Id" AllowPaging="true" PageSize="10" OnPageIndexChanging="grdViewCountryOthers_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Country" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <%# Eval("CountryName") %>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <%# Eval("CountryName")%>
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Currency" ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCurrency" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblCurrency" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Interval of % variation" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblIntervalMin" runat="server" Text=""></asp:Label>
                                &nbsp; - &nbsp;
                                <asp:Label ID="lblIntervalMax" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblIntervalMin" runat="server" Text=""></asp:Label>
                                &nbsp; - &nbsp;
                                <asp:Label ID="lblIntervalMax" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minimum % to consider special" ItemStyle-Width="25%"
                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblSpecial" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblSpecial" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkManage" runat="server" Text="Manage others" CommandName="ManageOthers"
                                    CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:LinkButton ID="lnkManage" runat="server" Text="Manage others" CommandName="ManageOthers"
                                    CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <tr>
                            <td colspan="4">
                                <b>No record found</b>
                            </td>
                        </tr>
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <div id="divAddUpdateCountryOthers" runat="server" style="display: none">
        <div id="AddUpdateCountryOthersPopUp-overlay">
        </div>
        <div id="AddUpdateCountryOthersPopUp">
            <div class="popup-top">
            </div>
            <div class="popup-mid">
                <div class="popup-mid-inner">
                    <div class="countrypopup">
                        <table cellspacing="10" width="100%">
                            <tr>
                                <td colspan="2" align="center">
                                    <div class="error" id="errorPopupCountryOthers" runat="server">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="57%">
                                    <b>Choose TAB languages to appear as hotel backend :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:CheckBoxList ID="chkListLanguage" runat="server" RepeatColumns="3">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="57%">
                                    <b>Select Currency :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:DropDownList ID="drpSelectCurrency" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="57%">
                                    <b>Select cancellation policy :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:DropDownList ID="drpCancellationPolicy" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td align="left" width="57%">
                                    <b>Check commision button can wait(days) with no click :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:TextBox ID="txtCheckCommitionButton" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="txtCheckCommitionButton"
                                        ValidChars="0123456789" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="57%">
                                    <b>Set the maximum hotel to be added in request basket :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:TextBox ID="txtRequiredMaximumHotelBasket" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtRequiredMaximumHotelBasket"
                                        ValidChars="0123456789" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="57%">
                                    <b>Define the interval of % variation :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:TextBox ID="txtIntervalVariationMin" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtIntervalVariationMin"
                                        ValidChars="-0123456789" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                    &nbsp; - &nbsp;
                                    <asp:TextBox ID="txtIntervalVariationMax" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtIntervalVariationMax"
                                        ValidChars="-0123456789" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="57%">
                                    <b>Define the minimum % to consider special :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:TextBox ID="txtMinimumSpecial" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="txtMinimumSpecial"
                                        ValidChars="0123456789-." runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="57%">
                                    <b>Define the difference for revenue modification trigger :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:TextBox ID="txtModificationTrigger" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    %
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtModificationTrigger"
                                        ValidChars="0123456789." runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td align="left" width="57%">
                                    <b>Set the invoice commission :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:TextBox ID="txtInvoiceCommissionHotel" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    %
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" TargetControlID="txtInvoiceCommissionHotel"
                                        ValidChars="0123456789." runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td align="left" width="57%">
                                    <b>Set the agency commission to hotel :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:TextBox ID="txtSetAgencyComm" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    %
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" TargetControlID="txtSetAgencyComm"
                                        ValidChars="0123456789." runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="57%">
                                    <b>Set the value of minimum stock on the basis on which an alert would be sent :</b>
                                </td>
                                <td align="left" width="43%">
                                    <asp:TextBox ID="txtMinimumStock" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" TargetControlID="txtMinimumStock"
                                        ValidChars="0123456789" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" width="57%">
                                    <b>Define the values for the combo box question "Where do you hear from us?" :</b>
                                </td>
                                <td align="left" width="43%">
                                    <div id="content" style="float: left; margin-right: 5px">
                                    </div>
                                    <asp:ImageButton ID="lnkAddMore" runat="server" ImageUrl="~/Images/addmore.png" OnClientClick="return addElement();">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="lnkAddRemove" runat="server" ImageUrl="~/Images/removemore.png"
                                        OnClientClick="return removeElement();"></asp:ImageButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <div class="button_section">
                        <asp:LinkButton ID="lnkSaveOthers" runat="server" CssClass="select" OnClick="lnkSaveOthers_Click">Save</asp:LinkButton>
                        &nbsp;or&nbsp;
                        <asp:LinkButton ID="lnkCancelOthers" runat="server" CssClass="cancelpop" OnClick="lnkCancelOthers_Click">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="popup-bottom">
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">

        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });

        function OpenCountryOthersPopUp() {
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#AddUpdateCountryOthersPopUp").show();
            jQuery("#AddUpdateCountryOthersPopUp-overlay").show();
            return false;
        }

        function CloseCountryOthersPopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('AddUpdateCountryOthersPopUp').style.display = 'none';
            document.getElementById('AddUpdateCountryOthersPopUp-overlay').style.display = 'none';

            jQuery("#<%=txtCheckCommitionButton.ClientID %>").val('');
            jQuery("#<%=txtRequiredMaximumHotelBasket.ClientID %>").val('');
            jQuery("#<%=txtMinimumSpecial.ClientID %>").val('');
            jQuery("#<%=txtModificationTrigger.ClientID %>").val('');
            jQuery("#<%=txtInvoiceCommissionHotel.ClientID %>").val('');
            jQuery("#<%=txtSetAgencyComm.ClientID %>").val('');
            jQuery("#<%=txtMinimumStock.ClientID %>").val('');
            jQuery("#<%=txtIntervalVariationMin.ClientID %>").val('');
            jQuery("#<%=txtIntervalVariationMax.ClientID %>").val('');
            return false;
        }



        jQuery(document).ready(function () {
            if (jQuery("#<%= errorPopupCountryOthers.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupCountryOthers.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            jQuery("#<%= errorPopupCountryOthers.ClientID %>").html("");
            jQuery("#<%= errorPopupCountryOthers.ClientID %>").hide();

        });

        var intTextBox = 0;
        function addElement() {
            intTextBox = intTextBox + 1;
            if (intTextBox > 1) {
                jQuery('#<%=lnkAddRemove.ClientID %>').show();
            }
            else {
                jQuery('#<%=lnkAddRemove.ClientID %>').hide();
            }
            var contentID = document.getElementById('content');
            var newTBDiv = document.createElement('div');
            newTBDiv.setAttribute('id', 'strText' + intTextBox);
            newTBDiv.innerHTML = "<input type='text' id='txtQuestion" + intTextBox + "' name='txtQuestion" + intTextBox + "' style='width:270px' onkeyup='javascript:deleteSpecialChar(this)'/></br></br>";
            contentID.appendChild(newTBDiv);
            return false;
        }

        function deleteSpecialChar(txtName) {
            if (txtName.value != '' && txtName.value.match(/^[\w ]+$/) == null) {
                txtName.value = txtName.value.replace(/[\W]/g, '');
            }
        }

        function addElementWithText(question) {

            //var question = question.replace("P~L", "'");
            //question = question.replace("P~R", "'");
            intTextBox = intTextBox + 1;
            if (intTextBox > 1) {
                jQuery('#<%=lnkAddRemove.ClientID %>').show();
            }
            else {
                jQuery('#<%=lnkAddRemove.ClientID %>').hide();
            }
            var contentID = document.getElementById('content');
            var newTBDiv = document.createElement('div');
            newTBDiv.setAttribute('id', 'strText' + intTextBox);
            newTBDiv.innerHTML = "<input type='text' id='txtQuestion" + intTextBox + "' value=\"" + unescape(question) + "\" name='txtQuestion" + intTextBox + "' style='width:270px' onkeyup='javascript:deleteSpecialChar(this)'/></br></br>";
            contentID.appendChild(newTBDiv);
            return false;
        }


        //FUNCTION TO REMOVE TEXT BOX ELEMENT
        function removeElement() {
            if (intTextBox != 0) {
                if (intTextBox == 2) {
                    jQuery('#<%=lnkAddRemove.ClientID %>').hide();
                }
                var contentID = document.getElementById('content');
                contentID.removeChild(document.getElementById('strText' + intTextBox));
                intTextBox = intTextBox - 1;
            }
            return false;
        }


        jQuery("#<%= lnkSaveOthers.ClientID %>").bind("click", function () {
            if (jQuery("#AddUpdateCountryOthersPopUp").hasClass("succesfuly")) {
                jQuery("#AddUpdateCountryOthersPopUp").removeClass("succesfuly").addClass("error").hide();
            }

            var isvalid = true;
            var errormessage = "";

            var cancellationPolicy = jQuery("#<%=drpCancellationPolicy.ClientID %>").val();
            if (cancellationPolicy.length <= 0 || cancellationPolicy == "" || cancellationPolicy == "0") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Please select country default cancellation policy';
                isvalid = false;
            }

//            var CommisionButton = jQuery("#<%=txtCheckCommitionButton.ClientID %>").val();
//            if (CommisionButton.length <= 0 || CommisionButton == "") {
//                if (errormessage.length > 0) {
//                    errormessage += "<br/>"
//                }
//                errormessage += 'Enter value of check commision button';
//                isvalid = false;
//            }

            var requiredbasket = jQuery("#<%= txtRequiredMaximumHotelBasket.ClientID %>").val();
            if (requiredbasket.length <= 0 || requiredbasket == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter maximum number of basket for request';
                isvalid = false;
            }

            var IntervalMin = jQuery("#<%= txtIntervalVariationMin.ClientID %>").val();
            if (IntervalMin.length <= 0 || IntervalMin == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter minimum interval value';
                isvalid = false;
            }
            else if (parseInt(IntervalMin) < -100 || parseInt(IntervalMin) > 100) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Minimum interval value should be between -100 to 100';
                isvalid = false;
            }

            var IntervalMax = jQuery("#<%= txtIntervalVariationMax.ClientID %>").val();
            if (IntervalMax.length <= 0 || IntervalMax == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter maximum interval value';
                isvalid = false;
            }
            else if (parseInt(IntervalMax) < -100 || parseInt(IntervalMax) > 100) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Maximum interval value should be between -100 to 100';
                isvalid = false;
            }

            var IntervalMin = jQuery("#<%= txtIntervalVariationMin.ClientID %>").val();
            var IntervalMax = jQuery("#<%= txtIntervalVariationMax.ClientID %>").val();

            if (parseInt(IntervalMin) > parseInt(IntervalMax)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Minimum value should be less than maximum value';
                isvalid = false;
            }

            var minimumSecial = jQuery("#<%= txtMinimumSpecial.ClientID %>").val();
            if (minimumSecial.length <= 0 || minimumSecial == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter minimum value to consider special';
                isvalid = false;
            }
            else if (isNaN(minimumSecial)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter minimum valid value to consider special';
                isvalid = false;
            }

            var modificationtrigger = jQuery("#<%=txtModificationTrigger.ClientID %>").val();
            if (modificationtrigger.length <= 0 || modificationtrigger == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter value of modification trigger';
                isvalid = false;
            }

            //            var InvoiceCommision = jQuery("#<%=txtInvoiceCommissionHotel.ClientID %>").val();
            //            if (InvoiceCommision.length <= 0 || InvoiceCommision == "") {
            //                if (errormessage.length > 0) {
            //                    errormessage += "<br/>"
            //                }
            //                errormessage += 'Enter invoice commission of hotel';
            //                isvalid = false;
            //            }

            //            var AgencyCommision = jQuery("#<%=txtSetAgencyComm.ClientID %>").val();
            //            if (AgencyCommision.length <= 0 || AgencyCommision == "") {
            //                if (errormessage.length > 0) {
            //                    errormessage += "<br/>"
            //                }
            //                errormessage += 'Enter agency commision value';
            //                isvalid = false;
            //            }

            var minimumStock = jQuery("#<%=txtMinimumStock.ClientID %>").val();
            if (minimumStock.length <= 0 || minimumStock == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter minimum stock value';
                isvalid = false;
            }

            var questioncount = 0;
            jQuery("#content :text").each(function () {
                questioncount = questioncount + 1;
                var txtQuestionVal = jQuery(this).val();
                if (txtQuestionVal.length <= 0 || txtQuestionVal == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += 'Enter the Hear from us question ' + questioncount + '';
                    isvalid = false;
                }
            });


            if (!isvalid) {
                jQuery("#<%= errorPopupCountryOthers.ClientID %>").show();
                jQuery("#<%= errorPopupCountryOthers.ClientID %>").html(errormessage);
                jQuery(".countrypopup").scrollTop(0);
                return false;
            }
            jQuery("#<%= errorPopupCountryOthers.ClientID %>").html("");
            jQuery("#<%= errorPopupCountryOthers.ClientID %>").hide();
            //jQuery("#Loding_overlay").height(jQuery("body").height());
            jQuery("#Loding_overlay").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#AddUpdateCountryOthersPopUp").show();
            jQuery("#AddUpdateCountryOthersPopUp-overlay").show();
        });

        
    </script>
</asp:Content>
