﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Web.Services;

public partial class SuperAdmin_Tableau : System.Web.UI.Page
{
    #region Object
    public CancellationPolicy objCancellationManager = new CancellationPolicy();
    public PackagePricingManager ObjPackagePrice = new PackagePricingManager();
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCountryDropDown();
            GetAllPackageItem();
            BindMeetingRoomHireGrid();
            BindPackageGrid();
            BindFoodFoodBeverageGrid();
            BindEquipmentGrid();
            BindOthersGrid();

        }
    }
    #endregion

    #region Event
    protected void grdViewMeeringRoom_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (ViewState["IsOne"] != null)
            {
                grdViewMeeringRoom.Columns[0].Visible = false;
            }
            PackageItems data = e.Row.DataItem as PackageItems;
            Label lblItme = (Label)e.Row.FindControl("lblItme");
            lblItme.Text = data.ItemName;
            Label lblVat = (Label)e.Row.FindControl("lblVat");
            lblVat.Text = data.Vat.ToString();
            Label lblUseOnSystem = (Label)e.Row.FindControl("lblUseOnSystem");
            if (data.IsActive)
            {
                lblUseOnSystem.Text = "Yes";
            }
            else
            {
                lblUseOnSystem.Text = "No";

            }
            Label lblDescription = (Label)e.Row.FindControl("lblDescription");
            if (data.PackageDescriptionCollection.Find(a => a.LanguageId == 1) != null)
            {
                lblDescription.Text = data.PackageDescriptionCollection.Find(a => a.LanguageId == 1).ItemDescription;
            }
            else
            {
                lblDescription.Text = "No description available.";
            }

        }
    }
    protected void grdPackage_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataKey currentDataKey = this.grdPackage.DataKeys[e.Row.DataItemIndex];
            int intPackageID = Convert.ToInt32(currentDataKey.Value);
            PackageMaster data = e.Row.DataItem as PackageMaster;
            Label lblPackageName = (Label)e.Row.FindControl("lblPackageName");
            lblPackageName.Text = data.PackageName;
            Label lblUseOnSystem = (Label)e.Row.FindControl("lblUseOnSystem");
            bool isActive = Convert.ToBoolean(data.IsActive);
            if (isActive)
            {
                lblUseOnSystem.Text = "Yes";
            }
            else
            {
                lblUseOnSystem.Text = "No";
            }
            TList<PackageItemMapping> ItemWithPackage = ObjPackagePrice.GetPackageItemsByPackageID(intPackageID);

            //if (ItemWithPackage.Count == 0)
            //{
                Label lblVat = (Label)e.Row.FindControl("lblVat");
                lblVat.Text = ObjPackagePrice.GetRoomRentalVatByCountry(Convert.ToInt64(ddlCountry.SelectedValue)).ToString();
                Label lblDescription = (Label)e.Row.FindControl("lblDescription");
                TList<PackageItems> objgetAll = (TList<PackageItems>)Session["AllPackageItem"];
                PackageItems obj3 = objgetAll.Find(a => a.ItemType == "3");
                if (obj3 != null)
                {
                    if (obj3.PackageDescriptionCollection.Find(a => a.LanguageId == 1) != null)
                    {
                        lblDescription.Text = obj3.PackageDescriptionCollection.Find(a => a.LanguageId == 1).ItemDescription;
                    }
                }
            //}

            int intIndex = 0;
            for (intIndex = 0; intIndex <= ItemWithPackage.Count - 1; intIndex++)
            {
                //Label lblVat = (Label)e.Row.FindControl("lblVat");
                //Label lblDescription = (Label)e.Row.FindControl("lblDescription");
                if (lblVat.Text == "" && lblDescription.Text == "")
                {
                    TList<PackageItems> obj = (TList<PackageItems>)Session["AllPackageItem"];
                    PackageItems obj2 = obj.Find(a => a.Id == ItemWithPackage[intIndex].ItemId);

                    if (obj2 != null)
                    {
                        if (obj2.PackageDescriptionCollection.Find(a => a.LanguageId == 1) != null)
                        {
                            lblVat.Text = obj2.Vat.ToString();
                            lblDescription.Text = obj2.PackageDescriptionCollection.Find(a => a.LanguageId == 1).ItemDescription;
                        }
                        else
                        {
                            lblDescription.Text = "Package description not available.";
                        }
                    }
                    else
                    {
                        lblDescription.Text = "Package description not available.";
                    }
                }
                else
                {
                    TList<PackageItems> obj = (TList<PackageItems>)Session["AllPackageItem"];
                    PackageItems obj2 = obj.Find(a => a.Id == ItemWithPackage[intIndex].ItemId);

                    if (obj2 != null)
                    {
                        if (obj2.PackageDescriptionCollection.Find(a => a.LanguageId == 1) != null)
                        {
                            lblVat.Text = lblVat.Text + "</br>" + obj2.Vat.ToString();
                            lblDescription.Text += "</br>" + obj2.PackageDescriptionCollection.Find(a => a.LanguageId == 1).ItemDescription;
                        }
                        else
                        {
                            lblDescription.Text += "</br>" + "Package description not available.";
                        }
                    }

                }
            }

        }
    }
    protected void grdFoodBeverage_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            PackageItems data = e.Row.DataItem as PackageItems;
            Label lblItemName = (Label)e.Row.FindControl("lblItemName");
            lblItemName.Text = data.ItemName;
            Label lblVat = (Label)e.Row.FindControl("lblVat");
            lblVat.Text = data.Vat.ToString();
            Label lblUseOnSystem = (Label)e.Row.FindControl("lblUseOnSystem");
            if (data.IsActive)
            {
                lblUseOnSystem.Text = "Yes";
            }
            else
            {
                lblUseOnSystem.Text = "No";

            }
            Label lblIsExtra = (Label)e.Row.FindControl("lblIsExtra");
            if (data.IsExtra)
            {
                lblIsExtra.Text = "Yes";
            }
            else
            {
                lblIsExtra.Text = "No";

            }
            Label lblDescription = (Label)e.Row.FindControl("lblDescription");
            if (data.PackageDescriptionCollection.Find(a => a.LanguageId == 1) != null)
            {
                lblDescription.Text = data.PackageDescriptionCollection.Find(a => a.LanguageId == 1).ItemDescription;
            }
            else
            {
                lblDescription.Text = "No description available.";
            }

        }
    }
    protected void grdEquipment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            PackageItems data = e.Row.DataItem as PackageItems;
            Label lblItemName = (Label)e.Row.FindControl("lblItemName");
            lblItemName.Text = data.ItemName;
            Label lblVat = (Label)e.Row.FindControl("lblVat");
            lblVat.Text = data.Vat.ToString();
            Label lblUseOnSystem = (Label)e.Row.FindControl("lblUseOnSystem");
            if (data.IsActive)
            {
                lblUseOnSystem.Text = "Yes";
            }
            else
            {
                lblUseOnSystem.Text = "No";

            }
            Label lblIsExtra = (Label)e.Row.FindControl("lblIsExtra");
            if (data.IsExtra)
            {
                lblIsExtra.Text = "Yes";
            }
            else
            {
                lblIsExtra.Text = "No";

            }
            Label lblDescription = (Label)e.Row.FindControl("lblDescription");
            if (data.PackageDescriptionCollection.Find(a => a.LanguageId == 1) != null)
            {
                lblDescription.Text = data.PackageDescriptionCollection.Find(a => a.LanguageId == 1).ItemDescription;
            }
            else
            {
                lblDescription.Text = "No description available.";
            }

        }
    }

    protected void grdOthers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PackageItems data = e.Row.DataItem as PackageItems;
            Label lblItemName = (Label)e.Row.FindControl("lblItemName");
            lblItemName.Text = data.ItemName;
            Label lblVat = (Label)e.Row.FindControl("lblVat");
            lblVat.Text = data.Vat.ToString();
            Label lblUseOnSystem = (Label)e.Row.FindControl("lblUseOnSystem");
            if (data.IsActive)
            {
                lblUseOnSystem.Text = "Yes";
            }
            else
            {
                lblUseOnSystem.Text = "No";

            }
            Label lblIsExtra = (Label)e.Row.FindControl("lblIsExtra");
            if (data.IsExtra)
            {
                lblIsExtra.Text = "Yes";
            }
            else
            {
                lblIsExtra.Text = "No";

            }
            Label lblDescription = (Label)e.Row.FindControl("lblDescription");
            if (data.PackageDescriptionCollection.Find(a => a.LanguageId == 1) != null)
            {
                lblDescription.Text = data.PackageDescriptionCollection.Find(a => a.LanguageId == 1).ItemDescription;
            }
            else
            {
                lblDescription.Text = "No description available.";
            }

        }
    }

    protected void grdFoodBeverage_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Modify")
        {
            trIsExtra.Visible = true;
            chkIsActive.Enabled = true;
            lblPackageLanguageTab.Text = "";
            hdnItemType.Value = "1";
            hdnRecordID.Value = Convert.ToString(e.CommandArgument);
            GetOldInfo(Convert.ToInt64(e.CommandArgument));
            UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableau(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(hdnRecordID.Value), ucLanguageTab);
            DivItemPopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenItemPopUp();", true);
        }
        else if (e.CommandName == "DeleteItem")
        {
            if (ObjPackagePrice.DeleteItem(Convert.ToInt32(e.CommandArgument)))
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('Item deleted successfully');", true);
                GetAllPackageItem();
                BindFoodFoodBeverageGrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('sorry ! this item used in package');", true);
            }
        }
    }
    protected void grdEquipment_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Modify")
        {
            trIsExtra.Visible = true;
            chkIsActive.Enabled = true;
            lblPackageLanguageTab.Text = "";
            hdnItemType.Value = "2";
            hdnRecordID.Value = Convert.ToString(e.CommandArgument);
            GetOldInfo(Convert.ToInt64(e.CommandArgument));
            UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableau(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(hdnRecordID.Value), ucLanguageTab);
            DivItemPopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenItemPopUp();", true);
        }
        else if (e.CommandName == "DeleteItem")
        {
            if (ObjPackagePrice.DeleteItem(Convert.ToInt32(e.CommandArgument)))
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('Item deleted successfully');", true);
                GetAllPackageItem();
                BindEquipmentGrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('sorry ! this item used in package');", true);
            }
        }
    }

    protected void grdOthers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Modify")
        {
            trIsExtra.Visible = true;
            chkIsActive.Enabled = true;
            lblPackageLanguageTab.Text = "";
            hdnItemType.Value = "4";
            hdnRecordID.Value = Convert.ToString(e.CommandArgument);
            GetOldInfo(Convert.ToInt64(e.CommandArgument));
            UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableau(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(hdnRecordID.Value), ucLanguageTab);
            DivItemPopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenItemPopUp();", true);
        }
        else if (e.CommandName == "DeleteItem")
        {
            if (ObjPackagePrice.DeleteItem(Convert.ToInt32(e.CommandArgument)))
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('Item deleted successfully');", true);
                GetAllPackageItem();
                BindOthersGrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('sorry ! this item used in package');", true);
            }
        }

    }
    protected void grdViewMeeringRoom_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Modify")
        {
            trIsExtra.Visible = false;
            chkIsActive.Enabled = false;
            lblPackageLanguageTab.Text = "";
            hdnItemType.Value = "3";
            hdnRecordID.Value = Convert.ToString(e.CommandArgument);
            GetOldInfo(Convert.ToInt64(e.CommandArgument));
            UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableau(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(hdnRecordID.Value), ucLanguageTab);
            DivItemPopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenItemPopUp();", true);
        }
        else if (e.CommandName == "DeleteItem")
        {
            if (ObjPackagePrice.DeleteItem(Convert.ToInt32(e.CommandArgument)))
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('Item deleted successfully');", true);
                GetAllPackageItem();
                BindMeetingRoomHireGrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('sorry ! this item used in package');", true);
            }
        }
    }

    protected void lnkAddpackage_Click(object sender, EventArgs e)
    {
        trItemCheckBoxList.Visible = true;
        var GetLanguage = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");
        if (GetLanguage != null)
        {
            hdnLanguageID.Value = Convert.ToInt32(GetLanguage.Id).ToString();
        }
        else
        {
            hdnLanguageID.Value = "0";
        }
        IsItemChecked.Value = "1";
        hdnRecordID.Value = "0";
        hdnPackageDescriptionID.Value = "0";
        ucLanguageTab.Text = "";
        CheckedMeetingRoomHire();
        int intCountryID = Convert.ToInt32(ddlCountry.SelectedValue);
        UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableauPackage(intCountryID, 0, lblPackageLanguageTab);
        divAddUpdatePackagePopUp.Style.Add("display", "block");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenPackagePopUp();", true);
    }
    protected void lnkAddNewMeetingRoom_Click(object sender, EventArgs e)
    {
        var GetLanguage = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");
        if (GetLanguage != null)
        {
            hdnLanguageID.Value = Convert.ToInt32(GetLanguage.Id).ToString();
        }
        else
        {
            hdnLanguageID.Value = "0";
        }
        trIsExtra.Visible = false;
        chkIsActive.Enabled = false;
        chkIsActive.Checked = true;
        hdnRecordID.Value = "0";
        hdnItmeDescriptionID.Value = "0";
        hdnItemType.Value = "3";
        lblPackageLanguageTab.Text = "";
        int intCountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableau(intCountryID, 0, ucLanguageTab);
        DivItemPopUp.Style.Add("display", "block");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenItemPopUp();", true);
    }
    protected void lnkFoodBeverageItem_Click(object sender, EventArgs e)
    {
        var GetLanguage = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");
        if (GetLanguage != null)
        {
            hdnLanguageID.Value = Convert.ToInt32(GetLanguage.Id).ToString();
        }
        else
        {
            hdnLanguageID.Value = "0";
        }
        trIsExtra.Visible = true;
        chkIsActive.Enabled = true;
        chkIsActive.Checked = true;
        hdnRecordID.Value = "0";
        hdnItmeDescriptionID.Value = "0";
        hdnItemType.Value = "1";
        lblPackageLanguageTab.Text = "";
        int intCountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableau(intCountryID, 0, ucLanguageTab);
        DivItemPopUp.Style.Add("display", "block");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenItemPopUp();", true);
    }
    protected void lnkAddNewEquipmentItem_Click(object sender, EventArgs e)
    {
        var GetLanguage = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");
        if (GetLanguage != null)
        {
            hdnLanguageID.Value = Convert.ToInt32(GetLanguage.Id).ToString();
        }
        else
        {
            hdnLanguageID.Value = "0";
        }
        trIsExtra.Visible = true;
        chkIsActive.Enabled = true;
        chkIsActive.Checked = true;
        hdnRecordID.Value = "0";
        hdnItmeDescriptionID.Value = "0";
        hdnItemType.Value = "2";
        lblPackageLanguageTab.Text = "";
        int intCountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableau(intCountryID, 0, ucLanguageTab);
        DivItemPopUp.Style.Add("display", "block");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenItemPopUp();", true);
    }

    protected void lnkAddNewOtherItem_Click(object sender, EventArgs e)
    {
        var GetLanguage = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");
        if (GetLanguage != null)
        {
            hdnLanguageID.Value = Convert.ToInt32(GetLanguage.Id).ToString();
        }
        else
        {
            hdnLanguageID.Value = "0";
        }
        trIsExtra.Visible = true;
        chkIsActive.Enabled = true;
        chkIsActive.Checked = true;
        hdnRecordID.Value = "0";
        hdnItmeDescriptionID.Value = "0";
        hdnItemType.Value = "4";
        lblPackageLanguageTab.Text = "";
        int intCountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableau(intCountryID, 0, ucLanguageTab);
        DivItemPopUp.Style.Add("display", "block");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenItemPopUp();", true);
    }
    protected void lnkFoodBeverageCnacel_Click(object sender, EventArgs e)
    {
        GetAllPackageItem();
        if (hdnItemType.Value == "1")
        {
            BindFoodFoodBeverageGrid();
            BindPackageGrid();
        }
        else if (hdnItemType.Value == "2")
        {
            BindEquipmentGrid();
            BindPackageGrid();
        }
        else if (hdnItemType.Value == "3")
        {
            BindMeetingRoomHireGrid();
            BindPackageGrid();
        }
        else if (hdnItemType.Value == "4")
        {
            BindOthersGrid();
            BindPackageGrid();
        }


        chkIsActive.Checked = true;
        chkIsExtra.Checked = false;
        hdnItemType.Value = "";
        hdnLanguageID.Value = "";
        hdnRecordID.Value = "";
        hdnItmeDescriptionID.Value = "";

        DivItemPopUp.Style.Add("display", "none");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "CloseItemPopUp();", true);
    }
    protected void lnkCancelPackage_Click(object sender, EventArgs e)
    {
        txtPackageName.Text = "";
        txtPackageDescription.Text = "";
        chkPackageIsActive.Checked = true;
        foreach (ListItem obj in chkListMeetingroom.Items)
        {
            obj.Selected = false;
        }
        foreach (ListItem obj in chkListEquipment.Items)
        {
            obj.Selected = false;
        }
        foreach (ListItem obj in chkListFoodBeverage.Items)
        {
            obj.Selected = false;
        }
        foreach (ListItem obj in chkListOthers.Items)
        {
            obj.Selected = false;
        }
        chkPackageIsActive.Checked = true;
        GetAllPackageItem();
        BindPackageGrid();
        hdnLanguageID.Value = "";
        hdnRecordID.Value = "";
        hdnPackageDescriptionID.Value = "";

        divAddUpdatePackagePopUp.Style.Add("display", "block");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "ClosePopUp", "ClosePackagePopUp();", true);
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCountry.SelectedValue != "0")
        {
            //BindCountryDropDown();
            GetAllPackageItem();
            BindMeetingRoomHireGrid();
            BindPackageGrid();
            BindFoodFoodBeverageGrid();
            BindEquipmentGrid();
            BindOthersGrid();
        }
    }

    #endregion

    #region Function
    void GetOldInfo(long ItemId)
    {
        int languageID = 0;
        var GetLanguage = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");

        if (GetLanguage != null)
        {
            languageID = Convert.ToInt32(GetLanguage.Id);

            hdnLanguageID.Value = GetLanguage.Id.ToString();
        }
        else
        {
            hdnLanguageID.Value = "0";
        }

        //Get item from Item table.
        PackageItems objItem = ObjPackagePrice.GetItemByID(ItemId);
        txtItem.Text = objItem.ItemName;
        txtVat.Text = Convert.ToString(objItem.Vat);

        if (objItem.IsActive)
        {
            chkIsActive.Checked = true;
        }
        else
        {
            chkIsActive.Checked = false;
        }
        if (objItem.IsExtra)
        {
            chkIsExtra.Checked = true;
        }
        else
        {
            chkIsExtra.Checked = false;
        }

        // get item description from PackageDescription Table.
        TList<PackageDescription> objItemDescription = ObjPackagePrice.GetPackageDescriptionByItemID(Convert.ToInt32(ItemId));
        if (objItemDescription.Count > 0)
        {
            if (objItemDescription.Find(a => a.LanguageId == languageID) != null)
            {
                hdnItmeDescriptionID.Value = Convert.ToString(objItemDescription.Find(a => a.LanguageId == languageID).Id);
                txtDescription.Text = objItemDescription.Find(a => a.LanguageId == languageID).ItemDescription;
                hdnLanguageID.Value = objItemDescription.Find(a => a.LanguageId == languageID).LanguageId.ToString();
            }
            else
            {
                hdnItmeDescriptionID.Value = "0";
            }
        }
        else
        {
            hdnItmeDescriptionID.Value = "0";
        }
    }
    void GetAllPackageItem()
    {
        TList<PackageItems> objAllItem = ObjPackagePrice.GetAllPackageItembyCountryid(Convert.ToInt32(ddlCountry.SelectedValue));

        if (objAllItem.FindAll(a => a.IsActive == true).Count > 0)
        {
            lnkAddpackage.Visible = true;
        }
        else
        {
            lnkAddpackage.Visible = false;
        }

        Session["AllPackageItem"] = objAllItem;
    }
    void BindMeetingRoomHireGrid()
    {
        TList<PackageItems> obj = new TList<PackageItems>();
        obj = (TList<PackageItems>)Session["AllPackageItem"];
        obj = obj.FindAll(a => a.ItemType == "3");
        if (obj.Count == 1)
        {
            lnkAddNewMeetingRoom.Visible = false;
            lnkAddpackage.Visible = true;
            ViewState["IsOne"] = "Y";
        }
        else if (obj.Count < 1)
        {
            lnkAddNewMeetingRoom.Visible = true;
        }
        else
        {
            lnkAddpackage.Visible = false;
        }
        grdViewMeeringRoom.DataSource = obj;
        grdViewMeeringRoom.DataBind();

        if (obj.Count > 0)
        {
            //MeetingroomDiv.Visible = true;
        }
        else
        {
            //MeetingroomDiv.Visible = false;
        }

        chkListMeetingroom.DataSource = obj;
        chkListMeetingroom.DataTextField = "ItemName";
        chkListMeetingroom.DataValueField = "Id";
        chkListMeetingroom.DataBind();
    }

    void BindOthersGrid()
    {
        TList<PackageItems> obj = new TList<PackageItems>();
        obj = (TList<PackageItems>)Session["AllPackageItem"];
        obj = obj.FindAll(a => a.ItemType == "4");

        grdOthers.DataSource = obj;
        grdOthers.DataBind();
        obj = obj.FindAll(a => a.IsActive == true && a.ItemType == "4");

        chkListOthers.DataSource = obj;
        chkListOthers.DataTextField = "ItemName";
        chkListOthers.DataValueField = "Id";
        chkListOthers.DataBind();

        if (obj.Count > 0)
        {
            OthersDiv.Visible = true;
        }
        else
        {
            OthersDiv.Visible = false;
        }

    }
    void BindPackageGrid()
    {
        grdPackage.DataSource = ObjPackagePrice.GetAllPackageNameSuperAdmin(Convert.ToInt64(ddlCountry.SelectedValue));
        grdPackage.DataBind();
    }
    void BindFoodFoodBeverageGrid()
    {
        TList<PackageItems> obj = new TList<PackageItems>();
        obj = (TList<PackageItems>)Session["AllPackageItem"];
        grdFoodBeverage.DataSource = obj.FindAll(a => a.ItemType == "1");
        grdFoodBeverage.DataBind();


        obj = (TList<PackageItems>)Session["AllPackageItem"];
        obj = obj.FindAll(a => a.IsActive == true && a.ItemType == "1");

        chkListFoodBeverage.DataSource = obj;
        chkListFoodBeverage.DataTextField = "ItemName";
        chkListFoodBeverage.DataValueField = "Id";
        chkListFoodBeverage.DataBind();

        if (obj.Count > 0)
        {
            FoodBeverageDiv.Visible = true;
        }
        else
        {
            FoodBeverageDiv.Visible = false;
        }
    }
    void BindEquipmentGrid()
    {
        TList<PackageItems> obj = new TList<PackageItems>();
        obj = (TList<PackageItems>)Session["AllPackageItem"];
        grdEquipment.DataSource = obj.FindAll(a => a.ItemType == "2");
        grdEquipment.DataBind();

        obj = (TList<PackageItems>)Session["AllPackageItem"];
        obj = obj.FindAll(a => a.ItemType == "2" && a.IsActive == true);

        chkListEquipment.DataSource = obj.FindAll(a => a.ItemType == "2");
        chkListEquipment.DataTextField = "ItemName";
        chkListEquipment.DataValueField = "Id";
        chkListEquipment.DataBind();

        if (obj.Count > 0)
        {
            EquipmentDiv.Visible = true;
        }
        else
        {
            EquipmentDiv.Visible = false;
        }

    }
    void BindCountryDropDown()
    {
        ddlCountry.DataSource = objCancellationManager.GetCountry();
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataValueField = "Id";
        ddlCountry.DataBind();
        ddlCountry.Items.FindByText("Belgium").Selected = true;
    }
    void CheckedMeetingRoomHire()
    {
        foreach (ListItem obj in chkListMeetingroom.Items)
        {
            //obj.SelectedItem = true;
            obj.Selected = true;
            obj.Enabled = false;

        }
    }

    [WebMethod]
    public static string GetDescription(int intLanguageID, int intItemID)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        // get item description from PackageDescription Table.
        PackagePricingManager ObjPackagePrice = new PackagePricingManager();
        TList<PackageDescription> objItemDescription = ObjPackagePrice.GetPackageDescriptionByItemID(intItemID);
        if (objItemDescription.Count > 0)
        {
            if (objItemDescription.Find(a => a.LanguageId == intLanguageID) != null)
            {
                var data = objItemDescription.Find(a => a.LanguageId == intLanguageID);
                var sendData = new
                {
                    data.Id,
                    data.ItemDescription,
                    data.ItemName
                };

                return serializer.Serialize(sendData);
            }
            else
            {
                return null;
            }
        }
        return null;
    }
    [WebMethod]
    public static string SaveItem(int intLanguageID, int intItemID, int intItemDescriptionID, string strItemName, string strItemDescription, decimal vat, bool boolIsActive, int intcountryID, string intItemType, bool boolIsExtra)
    {
        bool IsSuccessfull = false;
        long GetItemID = intItemID;
        long GetItemDescriptionId = intItemDescriptionID;
        string NeedToSave = string.Empty;
        PackagePricingManager ObjPackagePrice = new PackagePricingManager();
        var verifyEnlish = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");
        if (verifyEnlish != null)
        {
            if (intLanguageID == verifyEnlish.Id && intItemID == 0)
            {
                NeedToSave = "YES";
            }
            else if (intLanguageID != verifyEnlish.Id && intItemID != 0)
            {
                NeedToSave = "YES";
            }
            else if (intLanguageID == verifyEnlish.Id && intItemID != 0)
            {
                NeedToSave = "YES";
            }
            else
            {
                NeedToSave = "NO";
            }
        }
        else
        {
            return "Sorry ! Please contact to technical support";
        }

        if (NeedToSave == "YES")
        {
            PackageItems objItmes = new PackageItems();
            objItmes.Id = intItemID;
            objItmes.ItemType = intItemType;
            if (intLanguageID == verifyEnlish.Id)
            {
                objItmes.ItemName = strItemName.Replace("R~D", "'").Replace("R~L", "'");
            }
            else
            {
                PackageItems obj = ObjPackagePrice.GetItemByID(intItemID);
                objItmes.ItemName = obj.ItemName;
            }
            objItmes.CountryId = intcountryID;
            objItmes.IsActive = boolIsActive;
            objItmes.IsExtra = boolIsExtra;
            objItmes.Vat = vat;

            PackageDescription objItmeDescription = new PackageDescription();
            objItmeDescription.Id = intItemDescriptionID;
            objItmeDescription.ItemDescription = strItemDescription.Replace("R~D", "'").Replace("R~L", "'");
            objItmeDescription.ItemName = strItemName.Replace("R~D", "'").Replace("R~L", "'");
            objItmeDescription.LanguageId = intLanguageID;

            if (intItemID == 0 && intItemDescriptionID == 0)
            {
                IsSuccessfull = ObjPackagePrice.InsertItem(objItmes, objItmeDescription);
                GetItemID = objItmes.Id;
                GetItemDescriptionId = objItmeDescription.Id;
            }
            else if (intItemID != 0 && intItemDescriptionID != 0)
            {
                IsSuccessfull = ObjPackagePrice.UpdateItem(objItmes, objItmeDescription);
            }
            else if (intItemID != 0 && intItemDescriptionID == 0)
            {
                IsSuccessfull = ObjPackagePrice.InsertItemDescription(objItmes, objItmeDescription);
            }

            if (IsSuccessfull)
            {
                return "Saved Successfully," + GetItemID + "," + GetItemDescriptionId + "";
            }
            else
            {
                return "Please contact to admin";
            }
        }
        else
        {
            return "Please insert in english first";
        }

    }

    [WebMethod]
    public static string SavePackage(int intLanguageID, int intPackageID, int intPackageDescriptionID, string strPackageName, string strPackageDescription, bool boolIsActive, int intcountryID)
    {
        bool IsSuccessfull = false;
        long GetPackageID = intPackageID;
        long getPackageDescriptionID = intPackageDescriptionID;
        string NeedToSave = string.Empty;
        PackagePricingManager ObjPackagePrice = new PackagePricingManager();
        var verifyEnlish = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");
        if (verifyEnlish != null)
        {
            if (intLanguageID == verifyEnlish.Id && intPackageID == 0)
            {
                NeedToSave = "YES";
            }
            else if (intLanguageID != verifyEnlish.Id && intPackageID != 0)
            {
                NeedToSave = "YES";
            }
            else if (intLanguageID == verifyEnlish.Id && intPackageID != 0)
            {
                NeedToSave = "YES";
            }
            else
            {
                NeedToSave = "NO";
            }
        }
        else
        {
            return "Sorry ! Please contact to technical support";
        }
        if (NeedToSave == "YES")
        {
            PackageMaster objPackage = new PackageMaster();
            objPackage.Id = intPackageID;
            objPackage.PackageName = strPackageName.Replace("R~D", "'").Replace("R~L", "'");
            objPackage.CountryId = intcountryID;
            objPackage.IsActive = boolIsActive;

            PackageMasterDescription objPackageDescription = new PackageMasterDescription();
            objPackageDescription.Id = intPackageDescriptionID;
            objPackageDescription.Description = strPackageDescription.Replace("R~D", "'").Replace("R~L", "'");
            objPackageDescription.LanguageId = intLanguageID;

            if (intPackageID == 0 && intPackageDescriptionID == 0)
            {
                IsSuccessfull = ObjPackagePrice.InsertPackage(objPackage, objPackageDescription);
                GetPackageID = objPackage.Id;
                getPackageDescriptionID = objPackageDescription.Id;

            }
            else if (intPackageID != 0 && intPackageDescriptionID != 0)
            {
                IsSuccessfull = ObjPackagePrice.UpdatePackage(objPackage, objPackageDescription);
            }
            else if (intPackageID != 0 && intPackageDescriptionID == 0)
            {
                IsSuccessfull = ObjPackagePrice.InsertPackageDescription(objPackage, objPackageDescription);
            }

            if (IsSuccessfull)
            {
                return "Saved Successfully," + GetPackageID + "," + getPackageDescriptionID + "";
            }
            else
            {
                return "Please contact to admin";
            }
        }
        else
        {
            return "Please insert in english first";
        }

    }

    [WebMethod]
    public static string SavePackageItemMapping(int intPackageID, int intItemID)
    {
        PackagePricingManager ObjPackagePrice = new PackagePricingManager();
        PackageItemMapping obj = new PackageItemMapping();
        obj.PackageId = intPackageID;
        obj.ItemId = intItemID;
        if (ObjPackagePrice.MappingPackgeWithItem(obj))
        {
            return "Saved";
        }
        else
        {
            return "Not Saved";
        }
    }

    [WebMethod]
    public static string CleanOldMapping(int intPackageID)
    {
        bool isdone = false;
        PackagePricingManager ObjPackagePrice = new PackagePricingManager();
        isdone = ObjPackagePrice.DeletePackageItemMapping(intPackageID);
        if (isdone)
        {
            return "Deleted";
        }
        else
        {
            return "NotDeleted";
        }

    }
    #endregion

    protected void grdPackage_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Modify")
        {
            trItemCheckBoxList.Visible = false;
            IsItemChecked.Value = "0";
            ucLanguageTab.Text = "";
            hdnRecordID.Value = Convert.ToString(e.CommandArgument);
            GetPackageOldInfo(Convert.ToInt64(e.CommandArgument));
            CheckedMeetingRoomHire();
            UserControl_SuperAdmin_LanguageTab.CreateLanguageTabForTableauPackage(Convert.ToInt32(ddlCountry.SelectedValue), Convert.ToInt32(hdnRecordID.Value), lblPackageLanguageTab);
            divAddUpdatePackagePopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenPackagePopUp();", true);
        }
        else if (e.CommandName == "DeletePackage")
        {
            ObjPackagePrice.DeletePackage(Convert.ToInt32(e.CommandArgument));
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "alert('Package deleted successfully');", true);
            GetAllPackageItem();
            BindPackageGrid();
        }
    }

    /// <summary>
    /// This function used for get old information of package.
    /// </summary>
    /// <param name="intPackageID"></param>
    void GetPackageOldInfo(long intPackageID)
    {
        int languageID = 0;
        var GetLanguage = ObjPackagePrice.GetAllRequiredLanguage().Find(a => a.Name == "English");

        if (GetLanguage != null)
        {
            languageID = Convert.ToInt32(GetLanguage.Id);

            hdnLanguageID.Value = GetLanguage.Id.ToString();
        }
        else
        {
            hdnLanguageID.Value = "0";
        }
        //Get item from Item table.
        PackageMaster objPackageMaster = ObjPackagePrice.GetPackageByID(intPackageID);
        txtPackageName.Text = objPackageMaster.PackageName;

        if (Convert.ToBoolean(objPackageMaster.IsActive))
        {
            chkPackageIsActive.Checked = true;
        }
        else
        {
            chkPackageIsActive.Checked = false;
        }

        TList<PackageItemMapping> objMapping = ObjPackagePrice.GetPackgeItemByPackgeID(intPackageID);
        if (objMapping.Count > 0)
        {
            int intIndex = 0;
            for (intIndex = 0; intIndex <= objMapping.Count - 1; intIndex++)
            {
                foreach (ListItem obj in chkListMeetingroom.Items)
                {
                    if (obj.Value == objMapping[intIndex].ItemId.ToString())
                    {
                        obj.Selected = true;
                    }
                }
                foreach (ListItem obj in chkListEquipment.Items)
                {
                    if (obj.Value == objMapping[intIndex].ItemId.ToString())
                    {
                        obj.Selected = true;
                    }
                }
                foreach (ListItem obj in chkListFoodBeverage.Items)
                {
                    if (obj.Value == objMapping[intIndex].ItemId.ToString())
                    {
                        obj.Selected = true;
                    }
                }

            }

        }

        // get item description from PackageDescription Table.
        TList<PackageMasterDescription> objDescription = ObjPackagePrice.GetPackageMasterDescription(Convert.ToInt32(intPackageID));
        if (objDescription.Count > 0)
        {
            if (objDescription.Find(a => a.LanguageId == languageID) != null)
            {
                hdnPackageDescriptionID.Value = Convert.ToString(objDescription.Find(a => a.LanguageId == languageID).Id);
                txtPackageDescription.Text = objDescription.Find(a => a.LanguageId == languageID).Description;
                hdnLanguageID.Value = objDescription.Find(a => a.LanguageId == languageID).LanguageId.ToString();
            }
            else
            {
                hdnPackageDescriptionID.Value = "0";
            }
        }
        else
        {

            hdnPackageDescriptionID.Value = "0";
        }

    }

    [WebMethod]
    public static string IsMappingDone()
    {
        return "Saved";
    }

    [WebMethod]
    public static string GetPackageDescription(int intLanguageID, int intPackageID)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        // get item description from PackageDescription Table.
        PackagePricingManager ObjPackagePrice = new PackagePricingManager();
        TList<PackageMasterDescription> objDescription = ObjPackagePrice.GetPackageMasterDescription(intPackageID);
        if (objDescription.Count > 0)
        {
            if (objDescription.Find(a => a.LanguageId == intLanguageID) != null)
            {
                var data = objDescription.Find(a => a.LanguageId == intLanguageID);
                var sendData = new
                {
                    data.Id,
                    data.Description
                };

                return serializer.Serialize(sendData);
            }
            else
            {
                return null;
            }
        }
        return null;
    }


}