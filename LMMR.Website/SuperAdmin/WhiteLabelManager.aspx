﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="WhiteLabelManager.aspx.cs" Inherits="SuperAdmin_WhiteLabelManager" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnRecordID" Value="0" runat="server" />
    <asp:HiddenField ID="IsItemChecked" Value="0" runat="server" />
    <table width="100%" border="0" cellspacing="0">
        <tr>
            <td>
                <div id="divmsg" runat="server" class="error" style="display: none">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%;" style="background: #ffffff; border: 1px #A3D4F7 solid;">
                    <tr>
                        <td>
                            <div style="margin-right: 10px; float: left">
                                <div class="n-btn1">
                                    <asp:LinkButton ID="lnkButtonAddnew" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnkButtonAddnew_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Add new</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div style="margin-right: 10px; float: left;" runat="server" id="ModifyButton">
                                <div class="n-btn1">
                                    <asp:LinkButton ID="lnkButtonModify" OnClick="lnkButtonModify_Click" runat="server"
                                        CssClass="register-hote2-btn">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Modify</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div style="margin-right: 10px; float: left;" runat="server" id="DeleteButton">
                                <div class="n-btn1" style="margin-right: 10px;">
                                    <asp:LinkButton ID="lnkButtonDelete" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnkButtonDelete_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Deactivate</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </td>
                        <td align="right">
                            <div style="float: right;" id="pageing">
                            </div>
                        </td>
                    </tr>
                </table>
                <div id="DivgrdWhiteLabel">
                    <asp:GridView ID="grdWhiteLabel" runat="server" Width="100%" CellPadding="8" border="0"
                        HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                        AutoGenerateColumns="False" GridLines="None" CellSpacing="1" BackColor="#A3D4F7"
                        DataKeyNames="Id" PageSize="10" AllowPaging="true" OnRowCommand="grdWhiteLabel_RowCommand"
                        OnRowDataBound="grdWhiteLabel_RowDataBound" 
                        onpageindexchanging="grdWhiteLabel_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-Width="1%">
                                <ItemTemplate>
                                    <asp:CheckBox ID="rbselectpolicy" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:CheckBox ID="rbselectpolicy" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" ItemStyle-Width="14%" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Target URL" ItemStyle-Width="32%" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblTargetUrl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblTargetUrl" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Commission" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblCommission" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblCommission" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IsActive" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActive" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblIsActive" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="See Hotel" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSee" runat="server" Text="See Hotel"></asp:LinkButton>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:LinkButton ID="lnkSee" runat="server" Text="See Hotel"></asp:LinkButton>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Generated Code" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--<a id="lblDownloadCode" runat="server">Download</a>--%>
                                    <asp:HyperLink ID="lblDownloadCode" runat="server" Text="Download"></asp:HyperLink>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <%--<a id="lblDownloadCode" runat="server">Download</a>--%>
                                    <asp:HyperLink ID="lblDownloadCode" runat="server" Text="Download"></asp:HyperLink>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-Width="9%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <div style="margin-right: 10px; float: left">
                                        <div class="n-btn1">
                                            <asp:LinkButton ID="lnkSendCond" runat="server" CssClass="register-hote2-btn">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Send</div>
                        <div class="n-btn-right">
                        </div>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <div style="margin-right: 10px; float: left">
                                        <div class="n-btn1">
                                            <asp:LinkButton ID="lnkSendCond" runat="server" CssClass="register-hote2-btn">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Send</div>
                        <div class="n-btn-right">
                        </div>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <center>
                                <b>No record found</b></span></center>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                        <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                        <PagerTemplate>
                            <div id="Paging" style="width: 100%; display: none;">
                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </div>
                        </PagerTemplate>
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <div class="superadmin-example-mainbody" id="DivAddUpdateWL" runat="server" visible="false">
        <div id="divmessageWL" runat="server" class="error" style="display: none">
        </div>
        <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
            <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                <h2>
                    <asp:Label ID="lblHeader" runat="server" Text="Create Witel Label"></asp:Label></h2>
            </div>
        </div>
        <table style="margin-left: -1px;" width="100%" bgcolor="#92bddd" cellspacing="1"
            cellpadding="0">
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>WL ID:</strong>
                </td>
                <td>
                    <asp:Label ID="lblWLGeneratedNumber" runat="server" MaxLength="25" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Name:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtWLName" runat="server" MaxLength="25" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Target URL:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtTargetURL" runat="server" MaxLength="200" TabIndex="1" Width="300px"
                        CssClass="txtpadding" />
                    <span style="font-family: Arial; font-size: Smaller;">Example: http://www.mysite.com</span>
                </td>
            </tr>
            <tr bgcolor="#F1F8F8" id="trHotel">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                </td>
                <td>
                    <div style="overflow-x: hidden; overflow-y: auto; height: 380px;">
                        <asp:CheckBoxList ID="chkHotelList" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                            onclick="IsItemCheckedByCheckBox();">
                        </asp:CheckBoxList>
                    </div>
                </td>
            </tr>
            <tr bgcolor="#E3F0F1" style="display: none" id="trGroup">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                </td>
                <td>
                    <asp:CheckBoxList ID="chkBoxListGroup" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                        onclick="IsItemCheckedByCheckBox();">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Commission:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtCommission" runat="server" MaxLength="5" TabIndex="1" Width="50px"
                        CssClass="txtpadding" />
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtCommission"
                        ValidChars="0123456789." runat="server">
                    </asp:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Target Site Logo:</strong>
                </td>
                <td>
                    <asp:FileUpload ID="UploadLogo" runat="server" CssClass="txtpadding" />
                    <span style="font-family: Arial; font-size: Smaller;">Logo Size: width 155px, height
                        109px.Logo image should be transparent. </span>
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Is Active:</strong>
                </td>
                <td>
                    <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="txtpadding" />
                </td>
            </tr>
        </table>
        <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
            padding-bottom: 10px;">
            <div class="button_section">
                <asp:LinkButton ID="lnkSave" runat="server" CssClass="select" OnClick="lnkSave_Click">Save</asp:LinkButton>
                &nbsp;or&nbsp;
                <asp:LinkButton ID="lnkCancel" runat="server" CssClass="cancelpop" OnClick="lnkCancel_Click">Cancel</asp:LinkButton>
            </div>
        </div>
    </div>
    <div id="divAddUpdatePackagePopUp" runat="server" style="display: none">
        <div id="AddUpdatePackagePopUp-overlay">
        </div>
        <div id="AddUpdatePackagePopUp">
            <div class="popup-top">
            </div>
            <div class="popup-mid">
                <div style="position: absolute; right: 4px; top: 5px;">
                    <asp:ImageButton ID="imgBtnCloaseContactPopUp" ImageUrl="~/Images/close-black.png"
                        runat="server" OnClientClick="return ClosePopUp();" />
                </div>
                <div class="popup-mid-inner">
                    <table cellspacing="10" width="100%">
                        <tr>
                            <td>
                                <div style="overflow-x: hidden; overflow-y: auto; height: auto; max-height: 450px;">
                                    <asp:DataList ID="dlViewHotelGroup" ItemStyle-Width="300" ItemStyle-Height="20px"
                                        runat="server" OnItemDataBound="dlViewHotelGroup_ItemBound" RepeatColumns="3"
                                        RepeatDirection="Vertical">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="popup-bottom">
            </div>
        </div>
    </div>
    <div id="subscribe-overlay">
    </div>
    <div id="divPopUp" runat="server" style="display: none">
        <div id="Contactpopup-overlay">
        </div>
        <div id="Contactpopup">
            <div class="popup-top">
            </div>
            <div class="popup-mid">
                <div class="popup-mid-inner">
                    <div class="error" id="popupMailMsg" runat="server" style="width: 87%;">
                    </div>
                    <table cellspacing="10">
                        <tr>
                            <td>
                                <b>Email :</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEMail" runat="server" Width="250px" Text=""></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="button_section">
                        <asp:LinkButton ID="btnSandMail" runat="server" CssClass="select" OnClick="btnSandMail_click"
                            OnClientClick="return ValidateSendMail();">Send</asp:LinkButton>
                        &nbsp;or&nbsp;
                        <asp:LinkButton ID="btnCancelMail" runat="server" CssClass="cancelpop" OnClientClick="return CloseMailPopUp();">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="popup-bottom">
            </div>
        </div>
    </div>
    <script type="text/javascript" language="javascript">

        function SetFocusBottom(val) {

            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
        }

        function validateEmail(email) {
            var a = email;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }

        }

        function HideLoading() {

            jQuery("#Loding_overlay").hide();
        }

        function CheckOtherIsCheckedByGVID(spanChk) {
            var IsChecked = spanChk.checked;

            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById('ContentPlaceHolder1_grdWhiteLabel');
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {

                if (items[i].id != CurrentRdbID && items[i].type == "checkbox") {

                    if (items[i].checked) {

                        items[i].checked = false;

                        items[i].parentElement.parentElement.style.backgroundColor = '';

                        //items[i].parentElement.parentElement.style.color = 'black';
                    }
                    else {

                        items[i].parentElement.parentElement.style.backgroundColor = '';
                    }
                }
                else {

                    items[i].parentElement.parentElement.style.backgroundColor = '#feff99';
                }
            }
            if (IsChecked != true) {
                spanChk.parentElement.parentElement.style.backgroundColor = '';
            }
        }

        function OpenPopUp() {
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#AddUpdatePackagePopUp").show();
            jQuery("#AddUpdatePackagePopUp-overlay").show();
            return false;
        }

        function ClosePopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('AddUpdatePackagePopUp').style.display = 'none';
            document.getElementById('AddUpdatePackagePopUp-overlay').style.display = 'none';
            return false;
        }


        jQuery("#<%= lnkSave.ClientID %>").bind("click", function () {
            if (jQuery("#<%= divmessageWL.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessageWL.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";

            var WLName = jQuery("#<%= txtWLName.ClientID %>").val();
            if (WLName.length <= 0 || WLName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter white label name';
                isvalid = false;
            }

            var targetUrl = jQuery("#<%= txtTargetURL.ClientID %>").val();
            if (targetUrl.length <= 0 || targetUrl == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter target url';
                isvalid = false;
            }
            else {
                if (/^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(targetUrl)) {

                } else {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += 'Enter valid url.';
                    isvalid = false;
                }
            }

            var ischekced = "0";

            jQuery("#<%=chkHotelList.ClientID %>").find("input:checkbox:checked").each(function () {

                ischekced = "1";
            });
            if (ischekced == "0") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Select at least one hotel / group";
                isvalid = false;
            }

            var Commission = jQuery("#<%= txtCommission.ClientID %>").val();
            if (Commission.length <= 0 || Commission == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter Commission';
                isvalid = false;
            }
            else if (isNaN(Commission)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Please enter valid Commission value';
                isvalid = false;

            }

            if (jQuery("#<%=hdnRecordID.ClientID %>").val() == "0") {
                var file = jQuery('#<%= UploadLogo.ClientID %>').val();
                if (file.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Upload target site logo";
                    isvalid = false;
                }
                else {
                    if (!validate_file_format(file, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Please select file in (Gif, JPG, JPEG and Png) formats only.";
                        isvalid = false;
                    }
                }
            }

            if (!isvalid) {
                jQuery("#<%=divmessageWL.ClientID %>").show();
                jQuery("#<%=divmessageWL.ClientID %>").html(errormessage);
                var offseterror = jQuery("#<%=divmessageWL.ClientID %>").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }

            jQuery("#<%=divmessageWL.ClientID %>").hide();
            jQuery("#<%=divmessageWL.ClientID %>").html('');
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            jQuery("#Loding_overlaySec span").html("Saving...");
            jQuery("#Loding_overlaySec").show();
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';

        });

        function IsItemCheckedByCheckBox() {
            jQuery("#<%=IsItemChecked.ClientID %>").val('1');
        }

        jQuery(document).ready(function () {
            jQuery("#<%= lnkButtonModify.ClientID %>").bind("click", function () {
                if (jQuery("#ContentPlaceHolder1_grdWhiteLabel").find("input:checkbox:checked").length <= 0) {
                    alert('Please select white label to modify.');
                    return false;
                }
            });

            jQuery("#<%= lnkButtonDelete.ClientID %>").bind("click", function () {
                if (jQuery("#ContentPlaceHolder1_grdWhiteLabel").find("input:checkbox:checked").length <= 0) {
                    alert('Please select white label to deactivate.');
                    return false;
                }
                else {
                    return confirm("Are you sure you want to deactivate it?");
                }
            });
        });

        function validate_file_format(file_name, allowed_ext) {
            field_value = file_name;
            if (field_value != "") {
                var file_ext = (field_value.substring((field_value.lastIndexOf('.') + 1)).toLowerCase());
                ext = allowed_ext.split(',');
                var allow = 0;
                for (var i = 0; i < ext.length; i++) {
                    if (ext[i] == file_ext) {
                        allow = 1;
                    }
                }
                if (!allow) {
                    return false;
                }
                else {
                    return true;
                }
            }
            return false;
        }

        function OpenMailPopUp() {
            jQuery("#<%=popupMailMsg.ClientID %>").hide();
            jQuery("#<%=txtEMail.ClientID %>").val('');
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#Contactpopup").show();
            jQuery("#Contactpopup-overlay").show();
            return false;
        }

        function CloseMailPopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('Contactpopup').style.display = 'none';
            document.getElementById('Contactpopup-overlay').style.display = 'none';
            return false;
        }

        function ValidateSendMail() {
            if (jQuery("#<%= popupMailMsg.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= popupMailMsg.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var WLEmail = jQuery("#<%= txtEMail.ClientID %>").val();
            if (WLEmail.length <= 0 || WLEmail == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter email';
                isvalid = false;
            }
            else {
                if (!validateEmail(WLEmail)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += 'Enter valid email';
                    isvalid = false;
                }
            }

            if (!isvalid) {
                jQuery("#<%=popupMailMsg.ClientID %>").show();
                jQuery("#<%=popupMailMsg.ClientID %>").html(errormessage);
                return false;
            }


        }

        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });


    </script>
</asp:Content>
