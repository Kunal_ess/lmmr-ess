﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" ValidateRequest="false"
    AutoEventWireup="true" CodeFile="StaticPagesContents.aspx.cs" Inherits="SuperAdmin_StaticPagesContents" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="../UserControl/SuperAdmin/LanaguageTabWithoutCountry.ascx" TagName="LanaguageTabWithoutCountry"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnLanguageID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnCmsId" runat="server" />
    <asp:HiddenField ID="hdnHeading" runat="server" />
    <asp:HiddenField ID="hdnRecordID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPolicyID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnContentTitle" runat="server" Value="0" />
    <asp:HiddenField ID="hdnUniqueId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnLinkMedia" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTask" runat="server" Value="New" />
    <asp:Button ID="btnLanguage" runat="server" Text="Button" OnClick="btnLanguage_Click"
        Style="display: none" />
    <div class="main_container1">
        <!--superadmin-example-layout start -->
        <div class="superadmin-example-layout">
            <div class="superadmin-cms">
            <div id="divmessage" runat="server" style="margin-bottom: 10px; width: 913px;">
        </div>
                <div class="superadmin-cms-box1" style="min-height: 220px;">
                    <h3>
                        Front end</h3>
                    <ul>
                        <li>
                            <asp:LinkButton ID="lbtAboutUs" runat="server" OnClick="lbtAboutUs_Click">Your advantages</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtWhyBookOnline" runat="server" OnClick="lbtWhyBookOnline_Click">How to book online</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtLegalAspects" runat="server" OnClick="lbtLegalAspects_Click">Legal aspects</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtContactUs" runat="server" OnClick="lbtContactUs_Click">Contact us</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtWhySendRequest" runat="server" OnClick="lbtWhySendRequest_Click">Send now A REQUEST</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtTermsNConditions" runat="server" OnClick="lbtTermsNConditions_Click">Terms & conditions</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtJoinToday" runat="server" OnClick="lbtJoinToday_Click">Join today</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtCompanyInfo" runat="server" OnClick="lbtCompanyInfo_Click">Company information</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtHotelsOfWeek" runat="server" OnClick="lbtHotelsOfWeek_Click">Hotels of the week</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtForInvestors" runat="server" OnClick="lbtForInvestors_Click">For investors</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtPrivacyStatement" runat="server" OnClick="lbtPrivacyStatement_Click">Privacy statement</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtImageGalleryFrontPage" runat="server" OnClick="lbtImageGalleryFrontPage_Click">Image gallery front page</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtNews" runat="server" OnClick="lbtNews_Click">News</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtBenefits" runat="server" OnClick="lbtBenefits_Click">Benefits</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtSocialLinks" runat="server" OnClick="lbtSocialLinks_Click">Social networking</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtFrontEndBottomLink" runat="server" OnClick="lbtFrontEndBottomLink_Click">Front end bottom part</asp:LinkButton></li>
                            <li >
                            <asp:LinkButton ID="lnkStaticPages" runat="server" OnClick="lnkStaticPages_Click">More Information</asp:LinkButton></li>
                            <li>
                            <asp:LinkButton ID="lbtBookingAdvantage" runat="server" OnClick="lbtBookingAdvantage_Click">Booking advantage</asp:LinkButton></li>
                            <li>
                            <asp:LinkButton ID="lbtRequestAdvantage" runat="server" OnClick="lbtRequestAdvantage_Click">Request advantage</asp:LinkButton></li>
                    </ul>
                </div>
                <div class="superadmin-cms-box2">
                    <h3>
                        Backend for hotel</h3>
                    <ul>
                        <li>
                            <asp:LinkButton ID="lbtNewsBanner" runat="server" OnClick="lbtNewsBanner_Click">News banner</asp:LinkButton></li>
                    </ul>
                </div>
                <div class="superadmin-cms-box3">
                    <h3>
                        Backend for agencies
                    </h3>
                    <ul><li><asp:LinkButton ID="lnkAgencyImage" runat="server" OnClick="lbtAgencyImage_Click">Control panel top image</asp:LinkButton></li></ul>
                </div>
                <div class="superadmin-cms-box4">
                    <h3>
                        Backend for user</h3>
                    <ul>
                        <li>
                            <asp:LinkButton ID="lbtPersonalizedMessage" runat="server" OnClick="lbtPersonalizedMessage_Click">Personalised message</asp:LinkButton></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- superadmin-example-layout end-->
        <!--superadmin-example-layout start -->
        <div class="superadmin-example-mainbody" id="topTitleDIV" runat="server" visible="false" style="width: 979px;">
            <div class="superadmin-mainbody-sub1" id="titleHeadingDiv" runat="server" visible="false">
                <div class="superadmin-mainbody-sub1-left" >
                    <%= heading %>&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtPartners" runat="server"
                        Visible="false" OnClick="lbtPartners_Click">Partners</asp:LinkButton>
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtClients" runat="server" Visible="false"
                        OnClick="lbtClients_Click">Clients</asp:LinkButton>
                </div>
            </div>
            <div class="superadmin-tabbody1">
                
                <div class="pageing-operator" style="border: none;" runat="server" id="pagingDIV" visible="false">
                <div class="pageing-operator-left" id="divAdd" runat="server">
                <asp:LinkButton ID='lbtAddNewHotel' runat='server' CssClass='Add-contract-btn' Text='Add new hotel' OnClick='lbtAddNewHotel_Click' Visible="false" Width="135px" />
                <asp:LinkButton ID='lbtAddNewImage' runat='server' CssClass='Add-contract-btn'  OnClick='lbtAddNewImage_Click' Text='Add new image' Visible="false" Width="135px" />
                <asp:LinkButton ID='lbtAddNewNews' runat='server' CssClass='Add-contract-btn'  OnClick='lbtAddNewNews_Click' Text='Add new news' Visible="false" Width="135px"/>
                <asp:LinkButton ID='lbtAddBenefit' runat='server' CssClass='Add-contract-btn'  OnClick='lbtAddBenefit_Click' Text='Add new benefit' Visible="false" Width="135px"/>
                <asp:LinkButton ID='lbtAddNewBottomLink' runat='server' CssClass='Add-contract-btn'  OnClick='lbtAddNewBottomLink_Click' Text='Add new link' Visible="false" Width="135px"/>
                </div>
                <div style="float: left; width: 55%" id="pageing">
                </div>
            </div>
            <div id="frontStaticPages" runat="server" visible="false">
                    <asp:Repeater ID="rptFrontStaticPage" runat="server" 
                        onitemcommand="rptFrontStaticPage_ItemCommand" 
                        onitemdatabound="rptFrontStaticPage_ItemDataBound">
                        <HeaderTemplate>
                            <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="heading-earned-row" align="left"><strong>Page Name</strong></td>
                                <td class="heading-earned-row" align="center"><strong>Edit</strong></td>                                
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="con-earned" align="left"><asp:Label ID="lblPageName" runat="server"></asp:Label></td>
                                <td class="con-earned" align="center" ><asp:LinkButton ID="lnkEdit" runat="server" Text="Modify" CommandName="Modify" ></asp:LinkButton></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div id="gridHotelsOfTheWeek" runat="server" visible="false">
                    <asp:GridView ID="grvHotelsOfTheWeek" runat="server" ShowHeader="true" ShowHeaderWhenEmpty="true" AllowPaging="true" PageSize="10"
                        AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" OnPageIndexChanging="grvHotelsOfTheWeek_PageIndexChanging"
                        CellPadding="0" Width="100%" GridLines="None" CellSpacing="1" OnRowDataBound="grvHotelsOfTheWeek_RowDataBound"
                        OnRowCommand="grvHotelsOfTheWeek_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblSelect" runat="server" Text="Select"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtEdit" runat="server" Text="Modify" ToolTip='<%# Eval("Id") %>'
                                        OnClick="lbtEdit_Click"></asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblTitleBody" runat="server" Text='<%# Eval("ContentTitle") %>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" HorizontalAlign="Left" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescriptionBody" runat="server" Text='<%# Eval("ContentsDesc") %>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblImage" runat="server" Text="Image"></asp:Label></HeaderTemplate>
                                <ItemStyle CssClass="con1-earned" />
                                <ItemTemplate>
                                    <asp:Image ID="imgHotel" runat="server" Style="width: 140px; height: 100px;" /></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblFromDateText" runat="server" Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("FromDate"))%>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblToDateText" runat="server" Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("ToDate"))%>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblDelete" runat="server" Text="Delete"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtDelete" runat="server" OnClientClick="return confirm('Do you really want to delete ?');"
                                        OnClick="lbtDelete_Click" ToolTip='<%# Eval("Id") %>'>Delete</asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblActivate" runat="server" Text="Activate/Deactivate"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnActivateHotel" runat="server" CommandArgument='<%# Eval("ContentTitle") %>' ToolTip='<%# Eval("Id") %>'
                                        CommandName="Modify" Height="20px" Width="20px" />
                                    <%--<asp:Checkbox ID="chkActivateHotel" runat="server" OnClick="chkActivateHotel_CheckedChanged" AutoPostBack="true" ToolTip='<%# Eval("Id") %>' />--%></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                        <EmptyDataTemplate>
                            <tr>
                                <td colspan="3" align="center">
                                    "No records found."
                                </td>
                            </tr>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                        <PagerStyle HorizontalAlign="Right" BackColor="White" />
                        <PagerTemplate>
                            <div id="Paging" style="width: 100%; display: none;">
                                <table cellpadding="0" align="right">
                                    <tr>
                                        <td style="vertical-align:middle; height: 22px;">
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </PagerTemplate>
                        <%--<PagerSettings Mode="NumericFirstLast" Position="Top" />
                        <PagerStyle HorizontalAlign="Right" BackColor="White" />
                        <PagerTemplate>
                            <div style="float: right; width: auto;">
                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </div>
                        </PagerTemplate>--%>
                    </asp:GridView>
                    <div style="display:none;" >
                        <div style="float: right;">
                            <span><b>Activated&nbsp;&nbsp;<asp:CheckBox ID="chkActivate" runat="server" OnCheckedChanged="chkActivate_CheckedChanged"
                                AutoPostBack="true" /></b></span>
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
                <div id="gridBenefits" runat="server" visible="false">
                    <asp:GridView ID="grvBenefits" runat="server" ShowHeader="true" ShowHeaderWhenEmpty="true" AllowPaging="true" PageSize="10"
                        AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" OnPageIndexChanging="grvBenefits_PageIndexChanging"
                        CellPadding="0" Width="100%" GridLines="None" CellSpacing="1" OnRowDataBound="grvBenefits_RowDataBound"
                        OnRowCommand="grvBenefits_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblSelectDesc" runat="server" Text="Select"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtEditDesc" runat="server" Text="Modify" ToolTip='<%# Eval("Id") %>'
                                        OnClick="lbtEditDesc_Click"></asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblDesc" runat="server" Text="Short Description"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescBody" runat="server" Text='<%# Eval("ContentShortDesc") %>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblImageBenefits" runat="server" Text="Image"></asp:Label></HeaderTemplate>
                                <ItemStyle CssClass="con1-earned" />
                                <ItemTemplate>
                                    <asp:Image ID="imgHotelBenefits" runat="server" Style="width: 70px; height: 60px;" /></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblDeleteBenefit" runat="server" Text="Delete"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtDeleteBenefit" runat="server" OnClientClick="return confirm('Do you really want to delete ?');"
                                        OnClick="lbtDeleteBenefit_Click" ToolTip='<%# Eval("Id") %>'>Delete</asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblActivateBenefit" runat="server" Text="Activate/Deactivate"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnActivateBenefit" runat="server" CommandArgument='<%# Eval("ContentTitle") %>' ToolTip='<%# Eval("Id") %>'
                                        CommandName="Modify" Height="20px" Width="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                        <EmptyDataTemplate>
                            <tr>
                                <td colspan="3" align="center">
                                    "No records found."
                                </td>
                            </tr>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align:middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>
                    </asp:GridView>
                </div>
                <div id="gridNews" runat="server" visible="false">
                    <asp:GridView ID="grvNews" runat="server" ShowHeader="true" ShowHeaderWhenEmpty="true" AllowPaging="true" PageSize="10"
                        AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" OnPageIndexChanging="grvNews_PageIndexChanging"
                        CellPadding="0" Width="100%" GridLines="None" CellSpacing="1" OnRowCommand="grvNews_RowCommand"
                        OnRowDataBound="grvNews_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblSelectNews" runat="server" Text="Select"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtEditNews" runat="server" Text="Modify" ToolTip='<%# Eval("Id") %>'
                                        OnClick="lbtEditNews_Click"></asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblNewsDesc" runat="server" Text="News"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblNewsDescBody" runat="server" Text='<%# Eval("ContentsDesc") %>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblUpdatedDate" runat="server" Text="Published Date"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblUpdatedDateText" runat="server" Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("UpdatedDate"))%>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblDeleteNews" runat="server" Text="Delete"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtDeleteNews" runat="server" OnClientClick="return confirm('Do you really want to delete ?');"
                                        OnClick="lbtDeleteNews_Click" ToolTip='<%# Eval("Id") %>'>Delete</asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblActivateNews" runat="server" Text="Activate/Deactivate"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnActivateNews" runat="server" CommandArgument='<%# Eval("ContentTitle") %>' ToolTip='<%# Eval("Id") %>'
                                        CommandName="Modify" Height="20px" Width="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                        <EmptyDataTemplate>
                            <tr>
                                <td colspan="3" align="center">
                                    "No records found."
                                </td>
                            </tr>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align:middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>
                    </asp:GridView>
                </div>
                <div id="gridImageGallery" runat="server" visible="false">
                    <asp:GridView ID="grvImageGallery" runat="server" ShowHeader="true" ShowHeaderWhenEmpty="true" AllowPaging="true" PageSize="10"
                        AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" OnPageIndexChanging="grvImageGallery_PageIndexChanging"
                        OnRowCommand="grvImageGallery_RowCommand" CellPadding="0" Width="100%" GridLines="None"
                        CellSpacing="1" OnRowDataBound="grvImageGallery_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblSelectImage" runat="server" Text="Select"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtEditImage" runat="server" Text="Modify" ToolTip='<%# Eval("Id") %>'
                                        OnClick="lbtEditImage_Click"></asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblImageTitle" runat="server" Text="Image Title"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblImageTitleText" runat="server" Text='<%#  Eval("ContentTitle")%>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblImageDesc" runat="server" Text="Page Url"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblImageDescBody" runat="server" Text='<%# Eval("pageurl") %>'></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblImageGallery" runat="server" Text="Image"></asp:Label></HeaderTemplate>
                                <ItemStyle CssClass="con1-earned" />
                                <ItemTemplate>
                                    <asp:Image ID="imgGallery" runat="server" Style="width: 140px; height: 100px;" /></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblDeleteImage" runat="server" Text="Delete"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtDeleteImage" runat="server" OnClientClick="return confirm('Do you really want to delete ?');"
                                        OnClick="lbtDeleteImage_Click" ToolTip='<%# Eval("Id") %>'>Delete</asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblActivateImage" runat="server" Text="Activate/Deactivate"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnActivateImage" runat="server" CommandArgument='<%# Eval("Id") %>'
                                        CommandName="Modify" Height="20px" Width="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                        <EmptyDataTemplate>
                            <tr>
                                <td colspan="3" align="center">
                                    "No records found."
                                </td>
                            </tr>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align:middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>
                    </asp:GridView>
                </div>
                <div id="gridBottomLinks" runat="server" visible="false">
                    <asp:GridView ID="grvBottomLinks" runat="server" ShowHeader="true" ShowHeaderWhenEmpty="true" AllowPaging="true" PageSize="10"
                        AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" OnPageIndexChanging="grvBottomLinks_PageIndexChanging"
                        CellPadding="0" Width="100%" GridLines="None" CellSpacing="1" OnRowDataBound="grvBottomLinks_RowDataBound"
                        OnRowCommand="grvBottomLinks_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblSelectLink" runat="server" Text="Select"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtEditLink" runat="server" Text="Modify" ToolTip='<%# Eval("Id") %>'
                                        OnClick="lbtEditLink_Click"></asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblLink" runat="server" Text="Link text"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <u>
                                        <asp:Label ID="lblLinkText" runat="server" Text='<%# Eval("ContentShortDesc") %>'
                                            ToolTip='<%# Eval("Id") %>'></asp:Label></u>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblDeleteLink" runat="server" Text="Delete"></asp:Label></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtDeleteLink" runat="server" OnClientClick="return confirm('Do you really want to delete ?');"
                                        OnClick="lbtDeleteLink_Click" ToolTip='<%# Eval("Id") %>'>Delete</asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblActivateLink" runat="server" Text="Activate/Deactivate"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnActivateLink" runat="server" CommandArgument='<%# Eval("ContentTitle") %>' ToolTip='<%# Eval("Id") %>'
                                        CommandName="Modify" Height="20px" Width="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                          <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align:middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>
                    </asp:GridView>
                </div>
                <div id="hotelBanner" runat="server" visible="false">
                    <asp:GridView ID="grvBanner" runat="server" ShowHeader="true" ShowHeaderWhenEmpty="true" AllowPaging="true" PageSize="10"
                        AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" OnPageIndexChanging="grvBanner_PageIndexChanging"
                        CellPadding="0" Width="100%" GridLines="None" CellSpacing="1" OnRowDataBound="grvBanner_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblBannerImage" runat="server" Text="Image"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="imgBanner" runat="server" AlternateText="Image not available" Style="width: 757px;
                                        height: 255px;" /><br />
                                    <asp:LinkButton ID="lbtBannerImage" runat="server" Text="Modify" ToolTip='<%# Eval("Id") %>'
                                        OnClick="lbtBannerImage_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                        <EmptyDataTemplate>
                            <tr>
                                <td colspan="1" align="center">
                                    No Banner found. <i>
                                        <asp:LinkButton ID="lbtAddNew" runat="server" OnClick="lbtAddNew_Click" Text="Click here to add a new one"></asp:LinkButton></i>
                                </td>
                            </tr>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align:middle"; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>
                    </asp:GridView>
                </div>
                <div id="divAgencyControlPanelImage" runat="server" visible="false">
                    <div id="divmessageAgency" runat="server" style="display:none;" class="error" ></div>
                    <div style="float:left;width:100%;text-align:right;padding-right:20px;" ><asp:LinkButton ID="lnkModify" runat="server" Text="Modify" OnClick="lnkModify_OnClick"></asp:LinkButton></div>
                    <asp:Repeater ID="rptAgencyControlPanel" runat="server" onitemdatabound="rptAgencyControlPanel_ItemDataBound" >
                        <HeaderTemplate>
                            <table width="100%" cellpadding="5" cellspacing="1" style="float:left;"><tr>
                        </HeaderTemplate>
                        <ItemTemplate><td align="center"><asp:Image ID="imgControlPanelImage" runat="server" /><br /><asp:Label ID="lblControlPanelTitel" runat="server"></asp:Label></td></ItemTemplate>
                        <FooterTemplate></tr></table></FooterTemplate>
                    </asp:Repeater>
                    <div id="modifyAgencyControlPanel" runat="server" visible="false" >
                        <table style="float:left;" >
                            <tr>
                                <td><b>Image 1:</b> </td>
                                <td>Title: </td>
                                <td><asp:TextBox ID="txtTitleImage1" runat="server" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>File: </td>
                                <td><asp:FileUpload ID="upfImage1" runat="server" /></td>
                            </tr>
                            <tr>
                                <td><b>Image 2:</b> </td>
                                <td>Title: </td>
                                <td><asp:TextBox ID="txtTitleImage2" runat="server" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>File: </td>
                                <td><asp:FileUpload ID="upfImage2" runat="server" /></td>
                            </tr>
                            <tr>
                                <td><b>Image 3:</b> </td>
                                <td>Title: </td>
                                <td><asp:TextBox ID="txtTitleImage3" runat="server" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>File: </td>
                                <td><asp:FileUpload ID="upfImage3" runat="server" /></td>
                            </tr>
                        </table>
                        <div class="button_section">
                            <asp:LinkButton ID="lnkSaveAgencyImage" runat="server" OnClick="lnkSaveAgencyImage_Click"  CssClass="select" OnClientClick="return CheckAgencyControlPanel();" >Save</asp:LinkButton>
                            <asp:LinkButton ID="lnkCancelAgencyAdd" runat="server" OnClick="lnkCancelAgencyAdd_Click">Cancel</asp:LinkButton>
                        </div>
                        
                    </div>
                    <script language ="javascript" type="text/javascript">
                        jQuery(document).ready(function () { SetFocus(); });
                        function CheckAgencyControlPanel() {
                            var isvalid = true;
                            var errormessage = '';
                            if (jQuery("#<%= divmessageAgency.ClientID %>").hasClass("succesfuly")) {
                                jQuery("#<%= divmessageAgency.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                            }
                            if (jQuery('#<%= txtTitleImage1.ClientID %>') != null && jQuery('#<%= txtTitleImage1.ClientID %>') != undefined) {
                                var title = jQuery('#<%= txtTitleImage1.ClientID %>').val();
                                if (title == "") {
                                    if (errormessage.length > 0) {
                                        errormessage += "<br/>";
                                    }
                                    errormessage += 'Title image 1 is required.';
                                    isvalid = false;
                                }
                            }
                            var file1 = jQuery('#<%= upfImage1.ClientID %>').val();

                            if (file1.length <= 0) {
                                //if (errormessage.length > 0) {
                                //    errormessage += "<br/>";
                                //}
                                //errormessage += "Image 1 is required.";
                                //isvalid = false;
                            }
                            else {
                                if (!validate_file_format(file1, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                                    if (errormessage.length > 0) {
                                        errormessage += "<br/>";
                                    }
                                    errormessage += "Please select image 1 file in (Gif, JPG, JPEG, PNG) formats only.";
                                    isvalid = false;
                                }
                            }
                            if (jQuery('#<%= txtTitleImage2.ClientID %>') != null && jQuery('#<%= txtTitleImage2.ClientID %>') != undefined) {
                                var title = jQuery('#<%= txtTitleImage2.ClientID %>').val();
                                if (title == "") {
                                    if (errormessage.length > 0) {
                                        errormessage += "<br/>";
                                    }
                                    errormessage += 'Title image 2 is required.';
                                    isvalid = false;
                                }
                            }
                            var file2 = jQuery('#<%= upfImage2.ClientID %>').val();

                            if (file2.length <= 0) {
                                /*if (errormessage.length > 0) {
                                    errormessage += "<br/>";
                                }
                                errormessage += "Image 2 is required.";
                                isvalid = false;*/
                            }
                            else {
                                if (!validate_file_format(file2, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                                    if (errormessage.length > 0) {
                                        errormessage += "<br/>";
                                    }
                                    errormessage += "Please select image 2 file in (Gif, JPG, JPEG, PNG) formats only.";
                                    isvalid = false;
                                }
                            }
                            if (jQuery('#<%= txtTitleImage3.ClientID %>') != null && jQuery('#<%= txtTitleImage3.ClientID %>') != undefined) {
                                var title = jQuery('#<%= txtTitleImage3.ClientID %>').val();
                                if (title == "") {
                                    if (errormessage.length > 0) {
                                        errormessage += "<br/>";
                                    }
                                    errormessage += 'Title image 3 is required.';
                                    isvalid = false;
                                }
                            }
                            var file3 = jQuery('#<%= upfImage3.ClientID %>').val();

                            if (file3.length <= 0) {
                                /*if (errormessage.length > 0) {
                                    errormessage += "<br/>";
                                }
                                errormessage += "Image 3 is required.";
                                isvalid = false;*/
                            }
                            else {
                                if (!validate_file_format(file3, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                                    if (errormessage.length > 0) {
                                        errormessage += "<br/>";
                                    }
                                    errormessage += "Please select image 3 file in (Gif, JPG, JPEG, PNG) formats only.";
                                    isvalid = false;
                                }
                            }

                            if (!isvalid) {
                                jQuery("#<%= divmessageAgency.ClientID %>").show();
                                jQuery("#<%= divmessageAgency.ClientID %>").html(errormessage);
                                return false;
                            }
                            jQuery("#<%= divmessageAgency.ClientID %>").html("");
                            jQuery("#<%= divmessageAgency.ClientID %>").hide();
                            jQuery("#Loding_overlay").show();
                            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                        }
                        </script>
                </div>
                <div id="OnClickDIV" runat="server" visible="false">
                    <br />
                    <div id="TabbedPanels2" class="TabbedPanels" runat="server">
                        <uc1:LanaguageTabWithoutCountry ID="LanaguageTabWithoutCountry1" runat="server" />
                        <br />
                        <%--<script type="text/javascript">
                            var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels2");
            </script>--%>
                    </div>
                    <div id="editor" runat="server" visible="false">
                        <br />
                        <b>
                            <asp:Label ID="lblContentDesc" runat="server" Text="You can Edit Content description here"
                                Style="margin: 0 0 0 9px;"></asp:Label></b><br />
                        <div class="TabbedPanelsContentGroup">
                            <div class="TabbedPanelsContent">
                                <div class="tab-textarea1">
                                    <CKEditor:CKEditorControl ID="ckeEditorEN" runat="server" Toolbar="Full"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="titleDiv" runat="server" visible="false">
                        <b><span style="margin: 5px 0px 0px 7px">
                            <asp:Label ID="lblHeading" runat="server" Text="Title"></asp:Label></span></b><br />
                        <div style="margin: 10px 0 10px 5px;">
                            <asp:TextBox ID="txtTitle" runat="server" TextMode="SingleLine" MaxLength="100" ReadOnly="true"
                                Width="944px"></asp:TextBox></div>
                        <div style="margin: 10px 0 14px 7px;" id="calender" runat="server">
                            <b>From : </b>
                            <asp:TextBox ID="txtFrom" runat="server" MaxLength="20" CssClass="dateinputf"></asp:TextBox>
                            <input type="image" src="../Images/dateicon.png" id="calFrom2" tabindex="1" /><asp:CalendarExtender
                                ID="calExTO" runat="server" TargetControlID="txtFrom" PopupButtonID="calFrom2"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            &nbsp;&nbsp;<b>To : </b>
                            <asp:TextBox ID="txtTo" runat="server" MaxLength="20" CssClass="dateinputf"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
                                PopupButtonID="calTo" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <input type="image" src="../Images/dateicon.png" id="calTo" />
                        </div>
                    </div>
                    <div id="descriptionTextboxDIV" runat="server" visible="false" style="margin: 15px 0 15px 5px;">
                        <span><b>You can enter short Description here</b></span><br />
                        <asp:TextBox ID="txtDescription" runat="server" MaxLength="150" TextMode="MultiLine"
                            Style="margin: 15px 0 0;"></asp:TextBox><br />
                        <br />
                    </div>
                    <div id="fileUploadPDF" runat="server" visible="false" style="margin: 20px 0;">
                        <div>
                            <b><span style="margin: 0px 0px 0px 7px">
                                <asp:Label ID="lblPDFUpload" runat="server" Text="Upload a new PDF from here."></asp:Label></span></b>
                        </div>
                        <br />
                        <div style="margin: 0 0 0 5px;">
                            <asp:FileUpload ID="pdfUpload" runat="server" /></div>
                        <br />
                        <span style="font-family: Arial; font-size: Smaller; margin: 0px 0px 0px 5px">File format:
                            Pdf</span>
                        <br />
                        <br />
                        <div id="ViewPreviousFIleDIV" runat="server" style="float: left" visible="false">
                            <br />
                            <i>
                                <asp:LinkButton ID="lbtViewCurrentFile" runat="server">Click here to view current file</asp:LinkButton></i></div>
                        <br />
                        <br />
                        <div id="PublishedDateDIV" runat="server" visible="false">
                            <b><span style="margin: 0px 0px 0px 7px">Published Date : </span></b>
                            <asp:TextBox ID="txtPublishedDate" runat="server" MaxLength="20" CssClass="dateinputf"></asp:TextBox>
                            <input type="image" src="../Images/dateicon.png" id="calFrom" tabindex="1" /><asp:CalendarExtender
                                ID="CalendarExtender1" runat="server" TargetControlID="txtPublishedDate" PopupButtonID="calFrom"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                    <div id="bottomLinkDropDownListsDIV" runat="server" visible="false">
                        <b><span style="margin: 0px 0px 0px 7px">
                            <asp:Label ID="lblContentDescription" runat="server" Text="Link Text"></asp:Label></span></b><br />
                        <div style="margin: 10px 0 10px 5px;">
                            <asp:TextBox ID="txtContentDesc" runat="server" TextMode="SingleLine" MaxLength="50"
                                Width="938px"></asp:TextBox></div>
                        <b><span style="margin: 0px 0px 0px 7px">
                            <asp:Label ID="lblPageURL" runat="server" Text="Set URL"></asp:Label></span></b><br />
                        <table width="100%" style="margin: 7px 0px 7px 2px">
                            <tr>
                                <td align="left">
                                    Country&nbsp;
                                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    City&nbsp;
                                    <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    Zone&nbsp;
                                    <asp:DropDownList ID="ddlZone" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divMetaDescription" runat="server" visible="false">
                        <b><span style="margin: 0px 0px 0px 7px">
                            <asp:Label ID="lblKeywords" runat="server" Text="Meta Keywords"></asp:Label></span></b><br />
                        <div style="margin: 10px 0 10px 5px;">
                            <asp:TextBox ID="txtKeywords" runat="server" TextMode="MultiLine" MaxLength="200"
                                Width="938px"></asp:TextBox></div>
                        <br />
                        <b><span style="margin: 0px 0px 0px 7px">
                            <asp:Label ID="lblMetaDesc" runat="server" Text="Meta description"></asp:Label></span></b><br />
                        <div style="margin: 10px 0 10px 5px;">
                            <asp:TextBox ID="txtMetaDesc" runat="server" TextMode="MultiLine" MaxLength="200"
                                Width="938px"></asp:TextBox></div>
                    </div>
                    <div id="socialNetworkingDiv" runat="server" visible="false">
                        <br />
                        <br />
                        <table border="0" width="450px" cellspacing="0" cellpadding="6">
                            <tr>
                                <td>
                                    <span><b>Select a site</b></span>
                                    <asp:DropDownList ID="SocialSitesDDL" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SocialSitesDDL_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <br />
                                </td>
                                <td>
                                    <span><b>Page url</b></span>
                                    <asp:TextBox ID="txtPageUrl" runat="server" MaxLength="100"></asp:TextBox>
                                </td>
                                <td>
                                    <span><b>Icon</b></span>
                                    <asp:Image Height="30px" Width="30px" ID="imgIcon" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    </div>
                    <div id="fileUploadImage" runat="server" visible="false">
                        <b><span style="margin: 0px 0px 0px 7px">
                            <asp:Label ID="lblImageUpload" runat="server" Text="Upload a new Image file from here."></asp:Label></span></b><br />
                        <br />
                        <div style="margin: 0 0 0 5px;">
                            <asp:FileUpload ID="imageUpload" runat="server" /></div>
                        <br />
                        <span style="font-family: Arial; font-size: Smaller; margin: 0px 0px 0px 5px;">Image
                            format: JPG,JPEG,PNG,GIF. Ideal image dimension: 168x168 </span>
                            <br />
                            <div id="divpageurl" runat="server" >
                            <span><b>Page url</b></span>
                             <asp:TextBox ID="txturl" runat="server" MaxLength="100"></asp:TextBox> <span style="font-family: Arial; font-size: Smaller; margin: 0px 0px 0px 5px;">
                            Eg: https:/www.google.com/ </span>
                             </div>
                    </div>
                </div>
            </div>
            <div id="SaveStaticPages" runat="server" class="button_section" visible="false">
            <asp:LinkButton ID="lnkSaveStatic" runat="server" OnClick="lnkSaveStatic_Click"  CssClass="select">Save</asp:LinkButton>
                <asp:LinkButton ID="lnkCancel2" runat="server" OnClick="lbtCancel2_Click">Cancel</asp:LinkButton>
            </div>
            <div id="saveCancelBtnDIV" runat="server" class="button_section" visible="false">
                <asp:LinkButton ID="lbtSave" runat="server" OnClick="lbtSave_Click" CssClass="select">Save</asp:LinkButton><span>&nbsp;or</span>
                <asp:LinkButton ID="lbtCancel" runat="server" OnClick="lbtCancel_Click">Cancel</asp:LinkButton>
            </div>
            
        </div>
        <!-- superadmin-example-layout end-->
        <!-- end main_body-->
        <script language="javascript" type="text/javascript">

            jQuery("#<%= lnkSaveStatic.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var isvalid = true;
                var errormessage = "";

                if (jQuery('#<%= txtTitle.ClientID %>') != null && jQuery('#<%= txtTitle.ClientID %>') != undefined) {
                    var title = jQuery('#<%= txtTitle.ClientID %>').val();
                    if (title == "") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Title is required.';
                        isvalid = false;
                    }
                }


                var frame = jQuery("#<%=ckeEditorEN.ClientID %>").siblings().find('span').find('iframe').get(0);
                if (frame != undefined) {
                    var body = jQuery(frame).contents().find('body');
                    var text = jQuery(body).text();
                    if (text == '') {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Content description is required.';
                        isvalid = false;
                    }
                }
                
                if (!isvalid) {
                    jQuery("#<%= divmessage.ClientID %>").show();
                    jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                    var offseterror = jQuery("#<%= divmessage.ClientID %>").offset();
                    jQuery("#Loding_overlay").hide();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }

                jQuery(".error").hide();
                jQuery(".error").html('');
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Saving...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';

            });
            jQuery("#<%= lbtSave.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var isvalid = true;
                var errormessage = "";
                var fromdate = jQuery("#<%= txtFrom.ClientID %>").val();
                var todate = jQuery("#<%= txtTo.ClientID %>").val();
                if (fromdate != undefined && todate != undefined) {
                    if (fromdate != "dd/MM/yyyy" || todate != "dd/MM/yyyy") {
                        jQuery(".error").html("");
                        jQuery(".error").hide();
                        jQuery("#<%= txtFrom.ClientID %>").attr("disabled", false);
                        jQuery("#<%= txtTo.ClientID %>").attr("disabled", false);
                        jQuery("#Loding_overlay").show();

                        if (fromdate.length != 0 && todate.length != 0) {
                            var todayArr = todate.split('/');
                            var formdayArr = fromdate.split('/');
                            var fromdatecheck = new Date();
                            var todatecheck = new Date();
                            fromdatecheck.setFullYear(parseInt("20" + formdayArr[2],10), (parseInt(formdayArr[1],10) - 1), formdayArr[0]);
                            todatecheck.setFullYear(parseInt("20" + todayArr[2],10), (parseInt(todayArr[1],10) - 1), todayArr[0]);
                            if (fromdatecheck > todatecheck) {
                                if (errormessage.length > 0) {
                                    errormessage += "<br/>";
                                }
                                errormessage += '\"From date\" must be earlier or equal to \"To date\".';
                                //alert('\"From date\" must be earlier or equal to \"To date\".');
                                jQuery("#<%= txtFrom.ClientID %>").attr("disabled", true);
                                jQuery("#<%= txtTo.ClientID %>").attr("disabled", true);
                                isvalid = false;
                            }
                        }
                    }
                }

                if (jQuery('#<%= pdfUpload.ClientID %>') != null && jQuery('#<%= pdfUpload.ClientID %>') != undefined) {
                    var filePdf = jQuery('#<%= pdfUpload.ClientID %>').val();
                    var hdnCmsId = jQuery("#<%=hdnCmsId.ClientID %>").val();
                    var task = jQuery("#<%=hdnTask.ClientID %>").val();
                    if (hdnCmsId == "33" || hdnCmsId == "36" || hdnCmsId == "30" || hdnCmsId == "32" || hdnCmsId == "35" || task != "Edit") {
                        if (filePdf == "") {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += "You must provide a file.";
                            isvalid = false;
                        }
                    }
                }


                if (jQuery('#<%= txtDescription.ClientID %>') != null && jQuery('#<%= txtDescription.ClientID %>') != undefined) {
                    var desc = jQuery('#<%= txtDescription.ClientID %>').val();
                    if (desc == "") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Short description is required.';
                        isvalid = false;
                    }
                }

                if (jQuery('#<%= imageUpload.ClientID %>') != null && jQuery('#<%= imageUpload.ClientID %>') != undefined) {
                    var file = jQuery('#<%= imageUpload.ClientID %>').val();
                    var hdnCmsId = jQuery("#<%=hdnCmsId.ClientID %>").val();
                    var task = jQuery("#<%=hdnTask.ClientID %>").val();
                    if (hdnCmsId == "43" || task != "Edit") {
                        if (file == "") {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += 'Image is required.';
                            isvalid = false;
                        }
                    }
                }

                if (jQuery('#<%= txtTitle.ClientID %>') != null && jQuery('#<%= txtTitle.ClientID %>') != undefined) {
                    var title = jQuery('#<%= txtTitle.ClientID %>').val();
                    if (title == "") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Title is required.';
                        isvalid = false;
                    }
                }


                var frame = jQuery("#<%=ckeEditorEN.ClientID %>").siblings().find('span').find('iframe').get(0);
                if (frame != undefined) {
                    var body = jQuery(frame).contents().find('body');
                    var text = jQuery(body).text();
                    if (text == '') {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Content description is required.';
                        isvalid = false;
                    }
                }
                if (jQuery('#<%= ddlCity.ClientID %>') != null && jQuery('#<%= ddlCity.ClientID %>').val() != undefined) {
                    var ddlCity = jQuery('#<%= ddlCity.ClientID %>').val();
                    if (ddlCity == 0 || ddlCity == undefined) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'City is required.';
                        isvalid = false;
                    }
                }

                if (jQuery('#<%= txtContentDesc.ClientID %>') != null && jQuery('#<%= txtContentDesc.ClientID %>') != undefined) {
                    var txtContentDesc = jQuery('#<%= txtContentDesc.ClientID %>').val();
                    if (txtContentDesc == "") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Link text is required.';
                        isvalid = false;
                    }
                }

                if (jQuery('#<%= txtKeywords.ClientID %>') != null && jQuery('#<%= txtKeywords.ClientID %>') != undefined) {
                    var txtKeywords = jQuery('#<%= txtKeywords.ClientID %>').val();
                    if (txtKeywords == "") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Meta keywords required.';
                        isvalid = false;
                    }
                }

                if (jQuery('#<%= txtMetaDesc.ClientID %>') != null && jQuery('#<%= txtMetaDesc.ClientID %>') != undefined) {
                    var txtKeywords = jQuery('#<%= txtMetaDesc.ClientID %>').val();
                    if (txtKeywords == "") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Meta description is required.';
                        isvalid = false;
                    }
                }


                if (jQuery('#<%= calender.ClientID %>') != null && jQuery('#<%= calender.ClientID %>') != undefined) {
                    var fromDate = jQuery('#<%= txtFrom.ClientID %>').val();
                    if (fromDate == "") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'From date is required.';
                        isvalid = false;
                    }
                    var toDate = jQuery('#<%= txtTo.ClientID %>').val();
                    if (toDate == "") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'To date is required.';
                        isvalid = false;
                    }
                }

                if (jQuery('#<%= pdfUpload.ClientID %>') != null && jQuery('#<%= pdfUpload.ClientID %>') != undefined) {
                    var filePdf = jQuery('#<%= pdfUpload.ClientID %>').val();
                    if (filePdf != "") {
                        if (filePdf != undefined) {
                            if (!validate_file_format(filePdf, "pdf,Pdf,PDF,pDF")) {
                                if (errormessage.length > 0) {
                                    errormessage += "<br/>";
                                }
                                errormessage += "Please select file in (PDF) formats only.";
                                isvalid = false;
                            }
                        }
                    }
                }
                if (jQuery('#<%= imageUpload.ClientID %>') != null) {
                    var file = jQuery('#<%= imageUpload.ClientID %>').val();
                    if (file != "") {
                        if (file != undefined) {
                            if (!validate_file_format(file, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG")) {
                                if (errormessage.length > 0) {
                                    errormessage += "<br/>";
                                }
                                errormessage += "Please select image file in (Gif, JPG, JPEG and PNG) formats only.";
                                isvalid = false;
                            }
                        }
                    }
                }
                if (jQuery("#<%= hdnContentTitle.ClientID %>").val() == "FrontEndWelcomeMessage") {
                    if (content.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "You must provide a welcome message.";
                        isvalid = false;
                    }
                }
                if (!isvalid) {
                    jQuery("#<%= divmessage.ClientID %>").show();
                    jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                    var offseterror = jQuery("#<%= divmessage.ClientID %>").offset();
                    jQuery("#Loding_overlay").hide();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }

                jQuery(".error").hide();
                jQuery(".error").html('');
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Saving...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';

            });

            function validate_file_format(file_name, allowed_ext) {
                field_value = file_name;
                if (field_value != "") {
                    var file_ext = (field_value.substring((field_value.lastIndexOf('.') + 1)).toLowerCase());
                    ext = allowed_ext.split(',');
                    var allow = 0;
                    for (var i = 0; i < ext.length; i++) {
                        if (ext[i] == file_ext) {
                            allow = 1;
                        }
                    }
                    if (!allow) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                return false;
            }

            function GetLanguageID(languageId) {
                jQuery("#<%=hdnLanguageID.ClientID %>").val(languageId);
                jQuery(".superadmin-mainbody-sub6 ul li a").removeClass("select");
                jQuery("#liLanguageTab" + languageId + "").addClass("select");
                jQuery("#<%=btnLanguage.ClientID %>").click();
            }

            jQuery(document).ready(function () {
                jQuery("#<%= txtFrom.ClientID %>").attr("disabled", true);
                jQuery("#<%= txtTo.ClientID %>").attr("disabled", true);
                var languageId = jQuery("#<%=hdnLanguageID.ClientID %>").val();
                if (languageId != "0") {
                    jQuery(".superadmin-mainbody-sub6 ul li a").removeClass("select");
                    jQuery("#liLanguageTab" + languageId + "").addClass("select");
                }
            });

            function SetFocus() {
                window.scrollBy(0, 3000);
            }

            function open_win(pageurl) {

                var url = pageurl;
                var winName = 'myWindow';
                var w = '700';
                var h = '500';
                var scroll = 'no';
                var popupWindow = null;
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
                settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
                popupWindow = window.open(url, winName, settings);
                //alert(url);
                return false;

            }

            jQuery(document).ready(function () {
                if (jQuery("#Paging") != undefined) {
                    var inner = jQuery("#Paging").html();
                    jQuery("#pageing").html(inner);
                    jQuery("#Paging").html("");
                }
            });
            
        </script>
            
    </div>
</asp:Content>
