﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text;
using System.Configuration;
using System.IO;
using System.Net;

public partial class SuperAdmin_WhiteLabelManager : System.Web.UI.Page
{
    #region variable
    public ManageWhiteLabel objWLManager = new ManageWhiteLabel();
    EmailConfigManager em = new EmailConfigManager();
    ClientContract objClient = new ClientContract();
    SendMails objSendmail = new SendMails();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    #endregion
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divmsg.Style.Add("display", "none");
            divmsg.Attributes.Add("class", "error");
            BindWhiteLabelGrid();
        }

        ApplyPaging();
    }
    #endregion
    #region Function
    /// <summary>
    /// This function used to bind grid.
    /// </summary>
    void BindWhiteLabelGrid()
    {
        grdWhiteLabel.DataSource = objWLManager.GetAllWhiteLabel();
        grdWhiteLabel.DataBind();
    }

    /// <summary>
    /// This function used to apply paging 
    /// </summary>
    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grdWhiteLabel.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdWhiteLabel.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdWhiteLabel.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdWhiteLabel.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdWhiteLabel.PageIndex == grdWhiteLabel.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }

    /// <summary>
    /// This function used to bind all hotel with check box list.
    /// </summary>
    void BindHotelCheckBoxList()
    {
        HotelInfo ObjHotelinfo = new HotelInfo();
        string whereclauseforHotel = HotelColumn.IsRemoved + "=0 AND " + HotelColumn.IsActive + "=1 AND " + HotelColumn.GoOnline + "=1";
        string orderBy = HotelColumn.Name +" ASC";
        TList<Hotel> THotel = ObjHotelinfo.GetHotelbyCondition(whereclauseforHotel, orderBy);
        chkHotelList.DataSource = THotel;
        chkHotelList.DataTextField = "Name";
        chkHotelList.DataValueField = "Id";
        chkHotelList.DataBind();
    }
    
    /// <summary>
    /// This function used to read text file.
    /// </summary>
    /// <returns></returns>
    private string[] ReadCode()
    {
        string fileName = Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "WLCodeFile/") + "WLBaseCode.txt";

        string[] wlCode = File.ReadAllLines(fileName);
        //StringBuilder sb = new StringBuilder();
        //sb.Append(wlCode);
        return wlCode;
    }

    /// <summary>
    /// This function used to generate random ID.
    /// </summary>
    /// <returns></returns>
    public string GeneratWLID()
    {
      return   (100000 + objWLManager.GetWhiteLabelLastID()).ToString();
    }


    /// <summary>
    /// This function used to bind all hotel with datalist.
    /// </summary>
    /// <param name="WLId"></param>
    void BindHotelGroupDatalist(long WLId)
    {

        dlViewHotelGroup.DataSource = objWLManager.GetHotelMappingByWhiteLabelId(WLId);
        dlViewHotelGroup.DataBind();

    }
    #endregion
    #region Events
    protected void lnkButtonAddnew_Click(object sender, EventArgs e)
    {
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");
        divmessageWL.Attributes.Add("class", "error");
        divmessageWL.Style.Add("display", "none");
        IsItemChecked.Value = "0";
        DivAddUpdateWL.Visible = true;
        lblHeader.Text = "Create White Label";
        BindHotelCheckBoxList();

        lblWLGeneratedNumber.Text = GeneratWLID();
        txtWLName.Text = "";
        txtTargetURL.Text = "";
        txtCommission.Text = "";
        hdnRecordID.Value = "0";
        chkIsActive.Checked = true;

        foreach (ListItem objchckHotel in chkHotelList.Items)
        {
            objchckHotel.Selected = false;
        }
        foreach (ListItem objchckGroup in chkBoxListGroup.Items)
        {
            objchckGroup.Selected = false;
        }

        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddUpdateWL.ClientID + "');});", true);
    }
    protected void lnkButtonDelete_Click(object sender, EventArgs e)
    {
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");
        DivAddUpdateWL.Visible = false;
        hdnRecordID.Value = "0";
        foreach (GridViewRow l in grdWhiteLabel.Rows)
        {
            CheckBox r = (CheckBox)l.FindControl("rbselectpolicy");
            if (r.Checked)
            {
                DataKey currentDataKey = this.grdWhiteLabel.DataKeys[l.RowIndex];
                hdnRecordID.Value = currentDataKey.Value.ToString();
            }
        }

        if (objWLManager.Deactivate(Convert.ToInt64(hdnRecordID.Value)))
        {
            divmsg.Style.Add("display", "block");
            divmsg.Attributes.Add("class", "succesfuly");
            divmsg.InnerHtml = "White label Deactivated successfully";
            BindWhiteLabelGrid();
            ApplyPaging();
        }
        else
        {
            divmsg.Style.Add("display", "block");
            divmsg.Attributes.Add("class", "error");
            divmsg.InnerHtml = "error: Please contact to technical support.";

        }
    }
    protected void lnkButtonModify_Click(object sender, EventArgs e)
    {
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");
        divmessageWL.Attributes.Add("class", "error");
        divmessageWL.Style.Add("display", "none");
        IsItemChecked.Value = "0";
        hdnRecordID.Value = "0";
        foreach (GridViewRow l in grdWhiteLabel.Rows)
        {
            CheckBox r = (CheckBox)l.FindControl("rbselectpolicy");
            if (r.Checked)
            {
                DataKey currentDataKey = this.grdWhiteLabel.DataKeys[l.RowIndex];
                hdnRecordID.Value = currentDataKey.Value.ToString();
            }
        }

        if (hdnRecordID.Value != "0")
        {
            WhiteLabel objWL = objWLManager.GetWhiteLabelByID(Convert.ToInt64(hdnRecordID.Value));
            if (objWL != null)
            {
                lblWLGeneratedNumber.Text = objWL.WhiteLabelNumber;
                txtWLName.Text = objWL.Name;
                txtTargetURL.Text = objWL.TargetUrl;
                txtCommission.Text = objWL.Commission.ToString();
                if (Convert.ToBoolean(objWL.IsActive))
                {
                    chkIsActive.Checked = true;
                }
                else
                {
                    chkIsActive.Checked = false;
                }

                BindHotelCheckBoxList();

                TList<WhiteLabelMappingWithHotel> objMapping = objWLManager.GetHotelMappingByWhiteLabelId(Convert.ToInt64(hdnRecordID.Value));
                if (objMapping.Count > 0)
                {
                    int intIndex = 0;
                    for (intIndex = 0; intIndex <= objMapping.Count - 1; intIndex++)
                    {
                        foreach (ListItem objChek in chkHotelList.Items)
                        {
                            if (objMapping[intIndex].HotelId == Convert.ToInt64(objChek.Value))
                            {
                                objChek.Selected = true;
                            }
                        }
                    }
                }
            }


        }

        DivAddUpdateWL.Visible = true;
        lblHeader.Text = "Update White Label";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddUpdateWL.ClientID + "');});", true);
    }
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        if (hdnRecordID.Value != "0")
        {
            WhiteLabel ObjWL = objWLManager.GetWhiteLabelByID(Convert.ToInt64(hdnRecordID.Value));
            ObjWL.Name = txtWLName.Text;
            ObjWL.TargetUrl = txtTargetURL.Text;
            ObjWL.Commission = Convert.ToDecimal(txtCommission.Text);
            ObjWL.IsActive = chkIsActive.Checked;

            string strImageName = ObjWL.TargetSiteLogo;
            if (UploadLogo.HasFile)
            {
                int fileSize = UploadLogo.PostedFile.ContentLength;
                if (fileSize > (1048576))
                {
                    divmessageWL.InnerHtml = "Filesize of image is too large. Maximum file size permitted is 1 MB.";
                    divmessageWL.Attributes.Add("class", "error");
                    divmessageWL.Style.Add("display", "block");
                    return;
                }
                else
                {
                    Guid objID = Guid.NewGuid();
                    strImageName = Path.GetFileName(UploadLogo.FileName);
                    strImageName = strImageName.Replace(" ", "").Replace("%20", "").Trim();
                    strImageName = System.Text.RegularExpressions.Regex.Replace(strImageName, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                    strImageName = objID + strImageName;
                    UploadLogo.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "TargetSiteLogo/") + strImageName);
                }

            }

            ObjWL.TargetSiteLogo = strImageName;


            if (objWLManager.UpdateWhiteLabel(ObjWL))
            {
                if (IsItemChecked.Value == "1")
                {

                    objWLManager.ClearMappingHotel(ObjWL.Id);

                    foreach (ListItem objChk in chkHotelList.Items)
                    {
                        if (objChk.Selected == true)
                        {
                            WhiteLabelMappingWithHotel objMapping = new WhiteLabelMappingWithHotel();
                            objMapping.WhiteLabelId = ObjWL.Id;
                            objMapping.HotelId = Convert.ToInt64(objChk.Value);
                            objWLManager.InsertMappingHotel(objMapping);
                        }
                    }

                }

                divmsg.Style.Add("display", "block");
                divmsg.Attributes.Add("class", "succesfuly");
                divmsg.InnerHtml = "White label updated successfully";

            }
            else
            {
                divmsg.Style.Add("display", "block");
                divmsg.Attributes.Add("class", "error");
                divmsg.InnerHtml = "error: Please contact to technical support.";
            }
        }
        else
        {
            WhiteLabel ObjWL = new WhiteLabel();
            ObjWL.WhiteLabelNumber = GeneratWLID();
            ObjWL.Name = txtWLName.Text;
            ObjWL.TargetUrl = txtTargetURL.Text;
            ObjWL.Commission = Convert.ToDecimal(txtCommission.Text);
            ObjWL.CreationDate = DateTime.Today;
            ObjWL.IsActive = chkIsActive.Checked;
            string strImageName = "";
            if (UploadLogo.HasFile)
            {
                int fileSize = UploadLogo.PostedFile.ContentLength;
                if (fileSize > (1048576))
                {
                    divmessageWL.InnerHtml = "Filesize of image is too large. Maximum file size permitted is 1 MB.";
                    divmessageWL.Attributes.Add("class", "error");
                    divmessageWL.Style.Add("display", "block");
                    return;
                }
                else
                {
                    Guid objID = Guid.NewGuid();
                    strImageName = Path.GetFileName(UploadLogo.FileName);
                    strImageName = strImageName.Replace(" ", "").Replace("%20", "").Trim();
                    strImageName = System.Text.RegularExpressions.Regex.Replace(strImageName, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                    strImageName = objID + strImageName;
                    UploadLogo.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "TargetSiteLogo/") + strImageName);
                }

            }
            ObjWL.TargetSiteLogo = strImageName;

            ObjWL.CodeFile = "WhitelLabelCode" + GeneratWLID() + ".txt"; ;

            if (objWLManager.InsertWhiteLabel(ObjWL))
            {
                objWLManager.ClearMappingHotel(ObjWL.Id);

                foreach (ListItem objChk in chkHotelList.Items)
                {
                    if (objChk.Selected == true)
                    {
                        WhiteLabelMappingWithHotel objMapping = new WhiteLabelMappingWithHotel();
                        objMapping.WhiteLabelId = ObjWL.Id;
                        objMapping.HotelId = Convert.ToInt64(objChk.Value);
                        objWLManager.InsertMappingHotel(objMapping);
                    }
                }

                // }

                string[] GetCode = ReadCode();
                int i = 0;
                for (i = 0; i <= GetCode.Length - 1; i++)
                {
                    if (GetCode[i].Contains("@WHITE_LABEL_ID"))
                    {
                        GetCode[i] = GetCode[i].Replace("@WHITE_LABEL_ID", Convert.ToString(ObjWL.Id));

                        //GetCode = GetCode.Replace("@WHITE_LABEL_ID", Convert.ToString(ObjWL.Id));
                    }

                    if (GetCode[i].Contains("@HOSTED_URL"))
                    {
                        GetCode[i] = GetCode[i].Replace("@HOSTED_URL", SiteRootPath);
                    }
                }
                string CodefileName = "WhitelLabelCode" + lblWLGeneratedNumber.Text + ".txt";
                File.Create(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "WLCodeFile/") + CodefileName).Close(); ;

                string createdFilePath = Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "WLCodeFile/") + CodefileName;

                File.WriteAllLines(createdFilePath, GetCode);


                divmsg.Style.Add("display", "block");
                divmsg.Attributes.Add("class", "succesfuly");
                divmsg.InnerHtml = "White label created successfully";
            }
            else
            {
                divmsg.Style.Add("display", "block");
                divmsg.Attributes.Add("class", "error");
                divmsg.InnerHtml = "error: Please contact to technical support.";
            }
        }

        BindWhiteLabelGrid();
        ApplyPaging();
        DivAddUpdateWL.Visible = false;
    }
    protected void lnkCancel_Click(object sender, EventArgs e)
    {
        DivAddUpdateWL.Visible = false;
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");
        divmessageWL.Attributes.Add("class", "error");
        divmessageWL.Style.Add("display", "none");

    }
    protected void grdWhiteLabel_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            WhiteLabel data = e.Row.DataItem as WhiteLabel;
            Label lblName = (Label)e.Row.FindControl("lblName");
            lblName.Text = data.Name;
            Label lblTargetUrl = (Label)e.Row.FindControl("lblTargetUrl");
            lblTargetUrl.Text = data.TargetUrl;
            Label lblCommission = (Label)e.Row.FindControl("lblCommission");
            lblCommission.Text = data.Commission.ToString();
            Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");
            if (Convert.ToBoolean(data.IsActive))
            {
                lblIsActive.Text = "Yes";
            }
            else
            {
                lblIsActive.Text = "No";
            }
            HyperLink lblDownloadCode = (HyperLink)e.Row.FindControl("lblDownloadCode");
            lblDownloadCode.NavigateUrl = "Download.aspx?file=" + data.CodeFile + "";

            LinkButton lnkSeeHotel = (LinkButton)e.Row.FindControl("lnkSee");
            lnkSeeHotel.CommandArgument = data.Id.ToString();
            lnkSeeHotel.CommandName = "SeeHotel";
            lnkSeeHotel.Text = "See Hotel";

            LinkButton lnkSendCond = (LinkButton)e.Row.FindControl("lnkSendCond");
            lnkSendCond.CommandName = "Send";
            lnkSendCond.CommandArgument = data.Id.ToString();
        }

    }
    protected void dlViewHotelGroup_ItemBound(Object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            WhiteLabelMappingWithHotel data = e.Item.DataItem as WhiteLabelMappingWithHotel;
            Label lblHotelName = (Label)e.Item.FindControl("lblName");
            lblHotelName.Text = objWLManager.GetHotelNameById(data.HotelId);

        }
    }
    protected void grdWhiteLabel_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Send")
        {
            divPopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenMailPopUp();", true);
            ViewState["WLID"] = e.CommandArgument;

        }
        else if (e.CommandName == "SeeHotel")
        {

            BindHotelGroupDatalist(Convert.ToInt64(e.CommandArgument));
            divAddUpdatePackagePopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenPopUp();", true);
        }
    }
    protected void btnSandMail_click(object sender, EventArgs e)
    {
        WhiteLabel objWL = objWLManager.GetWhiteLabelByID(Convert.ToInt64(ViewState["WLID"]));
        if(objWL!=null)
        {
        EmailConfig eConfig = em.GetByName("Send White Label Code");
        EmailValueCollection objEmailValues = new EmailValueCollection();
        if (eConfig.IsActive)
        {
            objSendmail.FromEmail = ConfigurationManager.AppSettings["HotelSupportEmailID"];

            //Mail to User
            objSendmail.ToEmail = txtEMail.Text;
            objSendmail.Attachment.Add(new System.Net.Mail.Attachment(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "/WLCodeFile/" + objWL.CodeFile + "")));
            //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/JoinToday.html"));
            string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
            EmailConfigMapping emap2 = new EmailConfigMapping();
            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
            objSendmail.Subject = "LMMR-White Label Code";
            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForSendWLCode("Customer"))
            {
                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
            }
            objSendmail.Body = bodymsg;
            objSendmail.SendMail();

            divPopUp.Style.Add("display", "none");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "CloseMailPopUp", "CloseMailPopUp();", true);

            divmsg.Style.Add("display", "block");
            divmsg.Attributes.Add("class", "succesfuly");
            divmsg.InnerHtml = "White label code sent successfully";

        }
        }
    }
    protected void grdWhiteLabel_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdWhiteLabel.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            BindWhiteLabelGrid();
            ApplyPaging();
        }
        catch (Exception ex)
        {

        }
    }
    #endregion
}