﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>
     
<%@ Register Src="UserControl/Frontend/HotelOfTheWeek.ascx" TagName="HotelOfTheWeek"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/Frontend/Benefits.ascx" TagName="Benefits" TagPrefix="uc2" %>
<%@ Register Src="UserControl/Frontend/RecentlyJoinedHotel.ascx" TagName="RecentlyJoinedHotel"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Frontend/NewsSubscriber.ascx" TagName="NewsSubscriber"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <uc1:HotelOfTheWeek ID="HotelOfTheWeek2" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">

    <!-- ClickTale Top part -->
<script type="text/javascript">
    var WRInitTime = (new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->
    <div class="mainbody-left">
        <div class="banner">
            <iframe src="<%= SiteRootPath %>SlideShow.aspx" width="100%" height="100%"></iframe>
            <%--<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab">
                <param name="src" value="LMMR_Film_Clients.mp4">
                <param name="autoplay" value="true">
                <embed src="<%= SiteRootPath %>images/LMMR_Film_Clients.mp4" type="image/x-macpaint" pluginspage="http://www.apple.com/quicktime/download"
                    autoplay="true" width="460" height="300"></embed>
            </object>--%>
        </div>
        <div class="video" id="welcomeDIV" runat="server" visible="false">
            <span style="font-size: large;"><%= GetKeyResult("WELCOME")%> <%= firstName %> <%= lastName %> </span>
            <br />
            <br />
            <table width="100%" border="0">
                <tr>
                    <td><%= welcomeMessage%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="midSegement" runat="server">
        <div class="video">
            <div class="videoleftbody">
                <div class="videobody">
                    <%--<img src="<%= SiteRootPath %>images/con-img1.png" />--%>
                    <iframe width="220" height="139" src="http://www.youtube.com/embed/b2aEE8cIVjo?wmode=transparent&amp;rel=0
" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="video-button">
                    <%= GetKeyResult("FORGUESTS")%>
                </div>
            </div>
            <div class="videorightbody">
                <div class="videobody">
                    <%--<img src="<%= SiteRootPath %>images/con-img2.png" />--%>
                    <iframe width="220" height="139" src="http://www.youtube.com/embed/JxeFefZxGqM?wmode=transparent&amp;rel=0
" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="video-button">
                    <%= GetKeyResult("FORHOTELS")%>
                </div>
            </div>
        </div>
        <div class="mid">
            <div class="left">
                <h2>
                    <%= GetKeyResult("HOWTOBOOKONLINE")%></h2>
                <p>
                    <asp:Label ID="lblReadMoreLeftDesc" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    <asp:HyperLink ID="hypReadMoreLeft" runat="server" ><%= GetKeyResult("ABOUTUS")%></asp:HyperLink>
                </p>
            </div>
            <div class="right">
                <h2>
                    <%= GetKeyResult("HOWTOSENDAREQUEST")%></h2>
                <p>
                    <asp:Label ID="lblReadMoreRightDesc" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    <asp:HyperLink ID="hypReadMoreRight" runat="server" ><%= GetKeyResult("ABOUTUS")%></asp:HyperLink></p>
            </div>
        </div>
        <uc2:Benefits ID="Benefits1" runat="server" />
        </div>
    </div>
    <div class="mainbody-right">
        <div id="beforeLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>

                </div>
            </div>
            <div class="mainbody-right-join" id="divjoinus" runat="server">
                <div class="join">
                    <a runat="server" id="joinToday">
                        <%= GetKeyResult("JOINUSTODAY")%></a></div>
                <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
            </div>



            <div class="mainbody-right-you">
            <a href="http://www.youtube.com/watch?v=rkt3NIWecG8&feature=youtu.be" target="_blank">
                <img src="<%= SiteRootPath %>images/youtube-icon.png" /></a>&nbsp;&nbsp;<a href="http://www.youtube.com/watch?v=U3y7d7yxmvE&feature=relmfu"
                    target="_blank"><img src="<%= SiteRootPath %>images/small-vedio.png" /></a>
        </div>


        </div>
        <div style="width:216px; height:auto; overflow:hidden; margin:0px auto; display:none" id="afterLogin" runat="server">



<div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                
                    <a href="http://needameetingroom.com.beforethehype-001.openminds.be/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                </div>
            </div>

<!--start mainbody-right-afterlogin-body -->
	<div class="mainbody-right-afterlogin-body">
		<div class="mainbody-right-afterlogin-top">
			<div class="mainbody-right-afterlogin-inner">
			<b> <%= GetKeyResult("TODAY")%></b> <asp:Label ID="lstLoginTime" runat="server"></asp:Label>
			<div class="afterlogin-welcome">
			<span><%= GetKeyResult("WELCOME")%></span>
			<br>
			<asp:Label ID="lblLoginUserName" runat="server"></asp:Label> 
			</div>
			</div>
		</div>
		<div class="mainbody-right-afterlogin-bottom">
			<div class="mainbody-right-afterlogin-bottom-inner">
			<asp:LinkButton ID="hypManageProfile" runat="server" OnClick="hypManageProfile_Click"><%= GetKeyResult("MANAGEPROFILE")%></asp:LinkButton> <br/>
			<asp:LinkButton ID="hypChangepassword" runat="server" OnClick="hypChangepassword_Click"><%= GetKeyResult("CHANGEPASSWORD")%></asp:LinkButton><br/>
            <asp:LinkButton ID="hypListBookings" runat="server" OnClick="hypListBookings_Click" Visible="true"><%= GetKeyResult("MYBOOKINGS")%></asp:LinkButton><br/>
            <asp:LinkButton ID="hypListRequests" runat="server" OnClick="hypListRequests_Click" Visible="true"><%= GetKeyResult("MYREQUESTS")%></asp:LinkButton>
			</div>
		</div>
	</div>
	<!--end mainbody-right-afterlogin-body -->
	
	
	
	
	
</div>


        <div style="float:left;margin:10px;">
        <div class="g-plusone" data-size="medium" data-annotation="inline" data-width="300" ></div>
        <!-- Place this tag after the last +1 button tag. -->

<script type="text/javascript">

    (function () {

        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;

        po.src = 'https://apis.google.com/js/plusone.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

    })();

</script>
        </div>
 




        <div class="mainbody-right-news">
            <uc4:NewsSubscriber ID="NewsSubscriber1" runat="server" />
        </div>
        <uc3:RecentlyJoinedHotel ID="RecentlyJoinedHotel1" runat="server" />
    </div>
    <script type="text/javascript" language="javascript">

        function Navigate(val) {

            var url = 'Video.aspx?fid=' + val + '';
            var winName = 'myWindow';
            var w = '460';
            var h = '300';
            var scroll = 'no';
            var popupWindow = null;
            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
            settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
            popupWindow = window.open(url, winName, settings)
            return false;
        }

    </script>
   
<!-- ClickTale Bottom part -->
<div id="ClickTaleDiv" style="display: none;"></div>
<script type="text/javascript">
    if (document.location.protocol != 'https:')
        document.write(unescape("%3Cscript%20src='http://s.clicktale.net/WRd.js'%20type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    if (typeof ClickTale == 'function') ClickTale(24322, 0.0586, "www02");
</script>
<!-- ClickTale end of Bottom part -->
</asp:Content>
