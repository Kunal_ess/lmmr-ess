﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;

public partial class Sales_Main : System.Web.UI.MasterPage
{
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CurrentSalesPersonID"] == null)
            {
                //Response.Redirect("~/login.aspx");
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            if (Session["CurrentSalesPerson"] != null)
            {
                Users objUsers = (Users)Session["CurrentSalesPerson"];
                if (objUsers.Usertype == (int)Usertype.Salesperson)
                {
                    lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                    objUsers.LastLogin = DateTime.Now;
                    lstLoginTime.Text = DateTime.Now.ToLongDateString();
                }
                else
                {
                    Session.Remove("CurrentSalesPerson");
                    //Response.Redirect("../Login.aspx");
                    Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "login/english");
                    }
                }
            }
        }
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        Session.Remove("CurrentOperator");
        Session.Remove("CurrentOperatorID");
        Session.Remove("CurrentUserID");
        Session.Remove("CurrentUser");
        Session.Remove("CurrentHotelID");
        Session.Remove("CurrentSuperAdminID");
        Session.Remove("CurrentSuperAdmin");
        Session.Remove("CurrentSalesPerson");
        Session.Remove("CurrentSalesPersonID");
        //Session.Abandon();
        //Response.Redirect("~/login.aspx");
        Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
    }
}
