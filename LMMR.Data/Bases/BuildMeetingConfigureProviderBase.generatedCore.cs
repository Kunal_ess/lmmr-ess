﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BuildMeetingConfigureProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BuildMeetingConfigureProviderBaseCore : EntityProviderBase<LMMR.Entities.BuildMeetingConfigure, LMMR.Entities.BuildMeetingConfigureKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BuildMeetingConfigureKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">This table is used for other packages like Extra and Build meeting room. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">This table is used for other packages like Extra and Build meeting room. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_BookedMeetingRoom key.
		///		FK_BuildMeetingConfigure_BookedMeetingRoom Description: 
		/// </summary>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByBookedMeetingRoomId(System.Int64? _bookedMeetingRoomId)
		{
			int count = -1;
			return GetByBookedMeetingRoomId(_bookedMeetingRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_BookedMeetingRoom key.
		///		FK_BuildMeetingConfigure_BookedMeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		/// <remarks></remarks>
		public TList<BuildMeetingConfigure> GetByBookedMeetingRoomId(TransactionManager transactionManager, System.Int64? _bookedMeetingRoomId)
		{
			int count = -1;
			return GetByBookedMeetingRoomId(transactionManager, _bookedMeetingRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_BookedMeetingRoom key.
		///		FK_BuildMeetingConfigure_BookedMeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByBookedMeetingRoomId(TransactionManager transactionManager, System.Int64? _bookedMeetingRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByBookedMeetingRoomId(transactionManager, _bookedMeetingRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_BookedMeetingRoom key.
		///		fkBuildMeetingConfigureBookedMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByBookedMeetingRoomId(System.Int64? _bookedMeetingRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBookedMeetingRoomId(null, _bookedMeetingRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_BookedMeetingRoom key.
		///		fkBuildMeetingConfigureBookedMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByBookedMeetingRoomId(System.Int64? _bookedMeetingRoomId, int start, int pageLength,out int count)
		{
			return GetByBookedMeetingRoomId(null, _bookedMeetingRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_BookedMeetingRoom key.
		///		FK_BuildMeetingConfigure_BookedMeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public abstract TList<BuildMeetingConfigure> GetByBookedMeetingRoomId(TransactionManager transactionManager, System.Int64? _bookedMeetingRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_Booking key.
		///		FK_BuildMeetingConfigure_Booking Description: 
		/// </summary>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByBookingId(System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(_bookingId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_Booking key.
		///		FK_BuildMeetingConfigure_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		/// <remarks></remarks>
		public TList<BuildMeetingConfigure> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_Booking key.
		///		FK_BuildMeetingConfigure_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_Booking key.
		///		fkBuildMeetingConfigureBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByBookingId(System.Int64 _bookingId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBookingId(null, _bookingId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_Booking key.
		///		fkBuildMeetingConfigureBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByBookingId(System.Int64 _bookingId, int start, int pageLength,out int count)
		{
			return GetByBookingId(null, _bookingId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_Booking key.
		///		FK_BuildMeetingConfigure_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public abstract TList<BuildMeetingConfigure> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_PackageMaster key.
		///		FK_BuildMeetingConfigure_PackageMaster Description: 
		/// </summary>
		/// <param name="_packageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByPackageId(System.Int64? _packageId)
		{
			int count = -1;
			return GetByPackageId(_packageId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_PackageMaster key.
		///		FK_BuildMeetingConfigure_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		/// <remarks></remarks>
		public TList<BuildMeetingConfigure> GetByPackageId(TransactionManager transactionManager, System.Int64? _packageId)
		{
			int count = -1;
			return GetByPackageId(transactionManager, _packageId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_PackageMaster key.
		///		FK_BuildMeetingConfigure_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByPackageId(TransactionManager transactionManager, System.Int64? _packageId, int start, int pageLength)
		{
			int count = -1;
			return GetByPackageId(transactionManager, _packageId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_PackageMaster key.
		///		fkBuildMeetingConfigurePackageMaster Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_packageId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByPackageId(System.Int64? _packageId, int start, int pageLength)
		{
			int count =  -1;
			return GetByPackageId(null, _packageId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_PackageMaster key.
		///		fkBuildMeetingConfigurePackageMaster Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_packageId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public TList<BuildMeetingConfigure> GetByPackageId(System.Int64? _packageId, int start, int pageLength,out int count)
		{
			return GetByPackageId(null, _packageId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildMeetingConfigure_PackageMaster key.
		///		FK_BuildMeetingConfigure_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildMeetingConfigure objects.</returns>
		public abstract TList<BuildMeetingConfigure> GetByPackageId(TransactionManager transactionManager, System.Int64? _packageId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BuildMeetingConfigure Get(TransactionManager transactionManager, LMMR.Entities.BuildMeetingConfigureKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BuildMeetingConfigure index.
		/// </summary>
		/// <param name="_id">This table is used for other packages like Extra and Build meeting room</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildMeetingConfigure"/> class.</returns>
		public LMMR.Entities.BuildMeetingConfigure GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildMeetingConfigure index.
		/// </summary>
		/// <param name="_id">This table is used for other packages like Extra and Build meeting room</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildMeetingConfigure"/> class.</returns>
		public LMMR.Entities.BuildMeetingConfigure GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildMeetingConfigure index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">This table is used for other packages like Extra and Build meeting room</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildMeetingConfigure"/> class.</returns>
		public LMMR.Entities.BuildMeetingConfigure GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildMeetingConfigure index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">This table is used for other packages like Extra and Build meeting room</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildMeetingConfigure"/> class.</returns>
		public LMMR.Entities.BuildMeetingConfigure GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildMeetingConfigure index.
		/// </summary>
		/// <param name="_id">This table is used for other packages like Extra and Build meeting room</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildMeetingConfigure"/> class.</returns>
		public LMMR.Entities.BuildMeetingConfigure GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildMeetingConfigure index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">This table is used for other packages like Extra and Build meeting room</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildMeetingConfigure"/> class.</returns>
		public abstract LMMR.Entities.BuildMeetingConfigure GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BuildMeetingConfigure&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BuildMeetingConfigure&gt;"/></returns>
		public static TList<BuildMeetingConfigure> Fill(IDataReader reader, TList<BuildMeetingConfigure> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BuildMeetingConfigure c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BuildMeetingConfigure")
					.Append("|").Append((System.Int64)reader[((int)BuildMeetingConfigureColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BuildMeetingConfigure>(
					key.ToString(), // EntityTrackingKey
					"BuildMeetingConfigure",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BuildMeetingConfigure();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BuildMeetingConfigureColumn.Id - 1)];
					c.BookingId = (System.Int64)reader[((int)BuildMeetingConfigureColumn.BookingId - 1)];
					c.PackageId = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.PackageId - 1)))?null:(System.Int64?)reader[((int)BuildMeetingConfigureColumn.PackageId - 1)];
					c.ServeTime = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.ServeTime - 1)))?null:(System.String)reader[((int)BuildMeetingConfigureColumn.ServeTime - 1)];
					c.Quntity = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.Quntity - 1)))?null:(System.Int64?)reader[((int)BuildMeetingConfigureColumn.Quntity - 1)];
					c.TotalPrice = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.TotalPrice - 1)))?null:(System.Decimal?)reader[((int)BuildMeetingConfigureColumn.TotalPrice - 1)];
					c.ConfigureType = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.ConfigureType - 1)))?null:(System.Int32?)reader[((int)BuildMeetingConfigureColumn.ConfigureType - 1)];
					c.BookedMeetingRoomId = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.BookedMeetingRoomId - 1)))?null:(System.Int64?)reader[((int)BuildMeetingConfigureColumn.BookedMeetingRoomId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BuildMeetingConfigure"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildMeetingConfigure"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BuildMeetingConfigure entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BuildMeetingConfigureColumn.Id - 1)];
			entity.BookingId = (System.Int64)reader[((int)BuildMeetingConfigureColumn.BookingId - 1)];
			entity.PackageId = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.PackageId - 1)))?null:(System.Int64?)reader[((int)BuildMeetingConfigureColumn.PackageId - 1)];
			entity.ServeTime = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.ServeTime - 1)))?null:(System.String)reader[((int)BuildMeetingConfigureColumn.ServeTime - 1)];
			entity.Quntity = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.Quntity - 1)))?null:(System.Int64?)reader[((int)BuildMeetingConfigureColumn.Quntity - 1)];
			entity.TotalPrice = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.TotalPrice - 1)))?null:(System.Decimal?)reader[((int)BuildMeetingConfigureColumn.TotalPrice - 1)];
			entity.ConfigureType = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.ConfigureType - 1)))?null:(System.Int32?)reader[((int)BuildMeetingConfigureColumn.ConfigureType - 1)];
			entity.BookedMeetingRoomId = (reader.IsDBNull(((int)BuildMeetingConfigureColumn.BookedMeetingRoomId - 1)))?null:(System.Int64?)reader[((int)BuildMeetingConfigureColumn.BookedMeetingRoomId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BuildMeetingConfigure"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildMeetingConfigure"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BuildMeetingConfigure entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BookingId = (System.Int64)dataRow["BookingId"];
			entity.PackageId = Convert.IsDBNull(dataRow["PackageId"]) ? null : (System.Int64?)dataRow["PackageId"];
			entity.ServeTime = Convert.IsDBNull(dataRow["ServeTime"]) ? null : (System.String)dataRow["ServeTime"];
			entity.Quntity = Convert.IsDBNull(dataRow["Quntity"]) ? null : (System.Int64?)dataRow["Quntity"];
			entity.TotalPrice = Convert.IsDBNull(dataRow["TotalPrice"]) ? null : (System.Decimal?)dataRow["TotalPrice"];
			entity.ConfigureType = Convert.IsDBNull(dataRow["ConfigureType"]) ? null : (System.Int32?)dataRow["ConfigureType"];
			entity.BookedMeetingRoomId = Convert.IsDBNull(dataRow["BookedMeetingRoomId"]) ? null : (System.Int64?)dataRow["BookedMeetingRoomId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildMeetingConfigure"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BuildMeetingConfigure Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BuildMeetingConfigure entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BookedMeetingRoomIdSource	
			if (CanDeepLoad(entity, "BookedMeetingRoom|BookedMeetingRoomIdSource", deepLoadType, innerList) 
				&& entity.BookedMeetingRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.BookedMeetingRoomId ?? (long)0);
				BookedMeetingRoom tmpEntity = EntityManager.LocateEntity<BookedMeetingRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(BookedMeetingRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BookedMeetingRoomIdSource = tmpEntity;
				else
					entity.BookedMeetingRoomIdSource = DataRepository.BookedMeetingRoomProvider.GetById(transactionManager, (entity.BookedMeetingRoomId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookedMeetingRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BookedMeetingRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookedMeetingRoomProvider.DeepLoad(transactionManager, entity.BookedMeetingRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BookedMeetingRoomIdSource

			#region BookingIdSource	
			if (CanDeepLoad(entity, "Booking|BookingIdSource", deepLoadType, innerList) 
				&& entity.BookingIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BookingId;
				Booking tmpEntity = EntityManager.LocateEntity<Booking>(EntityLocator.ConstructKeyFromPkItems(typeof(Booking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BookingIdSource = tmpEntity;
				else
					entity.BookingIdSource = DataRepository.BookingProvider.GetById(transactionManager, entity.BookingId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BookingIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookingProvider.DeepLoad(transactionManager, entity.BookingIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BookingIdSource

			#region PackageIdSource	
			if (CanDeepLoad(entity, "PackageItems|PackageIdSource", deepLoadType, innerList) 
				&& entity.PackageIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PackageId ?? (long)0);
				PackageItems tmpEntity = EntityManager.LocateEntity<PackageItems>(EntityLocator.ConstructKeyFromPkItems(typeof(PackageItems), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PackageIdSource = tmpEntity;
				else
					entity.PackageIdSource = DataRepository.PackageItemsProvider.GetById(transactionManager, (entity.PackageId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PackageIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PackageItemsProvider.DeepLoad(transactionManager, entity.PackageIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PackageIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BuildMeetingConfigure object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BuildMeetingConfigure instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BuildMeetingConfigure Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BuildMeetingConfigure entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BookedMeetingRoomIdSource
			if (CanDeepSave(entity, "BookedMeetingRoom|BookedMeetingRoomIdSource", deepSaveType, innerList) 
				&& entity.BookedMeetingRoomIdSource != null)
			{
				DataRepository.BookedMeetingRoomProvider.Save(transactionManager, entity.BookedMeetingRoomIdSource);
				entity.BookedMeetingRoomId = entity.BookedMeetingRoomIdSource.Id;
			}
			#endregion 
			
			#region BookingIdSource
			if (CanDeepSave(entity, "Booking|BookingIdSource", deepSaveType, innerList) 
				&& entity.BookingIdSource != null)
			{
				DataRepository.BookingProvider.Save(transactionManager, entity.BookingIdSource);
				entity.BookingId = entity.BookingIdSource.Id;
			}
			#endregion 
			
			#region PackageIdSource
			if (CanDeepSave(entity, "PackageItems|PackageIdSource", deepSaveType, innerList) 
				&& entity.PackageIdSource != null)
			{
				DataRepository.PackageItemsProvider.Save(transactionManager, entity.PackageIdSource);
				entity.PackageId = entity.PackageIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BuildMeetingConfigureChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BuildMeetingConfigure</c>
	///</summary>
	public enum BuildMeetingConfigureChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BookedMeetingRoom</c> at BookedMeetingRoomIdSource
		///</summary>
		[ChildEntityType(typeof(BookedMeetingRoom))]
		BookedMeetingRoom,
			
		///<summary>
		/// Composite Property for <c>Booking</c> at BookingIdSource
		///</summary>
		[ChildEntityType(typeof(Booking))]
		Booking,
			
		///<summary>
		/// Composite Property for <c>PackageItems</c> at PackageIdSource
		///</summary>
		[ChildEntityType(typeof(PackageItems))]
		PackageItems,
		}
	
	#endregion BuildMeetingConfigureChildEntityTypes
	
	#region BuildMeetingConfigureFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BuildMeetingConfigureColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildMeetingConfigure"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildMeetingConfigureFilterBuilder : SqlFilterBuilder<BuildMeetingConfigureColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureFilterBuilder class.
		/// </summary>
		public BuildMeetingConfigureFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildMeetingConfigureFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildMeetingConfigureFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildMeetingConfigureFilterBuilder
	
	#region BuildMeetingConfigureParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BuildMeetingConfigureColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildMeetingConfigure"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildMeetingConfigureParameterBuilder : ParameterizedSqlFilterBuilder<BuildMeetingConfigureColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureParameterBuilder class.
		/// </summary>
		public BuildMeetingConfigureParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildMeetingConfigureParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildMeetingConfigureParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildMeetingConfigureParameterBuilder
	
	#region BuildMeetingConfigureSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BuildMeetingConfigureColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildMeetingConfigure"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BuildMeetingConfigureSortBuilder : SqlSortBuilder<BuildMeetingConfigureColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureSqlSortBuilder class.
		/// </summary>
		public BuildMeetingConfigureSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BuildMeetingConfigureSortBuilder
	
} // end namespace
