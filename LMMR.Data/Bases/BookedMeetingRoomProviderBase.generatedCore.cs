﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BookedMeetingRoomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BookedMeetingRoomProviderBaseCore : EntityProviderBase<LMMR.Entities.BookedMeetingRoom, LMMR.Entities.BookedMeetingRoomKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BookedMeetingRoomKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_Booking key.
		///		FK_BookedMeetingRoom_Booking Description: 
		/// </summary>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByBookingId(System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(_bookingId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_Booking key.
		///		FK_BookedMeetingRoom_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		/// <remarks></remarks>
		public TList<BookedMeetingRoom> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_Booking key.
		///		FK_BookedMeetingRoom_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_Booking key.
		///		fkBookedMeetingRoomBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByBookingId(System.Int64 _bookingId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBookingId(null, _bookingId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_Booking key.
		///		fkBookedMeetingRoomBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByBookingId(System.Int64 _bookingId, int start, int pageLength,out int count)
		{
			return GetByBookingId(null, _bookingId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_Booking key.
		///		FK_BookedMeetingRoom_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public abstract TList<BookedMeetingRoom> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoom key.
		///		FK_BookedMeetingRoom_MeetingRoom Description: 
		/// </summary>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByMeetingRoomId(System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(_meetingRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoom key.
		///		FK_BookedMeetingRoom_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		/// <remarks></remarks>
		public TList<BookedMeetingRoom> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoom key.
		///		FK_BookedMeetingRoom_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoom key.
		///		fkBookedMeetingRoomMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoom key.
		///		fkBookedMeetingRoomMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength,out int count)
		{
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoom key.
		///		FK_BookedMeetingRoom_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public abstract TList<BookedMeetingRoom> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoomConfig key.
		///		FK_BookedMeetingRoom_MeetingRoomConfig Description: 
		/// </summary>
		/// <param name="_meetingRoomConfigId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByMeetingRoomConfigId(System.Int64 _meetingRoomConfigId)
		{
			int count = -1;
			return GetByMeetingRoomConfigId(_meetingRoomConfigId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoomConfig key.
		///		FK_BookedMeetingRoom_MeetingRoomConfig Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomConfigId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		/// <remarks></remarks>
		public TList<BookedMeetingRoom> GetByMeetingRoomConfigId(TransactionManager transactionManager, System.Int64 _meetingRoomConfigId)
		{
			int count = -1;
			return GetByMeetingRoomConfigId(transactionManager, _meetingRoomConfigId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoomConfig key.
		///		FK_BookedMeetingRoom_MeetingRoomConfig Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomConfigId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByMeetingRoomConfigId(TransactionManager transactionManager, System.Int64 _meetingRoomConfigId, int start, int pageLength)
		{
			int count = -1;
			return GetByMeetingRoomConfigId(transactionManager, _meetingRoomConfigId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoomConfig key.
		///		fkBookedMeetingRoomMeetingRoomConfig Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomConfigId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByMeetingRoomConfigId(System.Int64 _meetingRoomConfigId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMeetingRoomConfigId(null, _meetingRoomConfigId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoomConfig key.
		///		fkBookedMeetingRoomMeetingRoomConfig Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomConfigId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public TList<BookedMeetingRoom> GetByMeetingRoomConfigId(System.Int64 _meetingRoomConfigId, int start, int pageLength,out int count)
		{
			return GetByMeetingRoomConfigId(null, _meetingRoomConfigId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedMeetingRoom_MeetingRoomConfig key.
		///		FK_BookedMeetingRoom_MeetingRoomConfig Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomConfigId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedMeetingRoom objects.</returns>
		public abstract TList<BookedMeetingRoom> GetByMeetingRoomConfigId(TransactionManager transactionManager, System.Int64 _meetingRoomConfigId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BookedMeetingRoom Get(TransactionManager transactionManager, LMMR.Entities.BookedMeetingRoomKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BookedMeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedMeetingRoom"/> class.</returns>
		public LMMR.Entities.BookedMeetingRoom GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedMeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedMeetingRoom"/> class.</returns>
		public LMMR.Entities.BookedMeetingRoom GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedMeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedMeetingRoom"/> class.</returns>
		public LMMR.Entities.BookedMeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedMeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedMeetingRoom"/> class.</returns>
		public LMMR.Entities.BookedMeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedMeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedMeetingRoom"/> class.</returns>
		public LMMR.Entities.BookedMeetingRoom GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedMeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedMeetingRoom"/> class.</returns>
		public abstract LMMR.Entities.BookedMeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BookedMeetingRoom&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BookedMeetingRoom&gt;"/></returns>
		public static TList<BookedMeetingRoom> Fill(IDataReader reader, TList<BookedMeetingRoom> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BookedMeetingRoom c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BookedMeetingRoom")
					.Append("|").Append((System.Int64)reader[((int)BookedMeetingRoomColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BookedMeetingRoom>(
					key.ToString(), // EntityTrackingKey
					"BookedMeetingRoom",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BookedMeetingRoom();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BookedMeetingRoomColumn.Id - 1)];
					c.BookingId = (System.Int64)reader[((int)BookedMeetingRoomColumn.BookingId - 1)];
					c.MeetingRoomId = (System.Int64)reader[((int)BookedMeetingRoomColumn.MeetingRoomId - 1)];
					c.MeetingRoomConfigId = (System.Int64)reader[((int)BookedMeetingRoomColumn.MeetingRoomConfigId - 1)];
					c.MeetingDay = (reader.IsDBNull(((int)BookedMeetingRoomColumn.MeetingDay - 1)))?null:(System.Int32?)reader[((int)BookedMeetingRoomColumn.MeetingDay - 1)];
					c.StartTime = (reader.IsDBNull(((int)BookedMeetingRoomColumn.StartTime - 1)))?null:(System.String)reader[((int)BookedMeetingRoomColumn.StartTime - 1)];
					c.EndTime = (reader.IsDBNull(((int)BookedMeetingRoomColumn.EndTime - 1)))?null:(System.String)reader[((int)BookedMeetingRoomColumn.EndTime - 1)];
					c.NoofParticipants = (reader.IsDBNull(((int)BookedMeetingRoomColumn.NoofParticipants - 1)))?null:(System.Int64?)reader[((int)BookedMeetingRoomColumn.NoofParticipants - 1)];
					c.MeetingDate = (reader.IsDBNull(((int)BookedMeetingRoomColumn.MeetingDate - 1)))?null:(System.DateTime?)reader[((int)BookedMeetingRoomColumn.MeetingDate - 1)];
					c.TotalPrice = (reader.IsDBNull(((int)BookedMeetingRoomColumn.TotalPrice - 1)))?null:(System.Decimal?)reader[((int)BookedMeetingRoomColumn.TotalPrice - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BookedMeetingRoom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BookedMeetingRoom"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BookedMeetingRoom entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BookedMeetingRoomColumn.Id - 1)];
			entity.BookingId = (System.Int64)reader[((int)BookedMeetingRoomColumn.BookingId - 1)];
			entity.MeetingRoomId = (System.Int64)reader[((int)BookedMeetingRoomColumn.MeetingRoomId - 1)];
			entity.MeetingRoomConfigId = (System.Int64)reader[((int)BookedMeetingRoomColumn.MeetingRoomConfigId - 1)];
			entity.MeetingDay = (reader.IsDBNull(((int)BookedMeetingRoomColumn.MeetingDay - 1)))?null:(System.Int32?)reader[((int)BookedMeetingRoomColumn.MeetingDay - 1)];
			entity.StartTime = (reader.IsDBNull(((int)BookedMeetingRoomColumn.StartTime - 1)))?null:(System.String)reader[((int)BookedMeetingRoomColumn.StartTime - 1)];
			entity.EndTime = (reader.IsDBNull(((int)BookedMeetingRoomColumn.EndTime - 1)))?null:(System.String)reader[((int)BookedMeetingRoomColumn.EndTime - 1)];
			entity.NoofParticipants = (reader.IsDBNull(((int)BookedMeetingRoomColumn.NoofParticipants - 1)))?null:(System.Int64?)reader[((int)BookedMeetingRoomColumn.NoofParticipants - 1)];
			entity.MeetingDate = (reader.IsDBNull(((int)BookedMeetingRoomColumn.MeetingDate - 1)))?null:(System.DateTime?)reader[((int)BookedMeetingRoomColumn.MeetingDate - 1)];
			entity.TotalPrice = (reader.IsDBNull(((int)BookedMeetingRoomColumn.TotalPrice - 1)))?null:(System.Decimal?)reader[((int)BookedMeetingRoomColumn.TotalPrice - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BookedMeetingRoom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BookedMeetingRoom"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BookedMeetingRoom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BookingId = (System.Int64)dataRow["BookingId"];
			entity.MeetingRoomId = (System.Int64)dataRow["MeetingRoomId"];
			entity.MeetingRoomConfigId = (System.Int64)dataRow["MeetingRoomConfigId"];
			entity.MeetingDay = Convert.IsDBNull(dataRow["MeetingDay"]) ? null : (System.Int32?)dataRow["MeetingDay"];
			entity.StartTime = Convert.IsDBNull(dataRow["StartTime"]) ? null : (System.String)dataRow["StartTime"];
			entity.EndTime = Convert.IsDBNull(dataRow["EndTime"]) ? null : (System.String)dataRow["EndTime"];
			entity.NoofParticipants = Convert.IsDBNull(dataRow["NoofParticipants"]) ? null : (System.Int64?)dataRow["NoofParticipants"];
			entity.MeetingDate = Convert.IsDBNull(dataRow["MeetingDate"]) ? null : (System.DateTime?)dataRow["MeetingDate"];
			entity.TotalPrice = Convert.IsDBNull(dataRow["TotalPrice"]) ? null : (System.Decimal?)dataRow["TotalPrice"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BookedMeetingRoom"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BookedMeetingRoom Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BookedMeetingRoom entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BookingIdSource	
			if (CanDeepLoad(entity, "Booking|BookingIdSource", deepLoadType, innerList) 
				&& entity.BookingIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BookingId;
				Booking tmpEntity = EntityManager.LocateEntity<Booking>(EntityLocator.ConstructKeyFromPkItems(typeof(Booking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BookingIdSource = tmpEntity;
				else
					entity.BookingIdSource = DataRepository.BookingProvider.GetById(transactionManager, entity.BookingId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BookingIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookingProvider.DeepLoad(transactionManager, entity.BookingIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BookingIdSource

			#region MeetingRoomIdSource	
			if (CanDeepLoad(entity, "MeetingRoom|MeetingRoomIdSource", deepLoadType, innerList) 
				&& entity.MeetingRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MeetingRoomId;
				MeetingRoom tmpEntity = EntityManager.LocateEntity<MeetingRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MeetingRoomIdSource = tmpEntity;
				else
					entity.MeetingRoomIdSource = DataRepository.MeetingRoomProvider.GetById(transactionManager, entity.MeetingRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MeetingRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomProvider.DeepLoad(transactionManager, entity.MeetingRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MeetingRoomIdSource

			#region MeetingRoomConfigIdSource	
			if (CanDeepLoad(entity, "MeetingRoomConfig|MeetingRoomConfigIdSource", deepLoadType, innerList) 
				&& entity.MeetingRoomConfigIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MeetingRoomConfigId;
				MeetingRoomConfig tmpEntity = EntityManager.LocateEntity<MeetingRoomConfig>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoomConfig), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MeetingRoomConfigIdSource = tmpEntity;
				else
					entity.MeetingRoomConfigIdSource = DataRepository.MeetingRoomConfigProvider.GetById(transactionManager, entity.MeetingRoomConfigId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomConfigIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MeetingRoomConfigIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomConfigProvider.DeepLoad(transactionManager, entity.MeetingRoomConfigIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MeetingRoomConfigIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region BuildPackageConfigureCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BuildPackageConfigure>|BuildPackageConfigureCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildPackageConfigureCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BuildPackageConfigureCollection = DataRepository.BuildPackageConfigureProvider.GetByBookedMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.BuildPackageConfigureCollection.Count > 0)
				{
					deepHandles.Add("BuildPackageConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BuildPackageConfigure>) DataRepository.BuildPackageConfigureProvider.DeepLoad,
						new object[] { transactionManager, entity.BuildPackageConfigureCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BuildMeetingConfigureCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BuildMeetingConfigure>|BuildMeetingConfigureCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildMeetingConfigureCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BuildMeetingConfigureCollection = DataRepository.BuildMeetingConfigureProvider.GetByBookedMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.BuildMeetingConfigureCollection.Count > 0)
				{
					deepHandles.Add("BuildMeetingConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BuildMeetingConfigure>) DataRepository.BuildMeetingConfigureProvider.DeepLoad,
						new object[] { transactionManager, entity.BuildMeetingConfigureCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BookedMeetingRoom object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BookedMeetingRoom instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BookedMeetingRoom Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BookedMeetingRoom entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BookingIdSource
			if (CanDeepSave(entity, "Booking|BookingIdSource", deepSaveType, innerList) 
				&& entity.BookingIdSource != null)
			{
				DataRepository.BookingProvider.Save(transactionManager, entity.BookingIdSource);
				entity.BookingId = entity.BookingIdSource.Id;
			}
			#endregion 
			
			#region MeetingRoomIdSource
			if (CanDeepSave(entity, "MeetingRoom|MeetingRoomIdSource", deepSaveType, innerList) 
				&& entity.MeetingRoomIdSource != null)
			{
				DataRepository.MeetingRoomProvider.Save(transactionManager, entity.MeetingRoomIdSource);
				entity.MeetingRoomId = entity.MeetingRoomIdSource.Id;
			}
			#endregion 
			
			#region MeetingRoomConfigIdSource
			if (CanDeepSave(entity, "MeetingRoomConfig|MeetingRoomConfigIdSource", deepSaveType, innerList) 
				&& entity.MeetingRoomConfigIdSource != null)
			{
				DataRepository.MeetingRoomConfigProvider.Save(transactionManager, entity.MeetingRoomConfigIdSource);
				entity.MeetingRoomConfigId = entity.MeetingRoomConfigIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<BuildPackageConfigure>
				if (CanDeepSave(entity.BuildPackageConfigureCollection, "List<BuildPackageConfigure>|BuildPackageConfigureCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BuildPackageConfigure child in entity.BuildPackageConfigureCollection)
					{
						if(child.BookedMeetingRoomIdSource != null)
						{
							child.BookedMeetingRoomId = child.BookedMeetingRoomIdSource.Id;
						}
						else
						{
							child.BookedMeetingRoomId = entity.Id;
						}

					}

					if (entity.BuildPackageConfigureCollection.Count > 0 || entity.BuildPackageConfigureCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BuildPackageConfigureProvider.Save(transactionManager, entity.BuildPackageConfigureCollection);
						
						deepHandles.Add("BuildPackageConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BuildPackageConfigure >) DataRepository.BuildPackageConfigureProvider.DeepSave,
							new object[] { transactionManager, entity.BuildPackageConfigureCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BuildMeetingConfigure>
				if (CanDeepSave(entity.BuildMeetingConfigureCollection, "List<BuildMeetingConfigure>|BuildMeetingConfigureCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BuildMeetingConfigure child in entity.BuildMeetingConfigureCollection)
					{
						if(child.BookedMeetingRoomIdSource != null)
						{
							child.BookedMeetingRoomId = child.BookedMeetingRoomIdSource.Id;
						}
						else
						{
							child.BookedMeetingRoomId = entity.Id;
						}

					}

					if (entity.BuildMeetingConfigureCollection.Count > 0 || entity.BuildMeetingConfigureCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BuildMeetingConfigureProvider.Save(transactionManager, entity.BuildMeetingConfigureCollection);
						
						deepHandles.Add("BuildMeetingConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BuildMeetingConfigure >) DataRepository.BuildMeetingConfigureProvider.DeepSave,
							new object[] { transactionManager, entity.BuildMeetingConfigureCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BookedMeetingRoomChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BookedMeetingRoom</c>
	///</summary>
	public enum BookedMeetingRoomChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Booking</c> at BookingIdSource
		///</summary>
		[ChildEntityType(typeof(Booking))]
		Booking,
			
		///<summary>
		/// Composite Property for <c>MeetingRoom</c> at MeetingRoomIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoom))]
		MeetingRoom,
			
		///<summary>
		/// Composite Property for <c>MeetingRoomConfig</c> at MeetingRoomConfigIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoomConfig))]
		MeetingRoomConfig,
	
		///<summary>
		/// Collection of <c>BookedMeetingRoom</c> as OneToMany for BuildPackageConfigureCollection
		///</summary>
		[ChildEntityType(typeof(TList<BuildPackageConfigure>))]
		BuildPackageConfigureCollection,

		///<summary>
		/// Collection of <c>BookedMeetingRoom</c> as OneToMany for BuildMeetingConfigureCollection
		///</summary>
		[ChildEntityType(typeof(TList<BuildMeetingConfigure>))]
		BuildMeetingConfigureCollection,
	}
	
	#endregion BookedMeetingRoomChildEntityTypes
	
	#region BookedMeetingRoomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BookedMeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookedMeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookedMeetingRoomFilterBuilder : SqlFilterBuilder<BookedMeetingRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomFilterBuilder class.
		/// </summary>
		public BookedMeetingRoomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookedMeetingRoomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookedMeetingRoomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookedMeetingRoomFilterBuilder
	
	#region BookedMeetingRoomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BookedMeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookedMeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookedMeetingRoomParameterBuilder : ParameterizedSqlFilterBuilder<BookedMeetingRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomParameterBuilder class.
		/// </summary>
		public BookedMeetingRoomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookedMeetingRoomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookedMeetingRoomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookedMeetingRoomParameterBuilder
	
	#region BookedMeetingRoomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BookedMeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookedMeetingRoom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BookedMeetingRoomSortBuilder : SqlSortBuilder<BookedMeetingRoomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomSqlSortBuilder class.
		/// </summary>
		public BookedMeetingRoomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BookedMeetingRoomSortBuilder
	
} // end namespace
