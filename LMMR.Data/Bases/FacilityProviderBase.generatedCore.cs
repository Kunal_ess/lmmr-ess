﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FacilityProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FacilityProviderBaseCore : EntityProviderBase<LMMR.Entities.Facility, LMMR.Entities.FacilityKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.FacilityKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Facility_FacilityType key.
		///		FK_Facility_FacilityType Description: 
		/// </summary>
		/// <param name="_facilityTypeId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Facility objects.</returns>
		public TList<Facility> GetByFacilityTypeId(System.Int64 _facilityTypeId)
		{
			int count = -1;
			return GetByFacilityTypeId(_facilityTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Facility_FacilityType key.
		///		FK_Facility_FacilityType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facilityTypeId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Facility objects.</returns>
		/// <remarks></remarks>
		public TList<Facility> GetByFacilityTypeId(TransactionManager transactionManager, System.Int64 _facilityTypeId)
		{
			int count = -1;
			return GetByFacilityTypeId(transactionManager, _facilityTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Facility_FacilityType key.
		///		FK_Facility_FacilityType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facilityTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Facility objects.</returns>
		public TList<Facility> GetByFacilityTypeId(TransactionManager transactionManager, System.Int64 _facilityTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByFacilityTypeId(transactionManager, _facilityTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Facility_FacilityType key.
		///		fkFacilityFacilityType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_facilityTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Facility objects.</returns>
		public TList<Facility> GetByFacilityTypeId(System.Int64 _facilityTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByFacilityTypeId(null, _facilityTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Facility_FacilityType key.
		///		fkFacilityFacilityType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_facilityTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Facility objects.</returns>
		public TList<Facility> GetByFacilityTypeId(System.Int64 _facilityTypeId, int start, int pageLength,out int count)
		{
			return GetByFacilityTypeId(null, _facilityTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Facility_FacilityType key.
		///		FK_Facility_FacilityType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facilityTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Facility objects.</returns>
		public abstract TList<Facility> GetByFacilityTypeId(TransactionManager transactionManager, System.Int64 _facilityTypeId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Facility Get(TransactionManager transactionManager, LMMR.Entities.FacilityKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Facility index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Facility"/> class.</returns>
		public LMMR.Entities.Facility GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Facility index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Facility"/> class.</returns>
		public LMMR.Entities.Facility GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Facility index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Facility"/> class.</returns>
		public LMMR.Entities.Facility GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Facility index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Facility"/> class.</returns>
		public LMMR.Entities.Facility GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Facility index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Facility"/> class.</returns>
		public LMMR.Entities.Facility GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Facility index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Facility"/> class.</returns>
		public abstract LMMR.Entities.Facility GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Facility&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Facility&gt;"/></returns>
		public static TList<Facility> Fill(IDataReader reader, TList<Facility> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Facility c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Facility")
					.Append("|").Append((System.Int64)reader[((int)FacilityColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Facility>(
					key.ToString(), // EntityTrackingKey
					"Facility",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Facility();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)FacilityColumn.Id - 1)];
					c.FacilityTypeId = (System.Int64)reader[((int)FacilityColumn.FacilityTypeId - 1)];
					c.FacilityName = (reader.IsDBNull(((int)FacilityColumn.FacilityName - 1)))?null:(System.String)reader[((int)FacilityColumn.FacilityName - 1)];
					c.Icon = (reader.IsDBNull(((int)FacilityColumn.Icon - 1)))?null:(System.String)reader[((int)FacilityColumn.Icon - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Facility"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Facility"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Facility entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)FacilityColumn.Id - 1)];
			entity.FacilityTypeId = (System.Int64)reader[((int)FacilityColumn.FacilityTypeId - 1)];
			entity.FacilityName = (reader.IsDBNull(((int)FacilityColumn.FacilityName - 1)))?null:(System.String)reader[((int)FacilityColumn.FacilityName - 1)];
			entity.Icon = (reader.IsDBNull(((int)FacilityColumn.Icon - 1)))?null:(System.String)reader[((int)FacilityColumn.Icon - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Facility"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Facility"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Facility entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.FacilityTypeId = (System.Int64)dataRow["FacilityTypeID"];
			entity.FacilityName = Convert.IsDBNull(dataRow["FacilityName"]) ? null : (System.String)dataRow["FacilityName"];
			entity.Icon = Convert.IsDBNull(dataRow["Icon"]) ? null : (System.String)dataRow["Icon"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Facility"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Facility Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Facility entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region FacilityTypeIdSource	
			if (CanDeepLoad(entity, "FacilityType|FacilityTypeIdSource", deepLoadType, innerList) 
				&& entity.FacilityTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.FacilityTypeId;
				FacilityType tmpEntity = EntityManager.LocateEntity<FacilityType>(EntityLocator.ConstructKeyFromPkItems(typeof(FacilityType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.FacilityTypeIdSource = tmpEntity;
				else
					entity.FacilityTypeIdSource = DataRepository.FacilityTypeProvider.GetById(transactionManager, entity.FacilityTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacilityTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.FacilityTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.FacilityTypeProvider.DeepLoad(transactionManager, entity.FacilityTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion FacilityTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region HotelFacilitiesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelFacilities>|HotelFacilitiesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelFacilitiesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelFacilitiesCollection = DataRepository.HotelFacilitiesProvider.GetByFacilitiesId(transactionManager, entity.Id);

				if (deep && entity.HotelFacilitiesCollection.Count > 0)
				{
					deepHandles.Add("HotelFacilitiesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelFacilities>) DataRepository.HotelFacilitiesProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelFacilitiesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Facility object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Facility instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Facility Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Facility entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region FacilityTypeIdSource
			if (CanDeepSave(entity, "FacilityType|FacilityTypeIdSource", deepSaveType, innerList) 
				&& entity.FacilityTypeIdSource != null)
			{
				DataRepository.FacilityTypeProvider.Save(transactionManager, entity.FacilityTypeIdSource);
				entity.FacilityTypeId = entity.FacilityTypeIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<HotelFacilities>
				if (CanDeepSave(entity.HotelFacilitiesCollection, "List<HotelFacilities>|HotelFacilitiesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelFacilities child in entity.HotelFacilitiesCollection)
					{
						if(child.FacilitiesIdSource != null)
						{
							child.FacilitiesId = child.FacilitiesIdSource.Id;
						}
						else
						{
							child.FacilitiesId = entity.Id;
						}

					}

					if (entity.HotelFacilitiesCollection.Count > 0 || entity.HotelFacilitiesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelFacilitiesProvider.Save(transactionManager, entity.HotelFacilitiesCollection);
						
						deepHandles.Add("HotelFacilitiesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelFacilities >) DataRepository.HotelFacilitiesProvider.DeepSave,
							new object[] { transactionManager, entity.HotelFacilitiesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FacilityChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Facility</c>
	///</summary>
	public enum FacilityChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>FacilityType</c> at FacilityTypeIdSource
		///</summary>
		[ChildEntityType(typeof(FacilityType))]
		FacilityType,
	
		///<summary>
		/// Collection of <c>Facility</c> as OneToMany for HotelFacilitiesCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelFacilities>))]
		HotelFacilitiesCollection,
	}
	
	#endregion FacilityChildEntityTypes
	
	#region FacilityFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FacilityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Facility"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacilityFilterBuilder : SqlFilterBuilder<FacilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacilityFilterBuilder class.
		/// </summary>
		public FacilityFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacilityFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacilityFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacilityFilterBuilder
	
	#region FacilityParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FacilityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Facility"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacilityParameterBuilder : ParameterizedSqlFilterBuilder<FacilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacilityParameterBuilder class.
		/// </summary>
		public FacilityParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacilityParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacilityParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacilityParameterBuilder
	
	#region FacilitySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FacilityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Facility"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FacilitySortBuilder : SqlSortBuilder<FacilityColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacilitySqlSortBuilder class.
		/// </summary>
		public FacilitySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FacilitySortBuilder
	
} // end namespace
