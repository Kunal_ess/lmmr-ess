﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="StatisticsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class StatisticsProviderBaseCore : EntityProviderBase<LMMR.Entities.Statistics, LMMR.Entities.StatisticsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.StatisticsKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Statistics_Hotel key.
		///		FK_Statistics_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Statistics objects.</returns>
		public TList<Statistics> GetByHotelId(System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Statistics_Hotel key.
		///		FK_Statistics_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Statistics objects.</returns>
		/// <remarks></remarks>
		public TList<Statistics> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Statistics_Hotel key.
		///		FK_Statistics_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Statistics objects.</returns>
		public TList<Statistics> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Statistics_Hotel key.
		///		fkStatisticsHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Statistics objects.</returns>
		public TList<Statistics> GetByHotelId(System.Int64 _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Statistics_Hotel key.
		///		fkStatisticsHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Statistics objects.</returns>
		public TList<Statistics> GetByHotelId(System.Int64 _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Statistics_Hotel key.
		///		FK_Statistics_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Statistics objects.</returns>
		public abstract TList<Statistics> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Statistics Get(TransactionManager transactionManager, LMMR.Entities.StatisticsKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Statistics index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Statistics"/> class.</returns>
		public LMMR.Entities.Statistics GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Statistics index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Statistics"/> class.</returns>
		public LMMR.Entities.Statistics GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Statistics index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Statistics"/> class.</returns>
		public LMMR.Entities.Statistics GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Statistics index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Statistics"/> class.</returns>
		public LMMR.Entities.Statistics GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Statistics index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Statistics"/> class.</returns>
		public LMMR.Entities.Statistics GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Statistics index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Statistics"/> class.</returns>
		public abstract LMMR.Entities.Statistics GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Statistics&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Statistics&gt;"/></returns>
		public static TList<Statistics> Fill(IDataReader reader, TList<Statistics> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Statistics c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Statistics")
					.Append("|").Append((System.Int64)reader[((int)StatisticsColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Statistics>(
					key.ToString(), // EntityTrackingKey
					"Statistics",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Statistics();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)StatisticsColumn.Id - 1)];
					c.HotelId = (System.Int64)reader[((int)StatisticsColumn.HotelId - 1)];
					c.StatDate = (reader.IsDBNull(((int)StatisticsColumn.StatDate - 1)))?null:(System.DateTime?)reader[((int)StatisticsColumn.StatDate - 1)];
					c.Views = (reader.IsDBNull(((int)StatisticsColumn.Views - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.Views - 1)];
					c.Visitor = (reader.IsDBNull(((int)StatisticsColumn.Visitor - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.Visitor - 1)];
					c.Booking = (reader.IsDBNull(((int)StatisticsColumn.Booking - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.Booking - 1)];
					c.Request = (reader.IsDBNull(((int)StatisticsColumn.Request - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.Request - 1)];
					c.FailureCancel = (reader.IsDBNull(((int)StatisticsColumn.FailureCancel - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.FailureCancel - 1)];
					c.FailureTimeout = (reader.IsDBNull(((int)StatisticsColumn.FailureTimeout - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.FailureTimeout - 1)];
					c.DeclineByOperator = (reader.IsDBNull(((int)StatisticsColumn.DeclineByOperator - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.DeclineByOperator - 1)];
					c.DeclineByHotel = (reader.IsDBNull(((int)StatisticsColumn.DeclineByHotel - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.DeclineByHotel - 1)];
					c.DeclineByHotelTentative = (reader.IsDBNull(((int)StatisticsColumn.DeclineByHotelTentative - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.DeclineByHotelTentative - 1)];
					c.NoActionTaken = (reader.IsDBNull(((int)StatisticsColumn.NoActionTaken - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.NoActionTaken - 1)];
					c.SensitiveBooking = (reader.IsDBNull(((int)StatisticsColumn.SensitiveBooking - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.SensitiveBooking - 1)];
					c.SensitiveRequest = (reader.IsDBNull(((int)StatisticsColumn.SensitiveRequest - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.SensitiveRequest - 1)];
					c.BookingId = (reader.IsDBNull(((int)StatisticsColumn.BookingId - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.BookingId - 1)];
					c.BookingType = (reader.IsDBNull(((int)StatisticsColumn.BookingType - 1)))?null:(System.Int32?)reader[((int)StatisticsColumn.BookingType - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Statistics"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Statistics"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Statistics entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)StatisticsColumn.Id - 1)];
			entity.HotelId = (System.Int64)reader[((int)StatisticsColumn.HotelId - 1)];
			entity.StatDate = (reader.IsDBNull(((int)StatisticsColumn.StatDate - 1)))?null:(System.DateTime?)reader[((int)StatisticsColumn.StatDate - 1)];
			entity.Views = (reader.IsDBNull(((int)StatisticsColumn.Views - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.Views - 1)];
			entity.Visitor = (reader.IsDBNull(((int)StatisticsColumn.Visitor - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.Visitor - 1)];
			entity.Booking = (reader.IsDBNull(((int)StatisticsColumn.Booking - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.Booking - 1)];
			entity.Request = (reader.IsDBNull(((int)StatisticsColumn.Request - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.Request - 1)];
			entity.FailureCancel = (reader.IsDBNull(((int)StatisticsColumn.FailureCancel - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.FailureCancel - 1)];
			entity.FailureTimeout = (reader.IsDBNull(((int)StatisticsColumn.FailureTimeout - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.FailureTimeout - 1)];
			entity.DeclineByOperator = (reader.IsDBNull(((int)StatisticsColumn.DeclineByOperator - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.DeclineByOperator - 1)];
			entity.DeclineByHotel = (reader.IsDBNull(((int)StatisticsColumn.DeclineByHotel - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.DeclineByHotel - 1)];
			entity.DeclineByHotelTentative = (reader.IsDBNull(((int)StatisticsColumn.DeclineByHotelTentative - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.DeclineByHotelTentative - 1)];
			entity.NoActionTaken = (reader.IsDBNull(((int)StatisticsColumn.NoActionTaken - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.NoActionTaken - 1)];
			entity.SensitiveBooking = (reader.IsDBNull(((int)StatisticsColumn.SensitiveBooking - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.SensitiveBooking - 1)];
			entity.SensitiveRequest = (reader.IsDBNull(((int)StatisticsColumn.SensitiveRequest - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.SensitiveRequest - 1)];
			entity.BookingId = (reader.IsDBNull(((int)StatisticsColumn.BookingId - 1)))?null:(System.Int64?)reader[((int)StatisticsColumn.BookingId - 1)];
			entity.BookingType = (reader.IsDBNull(((int)StatisticsColumn.BookingType - 1)))?null:(System.Int32?)reader[((int)StatisticsColumn.BookingType - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Statistics"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Statistics"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Statistics entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.StatDate = Convert.IsDBNull(dataRow["StatDate"]) ? null : (System.DateTime?)dataRow["StatDate"];
			entity.Views = Convert.IsDBNull(dataRow["Views"]) ? null : (System.Int64?)dataRow["Views"];
			entity.Visitor = Convert.IsDBNull(dataRow["Visitor"]) ? null : (System.Int64?)dataRow["Visitor"];
			entity.Booking = Convert.IsDBNull(dataRow["Booking"]) ? null : (System.Int64?)dataRow["Booking"];
			entity.Request = Convert.IsDBNull(dataRow["Request"]) ? null : (System.Int64?)dataRow["Request"];
			entity.FailureCancel = Convert.IsDBNull(dataRow["FailureCancel"]) ? null : (System.Int64?)dataRow["FailureCancel"];
			entity.FailureTimeout = Convert.IsDBNull(dataRow["FailureTimeout"]) ? null : (System.Int64?)dataRow["FailureTimeout"];
			entity.DeclineByOperator = Convert.IsDBNull(dataRow["DeclineByOperator"]) ? null : (System.Int64?)dataRow["DeclineByOperator"];
			entity.DeclineByHotel = Convert.IsDBNull(dataRow["DeclineByHotel"]) ? null : (System.Int64?)dataRow["DeclineByHotel"];
			entity.DeclineByHotelTentative = Convert.IsDBNull(dataRow["DeclineByHotelTentative"]) ? null : (System.Int64?)dataRow["DeclineByHotelTentative"];
			entity.NoActionTaken = Convert.IsDBNull(dataRow["NoActionTaken"]) ? null : (System.Int64?)dataRow["NoActionTaken"];
			entity.SensitiveBooking = Convert.IsDBNull(dataRow["SensitiveBooking"]) ? null : (System.Int64?)dataRow["SensitiveBooking"];
			entity.SensitiveRequest = Convert.IsDBNull(dataRow["SensitiveRequest"]) ? null : (System.Int64?)dataRow["SensitiveRequest"];
			entity.BookingId = Convert.IsDBNull(dataRow["BookingId"]) ? null : (System.Int64?)dataRow["BookingId"];
			entity.BookingType = Convert.IsDBNull(dataRow["BookingType"]) ? null : (System.Int32?)dataRow["BookingType"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Statistics"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Statistics Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Statistics entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelId;
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, entity.HotelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Statistics object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Statistics instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Statistics Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Statistics entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region StatisticsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Statistics</c>
	///</summary>
	public enum StatisticsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
		}
	
	#endregion StatisticsChildEntityTypes
	
	#region StatisticsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;StatisticsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Statistics"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StatisticsFilterBuilder : SqlFilterBuilder<StatisticsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StatisticsFilterBuilder class.
		/// </summary>
		public StatisticsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the StatisticsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StatisticsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StatisticsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StatisticsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StatisticsFilterBuilder
	
	#region StatisticsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;StatisticsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Statistics"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StatisticsParameterBuilder : ParameterizedSqlFilterBuilder<StatisticsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StatisticsParameterBuilder class.
		/// </summary>
		public StatisticsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the StatisticsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StatisticsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StatisticsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StatisticsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StatisticsParameterBuilder
	
	#region StatisticsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;StatisticsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Statistics"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class StatisticsSortBuilder : SqlSortBuilder<StatisticsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StatisticsSqlSortBuilder class.
		/// </summary>
		public StatisticsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion StatisticsSortBuilder
	
} // end namespace
