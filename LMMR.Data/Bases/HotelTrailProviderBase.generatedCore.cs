﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="HotelTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class HotelTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.HotelTrail, LMMR.Entities.HotelTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.HotelTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Trail_Hotel key.
		///		FK_Hotel_Trail_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelTrail objects.</returns>
		public TList<HotelTrail> GetByHotelId(System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Trail_Hotel key.
		///		FK_Hotel_Trail_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelTrail objects.</returns>
		/// <remarks></remarks>
		public TList<HotelTrail> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Trail_Hotel key.
		///		FK_Hotel_Trail_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelTrail objects.</returns>
		public TList<HotelTrail> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Trail_Hotel key.
		///		fkHotelTrailHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelTrail objects.</returns>
		public TList<HotelTrail> GetByHotelId(System.Int64 _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Trail_Hotel key.
		///		fkHotelTrailHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelTrail objects.</returns>
		public TList<HotelTrail> GetByHotelId(System.Int64 _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Trail_Hotel key.
		///		FK_Hotel_Trail_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelTrail objects.</returns>
		public abstract TList<HotelTrail> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.HotelTrail Get(TransactionManager transactionManager, LMMR.Entities.HotelTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Hotel_Trails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelTrail"/> class.</returns>
		public LMMR.Entities.HotelTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hotel_Trails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelTrail"/> class.</returns>
		public LMMR.Entities.HotelTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hotel_Trails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelTrail"/> class.</returns>
		public LMMR.Entities.HotelTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hotel_Trails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelTrail"/> class.</returns>
		public LMMR.Entities.HotelTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hotel_Trails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelTrail"/> class.</returns>
		public LMMR.Entities.HotelTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hotel_Trails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelTrail"/> class.</returns>
		public abstract LMMR.Entities.HotelTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;HotelTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;HotelTrail&gt;"/></returns>
		public static TList<HotelTrail> Fill(IDataReader reader, TList<HotelTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.HotelTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("HotelTrail")
					.Append("|").Append((System.Int64)reader[((int)HotelTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<HotelTrail>(
					key.ToString(), // EntityTrackingKey
					"HotelTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.HotelTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)HotelTrailColumn.Id - 1)];
					c.HotelId = (System.Int64)reader[((int)HotelTrailColumn.HotelId - 1)];
					c.Name = (reader.IsDBNull(((int)HotelTrailColumn.Name - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Name - 1)];
					c.CountryId = (System.Int64)reader[((int)HotelTrailColumn.CountryId - 1)];
					c.CityId = (System.Int64)reader[((int)HotelTrailColumn.CityId - 1)];
					c.ZoneId = (System.Int64)reader[((int)HotelTrailColumn.ZoneId - 1)];
					c.Stars = (reader.IsDBNull(((int)HotelTrailColumn.Stars - 1)))?null:(System.Int32?)reader[((int)HotelTrailColumn.Stars - 1)];
					c.Longitude = (reader.IsDBNull(((int)HotelTrailColumn.Longitude - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Longitude - 1)];
					c.Latitude = (reader.IsDBNull(((int)HotelTrailColumn.Latitude - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Latitude - 1)];
					c.Logo = (reader.IsDBNull(((int)HotelTrailColumn.Logo - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Logo - 1)];
					c.IsActive = (System.Boolean)reader[((int)HotelTrailColumn.IsActive - 1)];
					c.CancelationPolicyId = (System.Int64)reader[((int)HotelTrailColumn.CancelationPolicyId - 1)];
					c.GroupId = (System.Int64)reader[((int)HotelTrailColumn.GroupId - 1)];
					c.OperatorChoice = (reader.IsDBNull(((int)HotelTrailColumn.OperatorChoice - 1)))?null:(System.String)reader[((int)HotelTrailColumn.OperatorChoice - 1)];
					c.HotelPlan = (reader.IsDBNull(((int)HotelTrailColumn.HotelPlan - 1)))?null:(System.String)reader[((int)HotelTrailColumn.HotelPlan - 1)];
					c.ClientId = (System.Int64)reader[((int)HotelTrailColumn.ClientId - 1)];
					c.ContractId = (reader.IsDBNull(((int)HotelTrailColumn.ContractId - 1)))?null:(System.String)reader[((int)HotelTrailColumn.ContractId - 1)];
					c.StaffId = (System.Int64)reader[((int)HotelTrailColumn.StaffId - 1)];
					c.CurrencyId = (System.Int64)reader[((int)HotelTrailColumn.CurrencyId - 1)];
					c.ContractValue = (reader.IsDBNull(((int)HotelTrailColumn.ContractValue - 1)))?null:(System.Decimal?)reader[((int)HotelTrailColumn.ContractValue - 1)];
					c.TimeZone = (reader.IsDBNull(((int)HotelTrailColumn.TimeZone - 1)))?null:(System.String)reader[((int)HotelTrailColumn.TimeZone - 1)];
					c.Phone = (reader.IsDBNull(((int)HotelTrailColumn.Phone - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Phone - 1)];
					c.RtMFrom = (reader.IsDBNull(((int)HotelTrailColumn.RtMFrom - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtMFrom - 1)];
					c.RtMTo = (reader.IsDBNull(((int)HotelTrailColumn.RtMTo - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtMTo - 1)];
					c.RtAFrom = (reader.IsDBNull(((int)HotelTrailColumn.RtAFrom - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtAFrom - 1)];
					c.RtATo = (reader.IsDBNull(((int)HotelTrailColumn.RtATo - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtATo - 1)];
					c.RtFFrom = (reader.IsDBNull(((int)HotelTrailColumn.RtFFrom - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtFFrom - 1)];
					c.RtFTo = (reader.IsDBNull(((int)HotelTrailColumn.RtFTo - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtFTo - 1)];
					c.IsCreditCardExcepted = (System.Boolean)reader[((int)HotelTrailColumn.IsCreditCardExcepted - 1)];
					c.CreditCardType = (reader.IsDBNull(((int)HotelTrailColumn.CreditCardType - 1)))?null:(System.String)reader[((int)HotelTrailColumn.CreditCardType - 1)];
					c.Theme = (reader.IsDBNull(((int)HotelTrailColumn.Theme - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Theme - 1)];
					c.IsBedroomAvailable = (System.Boolean)reader[((int)HotelTrailColumn.IsBedroomAvailable - 1)];
					c.NumberOfBedroom = (reader.IsDBNull(((int)HotelTrailColumn.NumberOfBedroom - 1)))?null:(System.Int32?)reader[((int)HotelTrailColumn.NumberOfBedroom - 1)];
					c.Email = (reader.IsDBNull(((int)HotelTrailColumn.Email - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Email - 1)];
					c.Password = (reader.IsDBNull(((int)HotelTrailColumn.Password - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Password - 1)];
					c.ContactPerson = (reader.IsDBNull(((int)HotelTrailColumn.ContactPerson - 1)))?null:(System.String)reader[((int)HotelTrailColumn.ContactPerson - 1)];
					c.PhoneNumber = (reader.IsDBNull(((int)HotelTrailColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)HotelTrailColumn.PhoneNumber - 1)];
					c.CreationDate = (reader.IsDBNull(((int)HotelTrailColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)HotelTrailColumn.CreationDate - 1)];
					c.CountryCodePhone = (reader.IsDBNull(((int)HotelTrailColumn.CountryCodePhone - 1)))?null:(System.String)reader[((int)HotelTrailColumn.CountryCodePhone - 1)];
					c.CountryCodeFax = (reader.IsDBNull(((int)HotelTrailColumn.CountryCodeFax - 1)))?null:(System.String)reader[((int)HotelTrailColumn.CountryCodeFax - 1)];
					c.FaxNumber = (reader.IsDBNull(((int)HotelTrailColumn.FaxNumber - 1)))?null:(System.String)reader[((int)HotelTrailColumn.FaxNumber - 1)];
					c.GoOnline = (reader.IsDBNull(((int)HotelTrailColumn.GoOnline - 1)))?null:(System.Boolean?)reader[((int)HotelTrailColumn.GoOnline - 1)];
					c.Updatedate = (reader.IsDBNull(((int)HotelTrailColumn.Updatedate - 1)))?null:(System.DateTime?)reader[((int)HotelTrailColumn.Updatedate - 1)];
					c.UpdateBy = (reader.IsDBNull(((int)HotelTrailColumn.UpdateBy - 1)))?null:(System.Int32?)reader[((int)HotelTrailColumn.UpdateBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HotelTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.HotelTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)HotelTrailColumn.Id - 1)];
			entity.HotelId = (System.Int64)reader[((int)HotelTrailColumn.HotelId - 1)];
			entity.Name = (reader.IsDBNull(((int)HotelTrailColumn.Name - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Name - 1)];
			entity.CountryId = (System.Int64)reader[((int)HotelTrailColumn.CountryId - 1)];
			entity.CityId = (System.Int64)reader[((int)HotelTrailColumn.CityId - 1)];
			entity.ZoneId = (System.Int64)reader[((int)HotelTrailColumn.ZoneId - 1)];
			entity.Stars = (reader.IsDBNull(((int)HotelTrailColumn.Stars - 1)))?null:(System.Int32?)reader[((int)HotelTrailColumn.Stars - 1)];
			entity.Longitude = (reader.IsDBNull(((int)HotelTrailColumn.Longitude - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Longitude - 1)];
			entity.Latitude = (reader.IsDBNull(((int)HotelTrailColumn.Latitude - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Latitude - 1)];
			entity.Logo = (reader.IsDBNull(((int)HotelTrailColumn.Logo - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Logo - 1)];
			entity.IsActive = (System.Boolean)reader[((int)HotelTrailColumn.IsActive - 1)];
			entity.CancelationPolicyId = (System.Int64)reader[((int)HotelTrailColumn.CancelationPolicyId - 1)];
			entity.GroupId = (System.Int64)reader[((int)HotelTrailColumn.GroupId - 1)];
			entity.OperatorChoice = (reader.IsDBNull(((int)HotelTrailColumn.OperatorChoice - 1)))?null:(System.String)reader[((int)HotelTrailColumn.OperatorChoice - 1)];
			entity.HotelPlan = (reader.IsDBNull(((int)HotelTrailColumn.HotelPlan - 1)))?null:(System.String)reader[((int)HotelTrailColumn.HotelPlan - 1)];
			entity.ClientId = (System.Int64)reader[((int)HotelTrailColumn.ClientId - 1)];
			entity.ContractId = (reader.IsDBNull(((int)HotelTrailColumn.ContractId - 1)))?null:(System.String)reader[((int)HotelTrailColumn.ContractId - 1)];
			entity.StaffId = (System.Int64)reader[((int)HotelTrailColumn.StaffId - 1)];
			entity.CurrencyId = (System.Int64)reader[((int)HotelTrailColumn.CurrencyId - 1)];
			entity.ContractValue = (reader.IsDBNull(((int)HotelTrailColumn.ContractValue - 1)))?null:(System.Decimal?)reader[((int)HotelTrailColumn.ContractValue - 1)];
			entity.TimeZone = (reader.IsDBNull(((int)HotelTrailColumn.TimeZone - 1)))?null:(System.String)reader[((int)HotelTrailColumn.TimeZone - 1)];
			entity.Phone = (reader.IsDBNull(((int)HotelTrailColumn.Phone - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Phone - 1)];
			entity.RtMFrom = (reader.IsDBNull(((int)HotelTrailColumn.RtMFrom - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtMFrom - 1)];
			entity.RtMTo = (reader.IsDBNull(((int)HotelTrailColumn.RtMTo - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtMTo - 1)];
			entity.RtAFrom = (reader.IsDBNull(((int)HotelTrailColumn.RtAFrom - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtAFrom - 1)];
			entity.RtATo = (reader.IsDBNull(((int)HotelTrailColumn.RtATo - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtATo - 1)];
			entity.RtFFrom = (reader.IsDBNull(((int)HotelTrailColumn.RtFFrom - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtFFrom - 1)];
			entity.RtFTo = (reader.IsDBNull(((int)HotelTrailColumn.RtFTo - 1)))?null:(System.String)reader[((int)HotelTrailColumn.RtFTo - 1)];
			entity.IsCreditCardExcepted = (System.Boolean)reader[((int)HotelTrailColumn.IsCreditCardExcepted - 1)];
			entity.CreditCardType = (reader.IsDBNull(((int)HotelTrailColumn.CreditCardType - 1)))?null:(System.String)reader[((int)HotelTrailColumn.CreditCardType - 1)];
			entity.Theme = (reader.IsDBNull(((int)HotelTrailColumn.Theme - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Theme - 1)];
			entity.IsBedroomAvailable = (System.Boolean)reader[((int)HotelTrailColumn.IsBedroomAvailable - 1)];
			entity.NumberOfBedroom = (reader.IsDBNull(((int)HotelTrailColumn.NumberOfBedroom - 1)))?null:(System.Int32?)reader[((int)HotelTrailColumn.NumberOfBedroom - 1)];
			entity.Email = (reader.IsDBNull(((int)HotelTrailColumn.Email - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Email - 1)];
			entity.Password = (reader.IsDBNull(((int)HotelTrailColumn.Password - 1)))?null:(System.String)reader[((int)HotelTrailColumn.Password - 1)];
			entity.ContactPerson = (reader.IsDBNull(((int)HotelTrailColumn.ContactPerson - 1)))?null:(System.String)reader[((int)HotelTrailColumn.ContactPerson - 1)];
			entity.PhoneNumber = (reader.IsDBNull(((int)HotelTrailColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)HotelTrailColumn.PhoneNumber - 1)];
			entity.CreationDate = (reader.IsDBNull(((int)HotelTrailColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)HotelTrailColumn.CreationDate - 1)];
			entity.CountryCodePhone = (reader.IsDBNull(((int)HotelTrailColumn.CountryCodePhone - 1)))?null:(System.String)reader[((int)HotelTrailColumn.CountryCodePhone - 1)];
			entity.CountryCodeFax = (reader.IsDBNull(((int)HotelTrailColumn.CountryCodeFax - 1)))?null:(System.String)reader[((int)HotelTrailColumn.CountryCodeFax - 1)];
			entity.FaxNumber = (reader.IsDBNull(((int)HotelTrailColumn.FaxNumber - 1)))?null:(System.String)reader[((int)HotelTrailColumn.FaxNumber - 1)];
			entity.GoOnline = (reader.IsDBNull(((int)HotelTrailColumn.GoOnline - 1)))?null:(System.Boolean?)reader[((int)HotelTrailColumn.GoOnline - 1)];
			entity.Updatedate = (reader.IsDBNull(((int)HotelTrailColumn.Updatedate - 1)))?null:(System.DateTime?)reader[((int)HotelTrailColumn.Updatedate - 1)];
			entity.UpdateBy = (reader.IsDBNull(((int)HotelTrailColumn.UpdateBy - 1)))?null:(System.Int32?)reader[((int)HotelTrailColumn.UpdateBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HotelTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.HotelTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.CountryId = (System.Int64)dataRow["CountryId"];
			entity.CityId = (System.Int64)dataRow["CityId"];
			entity.ZoneId = (System.Int64)dataRow["ZoneId"];
			entity.Stars = Convert.IsDBNull(dataRow["Stars"]) ? null : (System.Int32?)dataRow["Stars"];
			entity.Longitude = Convert.IsDBNull(dataRow["Longitude"]) ? null : (System.String)dataRow["Longitude"];
			entity.Latitude = Convert.IsDBNull(dataRow["Latitude"]) ? null : (System.String)dataRow["Latitude"];
			entity.Logo = Convert.IsDBNull(dataRow["Logo"]) ? null : (System.String)dataRow["Logo"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.CancelationPolicyId = (System.Int64)dataRow["CancelationPolicyId"];
			entity.GroupId = (System.Int64)dataRow["GroupId"];
			entity.OperatorChoice = Convert.IsDBNull(dataRow["OperatorChoice"]) ? null : (System.String)dataRow["OperatorChoice"];
			entity.HotelPlan = Convert.IsDBNull(dataRow["HotelPlan"]) ? null : (System.String)dataRow["HotelPlan"];
			entity.ClientId = (System.Int64)dataRow["ClientId"];
			entity.ContractId = Convert.IsDBNull(dataRow["ContractId"]) ? null : (System.String)dataRow["ContractId"];
			entity.StaffId = (System.Int64)dataRow["StaffId"];
			entity.CurrencyId = (System.Int64)dataRow["CurrencyId"];
			entity.ContractValue = Convert.IsDBNull(dataRow["ContractValue"]) ? null : (System.Decimal?)dataRow["ContractValue"];
			entity.TimeZone = Convert.IsDBNull(dataRow["TimeZone"]) ? null : (System.String)dataRow["TimeZone"];
			entity.Phone = Convert.IsDBNull(dataRow["Phone"]) ? null : (System.String)dataRow["Phone"];
			entity.RtMFrom = Convert.IsDBNull(dataRow["RT_M_From"]) ? null : (System.String)dataRow["RT_M_From"];
			entity.RtMTo = Convert.IsDBNull(dataRow["RT_M_To"]) ? null : (System.String)dataRow["RT_M_To"];
			entity.RtAFrom = Convert.IsDBNull(dataRow["RT_A_From"]) ? null : (System.String)dataRow["RT_A_From"];
			entity.RtATo = Convert.IsDBNull(dataRow["RT_A_To"]) ? null : (System.String)dataRow["RT_A_To"];
			entity.RtFFrom = Convert.IsDBNull(dataRow["RT_F_From"]) ? null : (System.String)dataRow["RT_F_From"];
			entity.RtFTo = Convert.IsDBNull(dataRow["RT_F_To"]) ? null : (System.String)dataRow["RT_F_To"];
			entity.IsCreditCardExcepted = (System.Boolean)dataRow["IsCreditCardExcepted"];
			entity.CreditCardType = Convert.IsDBNull(dataRow["CreditCardType"]) ? null : (System.String)dataRow["CreditCardType"];
			entity.Theme = Convert.IsDBNull(dataRow["Theme"]) ? null : (System.String)dataRow["Theme"];
			entity.IsBedroomAvailable = (System.Boolean)dataRow["IsBedroomAvailable"];
			entity.NumberOfBedroom = Convert.IsDBNull(dataRow["NumberOfBedroom"]) ? null : (System.Int32?)dataRow["NumberOfBedroom"];
			entity.Email = Convert.IsDBNull(dataRow["Email"]) ? null : (System.String)dataRow["Email"];
			entity.Password = Convert.IsDBNull(dataRow["Password"]) ? null : (System.String)dataRow["Password"];
			entity.ContactPerson = Convert.IsDBNull(dataRow["ContactPerson"]) ? null : (System.String)dataRow["ContactPerson"];
			entity.PhoneNumber = Convert.IsDBNull(dataRow["PhoneNumber"]) ? null : (System.String)dataRow["PhoneNumber"];
			entity.CreationDate = Convert.IsDBNull(dataRow["CreationDate"]) ? null : (System.DateTime?)dataRow["CreationDate"];
			entity.CountryCodePhone = Convert.IsDBNull(dataRow["CountryCodePhone"]) ? null : (System.String)dataRow["CountryCodePhone"];
			entity.CountryCodeFax = Convert.IsDBNull(dataRow["CountryCodeFax"]) ? null : (System.String)dataRow["CountryCodeFax"];
			entity.FaxNumber = Convert.IsDBNull(dataRow["FaxNumber"]) ? null : (System.String)dataRow["FaxNumber"];
			entity.GoOnline = Convert.IsDBNull(dataRow["GoOnline"]) ? null : (System.Boolean?)dataRow["GoOnline"];
			entity.Updatedate = Convert.IsDBNull(dataRow["Updatedate"]) ? null : (System.DateTime?)dataRow["Updatedate"];
			entity.UpdateBy = Convert.IsDBNull(dataRow["UpdateBy"]) ? null : (System.Int32?)dataRow["UpdateBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.HotelTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.HotelTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelId;
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, entity.HotelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.HotelTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.HotelTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.HotelTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.HotelTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region HotelTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.HotelTrail</c>
	///</summary>
	public enum HotelTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
		}
	
	#endregion HotelTrailChildEntityTypes
	
	#region HotelTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;HotelTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelTrailFilterBuilder : SqlFilterBuilder<HotelTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelTrailFilterBuilder class.
		/// </summary>
		public HotelTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelTrailFilterBuilder
	
	#region HotelTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;HotelTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelTrailParameterBuilder : ParameterizedSqlFilterBuilder<HotelTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelTrailParameterBuilder class.
		/// </summary>
		public HotelTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelTrailParameterBuilder
	
	#region HotelTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;HotelTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class HotelTrailSortBuilder : SqlSortBuilder<HotelTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelTrailSqlSortBuilder class.
		/// </summary>
		public HotelTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion HotelTrailSortBuilder
	
} // end namespace
