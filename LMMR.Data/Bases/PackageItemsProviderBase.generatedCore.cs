﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PackageItemsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PackageItemsProviderBaseCore : EntityProviderBase<LMMR.Entities.PackageItems, LMMR.Entities.PackageItemsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.PackageItemsKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageItems_Country key.
		///		FK_PackageItems_Country Description: 
		/// </summary>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageItems objects.</returns>
		public TList<PackageItems> GetByCountryId(System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(_countryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageItems_Country key.
		///		FK_PackageItems_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageItems objects.</returns>
		/// <remarks></remarks>
		public TList<PackageItems> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageItems_Country key.
		///		FK_PackageItems_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageItems objects.</returns>
		public TList<PackageItems> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageItems_Country key.
		///		fkPackageItemsCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageItems objects.</returns>
		public TList<PackageItems> GetByCountryId(System.Int64 _countryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryId(null, _countryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageItems_Country key.
		///		fkPackageItemsCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageItems objects.</returns>
		public TList<PackageItems> GetByCountryId(System.Int64 _countryId, int start, int pageLength,out int count)
		{
			return GetByCountryId(null, _countryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageItems_Country key.
		///		FK_PackageItems_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageItems objects.</returns>
		public abstract TList<PackageItems> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.PackageItems Get(TransactionManager transactionManager, LMMR.Entities.PackageItemsKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Section index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageItems"/> class.</returns>
		public LMMR.Entities.PackageItems GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Section index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageItems"/> class.</returns>
		public LMMR.Entities.PackageItems GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Section index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageItems"/> class.</returns>
		public LMMR.Entities.PackageItems GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Section index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageItems"/> class.</returns>
		public LMMR.Entities.PackageItems GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Section index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageItems"/> class.</returns>
		public LMMR.Entities.PackageItems GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Section index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageItems"/> class.</returns>
		public abstract LMMR.Entities.PackageItems GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PackageItems&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PackageItems&gt;"/></returns>
		public static TList<PackageItems> Fill(IDataReader reader, TList<PackageItems> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.PackageItems c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PackageItems")
					.Append("|").Append((System.Int64)reader[((int)PackageItemsColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PackageItems>(
					key.ToString(), // EntityTrackingKey
					"PackageItems",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.PackageItems();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)PackageItemsColumn.Id - 1)];
					c.ItemType = (System.String)reader[((int)PackageItemsColumn.ItemType - 1)];
					c.ItemName = (reader.IsDBNull(((int)PackageItemsColumn.ItemName - 1)))?null:(System.String)reader[((int)PackageItemsColumn.ItemName - 1)];
					c.Vat = (reader.IsDBNull(((int)PackageItemsColumn.Vat - 1)))?null:(System.Decimal?)reader[((int)PackageItemsColumn.Vat - 1)];
					c.IsActive = (System.Boolean)reader[((int)PackageItemsColumn.IsActive - 1)];
					c.CreatedDate = (reader.IsDBNull(((int)PackageItemsColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)PackageItemsColumn.CreatedDate - 1)];
					c.CountryId = (System.Int64)reader[((int)PackageItemsColumn.CountryId - 1)];
					c.IsExtra = (System.Boolean)reader[((int)PackageItemsColumn.IsExtra - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageItems"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageItems"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.PackageItems entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)PackageItemsColumn.Id - 1)];
			entity.ItemType = (System.String)reader[((int)PackageItemsColumn.ItemType - 1)];
			entity.ItemName = (reader.IsDBNull(((int)PackageItemsColumn.ItemName - 1)))?null:(System.String)reader[((int)PackageItemsColumn.ItemName - 1)];
			entity.Vat = (reader.IsDBNull(((int)PackageItemsColumn.Vat - 1)))?null:(System.Decimal?)reader[((int)PackageItemsColumn.Vat - 1)];
			entity.IsActive = (System.Boolean)reader[((int)PackageItemsColumn.IsActive - 1)];
			entity.CreatedDate = (reader.IsDBNull(((int)PackageItemsColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)PackageItemsColumn.CreatedDate - 1)];
			entity.CountryId = (System.Int64)reader[((int)PackageItemsColumn.CountryId - 1)];
			entity.IsExtra = (System.Boolean)reader[((int)PackageItemsColumn.IsExtra - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageItems"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageItems"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.PackageItems entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.ItemType = (System.String)dataRow["ItemType"];
			entity.ItemName = Convert.IsDBNull(dataRow["ItemName"]) ? null : (System.String)dataRow["ItemName"];
			entity.Vat = Convert.IsDBNull(dataRow["VAT"]) ? null : (System.Decimal?)dataRow["VAT"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.CreatedDate = Convert.IsDBNull(dataRow["CreatedDate"]) ? null : (System.DateTime?)dataRow["CreatedDate"];
			entity.CountryId = (System.Int64)dataRow["CountryId"];
			entity.IsExtra = (System.Boolean)dataRow["IsExtra"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageItems"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.PackageItems Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.PackageItems entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CountryIdSource	
			if (CanDeepLoad(entity, "Country|CountryIdSource", deepLoadType, innerList) 
				&& entity.CountryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CountryId;
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryIdSource = tmpEntity;
				else
					entity.CountryIdSource = DataRepository.CountryProvider.GetById(transactionManager, entity.CountryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region PackageByHotelCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageByHotel>|PackageByHotelCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageByHotelCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageByHotelCollection = DataRepository.PackageByHotelProvider.GetByItemId(transactionManager, entity.Id);

				if (deep && entity.PackageByHotelCollection.Count > 0)
				{
					deepHandles.Add("PackageByHotelCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageByHotel>) DataRepository.PackageByHotelProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageByHotelCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageItemMappingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageItemMapping>|PackageItemMappingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageItemMappingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageItemMappingCollection = DataRepository.PackageItemMappingProvider.GetByItemId(transactionManager, entity.Id);

				if (deep && entity.PackageItemMappingCollection.Count > 0)
				{
					deepHandles.Add("PackageItemMappingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageItemMapping>) DataRepository.PackageItemMappingProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageItemMappingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageDescription>|PackageDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageDescriptionCollection = DataRepository.PackageDescriptionProvider.GetByItemId(transactionManager, entity.Id);

				if (deep && entity.PackageDescriptionCollection.Count > 0)
				{
					deepHandles.Add("PackageDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageDescription>) DataRepository.PackageDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BuildMeetingConfigureCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BuildMeetingConfigure>|BuildMeetingConfigureCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildMeetingConfigureCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BuildMeetingConfigureCollection = DataRepository.BuildMeetingConfigureProvider.GetByPackageId(transactionManager, entity.Id);

				if (deep && entity.BuildMeetingConfigureCollection.Count > 0)
				{
					deepHandles.Add("BuildMeetingConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BuildMeetingConfigure>) DataRepository.BuildMeetingConfigureProvider.DeepLoad,
						new object[] { transactionManager, entity.BuildMeetingConfigureCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BuildPackageConfigureDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BuildPackageConfigureDesc>|BuildPackageConfigureDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildPackageConfigureDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BuildPackageConfigureDescCollection = DataRepository.BuildPackageConfigureDescProvider.GetBySectionDescId(transactionManager, entity.Id);

				if (deep && entity.BuildPackageConfigureDescCollection.Count > 0)
				{
					deepHandles.Add("BuildPackageConfigureDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BuildPackageConfigureDesc>) DataRepository.BuildPackageConfigureDescProvider.DeepLoad,
						new object[] { transactionManager, entity.BuildPackageConfigureDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.PackageItems object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.PackageItems instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.PackageItems Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.PackageItems entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CountryIdSource
			if (CanDeepSave(entity, "Country|CountryIdSource", deepSaveType, innerList) 
				&& entity.CountryIdSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryIdSource);
				entity.CountryId = entity.CountryIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<PackageByHotel>
				if (CanDeepSave(entity.PackageByHotelCollection, "List<PackageByHotel>|PackageByHotelCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageByHotel child in entity.PackageByHotelCollection)
					{
						if(child.ItemIdSource != null)
						{
							child.ItemId = child.ItemIdSource.Id;
						}
						else
						{
							child.ItemId = entity.Id;
						}

					}

					if (entity.PackageByHotelCollection.Count > 0 || entity.PackageByHotelCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageByHotelProvider.Save(transactionManager, entity.PackageByHotelCollection);
						
						deepHandles.Add("PackageByHotelCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageByHotel >) DataRepository.PackageByHotelProvider.DeepSave,
							new object[] { transactionManager, entity.PackageByHotelCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageItemMapping>
				if (CanDeepSave(entity.PackageItemMappingCollection, "List<PackageItemMapping>|PackageItemMappingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageItemMapping child in entity.PackageItemMappingCollection)
					{
						if(child.ItemIdSource != null)
						{
							child.ItemId = child.ItemIdSource.Id;
						}
						else
						{
							child.ItemId = entity.Id;
						}

					}

					if (entity.PackageItemMappingCollection.Count > 0 || entity.PackageItemMappingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageItemMappingProvider.Save(transactionManager, entity.PackageItemMappingCollection);
						
						deepHandles.Add("PackageItemMappingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageItemMapping >) DataRepository.PackageItemMappingProvider.DeepSave,
							new object[] { transactionManager, entity.PackageItemMappingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageDescription>
				if (CanDeepSave(entity.PackageDescriptionCollection, "List<PackageDescription>|PackageDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageDescription child in entity.PackageDescriptionCollection)
					{
						if(child.ItemIdSource != null)
						{
							child.ItemId = child.ItemIdSource.Id;
						}
						else
						{
							child.ItemId = entity.Id;
						}

					}

					if (entity.PackageDescriptionCollection.Count > 0 || entity.PackageDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageDescriptionProvider.Save(transactionManager, entity.PackageDescriptionCollection);
						
						deepHandles.Add("PackageDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageDescription >) DataRepository.PackageDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.PackageDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BuildMeetingConfigure>
				if (CanDeepSave(entity.BuildMeetingConfigureCollection, "List<BuildMeetingConfigure>|BuildMeetingConfigureCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BuildMeetingConfigure child in entity.BuildMeetingConfigureCollection)
					{
						if(child.PackageIdSource != null)
						{
							child.PackageId = child.PackageIdSource.Id;
						}
						else
						{
							child.PackageId = entity.Id;
						}

					}

					if (entity.BuildMeetingConfigureCollection.Count > 0 || entity.BuildMeetingConfigureCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BuildMeetingConfigureProvider.Save(transactionManager, entity.BuildMeetingConfigureCollection);
						
						deepHandles.Add("BuildMeetingConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BuildMeetingConfigure >) DataRepository.BuildMeetingConfigureProvider.DeepSave,
							new object[] { transactionManager, entity.BuildMeetingConfigureCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BuildPackageConfigureDesc>
				if (CanDeepSave(entity.BuildPackageConfigureDescCollection, "List<BuildPackageConfigureDesc>|BuildPackageConfigureDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BuildPackageConfigureDesc child in entity.BuildPackageConfigureDescCollection)
					{
						if(child.SectionDescIdSource != null)
						{
							child.SectionDescId = child.SectionDescIdSource.Id;
						}
						else
						{
							child.SectionDescId = entity.Id;
						}

					}

					if (entity.BuildPackageConfigureDescCollection.Count > 0 || entity.BuildPackageConfigureDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BuildPackageConfigureDescProvider.Save(transactionManager, entity.BuildPackageConfigureDescCollection);
						
						deepHandles.Add("BuildPackageConfigureDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BuildPackageConfigureDesc >) DataRepository.BuildPackageConfigureDescProvider.DeepSave,
							new object[] { transactionManager, entity.BuildPackageConfigureDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PackageItemsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.PackageItems</c>
	///</summary>
	public enum PackageItemsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Country</c> at CountryIdSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
	
		///<summary>
		/// Collection of <c>PackageItems</c> as OneToMany for PackageByHotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageByHotel>))]
		PackageByHotelCollection,

		///<summary>
		/// Collection of <c>PackageItems</c> as OneToMany for PackageItemMappingCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageItemMapping>))]
		PackageItemMappingCollection,

		///<summary>
		/// Collection of <c>PackageItems</c> as OneToMany for PackageDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageDescription>))]
		PackageDescriptionCollection,

		///<summary>
		/// Collection of <c>PackageItems</c> as OneToMany for BuildMeetingConfigureCollection
		///</summary>
		[ChildEntityType(typeof(TList<BuildMeetingConfigure>))]
		BuildMeetingConfigureCollection,

		///<summary>
		/// Collection of <c>PackageItems</c> as OneToMany for BuildPackageConfigureDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<BuildPackageConfigureDesc>))]
		BuildPackageConfigureDescCollection,
	}
	
	#endregion PackageItemsChildEntityTypes
	
	#region PackageItemsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PackageItemsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageItemsFilterBuilder : SqlFilterBuilder<PackageItemsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageItemsFilterBuilder class.
		/// </summary>
		public PackageItemsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageItemsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageItemsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageItemsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageItemsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageItemsFilterBuilder
	
	#region PackageItemsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PackageItemsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageItemsParameterBuilder : ParameterizedSqlFilterBuilder<PackageItemsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageItemsParameterBuilder class.
		/// </summary>
		public PackageItemsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageItemsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageItemsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageItemsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageItemsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageItemsParameterBuilder
	
	#region PackageItemsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PackageItemsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageItems"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PackageItemsSortBuilder : SqlSortBuilder<PackageItemsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageItemsSqlSortBuilder class.
		/// </summary>
		public PackageItemsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PackageItemsSortBuilder
	
} // end namespace
