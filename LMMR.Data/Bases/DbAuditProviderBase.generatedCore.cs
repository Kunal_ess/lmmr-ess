﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="DbAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class DbAuditProviderBaseCore : EntityProviderBase<LMMR.Entities.DbAudit, LMMR.Entities.DbAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.DbAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.DbAudit Get(TransactionManager transactionManager, LMMR.Entities.DbAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_DBAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.DbAudit"/> class.</returns>
		public LMMR.Entities.DbAudit GetByAuditId(System.Int64 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_DBAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.DbAudit"/> class.</returns>
		public LMMR.Entities.DbAudit GetByAuditId(System.Int64 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_DBAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.DbAudit"/> class.</returns>
		public LMMR.Entities.DbAudit GetByAuditId(TransactionManager transactionManager, System.Int64 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_DBAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.DbAudit"/> class.</returns>
		public LMMR.Entities.DbAudit GetByAuditId(TransactionManager transactionManager, System.Int64 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_DBAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.DbAudit"/> class.</returns>
		public LMMR.Entities.DbAudit GetByAuditId(System.Int64 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_DBAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.DbAudit"/> class.</returns>
		public abstract LMMR.Entities.DbAudit GetByAuditId(TransactionManager transactionManager, System.Int64 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;DbAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;DbAudit&gt;"/></returns>
		public static TList<DbAudit> Fill(IDataReader reader, TList<DbAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.DbAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("DbAudit")
					.Append("|").Append((System.Int64)reader[((int)DbAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<DbAudit>(
					key.ToString(), // EntityTrackingKey
					"DbAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.DbAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int64)reader[((int)DbAuditColumn.AuditId - 1)];
					c.RevisionStamp = (reader.IsDBNull(((int)DbAuditColumn.RevisionStamp - 1)))?null:(System.DateTime?)reader[((int)DbAuditColumn.RevisionStamp - 1)];
					c.EntityTblName = (reader.IsDBNull(((int)DbAuditColumn.EntityTblName - 1)))?null:(System.String)reader[((int)DbAuditColumn.EntityTblName - 1)];
					c.UserName = (reader.IsDBNull(((int)DbAuditColumn.UserName - 1)))?null:(System.Int64?)reader[((int)DbAuditColumn.UserName - 1)];
					c.Actions = (reader.IsDBNull(((int)DbAuditColumn.Actions - 1)))?null:(System.String)reader[((int)DbAuditColumn.Actions - 1)];
					c.OldData = (reader.IsDBNull(((int)DbAuditColumn.OldData - 1)))?null:(string)reader[((int)DbAuditColumn.OldData - 1)];
					c.NewData = (reader.IsDBNull(((int)DbAuditColumn.NewData - 1)))?null:(string)reader[((int)DbAuditColumn.NewData - 1)];
					c.PageName = (reader.IsDBNull(((int)DbAuditColumn.PageName - 1)))?null:(System.Int32?)reader[((int)DbAuditColumn.PageName - 1)];
					c.ReferenceId = (reader.IsDBNull(((int)DbAuditColumn.ReferenceId - 1)))?null:(System.String)reader[((int)DbAuditColumn.ReferenceId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.DbAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.DbAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.DbAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int64)reader[((int)DbAuditColumn.AuditId - 1)];
			entity.RevisionStamp = (reader.IsDBNull(((int)DbAuditColumn.RevisionStamp - 1)))?null:(System.DateTime?)reader[((int)DbAuditColumn.RevisionStamp - 1)];
			entity.EntityTblName = (reader.IsDBNull(((int)DbAuditColumn.EntityTblName - 1)))?null:(System.String)reader[((int)DbAuditColumn.EntityTblName - 1)];
			entity.UserName = (reader.IsDBNull(((int)DbAuditColumn.UserName - 1)))?null:(System.Int64?)reader[((int)DbAuditColumn.UserName - 1)];
			entity.Actions = (reader.IsDBNull(((int)DbAuditColumn.Actions - 1)))?null:(System.String)reader[((int)DbAuditColumn.Actions - 1)];
			entity.OldData = (reader.IsDBNull(((int)DbAuditColumn.OldData - 1)))?null:(string)reader[((int)DbAuditColumn.OldData - 1)];
			entity.NewData = (reader.IsDBNull(((int)DbAuditColumn.NewData - 1)))?null:(string)reader[((int)DbAuditColumn.NewData - 1)];
			entity.PageName = (reader.IsDBNull(((int)DbAuditColumn.PageName - 1)))?null:(System.Int32?)reader[((int)DbAuditColumn.PageName - 1)];
			entity.ReferenceId = (reader.IsDBNull(((int)DbAuditColumn.ReferenceId - 1)))?null:(System.String)reader[((int)DbAuditColumn.ReferenceId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.DbAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.DbAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.DbAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int64)dataRow["AuditID"];
			entity.RevisionStamp = Convert.IsDBNull(dataRow["RevisionStamp"]) ? null : (System.DateTime?)dataRow["RevisionStamp"];
			entity.EntityTblName = Convert.IsDBNull(dataRow["EntityTblName"]) ? null : (System.String)dataRow["EntityTblName"];
			entity.UserName = Convert.IsDBNull(dataRow["UserName"]) ? null : (System.Int64?)dataRow["UserName"];
			entity.Actions = Convert.IsDBNull(dataRow["Actions"]) ? null : (System.String)dataRow["Actions"];
			entity.OldData = Convert.IsDBNull(dataRow["OldData"]) ? null : (string)dataRow["OldData"];
			entity.NewData = Convert.IsDBNull(dataRow["NewData"]) ? null : (string)dataRow["NewData"];
			entity.PageName = Convert.IsDBNull(dataRow["PageName"]) ? null : (System.Int32?)dataRow["PageName"];
			entity.ReferenceId = Convert.IsDBNull(dataRow["ReferenceId"]) ? null : (System.String)dataRow["ReferenceId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.DbAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.DbAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.DbAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.DbAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.DbAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.DbAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.DbAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region DbAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.DbAudit</c>
	///</summary>
	public enum DbAuditChildEntityTypes
	{
	}
	
	#endregion DbAuditChildEntityTypes
	
	#region DbAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;DbAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DbAuditFilterBuilder : SqlFilterBuilder<DbAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DbAuditFilterBuilder class.
		/// </summary>
		public DbAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the DbAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DbAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DbAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DbAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DbAuditFilterBuilder
	
	#region DbAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;DbAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DbAuditParameterBuilder : ParameterizedSqlFilterBuilder<DbAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DbAuditParameterBuilder class.
		/// </summary>
		public DbAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the DbAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DbAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DbAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DbAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DbAuditParameterBuilder
	
	#region DbAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;DbAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DbAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class DbAuditSortBuilder : SqlSortBuilder<DbAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DbAuditSqlSortBuilder class.
		/// </summary>
		public DbAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion DbAuditSortBuilder
	
} // end namespace
