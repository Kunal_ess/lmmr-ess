﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MeetingRoomDescTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MeetingRoomDescTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.MeetingRoomDescTrail, LMMR.Entities.MeetingRoomDescTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.MeetingRoomDescTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Trail_MeetingRoomDesc key.
		///		FK_MeetingRoomDesc_Trail_MeetingRoomDesc Description: 
		/// </summary>
		/// <param name="_meetingRoomDescId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDescTrail objects.</returns>
		public TList<MeetingRoomDescTrail> GetByMeetingRoomDescId(System.Int64? _meetingRoomDescId)
		{
			int count = -1;
			return GetByMeetingRoomDescId(_meetingRoomDescId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Trail_MeetingRoomDesc key.
		///		FK_MeetingRoomDesc_Trail_MeetingRoomDesc Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomDescId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDescTrail objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomDescTrail> GetByMeetingRoomDescId(TransactionManager transactionManager, System.Int64? _meetingRoomDescId)
		{
			int count = -1;
			return GetByMeetingRoomDescId(transactionManager, _meetingRoomDescId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Trail_MeetingRoomDesc key.
		///		FK_MeetingRoomDesc_Trail_MeetingRoomDesc Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomDescId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDescTrail objects.</returns>
		public TList<MeetingRoomDescTrail> GetByMeetingRoomDescId(TransactionManager transactionManager, System.Int64? _meetingRoomDescId, int start, int pageLength)
		{
			int count = -1;
			return GetByMeetingRoomDescId(transactionManager, _meetingRoomDescId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Trail_MeetingRoomDesc key.
		///		fkMeetingRoomDescTrailMeetingRoomDesc Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomDescId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDescTrail objects.</returns>
		public TList<MeetingRoomDescTrail> GetByMeetingRoomDescId(System.Int64? _meetingRoomDescId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMeetingRoomDescId(null, _meetingRoomDescId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Trail_MeetingRoomDesc key.
		///		fkMeetingRoomDescTrailMeetingRoomDesc Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomDescId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDescTrail objects.</returns>
		public TList<MeetingRoomDescTrail> GetByMeetingRoomDescId(System.Int64? _meetingRoomDescId, int start, int pageLength,out int count)
		{
			return GetByMeetingRoomDescId(null, _meetingRoomDescId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Trail_MeetingRoomDesc key.
		///		FK_MeetingRoomDesc_Trail_MeetingRoomDesc Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomDescId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDescTrail objects.</returns>
		public abstract TList<MeetingRoomDescTrail> GetByMeetingRoomDescId(TransactionManager transactionManager, System.Int64? _meetingRoomDescId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.MeetingRoomDescTrail Get(TransactionManager transactionManager, LMMR.Entities.MeetingRoomDescTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_MeetingRoomDesc_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDescTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomDescTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDescTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomDescTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDescTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomDescTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDescTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomDescTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDescTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomDescTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDescTrail"/> class.</returns>
		public abstract LMMR.Entities.MeetingRoomDescTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MeetingRoomDescTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MeetingRoomDescTrail&gt;"/></returns>
		public static TList<MeetingRoomDescTrail> Fill(IDataReader reader, TList<MeetingRoomDescTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.MeetingRoomDescTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MeetingRoomDescTrail")
					.Append("|").Append((System.Int64)reader[((int)MeetingRoomDescTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MeetingRoomDescTrail>(
					key.ToString(), // EntityTrackingKey
					"MeetingRoomDescTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.MeetingRoomDescTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)MeetingRoomDescTrailColumn.Id - 1)];
					c.MeetingRoomDescId = (reader.IsDBNull(((int)MeetingRoomDescTrailColumn.MeetingRoomDescId - 1)))?null:(System.Int64?)reader[((int)MeetingRoomDescTrailColumn.MeetingRoomDescId - 1)];
					c.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomDescTrailColumn.MeetingRoomId - 1)];
					c.LanguageId = (System.Int64)reader[((int)MeetingRoomDescTrailColumn.LanguageId - 1)];
					c.Description = (reader.IsDBNull(((int)MeetingRoomDescTrailColumn.Description - 1)))?null:(System.String)reader[((int)MeetingRoomDescTrailColumn.Description - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomDescTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomDescTrailColumn.UpdatedBy - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)MeetingRoomDescTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)MeetingRoomDescTrailColumn.UpdateDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomDescTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomDescTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.MeetingRoomDescTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)MeetingRoomDescTrailColumn.Id - 1)];
			entity.MeetingRoomDescId = (reader.IsDBNull(((int)MeetingRoomDescTrailColumn.MeetingRoomDescId - 1)))?null:(System.Int64?)reader[((int)MeetingRoomDescTrailColumn.MeetingRoomDescId - 1)];
			entity.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomDescTrailColumn.MeetingRoomId - 1)];
			entity.LanguageId = (System.Int64)reader[((int)MeetingRoomDescTrailColumn.LanguageId - 1)];
			entity.Description = (reader.IsDBNull(((int)MeetingRoomDescTrailColumn.Description - 1)))?null:(System.String)reader[((int)MeetingRoomDescTrailColumn.Description - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomDescTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomDescTrailColumn.UpdatedBy - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)MeetingRoomDescTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)MeetingRoomDescTrailColumn.UpdateDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomDescTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomDescTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.MeetingRoomDescTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.MeetingRoomDescId = Convert.IsDBNull(dataRow["MeetingRoom_DescId"]) ? null : (System.Int64?)dataRow["MeetingRoom_DescId"];
			entity.MeetingRoomId = (System.Int64)dataRow["MeetingRoom_Id"];
			entity.LanguageId = (System.Int64)dataRow["Language_Id"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomDescTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomDescTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.MeetingRoomDescTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region MeetingRoomDescIdSource	
			if (CanDeepLoad(entity, "MeetingRoomDesc|MeetingRoomDescIdSource", deepLoadType, innerList) 
				&& entity.MeetingRoomDescIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.MeetingRoomDescId ?? (long)0);
				MeetingRoomDesc tmpEntity = EntityManager.LocateEntity<MeetingRoomDesc>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoomDesc), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MeetingRoomDescIdSource = tmpEntity;
				else
					entity.MeetingRoomDescIdSource = DataRepository.MeetingRoomDescProvider.GetById(transactionManager, (entity.MeetingRoomDescId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomDescIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MeetingRoomDescIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomDescProvider.DeepLoad(transactionManager, entity.MeetingRoomDescIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MeetingRoomDescIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.MeetingRoomDescTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.MeetingRoomDescTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomDescTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.MeetingRoomDescTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region MeetingRoomDescIdSource
			if (CanDeepSave(entity, "MeetingRoomDesc|MeetingRoomDescIdSource", deepSaveType, innerList) 
				&& entity.MeetingRoomDescIdSource != null)
			{
				DataRepository.MeetingRoomDescProvider.Save(transactionManager, entity.MeetingRoomDescIdSource);
				entity.MeetingRoomDescId = entity.MeetingRoomDescIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MeetingRoomDescTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.MeetingRoomDescTrail</c>
	///</summary>
	public enum MeetingRoomDescTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>MeetingRoomDesc</c> at MeetingRoomDescIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoomDesc))]
		MeetingRoomDesc,
		}
	
	#endregion MeetingRoomDescTrailChildEntityTypes
	
	#region MeetingRoomDescTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MeetingRoomDescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomDescTrailFilterBuilder : SqlFilterBuilder<MeetingRoomDescTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailFilterBuilder class.
		/// </summary>
		public MeetingRoomDescTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomDescTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomDescTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomDescTrailFilterBuilder
	
	#region MeetingRoomDescTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MeetingRoomDescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomDescTrailParameterBuilder : ParameterizedSqlFilterBuilder<MeetingRoomDescTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailParameterBuilder class.
		/// </summary>
		public MeetingRoomDescTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomDescTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomDescTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomDescTrailParameterBuilder
	
	#region MeetingRoomDescTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MeetingRoomDescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDescTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MeetingRoomDescTrailSortBuilder : SqlSortBuilder<MeetingRoomDescTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailSqlSortBuilder class.
		/// </summary>
		public MeetingRoomDescTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MeetingRoomDescTrailSortBuilder
	
} // end namespace
