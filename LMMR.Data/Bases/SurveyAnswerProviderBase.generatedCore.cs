﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SurveyAnswerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SurveyAnswerProviderBaseCore : EntityProviderBase<LMMR.Entities.SurveyAnswer, LMMR.Entities.SurveyAnswerKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SurveyAnswerKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Categ__574B3C63 key.
		///		FK__survey_An__Categ__574B3C63 Description: 
		/// </summary>
		/// <param name="_categoryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public TList<SurveyAnswer> GetByCategoryId(System.Int32? _categoryId)
		{
			int count = -1;
			return GetByCategoryId(_categoryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Categ__574B3C63 key.
		///		FK__survey_An__Categ__574B3C63 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		/// <remarks></remarks>
		public TList<SurveyAnswer> GetByCategoryId(TransactionManager transactionManager, System.Int32? _categoryId)
		{
			int count = -1;
			return GetByCategoryId(transactionManager, _categoryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Categ__574B3C63 key.
		///		FK__survey_An__Categ__574B3C63 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public TList<SurveyAnswer> GetByCategoryId(TransactionManager transactionManager, System.Int32? _categoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryId(transactionManager, _categoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Categ__574B3C63 key.
		///		fkSurveyAnCateg574b3c63 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public TList<SurveyAnswer> GetByCategoryId(System.Int32? _categoryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCategoryId(null, _categoryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Categ__574B3C63 key.
		///		fkSurveyAnCateg574b3c63 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public TList<SurveyAnswer> GetByCategoryId(System.Int32? _categoryId, int start, int pageLength,out int count)
		{
			return GetByCategoryId(null, _categoryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Categ__574B3C63 key.
		///		FK__survey_An__Categ__574B3C63 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public abstract TList<SurveyAnswer> GetByCategoryId(TransactionManager transactionManager, System.Int32? _categoryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Quest__583F609C key.
		///		FK__survey_An__Quest__583F609C Description: 
		/// </summary>
		/// <param name="_questionId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public TList<SurveyAnswer> GetByQuestionId(System.Int32? _questionId)
		{
			int count = -1;
			return GetByQuestionId(_questionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Quest__583F609C key.
		///		FK__survey_An__Quest__583F609C Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		/// <remarks></remarks>
		public TList<SurveyAnswer> GetByQuestionId(TransactionManager transactionManager, System.Int32? _questionId)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Quest__583F609C key.
		///		FK__survey_An__Quest__583F609C Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public TList<SurveyAnswer> GetByQuestionId(TransactionManager transactionManager, System.Int32? _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Quest__583F609C key.
		///		fkSurveyAnQuest583f609c Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public TList<SurveyAnswer> GetByQuestionId(System.Int32? _questionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionId(null, _questionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Quest__583F609C key.
		///		fkSurveyAnQuest583f609c Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public TList<SurveyAnswer> GetByQuestionId(System.Int32? _questionId, int start, int pageLength,out int count)
		{
			return GetByQuestionId(null, _questionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_An__Quest__583F609C key.
		///		FK__survey_An__Quest__583F609C Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyAnswer objects.</returns>
		public abstract TList<SurveyAnswer> GetByQuestionId(TransactionManager transactionManager, System.Int32? _questionId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SurveyAnswer Get(TransactionManager transactionManager, LMMR.Entities.SurveyAnswerKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__survey_A__3214EC07546ECFB8 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyAnswer"/> class.</returns>
		public LMMR.Entities.SurveyAnswer GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_A__3214EC07546ECFB8 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyAnswer"/> class.</returns>
		public LMMR.Entities.SurveyAnswer GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_A__3214EC07546ECFB8 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyAnswer"/> class.</returns>
		public LMMR.Entities.SurveyAnswer GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_A__3214EC07546ECFB8 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyAnswer"/> class.</returns>
		public LMMR.Entities.SurveyAnswer GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_A__3214EC07546ECFB8 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyAnswer"/> class.</returns>
		public LMMR.Entities.SurveyAnswer GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_A__3214EC07546ECFB8 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyAnswer"/> class.</returns>
		public abstract LMMR.Entities.SurveyAnswer GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SurveyAnswer&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SurveyAnswer&gt;"/></returns>
		public static TList<SurveyAnswer> Fill(IDataReader reader, TList<SurveyAnswer> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SurveyAnswer c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SurveyAnswer")
					.Append("|").Append((System.Int32)reader[((int)SurveyAnswerColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SurveyAnswer>(
					key.ToString(), // EntityTrackingKey
					"SurveyAnswer",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SurveyAnswer();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)SurveyAnswerColumn.Id - 1)];
					c.Userid = (reader.IsDBNull(((int)SurveyAnswerColumn.Userid - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.Userid - 1)];
					c.QuestionId = (reader.IsDBNull(((int)SurveyAnswerColumn.QuestionId - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.QuestionId - 1)];
					c.CategoryId = (reader.IsDBNull(((int)SurveyAnswerColumn.CategoryId - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.CategoryId - 1)];
					c.VenueId = (reader.IsDBNull(((int)SurveyAnswerColumn.VenueId - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.VenueId - 1)];
					c.Ratings = (reader.IsDBNull(((int)SurveyAnswerColumn.Ratings - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.Ratings - 1)];
					c.Additionalcomments = (reader.IsDBNull(((int)SurveyAnswerColumn.Additionalcomments - 1)))?null:(System.String)reader[((int)SurveyAnswerColumn.Additionalcomments - 1)];
					c.DateOfSurvey = (reader.IsDBNull(((int)SurveyAnswerColumn.DateOfSurvey - 1)))?null:(System.DateTime?)reader[((int)SurveyAnswerColumn.DateOfSurvey - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyAnswer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyAnswer"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SurveyAnswer entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)SurveyAnswerColumn.Id - 1)];
			entity.Userid = (reader.IsDBNull(((int)SurveyAnswerColumn.Userid - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.Userid - 1)];
			entity.QuestionId = (reader.IsDBNull(((int)SurveyAnswerColumn.QuestionId - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.QuestionId - 1)];
			entity.CategoryId = (reader.IsDBNull(((int)SurveyAnswerColumn.CategoryId - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.CategoryId - 1)];
			entity.VenueId = (reader.IsDBNull(((int)SurveyAnswerColumn.VenueId - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.VenueId - 1)];
			entity.Ratings = (reader.IsDBNull(((int)SurveyAnswerColumn.Ratings - 1)))?null:(System.Int32?)reader[((int)SurveyAnswerColumn.Ratings - 1)];
			entity.Additionalcomments = (reader.IsDBNull(((int)SurveyAnswerColumn.Additionalcomments - 1)))?null:(System.String)reader[((int)SurveyAnswerColumn.Additionalcomments - 1)];
			entity.DateOfSurvey = (reader.IsDBNull(((int)SurveyAnswerColumn.DateOfSurvey - 1)))?null:(System.DateTime?)reader[((int)SurveyAnswerColumn.DateOfSurvey - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyAnswer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyAnswer"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SurveyAnswer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.Userid = Convert.IsDBNull(dataRow["Userid"]) ? null : (System.Int32?)dataRow["Userid"];
			entity.QuestionId = Convert.IsDBNull(dataRow["QuestionID"]) ? null : (System.Int32?)dataRow["QuestionID"];
			entity.CategoryId = Convert.IsDBNull(dataRow["CategoryID"]) ? null : (System.Int32?)dataRow["CategoryID"];
			entity.VenueId = Convert.IsDBNull(dataRow["VenueID"]) ? null : (System.Int32?)dataRow["VenueID"];
			entity.Ratings = Convert.IsDBNull(dataRow["Ratings"]) ? null : (System.Int32?)dataRow["Ratings"];
			entity.Additionalcomments = Convert.IsDBNull(dataRow["Additionalcomments"]) ? null : (System.String)dataRow["Additionalcomments"];
			entity.DateOfSurvey = Convert.IsDBNull(dataRow["DateOfSurvey"]) ? null : (System.DateTime?)dataRow["DateOfSurvey"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyAnswer"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyAnswer Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SurveyAnswer entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CategoryIdSource	
			if (CanDeepLoad(entity, "SurveyCategory|CategoryIdSource", deepLoadType, innerList) 
				&& entity.CategoryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CategoryId ?? (int)0);
				SurveyCategory tmpEntity = EntityManager.LocateEntity<SurveyCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(SurveyCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CategoryIdSource = tmpEntity;
				else
					entity.CategoryIdSource = DataRepository.SurveyCategoryProvider.GetById(transactionManager, (entity.CategoryId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SurveyCategoryProvider.DeepLoad(transactionManager, entity.CategoryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CategoryIdSource

			#region QuestionIdSource	
			if (CanDeepLoad(entity, "SurveyQuestion|QuestionIdSource", deepLoadType, innerList) 
				&& entity.QuestionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.QuestionId ?? (int)0);
				SurveyQuestion tmpEntity = EntityManager.LocateEntity<SurveyQuestion>(EntityLocator.ConstructKeyFromPkItems(typeof(SurveyQuestion), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionIdSource = tmpEntity;
				else
					entity.QuestionIdSource = DataRepository.SurveyQuestionProvider.GetById(transactionManager, (entity.QuestionId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SurveyQuestionProvider.DeepLoad(transactionManager, entity.QuestionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SurveyAnswer object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SurveyAnswer instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyAnswer Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SurveyAnswer entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CategoryIdSource
			if (CanDeepSave(entity, "SurveyCategory|CategoryIdSource", deepSaveType, innerList) 
				&& entity.CategoryIdSource != null)
			{
				DataRepository.SurveyCategoryProvider.Save(transactionManager, entity.CategoryIdSource);
				entity.CategoryId = entity.CategoryIdSource.Id;
			}
			#endregion 
			
			#region QuestionIdSource
			if (CanDeepSave(entity, "SurveyQuestion|QuestionIdSource", deepSaveType, innerList) 
				&& entity.QuestionIdSource != null)
			{
				DataRepository.SurveyQuestionProvider.Save(transactionManager, entity.QuestionIdSource);
				entity.QuestionId = entity.QuestionIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SurveyAnswerChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SurveyAnswer</c>
	///</summary>
	public enum SurveyAnswerChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>SurveyCategory</c> at CategoryIdSource
		///</summary>
		[ChildEntityType(typeof(SurveyCategory))]
		SurveyCategory,
			
		///<summary>
		/// Composite Property for <c>SurveyQuestion</c> at QuestionIdSource
		///</summary>
		[ChildEntityType(typeof(SurveyQuestion))]
		SurveyQuestion,
		}
	
	#endregion SurveyAnswerChildEntityTypes
	
	#region SurveyAnswerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SurveyAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyAnswerFilterBuilder : SqlFilterBuilder<SurveyAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyAnswerFilterBuilder class.
		/// </summary>
		public SurveyAnswerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyAnswerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyAnswerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyAnswerFilterBuilder
	
	#region SurveyAnswerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SurveyAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyAnswerParameterBuilder : ParameterizedSqlFilterBuilder<SurveyAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyAnswerParameterBuilder class.
		/// </summary>
		public SurveyAnswerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyAnswerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyAnswerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyAnswerParameterBuilder
	
	#region SurveyAnswerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SurveyAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyAnswer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SurveyAnswerSortBuilder : SqlSortBuilder<SurveyAnswerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyAnswerSqlSortBuilder class.
		/// </summary>
		public SurveyAnswerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SurveyAnswerSortBuilder
	
} // end namespace
