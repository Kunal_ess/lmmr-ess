﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CountryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CountryProviderBaseCore : EntityProviderBase<LMMR.Entities.Country, LMMR.Entities.CountryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.CountryKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Country_Currency key.
		///		FK_Country_Currency Description: 
		/// </summary>
		/// <param name="_currencyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Country objects.</returns>
		public TList<Country> GetByCurrencyId(System.Int64 _currencyId)
		{
			int count = -1;
			return GetByCurrencyId(_currencyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Country_Currency key.
		///		FK_Country_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Country objects.</returns>
		/// <remarks></remarks>
		public TList<Country> GetByCurrencyId(TransactionManager transactionManager, System.Int64 _currencyId)
		{
			int count = -1;
			return GetByCurrencyId(transactionManager, _currencyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Country_Currency key.
		///		FK_Country_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Country objects.</returns>
		public TList<Country> GetByCurrencyId(TransactionManager transactionManager, System.Int64 _currencyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCurrencyId(transactionManager, _currencyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Country_Currency key.
		///		fkCountryCurrency Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currencyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Country objects.</returns>
		public TList<Country> GetByCurrencyId(System.Int64 _currencyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCurrencyId(null, _currencyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Country_Currency key.
		///		fkCountryCurrency Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currencyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Country objects.</returns>
		public TList<Country> GetByCurrencyId(System.Int64 _currencyId, int start, int pageLength,out int count)
		{
			return GetByCurrencyId(null, _currencyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Country_Currency key.
		///		FK_Country_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Country objects.</returns>
		public abstract TList<Country> GetByCurrencyId(TransactionManager transactionManager, System.Int64 _currencyId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Country Get(TransactionManager transactionManager, LMMR.Entities.CountryKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Country index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Country"/> class.</returns>
		public LMMR.Entities.Country GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Country index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Country"/> class.</returns>
		public LMMR.Entities.Country GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Country index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Country"/> class.</returns>
		public LMMR.Entities.Country GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Country index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Country"/> class.</returns>
		public LMMR.Entities.Country GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Country index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Country"/> class.</returns>
		public LMMR.Entities.Country GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Country index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Country"/> class.</returns>
		public abstract LMMR.Entities.Country GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Country&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Country&gt;"/></returns>
		public static TList<Country> Fill(IDataReader reader, TList<Country> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Country c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Country")
					.Append("|").Append((System.Int64)reader[((int)CountryColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Country>(
					key.ToString(), // EntityTrackingKey
					"Country",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Country();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)CountryColumn.Id - 1)];
					c.CountryName = (reader.IsDBNull(((int)CountryColumn.CountryName - 1)))?null:(System.String)reader[((int)CountryColumn.CountryName - 1)];
					c.CurrencyId = (System.Int64)reader[((int)CountryColumn.CurrencyId - 1)];
					c.IsActive = (System.Boolean)reader[((int)CountryColumn.IsActive - 1)];
					c.IsForAll = (reader.IsDBNull(((int)CountryColumn.IsForAll - 1)))?null:(System.Boolean?)reader[((int)CountryColumn.IsForAll - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Country"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Country"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Country entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)CountryColumn.Id - 1)];
			entity.CountryName = (reader.IsDBNull(((int)CountryColumn.CountryName - 1)))?null:(System.String)reader[((int)CountryColumn.CountryName - 1)];
			entity.CurrencyId = (System.Int64)reader[((int)CountryColumn.CurrencyId - 1)];
			entity.IsActive = (System.Boolean)reader[((int)CountryColumn.IsActive - 1)];
			entity.IsForAll = (reader.IsDBNull(((int)CountryColumn.IsForAll - 1)))?null:(System.Boolean?)reader[((int)CountryColumn.IsForAll - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Country"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Country"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Country entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.CountryName = Convert.IsDBNull(dataRow["CountryName"]) ? null : (System.String)dataRow["CountryName"];
			entity.CurrencyId = (System.Int64)dataRow["CurrencyId"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.IsForAll = Convert.IsDBNull(dataRow["IsForAll"]) ? null : (System.Boolean?)dataRow["IsForAll"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Country"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Country Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Country entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CurrencyIdSource	
			if (CanDeepLoad(entity, "Currency|CurrencyIdSource", deepLoadType, innerList) 
				&& entity.CurrencyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CurrencyId;
				Currency tmpEntity = EntityManager.LocateEntity<Currency>(EntityLocator.ConstructKeyFromPkItems(typeof(Currency), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CurrencyIdSource = tmpEntity;
				else
					entity.CurrencyIdSource = DataRepository.CurrencyProvider.GetById(transactionManager, entity.CurrencyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CurrencyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CurrencyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CurrencyProvider.DeepLoad(transactionManager, entity.CurrencyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CurrencyIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region UserDetailsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<UserDetails>|UserDetailsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserDetailsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UserDetailsCollection = DataRepository.UserDetailsProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.UserDetailsCollection.Count > 0)
				{
					deepHandles.Add("UserDetailsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<UserDetails>) DataRepository.UserDetailsProvider.DeepLoad,
						new object[] { transactionManager, entity.UserDetailsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageItemsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageItems>|PackageItemsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageItemsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageItemsCollection = DataRepository.PackageItemsProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.PackageItemsCollection.Count > 0)
				{
					deepHandles.Add("PackageItemsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageItems>) DataRepository.PackageItemsProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageItemsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region RpfCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Rpf>|RpfCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RpfCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.RpfCollection = DataRepository.RpfProvider.GetByCountryid(transactionManager, entity.Id);

				if (deep && entity.RpfCollection.Count > 0)
				{
					deepHandles.Add("RpfCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Rpf>) DataRepository.RpfProvider.DeepLoad,
						new object[] { transactionManager, entity.RpfCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CountryLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CountryLanguage>|CountryLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CountryLanguageCollection = DataRepository.CountryLanguageProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.CountryLanguageCollection.Count > 0)
				{
					deepHandles.Add("CountryLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CountryLanguage>) DataRepository.CountryLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.CountryLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FrontEndBottomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FrontEndBottom>|FrontEndBottomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FrontEndBottomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FrontEndBottomCollection = DataRepository.FrontEndBottomProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.FrontEndBottomCollection.Count > 0)
				{
					deepHandles.Add("FrontEndBottomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FrontEndBottom>) DataRepository.FrontEndBottomProvider.DeepLoad,
						new object[] { transactionManager, entity.FrontEndBottomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PriorityBasketCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PriorityBasket>|PriorityBasketCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PriorityBasketCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PriorityBasketCollection = DataRepository.PriorityBasketProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.PriorityBasketCollection.Count > 0)
				{
					deepHandles.Add("PriorityBasketCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PriorityBasket>) DataRepository.PriorityBasketProvider.DeepLoad,
						new object[] { transactionManager, entity.PriorityBasketCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageMasterCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageMaster>|PackageMasterCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageMasterCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageMasterCollection = DataRepository.PackageMasterProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.PackageMasterCollection.Count > 0)
				{
					deepHandles.Add("PackageMasterCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageMaster>) DataRepository.PackageMasterProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageMasterCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CreditCardDetailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CreditCardDetail>|CreditCardDetailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreditCardDetailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CreditCardDetailCollection = DataRepository.CreditCardDetailProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.CreditCardDetailCollection.Count > 0)
				{
					deepHandles.Add("CreditCardDetailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CreditCardDetail>) DataRepository.CreditCardDetailProvider.DeepLoad,
						new object[] { transactionManager, entity.CreditCardDetailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CityCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<City>|CityCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CityCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CityCollection = DataRepository.CityProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.CityCollection.Count > 0)
				{
					deepHandles.Add("CityCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<City>) DataRepository.CityProvider.DeepLoad,
						new object[] { transactionManager, entity.CityCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OthersCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Others>|OthersCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OthersCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OthersCollection = DataRepository.OthersProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.OthersCollection.Count > 0)
				{
					deepHandles.Add("OthersCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Others>) DataRepository.OthersProvider.DeepLoad,
						new object[] { transactionManager, entity.OthersCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HearUsQuestionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HearUsQuestion>|HearUsQuestionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HearUsQuestionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HearUsQuestionCollection = DataRepository.HearUsQuestionProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.HearUsQuestionCollection.Count > 0)
				{
					deepHandles.Add("HearUsQuestionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HearUsQuestion>) DataRepository.HearUsQuestionProvider.DeepLoad,
						new object[] { transactionManager, entity.HearUsQuestionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CancelationPolicyCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CancelationPolicy>|CancelationPolicyCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CancelationPolicyCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CancelationPolicyCollection = DataRepository.CancelationPolicyProvider.GetByCountryId(transactionManager, entity.Id);

				if (deep && entity.CancelationPolicyCollection.Count > 0)
				{
					deepHandles.Add("CancelationPolicyCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CancelationPolicy>) DataRepository.CancelationPolicyProvider.DeepLoad,
						new object[] { transactionManager, entity.CancelationPolicyCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Country object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Country instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Country Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Country entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CurrencyIdSource
			if (CanDeepSave(entity, "Currency|CurrencyIdSource", deepSaveType, innerList) 
				&& entity.CurrencyIdSource != null)
			{
				DataRepository.CurrencyProvider.Save(transactionManager, entity.CurrencyIdSource);
				entity.CurrencyId = entity.CurrencyIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<UserDetails>
				if (CanDeepSave(entity.UserDetailsCollection, "List<UserDetails>|UserDetailsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(UserDetails child in entity.UserDetailsCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.UserDetailsCollection.Count > 0 || entity.UserDetailsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.UserDetailsProvider.Save(transactionManager, entity.UserDetailsCollection);
						
						deepHandles.Add("UserDetailsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< UserDetails >) DataRepository.UserDetailsProvider.DeepSave,
							new object[] { transactionManager, entity.UserDetailsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageItems>
				if (CanDeepSave(entity.PackageItemsCollection, "List<PackageItems>|PackageItemsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageItems child in entity.PackageItemsCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.PackageItemsCollection.Count > 0 || entity.PackageItemsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageItemsProvider.Save(transactionManager, entity.PackageItemsCollection);
						
						deepHandles.Add("PackageItemsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageItems >) DataRepository.PackageItemsProvider.DeepSave,
							new object[] { transactionManager, entity.PackageItemsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Rpf>
				if (CanDeepSave(entity.RpfCollection, "List<Rpf>|RpfCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Rpf child in entity.RpfCollection)
					{
						if(child.CountryidSource != null)
						{
							child.Countryid = child.CountryidSource.Id;
						}
						else
						{
							child.Countryid = entity.Id;
						}

					}

					if (entity.RpfCollection.Count > 0 || entity.RpfCollection.DeletedItems.Count > 0)
					{
						//DataRepository.RpfProvider.Save(transactionManager, entity.RpfCollection);
						
						deepHandles.Add("RpfCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Rpf >) DataRepository.RpfProvider.DeepSave,
							new object[] { transactionManager, entity.RpfCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CountryLanguage>
				if (CanDeepSave(entity.CountryLanguageCollection, "List<CountryLanguage>|CountryLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CountryLanguage child in entity.CountryLanguageCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.CountryLanguageCollection.Count > 0 || entity.CountryLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CountryLanguageProvider.Save(transactionManager, entity.CountryLanguageCollection);
						
						deepHandles.Add("CountryLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CountryLanguage >) DataRepository.CountryLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.CountryLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FrontEndBottom>
				if (CanDeepSave(entity.FrontEndBottomCollection, "List<FrontEndBottom>|FrontEndBottomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FrontEndBottom child in entity.FrontEndBottomCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.FrontEndBottomCollection.Count > 0 || entity.FrontEndBottomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FrontEndBottomProvider.Save(transactionManager, entity.FrontEndBottomCollection);
						
						deepHandles.Add("FrontEndBottomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FrontEndBottom >) DataRepository.FrontEndBottomProvider.DeepSave,
							new object[] { transactionManager, entity.FrontEndBottomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PriorityBasket>
				if (CanDeepSave(entity.PriorityBasketCollection, "List<PriorityBasket>|PriorityBasketCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PriorityBasket child in entity.PriorityBasketCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.PriorityBasketCollection.Count > 0 || entity.PriorityBasketCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PriorityBasketProvider.Save(transactionManager, entity.PriorityBasketCollection);
						
						deepHandles.Add("PriorityBasketCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PriorityBasket >) DataRepository.PriorityBasketProvider.DeepSave,
							new object[] { transactionManager, entity.PriorityBasketCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageMaster>
				if (CanDeepSave(entity.PackageMasterCollection, "List<PackageMaster>|PackageMasterCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageMaster child in entity.PackageMasterCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.PackageMasterCollection.Count > 0 || entity.PackageMasterCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageMasterProvider.Save(transactionManager, entity.PackageMasterCollection);
						
						deepHandles.Add("PackageMasterCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageMaster >) DataRepository.PackageMasterProvider.DeepSave,
							new object[] { transactionManager, entity.PackageMasterCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CreditCardDetail>
				if (CanDeepSave(entity.CreditCardDetailCollection, "List<CreditCardDetail>|CreditCardDetailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CreditCardDetail child in entity.CreditCardDetailCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.CreditCardDetailCollection.Count > 0 || entity.CreditCardDetailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CreditCardDetailProvider.Save(transactionManager, entity.CreditCardDetailCollection);
						
						deepHandles.Add("CreditCardDetailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CreditCardDetail >) DataRepository.CreditCardDetailProvider.DeepSave,
							new object[] { transactionManager, entity.CreditCardDetailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<City>
				if (CanDeepSave(entity.CityCollection, "List<City>|CityCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(City child in entity.CityCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.CityCollection.Count > 0 || entity.CityCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CityProvider.Save(transactionManager, entity.CityCollection);
						
						deepHandles.Add("CityCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< City >) DataRepository.CityProvider.DeepSave,
							new object[] { transactionManager, entity.CityCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Others>
				if (CanDeepSave(entity.OthersCollection, "List<Others>|OthersCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Others child in entity.OthersCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.OthersCollection.Count > 0 || entity.OthersCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OthersProvider.Save(transactionManager, entity.OthersCollection);
						
						deepHandles.Add("OthersCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Others >) DataRepository.OthersProvider.DeepSave,
							new object[] { transactionManager, entity.OthersCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HearUsQuestion>
				if (CanDeepSave(entity.HearUsQuestionCollection, "List<HearUsQuestion>|HearUsQuestionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HearUsQuestion child in entity.HearUsQuestionCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.HearUsQuestionCollection.Count > 0 || entity.HearUsQuestionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HearUsQuestionProvider.Save(transactionManager, entity.HearUsQuestionCollection);
						
						deepHandles.Add("HearUsQuestionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HearUsQuestion >) DataRepository.HearUsQuestionProvider.DeepSave,
							new object[] { transactionManager, entity.HearUsQuestionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CancelationPolicy>
				if (CanDeepSave(entity.CancelationPolicyCollection, "List<CancelationPolicy>|CancelationPolicyCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CancelationPolicy child in entity.CancelationPolicyCollection)
					{
						if(child.CountryIdSource != null)
						{
							child.CountryId = child.CountryIdSource.Id;
						}
						else
						{
							child.CountryId = entity.Id;
						}

					}

					if (entity.CancelationPolicyCollection.Count > 0 || entity.CancelationPolicyCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CancelationPolicyProvider.Save(transactionManager, entity.CancelationPolicyCollection);
						
						deepHandles.Add("CancelationPolicyCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CancelationPolicy >) DataRepository.CancelationPolicyProvider.DeepSave,
							new object[] { transactionManager, entity.CancelationPolicyCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CountryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Country</c>
	///</summary>
	public enum CountryChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Currency</c> at CurrencyIdSource
		///</summary>
		[ChildEntityType(typeof(Currency))]
		Currency,
	
		///<summary>
		/// Collection of <c>Country</c> as OneToMany for UserDetailsCollection
		///</summary>
		[ChildEntityType(typeof(TList<UserDetails>))]
		UserDetailsCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for PackageItemsCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageItems>))]
		PackageItemsCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for RpfCollection
		///</summary>
		[ChildEntityType(typeof(TList<Rpf>))]
		RpfCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for CountryLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<CountryLanguage>))]
		CountryLanguageCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for FrontEndBottomCollection
		///</summary>
		[ChildEntityType(typeof(TList<FrontEndBottom>))]
		FrontEndBottomCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for PriorityBasketCollection
		///</summary>
		[ChildEntityType(typeof(TList<PriorityBasket>))]
		PriorityBasketCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for PackageMasterCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageMaster>))]
		PackageMasterCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for CreditCardDetailCollection
		///</summary>
		[ChildEntityType(typeof(TList<CreditCardDetail>))]
		CreditCardDetailCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for CityCollection
		///</summary>
		[ChildEntityType(typeof(TList<City>))]
		CityCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for OthersCollection
		///</summary>
		[ChildEntityType(typeof(TList<Others>))]
		OthersCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for HearUsQuestionCollection
		///</summary>
		[ChildEntityType(typeof(TList<HearUsQuestion>))]
		HearUsQuestionCollection,

		///<summary>
		/// Collection of <c>Country</c> as OneToMany for CancelationPolicyCollection
		///</summary>
		[ChildEntityType(typeof(TList<CancelationPolicy>))]
		CancelationPolicyCollection,
	}
	
	#endregion CountryChildEntityTypes
	
	#region CountryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryFilterBuilder : SqlFilterBuilder<CountryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryFilterBuilder class.
		/// </summary>
		public CountryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryFilterBuilder
	
	#region CountryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryParameterBuilder : ParameterizedSqlFilterBuilder<CountryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryParameterBuilder class.
		/// </summary>
		public CountryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryParameterBuilder
	
	#region CountrySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CountrySortBuilder : SqlSortBuilder<CountryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountrySqlSortBuilder class.
		/// </summary>
		public CountrySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CountrySortBuilder
	
} // end namespace
