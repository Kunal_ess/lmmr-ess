﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BuildPackageConfigureDescProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BuildPackageConfigureDescProviderBaseCore : EntityProviderBase<LMMR.Entities.BuildPackageConfigureDesc, LMMR.Entities.BuildPackageConfigureDescKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BuildPackageConfigureDescKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_BuildPackageConfigure key.
		///		FK_BuildPackageConfigureDesc_BuildPackageConfigure Description: 
		/// </summary>
		/// <param name="_buildPackageConfigId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public TList<BuildPackageConfigureDesc> GetByBuildPackageConfigId(System.Int64 _buildPackageConfigId)
		{
			int count = -1;
			return GetByBuildPackageConfigId(_buildPackageConfigId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_BuildPackageConfigure key.
		///		FK_BuildPackageConfigureDesc_BuildPackageConfigure Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_buildPackageConfigId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		/// <remarks></remarks>
		public TList<BuildPackageConfigureDesc> GetByBuildPackageConfigId(TransactionManager transactionManager, System.Int64 _buildPackageConfigId)
		{
			int count = -1;
			return GetByBuildPackageConfigId(transactionManager, _buildPackageConfigId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_BuildPackageConfigure key.
		///		FK_BuildPackageConfigureDesc_BuildPackageConfigure Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_buildPackageConfigId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public TList<BuildPackageConfigureDesc> GetByBuildPackageConfigId(TransactionManager transactionManager, System.Int64 _buildPackageConfigId, int start, int pageLength)
		{
			int count = -1;
			return GetByBuildPackageConfigId(transactionManager, _buildPackageConfigId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_BuildPackageConfigure key.
		///		fkBuildPackageConfigureDescBuildPackageConfigure Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_buildPackageConfigId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public TList<BuildPackageConfigureDesc> GetByBuildPackageConfigId(System.Int64 _buildPackageConfigId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBuildPackageConfigId(null, _buildPackageConfigId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_BuildPackageConfigure key.
		///		fkBuildPackageConfigureDescBuildPackageConfigure Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_buildPackageConfigId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public TList<BuildPackageConfigureDesc> GetByBuildPackageConfigId(System.Int64 _buildPackageConfigId, int start, int pageLength,out int count)
		{
			return GetByBuildPackageConfigId(null, _buildPackageConfigId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_BuildPackageConfigure key.
		///		FK_BuildPackageConfigureDesc_BuildPackageConfigure Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_buildPackageConfigId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public abstract TList<BuildPackageConfigureDesc> GetByBuildPackageConfigId(TransactionManager transactionManager, System.Int64 _buildPackageConfigId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_PackageItems key.
		///		FK_BuildPackageConfigureDesc_PackageItems Description: 
		/// </summary>
		/// <param name="_sectionDescId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public TList<BuildPackageConfigureDesc> GetBySectionDescId(System.Int64 _sectionDescId)
		{
			int count = -1;
			return GetBySectionDescId(_sectionDescId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_PackageItems key.
		///		FK_BuildPackageConfigureDesc_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionDescId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		/// <remarks></remarks>
		public TList<BuildPackageConfigureDesc> GetBySectionDescId(TransactionManager transactionManager, System.Int64 _sectionDescId)
		{
			int count = -1;
			return GetBySectionDescId(transactionManager, _sectionDescId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_PackageItems key.
		///		FK_BuildPackageConfigureDesc_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionDescId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public TList<BuildPackageConfigureDesc> GetBySectionDescId(TransactionManager transactionManager, System.Int64 _sectionDescId, int start, int pageLength)
		{
			int count = -1;
			return GetBySectionDescId(transactionManager, _sectionDescId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_PackageItems key.
		///		fkBuildPackageConfigureDescPackageItems Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sectionDescId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public TList<BuildPackageConfigureDesc> GetBySectionDescId(System.Int64 _sectionDescId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySectionDescId(null, _sectionDescId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_PackageItems key.
		///		fkBuildPackageConfigureDescPackageItems Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sectionDescId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public TList<BuildPackageConfigureDesc> GetBySectionDescId(System.Int64 _sectionDescId, int start, int pageLength,out int count)
		{
			return GetBySectionDescId(null, _sectionDescId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigureDesc_PackageItems key.
		///		FK_BuildPackageConfigureDesc_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionDescId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigureDesc objects.</returns>
		public abstract TList<BuildPackageConfigureDesc> GetBySectionDescId(TransactionManager transactionManager, System.Int64 _sectionDescId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BuildPackageConfigureDesc Get(TransactionManager transactionManager, LMMR.Entities.BuildPackageConfigureDescKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BuildPackageConfigureDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigureDesc GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildPackageConfigureDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigureDesc GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildPackageConfigureDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigureDesc GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildPackageConfigureDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigureDesc GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildPackageConfigureDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigureDesc GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildPackageConfigureDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> class.</returns>
		public abstract LMMR.Entities.BuildPackageConfigureDesc GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BuildPackageConfigureDesc&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BuildPackageConfigureDesc&gt;"/></returns>
		public static TList<BuildPackageConfigureDesc> Fill(IDataReader reader, TList<BuildPackageConfigureDesc> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BuildPackageConfigureDesc c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BuildPackageConfigureDesc")
					.Append("|").Append((System.Int64)reader[((int)BuildPackageConfigureDescColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BuildPackageConfigureDesc>(
					key.ToString(), // EntityTrackingKey
					"BuildPackageConfigureDesc",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BuildPackageConfigureDesc();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BuildPackageConfigureDescColumn.Id - 1)];
					c.SectionDescId = (System.Int64)reader[((int)BuildPackageConfigureDescColumn.SectionDescId - 1)];
					c.BuildPackageConfigId = (System.Int64)reader[((int)BuildPackageConfigureDescColumn.BuildPackageConfigId - 1)];
					c.FromTime = (reader.IsDBNull(((int)BuildPackageConfigureDescColumn.FromTime - 1)))?null:(System.String)reader[((int)BuildPackageConfigureDescColumn.FromTime - 1)];
					c.ToTime = (reader.IsDBNull(((int)BuildPackageConfigureDescColumn.ToTime - 1)))?null:(System.String)reader[((int)BuildPackageConfigureDescColumn.ToTime - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BuildPackageConfigureDesc entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BuildPackageConfigureDescColumn.Id - 1)];
			entity.SectionDescId = (System.Int64)reader[((int)BuildPackageConfigureDescColumn.SectionDescId - 1)];
			entity.BuildPackageConfigId = (System.Int64)reader[((int)BuildPackageConfigureDescColumn.BuildPackageConfigId - 1)];
			entity.FromTime = (reader.IsDBNull(((int)BuildPackageConfigureDescColumn.FromTime - 1)))?null:(System.String)reader[((int)BuildPackageConfigureDescColumn.FromTime - 1)];
			entity.ToTime = (reader.IsDBNull(((int)BuildPackageConfigureDescColumn.ToTime - 1)))?null:(System.String)reader[((int)BuildPackageConfigureDescColumn.ToTime - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BuildPackageConfigureDesc entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.SectionDescId = (System.Int64)dataRow["SectionDescId"];
			entity.BuildPackageConfigId = (System.Int64)dataRow["BuildPackageConfigId"];
			entity.FromTime = Convert.IsDBNull(dataRow["FromTime"]) ? null : (System.String)dataRow["FromTime"];
			entity.ToTime = Convert.IsDBNull(dataRow["ToTime"]) ? null : (System.String)dataRow["ToTime"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildPackageConfigureDesc"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BuildPackageConfigureDesc Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BuildPackageConfigureDesc entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BuildPackageConfigIdSource	
			if (CanDeepLoad(entity, "BuildPackageConfigure|BuildPackageConfigIdSource", deepLoadType, innerList) 
				&& entity.BuildPackageConfigIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BuildPackageConfigId;
				BuildPackageConfigure tmpEntity = EntityManager.LocateEntity<BuildPackageConfigure>(EntityLocator.ConstructKeyFromPkItems(typeof(BuildPackageConfigure), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BuildPackageConfigIdSource = tmpEntity;
				else
					entity.BuildPackageConfigIdSource = DataRepository.BuildPackageConfigureProvider.GetById(transactionManager, entity.BuildPackageConfigId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildPackageConfigIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BuildPackageConfigIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BuildPackageConfigureProvider.DeepLoad(transactionManager, entity.BuildPackageConfigIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BuildPackageConfigIdSource

			#region SectionDescIdSource	
			if (CanDeepLoad(entity, "PackageItems|SectionDescIdSource", deepLoadType, innerList) 
				&& entity.SectionDescIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SectionDescId;
				PackageItems tmpEntity = EntityManager.LocateEntity<PackageItems>(EntityLocator.ConstructKeyFromPkItems(typeof(PackageItems), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SectionDescIdSource = tmpEntity;
				else
					entity.SectionDescIdSource = DataRepository.PackageItemsProvider.GetById(transactionManager, entity.SectionDescId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SectionDescIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SectionDescIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PackageItemsProvider.DeepLoad(transactionManager, entity.SectionDescIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SectionDescIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BuildPackageConfigureDesc object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BuildPackageConfigureDesc instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BuildPackageConfigureDesc Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BuildPackageConfigureDesc entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BuildPackageConfigIdSource
			if (CanDeepSave(entity, "BuildPackageConfigure|BuildPackageConfigIdSource", deepSaveType, innerList) 
				&& entity.BuildPackageConfigIdSource != null)
			{
				DataRepository.BuildPackageConfigureProvider.Save(transactionManager, entity.BuildPackageConfigIdSource);
				entity.BuildPackageConfigId = entity.BuildPackageConfigIdSource.Id;
			}
			#endregion 
			
			#region SectionDescIdSource
			if (CanDeepSave(entity, "PackageItems|SectionDescIdSource", deepSaveType, innerList) 
				&& entity.SectionDescIdSource != null)
			{
				DataRepository.PackageItemsProvider.Save(transactionManager, entity.SectionDescIdSource);
				entity.SectionDescId = entity.SectionDescIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BuildPackageConfigureDescChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BuildPackageConfigureDesc</c>
	///</summary>
	public enum BuildPackageConfigureDescChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BuildPackageConfigure</c> at BuildPackageConfigIdSource
		///</summary>
		[ChildEntityType(typeof(BuildPackageConfigure))]
		BuildPackageConfigure,
			
		///<summary>
		/// Composite Property for <c>PackageItems</c> at SectionDescIdSource
		///</summary>
		[ChildEntityType(typeof(PackageItems))]
		PackageItems,
		}
	
	#endregion BuildPackageConfigureDescChildEntityTypes
	
	#region BuildPackageConfigureDescFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BuildPackageConfigureDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigureDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildPackageConfigureDescFilterBuilder : SqlFilterBuilder<BuildPackageConfigureDescColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescFilterBuilder class.
		/// </summary>
		public BuildPackageConfigureDescFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildPackageConfigureDescFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildPackageConfigureDescFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildPackageConfigureDescFilterBuilder
	
	#region BuildPackageConfigureDescParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BuildPackageConfigureDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigureDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildPackageConfigureDescParameterBuilder : ParameterizedSqlFilterBuilder<BuildPackageConfigureDescColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescParameterBuilder class.
		/// </summary>
		public BuildPackageConfigureDescParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildPackageConfigureDescParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildPackageConfigureDescParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildPackageConfigureDescParameterBuilder
	
	#region BuildPackageConfigureDescSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BuildPackageConfigureDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigureDesc"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BuildPackageConfigureDescSortBuilder : SqlSortBuilder<BuildPackageConfigureDescColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescSqlSortBuilder class.
		/// </summary>
		public BuildPackageConfigureDescSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BuildPackageConfigureDescSortBuilder
	
} // end namespace
