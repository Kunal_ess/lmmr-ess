﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="InvoiceCommissionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class InvoiceCommissionProviderBaseCore : EntityProviderBase<LMMR.Entities.InvoiceCommission, LMMR.Entities.InvoiceCommissionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.InvoiceCommissionKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.InvoiceCommission Get(TransactionManager transactionManager, LMMR.Entities.InvoiceCommissionKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_InvoiceCommission index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommission"/> class.</returns>
		public LMMR.Entities.InvoiceCommission GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommission index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommission"/> class.</returns>
		public LMMR.Entities.InvoiceCommission GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommission index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommission"/> class.</returns>
		public LMMR.Entities.InvoiceCommission GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommission index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommission"/> class.</returns>
		public LMMR.Entities.InvoiceCommission GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommission index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommission"/> class.</returns>
		public LMMR.Entities.InvoiceCommission GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommission index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommission"/> class.</returns>
		public abstract LMMR.Entities.InvoiceCommission GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;InvoiceCommission&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;InvoiceCommission&gt;"/></returns>
		public static TList<InvoiceCommission> Fill(IDataReader reader, TList<InvoiceCommission> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.InvoiceCommission c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("InvoiceCommission")
					.Append("|").Append((System.Int32)reader[((int)InvoiceCommissionColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<InvoiceCommission>(
					key.ToString(), // EntityTrackingKey
					"InvoiceCommission",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.InvoiceCommission();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)InvoiceCommissionColumn.Id - 1)];
					c.FromDay = (reader.IsDBNull(((int)InvoiceCommissionColumn.FromDay - 1)))?null:(System.Int32?)reader[((int)InvoiceCommissionColumn.FromDay - 1)];
					c.ToDay = (reader.IsDBNull(((int)InvoiceCommissionColumn.ToDay - 1)))?null:(System.Int32?)reader[((int)InvoiceCommissionColumn.ToDay - 1)];
					c.DefaultCommission = (reader.IsDBNull(((int)InvoiceCommissionColumn.DefaultCommission - 1)))?null:(System.Int32?)reader[((int)InvoiceCommissionColumn.DefaultCommission - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.InvoiceCommission"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.InvoiceCommission"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.InvoiceCommission entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)InvoiceCommissionColumn.Id - 1)];
			entity.FromDay = (reader.IsDBNull(((int)InvoiceCommissionColumn.FromDay - 1)))?null:(System.Int32?)reader[((int)InvoiceCommissionColumn.FromDay - 1)];
			entity.ToDay = (reader.IsDBNull(((int)InvoiceCommissionColumn.ToDay - 1)))?null:(System.Int32?)reader[((int)InvoiceCommissionColumn.ToDay - 1)];
			entity.DefaultCommission = (reader.IsDBNull(((int)InvoiceCommissionColumn.DefaultCommission - 1)))?null:(System.Int32?)reader[((int)InvoiceCommissionColumn.DefaultCommission - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.InvoiceCommission"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.InvoiceCommission"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.InvoiceCommission entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.FromDay = Convert.IsDBNull(dataRow["FromDay"]) ? null : (System.Int32?)dataRow["FromDay"];
			entity.ToDay = Convert.IsDBNull(dataRow["ToDay"]) ? null : (System.Int32?)dataRow["ToDay"];
			entity.DefaultCommission = Convert.IsDBNull(dataRow["DefaultCommission"]) ? null : (System.Int32?)dataRow["DefaultCommission"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.InvoiceCommission"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.InvoiceCommission Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.InvoiceCommission entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region InvoiceCommPercentageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<InvoiceCommPercentage>|InvoiceCommPercentageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'InvoiceCommPercentageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.InvoiceCommPercentageCollection = DataRepository.InvoiceCommPercentageProvider.GetByCommissionDayId(transactionManager, entity.Id);

				if (deep && entity.InvoiceCommPercentageCollection.Count > 0)
				{
					deepHandles.Add("InvoiceCommPercentageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<InvoiceCommPercentage>) DataRepository.InvoiceCommPercentageProvider.DeepLoad,
						new object[] { transactionManager, entity.InvoiceCommPercentageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.InvoiceCommission object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.InvoiceCommission instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.InvoiceCommission Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.InvoiceCommission entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<InvoiceCommPercentage>
				if (CanDeepSave(entity.InvoiceCommPercentageCollection, "List<InvoiceCommPercentage>|InvoiceCommPercentageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(InvoiceCommPercentage child in entity.InvoiceCommPercentageCollection)
					{
						if(child.CommissionDayIdSource != null)
						{
							child.CommissionDayId = child.CommissionDayIdSource.Id;
						}
						else
						{
							child.CommissionDayId = entity.Id;
						}

					}

					if (entity.InvoiceCommPercentageCollection.Count > 0 || entity.InvoiceCommPercentageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.InvoiceCommPercentageProvider.Save(transactionManager, entity.InvoiceCommPercentageCollection);
						
						deepHandles.Add("InvoiceCommPercentageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< InvoiceCommPercentage >) DataRepository.InvoiceCommPercentageProvider.DeepSave,
							new object[] { transactionManager, entity.InvoiceCommPercentageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region InvoiceCommissionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.InvoiceCommission</c>
	///</summary>
	public enum InvoiceCommissionChildEntityTypes
	{

		///<summary>
		/// Collection of <c>InvoiceCommission</c> as OneToMany for InvoiceCommPercentageCollection
		///</summary>
		[ChildEntityType(typeof(TList<InvoiceCommPercentage>))]
		InvoiceCommPercentageCollection,
	}
	
	#endregion InvoiceCommissionChildEntityTypes
	
	#region InvoiceCommissionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;InvoiceCommissionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="InvoiceCommission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceCommissionFilterBuilder : SqlFilterBuilder<InvoiceCommissionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionFilterBuilder class.
		/// </summary>
		public InvoiceCommissionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceCommissionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceCommissionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceCommissionFilterBuilder
	
	#region InvoiceCommissionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;InvoiceCommissionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="InvoiceCommission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceCommissionParameterBuilder : ParameterizedSqlFilterBuilder<InvoiceCommissionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionParameterBuilder class.
		/// </summary>
		public InvoiceCommissionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceCommissionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceCommissionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceCommissionParameterBuilder
	
	#region InvoiceCommissionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;InvoiceCommissionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="InvoiceCommission"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class InvoiceCommissionSortBuilder : SqlSortBuilder<InvoiceCommissionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionSqlSortBuilder class.
		/// </summary>
		public InvoiceCommissionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion InvoiceCommissionSortBuilder
	
} // end namespace
