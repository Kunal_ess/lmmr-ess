﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MeetingRoomDescProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MeetingRoomDescProviderBaseCore : EntityProviderBase<LMMR.Entities.MeetingRoomDesc, LMMR.Entities.MeetingRoomDescKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.MeetingRoomDescKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Language key.
		///		FK_MeetingRoomDesc_Language Description: 
		/// </summary>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByLanguageId(System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(_languageId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Language key.
		///		FK_MeetingRoomDesc_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomDesc> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Language key.
		///		FK_MeetingRoomDesc_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Language key.
		///		fkMeetingRoomDescLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByLanguageId(System.Int64 _languageId, int start, int pageLength)
		{
			int count =  -1;
			return GetByLanguageId(null, _languageId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Language key.
		///		fkMeetingRoomDescLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByLanguageId(System.Int64 _languageId, int start, int pageLength,out int count)
		{
			return GetByLanguageId(null, _languageId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Language key.
		///		FK_MeetingRoomDesc_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public abstract TList<MeetingRoomDesc> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_MeetingRoom key.
		///		FK_MeetingRoomDesc_MeetingRoom Description: 
		/// </summary>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByMeetingRoomId(System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(_meetingRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_MeetingRoom key.
		///		FK_MeetingRoomDesc_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomDesc> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_MeetingRoom key.
		///		FK_MeetingRoomDesc_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_MeetingRoom key.
		///		fkMeetingRoomDescMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_MeetingRoom key.
		///		fkMeetingRoomDescMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength,out int count)
		{
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_MeetingRoom key.
		///		FK_MeetingRoomDesc_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public abstract TList<MeetingRoomDesc> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Users key.
		///		FK_MeetingRoomDesc_Users Description: 
		/// </summary>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByUpdatedBy(System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(_updatedBy, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Users key.
		///		FK_MeetingRoomDesc_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomDesc> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Users key.
		///		FK_MeetingRoomDesc_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Users key.
		///		fkMeetingRoomDescUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength)
		{
			int count =  -1;
			return GetByUpdatedBy(null, _updatedBy, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Users key.
		///		fkMeetingRoomDescUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public TList<MeetingRoomDesc> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength,out int count)
		{
			return GetByUpdatedBy(null, _updatedBy, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomDesc_Users key.
		///		FK_MeetingRoomDesc_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomDesc objects.</returns>
		public abstract TList<MeetingRoomDesc> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.MeetingRoomDesc Get(TransactionManager transactionManager, LMMR.Entities.MeetingRoomDescKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_MeetingRoomDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDesc"/> class.</returns>
		public LMMR.Entities.MeetingRoomDesc GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDesc"/> class.</returns>
		public LMMR.Entities.MeetingRoomDesc GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDesc"/> class.</returns>
		public LMMR.Entities.MeetingRoomDesc GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDesc"/> class.</returns>
		public LMMR.Entities.MeetingRoomDesc GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDesc"/> class.</returns>
		public LMMR.Entities.MeetingRoomDesc GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomDesc"/> class.</returns>
		public abstract LMMR.Entities.MeetingRoomDesc GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MeetingRoomDesc&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MeetingRoomDesc&gt;"/></returns>
		public static TList<MeetingRoomDesc> Fill(IDataReader reader, TList<MeetingRoomDesc> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.MeetingRoomDesc c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MeetingRoomDesc")
					.Append("|").Append((System.Int64)reader[((int)MeetingRoomDescColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MeetingRoomDesc>(
					key.ToString(), // EntityTrackingKey
					"MeetingRoomDesc",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.MeetingRoomDesc();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)MeetingRoomDescColumn.Id - 1)];
					c.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomDescColumn.MeetingRoomId - 1)];
					c.LanguageId = (System.Int64)reader[((int)MeetingRoomDescColumn.LanguageId - 1)];
					c.Description = (reader.IsDBNull(((int)MeetingRoomDescColumn.Description - 1)))?null:(System.String)reader[((int)MeetingRoomDescColumn.Description - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomDescColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomDescColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomDesc"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomDesc"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.MeetingRoomDesc entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)MeetingRoomDescColumn.Id - 1)];
			entity.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomDescColumn.MeetingRoomId - 1)];
			entity.LanguageId = (System.Int64)reader[((int)MeetingRoomDescColumn.LanguageId - 1)];
			entity.Description = (reader.IsDBNull(((int)MeetingRoomDescColumn.Description - 1)))?null:(System.String)reader[((int)MeetingRoomDescColumn.Description - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomDescColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomDescColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomDesc"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomDesc"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.MeetingRoomDesc entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.MeetingRoomId = (System.Int64)dataRow["MeetingRoom_Id"];
			entity.LanguageId = (System.Int64)dataRow["Language_Id"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomDesc"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomDesc Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.MeetingRoomDesc entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region LanguageIdSource	
			if (CanDeepLoad(entity, "Language|LanguageIdSource", deepLoadType, innerList) 
				&& entity.LanguageIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LanguageId;
				Language tmpEntity = EntityManager.LocateEntity<Language>(EntityLocator.ConstructKeyFromPkItems(typeof(Language), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LanguageIdSource = tmpEntity;
				else
					entity.LanguageIdSource = DataRepository.LanguageProvider.GetById(transactionManager, entity.LanguageId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LanguageIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LanguageIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LanguageProvider.DeepLoad(transactionManager, entity.LanguageIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LanguageIdSource

			#region MeetingRoomIdSource	
			if (CanDeepLoad(entity, "MeetingRoom|MeetingRoomIdSource", deepLoadType, innerList) 
				&& entity.MeetingRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MeetingRoomId;
				MeetingRoom tmpEntity = EntityManager.LocateEntity<MeetingRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MeetingRoomIdSource = tmpEntity;
				else
					entity.MeetingRoomIdSource = DataRepository.MeetingRoomProvider.GetById(transactionManager, entity.MeetingRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MeetingRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomProvider.DeepLoad(transactionManager, entity.MeetingRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MeetingRoomIdSource

			#region UpdatedBySource	
			if (CanDeepLoad(entity, "Users|UpdatedBySource", deepLoadType, innerList) 
				&& entity.UpdatedBySource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UpdatedBy ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UpdatedBySource = tmpEntity;
				else
					entity.UpdatedBySource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UpdatedBy ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UpdatedBySource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UpdatedBySource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UpdatedBySource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UpdatedBySource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region MeetingRoomDescTrailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomDescTrail>|MeetingRoomDescTrailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomDescTrailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomDescTrailCollection = DataRepository.MeetingRoomDescTrailProvider.GetByMeetingRoomDescId(transactionManager, entity.Id);

				if (deep && entity.MeetingRoomDescTrailCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomDescTrailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomDescTrail>) DataRepository.MeetingRoomDescTrailProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomDescTrailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.MeetingRoomDesc object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.MeetingRoomDesc instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomDesc Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.MeetingRoomDesc entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region LanguageIdSource
			if (CanDeepSave(entity, "Language|LanguageIdSource", deepSaveType, innerList) 
				&& entity.LanguageIdSource != null)
			{
				DataRepository.LanguageProvider.Save(transactionManager, entity.LanguageIdSource);
				entity.LanguageId = entity.LanguageIdSource.Id;
			}
			#endregion 
			
			#region MeetingRoomIdSource
			if (CanDeepSave(entity, "MeetingRoom|MeetingRoomIdSource", deepSaveType, innerList) 
				&& entity.MeetingRoomIdSource != null)
			{
				DataRepository.MeetingRoomProvider.Save(transactionManager, entity.MeetingRoomIdSource);
				entity.MeetingRoomId = entity.MeetingRoomIdSource.Id;
			}
			#endregion 
			
			#region UpdatedBySource
			if (CanDeepSave(entity, "Users|UpdatedBySource", deepSaveType, innerList) 
				&& entity.UpdatedBySource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UpdatedBySource);
				entity.UpdatedBy = entity.UpdatedBySource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<MeetingRoomDescTrail>
				if (CanDeepSave(entity.MeetingRoomDescTrailCollection, "List<MeetingRoomDescTrail>|MeetingRoomDescTrailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomDescTrail child in entity.MeetingRoomDescTrailCollection)
					{
						if(child.MeetingRoomDescIdSource != null)
						{
							child.MeetingRoomDescId = child.MeetingRoomDescIdSource.Id;
						}
						else
						{
							child.MeetingRoomDescId = entity.Id;
						}

					}

					if (entity.MeetingRoomDescTrailCollection.Count > 0 || entity.MeetingRoomDescTrailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomDescTrailProvider.Save(transactionManager, entity.MeetingRoomDescTrailCollection);
						
						deepHandles.Add("MeetingRoomDescTrailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomDescTrail >) DataRepository.MeetingRoomDescTrailProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomDescTrailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MeetingRoomDescChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.MeetingRoomDesc</c>
	///</summary>
	public enum MeetingRoomDescChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Language</c> at LanguageIdSource
		///</summary>
		[ChildEntityType(typeof(Language))]
		Language,
			
		///<summary>
		/// Composite Property for <c>MeetingRoom</c> at MeetingRoomIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoom))]
		MeetingRoom,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UpdatedBySource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>MeetingRoomDesc</c> as OneToMany for MeetingRoomDescTrailCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomDescTrail>))]
		MeetingRoomDescTrailCollection,
	}
	
	#endregion MeetingRoomDescChildEntityTypes
	
	#region MeetingRoomDescFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MeetingRoomDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomDescFilterBuilder : SqlFilterBuilder<MeetingRoomDescColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescFilterBuilder class.
		/// </summary>
		public MeetingRoomDescFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomDescFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomDescFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomDescFilterBuilder
	
	#region MeetingRoomDescParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MeetingRoomDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomDescParameterBuilder : ParameterizedSqlFilterBuilder<MeetingRoomDescColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescParameterBuilder class.
		/// </summary>
		public MeetingRoomDescParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomDescParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomDescParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomDescParameterBuilder
	
	#region MeetingRoomDescSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MeetingRoomDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDesc"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MeetingRoomDescSortBuilder : SqlSortBuilder<MeetingRoomDescColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescSqlSortBuilder class.
		/// </summary>
		public MeetingRoomDescSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MeetingRoomDescSortBuilder
	
} // end namespace
