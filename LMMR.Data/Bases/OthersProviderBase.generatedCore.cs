﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OthersProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class OthersProviderBaseCore : EntityProviderBase<LMMR.Entities.Others, LMMR.Entities.OthersKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.OthersKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_CancelationPolicy key.
		///		FK_Others_CancelationPolicy Description: 
		/// </summary>
		/// <param name="_cancellationPolicyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCancellationPolicyId(System.Int64? _cancellationPolicyId)
		{
			int count = -1;
			return GetByCancellationPolicyId(_cancellationPolicyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_CancelationPolicy key.
		///		FK_Others_CancelationPolicy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cancellationPolicyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		/// <remarks></remarks>
		public TList<Others> GetByCancellationPolicyId(TransactionManager transactionManager, System.Int64? _cancellationPolicyId)
		{
			int count = -1;
			return GetByCancellationPolicyId(transactionManager, _cancellationPolicyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_CancelationPolicy key.
		///		FK_Others_CancelationPolicy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cancellationPolicyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCancellationPolicyId(TransactionManager transactionManager, System.Int64? _cancellationPolicyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCancellationPolicyId(transactionManager, _cancellationPolicyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_CancelationPolicy key.
		///		fkOthersCancelationPolicy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cancellationPolicyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCancellationPolicyId(System.Int64? _cancellationPolicyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCancellationPolicyId(null, _cancellationPolicyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_CancelationPolicy key.
		///		fkOthersCancelationPolicy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cancellationPolicyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCancellationPolicyId(System.Int64? _cancellationPolicyId, int start, int pageLength,out int count)
		{
			return GetByCancellationPolicyId(null, _cancellationPolicyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_CancelationPolicy key.
		///		FK_Others_CancelationPolicy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cancellationPolicyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public abstract TList<Others> GetByCancellationPolicyId(TransactionManager transactionManager, System.Int64? _cancellationPolicyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Country key.
		///		FK_Others_Country Description: 
		/// </summary>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCountryId(System.Int64? _countryId)
		{
			int count = -1;
			return GetByCountryId(_countryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Country key.
		///		FK_Others_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		/// <remarks></remarks>
		public TList<Others> GetByCountryId(TransactionManager transactionManager, System.Int64? _countryId)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Country key.
		///		FK_Others_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCountryId(TransactionManager transactionManager, System.Int64? _countryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Country key.
		///		fkOthersCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCountryId(System.Int64? _countryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryId(null, _countryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Country key.
		///		fkOthersCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCountryId(System.Int64? _countryId, int start, int pageLength,out int count)
		{
			return GetByCountryId(null, _countryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Country key.
		///		FK_Others_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public abstract TList<Others> GetByCountryId(TransactionManager transactionManager, System.Int64? _countryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Currency key.
		///		FK_Others_Currency Description: 
		/// </summary>
		/// <param name="_currencyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCurrencyId(System.Int64? _currencyId)
		{
			int count = -1;
			return GetByCurrencyId(_currencyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Currency key.
		///		FK_Others_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		/// <remarks></remarks>
		public TList<Others> GetByCurrencyId(TransactionManager transactionManager, System.Int64? _currencyId)
		{
			int count = -1;
			return GetByCurrencyId(transactionManager, _currencyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Currency key.
		///		FK_Others_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCurrencyId(TransactionManager transactionManager, System.Int64? _currencyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCurrencyId(transactionManager, _currencyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Currency key.
		///		fkOthersCurrency Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currencyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCurrencyId(System.Int64? _currencyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCurrencyId(null, _currencyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Currency key.
		///		fkOthersCurrency Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currencyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public TList<Others> GetByCurrencyId(System.Int64? _currencyId, int start, int pageLength,out int count)
		{
			return GetByCurrencyId(null, _currencyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Others_Currency key.
		///		FK_Others_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Others objects.</returns>
		public abstract TList<Others> GetByCurrencyId(TransactionManager transactionManager, System.Int64? _currencyId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Others Get(TransactionManager transactionManager, LMMR.Entities.OthersKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Others index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Others"/> class.</returns>
		public LMMR.Entities.Others GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Others index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Others"/> class.</returns>
		public LMMR.Entities.Others GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Others index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Others"/> class.</returns>
		public LMMR.Entities.Others GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Others index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Others"/> class.</returns>
		public LMMR.Entities.Others GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Others index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Others"/> class.</returns>
		public LMMR.Entities.Others GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Others index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Others"/> class.</returns>
		public abstract LMMR.Entities.Others GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Others&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Others&gt;"/></returns>
		public static TList<Others> Fill(IDataReader reader, TList<Others> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Others c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Others")
					.Append("|").Append((System.Int64)reader[((int)OthersColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Others>(
					key.ToString(), // EntityTrackingKey
					"Others",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Others();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)OthersColumn.Id - 1)];
					c.CountryId = (reader.IsDBNull(((int)OthersColumn.CountryId - 1)))?null:(System.Int64?)reader[((int)OthersColumn.CountryId - 1)];
					c.CancellationPolicyId = (reader.IsDBNull(((int)OthersColumn.CancellationPolicyId - 1)))?null:(System.Int64?)reader[((int)OthersColumn.CancellationPolicyId - 1)];
					c.MaxRequestBasket = (reader.IsDBNull(((int)OthersColumn.MaxRequestBasket - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MaxRequestBasket - 1)];
					c.CheckCommWait = (reader.IsDBNull(((int)OthersColumn.CheckCommWait - 1)))?null:(System.Int32?)reader[((int)OthersColumn.CheckCommWait - 1)];
					c.CurrencyId = (reader.IsDBNull(((int)OthersColumn.CurrencyId - 1)))?null:(System.Int64?)reader[((int)OthersColumn.CurrencyId - 1)];
					c.MinIntervalVariation = (reader.IsDBNull(((int)OthersColumn.MinIntervalVariation - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MinIntervalVariation - 1)];
					c.MaxIntervalVariation = (reader.IsDBNull(((int)OthersColumn.MaxIntervalVariation - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MaxIntervalVariation - 1)];
					c.MinConsiderSpl = (reader.IsDBNull(((int)OthersColumn.MinConsiderSpl - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MinConsiderSpl - 1)];
					c.RevenueModificationTrigger = (reader.IsDBNull(((int)OthersColumn.RevenueModificationTrigger - 1)))?null:(System.Int32?)reader[((int)OthersColumn.RevenueModificationTrigger - 1)];
					c.InvoiceComm = (reader.IsDBNull(((int)OthersColumn.InvoiceComm - 1)))?null:(System.Int32?)reader[((int)OthersColumn.InvoiceComm - 1)];
					c.AgencyComm = (reader.IsDBNull(((int)OthersColumn.AgencyComm - 1)))?null:(System.Int32?)reader[((int)OthersColumn.AgencyComm - 1)];
					c.SessionTimeout = (reader.IsDBNull(((int)OthersColumn.SessionTimeout - 1)))?null:(System.Int32?)reader[((int)OthersColumn.SessionTimeout - 1)];
					c.MinStockforHotel = (reader.IsDBNull(((int)OthersColumn.MinStockforHotel - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MinStockforHotel - 1)];
					c.CreationDate = (reader.IsDBNull(((int)OthersColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)OthersColumn.CreationDate - 1)];
					c.InvoiceVerify = (reader.IsDBNull(((int)OthersColumn.InvoiceVerify - 1)))?null:(System.Boolean?)reader[((int)OthersColumn.InvoiceVerify - 1)];
					c.ScrollHotelOfWeek = (reader.IsDBNull(((int)OthersColumn.ScrollHotelOfWeek - 1)))?null:(System.Int32?)reader[((int)OthersColumn.ScrollHotelOfWeek - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Others"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Others"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Others entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)OthersColumn.Id - 1)];
			entity.CountryId = (reader.IsDBNull(((int)OthersColumn.CountryId - 1)))?null:(System.Int64?)reader[((int)OthersColumn.CountryId - 1)];
			entity.CancellationPolicyId = (reader.IsDBNull(((int)OthersColumn.CancellationPolicyId - 1)))?null:(System.Int64?)reader[((int)OthersColumn.CancellationPolicyId - 1)];
			entity.MaxRequestBasket = (reader.IsDBNull(((int)OthersColumn.MaxRequestBasket - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MaxRequestBasket - 1)];
			entity.CheckCommWait = (reader.IsDBNull(((int)OthersColumn.CheckCommWait - 1)))?null:(System.Int32?)reader[((int)OthersColumn.CheckCommWait - 1)];
			entity.CurrencyId = (reader.IsDBNull(((int)OthersColumn.CurrencyId - 1)))?null:(System.Int64?)reader[((int)OthersColumn.CurrencyId - 1)];
			entity.MinIntervalVariation = (reader.IsDBNull(((int)OthersColumn.MinIntervalVariation - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MinIntervalVariation - 1)];
			entity.MaxIntervalVariation = (reader.IsDBNull(((int)OthersColumn.MaxIntervalVariation - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MaxIntervalVariation - 1)];
			entity.MinConsiderSpl = (reader.IsDBNull(((int)OthersColumn.MinConsiderSpl - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MinConsiderSpl - 1)];
			entity.RevenueModificationTrigger = (reader.IsDBNull(((int)OthersColumn.RevenueModificationTrigger - 1)))?null:(System.Int32?)reader[((int)OthersColumn.RevenueModificationTrigger - 1)];
			entity.InvoiceComm = (reader.IsDBNull(((int)OthersColumn.InvoiceComm - 1)))?null:(System.Int32?)reader[((int)OthersColumn.InvoiceComm - 1)];
			entity.AgencyComm = (reader.IsDBNull(((int)OthersColumn.AgencyComm - 1)))?null:(System.Int32?)reader[((int)OthersColumn.AgencyComm - 1)];
			entity.SessionTimeout = (reader.IsDBNull(((int)OthersColumn.SessionTimeout - 1)))?null:(System.Int32?)reader[((int)OthersColumn.SessionTimeout - 1)];
			entity.MinStockforHotel = (reader.IsDBNull(((int)OthersColumn.MinStockforHotel - 1)))?null:(System.Int32?)reader[((int)OthersColumn.MinStockforHotel - 1)];
			entity.CreationDate = (reader.IsDBNull(((int)OthersColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)OthersColumn.CreationDate - 1)];
			entity.InvoiceVerify = (reader.IsDBNull(((int)OthersColumn.InvoiceVerify - 1)))?null:(System.Boolean?)reader[((int)OthersColumn.InvoiceVerify - 1)];
			entity.ScrollHotelOfWeek = (reader.IsDBNull(((int)OthersColumn.ScrollHotelOfWeek - 1)))?null:(System.Int32?)reader[((int)OthersColumn.ScrollHotelOfWeek - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Others"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Others"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Others entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.CountryId = Convert.IsDBNull(dataRow["CountryID"]) ? null : (System.Int64?)dataRow["CountryID"];
			entity.CancellationPolicyId = Convert.IsDBNull(dataRow["CancellationPolicyID"]) ? null : (System.Int64?)dataRow["CancellationPolicyID"];
			entity.MaxRequestBasket = Convert.IsDBNull(dataRow["MaxRequestBasket"]) ? null : (System.Int32?)dataRow["MaxRequestBasket"];
			entity.CheckCommWait = Convert.IsDBNull(dataRow["CheckCommWait"]) ? null : (System.Int32?)dataRow["CheckCommWait"];
			entity.CurrencyId = Convert.IsDBNull(dataRow["CurrencyID"]) ? null : (System.Int64?)dataRow["CurrencyID"];
			entity.MinIntervalVariation = Convert.IsDBNull(dataRow["MinIntervalVariation"]) ? null : (System.Int32?)dataRow["MinIntervalVariation"];
			entity.MaxIntervalVariation = Convert.IsDBNull(dataRow["MaxIntervalVariation"]) ? null : (System.Int32?)dataRow["MaxIntervalVariation"];
			entity.MinConsiderSpl = Convert.IsDBNull(dataRow["MinConsiderSpl"]) ? null : (System.Int32?)dataRow["MinConsiderSpl"];
			entity.RevenueModificationTrigger = Convert.IsDBNull(dataRow["RevenueModificationTrigger"]) ? null : (System.Int32?)dataRow["RevenueModificationTrigger"];
			entity.InvoiceComm = Convert.IsDBNull(dataRow["InvoiceComm"]) ? null : (System.Int32?)dataRow["InvoiceComm"];
			entity.AgencyComm = Convert.IsDBNull(dataRow["AgencyComm"]) ? null : (System.Int32?)dataRow["AgencyComm"];
			entity.SessionTimeout = Convert.IsDBNull(dataRow["SessionTimeout"]) ? null : (System.Int32?)dataRow["SessionTimeout"];
			entity.MinStockforHotel = Convert.IsDBNull(dataRow["MinStockforHotel"]) ? null : (System.Int32?)dataRow["MinStockforHotel"];
			entity.CreationDate = Convert.IsDBNull(dataRow["CreationDate"]) ? null : (System.DateTime?)dataRow["CreationDate"];
			entity.InvoiceVerify = Convert.IsDBNull(dataRow["InvoiceVerify"]) ? null : (System.Boolean?)dataRow["InvoiceVerify"];
			entity.ScrollHotelOfWeek = Convert.IsDBNull(dataRow["ScrollHotelOfWeek"]) ? null : (System.Int32?)dataRow["ScrollHotelOfWeek"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Others"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Others Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Others entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CancellationPolicyIdSource	
			if (CanDeepLoad(entity, "CancelationPolicy|CancellationPolicyIdSource", deepLoadType, innerList) 
				&& entity.CancellationPolicyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CancellationPolicyId ?? (long)0);
				CancelationPolicy tmpEntity = EntityManager.LocateEntity<CancelationPolicy>(EntityLocator.ConstructKeyFromPkItems(typeof(CancelationPolicy), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CancellationPolicyIdSource = tmpEntity;
				else
					entity.CancellationPolicyIdSource = DataRepository.CancelationPolicyProvider.GetById(transactionManager, (entity.CancellationPolicyId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CancellationPolicyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CancellationPolicyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CancelationPolicyProvider.DeepLoad(transactionManager, entity.CancellationPolicyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CancellationPolicyIdSource

			#region CountryIdSource	
			if (CanDeepLoad(entity, "Country|CountryIdSource", deepLoadType, innerList) 
				&& entity.CountryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CountryId ?? (long)0);
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryIdSource = tmpEntity;
				else
					entity.CountryIdSource = DataRepository.CountryProvider.GetById(transactionManager, (entity.CountryId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryIdSource

			#region CurrencyIdSource	
			if (CanDeepLoad(entity, "Currency|CurrencyIdSource", deepLoadType, innerList) 
				&& entity.CurrencyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CurrencyId ?? (long)0);
				Currency tmpEntity = EntityManager.LocateEntity<Currency>(EntityLocator.ConstructKeyFromPkItems(typeof(Currency), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CurrencyIdSource = tmpEntity;
				else
					entity.CurrencyIdSource = DataRepository.CurrencyProvider.GetById(transactionManager, (entity.CurrencyId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CurrencyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CurrencyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CurrencyProvider.DeepLoad(transactionManager, entity.CurrencyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CurrencyIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Others object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Others instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Others Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Others entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CancellationPolicyIdSource
			if (CanDeepSave(entity, "CancelationPolicy|CancellationPolicyIdSource", deepSaveType, innerList) 
				&& entity.CancellationPolicyIdSource != null)
			{
				DataRepository.CancelationPolicyProvider.Save(transactionManager, entity.CancellationPolicyIdSource);
				entity.CancellationPolicyId = entity.CancellationPolicyIdSource.Id;
			}
			#endregion 
			
			#region CountryIdSource
			if (CanDeepSave(entity, "Country|CountryIdSource", deepSaveType, innerList) 
				&& entity.CountryIdSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryIdSource);
				entity.CountryId = entity.CountryIdSource.Id;
			}
			#endregion 
			
			#region CurrencyIdSource
			if (CanDeepSave(entity, "Currency|CurrencyIdSource", deepSaveType, innerList) 
				&& entity.CurrencyIdSource != null)
			{
				DataRepository.CurrencyProvider.Save(transactionManager, entity.CurrencyIdSource);
				entity.CurrencyId = entity.CurrencyIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region OthersChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Others</c>
	///</summary>
	public enum OthersChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CancelationPolicy</c> at CancellationPolicyIdSource
		///</summary>
		[ChildEntityType(typeof(CancelationPolicy))]
		CancelationPolicy,
			
		///<summary>
		/// Composite Property for <c>Country</c> at CountryIdSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
			
		///<summary>
		/// Composite Property for <c>Currency</c> at CurrencyIdSource
		///</summary>
		[ChildEntityType(typeof(Currency))]
		Currency,
		}
	
	#endregion OthersChildEntityTypes
	
	#region OthersFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;OthersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Others"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OthersFilterBuilder : SqlFilterBuilder<OthersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OthersFilterBuilder class.
		/// </summary>
		public OthersFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OthersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OthersFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OthersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OthersFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OthersFilterBuilder
	
	#region OthersParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;OthersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Others"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OthersParameterBuilder : ParameterizedSqlFilterBuilder<OthersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OthersParameterBuilder class.
		/// </summary>
		public OthersParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OthersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OthersParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OthersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OthersParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OthersParameterBuilder
	
	#region OthersSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;OthersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Others"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OthersSortBuilder : SqlSortBuilder<OthersColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OthersSqlSortBuilder class.
		/// </summary>
		public OthersSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OthersSortBuilder
	
} // end namespace
