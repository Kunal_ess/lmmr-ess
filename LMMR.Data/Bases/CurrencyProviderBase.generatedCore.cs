﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CurrencyProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CurrencyProviderBaseCore : EntityProviderBase<LMMR.Entities.Currency, LMMR.Entities.CurrencyKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.CurrencyKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Currency Get(TransactionManager transactionManager, LMMR.Entities.CurrencyKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Currency index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Currency"/> class.</returns>
		public LMMR.Entities.Currency GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Currency index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Currency"/> class.</returns>
		public LMMR.Entities.Currency GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Currency index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Currency"/> class.</returns>
		public LMMR.Entities.Currency GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Currency index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Currency"/> class.</returns>
		public LMMR.Entities.Currency GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Currency index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Currency"/> class.</returns>
		public LMMR.Entities.Currency GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Currency index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Currency"/> class.</returns>
		public abstract LMMR.Entities.Currency GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Currency&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Currency&gt;"/></returns>
		public static TList<Currency> Fill(IDataReader reader, TList<Currency> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Currency c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Currency")
					.Append("|").Append((System.Int64)reader[((int)CurrencyColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Currency>(
					key.ToString(), // EntityTrackingKey
					"Currency",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Currency();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)CurrencyColumn.Id - 1)];
					c.Currency = (reader.IsDBNull(((int)CurrencyColumn.Currency - 1)))?null:(System.String)reader[((int)CurrencyColumn.Currency - 1)];
					c.CurrencySignature = (reader.IsDBNull(((int)CurrencyColumn.CurrencySignature - 1)))?null:(System.String)reader[((int)CurrencyColumn.CurrencySignature - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Currency"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Currency"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Currency entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)CurrencyColumn.Id - 1)];
			entity.Currency = (reader.IsDBNull(((int)CurrencyColumn.Currency - 1)))?null:(System.String)reader[((int)CurrencyColumn.Currency - 1)];
			entity.CurrencySignature = (reader.IsDBNull(((int)CurrencyColumn.CurrencySignature - 1)))?null:(System.String)reader[((int)CurrencyColumn.CurrencySignature - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Currency"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Currency"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Currency entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.Currency = Convert.IsDBNull(dataRow["Currency"]) ? null : (System.String)dataRow["Currency"];
			entity.CurrencySignature = Convert.IsDBNull(dataRow["CurrencySignature"]) ? null : (System.String)dataRow["CurrencySignature"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Currency"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Currency Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Currency entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region OthersCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Others>|OthersCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OthersCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OthersCollection = DataRepository.OthersProvider.GetByCurrencyId(transactionManager, entity.Id);

				if (deep && entity.OthersCollection.Count > 0)
				{
					deepHandles.Add("OthersCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Others>) DataRepository.OthersProvider.DeepLoad,
						new object[] { transactionManager, entity.OthersCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CountryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Country>|CountryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CountryCollection = DataRepository.CountryProvider.GetByCurrencyId(transactionManager, entity.Id);

				if (deep && entity.CountryCollection.Count > 0)
				{
					deepHandles.Add("CountryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Country>) DataRepository.CountryProvider.DeepLoad,
						new object[] { transactionManager, entity.CountryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Booking>|BookingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookingCollection = DataRepository.BookingProvider.GetByCurrencyId(transactionManager, entity.Id);

				if (deep && entity.BookingCollection.Count > 0)
				{
					deepHandles.Add("BookingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Booking>) DataRepository.BookingProvider.DeepLoad,
						new object[] { transactionManager, entity.BookingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Currency object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Currency instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Currency Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Currency entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Others>
				if (CanDeepSave(entity.OthersCollection, "List<Others>|OthersCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Others child in entity.OthersCollection)
					{
						if(child.CurrencyIdSource != null)
						{
							child.CurrencyId = child.CurrencyIdSource.Id;
						}
						else
						{
							child.CurrencyId = entity.Id;
						}

					}

					if (entity.OthersCollection.Count > 0 || entity.OthersCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OthersProvider.Save(transactionManager, entity.OthersCollection);
						
						deepHandles.Add("OthersCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Others >) DataRepository.OthersProvider.DeepSave,
							new object[] { transactionManager, entity.OthersCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Country>
				if (CanDeepSave(entity.CountryCollection, "List<Country>|CountryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Country child in entity.CountryCollection)
					{
						if(child.CurrencyIdSource != null)
						{
							child.CurrencyId = child.CurrencyIdSource.Id;
						}
						else
						{
							child.CurrencyId = entity.Id;
						}

					}

					if (entity.CountryCollection.Count > 0 || entity.CountryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CountryProvider.Save(transactionManager, entity.CountryCollection);
						
						deepHandles.Add("CountryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Country >) DataRepository.CountryProvider.DeepSave,
							new object[] { transactionManager, entity.CountryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Booking>
				if (CanDeepSave(entity.BookingCollection, "List<Booking>|BookingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Booking child in entity.BookingCollection)
					{
						if(child.CurrencyIdSource != null)
						{
							child.CurrencyId = child.CurrencyIdSource.Id;
						}
						else
						{
							child.CurrencyId = entity.Id;
						}

					}

					if (entity.BookingCollection.Count > 0 || entity.BookingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookingProvider.Save(transactionManager, entity.BookingCollection);
						
						deepHandles.Add("BookingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Booking >) DataRepository.BookingProvider.DeepSave,
							new object[] { transactionManager, entity.BookingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CurrencyChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Currency</c>
	///</summary>
	public enum CurrencyChildEntityTypes
	{

		///<summary>
		/// Collection of <c>Currency</c> as OneToMany for OthersCollection
		///</summary>
		[ChildEntityType(typeof(TList<Others>))]
		OthersCollection,

		///<summary>
		/// Collection of <c>Currency</c> as OneToMany for CountryCollection
		///</summary>
		[ChildEntityType(typeof(TList<Country>))]
		CountryCollection,

		///<summary>
		/// Collection of <c>Currency</c> as OneToMany for BookingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Booking>))]
		BookingCollection,
	}
	
	#endregion CurrencyChildEntityTypes
	
	#region CurrencyFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CurrencyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Currency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyFilterBuilder : SqlFilterBuilder<CurrencyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyFilterBuilder class.
		/// </summary>
		public CurrencyFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyFilterBuilder
	
	#region CurrencyParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CurrencyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Currency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyParameterBuilder : ParameterizedSqlFilterBuilder<CurrencyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyParameterBuilder class.
		/// </summary>
		public CurrencyParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyParameterBuilder
	
	#region CurrencySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CurrencyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Currency"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CurrencySortBuilder : SqlSortBuilder<CurrencyColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencySqlSortBuilder class.
		/// </summary>
		public CurrencySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CurrencySortBuilder
	
} // end namespace
