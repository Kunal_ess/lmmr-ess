﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BedRoomDescTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BedRoomDescTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.BedRoomDescTrail, LMMR.Entities.BedRoomDescTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BedRoomDescTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Trail_BedRoomDesc key.
		///		FK_BedRoomDesc_Trail_BedRoomDesc Description: 
		/// </summary>
		/// <param name="_bedRoomDescId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDescTrail objects.</returns>
		public TList<BedRoomDescTrail> GetByBedRoomDescId(System.Int64? _bedRoomDescId)
		{
			int count = -1;
			return GetByBedRoomDescId(_bedRoomDescId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Trail_BedRoomDesc key.
		///		FK_BedRoomDesc_Trail_BedRoomDesc Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomDescId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDescTrail objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoomDescTrail> GetByBedRoomDescId(TransactionManager transactionManager, System.Int64? _bedRoomDescId)
		{
			int count = -1;
			return GetByBedRoomDescId(transactionManager, _bedRoomDescId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Trail_BedRoomDesc key.
		///		FK_BedRoomDesc_Trail_BedRoomDesc Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomDescId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDescTrail objects.</returns>
		public TList<BedRoomDescTrail> GetByBedRoomDescId(TransactionManager transactionManager, System.Int64? _bedRoomDescId, int start, int pageLength)
		{
			int count = -1;
			return GetByBedRoomDescId(transactionManager, _bedRoomDescId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Trail_BedRoomDesc key.
		///		fkBedRoomDescTrailBedRoomDesc Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomDescId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDescTrail objects.</returns>
		public TList<BedRoomDescTrail> GetByBedRoomDescId(System.Int64? _bedRoomDescId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBedRoomDescId(null, _bedRoomDescId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Trail_BedRoomDesc key.
		///		fkBedRoomDescTrailBedRoomDesc Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomDescId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDescTrail objects.</returns>
		public TList<BedRoomDescTrail> GetByBedRoomDescId(System.Int64? _bedRoomDescId, int start, int pageLength,out int count)
		{
			return GetByBedRoomDescId(null, _bedRoomDescId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Trail_BedRoomDesc key.
		///		FK_BedRoomDesc_Trail_BedRoomDesc Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomDescId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDescTrail objects.</returns>
		public abstract TList<BedRoomDescTrail> GetByBedRoomDescId(TransactionManager transactionManager, System.Int64? _bedRoomDescId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BedRoomDescTrail Get(TransactionManager transactionManager, LMMR.Entities.BedRoomDescTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BedRoomDesc_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDescTrail"/> class.</returns>
		public LMMR.Entities.BedRoomDescTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDescTrail"/> class.</returns>
		public LMMR.Entities.BedRoomDescTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDescTrail"/> class.</returns>
		public LMMR.Entities.BedRoomDescTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDescTrail"/> class.</returns>
		public LMMR.Entities.BedRoomDescTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDescTrail"/> class.</returns>
		public LMMR.Entities.BedRoomDescTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDescTrail"/> class.</returns>
		public abstract LMMR.Entities.BedRoomDescTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BedRoomDescTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BedRoomDescTrail&gt;"/></returns>
		public static TList<BedRoomDescTrail> Fill(IDataReader reader, TList<BedRoomDescTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BedRoomDescTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BedRoomDescTrail")
					.Append("|").Append((System.Int64)reader[((int)BedRoomDescTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BedRoomDescTrail>(
					key.ToString(), // EntityTrackingKey
					"BedRoomDescTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BedRoomDescTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BedRoomDescTrailColumn.Id - 1)];
					c.BedRoomDescId = (reader.IsDBNull(((int)BedRoomDescTrailColumn.BedRoomDescId - 1)))?null:(System.Int64?)reader[((int)BedRoomDescTrailColumn.BedRoomDescId - 1)];
					c.BedRoomId = (System.Int64)reader[((int)BedRoomDescTrailColumn.BedRoomId - 1)];
					c.LanguageId = (System.Int64)reader[((int)BedRoomDescTrailColumn.LanguageId - 1)];
					c.Description = (reader.IsDBNull(((int)BedRoomDescTrailColumn.Description - 1)))?null:(System.String)reader[((int)BedRoomDescTrailColumn.Description - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)BedRoomDescTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomDescTrailColumn.UpdatedBy - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)BedRoomDescTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)BedRoomDescTrailColumn.UpdateDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomDescTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomDescTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BedRoomDescTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BedRoomDescTrailColumn.Id - 1)];
			entity.BedRoomDescId = (reader.IsDBNull(((int)BedRoomDescTrailColumn.BedRoomDescId - 1)))?null:(System.Int64?)reader[((int)BedRoomDescTrailColumn.BedRoomDescId - 1)];
			entity.BedRoomId = (System.Int64)reader[((int)BedRoomDescTrailColumn.BedRoomId - 1)];
			entity.LanguageId = (System.Int64)reader[((int)BedRoomDescTrailColumn.LanguageId - 1)];
			entity.Description = (reader.IsDBNull(((int)BedRoomDescTrailColumn.Description - 1)))?null:(System.String)reader[((int)BedRoomDescTrailColumn.Description - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)BedRoomDescTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomDescTrailColumn.UpdatedBy - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)BedRoomDescTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)BedRoomDescTrailColumn.UpdateDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomDescTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomDescTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BedRoomDescTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BedRoomDescId = Convert.IsDBNull(dataRow["BedRoomDescId"]) ? null : (System.Int64?)dataRow["BedRoomDescId"];
			entity.BedRoomId = (System.Int64)dataRow["BedRoomId"];
			entity.LanguageId = (System.Int64)dataRow["LanguageId"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomDescTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomDescTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BedRoomDescTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BedRoomDescIdSource	
			if (CanDeepLoad(entity, "BedRoomDesc|BedRoomDescIdSource", deepLoadType, innerList) 
				&& entity.BedRoomDescIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.BedRoomDescId ?? (long)0);
				BedRoomDesc tmpEntity = EntityManager.LocateEntity<BedRoomDesc>(EntityLocator.ConstructKeyFromPkItems(typeof(BedRoomDesc), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BedRoomDescIdSource = tmpEntity;
				else
					entity.BedRoomDescIdSource = DataRepository.BedRoomDescProvider.GetById(transactionManager, (entity.BedRoomDescId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomDescIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BedRoomDescIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BedRoomDescProvider.DeepLoad(transactionManager, entity.BedRoomDescIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BedRoomDescIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BedRoomDescTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BedRoomDescTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomDescTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BedRoomDescTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BedRoomDescIdSource
			if (CanDeepSave(entity, "BedRoomDesc|BedRoomDescIdSource", deepSaveType, innerList) 
				&& entity.BedRoomDescIdSource != null)
			{
				DataRepository.BedRoomDescProvider.Save(transactionManager, entity.BedRoomDescIdSource);
				entity.BedRoomDescId = entity.BedRoomDescIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BedRoomDescTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BedRoomDescTrail</c>
	///</summary>
	public enum BedRoomDescTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BedRoomDesc</c> at BedRoomDescIdSource
		///</summary>
		[ChildEntityType(typeof(BedRoomDesc))]
		BedRoomDesc,
		}
	
	#endregion BedRoomDescTrailChildEntityTypes
	
	#region BedRoomDescTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BedRoomDescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomDescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomDescTrailFilterBuilder : SqlFilterBuilder<BedRoomDescTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailFilterBuilder class.
		/// </summary>
		public BedRoomDescTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomDescTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomDescTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomDescTrailFilterBuilder
	
	#region BedRoomDescTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BedRoomDescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomDescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomDescTrailParameterBuilder : ParameterizedSqlFilterBuilder<BedRoomDescTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailParameterBuilder class.
		/// </summary>
		public BedRoomDescTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomDescTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomDescTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomDescTrailParameterBuilder
	
	#region BedRoomDescTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BedRoomDescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomDescTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BedRoomDescTrailSortBuilder : SqlSortBuilder<BedRoomDescTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailSqlSortBuilder class.
		/// </summary>
		public BedRoomDescTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BedRoomDescTrailSortBuilder
	
} // end namespace
