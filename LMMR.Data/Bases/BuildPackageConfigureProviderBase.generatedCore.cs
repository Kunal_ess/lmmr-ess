﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BuildPackageConfigureProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BuildPackageConfigureProviderBaseCore : EntityProviderBase<LMMR.Entities.BuildPackageConfigure, LMMR.Entities.BuildPackageConfigureKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BuildPackageConfigureKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">This table is used for DDR packages. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">This table is used for DDR packages. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_BookedMeetingRoom key.
		///		FK_BuildPackageConfigure_BookedMeetingRoom Description: 
		/// </summary>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByBookedMeetingRoomId(System.Int64? _bookedMeetingRoomId)
		{
			int count = -1;
			return GetByBookedMeetingRoomId(_bookedMeetingRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_BookedMeetingRoom key.
		///		FK_BuildPackageConfigure_BookedMeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		/// <remarks></remarks>
		public TList<BuildPackageConfigure> GetByBookedMeetingRoomId(TransactionManager transactionManager, System.Int64? _bookedMeetingRoomId)
		{
			int count = -1;
			return GetByBookedMeetingRoomId(transactionManager, _bookedMeetingRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_BookedMeetingRoom key.
		///		FK_BuildPackageConfigure_BookedMeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByBookedMeetingRoomId(TransactionManager transactionManager, System.Int64? _bookedMeetingRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByBookedMeetingRoomId(transactionManager, _bookedMeetingRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_BookedMeetingRoom key.
		///		fkBuildPackageConfigureBookedMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByBookedMeetingRoomId(System.Int64? _bookedMeetingRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBookedMeetingRoomId(null, _bookedMeetingRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_BookedMeetingRoom key.
		///		fkBuildPackageConfigureBookedMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByBookedMeetingRoomId(System.Int64? _bookedMeetingRoomId, int start, int pageLength,out int count)
		{
			return GetByBookedMeetingRoomId(null, _bookedMeetingRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_BookedMeetingRoom key.
		///		FK_BuildPackageConfigure_BookedMeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookedMeetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public abstract TList<BuildPackageConfigure> GetByBookedMeetingRoomId(TransactionManager transactionManager, System.Int64? _bookedMeetingRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_Booking key.
		///		FK_BuildPackageConfigure_Booking Description: 
		/// </summary>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByBookingId(System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(_bookingId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_Booking key.
		///		FK_BuildPackageConfigure_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		/// <remarks></remarks>
		public TList<BuildPackageConfigure> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_Booking key.
		///		FK_BuildPackageConfigure_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_Booking key.
		///		fkBuildPackageConfigureBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByBookingId(System.Int64 _bookingId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBookingId(null, _bookingId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_Booking key.
		///		fkBuildPackageConfigureBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByBookingId(System.Int64 _bookingId, int start, int pageLength,out int count)
		{
			return GetByBookingId(null, _bookingId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_Booking key.
		///		FK_BuildPackageConfigure_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public abstract TList<BuildPackageConfigure> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_PackageMaster key.
		///		FK_BuildPackageConfigure_PackageMaster Description: 
		/// </summary>
		/// <param name="_packageItemId">This will be packageid</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByPackageItemId(System.Int64 _packageItemId)
		{
			int count = -1;
			return GetByPackageItemId(_packageItemId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_PackageMaster key.
		///		FK_BuildPackageConfigure_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageItemId">This will be packageid</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		/// <remarks></remarks>
		public TList<BuildPackageConfigure> GetByPackageItemId(TransactionManager transactionManager, System.Int64 _packageItemId)
		{
			int count = -1;
			return GetByPackageItemId(transactionManager, _packageItemId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_PackageMaster key.
		///		FK_BuildPackageConfigure_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageItemId">This will be packageid</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByPackageItemId(TransactionManager transactionManager, System.Int64 _packageItemId, int start, int pageLength)
		{
			int count = -1;
			return GetByPackageItemId(transactionManager, _packageItemId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_PackageMaster key.
		///		fkBuildPackageConfigurePackageMaster Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_packageItemId">This will be packageid</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByPackageItemId(System.Int64 _packageItemId, int start, int pageLength)
		{
			int count =  -1;
			return GetByPackageItemId(null, _packageItemId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_PackageMaster key.
		///		fkBuildPackageConfigurePackageMaster Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_packageItemId">This will be packageid</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public TList<BuildPackageConfigure> GetByPackageItemId(System.Int64 _packageItemId, int start, int pageLength,out int count)
		{
			return GetByPackageItemId(null, _packageItemId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BuildPackageConfigure_PackageMaster key.
		///		FK_BuildPackageConfigure_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageItemId">This will be packageid</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BuildPackageConfigure objects.</returns>
		public abstract TList<BuildPackageConfigure> GetByPackageItemId(TransactionManager transactionManager, System.Int64 _packageItemId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BuildPackageConfigure Get(TransactionManager transactionManager, LMMR.Entities.BuildPackageConfigureKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BuildYourMeeting index.
		/// </summary>
		/// <param name="_id">This table is used for DDR packages</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigure"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigure GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildYourMeeting index.
		/// </summary>
		/// <param name="_id">This table is used for DDR packages</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigure"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigure GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildYourMeeting index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">This table is used for DDR packages</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigure"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigure GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildYourMeeting index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">This table is used for DDR packages</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigure"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigure GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildYourMeeting index.
		/// </summary>
		/// <param name="_id">This table is used for DDR packages</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigure"/> class.</returns>
		public LMMR.Entities.BuildPackageConfigure GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BuildYourMeeting index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">This table is used for DDR packages</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BuildPackageConfigure"/> class.</returns>
		public abstract LMMR.Entities.BuildPackageConfigure GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BuildPackageConfigure&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BuildPackageConfigure&gt;"/></returns>
		public static TList<BuildPackageConfigure> Fill(IDataReader reader, TList<BuildPackageConfigure> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BuildPackageConfigure c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BuildPackageConfigure")
					.Append("|").Append((System.Int64)reader[((int)BuildPackageConfigureColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BuildPackageConfigure>(
					key.ToString(), // EntityTrackingKey
					"BuildPackageConfigure",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BuildPackageConfigure();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BuildPackageConfigureColumn.Id - 1)];
					c.BookingId = (System.Int64)reader[((int)BuildPackageConfigureColumn.BookingId - 1)];
					c.MeetingDay = (reader.IsDBNull(((int)BuildPackageConfigureColumn.MeetingDay - 1)))?null:(System.Int32?)reader[((int)BuildPackageConfigureColumn.MeetingDay - 1)];
					c.PackageItemId = (System.Int64)reader[((int)BuildPackageConfigureColumn.PackageItemId - 1)];
					c.NoOfPerson = (reader.IsDBNull(((int)BuildPackageConfigureColumn.NoOfPerson - 1)))?null:(System.Int64?)reader[((int)BuildPackageConfigureColumn.NoOfPerson - 1)];
					c.TotalPrice = (reader.IsDBNull(((int)BuildPackageConfigureColumn.TotalPrice - 1)))?null:(System.Decimal?)reader[((int)BuildPackageConfigureColumn.TotalPrice - 1)];
					c.BookedMeetingRoomId = (reader.IsDBNull(((int)BuildPackageConfigureColumn.BookedMeetingRoomId - 1)))?null:(System.Int64?)reader[((int)BuildPackageConfigureColumn.BookedMeetingRoomId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BuildPackageConfigure"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildPackageConfigure"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BuildPackageConfigure entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BuildPackageConfigureColumn.Id - 1)];
			entity.BookingId = (System.Int64)reader[((int)BuildPackageConfigureColumn.BookingId - 1)];
			entity.MeetingDay = (reader.IsDBNull(((int)BuildPackageConfigureColumn.MeetingDay - 1)))?null:(System.Int32?)reader[((int)BuildPackageConfigureColumn.MeetingDay - 1)];
			entity.PackageItemId = (System.Int64)reader[((int)BuildPackageConfigureColumn.PackageItemId - 1)];
			entity.NoOfPerson = (reader.IsDBNull(((int)BuildPackageConfigureColumn.NoOfPerson - 1)))?null:(System.Int64?)reader[((int)BuildPackageConfigureColumn.NoOfPerson - 1)];
			entity.TotalPrice = (reader.IsDBNull(((int)BuildPackageConfigureColumn.TotalPrice - 1)))?null:(System.Decimal?)reader[((int)BuildPackageConfigureColumn.TotalPrice - 1)];
			entity.BookedMeetingRoomId = (reader.IsDBNull(((int)BuildPackageConfigureColumn.BookedMeetingRoomId - 1)))?null:(System.Int64?)reader[((int)BuildPackageConfigureColumn.BookedMeetingRoomId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BuildPackageConfigure"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildPackageConfigure"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BuildPackageConfigure entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BookingId = (System.Int64)dataRow["BookingId"];
			entity.MeetingDay = Convert.IsDBNull(dataRow["MeetingDay"]) ? null : (System.Int32?)dataRow["MeetingDay"];
			entity.PackageItemId = (System.Int64)dataRow["PackageItemId"];
			entity.NoOfPerson = Convert.IsDBNull(dataRow["NoOfPerson"]) ? null : (System.Int64?)dataRow["NoOfPerson"];
			entity.TotalPrice = Convert.IsDBNull(dataRow["TotalPrice"]) ? null : (System.Decimal?)dataRow["TotalPrice"];
			entity.BookedMeetingRoomId = Convert.IsDBNull(dataRow["BookedMeetingRoomId"]) ? null : (System.Int64?)dataRow["BookedMeetingRoomId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BuildPackageConfigure"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BuildPackageConfigure Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BuildPackageConfigure entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BookedMeetingRoomIdSource	
			if (CanDeepLoad(entity, "BookedMeetingRoom|BookedMeetingRoomIdSource", deepLoadType, innerList) 
				&& entity.BookedMeetingRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.BookedMeetingRoomId ?? (long)0);
				BookedMeetingRoom tmpEntity = EntityManager.LocateEntity<BookedMeetingRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(BookedMeetingRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BookedMeetingRoomIdSource = tmpEntity;
				else
					entity.BookedMeetingRoomIdSource = DataRepository.BookedMeetingRoomProvider.GetById(transactionManager, (entity.BookedMeetingRoomId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookedMeetingRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BookedMeetingRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookedMeetingRoomProvider.DeepLoad(transactionManager, entity.BookedMeetingRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BookedMeetingRoomIdSource

			#region BookingIdSource	
			if (CanDeepLoad(entity, "Booking|BookingIdSource", deepLoadType, innerList) 
				&& entity.BookingIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BookingId;
				Booking tmpEntity = EntityManager.LocateEntity<Booking>(EntityLocator.ConstructKeyFromPkItems(typeof(Booking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BookingIdSource = tmpEntity;
				else
					entity.BookingIdSource = DataRepository.BookingProvider.GetById(transactionManager, entity.BookingId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BookingIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookingProvider.DeepLoad(transactionManager, entity.BookingIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BookingIdSource

			#region PackageItemIdSource	
			if (CanDeepLoad(entity, "PackageMaster|PackageItemIdSource", deepLoadType, innerList) 
				&& entity.PackageItemIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PackageItemId;
				PackageMaster tmpEntity = EntityManager.LocateEntity<PackageMaster>(EntityLocator.ConstructKeyFromPkItems(typeof(PackageMaster), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PackageItemIdSource = tmpEntity;
				else
					entity.PackageItemIdSource = DataRepository.PackageMasterProvider.GetById(transactionManager, entity.PackageItemId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageItemIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PackageItemIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PackageMasterProvider.DeepLoad(transactionManager, entity.PackageItemIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PackageItemIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region BuildPackageConfigureDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BuildPackageConfigureDesc>|BuildPackageConfigureDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildPackageConfigureDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BuildPackageConfigureDescCollection = DataRepository.BuildPackageConfigureDescProvider.GetByBuildPackageConfigId(transactionManager, entity.Id);

				if (deep && entity.BuildPackageConfigureDescCollection.Count > 0)
				{
					deepHandles.Add("BuildPackageConfigureDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BuildPackageConfigureDesc>) DataRepository.BuildPackageConfigureDescProvider.DeepLoad,
						new object[] { transactionManager, entity.BuildPackageConfigureDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BuildPackageConfigure object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BuildPackageConfigure instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BuildPackageConfigure Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BuildPackageConfigure entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BookedMeetingRoomIdSource
			if (CanDeepSave(entity, "BookedMeetingRoom|BookedMeetingRoomIdSource", deepSaveType, innerList) 
				&& entity.BookedMeetingRoomIdSource != null)
			{
				DataRepository.BookedMeetingRoomProvider.Save(transactionManager, entity.BookedMeetingRoomIdSource);
				entity.BookedMeetingRoomId = entity.BookedMeetingRoomIdSource.Id;
			}
			#endregion 
			
			#region BookingIdSource
			if (CanDeepSave(entity, "Booking|BookingIdSource", deepSaveType, innerList) 
				&& entity.BookingIdSource != null)
			{
				DataRepository.BookingProvider.Save(transactionManager, entity.BookingIdSource);
				entity.BookingId = entity.BookingIdSource.Id;
			}
			#endregion 
			
			#region PackageItemIdSource
			if (CanDeepSave(entity, "PackageMaster|PackageItemIdSource", deepSaveType, innerList) 
				&& entity.PackageItemIdSource != null)
			{
				DataRepository.PackageMasterProvider.Save(transactionManager, entity.PackageItemIdSource);
				entity.PackageItemId = entity.PackageItemIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<BuildPackageConfigureDesc>
				if (CanDeepSave(entity.BuildPackageConfigureDescCollection, "List<BuildPackageConfigureDesc>|BuildPackageConfigureDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BuildPackageConfigureDesc child in entity.BuildPackageConfigureDescCollection)
					{
						if(child.BuildPackageConfigIdSource != null)
						{
							child.BuildPackageConfigId = child.BuildPackageConfigIdSource.Id;
						}
						else
						{
							child.BuildPackageConfigId = entity.Id;
						}

					}

					if (entity.BuildPackageConfigureDescCollection.Count > 0 || entity.BuildPackageConfigureDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BuildPackageConfigureDescProvider.Save(transactionManager, entity.BuildPackageConfigureDescCollection);
						
						deepHandles.Add("BuildPackageConfigureDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BuildPackageConfigureDesc >) DataRepository.BuildPackageConfigureDescProvider.DeepSave,
							new object[] { transactionManager, entity.BuildPackageConfigureDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BuildPackageConfigureChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BuildPackageConfigure</c>
	///</summary>
	public enum BuildPackageConfigureChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BookedMeetingRoom</c> at BookedMeetingRoomIdSource
		///</summary>
		[ChildEntityType(typeof(BookedMeetingRoom))]
		BookedMeetingRoom,
			
		///<summary>
		/// Composite Property for <c>Booking</c> at BookingIdSource
		///</summary>
		[ChildEntityType(typeof(Booking))]
		Booking,
			
		///<summary>
		/// Composite Property for <c>PackageMaster</c> at PackageItemIdSource
		///</summary>
		[ChildEntityType(typeof(PackageMaster))]
		PackageMaster,
	
		///<summary>
		/// Collection of <c>BuildPackageConfigure</c> as OneToMany for BuildPackageConfigureDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<BuildPackageConfigureDesc>))]
		BuildPackageConfigureDescCollection,
	}
	
	#endregion BuildPackageConfigureChildEntityTypes
	
	#region BuildPackageConfigureFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BuildPackageConfigureColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigure"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildPackageConfigureFilterBuilder : SqlFilterBuilder<BuildPackageConfigureColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureFilterBuilder class.
		/// </summary>
		public BuildPackageConfigureFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildPackageConfigureFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildPackageConfigureFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildPackageConfigureFilterBuilder
	
	#region BuildPackageConfigureParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BuildPackageConfigureColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigure"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildPackageConfigureParameterBuilder : ParameterizedSqlFilterBuilder<BuildPackageConfigureColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureParameterBuilder class.
		/// </summary>
		public BuildPackageConfigureParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildPackageConfigureParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildPackageConfigureParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildPackageConfigureParameterBuilder
	
	#region BuildPackageConfigureSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BuildPackageConfigureColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigure"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BuildPackageConfigureSortBuilder : SqlSortBuilder<BuildPackageConfigureColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureSqlSortBuilder class.
		/// </summary>
		public BuildPackageConfigureSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BuildPackageConfigureSortBuilder
	
} // end namespace
