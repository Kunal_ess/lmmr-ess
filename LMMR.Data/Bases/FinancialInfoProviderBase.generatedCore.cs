﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FinancialInfoProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FinancialInfoProviderBaseCore : EntityProviderBase<LMMR.Entities.FinancialInfo, LMMR.Entities.FinancialInfoKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.FinancialInfoKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__Financial__UserI__4FBCC72F key.
		///		FK__Financial__UserI__4FBCC72F Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		public TList<FinancialInfo> GetByUserId(System.Int64? _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__Financial__UserI__4FBCC72F key.
		///		FK__Financial__UserI__4FBCC72F Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		/// <remarks></remarks>
		public TList<FinancialInfo> GetByUserId(TransactionManager transactionManager, System.Int64? _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__Financial__UserI__4FBCC72F key.
		///		FK__Financial__UserI__4FBCC72F Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		public TList<FinancialInfo> GetByUserId(TransactionManager transactionManager, System.Int64? _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__Financial__UserI__4FBCC72F key.
		///		fkFinancialUseri4Fbcc72f Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		public TList<FinancialInfo> GetByUserId(System.Int64? _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__Financial__UserI__4FBCC72F key.
		///		fkFinancialUseri4Fbcc72f Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		public TList<FinancialInfo> GetByUserId(System.Int64? _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__Financial__UserI__4FBCC72F key.
		///		FK__Financial__UserI__4FBCC72F Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		public abstract TList<FinancialInfo> GetByUserId(TransactionManager transactionManager, System.Int64? _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.FinancialInfo Get(TransactionManager transactionManager, LMMR.Entities.FinancialInfoKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FinancialInfo index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FinancialInfo"/> class.</returns>
		public LMMR.Entities.FinancialInfo GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FinancialInfo index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FinancialInfo"/> class.</returns>
		public LMMR.Entities.FinancialInfo GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FinancialInfo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FinancialInfo"/> class.</returns>
		public LMMR.Entities.FinancialInfo GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FinancialInfo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FinancialInfo"/> class.</returns>
		public LMMR.Entities.FinancialInfo GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FinancialInfo index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FinancialInfo"/> class.</returns>
		public LMMR.Entities.FinancialInfo GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FinancialInfo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FinancialInfo"/> class.</returns>
		public abstract LMMR.Entities.FinancialInfo GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FinancialInfo&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FinancialInfo&gt;"/></returns>
		public static TList<FinancialInfo> Fill(IDataReader reader, TList<FinancialInfo> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.FinancialInfo c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FinancialInfo")
					.Append("|").Append((System.Int64)reader[((int)FinancialInfoColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FinancialInfo>(
					key.ToString(), // EntityTrackingKey
					"FinancialInfo",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.FinancialInfo();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)FinancialInfoColumn.Id - 1)];
					c.Name = (reader.IsDBNull(((int)FinancialInfoColumn.Name - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.Name - 1)];
					c.Department = (reader.IsDBNull(((int)FinancialInfoColumn.Department - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.Department - 1)];
					c.CommunicationEmail = (reader.IsDBNull(((int)FinancialInfoColumn.CommunicationEmail - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.CommunicationEmail - 1)];
					c.CountryId = (reader.IsDBNull(((int)FinancialInfoColumn.CountryId - 1)))?null:(System.Int32?)reader[((int)FinancialInfoColumn.CountryId - 1)];
					c.CityId = (reader.IsDBNull(((int)FinancialInfoColumn.CityId - 1)))?null:(System.Int32?)reader[((int)FinancialInfoColumn.CityId - 1)];
					c.Address = (reader.IsDBNull(((int)FinancialInfoColumn.Address - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.Address - 1)];
					c.Phone = (reader.IsDBNull(((int)FinancialInfoColumn.Phone - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.Phone - 1)];
					c.CountryCode = (reader.IsDBNull(((int)FinancialInfoColumn.CountryCode - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.CountryCode - 1)];
					c.PostalCode = (reader.IsDBNull(((int)FinancialInfoColumn.PostalCode - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.PostalCode - 1)];
					c.UserId = (reader.IsDBNull(((int)FinancialInfoColumn.UserId - 1)))?null:(System.Int64?)reader[((int)FinancialInfoColumn.UserId - 1)];
					c.VatValue = (reader.IsDBNull(((int)FinancialInfoColumn.VatValue - 1)))?null:(System.Int64?)reader[((int)FinancialInfoColumn.VatValue - 1)];
					c.IsDeleted = (System.Boolean)reader[((int)FinancialInfoColumn.IsDeleted - 1)];
					c.DeletedDate = (reader.IsDBNull(((int)FinancialInfoColumn.DeletedDate - 1)))?null:(System.DateTime?)reader[((int)FinancialInfoColumn.DeletedDate - 1)];
					c.VatNo = (reader.IsDBNull(((int)FinancialInfoColumn.VatNo - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.VatNo - 1)];
					c.CityName = (reader.IsDBNull(((int)FinancialInfoColumn.CityName - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.CityName - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.FinancialInfo"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FinancialInfo"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.FinancialInfo entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)FinancialInfoColumn.Id - 1)];
			entity.Name = (reader.IsDBNull(((int)FinancialInfoColumn.Name - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.Name - 1)];
			entity.Department = (reader.IsDBNull(((int)FinancialInfoColumn.Department - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.Department - 1)];
			entity.CommunicationEmail = (reader.IsDBNull(((int)FinancialInfoColumn.CommunicationEmail - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.CommunicationEmail - 1)];
			entity.CountryId = (reader.IsDBNull(((int)FinancialInfoColumn.CountryId - 1)))?null:(System.Int32?)reader[((int)FinancialInfoColumn.CountryId - 1)];
			entity.CityId = (reader.IsDBNull(((int)FinancialInfoColumn.CityId - 1)))?null:(System.Int32?)reader[((int)FinancialInfoColumn.CityId - 1)];
			entity.Address = (reader.IsDBNull(((int)FinancialInfoColumn.Address - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.Address - 1)];
			entity.Phone = (reader.IsDBNull(((int)FinancialInfoColumn.Phone - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.Phone - 1)];
			entity.CountryCode = (reader.IsDBNull(((int)FinancialInfoColumn.CountryCode - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.CountryCode - 1)];
			entity.PostalCode = (reader.IsDBNull(((int)FinancialInfoColumn.PostalCode - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.PostalCode - 1)];
			entity.UserId = (reader.IsDBNull(((int)FinancialInfoColumn.UserId - 1)))?null:(System.Int64?)reader[((int)FinancialInfoColumn.UserId - 1)];
			entity.VatValue = (reader.IsDBNull(((int)FinancialInfoColumn.VatValue - 1)))?null:(System.Int64?)reader[((int)FinancialInfoColumn.VatValue - 1)];
			entity.IsDeleted = (System.Boolean)reader[((int)FinancialInfoColumn.IsDeleted - 1)];
			entity.DeletedDate = (reader.IsDBNull(((int)FinancialInfoColumn.DeletedDate - 1)))?null:(System.DateTime?)reader[((int)FinancialInfoColumn.DeletedDate - 1)];
			entity.VatNo = (reader.IsDBNull(((int)FinancialInfoColumn.VatNo - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.VatNo - 1)];
			entity.CityName = (reader.IsDBNull(((int)FinancialInfoColumn.CityName - 1)))?null:(System.String)reader[((int)FinancialInfoColumn.CityName - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.FinancialInfo"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FinancialInfo"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.FinancialInfo entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Department = Convert.IsDBNull(dataRow["Department"]) ? null : (System.String)dataRow["Department"];
			entity.CommunicationEmail = Convert.IsDBNull(dataRow["CommunicationEmail"]) ? null : (System.String)dataRow["CommunicationEmail"];
			entity.CountryId = Convert.IsDBNull(dataRow["CountryId"]) ? null : (System.Int32?)dataRow["CountryId"];
			entity.CityId = Convert.IsDBNull(dataRow["CityId"]) ? null : (System.Int32?)dataRow["CityId"];
			entity.Address = Convert.IsDBNull(dataRow["Address"]) ? null : (System.String)dataRow["Address"];
			entity.Phone = Convert.IsDBNull(dataRow["Phone"]) ? null : (System.String)dataRow["Phone"];
			entity.CountryCode = Convert.IsDBNull(dataRow["CountryCode"]) ? null : (System.String)dataRow["CountryCode"];
			entity.PostalCode = Convert.IsDBNull(dataRow["PostalCode"]) ? null : (System.String)dataRow["PostalCode"];
			entity.UserId = Convert.IsDBNull(dataRow["UserID"]) ? null : (System.Int64?)dataRow["UserID"];
			entity.VatValue = Convert.IsDBNull(dataRow["VatValue"]) ? null : (System.Int64?)dataRow["VatValue"];
			entity.IsDeleted = (System.Boolean)dataRow["IsDeleted"];
			entity.DeletedDate = Convert.IsDBNull(dataRow["DeletedDate"]) ? null : (System.DateTime?)dataRow["DeletedDate"];
			entity.VatNo = Convert.IsDBNull(dataRow["VatNo"]) ? null : (System.String)dataRow["VatNo"];
			entity.CityName = Convert.IsDBNull(dataRow["CityName"]) ? null : (System.String)dataRow["CityName"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FinancialInfo"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.FinancialInfo Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.FinancialInfo entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UserId ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UserId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.FinancialInfo object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.FinancialInfo instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.FinancialInfo Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.FinancialInfo entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FinancialInfoChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.FinancialInfo</c>
	///</summary>
	public enum FinancialInfoChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at UserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion FinancialInfoChildEntityTypes
	
	#region FinancialInfoFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FinancialInfoColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FinancialInfo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FinancialInfoFilterBuilder : SqlFilterBuilder<FinancialInfoColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FinancialInfoFilterBuilder class.
		/// </summary>
		public FinancialInfoFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FinancialInfoFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FinancialInfoFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FinancialInfoFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FinancialInfoFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FinancialInfoFilterBuilder
	
	#region FinancialInfoParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FinancialInfoColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FinancialInfo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FinancialInfoParameterBuilder : ParameterizedSqlFilterBuilder<FinancialInfoColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FinancialInfoParameterBuilder class.
		/// </summary>
		public FinancialInfoParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FinancialInfoParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FinancialInfoParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FinancialInfoParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FinancialInfoParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FinancialInfoParameterBuilder
	
	#region FinancialInfoSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FinancialInfoColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FinancialInfo"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FinancialInfoSortBuilder : SqlSortBuilder<FinancialInfoColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FinancialInfoSqlSortBuilder class.
		/// </summary>
		public FinancialInfoSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FinancialInfoSortBuilder
	
} // end namespace
