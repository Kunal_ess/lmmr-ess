﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewbookingrequestProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewbookingrequestProviderBaseCore : EntityViewProviderBase<Viewbookingrequest>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;Viewbookingrequest&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;Viewbookingrequest&gt;"/></returns>
		protected static VList&lt;Viewbookingrequest&gt; Fill(DataSet dataSet, VList<Viewbookingrequest> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<Viewbookingrequest>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;Viewbookingrequest&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<Viewbookingrequest>"/></returns>
		protected static VList&lt;Viewbookingrequest&gt; Fill(DataTable dataTable, VList<Viewbookingrequest> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					Viewbookingrequest c = new Viewbookingrequest();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.Usertype = (Convert.IsDBNull(row["Usertype"]))?string.Empty:(System.String)row["Usertype"];
					c.ChannelBy = (Convert.IsDBNull(row["ChannelBy"]))?string.Empty:(System.String)row["ChannelBy"];
					c.ChannelId = (Convert.IsDBNull(row["ChannelID"]))?string.Empty:(System.String)row["ChannelID"];
					c.TypeUser = (Convert.IsDBNull(row["TypeUser"]))?string.Empty:(System.String)row["TypeUser"];
					c.Contact = (Convert.IsDBNull(row["Contact"]))?string.Empty:(System.String)row["Contact"];
					c.FinalTotalPrice = (Convert.IsDBNull(row["FinalTotalPrice"]))?0.0m:(System.Decimal?)row["FinalTotalPrice"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.MainMeetingRoomId = (Convert.IsDBNull(row["MainMeetingRoomId"]))?(long)0:(System.Int64)row["MainMeetingRoomId"];
					c.BookingDate = (Convert.IsDBNull(row["BookingDate"]))?DateTime.MinValue:(System.DateTime?)row["BookingDate"];
					c.ArrivalDate = (Convert.IsDBNull(row["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)row["ArrivalDate"];
					c.DepartureDate = (Convert.IsDBNull(row["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)row["DepartureDate"];
					c.Duration = (Convert.IsDBNull(row["Duration"]))?(long)0:(System.Int64?)row["Duration"];
					c.CurrencyId = (Convert.IsDBNull(row["CurrencyID"]))?(long)0:(System.Int64?)row["CurrencyID"];
					c.LastName = (Convert.IsDBNull(row["LastName"]))?string.Empty:(System.String)row["LastName"];
					c.EmailId = (Convert.IsDBNull(row["EmailId"]))?string.Empty:(System.String)row["EmailId"];
					c.RequestStatus = (Convert.IsDBNull(row["RequestStatus"]))?(int)0:(System.Int32?)row["RequestStatus"];
					c.IsUserBookingProcessDone = (Convert.IsDBNull(row["IsUserBookingProcessDone"]))?false:(System.Boolean)row["IsUserBookingProcessDone"];
					c.RevenueAmount = (Convert.IsDBNull(row["RevenueAmount"]))?0.0m:(System.Decimal?)row["RevenueAmount"];
					c.RevenueReason = (Convert.IsDBNull(row["RevenueReason"]))?string.Empty:(System.String)row["RevenueReason"];
					c.ComissionSubmitDate = (Convert.IsDBNull(row["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)row["ComissionSubmitDate"];
					c.AgencyUserId = (Convert.IsDBNull(row["AgencyUserId"]))?(long)0:(System.Int64?)row["AgencyUserId"];
					c.AgencyClientId = (Convert.IsDBNull(row["AgencyClientId"]))?(long)0:(System.Int64?)row["AgencyClientId"];
					c.IsBookedByAgencyUser = (Convert.IsDBNull(row["IsBookedByAgencyUser"]))?false:(System.Boolean?)row["IsBookedByAgencyUser"];
					c.CreatorId = (Convert.IsDBNull(row["CreatorId"]))?(long)0:(System.Int64)row["CreatorId"];
					c.IsPackageSelected = (Convert.IsDBNull(row["IsPackageSelected"]))?false:(System.Boolean)row["IsPackageSelected"];
					c.Accomodation = (Convert.IsDBNull(row["Accomodation"]))?(int)0:(System.Int32?)row["Accomodation"];
					c.IsBedroom = (Convert.IsDBNull(row["IsBedroom"]))?false:(System.Boolean)row["IsBedroom"];
					c.SpecialRequest = (Convert.IsDBNull(row["SpecialRequest"]))?string.Empty:(System.String)row["SpecialRequest"];
					c.IsCancled = (Convert.IsDBNull(row["IsCancled"]))?false:(System.Boolean?)row["IsCancled"];
					c.IsComissionDone = (Convert.IsDBNull(row["IsComissionDone"]))?false:(System.Boolean?)row["IsComissionDone"];
					c.BookType = (Convert.IsDBNull(row["BookType"]))?(int)0:(System.Int32?)row["BookType"];
					c.FrozenDate = (Convert.IsDBNull(row["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)row["FrozenDate"];
					c.IsFrozen = (Convert.IsDBNull(row["IsFrozen"]))?false:(System.Boolean)row["IsFrozen"];
					c.ConfirmRevenueAmount = (Convert.IsDBNull(row["ConfirmRevenueAmount"]))?0.0m:(System.Decimal?)row["ConfirmRevenueAmount"];
					c.InvoiceId = (Convert.IsDBNull(row["InvoiceId"]))?(long)0:(System.Int64?)row["InvoiceId"];
					c.CountryId = (Convert.IsDBNull(row["CountryId"]))?(long)0:(System.Int64)row["CountryId"];
					c.CityId = (Convert.IsDBNull(row["CityId"]))?(long)0:(System.Int64)row["CityId"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.SupportingDocNetto = (Convert.IsDBNull(row["SupportingDocNetto"]))?string.Empty:(System.String)row["SupportingDocNetto"];
					c.RequestCollectionId = (Convert.IsDBNull(row["RequestCollectionID"]))?string.Empty:(System.String)row["RequestCollectionID"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;Viewbookingrequest&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;Viewbookingrequest&gt;"/></returns>
		protected VList<Viewbookingrequest> Fill(IDataReader reader, VList<Viewbookingrequest> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					Viewbookingrequest entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<Viewbookingrequest>("Viewbookingrequest",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new Viewbookingrequest();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewbookingrequestColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.Name = (reader.IsDBNull(((int)ViewbookingrequestColumn.Name)))?null:(System.String)reader[((int)ViewbookingrequestColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.Usertype = (System.String)reader[((int)ViewbookingrequestColumn.Usertype)];
					//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?string.Empty:(System.String)reader["Usertype"];
					entity.ChannelBy = (reader.IsDBNull(((int)ViewbookingrequestColumn.ChannelBy)))?null:(System.String)reader[((int)ViewbookingrequestColumn.ChannelBy)];
					//entity.ChannelBy = (Convert.IsDBNull(reader["ChannelBy"]))?string.Empty:(System.String)reader["ChannelBy"];
					entity.ChannelId = (reader.IsDBNull(((int)ViewbookingrequestColumn.ChannelId)))?null:(System.String)reader[((int)ViewbookingrequestColumn.ChannelId)];
					//entity.ChannelId = (Convert.IsDBNull(reader["ChannelID"]))?string.Empty:(System.String)reader["ChannelID"];
					entity.TypeUser = (System.String)reader[((int)ViewbookingrequestColumn.TypeUser)];
					//entity.TypeUser = (Convert.IsDBNull(reader["TypeUser"]))?string.Empty:(System.String)reader["TypeUser"];
					entity.Contact = (reader.IsDBNull(((int)ViewbookingrequestColumn.Contact)))?null:(System.String)reader[((int)ViewbookingrequestColumn.Contact)];
					//entity.Contact = (Convert.IsDBNull(reader["Contact"]))?string.Empty:(System.String)reader["Contact"];
					entity.FinalTotalPrice = (reader.IsDBNull(((int)ViewbookingrequestColumn.FinalTotalPrice)))?null:(System.Decimal?)reader[((int)ViewbookingrequestColumn.FinalTotalPrice)];
					//entity.FinalTotalPrice = (Convert.IsDBNull(reader["FinalTotalPrice"]))?0.0m:(System.Decimal?)reader["FinalTotalPrice"];
					entity.HotelId = (System.Int64)reader[((int)ViewbookingrequestColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.MainMeetingRoomId = (System.Int64)reader[((int)ViewbookingrequestColumn.MainMeetingRoomId)];
					//entity.MainMeetingRoomId = (Convert.IsDBNull(reader["MainMeetingRoomId"]))?(long)0:(System.Int64)reader["MainMeetingRoomId"];
					entity.BookingDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.BookingDate)];
					//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
					entity.ArrivalDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.ArrivalDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.ArrivalDate)];
					//entity.ArrivalDate = (Convert.IsDBNull(reader["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)reader["ArrivalDate"];
					entity.DepartureDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.DepartureDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.DepartureDate)];
					//entity.DepartureDate = (Convert.IsDBNull(reader["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)reader["DepartureDate"];
					entity.Duration = (reader.IsDBNull(((int)ViewbookingrequestColumn.Duration)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.Duration)];
					//entity.Duration = (Convert.IsDBNull(reader["Duration"]))?(long)0:(System.Int64?)reader["Duration"];
					entity.CurrencyId = (reader.IsDBNull(((int)ViewbookingrequestColumn.CurrencyId)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.CurrencyId)];
					//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyID"]))?(long)0:(System.Int64?)reader["CurrencyID"];
					entity.LastName = (reader.IsDBNull(((int)ViewbookingrequestColumn.LastName)))?null:(System.String)reader[((int)ViewbookingrequestColumn.LastName)];
					//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
					entity.EmailId = (reader.IsDBNull(((int)ViewbookingrequestColumn.EmailId)))?null:(System.String)reader[((int)ViewbookingrequestColumn.EmailId)];
					//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
					entity.RequestStatus = (reader.IsDBNull(((int)ViewbookingrequestColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewbookingrequestColumn.RequestStatus)];
					//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
					entity.IsUserBookingProcessDone = (System.Boolean)reader[((int)ViewbookingrequestColumn.IsUserBookingProcessDone)];
					//entity.IsUserBookingProcessDone = (Convert.IsDBNull(reader["IsUserBookingProcessDone"]))?false:(System.Boolean)reader["IsUserBookingProcessDone"];
					entity.RevenueAmount = (reader.IsDBNull(((int)ViewbookingrequestColumn.RevenueAmount)))?null:(System.Decimal?)reader[((int)ViewbookingrequestColumn.RevenueAmount)];
					//entity.RevenueAmount = (Convert.IsDBNull(reader["RevenueAmount"]))?0.0m:(System.Decimal?)reader["RevenueAmount"];
					entity.RevenueReason = (reader.IsDBNull(((int)ViewbookingrequestColumn.RevenueReason)))?null:(System.String)reader[((int)ViewbookingrequestColumn.RevenueReason)];
					//entity.RevenueReason = (Convert.IsDBNull(reader["RevenueReason"]))?string.Empty:(System.String)reader["RevenueReason"];
					entity.ComissionSubmitDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.ComissionSubmitDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.ComissionSubmitDate)];
					//entity.ComissionSubmitDate = (Convert.IsDBNull(reader["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)reader["ComissionSubmitDate"];
					entity.AgencyUserId = (reader.IsDBNull(((int)ViewbookingrequestColumn.AgencyUserId)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.AgencyUserId)];
					//entity.AgencyUserId = (Convert.IsDBNull(reader["AgencyUserId"]))?(long)0:(System.Int64?)reader["AgencyUserId"];
					entity.AgencyClientId = (reader.IsDBNull(((int)ViewbookingrequestColumn.AgencyClientId)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.AgencyClientId)];
					//entity.AgencyClientId = (Convert.IsDBNull(reader["AgencyClientId"]))?(long)0:(System.Int64?)reader["AgencyClientId"];
					entity.IsBookedByAgencyUser = (reader.IsDBNull(((int)ViewbookingrequestColumn.IsBookedByAgencyUser)))?null:(System.Boolean?)reader[((int)ViewbookingrequestColumn.IsBookedByAgencyUser)];
					//entity.IsBookedByAgencyUser = (Convert.IsDBNull(reader["IsBookedByAgencyUser"]))?false:(System.Boolean?)reader["IsBookedByAgencyUser"];
					entity.CreatorId = (System.Int64)reader[((int)ViewbookingrequestColumn.CreatorId)];
					//entity.CreatorId = (Convert.IsDBNull(reader["CreatorId"]))?(long)0:(System.Int64)reader["CreatorId"];
					entity.IsPackageSelected = (System.Boolean)reader[((int)ViewbookingrequestColumn.IsPackageSelected)];
					//entity.IsPackageSelected = (Convert.IsDBNull(reader["IsPackageSelected"]))?false:(System.Boolean)reader["IsPackageSelected"];
					entity.Accomodation = (reader.IsDBNull(((int)ViewbookingrequestColumn.Accomodation)))?null:(System.Int32?)reader[((int)ViewbookingrequestColumn.Accomodation)];
					//entity.Accomodation = (Convert.IsDBNull(reader["Accomodation"]))?(int)0:(System.Int32?)reader["Accomodation"];
					entity.IsBedroom = (System.Boolean)reader[((int)ViewbookingrequestColumn.IsBedroom)];
					//entity.IsBedroom = (Convert.IsDBNull(reader["IsBedroom"]))?false:(System.Boolean)reader["IsBedroom"];
					entity.SpecialRequest = (reader.IsDBNull(((int)ViewbookingrequestColumn.SpecialRequest)))?null:(System.String)reader[((int)ViewbookingrequestColumn.SpecialRequest)];
					//entity.SpecialRequest = (Convert.IsDBNull(reader["SpecialRequest"]))?string.Empty:(System.String)reader["SpecialRequest"];
					entity.IsCancled = (reader.IsDBNull(((int)ViewbookingrequestColumn.IsCancled)))?null:(System.Boolean?)reader[((int)ViewbookingrequestColumn.IsCancled)];
					//entity.IsCancled = (Convert.IsDBNull(reader["IsCancled"]))?false:(System.Boolean?)reader["IsCancled"];
					entity.IsComissionDone = (reader.IsDBNull(((int)ViewbookingrequestColumn.IsComissionDone)))?null:(System.Boolean?)reader[((int)ViewbookingrequestColumn.IsComissionDone)];
					//entity.IsComissionDone = (Convert.IsDBNull(reader["IsComissionDone"]))?false:(System.Boolean?)reader["IsComissionDone"];
					entity.BookType = (reader.IsDBNull(((int)ViewbookingrequestColumn.BookType)))?null:(System.Int32?)reader[((int)ViewbookingrequestColumn.BookType)];
					//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
					entity.FrozenDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.FrozenDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.FrozenDate)];
					//entity.FrozenDate = (Convert.IsDBNull(reader["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)reader["FrozenDate"];
					entity.IsFrozen = (System.Boolean)reader[((int)ViewbookingrequestColumn.IsFrozen)];
					//entity.IsFrozen = (Convert.IsDBNull(reader["IsFrozen"]))?false:(System.Boolean)reader["IsFrozen"];
					entity.ConfirmRevenueAmount = (reader.IsDBNull(((int)ViewbookingrequestColumn.ConfirmRevenueAmount)))?null:(System.Decimal?)reader[((int)ViewbookingrequestColumn.ConfirmRevenueAmount)];
					//entity.ConfirmRevenueAmount = (Convert.IsDBNull(reader["ConfirmRevenueAmount"]))?0.0m:(System.Decimal?)reader["ConfirmRevenueAmount"];
					entity.InvoiceId = (reader.IsDBNull(((int)ViewbookingrequestColumn.InvoiceId)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.InvoiceId)];
					//entity.InvoiceId = (Convert.IsDBNull(reader["InvoiceId"]))?(long)0:(System.Int64?)reader["InvoiceId"];
					entity.CountryId = (System.Int64)reader[((int)ViewbookingrequestColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
					entity.CityId = (System.Int64)reader[((int)ViewbookingrequestColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
					entity.HotelName = (reader.IsDBNull(((int)ViewbookingrequestColumn.HotelName)))?null:(System.String)reader[((int)ViewbookingrequestColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.SupportingDocNetto = (reader.IsDBNull(((int)ViewbookingrequestColumn.SupportingDocNetto)))?null:(System.String)reader[((int)ViewbookingrequestColumn.SupportingDocNetto)];
					//entity.SupportingDocNetto = (Convert.IsDBNull(reader["SupportingDocNetto"]))?string.Empty:(System.String)reader["SupportingDocNetto"];
					entity.RequestCollectionId = (reader.IsDBNull(((int)ViewbookingrequestColumn.RequestCollectionId)))?null:(System.String)reader[((int)ViewbookingrequestColumn.RequestCollectionId)];
					//entity.RequestCollectionId = (Convert.IsDBNull(reader["RequestCollectionID"]))?string.Empty:(System.String)reader["RequestCollectionID"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="Viewbookingrequest"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="Viewbookingrequest"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, Viewbookingrequest entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewbookingrequestColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.Name = (reader.IsDBNull(((int)ViewbookingrequestColumn.Name)))?null:(System.String)reader[((int)ViewbookingrequestColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.Usertype = (System.String)reader[((int)ViewbookingrequestColumn.Usertype)];
			//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?string.Empty:(System.String)reader["Usertype"];
			entity.ChannelBy = (reader.IsDBNull(((int)ViewbookingrequestColumn.ChannelBy)))?null:(System.String)reader[((int)ViewbookingrequestColumn.ChannelBy)];
			//entity.ChannelBy = (Convert.IsDBNull(reader["ChannelBy"]))?string.Empty:(System.String)reader["ChannelBy"];
			entity.ChannelId = (reader.IsDBNull(((int)ViewbookingrequestColumn.ChannelId)))?null:(System.String)reader[((int)ViewbookingrequestColumn.ChannelId)];
			//entity.ChannelId = (Convert.IsDBNull(reader["ChannelID"]))?string.Empty:(System.String)reader["ChannelID"];
			entity.TypeUser = (System.String)reader[((int)ViewbookingrequestColumn.TypeUser)];
			//entity.TypeUser = (Convert.IsDBNull(reader["TypeUser"]))?string.Empty:(System.String)reader["TypeUser"];
			entity.Contact = (reader.IsDBNull(((int)ViewbookingrequestColumn.Contact)))?null:(System.String)reader[((int)ViewbookingrequestColumn.Contact)];
			//entity.Contact = (Convert.IsDBNull(reader["Contact"]))?string.Empty:(System.String)reader["Contact"];
			entity.FinalTotalPrice = (reader.IsDBNull(((int)ViewbookingrequestColumn.FinalTotalPrice)))?null:(System.Decimal?)reader[((int)ViewbookingrequestColumn.FinalTotalPrice)];
			//entity.FinalTotalPrice = (Convert.IsDBNull(reader["FinalTotalPrice"]))?0.0m:(System.Decimal?)reader["FinalTotalPrice"];
			entity.HotelId = (System.Int64)reader[((int)ViewbookingrequestColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.MainMeetingRoomId = (System.Int64)reader[((int)ViewbookingrequestColumn.MainMeetingRoomId)];
			//entity.MainMeetingRoomId = (Convert.IsDBNull(reader["MainMeetingRoomId"]))?(long)0:(System.Int64)reader["MainMeetingRoomId"];
			entity.BookingDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.BookingDate)];
			//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
			entity.ArrivalDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.ArrivalDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.ArrivalDate)];
			//entity.ArrivalDate = (Convert.IsDBNull(reader["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)reader["ArrivalDate"];
			entity.DepartureDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.DepartureDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.DepartureDate)];
			//entity.DepartureDate = (Convert.IsDBNull(reader["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)reader["DepartureDate"];
			entity.Duration = (reader.IsDBNull(((int)ViewbookingrequestColumn.Duration)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.Duration)];
			//entity.Duration = (Convert.IsDBNull(reader["Duration"]))?(long)0:(System.Int64?)reader["Duration"];
			entity.CurrencyId = (reader.IsDBNull(((int)ViewbookingrequestColumn.CurrencyId)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.CurrencyId)];
			//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyID"]))?(long)0:(System.Int64?)reader["CurrencyID"];
			entity.LastName = (reader.IsDBNull(((int)ViewbookingrequestColumn.LastName)))?null:(System.String)reader[((int)ViewbookingrequestColumn.LastName)];
			//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
			entity.EmailId = (reader.IsDBNull(((int)ViewbookingrequestColumn.EmailId)))?null:(System.String)reader[((int)ViewbookingrequestColumn.EmailId)];
			//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
			entity.RequestStatus = (reader.IsDBNull(((int)ViewbookingrequestColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewbookingrequestColumn.RequestStatus)];
			//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
			entity.IsUserBookingProcessDone = (System.Boolean)reader[((int)ViewbookingrequestColumn.IsUserBookingProcessDone)];
			//entity.IsUserBookingProcessDone = (Convert.IsDBNull(reader["IsUserBookingProcessDone"]))?false:(System.Boolean)reader["IsUserBookingProcessDone"];
			entity.RevenueAmount = (reader.IsDBNull(((int)ViewbookingrequestColumn.RevenueAmount)))?null:(System.Decimal?)reader[((int)ViewbookingrequestColumn.RevenueAmount)];
			//entity.RevenueAmount = (Convert.IsDBNull(reader["RevenueAmount"]))?0.0m:(System.Decimal?)reader["RevenueAmount"];
			entity.RevenueReason = (reader.IsDBNull(((int)ViewbookingrequestColumn.RevenueReason)))?null:(System.String)reader[((int)ViewbookingrequestColumn.RevenueReason)];
			//entity.RevenueReason = (Convert.IsDBNull(reader["RevenueReason"]))?string.Empty:(System.String)reader["RevenueReason"];
			entity.ComissionSubmitDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.ComissionSubmitDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.ComissionSubmitDate)];
			//entity.ComissionSubmitDate = (Convert.IsDBNull(reader["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)reader["ComissionSubmitDate"];
			entity.AgencyUserId = (reader.IsDBNull(((int)ViewbookingrequestColumn.AgencyUserId)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.AgencyUserId)];
			//entity.AgencyUserId = (Convert.IsDBNull(reader["AgencyUserId"]))?(long)0:(System.Int64?)reader["AgencyUserId"];
			entity.AgencyClientId = (reader.IsDBNull(((int)ViewbookingrequestColumn.AgencyClientId)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.AgencyClientId)];
			//entity.AgencyClientId = (Convert.IsDBNull(reader["AgencyClientId"]))?(long)0:(System.Int64?)reader["AgencyClientId"];
			entity.IsBookedByAgencyUser = (reader.IsDBNull(((int)ViewbookingrequestColumn.IsBookedByAgencyUser)))?null:(System.Boolean?)reader[((int)ViewbookingrequestColumn.IsBookedByAgencyUser)];
			//entity.IsBookedByAgencyUser = (Convert.IsDBNull(reader["IsBookedByAgencyUser"]))?false:(System.Boolean?)reader["IsBookedByAgencyUser"];
			entity.CreatorId = (System.Int64)reader[((int)ViewbookingrequestColumn.CreatorId)];
			//entity.CreatorId = (Convert.IsDBNull(reader["CreatorId"]))?(long)0:(System.Int64)reader["CreatorId"];
			entity.IsPackageSelected = (System.Boolean)reader[((int)ViewbookingrequestColumn.IsPackageSelected)];
			//entity.IsPackageSelected = (Convert.IsDBNull(reader["IsPackageSelected"]))?false:(System.Boolean)reader["IsPackageSelected"];
			entity.Accomodation = (reader.IsDBNull(((int)ViewbookingrequestColumn.Accomodation)))?null:(System.Int32?)reader[((int)ViewbookingrequestColumn.Accomodation)];
			//entity.Accomodation = (Convert.IsDBNull(reader["Accomodation"]))?(int)0:(System.Int32?)reader["Accomodation"];
			entity.IsBedroom = (System.Boolean)reader[((int)ViewbookingrequestColumn.IsBedroom)];
			//entity.IsBedroom = (Convert.IsDBNull(reader["IsBedroom"]))?false:(System.Boolean)reader["IsBedroom"];
			entity.SpecialRequest = (reader.IsDBNull(((int)ViewbookingrequestColumn.SpecialRequest)))?null:(System.String)reader[((int)ViewbookingrequestColumn.SpecialRequest)];
			//entity.SpecialRequest = (Convert.IsDBNull(reader["SpecialRequest"]))?string.Empty:(System.String)reader["SpecialRequest"];
			entity.IsCancled = (reader.IsDBNull(((int)ViewbookingrequestColumn.IsCancled)))?null:(System.Boolean?)reader[((int)ViewbookingrequestColumn.IsCancled)];
			//entity.IsCancled = (Convert.IsDBNull(reader["IsCancled"]))?false:(System.Boolean?)reader["IsCancled"];
			entity.IsComissionDone = (reader.IsDBNull(((int)ViewbookingrequestColumn.IsComissionDone)))?null:(System.Boolean?)reader[((int)ViewbookingrequestColumn.IsComissionDone)];
			//entity.IsComissionDone = (Convert.IsDBNull(reader["IsComissionDone"]))?false:(System.Boolean?)reader["IsComissionDone"];
			entity.BookType = (reader.IsDBNull(((int)ViewbookingrequestColumn.BookType)))?null:(System.Int32?)reader[((int)ViewbookingrequestColumn.BookType)];
			//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
			entity.FrozenDate = (reader.IsDBNull(((int)ViewbookingrequestColumn.FrozenDate)))?null:(System.DateTime?)reader[((int)ViewbookingrequestColumn.FrozenDate)];
			//entity.FrozenDate = (Convert.IsDBNull(reader["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)reader["FrozenDate"];
			entity.IsFrozen = (System.Boolean)reader[((int)ViewbookingrequestColumn.IsFrozen)];
			//entity.IsFrozen = (Convert.IsDBNull(reader["IsFrozen"]))?false:(System.Boolean)reader["IsFrozen"];
			entity.ConfirmRevenueAmount = (reader.IsDBNull(((int)ViewbookingrequestColumn.ConfirmRevenueAmount)))?null:(System.Decimal?)reader[((int)ViewbookingrequestColumn.ConfirmRevenueAmount)];
			//entity.ConfirmRevenueAmount = (Convert.IsDBNull(reader["ConfirmRevenueAmount"]))?0.0m:(System.Decimal?)reader["ConfirmRevenueAmount"];
			entity.InvoiceId = (reader.IsDBNull(((int)ViewbookingrequestColumn.InvoiceId)))?null:(System.Int64?)reader[((int)ViewbookingrequestColumn.InvoiceId)];
			//entity.InvoiceId = (Convert.IsDBNull(reader["InvoiceId"]))?(long)0:(System.Int64?)reader["InvoiceId"];
			entity.CountryId = (System.Int64)reader[((int)ViewbookingrequestColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
			entity.CityId = (System.Int64)reader[((int)ViewbookingrequestColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
			entity.HotelName = (reader.IsDBNull(((int)ViewbookingrequestColumn.HotelName)))?null:(System.String)reader[((int)ViewbookingrequestColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.SupportingDocNetto = (reader.IsDBNull(((int)ViewbookingrequestColumn.SupportingDocNetto)))?null:(System.String)reader[((int)ViewbookingrequestColumn.SupportingDocNetto)];
			//entity.SupportingDocNetto = (Convert.IsDBNull(reader["SupportingDocNetto"]))?string.Empty:(System.String)reader["SupportingDocNetto"];
			entity.RequestCollectionId = (reader.IsDBNull(((int)ViewbookingrequestColumn.RequestCollectionId)))?null:(System.String)reader[((int)ViewbookingrequestColumn.RequestCollectionId)];
			//entity.RequestCollectionId = (Convert.IsDBNull(reader["RequestCollectionID"]))?string.Empty:(System.String)reader["RequestCollectionID"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="Viewbookingrequest"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="Viewbookingrequest"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, Viewbookingrequest entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.Usertype = (Convert.IsDBNull(dataRow["Usertype"]))?string.Empty:(System.String)dataRow["Usertype"];
			entity.ChannelBy = (Convert.IsDBNull(dataRow["ChannelBy"]))?string.Empty:(System.String)dataRow["ChannelBy"];
			entity.ChannelId = (Convert.IsDBNull(dataRow["ChannelID"]))?string.Empty:(System.String)dataRow["ChannelID"];
			entity.TypeUser = (Convert.IsDBNull(dataRow["TypeUser"]))?string.Empty:(System.String)dataRow["TypeUser"];
			entity.Contact = (Convert.IsDBNull(dataRow["Contact"]))?string.Empty:(System.String)dataRow["Contact"];
			entity.FinalTotalPrice = (Convert.IsDBNull(dataRow["FinalTotalPrice"]))?0.0m:(System.Decimal?)dataRow["FinalTotalPrice"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.MainMeetingRoomId = (Convert.IsDBNull(dataRow["MainMeetingRoomId"]))?(long)0:(System.Int64)dataRow["MainMeetingRoomId"];
			entity.BookingDate = (Convert.IsDBNull(dataRow["BookingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BookingDate"];
			entity.ArrivalDate = (Convert.IsDBNull(dataRow["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ArrivalDate"];
			entity.DepartureDate = (Convert.IsDBNull(dataRow["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["DepartureDate"];
			entity.Duration = (Convert.IsDBNull(dataRow["Duration"]))?(long)0:(System.Int64?)dataRow["Duration"];
			entity.CurrencyId = (Convert.IsDBNull(dataRow["CurrencyID"]))?(long)0:(System.Int64?)dataRow["CurrencyID"];
			entity.LastName = (Convert.IsDBNull(dataRow["LastName"]))?string.Empty:(System.String)dataRow["LastName"];
			entity.EmailId = (Convert.IsDBNull(dataRow["EmailId"]))?string.Empty:(System.String)dataRow["EmailId"];
			entity.RequestStatus = (Convert.IsDBNull(dataRow["RequestStatus"]))?(int)0:(System.Int32?)dataRow["RequestStatus"];
			entity.IsUserBookingProcessDone = (Convert.IsDBNull(dataRow["IsUserBookingProcessDone"]))?false:(System.Boolean)dataRow["IsUserBookingProcessDone"];
			entity.RevenueAmount = (Convert.IsDBNull(dataRow["RevenueAmount"]))?0.0m:(System.Decimal?)dataRow["RevenueAmount"];
			entity.RevenueReason = (Convert.IsDBNull(dataRow["RevenueReason"]))?string.Empty:(System.String)dataRow["RevenueReason"];
			entity.ComissionSubmitDate = (Convert.IsDBNull(dataRow["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ComissionSubmitDate"];
			entity.AgencyUserId = (Convert.IsDBNull(dataRow["AgencyUserId"]))?(long)0:(System.Int64?)dataRow["AgencyUserId"];
			entity.AgencyClientId = (Convert.IsDBNull(dataRow["AgencyClientId"]))?(long)0:(System.Int64?)dataRow["AgencyClientId"];
			entity.IsBookedByAgencyUser = (Convert.IsDBNull(dataRow["IsBookedByAgencyUser"]))?false:(System.Boolean?)dataRow["IsBookedByAgencyUser"];
			entity.CreatorId = (Convert.IsDBNull(dataRow["CreatorId"]))?(long)0:(System.Int64)dataRow["CreatorId"];
			entity.IsPackageSelected = (Convert.IsDBNull(dataRow["IsPackageSelected"]))?false:(System.Boolean)dataRow["IsPackageSelected"];
			entity.Accomodation = (Convert.IsDBNull(dataRow["Accomodation"]))?(int)0:(System.Int32?)dataRow["Accomodation"];
			entity.IsBedroom = (Convert.IsDBNull(dataRow["IsBedroom"]))?false:(System.Boolean)dataRow["IsBedroom"];
			entity.SpecialRequest = (Convert.IsDBNull(dataRow["SpecialRequest"]))?string.Empty:(System.String)dataRow["SpecialRequest"];
			entity.IsCancled = (Convert.IsDBNull(dataRow["IsCancled"]))?false:(System.Boolean?)dataRow["IsCancled"];
			entity.IsComissionDone = (Convert.IsDBNull(dataRow["IsComissionDone"]))?false:(System.Boolean?)dataRow["IsComissionDone"];
			entity.BookType = (Convert.IsDBNull(dataRow["BookType"]))?(int)0:(System.Int32?)dataRow["BookType"];
			entity.FrozenDate = (Convert.IsDBNull(dataRow["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["FrozenDate"];
			entity.IsFrozen = (Convert.IsDBNull(dataRow["IsFrozen"]))?false:(System.Boolean)dataRow["IsFrozen"];
			entity.ConfirmRevenueAmount = (Convert.IsDBNull(dataRow["ConfirmRevenueAmount"]))?0.0m:(System.Decimal?)dataRow["ConfirmRevenueAmount"];
			entity.InvoiceId = (Convert.IsDBNull(dataRow["InvoiceId"]))?(long)0:(System.Int64?)dataRow["InvoiceId"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryId"]))?(long)0:(System.Int64)dataRow["CountryId"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityId"]))?(long)0:(System.Int64)dataRow["CityId"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.SupportingDocNetto = (Convert.IsDBNull(dataRow["SupportingDocNetto"]))?string.Empty:(System.String)dataRow["SupportingDocNetto"];
			entity.RequestCollectionId = (Convert.IsDBNull(dataRow["RequestCollectionID"]))?string.Empty:(System.String)dataRow["RequestCollectionID"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewbookingrequestFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Viewbookingrequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewbookingrequestFilterBuilder : SqlFilterBuilder<ViewbookingrequestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestFilterBuilder class.
		/// </summary>
		public ViewbookingrequestFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewbookingrequestFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewbookingrequestFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewbookingrequestFilterBuilder

	#region ViewbookingrequestParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Viewbookingrequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewbookingrequestParameterBuilder : ParameterizedSqlFilterBuilder<ViewbookingrequestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestParameterBuilder class.
		/// </summary>
		public ViewbookingrequestParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewbookingrequestParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewbookingrequestParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewbookingrequestParameterBuilder
	
	#region ViewbookingrequestSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Viewbookingrequest"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewbookingrequestSortBuilder : SqlSortBuilder<ViewbookingrequestColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestSqlSortBuilder class.
		/// </summary>
		public ViewbookingrequestSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewbookingrequestSortBuilder

} // end namespace
