﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewHotelInvoiceProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewHotelInvoiceProviderBaseCore : EntityViewProviderBase<ViewHotelInvoice>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewHotelInvoice&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewHotelInvoice&gt;"/></returns>
		protected static VList&lt;ViewHotelInvoice&gt; Fill(DataSet dataSet, VList<ViewHotelInvoice> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewHotelInvoice>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewHotelInvoice&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewHotelInvoice>"/></returns>
		protected static VList&lt;ViewHotelInvoice&gt; Fill(DataTable dataTable, VList<ViewHotelInvoice> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewHotelInvoice c = new ViewHotelInvoice();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.CityId = (Convert.IsDBNull(row["CityId"]))?(long)0:(System.Int64)row["CityId"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.InvoiceNumber = (Convert.IsDBNull(row["InvoiceNumber"]))?string.Empty:(System.String)row["InvoiceNumber"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.Usertype = (Convert.IsDBNull(row["Usertype"]))?string.Empty:(System.String)row["Usertype"];
					c.TypeUser = (Convert.IsDBNull(row["TypeUser"]))?string.Empty:(System.String)row["TypeUser"];
					c.Contact = (Convert.IsDBNull(row["Contact"]))?string.Empty:(System.String)row["Contact"];
					c.FinalTotalPrice = (Convert.IsDBNull(row["FinalTotalPrice"]))?0.0m:(System.Decimal?)row["FinalTotalPrice"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.MainMeetingRoomId = (Convert.IsDBNull(row["MainMeetingRoomId"]))?(long)0:(System.Int64)row["MainMeetingRoomId"];
					c.BookingDate = (Convert.IsDBNull(row["BookingDate"]))?DateTime.MinValue:(System.DateTime?)row["BookingDate"];
					c.ArrivalDate = (Convert.IsDBNull(row["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)row["ArrivalDate"];
					c.DepartureDate = (Convert.IsDBNull(row["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)row["DepartureDate"];
					c.Duration = (Convert.IsDBNull(row["Duration"]))?(long)0:(System.Int64?)row["Duration"];
					c.CurrencyId = (Convert.IsDBNull(row["CurrencyID"]))?(long)0:(System.Int64?)row["CurrencyID"];
					c.LastName = (Convert.IsDBNull(row["LastName"]))?string.Empty:(System.String)row["LastName"];
					c.EmailId = (Convert.IsDBNull(row["EmailId"]))?string.Empty:(System.String)row["EmailId"];
					c.RequestStatus = (Convert.IsDBNull(row["RequestStatus"]))?(int)0:(System.Int32?)row["RequestStatus"];
					c.IsUserBookingProcessDone = (Convert.IsDBNull(row["IsUserBookingProcessDone"]))?false:(System.Boolean)row["IsUserBookingProcessDone"];
					c.RevenueAmount = (Convert.IsDBNull(row["RevenueAmount"]))?0.0m:(System.Decimal?)row["RevenueAmount"];
					c.RevenueReason = (Convert.IsDBNull(row["RevenueReason"]))?string.Empty:(System.String)row["RevenueReason"];
					c.ComissionSubmitDate = (Convert.IsDBNull(row["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)row["ComissionSubmitDate"];
					c.AgencyUserId = (Convert.IsDBNull(row["AgencyUserId"]))?(long)0:(System.Int64)row["AgencyUserId"];
					c.AgencyClientId = (Convert.IsDBNull(row["AgencyClientId"]))?(long)0:(System.Int64)row["AgencyClientId"];
					c.IsBookedByAgencyUser = (Convert.IsDBNull(row["IsBookedByAgencyUser"]))?false:(System.Boolean?)row["IsBookedByAgencyUser"];
					c.CreatorId = (Convert.IsDBNull(row["CreatorId"]))?(long)0:(System.Int64)row["CreatorId"];
					c.IsPackageSelected = (Convert.IsDBNull(row["IsPackageSelected"]))?false:(System.Boolean)row["IsPackageSelected"];
					c.Accomodation = (Convert.IsDBNull(row["Accomodation"]))?(int)0:(System.Int32?)row["Accomodation"];
					c.IsBedroom = (Convert.IsDBNull(row["IsBedroom"]))?false:(System.Boolean)row["IsBedroom"];
					c.SpecialRequest = (Convert.IsDBNull(row["SpecialRequest"]))?string.Empty:(System.String)row["SpecialRequest"];
					c.IsCancled = (Convert.IsDBNull(row["IsCancled"]))?false:(System.Boolean?)row["IsCancled"];
					c.IsComissionDone = (Convert.IsDBNull(row["IsComissionDone"]))?false:(System.Boolean?)row["IsComissionDone"];
					c.BookType = (Convert.IsDBNull(row["BookType"]))?(int)0:(System.Int32?)row["BookType"];
					c.FrozenDate = (Convert.IsDBNull(row["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)row["FrozenDate"];
					c.IsFrozen = (Convert.IsDBNull(row["IsFrozen"]))?false:(System.Boolean)row["IsFrozen"];
					c.CountryId = (Convert.IsDBNull(row["CountryId"]))?(long)0:(System.Int64)row["CountryId"];
					c.InvoiceId = (Convert.IsDBNull(row["InvoiceId"]))?(long)0:(System.Int64)row["InvoiceId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewHotelInvoice&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewHotelInvoice&gt;"/></returns>
		protected VList<ViewHotelInvoice> Fill(IDataReader reader, VList<ViewHotelInvoice> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewHotelInvoice entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewHotelInvoice>("ViewHotelInvoice",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewHotelInvoice();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewHotelInvoiceColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.CityId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
					entity.HotelName = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.HotelName)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.InvoiceNumber = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.InvoiceNumber)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.InvoiceNumber)];
					//entity.InvoiceNumber = (Convert.IsDBNull(reader["InvoiceNumber"]))?string.Empty:(System.String)reader["InvoiceNumber"];
					entity.Name = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.Name)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.Usertype = (System.String)reader[((int)ViewHotelInvoiceColumn.Usertype)];
					//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?string.Empty:(System.String)reader["Usertype"];
					entity.TypeUser = (System.String)reader[((int)ViewHotelInvoiceColumn.TypeUser)];
					//entity.TypeUser = (Convert.IsDBNull(reader["TypeUser"]))?string.Empty:(System.String)reader["TypeUser"];
					entity.Contact = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.Contact)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.Contact)];
					//entity.Contact = (Convert.IsDBNull(reader["Contact"]))?string.Empty:(System.String)reader["Contact"];
					entity.FinalTotalPrice = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.FinalTotalPrice)))?null:(System.Decimal?)reader[((int)ViewHotelInvoiceColumn.FinalTotalPrice)];
					//entity.FinalTotalPrice = (Convert.IsDBNull(reader["FinalTotalPrice"]))?0.0m:(System.Decimal?)reader["FinalTotalPrice"];
					entity.HotelId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.MainMeetingRoomId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.MainMeetingRoomId)];
					//entity.MainMeetingRoomId = (Convert.IsDBNull(reader["MainMeetingRoomId"]))?(long)0:(System.Int64)reader["MainMeetingRoomId"];
					entity.BookingDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.BookingDate)];
					//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
					entity.ArrivalDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.ArrivalDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.ArrivalDate)];
					//entity.ArrivalDate = (Convert.IsDBNull(reader["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)reader["ArrivalDate"];
					entity.DepartureDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.DepartureDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.DepartureDate)];
					//entity.DepartureDate = (Convert.IsDBNull(reader["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)reader["DepartureDate"];
					entity.Duration = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.Duration)))?null:(System.Int64?)reader[((int)ViewHotelInvoiceColumn.Duration)];
					//entity.Duration = (Convert.IsDBNull(reader["Duration"]))?(long)0:(System.Int64?)reader["Duration"];
					entity.CurrencyId = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.CurrencyId)))?null:(System.Int64?)reader[((int)ViewHotelInvoiceColumn.CurrencyId)];
					//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyID"]))?(long)0:(System.Int64?)reader["CurrencyID"];
					entity.LastName = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.LastName)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.LastName)];
					//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
					entity.EmailId = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.EmailId)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.EmailId)];
					//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
					entity.RequestStatus = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewHotelInvoiceColumn.RequestStatus)];
					//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
					entity.IsUserBookingProcessDone = (System.Boolean)reader[((int)ViewHotelInvoiceColumn.IsUserBookingProcessDone)];
					//entity.IsUserBookingProcessDone = (Convert.IsDBNull(reader["IsUserBookingProcessDone"]))?false:(System.Boolean)reader["IsUserBookingProcessDone"];
					entity.RevenueAmount = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.RevenueAmount)))?null:(System.Decimal?)reader[((int)ViewHotelInvoiceColumn.RevenueAmount)];
					//entity.RevenueAmount = (Convert.IsDBNull(reader["RevenueAmount"]))?0.0m:(System.Decimal?)reader["RevenueAmount"];
					entity.RevenueReason = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.RevenueReason)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.RevenueReason)];
					//entity.RevenueReason = (Convert.IsDBNull(reader["RevenueReason"]))?string.Empty:(System.String)reader["RevenueReason"];
					entity.ComissionSubmitDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.ComissionSubmitDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.ComissionSubmitDate)];
					//entity.ComissionSubmitDate = (Convert.IsDBNull(reader["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)reader["ComissionSubmitDate"];
					entity.AgencyUserId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.AgencyUserId)];
					//entity.AgencyUserId = (Convert.IsDBNull(reader["AgencyUserId"]))?(long)0:(System.Int64)reader["AgencyUserId"];
					entity.AgencyClientId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.AgencyClientId)];
					//entity.AgencyClientId = (Convert.IsDBNull(reader["AgencyClientId"]))?(long)0:(System.Int64)reader["AgencyClientId"];
					entity.IsBookedByAgencyUser = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.IsBookedByAgencyUser)))?null:(System.Boolean?)reader[((int)ViewHotelInvoiceColumn.IsBookedByAgencyUser)];
					//entity.IsBookedByAgencyUser = (Convert.IsDBNull(reader["IsBookedByAgencyUser"]))?false:(System.Boolean?)reader["IsBookedByAgencyUser"];
					entity.CreatorId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.CreatorId)];
					//entity.CreatorId = (Convert.IsDBNull(reader["CreatorId"]))?(long)0:(System.Int64)reader["CreatorId"];
					entity.IsPackageSelected = (System.Boolean)reader[((int)ViewHotelInvoiceColumn.IsPackageSelected)];
					//entity.IsPackageSelected = (Convert.IsDBNull(reader["IsPackageSelected"]))?false:(System.Boolean)reader["IsPackageSelected"];
					entity.Accomodation = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.Accomodation)))?null:(System.Int32?)reader[((int)ViewHotelInvoiceColumn.Accomodation)];
					//entity.Accomodation = (Convert.IsDBNull(reader["Accomodation"]))?(int)0:(System.Int32?)reader["Accomodation"];
					entity.IsBedroom = (System.Boolean)reader[((int)ViewHotelInvoiceColumn.IsBedroom)];
					//entity.IsBedroom = (Convert.IsDBNull(reader["IsBedroom"]))?false:(System.Boolean)reader["IsBedroom"];
					entity.SpecialRequest = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.SpecialRequest)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.SpecialRequest)];
					//entity.SpecialRequest = (Convert.IsDBNull(reader["SpecialRequest"]))?string.Empty:(System.String)reader["SpecialRequest"];
					entity.IsCancled = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.IsCancled)))?null:(System.Boolean?)reader[((int)ViewHotelInvoiceColumn.IsCancled)];
					//entity.IsCancled = (Convert.IsDBNull(reader["IsCancled"]))?false:(System.Boolean?)reader["IsCancled"];
					entity.IsComissionDone = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.IsComissionDone)))?null:(System.Boolean?)reader[((int)ViewHotelInvoiceColumn.IsComissionDone)];
					//entity.IsComissionDone = (Convert.IsDBNull(reader["IsComissionDone"]))?false:(System.Boolean?)reader["IsComissionDone"];
					entity.BookType = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.BookType)))?null:(System.Int32?)reader[((int)ViewHotelInvoiceColumn.BookType)];
					//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
					entity.FrozenDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.FrozenDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.FrozenDate)];
					//entity.FrozenDate = (Convert.IsDBNull(reader["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)reader["FrozenDate"];
					entity.IsFrozen = (System.Boolean)reader[((int)ViewHotelInvoiceColumn.IsFrozen)];
					//entity.IsFrozen = (Convert.IsDBNull(reader["IsFrozen"]))?false:(System.Boolean)reader["IsFrozen"];
					entity.CountryId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
					entity.InvoiceId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.InvoiceId)];
					//entity.InvoiceId = (Convert.IsDBNull(reader["InvoiceId"]))?(long)0:(System.Int64)reader["InvoiceId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewHotelInvoice"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewHotelInvoice"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewHotelInvoice entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewHotelInvoiceColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.CityId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
			entity.HotelName = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.HotelName)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.InvoiceNumber = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.InvoiceNumber)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.InvoiceNumber)];
			//entity.InvoiceNumber = (Convert.IsDBNull(reader["InvoiceNumber"]))?string.Empty:(System.String)reader["InvoiceNumber"];
			entity.Name = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.Name)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.Usertype = (System.String)reader[((int)ViewHotelInvoiceColumn.Usertype)];
			//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?string.Empty:(System.String)reader["Usertype"];
			entity.TypeUser = (System.String)reader[((int)ViewHotelInvoiceColumn.TypeUser)];
			//entity.TypeUser = (Convert.IsDBNull(reader["TypeUser"]))?string.Empty:(System.String)reader["TypeUser"];
			entity.Contact = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.Contact)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.Contact)];
			//entity.Contact = (Convert.IsDBNull(reader["Contact"]))?string.Empty:(System.String)reader["Contact"];
			entity.FinalTotalPrice = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.FinalTotalPrice)))?null:(System.Decimal?)reader[((int)ViewHotelInvoiceColumn.FinalTotalPrice)];
			//entity.FinalTotalPrice = (Convert.IsDBNull(reader["FinalTotalPrice"]))?0.0m:(System.Decimal?)reader["FinalTotalPrice"];
			entity.HotelId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.MainMeetingRoomId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.MainMeetingRoomId)];
			//entity.MainMeetingRoomId = (Convert.IsDBNull(reader["MainMeetingRoomId"]))?(long)0:(System.Int64)reader["MainMeetingRoomId"];
			entity.BookingDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.BookingDate)];
			//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
			entity.ArrivalDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.ArrivalDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.ArrivalDate)];
			//entity.ArrivalDate = (Convert.IsDBNull(reader["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)reader["ArrivalDate"];
			entity.DepartureDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.DepartureDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.DepartureDate)];
			//entity.DepartureDate = (Convert.IsDBNull(reader["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)reader["DepartureDate"];
			entity.Duration = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.Duration)))?null:(System.Int64?)reader[((int)ViewHotelInvoiceColumn.Duration)];
			//entity.Duration = (Convert.IsDBNull(reader["Duration"]))?(long)0:(System.Int64?)reader["Duration"];
			entity.CurrencyId = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.CurrencyId)))?null:(System.Int64?)reader[((int)ViewHotelInvoiceColumn.CurrencyId)];
			//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyID"]))?(long)0:(System.Int64?)reader["CurrencyID"];
			entity.LastName = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.LastName)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.LastName)];
			//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
			entity.EmailId = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.EmailId)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.EmailId)];
			//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
			entity.RequestStatus = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewHotelInvoiceColumn.RequestStatus)];
			//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
			entity.IsUserBookingProcessDone = (System.Boolean)reader[((int)ViewHotelInvoiceColumn.IsUserBookingProcessDone)];
			//entity.IsUserBookingProcessDone = (Convert.IsDBNull(reader["IsUserBookingProcessDone"]))?false:(System.Boolean)reader["IsUserBookingProcessDone"];
			entity.RevenueAmount = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.RevenueAmount)))?null:(System.Decimal?)reader[((int)ViewHotelInvoiceColumn.RevenueAmount)];
			//entity.RevenueAmount = (Convert.IsDBNull(reader["RevenueAmount"]))?0.0m:(System.Decimal?)reader["RevenueAmount"];
			entity.RevenueReason = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.RevenueReason)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.RevenueReason)];
			//entity.RevenueReason = (Convert.IsDBNull(reader["RevenueReason"]))?string.Empty:(System.String)reader["RevenueReason"];
			entity.ComissionSubmitDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.ComissionSubmitDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.ComissionSubmitDate)];
			//entity.ComissionSubmitDate = (Convert.IsDBNull(reader["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)reader["ComissionSubmitDate"];
			entity.AgencyUserId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.AgencyUserId)];
			//entity.AgencyUserId = (Convert.IsDBNull(reader["AgencyUserId"]))?(long)0:(System.Int64)reader["AgencyUserId"];
			entity.AgencyClientId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.AgencyClientId)];
			//entity.AgencyClientId = (Convert.IsDBNull(reader["AgencyClientId"]))?(long)0:(System.Int64)reader["AgencyClientId"];
			entity.IsBookedByAgencyUser = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.IsBookedByAgencyUser)))?null:(System.Boolean?)reader[((int)ViewHotelInvoiceColumn.IsBookedByAgencyUser)];
			//entity.IsBookedByAgencyUser = (Convert.IsDBNull(reader["IsBookedByAgencyUser"]))?false:(System.Boolean?)reader["IsBookedByAgencyUser"];
			entity.CreatorId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.CreatorId)];
			//entity.CreatorId = (Convert.IsDBNull(reader["CreatorId"]))?(long)0:(System.Int64)reader["CreatorId"];
			entity.IsPackageSelected = (System.Boolean)reader[((int)ViewHotelInvoiceColumn.IsPackageSelected)];
			//entity.IsPackageSelected = (Convert.IsDBNull(reader["IsPackageSelected"]))?false:(System.Boolean)reader["IsPackageSelected"];
			entity.Accomodation = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.Accomodation)))?null:(System.Int32?)reader[((int)ViewHotelInvoiceColumn.Accomodation)];
			//entity.Accomodation = (Convert.IsDBNull(reader["Accomodation"]))?(int)0:(System.Int32?)reader["Accomodation"];
			entity.IsBedroom = (System.Boolean)reader[((int)ViewHotelInvoiceColumn.IsBedroom)];
			//entity.IsBedroom = (Convert.IsDBNull(reader["IsBedroom"]))?false:(System.Boolean)reader["IsBedroom"];
			entity.SpecialRequest = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.SpecialRequest)))?null:(System.String)reader[((int)ViewHotelInvoiceColumn.SpecialRequest)];
			//entity.SpecialRequest = (Convert.IsDBNull(reader["SpecialRequest"]))?string.Empty:(System.String)reader["SpecialRequest"];
			entity.IsCancled = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.IsCancled)))?null:(System.Boolean?)reader[((int)ViewHotelInvoiceColumn.IsCancled)];
			//entity.IsCancled = (Convert.IsDBNull(reader["IsCancled"]))?false:(System.Boolean?)reader["IsCancled"];
			entity.IsComissionDone = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.IsComissionDone)))?null:(System.Boolean?)reader[((int)ViewHotelInvoiceColumn.IsComissionDone)];
			//entity.IsComissionDone = (Convert.IsDBNull(reader["IsComissionDone"]))?false:(System.Boolean?)reader["IsComissionDone"];
			entity.BookType = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.BookType)))?null:(System.Int32?)reader[((int)ViewHotelInvoiceColumn.BookType)];
			//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
			entity.FrozenDate = (reader.IsDBNull(((int)ViewHotelInvoiceColumn.FrozenDate)))?null:(System.DateTime?)reader[((int)ViewHotelInvoiceColumn.FrozenDate)];
			//entity.FrozenDate = (Convert.IsDBNull(reader["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)reader["FrozenDate"];
			entity.IsFrozen = (System.Boolean)reader[((int)ViewHotelInvoiceColumn.IsFrozen)];
			//entity.IsFrozen = (Convert.IsDBNull(reader["IsFrozen"]))?false:(System.Boolean)reader["IsFrozen"];
			entity.CountryId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
			entity.InvoiceId = (System.Int64)reader[((int)ViewHotelInvoiceColumn.InvoiceId)];
			//entity.InvoiceId = (Convert.IsDBNull(reader["InvoiceId"]))?(long)0:(System.Int64)reader["InvoiceId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewHotelInvoice"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewHotelInvoice"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewHotelInvoice entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityId"]))?(long)0:(System.Int64)dataRow["CityId"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.InvoiceNumber = (Convert.IsDBNull(dataRow["InvoiceNumber"]))?string.Empty:(System.String)dataRow["InvoiceNumber"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.Usertype = (Convert.IsDBNull(dataRow["Usertype"]))?string.Empty:(System.String)dataRow["Usertype"];
			entity.TypeUser = (Convert.IsDBNull(dataRow["TypeUser"]))?string.Empty:(System.String)dataRow["TypeUser"];
			entity.Contact = (Convert.IsDBNull(dataRow["Contact"]))?string.Empty:(System.String)dataRow["Contact"];
			entity.FinalTotalPrice = (Convert.IsDBNull(dataRow["FinalTotalPrice"]))?0.0m:(System.Decimal?)dataRow["FinalTotalPrice"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.MainMeetingRoomId = (Convert.IsDBNull(dataRow["MainMeetingRoomId"]))?(long)0:(System.Int64)dataRow["MainMeetingRoomId"];
			entity.BookingDate = (Convert.IsDBNull(dataRow["BookingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BookingDate"];
			entity.ArrivalDate = (Convert.IsDBNull(dataRow["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ArrivalDate"];
			entity.DepartureDate = (Convert.IsDBNull(dataRow["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["DepartureDate"];
			entity.Duration = (Convert.IsDBNull(dataRow["Duration"]))?(long)0:(System.Int64?)dataRow["Duration"];
			entity.CurrencyId = (Convert.IsDBNull(dataRow["CurrencyID"]))?(long)0:(System.Int64?)dataRow["CurrencyID"];
			entity.LastName = (Convert.IsDBNull(dataRow["LastName"]))?string.Empty:(System.String)dataRow["LastName"];
			entity.EmailId = (Convert.IsDBNull(dataRow["EmailId"]))?string.Empty:(System.String)dataRow["EmailId"];
			entity.RequestStatus = (Convert.IsDBNull(dataRow["RequestStatus"]))?(int)0:(System.Int32?)dataRow["RequestStatus"];
			entity.IsUserBookingProcessDone = (Convert.IsDBNull(dataRow["IsUserBookingProcessDone"]))?false:(System.Boolean)dataRow["IsUserBookingProcessDone"];
			entity.RevenueAmount = (Convert.IsDBNull(dataRow["RevenueAmount"]))?0.0m:(System.Decimal?)dataRow["RevenueAmount"];
			entity.RevenueReason = (Convert.IsDBNull(dataRow["RevenueReason"]))?string.Empty:(System.String)dataRow["RevenueReason"];
			entity.ComissionSubmitDate = (Convert.IsDBNull(dataRow["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ComissionSubmitDate"];
			entity.AgencyUserId = (Convert.IsDBNull(dataRow["AgencyUserId"]))?(long)0:(System.Int64)dataRow["AgencyUserId"];
			entity.AgencyClientId = (Convert.IsDBNull(dataRow["AgencyClientId"]))?(long)0:(System.Int64)dataRow["AgencyClientId"];
			entity.IsBookedByAgencyUser = (Convert.IsDBNull(dataRow["IsBookedByAgencyUser"]))?false:(System.Boolean?)dataRow["IsBookedByAgencyUser"];
			entity.CreatorId = (Convert.IsDBNull(dataRow["CreatorId"]))?(long)0:(System.Int64)dataRow["CreatorId"];
			entity.IsPackageSelected = (Convert.IsDBNull(dataRow["IsPackageSelected"]))?false:(System.Boolean)dataRow["IsPackageSelected"];
			entity.Accomodation = (Convert.IsDBNull(dataRow["Accomodation"]))?(int)0:(System.Int32?)dataRow["Accomodation"];
			entity.IsBedroom = (Convert.IsDBNull(dataRow["IsBedroom"]))?false:(System.Boolean)dataRow["IsBedroom"];
			entity.SpecialRequest = (Convert.IsDBNull(dataRow["SpecialRequest"]))?string.Empty:(System.String)dataRow["SpecialRequest"];
			entity.IsCancled = (Convert.IsDBNull(dataRow["IsCancled"]))?false:(System.Boolean?)dataRow["IsCancled"];
			entity.IsComissionDone = (Convert.IsDBNull(dataRow["IsComissionDone"]))?false:(System.Boolean?)dataRow["IsComissionDone"];
			entity.BookType = (Convert.IsDBNull(dataRow["BookType"]))?(int)0:(System.Int32?)dataRow["BookType"];
			entity.FrozenDate = (Convert.IsDBNull(dataRow["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["FrozenDate"];
			entity.IsFrozen = (Convert.IsDBNull(dataRow["IsFrozen"]))?false:(System.Boolean)dataRow["IsFrozen"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryId"]))?(long)0:(System.Int64)dataRow["CountryId"];
			entity.InvoiceId = (Convert.IsDBNull(dataRow["InvoiceId"]))?(long)0:(System.Int64)dataRow["InvoiceId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewHotelInvoiceFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewHotelInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewHotelInvoiceFilterBuilder : SqlFilterBuilder<ViewHotelInvoiceColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewHotelInvoiceFilterBuilder class.
		/// </summary>
		public ViewHotelInvoiceFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewHotelInvoiceFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewHotelInvoiceFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewHotelInvoiceFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewHotelInvoiceFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewHotelInvoiceFilterBuilder

	#region ViewHotelInvoiceParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewHotelInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewHotelInvoiceParameterBuilder : ParameterizedSqlFilterBuilder<ViewHotelInvoiceColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewHotelInvoiceParameterBuilder class.
		/// </summary>
		public ViewHotelInvoiceParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewHotelInvoiceParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewHotelInvoiceParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewHotelInvoiceParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewHotelInvoiceParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewHotelInvoiceParameterBuilder
	
	#region ViewHotelInvoiceSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewHotelInvoice"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewHotelInvoiceSortBuilder : SqlSortBuilder<ViewHotelInvoiceColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewHotelInvoiceSqlSortBuilder class.
		/// </summary>
		public ViewHotelInvoiceSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewHotelInvoiceSortBuilder

} // end namespace
