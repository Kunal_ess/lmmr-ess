﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewMeetingRoomAvailabilityProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewMeetingRoomAvailabilityProviderBaseCore : EntityViewProviderBase<ViewMeetingRoomAvailability>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewMeetingRoomAvailability&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewMeetingRoomAvailability&gt;"/></returns>
		protected static VList&lt;ViewMeetingRoomAvailability&gt; Fill(DataSet dataSet, VList<ViewMeetingRoomAvailability> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewMeetingRoomAvailability>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewMeetingRoomAvailability&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewMeetingRoomAvailability>"/></returns>
		protected static VList&lt;ViewMeetingRoomAvailability&gt; Fill(DataTable dataTable, VList<ViewMeetingRoomAvailability> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewMeetingRoomAvailability c = new ViewMeetingRoomAvailability();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.AvailabilityHotelId = (Convert.IsDBNull(row["AvailabilityHotelId"]))?(long)0:(System.Int64)row["AvailabilityHotelId"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.FullPropertyStatus = (Convert.IsDBNull(row["FullPropertyStatus"]))?(int)0:(System.Int32?)row["FullPropertyStatus"];
					c.AvailibilityOfRoomsId = (Convert.IsDBNull(row["AvailibilityOfRoomsId"]))?(long)0:(System.Int64)row["AvailibilityOfRoomsId"];
					c.AvailibilityIdForBedRoom = (Convert.IsDBNull(row["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)row["AvailibilityIdForBedRoom"];
					c.RoomId = (Convert.IsDBNull(row["RoomId"]))?(long)0:(System.Int64)row["RoomId"];
					c.MorningStatus = (Convert.IsDBNull(row["MorningStatus"]))?(int)0:(System.Int32?)row["MorningStatus"];
					c.AfternoonStatus = (Convert.IsDBNull(row["AfternoonStatus"]))?(int)0:(System.Int32?)row["AfternoonStatus"];
					c.MeetingRoomId = (Convert.IsDBNull(row["MeetingRoomId"]))?(long)0:(System.Int64)row["MeetingRoomId"];
					c.MeetingroomHotelId = (Convert.IsDBNull(row["MeetingroomHotelId"]))?(long)0:(System.Int64)row["MeetingroomHotelId"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.Picture = (Convert.IsDBNull(row["Picture"]))?string.Empty:(System.String)row["Picture"];
					c.DayLight = (Convert.IsDBNull(row["DayLight"]))?(int)0:(System.Int32)row["DayLight"];
					c.Height = (Convert.IsDBNull(row["Height"]))?0.0m:(System.Decimal?)row["Height"];
					c.Surface = (Convert.IsDBNull(row["Surface"]))?(long)0:(System.Int64?)row["Surface"];
					c.MrPlan = (Convert.IsDBNull(row["MR_Plan"]))?string.Empty:(System.String)row["MR_Plan"];
					c.HalfdayPrice = (Convert.IsDBNull(row["HalfdayPrice"]))?0.0m:(System.Decimal?)row["HalfdayPrice"];
					c.FulldayPrice = (Convert.IsDBNull(row["FulldayPrice"]))?0.0m:(System.Decimal?)row["FulldayPrice"];
					c.RoomType = (Convert.IsDBNull(row["RoomType"]))?(int)0:(System.Int32?)row["RoomType"];
					c.MaxNo = (Convert.IsDBNull(row["MaxNo"]))?(int)0:(System.Int32?)row["MaxNo"];
					c.MinNo = (Convert.IsDBNull(row["MinNo"]))?(int)0:(System.Int32?)row["MinNo"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewMeetingRoomAvailability&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewMeetingRoomAvailability&gt;"/></returns>
		protected VList<ViewMeetingRoomAvailability> Fill(IDataReader reader, VList<ViewMeetingRoomAvailability> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewMeetingRoomAvailability entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewMeetingRoomAvailability>("ViewMeetingRoomAvailability",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewMeetingRoomAvailability();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.AvailabilityHotelId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.AvailabilityHotelId)];
					//entity.AvailabilityHotelId = (Convert.IsDBNull(reader["AvailabilityHotelId"]))?(long)0:(System.Int64)reader["AvailabilityHotelId"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewMeetingRoomAvailabilityColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.FullPropertyStatus = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.FullPropertyStatus)];
					//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
					entity.AvailibilityOfRoomsId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.AvailibilityOfRoomsId)];
					//entity.AvailibilityOfRoomsId = (Convert.IsDBNull(reader["AvailibilityOfRoomsId"]))?(long)0:(System.Int64)reader["AvailibilityOfRoomsId"];
					entity.AvailibilityIdForBedRoom = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.AvailibilityIdForBedRoom)];
					//entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(reader["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)reader["AvailibilityIdForBedRoom"];
					entity.RoomId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.RoomId)];
					//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
					entity.MorningStatus = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.MorningStatus)];
					//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
					entity.AfternoonStatus = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.AfternoonStatus)];
					//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
					entity.MeetingRoomId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.MeetingRoomId)];
					//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomId"]))?(long)0:(System.Int64)reader["MeetingRoomId"];
					entity.MeetingroomHotelId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.MeetingroomHotelId)];
					//entity.MeetingroomHotelId = (Convert.IsDBNull(reader["MeetingroomHotelId"]))?(long)0:(System.Int64)reader["MeetingroomHotelId"];
					entity.Name = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.Name)))?null:(System.String)reader[((int)ViewMeetingRoomAvailabilityColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.HotelId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.Picture = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.Picture)))?null:(System.String)reader[((int)ViewMeetingRoomAvailabilityColumn.Picture)];
					//entity.Picture = (Convert.IsDBNull(reader["Picture"]))?string.Empty:(System.String)reader["Picture"];
					entity.DayLight = (System.Int32)reader[((int)ViewMeetingRoomAvailabilityColumn.DayLight)];
					//entity.DayLight = (Convert.IsDBNull(reader["DayLight"]))?(int)0:(System.Int32)reader["DayLight"];
					entity.Height = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.Height)))?null:(System.Decimal?)reader[((int)ViewMeetingRoomAvailabilityColumn.Height)];
					//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
					entity.Surface = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.Surface)))?null:(System.Int64?)reader[((int)ViewMeetingRoomAvailabilityColumn.Surface)];
					//entity.Surface = (Convert.IsDBNull(reader["Surface"]))?(long)0:(System.Int64?)reader["Surface"];
					entity.MrPlan = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.MrPlan)))?null:(System.String)reader[((int)ViewMeetingRoomAvailabilityColumn.MrPlan)];
					//entity.MrPlan = (Convert.IsDBNull(reader["MR_Plan"]))?string.Empty:(System.String)reader["MR_Plan"];
					entity.HalfdayPrice = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.HalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewMeetingRoomAvailabilityColumn.HalfdayPrice)];
					//entity.HalfdayPrice = (Convert.IsDBNull(reader["HalfdayPrice"]))?0.0m:(System.Decimal?)reader["HalfdayPrice"];
					entity.FulldayPrice = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.FulldayPrice)))?null:(System.Decimal?)reader[((int)ViewMeetingRoomAvailabilityColumn.FulldayPrice)];
					//entity.FulldayPrice = (Convert.IsDBNull(reader["FulldayPrice"]))?0.0m:(System.Decimal?)reader["FulldayPrice"];
					entity.RoomType = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.RoomType)];
					//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
					entity.MaxNo = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.MaxNo)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.MaxNo)];
					//entity.MaxNo = (Convert.IsDBNull(reader["MaxNo"]))?(int)0:(System.Int32?)reader["MaxNo"];
					entity.MinNo = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.MinNo)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.MinNo)];
					//entity.MinNo = (Convert.IsDBNull(reader["MinNo"]))?(int)0:(System.Int32?)reader["MinNo"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewMeetingRoomAvailability"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewMeetingRoomAvailability"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewMeetingRoomAvailability entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.AvailabilityHotelId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.AvailabilityHotelId)];
			//entity.AvailabilityHotelId = (Convert.IsDBNull(reader["AvailabilityHotelId"]))?(long)0:(System.Int64)reader["AvailabilityHotelId"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewMeetingRoomAvailabilityColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.FullPropertyStatus = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.FullPropertyStatus)];
			//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
			entity.AvailibilityOfRoomsId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.AvailibilityOfRoomsId)];
			//entity.AvailibilityOfRoomsId = (Convert.IsDBNull(reader["AvailibilityOfRoomsId"]))?(long)0:(System.Int64)reader["AvailibilityOfRoomsId"];
			entity.AvailibilityIdForBedRoom = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.AvailibilityIdForBedRoom)];
			//entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(reader["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)reader["AvailibilityIdForBedRoom"];
			entity.RoomId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.RoomId)];
			//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
			entity.MorningStatus = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.MorningStatus)];
			//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
			entity.AfternoonStatus = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.AfternoonStatus)];
			//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
			entity.MeetingRoomId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.MeetingRoomId)];
			//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomId"]))?(long)0:(System.Int64)reader["MeetingRoomId"];
			entity.MeetingroomHotelId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.MeetingroomHotelId)];
			//entity.MeetingroomHotelId = (Convert.IsDBNull(reader["MeetingroomHotelId"]))?(long)0:(System.Int64)reader["MeetingroomHotelId"];
			entity.Name = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.Name)))?null:(System.String)reader[((int)ViewMeetingRoomAvailabilityColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.HotelId = (System.Int64)reader[((int)ViewMeetingRoomAvailabilityColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.Picture = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.Picture)))?null:(System.String)reader[((int)ViewMeetingRoomAvailabilityColumn.Picture)];
			//entity.Picture = (Convert.IsDBNull(reader["Picture"]))?string.Empty:(System.String)reader["Picture"];
			entity.DayLight = (System.Int32)reader[((int)ViewMeetingRoomAvailabilityColumn.DayLight)];
			//entity.DayLight = (Convert.IsDBNull(reader["DayLight"]))?(int)0:(System.Int32)reader["DayLight"];
			entity.Height = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.Height)))?null:(System.Decimal?)reader[((int)ViewMeetingRoomAvailabilityColumn.Height)];
			//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
			entity.Surface = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.Surface)))?null:(System.Int64?)reader[((int)ViewMeetingRoomAvailabilityColumn.Surface)];
			//entity.Surface = (Convert.IsDBNull(reader["Surface"]))?(long)0:(System.Int64?)reader["Surface"];
			entity.MrPlan = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.MrPlan)))?null:(System.String)reader[((int)ViewMeetingRoomAvailabilityColumn.MrPlan)];
			//entity.MrPlan = (Convert.IsDBNull(reader["MR_Plan"]))?string.Empty:(System.String)reader["MR_Plan"];
			entity.HalfdayPrice = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.HalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewMeetingRoomAvailabilityColumn.HalfdayPrice)];
			//entity.HalfdayPrice = (Convert.IsDBNull(reader["HalfdayPrice"]))?0.0m:(System.Decimal?)reader["HalfdayPrice"];
			entity.FulldayPrice = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.FulldayPrice)))?null:(System.Decimal?)reader[((int)ViewMeetingRoomAvailabilityColumn.FulldayPrice)];
			//entity.FulldayPrice = (Convert.IsDBNull(reader["FulldayPrice"]))?0.0m:(System.Decimal?)reader["FulldayPrice"];
			entity.RoomType = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.RoomType)];
			//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
			entity.MaxNo = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.MaxNo)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.MaxNo)];
			//entity.MaxNo = (Convert.IsDBNull(reader["MaxNo"]))?(int)0:(System.Int32?)reader["MaxNo"];
			entity.MinNo = (reader.IsDBNull(((int)ViewMeetingRoomAvailabilityColumn.MinNo)))?null:(System.Int32?)reader[((int)ViewMeetingRoomAvailabilityColumn.MinNo)];
			//entity.MinNo = (Convert.IsDBNull(reader["MinNo"]))?(int)0:(System.Int32?)reader["MinNo"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewMeetingRoomAvailability"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewMeetingRoomAvailability"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewMeetingRoomAvailability entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.AvailabilityHotelId = (Convert.IsDBNull(dataRow["AvailabilityHotelId"]))?(long)0:(System.Int64)dataRow["AvailabilityHotelId"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.FullPropertyStatus = (Convert.IsDBNull(dataRow["FullPropertyStatus"]))?(int)0:(System.Int32?)dataRow["FullPropertyStatus"];
			entity.AvailibilityOfRoomsId = (Convert.IsDBNull(dataRow["AvailibilityOfRoomsId"]))?(long)0:(System.Int64)dataRow["AvailibilityOfRoomsId"];
			entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(dataRow["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)dataRow["AvailibilityIdForBedRoom"];
			entity.RoomId = (Convert.IsDBNull(dataRow["RoomId"]))?(long)0:(System.Int64)dataRow["RoomId"];
			entity.MorningStatus = (Convert.IsDBNull(dataRow["MorningStatus"]))?(int)0:(System.Int32?)dataRow["MorningStatus"];
			entity.AfternoonStatus = (Convert.IsDBNull(dataRow["AfternoonStatus"]))?(int)0:(System.Int32?)dataRow["AfternoonStatus"];
			entity.MeetingRoomId = (Convert.IsDBNull(dataRow["MeetingRoomId"]))?(long)0:(System.Int64)dataRow["MeetingRoomId"];
			entity.MeetingroomHotelId = (Convert.IsDBNull(dataRow["MeetingroomHotelId"]))?(long)0:(System.Int64)dataRow["MeetingroomHotelId"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.Picture = (Convert.IsDBNull(dataRow["Picture"]))?string.Empty:(System.String)dataRow["Picture"];
			entity.DayLight = (Convert.IsDBNull(dataRow["DayLight"]))?(int)0:(System.Int32)dataRow["DayLight"];
			entity.Height = (Convert.IsDBNull(dataRow["Height"]))?0.0m:(System.Decimal?)dataRow["Height"];
			entity.Surface = (Convert.IsDBNull(dataRow["Surface"]))?(long)0:(System.Int64?)dataRow["Surface"];
			entity.MrPlan = (Convert.IsDBNull(dataRow["MR_Plan"]))?string.Empty:(System.String)dataRow["MR_Plan"];
			entity.HalfdayPrice = (Convert.IsDBNull(dataRow["HalfdayPrice"]))?0.0m:(System.Decimal?)dataRow["HalfdayPrice"];
			entity.FulldayPrice = (Convert.IsDBNull(dataRow["FulldayPrice"]))?0.0m:(System.Decimal?)dataRow["FulldayPrice"];
			entity.RoomType = (Convert.IsDBNull(dataRow["RoomType"]))?(int)0:(System.Int32?)dataRow["RoomType"];
			entity.MaxNo = (Convert.IsDBNull(dataRow["MaxNo"]))?(int)0:(System.Int32?)dataRow["MaxNo"];
			entity.MinNo = (Convert.IsDBNull(dataRow["MinNo"]))?(int)0:(System.Int32?)dataRow["MinNo"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewMeetingRoomAvailabilityFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewMeetingRoomAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewMeetingRoomAvailabilityFilterBuilder : SqlFilterBuilder<ViewMeetingRoomAvailabilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityFilterBuilder class.
		/// </summary>
		public ViewMeetingRoomAvailabilityFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewMeetingRoomAvailabilityFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewMeetingRoomAvailabilityFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewMeetingRoomAvailabilityFilterBuilder

	#region ViewMeetingRoomAvailabilityParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewMeetingRoomAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewMeetingRoomAvailabilityParameterBuilder : ParameterizedSqlFilterBuilder<ViewMeetingRoomAvailabilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityParameterBuilder class.
		/// </summary>
		public ViewMeetingRoomAvailabilityParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewMeetingRoomAvailabilityParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewMeetingRoomAvailabilityParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewMeetingRoomAvailabilityParameterBuilder
	
	#region ViewMeetingRoomAvailabilitySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewMeetingRoomAvailability"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewMeetingRoomAvailabilitySortBuilder : SqlSortBuilder<ViewMeetingRoomAvailabilityColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilitySqlSortBuilder class.
		/// </summary>
		public ViewMeetingRoomAvailabilitySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewMeetingRoomAvailabilitySortBuilder

} // end namespace
