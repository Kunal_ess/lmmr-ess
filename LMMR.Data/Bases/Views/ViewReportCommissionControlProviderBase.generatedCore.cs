﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportCommissionControlProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportCommissionControlProviderBaseCore : EntityViewProviderBase<ViewReportCommissionControl>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportCommissionControl&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportCommissionControl&gt;"/></returns>
		protected static VList&lt;ViewReportCommissionControl&gt; Fill(DataSet dataSet, VList<ViewReportCommissionControl> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportCommissionControl>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportCommissionControl&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportCommissionControl>"/></returns>
		protected static VList&lt;ViewReportCommissionControl&gt; Fill(DataTable dataTable, VList<ViewReportCommissionControl> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportCommissionControl c = new ViewReportCommissionControl();
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityID"]))?(long)0:(System.Int64)row["CityID"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.HotelId = (Convert.IsDBNull(row["HotelID"]))?(long)0:(System.Int64)row["HotelID"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.BookingDate = (Convert.IsDBNull(row["BookingDate"]))?DateTime.MinValue:(System.DateTime?)row["BookingDate"];
					c.InitialValue = (Convert.IsDBNull(row["InitialValue"]))?0.0m:(System.Decimal?)row["InitialValue"];
					c.InitialComm = (Convert.IsDBNull(row["InitialComm"]))?0.0m:(System.Decimal?)row["InitialComm"];
					c.ActualValue = (Convert.IsDBNull(row["ActualValue"]))?0.0m:(System.Decimal?)row["ActualValue"];
					c.ActualComm = (Convert.IsDBNull(row["ActualComm"]))?0.0m:(System.Decimal?)row["ActualComm"];
					c.RequestStatus = (Convert.IsDBNull(row["RequestStatus"]))?(int)0:(System.Int32?)row["RequestStatus"];
					c.BookType = (Convert.IsDBNull(row["BookType"]))?(int)0:(System.Int32?)row["BookType"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportCommissionControl&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportCommissionControl&gt;"/></returns>
		protected VList<ViewReportCommissionControl> Fill(IDataReader reader, VList<ViewReportCommissionControl> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportCommissionControl entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportCommissionControl>("ViewReportCommissionControl",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportCommissionControl();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CountryId = (System.Int64)reader[((int)ViewReportCommissionControlColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.CountryName)))?null:(System.String)reader[((int)ViewReportCommissionControlColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewReportCommissionControlColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
					entity.City = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.City)))?null:(System.String)reader[((int)ViewReportCommissionControlColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.HotelId = (System.Int64)reader[((int)ViewReportCommissionControlColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
					entity.HotelName = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.HotelName)))?null:(System.String)reader[((int)ViewReportCommissionControlColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.BookingDate = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewReportCommissionControlColumn.BookingDate)];
					//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
					entity.InitialValue = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.InitialValue)))?null:(System.Decimal?)reader[((int)ViewReportCommissionControlColumn.InitialValue)];
					//entity.InitialValue = (Convert.IsDBNull(reader["InitialValue"]))?0.0m:(System.Decimal?)reader["InitialValue"];
					entity.InitialComm = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.InitialComm)))?null:(System.Decimal?)reader[((int)ViewReportCommissionControlColumn.InitialComm)];
					//entity.InitialComm = (Convert.IsDBNull(reader["InitialComm"]))?0.0m:(System.Decimal?)reader["InitialComm"];
					entity.ActualValue = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.ActualValue)))?null:(System.Decimal?)reader[((int)ViewReportCommissionControlColumn.ActualValue)];
					//entity.ActualValue = (Convert.IsDBNull(reader["ActualValue"]))?0.0m:(System.Decimal?)reader["ActualValue"];
					entity.ActualComm = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.ActualComm)))?null:(System.Decimal?)reader[((int)ViewReportCommissionControlColumn.ActualComm)];
					//entity.ActualComm = (Convert.IsDBNull(reader["ActualComm"]))?0.0m:(System.Decimal?)reader["ActualComm"];
					entity.RequestStatus = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewReportCommissionControlColumn.RequestStatus)];
					//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
					entity.BookType = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.BookType)))?null:(System.Int32?)reader[((int)ViewReportCommissionControlColumn.BookType)];
					//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportCommissionControl"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportCommissionControl"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportCommissionControl entity)
		{
			reader.Read();
			entity.CountryId = (System.Int64)reader[((int)ViewReportCommissionControlColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.CountryName)))?null:(System.String)reader[((int)ViewReportCommissionControlColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewReportCommissionControlColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
			entity.City = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.City)))?null:(System.String)reader[((int)ViewReportCommissionControlColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.HotelId = (System.Int64)reader[((int)ViewReportCommissionControlColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
			entity.HotelName = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.HotelName)))?null:(System.String)reader[((int)ViewReportCommissionControlColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.BookingDate = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewReportCommissionControlColumn.BookingDate)];
			//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
			entity.InitialValue = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.InitialValue)))?null:(System.Decimal?)reader[((int)ViewReportCommissionControlColumn.InitialValue)];
			//entity.InitialValue = (Convert.IsDBNull(reader["InitialValue"]))?0.0m:(System.Decimal?)reader["InitialValue"];
			entity.InitialComm = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.InitialComm)))?null:(System.Decimal?)reader[((int)ViewReportCommissionControlColumn.InitialComm)];
			//entity.InitialComm = (Convert.IsDBNull(reader["InitialComm"]))?0.0m:(System.Decimal?)reader["InitialComm"];
			entity.ActualValue = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.ActualValue)))?null:(System.Decimal?)reader[((int)ViewReportCommissionControlColumn.ActualValue)];
			//entity.ActualValue = (Convert.IsDBNull(reader["ActualValue"]))?0.0m:(System.Decimal?)reader["ActualValue"];
			entity.ActualComm = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.ActualComm)))?null:(System.Decimal?)reader[((int)ViewReportCommissionControlColumn.ActualComm)];
			//entity.ActualComm = (Convert.IsDBNull(reader["ActualComm"]))?0.0m:(System.Decimal?)reader["ActualComm"];
			entity.RequestStatus = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewReportCommissionControlColumn.RequestStatus)];
			//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
			entity.BookType = (reader.IsDBNull(((int)ViewReportCommissionControlColumn.BookType)))?null:(System.Int32?)reader[((int)ViewReportCommissionControlColumn.BookType)];
			//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportCommissionControl"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportCommissionControl"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportCommissionControl entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityID"]))?(long)0:(System.Int64)dataRow["CityID"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelID"]))?(long)0:(System.Int64)dataRow["HotelID"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.BookingDate = (Convert.IsDBNull(dataRow["BookingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BookingDate"];
			entity.InitialValue = (Convert.IsDBNull(dataRow["InitialValue"]))?0.0m:(System.Decimal?)dataRow["InitialValue"];
			entity.InitialComm = (Convert.IsDBNull(dataRow["InitialComm"]))?0.0m:(System.Decimal?)dataRow["InitialComm"];
			entity.ActualValue = (Convert.IsDBNull(dataRow["ActualValue"]))?0.0m:(System.Decimal?)dataRow["ActualValue"];
			entity.ActualComm = (Convert.IsDBNull(dataRow["ActualComm"]))?0.0m:(System.Decimal?)dataRow["ActualComm"];
			entity.RequestStatus = (Convert.IsDBNull(dataRow["RequestStatus"]))?(int)0:(System.Int32?)dataRow["RequestStatus"];
			entity.BookType = (Convert.IsDBNull(dataRow["BookType"]))?(int)0:(System.Int32?)dataRow["BookType"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportCommissionControlFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportCommissionControl"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportCommissionControlFilterBuilder : SqlFilterBuilder<ViewReportCommissionControlColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlFilterBuilder class.
		/// </summary>
		public ViewReportCommissionControlFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportCommissionControlFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportCommissionControlFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportCommissionControlFilterBuilder

	#region ViewReportCommissionControlParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportCommissionControl"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportCommissionControlParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportCommissionControlColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlParameterBuilder class.
		/// </summary>
		public ViewReportCommissionControlParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportCommissionControlParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportCommissionControlParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportCommissionControlParameterBuilder
	
	#region ViewReportCommissionControlSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportCommissionControl"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportCommissionControlSortBuilder : SqlSortBuilder<ViewReportCommissionControlColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlSqlSortBuilder class.
		/// </summary>
		public ViewReportCommissionControlSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportCommissionControlSortBuilder

} // end namespace
