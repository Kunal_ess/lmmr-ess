﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewFacilityIconProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewFacilityIconProviderBaseCore : EntityViewProviderBase<ViewFacilityIcon>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewFacilityIcon&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewFacilityIcon&gt;"/></returns>
		protected static VList&lt;ViewFacilityIcon&gt; Fill(DataSet dataSet, VList<ViewFacilityIcon> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewFacilityIcon>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewFacilityIcon&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewFacilityIcon>"/></returns>
		protected static VList&lt;ViewFacilityIcon&gt; Fill(DataTable dataTable, VList<ViewFacilityIcon> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewFacilityIcon c = new ViewFacilityIcon();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.FacilityTypeId = (Convert.IsDBNull(row["FacilityTypeID"]))?(long)0:(System.Int64)row["FacilityTypeID"];
					c.FacilityName = (Convert.IsDBNull(row["FacilityName"]))?string.Empty:(System.String)row["FacilityName"];
					c.Icon = (Convert.IsDBNull(row["Icon"]))?string.Empty:(System.String)row["Icon"];
					c.HotelId = (Convert.IsDBNull(row["Hotel_Id"]))?(long)0:(System.Int64)row["Hotel_Id"];
					c.FacilitiesId = (Convert.IsDBNull(row["FacilitiesId"]))?(long)0:(System.Int64)row["FacilitiesId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewFacilityIcon&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewFacilityIcon&gt;"/></returns>
		protected VList<ViewFacilityIcon> Fill(IDataReader reader, VList<ViewFacilityIcon> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewFacilityIcon entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewFacilityIcon>("ViewFacilityIcon",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewFacilityIcon();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewFacilityIconColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.FacilityTypeId = (System.Int64)reader[((int)ViewFacilityIconColumn.FacilityTypeId)];
					//entity.FacilityTypeId = (Convert.IsDBNull(reader["FacilityTypeID"]))?(long)0:(System.Int64)reader["FacilityTypeID"];
					entity.FacilityName = (reader.IsDBNull(((int)ViewFacilityIconColumn.FacilityName)))?null:(System.String)reader[((int)ViewFacilityIconColumn.FacilityName)];
					//entity.FacilityName = (Convert.IsDBNull(reader["FacilityName"]))?string.Empty:(System.String)reader["FacilityName"];
					entity.Icon = (reader.IsDBNull(((int)ViewFacilityIconColumn.Icon)))?null:(System.String)reader[((int)ViewFacilityIconColumn.Icon)];
					//entity.Icon = (Convert.IsDBNull(reader["Icon"]))?string.Empty:(System.String)reader["Icon"];
					entity.HotelId = (System.Int64)reader[((int)ViewFacilityIconColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["Hotel_Id"]))?(long)0:(System.Int64)reader["Hotel_Id"];
					entity.FacilitiesId = (System.Int64)reader[((int)ViewFacilityIconColumn.FacilitiesId)];
					//entity.FacilitiesId = (Convert.IsDBNull(reader["FacilitiesId"]))?(long)0:(System.Int64)reader["FacilitiesId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewFacilityIcon"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewFacilityIcon"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewFacilityIcon entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewFacilityIconColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.FacilityTypeId = (System.Int64)reader[((int)ViewFacilityIconColumn.FacilityTypeId)];
			//entity.FacilityTypeId = (Convert.IsDBNull(reader["FacilityTypeID"]))?(long)0:(System.Int64)reader["FacilityTypeID"];
			entity.FacilityName = (reader.IsDBNull(((int)ViewFacilityIconColumn.FacilityName)))?null:(System.String)reader[((int)ViewFacilityIconColumn.FacilityName)];
			//entity.FacilityName = (Convert.IsDBNull(reader["FacilityName"]))?string.Empty:(System.String)reader["FacilityName"];
			entity.Icon = (reader.IsDBNull(((int)ViewFacilityIconColumn.Icon)))?null:(System.String)reader[((int)ViewFacilityIconColumn.Icon)];
			//entity.Icon = (Convert.IsDBNull(reader["Icon"]))?string.Empty:(System.String)reader["Icon"];
			entity.HotelId = (System.Int64)reader[((int)ViewFacilityIconColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["Hotel_Id"]))?(long)0:(System.Int64)reader["Hotel_Id"];
			entity.FacilitiesId = (System.Int64)reader[((int)ViewFacilityIconColumn.FacilitiesId)];
			//entity.FacilitiesId = (Convert.IsDBNull(reader["FacilitiesId"]))?(long)0:(System.Int64)reader["FacilitiesId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewFacilityIcon"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewFacilityIcon"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewFacilityIcon entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.FacilityTypeId = (Convert.IsDBNull(dataRow["FacilityTypeID"]))?(long)0:(System.Int64)dataRow["FacilityTypeID"];
			entity.FacilityName = (Convert.IsDBNull(dataRow["FacilityName"]))?string.Empty:(System.String)dataRow["FacilityName"];
			entity.Icon = (Convert.IsDBNull(dataRow["Icon"]))?string.Empty:(System.String)dataRow["Icon"];
			entity.HotelId = (Convert.IsDBNull(dataRow["Hotel_Id"]))?(long)0:(System.Int64)dataRow["Hotel_Id"];
			entity.FacilitiesId = (Convert.IsDBNull(dataRow["FacilitiesId"]))?(long)0:(System.Int64)dataRow["FacilitiesId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewFacilityIconFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFacilityIcon"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFacilityIconFilterBuilder : SqlFilterBuilder<ViewFacilityIconColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconFilterBuilder class.
		/// </summary>
		public ViewFacilityIconFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFacilityIconFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFacilityIconFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFacilityIconFilterBuilder

	#region ViewFacilityIconParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFacilityIcon"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFacilityIconParameterBuilder : ParameterizedSqlFilterBuilder<ViewFacilityIconColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconParameterBuilder class.
		/// </summary>
		public ViewFacilityIconParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFacilityIconParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFacilityIconParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFacilityIconParameterBuilder
	
	#region ViewFacilityIconSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFacilityIcon"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewFacilityIconSortBuilder : SqlSortBuilder<ViewFacilityIconColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconSqlSortBuilder class.
		/// </summary>
		public ViewFacilityIconSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewFacilityIconSortBuilder

} // end namespace
