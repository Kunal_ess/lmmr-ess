﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewRequestProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewRequestProviderBaseCore : EntityViewProviderBase<ViewRequest>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewRequest&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewRequest&gt;"/></returns>
		protected static VList&lt;ViewRequest&gt; Fill(DataSet dataSet, VList<ViewRequest> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewRequest>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewRequest&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewRequest>"/></returns>
		protected static VList&lt;ViewRequest&gt; Fill(DataTable dataTable, VList<ViewRequest> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewRequest c = new ViewRequest();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.Usertype = (Convert.IsDBNull(row["Usertype"]))?string.Empty:(System.String)row["Usertype"];
					c.TypeUser = (Convert.IsDBNull(row["TypeUser"]))?string.Empty:(System.String)row["TypeUser"];
					c.Contact = (Convert.IsDBNull(row["Contact"]))?string.Empty:(System.String)row["Contact"];
					c.FinalTotalPrice = (Convert.IsDBNull(row["FinalTotalPrice"]))?0.0m:(System.Decimal?)row["FinalTotalPrice"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.MainMeetingRoomId = (Convert.IsDBNull(row["MainMeetingRoomId"]))?(long)0:(System.Int64)row["MainMeetingRoomId"];
					c.BookingDate = (Convert.IsDBNull(row["BookingDate"]))?DateTime.MinValue:(System.DateTime?)row["BookingDate"];
					c.ArrivalDate = (Convert.IsDBNull(row["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)row["ArrivalDate"];
					c.DepartureDate = (Convert.IsDBNull(row["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)row["DepartureDate"];
					c.Duration = (Convert.IsDBNull(row["Duration"]))?(long)0:(System.Int64?)row["Duration"];
					c.CurrencyId = (Convert.IsDBNull(row["CurrencyID"]))?(long)0:(System.Int64?)row["CurrencyID"];
					c.MeetingRoomId = (Convert.IsDBNull(row["MeetingRoomId"]))?(long)0:(System.Int64)row["MeetingRoomId"];
					c.MeetingDay = (Convert.IsDBNull(row["MeetingDay"]))?(int)0:(System.Int32?)row["MeetingDay"];
					c.NoofParticipants = (Convert.IsDBNull(row["NoofParticipants"]))?(long)0:(System.Int64?)row["NoofParticipants"];
					c.MeetingDate = (Convert.IsDBNull(row["MeetingDate"]))?DateTime.MinValue:(System.DateTime?)row["MeetingDate"];
					c.TotalPrice = (Convert.IsDBNull(row["TotalPrice"]))?0.0m:(System.Decimal?)row["TotalPrice"];
					c.LastName = (Convert.IsDBNull(row["LastName"]))?string.Empty:(System.String)row["LastName"];
					c.EmailId = (Convert.IsDBNull(row["EmailId"]))?string.Empty:(System.String)row["EmailId"];
					c.RequestStatus = (Convert.IsDBNull(row["RequestStatus"]))?(int)0:(System.Int32?)row["RequestStatus"];
					c.IsUserBookingProcessDone = (Convert.IsDBNull(row["IsUserBookingProcessDone"]))?false:(System.Boolean)row["IsUserBookingProcessDone"];
					c.RevenueAmount = (Convert.IsDBNull(row["RevenueAmount"]))?0.0m:(System.Decimal?)row["RevenueAmount"];
					c.RevenueReason = (Convert.IsDBNull(row["RevenueReason"]))?string.Empty:(System.String)row["RevenueReason"];
					c.ComissionSubmitDate = (Convert.IsDBNull(row["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)row["ComissionSubmitDate"];
					c.AgencyUserId = (Convert.IsDBNull(row["AgencyUserId"]))?(long)0:(System.Int64?)row["AgencyUserId"];
					c.AgencyClientId = (Convert.IsDBNull(row["AgencyClientId"]))?(long)0:(System.Int64?)row["AgencyClientId"];
					c.IsBookedByAgencyUser = (Convert.IsDBNull(row["IsBookedByAgencyUser"]))?false:(System.Boolean?)row["IsBookedByAgencyUser"];
					c.CreatorId = (Convert.IsDBNull(row["CreatorId"]))?(long)0:(System.Int64)row["CreatorId"];
					c.IsPackageSelected = (Convert.IsDBNull(row["IsPackageSelected"]))?false:(System.Boolean)row["IsPackageSelected"];
					c.Accomodation = (Convert.IsDBNull(row["Accomodation"]))?(int)0:(System.Int32?)row["Accomodation"];
					c.IsBedroom = (Convert.IsDBNull(row["IsBedroom"]))?false:(System.Boolean)row["IsBedroom"];
					c.SpecialRequest = (Convert.IsDBNull(row["SpecialRequest"]))?string.Empty:(System.String)row["SpecialRequest"];
					c.IsCancled = (Convert.IsDBNull(row["IsCancled"]))?false:(System.Boolean?)row["IsCancled"];
					c.IsComissionDone = (Convert.IsDBNull(row["IsComissionDone"]))?false:(System.Boolean?)row["IsComissionDone"];
					c.BookType = (Convert.IsDBNull(row["BookType"]))?(int)0:(System.Int32?)row["BookType"];
					c.FrozenDate = (Convert.IsDBNull(row["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)row["FrozenDate"];
					c.IsFrozen = (Convert.IsDBNull(row["IsFrozen"]))?false:(System.Boolean)row["IsFrozen"];
					c.MeetingRoomConfigId = (Convert.IsDBNull(row["MeetingRoomConfigId"]))?(long)0:(System.Int64)row["MeetingRoomConfigId"];
					c.Day = (Convert.IsDBNull(row["day"]))?string.Empty:(System.String)row["day"];
					c.EndTime = (Convert.IsDBNull(row["EndTime"]))?string.Empty:(System.String)row["EndTime"];
					c.StartTime = (Convert.IsDBNull(row["StartTime"]))?string.Empty:(System.String)row["StartTime"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewRequest&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewRequest&gt;"/></returns>
		protected VList<ViewRequest> Fill(IDataReader reader, VList<ViewRequest> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewRequest entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewRequest>("ViewRequest",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewRequest();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewRequestColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.Name = (reader.IsDBNull(((int)ViewRequestColumn.Name)))?null:(System.String)reader[((int)ViewRequestColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.Usertype = (System.String)reader[((int)ViewRequestColumn.Usertype)];
					//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?string.Empty:(System.String)reader["Usertype"];
					entity.TypeUser = (System.String)reader[((int)ViewRequestColumn.TypeUser)];
					//entity.TypeUser = (Convert.IsDBNull(reader["TypeUser"]))?string.Empty:(System.String)reader["TypeUser"];
					entity.Contact = (reader.IsDBNull(((int)ViewRequestColumn.Contact)))?null:(System.String)reader[((int)ViewRequestColumn.Contact)];
					//entity.Contact = (Convert.IsDBNull(reader["Contact"]))?string.Empty:(System.String)reader["Contact"];
					entity.FinalTotalPrice = (reader.IsDBNull(((int)ViewRequestColumn.FinalTotalPrice)))?null:(System.Decimal?)reader[((int)ViewRequestColumn.FinalTotalPrice)];
					//entity.FinalTotalPrice = (Convert.IsDBNull(reader["FinalTotalPrice"]))?0.0m:(System.Decimal?)reader["FinalTotalPrice"];
					entity.HotelId = (System.Int64)reader[((int)ViewRequestColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.MainMeetingRoomId = (System.Int64)reader[((int)ViewRequestColumn.MainMeetingRoomId)];
					//entity.MainMeetingRoomId = (Convert.IsDBNull(reader["MainMeetingRoomId"]))?(long)0:(System.Int64)reader["MainMeetingRoomId"];
					entity.BookingDate = (reader.IsDBNull(((int)ViewRequestColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.BookingDate)];
					//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
					entity.ArrivalDate = (reader.IsDBNull(((int)ViewRequestColumn.ArrivalDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.ArrivalDate)];
					//entity.ArrivalDate = (Convert.IsDBNull(reader["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)reader["ArrivalDate"];
					entity.DepartureDate = (reader.IsDBNull(((int)ViewRequestColumn.DepartureDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.DepartureDate)];
					//entity.DepartureDate = (Convert.IsDBNull(reader["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)reader["DepartureDate"];
					entity.Duration = (reader.IsDBNull(((int)ViewRequestColumn.Duration)))?null:(System.Int64?)reader[((int)ViewRequestColumn.Duration)];
					//entity.Duration = (Convert.IsDBNull(reader["Duration"]))?(long)0:(System.Int64?)reader["Duration"];
					entity.CurrencyId = (reader.IsDBNull(((int)ViewRequestColumn.CurrencyId)))?null:(System.Int64?)reader[((int)ViewRequestColumn.CurrencyId)];
					//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyID"]))?(long)0:(System.Int64?)reader["CurrencyID"];
					entity.MeetingRoomId = (System.Int64)reader[((int)ViewRequestColumn.MeetingRoomId)];
					//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomId"]))?(long)0:(System.Int64)reader["MeetingRoomId"];
					entity.MeetingDay = (reader.IsDBNull(((int)ViewRequestColumn.MeetingDay)))?null:(System.Int32?)reader[((int)ViewRequestColumn.MeetingDay)];
					//entity.MeetingDay = (Convert.IsDBNull(reader["MeetingDay"]))?(int)0:(System.Int32?)reader["MeetingDay"];
					entity.NoofParticipants = (reader.IsDBNull(((int)ViewRequestColumn.NoofParticipants)))?null:(System.Int64?)reader[((int)ViewRequestColumn.NoofParticipants)];
					//entity.NoofParticipants = (Convert.IsDBNull(reader["NoofParticipants"]))?(long)0:(System.Int64?)reader["NoofParticipants"];
					entity.MeetingDate = (reader.IsDBNull(((int)ViewRequestColumn.MeetingDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.MeetingDate)];
					//entity.MeetingDate = (Convert.IsDBNull(reader["MeetingDate"]))?DateTime.MinValue:(System.DateTime?)reader["MeetingDate"];
					entity.TotalPrice = (reader.IsDBNull(((int)ViewRequestColumn.TotalPrice)))?null:(System.Decimal?)reader[((int)ViewRequestColumn.TotalPrice)];
					//entity.TotalPrice = (Convert.IsDBNull(reader["TotalPrice"]))?0.0m:(System.Decimal?)reader["TotalPrice"];
					entity.LastName = (reader.IsDBNull(((int)ViewRequestColumn.LastName)))?null:(System.String)reader[((int)ViewRequestColumn.LastName)];
					//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
					entity.EmailId = (reader.IsDBNull(((int)ViewRequestColumn.EmailId)))?null:(System.String)reader[((int)ViewRequestColumn.EmailId)];
					//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
					entity.RequestStatus = (reader.IsDBNull(((int)ViewRequestColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewRequestColumn.RequestStatus)];
					//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
					entity.IsUserBookingProcessDone = (System.Boolean)reader[((int)ViewRequestColumn.IsUserBookingProcessDone)];
					//entity.IsUserBookingProcessDone = (Convert.IsDBNull(reader["IsUserBookingProcessDone"]))?false:(System.Boolean)reader["IsUserBookingProcessDone"];
					entity.RevenueAmount = (reader.IsDBNull(((int)ViewRequestColumn.RevenueAmount)))?null:(System.Decimal?)reader[((int)ViewRequestColumn.RevenueAmount)];
					//entity.RevenueAmount = (Convert.IsDBNull(reader["RevenueAmount"]))?0.0m:(System.Decimal?)reader["RevenueAmount"];
					entity.RevenueReason = (reader.IsDBNull(((int)ViewRequestColumn.RevenueReason)))?null:(System.String)reader[((int)ViewRequestColumn.RevenueReason)];
					//entity.RevenueReason = (Convert.IsDBNull(reader["RevenueReason"]))?string.Empty:(System.String)reader["RevenueReason"];
					entity.ComissionSubmitDate = (reader.IsDBNull(((int)ViewRequestColumn.ComissionSubmitDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.ComissionSubmitDate)];
					//entity.ComissionSubmitDate = (Convert.IsDBNull(reader["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)reader["ComissionSubmitDate"];
					entity.AgencyUserId = (reader.IsDBNull(((int)ViewRequestColumn.AgencyUserId)))?null:(System.Int64?)reader[((int)ViewRequestColumn.AgencyUserId)];
					//entity.AgencyUserId = (Convert.IsDBNull(reader["AgencyUserId"]))?(long)0:(System.Int64?)reader["AgencyUserId"];
					entity.AgencyClientId = (reader.IsDBNull(((int)ViewRequestColumn.AgencyClientId)))?null:(System.Int64?)reader[((int)ViewRequestColumn.AgencyClientId)];
					//entity.AgencyClientId = (Convert.IsDBNull(reader["AgencyClientId"]))?(long)0:(System.Int64?)reader["AgencyClientId"];
					entity.IsBookedByAgencyUser = (reader.IsDBNull(((int)ViewRequestColumn.IsBookedByAgencyUser)))?null:(System.Boolean?)reader[((int)ViewRequestColumn.IsBookedByAgencyUser)];
					//entity.IsBookedByAgencyUser = (Convert.IsDBNull(reader["IsBookedByAgencyUser"]))?false:(System.Boolean?)reader["IsBookedByAgencyUser"];
					entity.CreatorId = (System.Int64)reader[((int)ViewRequestColumn.CreatorId)];
					//entity.CreatorId = (Convert.IsDBNull(reader["CreatorId"]))?(long)0:(System.Int64)reader["CreatorId"];
					entity.IsPackageSelected = (System.Boolean)reader[((int)ViewRequestColumn.IsPackageSelected)];
					//entity.IsPackageSelected = (Convert.IsDBNull(reader["IsPackageSelected"]))?false:(System.Boolean)reader["IsPackageSelected"];
					entity.Accomodation = (reader.IsDBNull(((int)ViewRequestColumn.Accomodation)))?null:(System.Int32?)reader[((int)ViewRequestColumn.Accomodation)];
					//entity.Accomodation = (Convert.IsDBNull(reader["Accomodation"]))?(int)0:(System.Int32?)reader["Accomodation"];
					entity.IsBedroom = (System.Boolean)reader[((int)ViewRequestColumn.IsBedroom)];
					//entity.IsBedroom = (Convert.IsDBNull(reader["IsBedroom"]))?false:(System.Boolean)reader["IsBedroom"];
					entity.SpecialRequest = (reader.IsDBNull(((int)ViewRequestColumn.SpecialRequest)))?null:(System.String)reader[((int)ViewRequestColumn.SpecialRequest)];
					//entity.SpecialRequest = (Convert.IsDBNull(reader["SpecialRequest"]))?string.Empty:(System.String)reader["SpecialRequest"];
					entity.IsCancled = (reader.IsDBNull(((int)ViewRequestColumn.IsCancled)))?null:(System.Boolean?)reader[((int)ViewRequestColumn.IsCancled)];
					//entity.IsCancled = (Convert.IsDBNull(reader["IsCancled"]))?false:(System.Boolean?)reader["IsCancled"];
					entity.IsComissionDone = (reader.IsDBNull(((int)ViewRequestColumn.IsComissionDone)))?null:(System.Boolean?)reader[((int)ViewRequestColumn.IsComissionDone)];
					//entity.IsComissionDone = (Convert.IsDBNull(reader["IsComissionDone"]))?false:(System.Boolean?)reader["IsComissionDone"];
					entity.BookType = (reader.IsDBNull(((int)ViewRequestColumn.BookType)))?null:(System.Int32?)reader[((int)ViewRequestColumn.BookType)];
					//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
					entity.FrozenDate = (reader.IsDBNull(((int)ViewRequestColumn.FrozenDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.FrozenDate)];
					//entity.FrozenDate = (Convert.IsDBNull(reader["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)reader["FrozenDate"];
					entity.IsFrozen = (System.Boolean)reader[((int)ViewRequestColumn.IsFrozen)];
					//entity.IsFrozen = (Convert.IsDBNull(reader["IsFrozen"]))?false:(System.Boolean)reader["IsFrozen"];
					entity.MeetingRoomConfigId = (System.Int64)reader[((int)ViewRequestColumn.MeetingRoomConfigId)];
					//entity.MeetingRoomConfigId = (Convert.IsDBNull(reader["MeetingRoomConfigId"]))?(long)0:(System.Int64)reader["MeetingRoomConfigId"];
					entity.Day = (reader.IsDBNull(((int)ViewRequestColumn.Day)))?null:(System.String)reader[((int)ViewRequestColumn.Day)];
					//entity.Day = (Convert.IsDBNull(reader["day"]))?string.Empty:(System.String)reader["day"];
					entity.EndTime = (reader.IsDBNull(((int)ViewRequestColumn.EndTime)))?null:(System.String)reader[((int)ViewRequestColumn.EndTime)];
					//entity.EndTime = (Convert.IsDBNull(reader["EndTime"]))?string.Empty:(System.String)reader["EndTime"];
					entity.StartTime = (reader.IsDBNull(((int)ViewRequestColumn.StartTime)))?null:(System.String)reader[((int)ViewRequestColumn.StartTime)];
					//entity.StartTime = (Convert.IsDBNull(reader["StartTime"]))?string.Empty:(System.String)reader["StartTime"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewRequest"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewRequest"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewRequest entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewRequestColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.Name = (reader.IsDBNull(((int)ViewRequestColumn.Name)))?null:(System.String)reader[((int)ViewRequestColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.Usertype = (System.String)reader[((int)ViewRequestColumn.Usertype)];
			//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?string.Empty:(System.String)reader["Usertype"];
			entity.TypeUser = (System.String)reader[((int)ViewRequestColumn.TypeUser)];
			//entity.TypeUser = (Convert.IsDBNull(reader["TypeUser"]))?string.Empty:(System.String)reader["TypeUser"];
			entity.Contact = (reader.IsDBNull(((int)ViewRequestColumn.Contact)))?null:(System.String)reader[((int)ViewRequestColumn.Contact)];
			//entity.Contact = (Convert.IsDBNull(reader["Contact"]))?string.Empty:(System.String)reader["Contact"];
			entity.FinalTotalPrice = (reader.IsDBNull(((int)ViewRequestColumn.FinalTotalPrice)))?null:(System.Decimal?)reader[((int)ViewRequestColumn.FinalTotalPrice)];
			//entity.FinalTotalPrice = (Convert.IsDBNull(reader["FinalTotalPrice"]))?0.0m:(System.Decimal?)reader["FinalTotalPrice"];
			entity.HotelId = (System.Int64)reader[((int)ViewRequestColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.MainMeetingRoomId = (System.Int64)reader[((int)ViewRequestColumn.MainMeetingRoomId)];
			//entity.MainMeetingRoomId = (Convert.IsDBNull(reader["MainMeetingRoomId"]))?(long)0:(System.Int64)reader["MainMeetingRoomId"];
			entity.BookingDate = (reader.IsDBNull(((int)ViewRequestColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.BookingDate)];
			//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
			entity.ArrivalDate = (reader.IsDBNull(((int)ViewRequestColumn.ArrivalDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.ArrivalDate)];
			//entity.ArrivalDate = (Convert.IsDBNull(reader["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)reader["ArrivalDate"];
			entity.DepartureDate = (reader.IsDBNull(((int)ViewRequestColumn.DepartureDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.DepartureDate)];
			//entity.DepartureDate = (Convert.IsDBNull(reader["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)reader["DepartureDate"];
			entity.Duration = (reader.IsDBNull(((int)ViewRequestColumn.Duration)))?null:(System.Int64?)reader[((int)ViewRequestColumn.Duration)];
			//entity.Duration = (Convert.IsDBNull(reader["Duration"]))?(long)0:(System.Int64?)reader["Duration"];
			entity.CurrencyId = (reader.IsDBNull(((int)ViewRequestColumn.CurrencyId)))?null:(System.Int64?)reader[((int)ViewRequestColumn.CurrencyId)];
			//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyID"]))?(long)0:(System.Int64?)reader["CurrencyID"];
			entity.MeetingRoomId = (System.Int64)reader[((int)ViewRequestColumn.MeetingRoomId)];
			//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomId"]))?(long)0:(System.Int64)reader["MeetingRoomId"];
			entity.MeetingDay = (reader.IsDBNull(((int)ViewRequestColumn.MeetingDay)))?null:(System.Int32?)reader[((int)ViewRequestColumn.MeetingDay)];
			//entity.MeetingDay = (Convert.IsDBNull(reader["MeetingDay"]))?(int)0:(System.Int32?)reader["MeetingDay"];
			entity.NoofParticipants = (reader.IsDBNull(((int)ViewRequestColumn.NoofParticipants)))?null:(System.Int64?)reader[((int)ViewRequestColumn.NoofParticipants)];
			//entity.NoofParticipants = (Convert.IsDBNull(reader["NoofParticipants"]))?(long)0:(System.Int64?)reader["NoofParticipants"];
			entity.MeetingDate = (reader.IsDBNull(((int)ViewRequestColumn.MeetingDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.MeetingDate)];
			//entity.MeetingDate = (Convert.IsDBNull(reader["MeetingDate"]))?DateTime.MinValue:(System.DateTime?)reader["MeetingDate"];
			entity.TotalPrice = (reader.IsDBNull(((int)ViewRequestColumn.TotalPrice)))?null:(System.Decimal?)reader[((int)ViewRequestColumn.TotalPrice)];
			//entity.TotalPrice = (Convert.IsDBNull(reader["TotalPrice"]))?0.0m:(System.Decimal?)reader["TotalPrice"];
			entity.LastName = (reader.IsDBNull(((int)ViewRequestColumn.LastName)))?null:(System.String)reader[((int)ViewRequestColumn.LastName)];
			//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
			entity.EmailId = (reader.IsDBNull(((int)ViewRequestColumn.EmailId)))?null:(System.String)reader[((int)ViewRequestColumn.EmailId)];
			//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
			entity.RequestStatus = (reader.IsDBNull(((int)ViewRequestColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewRequestColumn.RequestStatus)];
			//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
			entity.IsUserBookingProcessDone = (System.Boolean)reader[((int)ViewRequestColumn.IsUserBookingProcessDone)];
			//entity.IsUserBookingProcessDone = (Convert.IsDBNull(reader["IsUserBookingProcessDone"]))?false:(System.Boolean)reader["IsUserBookingProcessDone"];
			entity.RevenueAmount = (reader.IsDBNull(((int)ViewRequestColumn.RevenueAmount)))?null:(System.Decimal?)reader[((int)ViewRequestColumn.RevenueAmount)];
			//entity.RevenueAmount = (Convert.IsDBNull(reader["RevenueAmount"]))?0.0m:(System.Decimal?)reader["RevenueAmount"];
			entity.RevenueReason = (reader.IsDBNull(((int)ViewRequestColumn.RevenueReason)))?null:(System.String)reader[((int)ViewRequestColumn.RevenueReason)];
			//entity.RevenueReason = (Convert.IsDBNull(reader["RevenueReason"]))?string.Empty:(System.String)reader["RevenueReason"];
			entity.ComissionSubmitDate = (reader.IsDBNull(((int)ViewRequestColumn.ComissionSubmitDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.ComissionSubmitDate)];
			//entity.ComissionSubmitDate = (Convert.IsDBNull(reader["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)reader["ComissionSubmitDate"];
			entity.AgencyUserId = (reader.IsDBNull(((int)ViewRequestColumn.AgencyUserId)))?null:(System.Int64?)reader[((int)ViewRequestColumn.AgencyUserId)];
			//entity.AgencyUserId = (Convert.IsDBNull(reader["AgencyUserId"]))?(long)0:(System.Int64?)reader["AgencyUserId"];
			entity.AgencyClientId = (reader.IsDBNull(((int)ViewRequestColumn.AgencyClientId)))?null:(System.Int64?)reader[((int)ViewRequestColumn.AgencyClientId)];
			//entity.AgencyClientId = (Convert.IsDBNull(reader["AgencyClientId"]))?(long)0:(System.Int64?)reader["AgencyClientId"];
			entity.IsBookedByAgencyUser = (reader.IsDBNull(((int)ViewRequestColumn.IsBookedByAgencyUser)))?null:(System.Boolean?)reader[((int)ViewRequestColumn.IsBookedByAgencyUser)];
			//entity.IsBookedByAgencyUser = (Convert.IsDBNull(reader["IsBookedByAgencyUser"]))?false:(System.Boolean?)reader["IsBookedByAgencyUser"];
			entity.CreatorId = (System.Int64)reader[((int)ViewRequestColumn.CreatorId)];
			//entity.CreatorId = (Convert.IsDBNull(reader["CreatorId"]))?(long)0:(System.Int64)reader["CreatorId"];
			entity.IsPackageSelected = (System.Boolean)reader[((int)ViewRequestColumn.IsPackageSelected)];
			//entity.IsPackageSelected = (Convert.IsDBNull(reader["IsPackageSelected"]))?false:(System.Boolean)reader["IsPackageSelected"];
			entity.Accomodation = (reader.IsDBNull(((int)ViewRequestColumn.Accomodation)))?null:(System.Int32?)reader[((int)ViewRequestColumn.Accomodation)];
			//entity.Accomodation = (Convert.IsDBNull(reader["Accomodation"]))?(int)0:(System.Int32?)reader["Accomodation"];
			entity.IsBedroom = (System.Boolean)reader[((int)ViewRequestColumn.IsBedroom)];
			//entity.IsBedroom = (Convert.IsDBNull(reader["IsBedroom"]))?false:(System.Boolean)reader["IsBedroom"];
			entity.SpecialRequest = (reader.IsDBNull(((int)ViewRequestColumn.SpecialRequest)))?null:(System.String)reader[((int)ViewRequestColumn.SpecialRequest)];
			//entity.SpecialRequest = (Convert.IsDBNull(reader["SpecialRequest"]))?string.Empty:(System.String)reader["SpecialRequest"];
			entity.IsCancled = (reader.IsDBNull(((int)ViewRequestColumn.IsCancled)))?null:(System.Boolean?)reader[((int)ViewRequestColumn.IsCancled)];
			//entity.IsCancled = (Convert.IsDBNull(reader["IsCancled"]))?false:(System.Boolean?)reader["IsCancled"];
			entity.IsComissionDone = (reader.IsDBNull(((int)ViewRequestColumn.IsComissionDone)))?null:(System.Boolean?)reader[((int)ViewRequestColumn.IsComissionDone)];
			//entity.IsComissionDone = (Convert.IsDBNull(reader["IsComissionDone"]))?false:(System.Boolean?)reader["IsComissionDone"];
			entity.BookType = (reader.IsDBNull(((int)ViewRequestColumn.BookType)))?null:(System.Int32?)reader[((int)ViewRequestColumn.BookType)];
			//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
			entity.FrozenDate = (reader.IsDBNull(((int)ViewRequestColumn.FrozenDate)))?null:(System.DateTime?)reader[((int)ViewRequestColumn.FrozenDate)];
			//entity.FrozenDate = (Convert.IsDBNull(reader["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)reader["FrozenDate"];
			entity.IsFrozen = (System.Boolean)reader[((int)ViewRequestColumn.IsFrozen)];
			//entity.IsFrozen = (Convert.IsDBNull(reader["IsFrozen"]))?false:(System.Boolean)reader["IsFrozen"];
			entity.MeetingRoomConfigId = (System.Int64)reader[((int)ViewRequestColumn.MeetingRoomConfigId)];
			//entity.MeetingRoomConfigId = (Convert.IsDBNull(reader["MeetingRoomConfigId"]))?(long)0:(System.Int64)reader["MeetingRoomConfigId"];
			entity.Day = (reader.IsDBNull(((int)ViewRequestColumn.Day)))?null:(System.String)reader[((int)ViewRequestColumn.Day)];
			//entity.Day = (Convert.IsDBNull(reader["day"]))?string.Empty:(System.String)reader["day"];
			entity.EndTime = (reader.IsDBNull(((int)ViewRequestColumn.EndTime)))?null:(System.String)reader[((int)ViewRequestColumn.EndTime)];
			//entity.EndTime = (Convert.IsDBNull(reader["EndTime"]))?string.Empty:(System.String)reader["EndTime"];
			entity.StartTime = (reader.IsDBNull(((int)ViewRequestColumn.StartTime)))?null:(System.String)reader[((int)ViewRequestColumn.StartTime)];
			//entity.StartTime = (Convert.IsDBNull(reader["StartTime"]))?string.Empty:(System.String)reader["StartTime"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewRequest"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewRequest"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewRequest entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.Usertype = (Convert.IsDBNull(dataRow["Usertype"]))?string.Empty:(System.String)dataRow["Usertype"];
			entity.TypeUser = (Convert.IsDBNull(dataRow["TypeUser"]))?string.Empty:(System.String)dataRow["TypeUser"];
			entity.Contact = (Convert.IsDBNull(dataRow["Contact"]))?string.Empty:(System.String)dataRow["Contact"];
			entity.FinalTotalPrice = (Convert.IsDBNull(dataRow["FinalTotalPrice"]))?0.0m:(System.Decimal?)dataRow["FinalTotalPrice"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.MainMeetingRoomId = (Convert.IsDBNull(dataRow["MainMeetingRoomId"]))?(long)0:(System.Int64)dataRow["MainMeetingRoomId"];
			entity.BookingDate = (Convert.IsDBNull(dataRow["BookingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BookingDate"];
			entity.ArrivalDate = (Convert.IsDBNull(dataRow["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ArrivalDate"];
			entity.DepartureDate = (Convert.IsDBNull(dataRow["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["DepartureDate"];
			entity.Duration = (Convert.IsDBNull(dataRow["Duration"]))?(long)0:(System.Int64?)dataRow["Duration"];
			entity.CurrencyId = (Convert.IsDBNull(dataRow["CurrencyID"]))?(long)0:(System.Int64?)dataRow["CurrencyID"];
			entity.MeetingRoomId = (Convert.IsDBNull(dataRow["MeetingRoomId"]))?(long)0:(System.Int64)dataRow["MeetingRoomId"];
			entity.MeetingDay = (Convert.IsDBNull(dataRow["MeetingDay"]))?(int)0:(System.Int32?)dataRow["MeetingDay"];
			entity.NoofParticipants = (Convert.IsDBNull(dataRow["NoofParticipants"]))?(long)0:(System.Int64?)dataRow["NoofParticipants"];
			entity.MeetingDate = (Convert.IsDBNull(dataRow["MeetingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["MeetingDate"];
			entity.TotalPrice = (Convert.IsDBNull(dataRow["TotalPrice"]))?0.0m:(System.Decimal?)dataRow["TotalPrice"];
			entity.LastName = (Convert.IsDBNull(dataRow["LastName"]))?string.Empty:(System.String)dataRow["LastName"];
			entity.EmailId = (Convert.IsDBNull(dataRow["EmailId"]))?string.Empty:(System.String)dataRow["EmailId"];
			entity.RequestStatus = (Convert.IsDBNull(dataRow["RequestStatus"]))?(int)0:(System.Int32?)dataRow["RequestStatus"];
			entity.IsUserBookingProcessDone = (Convert.IsDBNull(dataRow["IsUserBookingProcessDone"]))?false:(System.Boolean)dataRow["IsUserBookingProcessDone"];
			entity.RevenueAmount = (Convert.IsDBNull(dataRow["RevenueAmount"]))?0.0m:(System.Decimal?)dataRow["RevenueAmount"];
			entity.RevenueReason = (Convert.IsDBNull(dataRow["RevenueReason"]))?string.Empty:(System.String)dataRow["RevenueReason"];
			entity.ComissionSubmitDate = (Convert.IsDBNull(dataRow["ComissionSubmitDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ComissionSubmitDate"];
			entity.AgencyUserId = (Convert.IsDBNull(dataRow["AgencyUserId"]))?(long)0:(System.Int64?)dataRow["AgencyUserId"];
			entity.AgencyClientId = (Convert.IsDBNull(dataRow["AgencyClientId"]))?(long)0:(System.Int64?)dataRow["AgencyClientId"];
			entity.IsBookedByAgencyUser = (Convert.IsDBNull(dataRow["IsBookedByAgencyUser"]))?false:(System.Boolean?)dataRow["IsBookedByAgencyUser"];
			entity.CreatorId = (Convert.IsDBNull(dataRow["CreatorId"]))?(long)0:(System.Int64)dataRow["CreatorId"];
			entity.IsPackageSelected = (Convert.IsDBNull(dataRow["IsPackageSelected"]))?false:(System.Boolean)dataRow["IsPackageSelected"];
			entity.Accomodation = (Convert.IsDBNull(dataRow["Accomodation"]))?(int)0:(System.Int32?)dataRow["Accomodation"];
			entity.IsBedroom = (Convert.IsDBNull(dataRow["IsBedroom"]))?false:(System.Boolean)dataRow["IsBedroom"];
			entity.SpecialRequest = (Convert.IsDBNull(dataRow["SpecialRequest"]))?string.Empty:(System.String)dataRow["SpecialRequest"];
			entity.IsCancled = (Convert.IsDBNull(dataRow["IsCancled"]))?false:(System.Boolean?)dataRow["IsCancled"];
			entity.IsComissionDone = (Convert.IsDBNull(dataRow["IsComissionDone"]))?false:(System.Boolean?)dataRow["IsComissionDone"];
			entity.BookType = (Convert.IsDBNull(dataRow["BookType"]))?(int)0:(System.Int32?)dataRow["BookType"];
			entity.FrozenDate = (Convert.IsDBNull(dataRow["FrozenDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["FrozenDate"];
			entity.IsFrozen = (Convert.IsDBNull(dataRow["IsFrozen"]))?false:(System.Boolean)dataRow["IsFrozen"];
			entity.MeetingRoomConfigId = (Convert.IsDBNull(dataRow["MeetingRoomConfigId"]))?(long)0:(System.Int64)dataRow["MeetingRoomConfigId"];
			entity.Day = (Convert.IsDBNull(dataRow["day"]))?string.Empty:(System.String)dataRow["day"];
			entity.EndTime = (Convert.IsDBNull(dataRow["EndTime"]))?string.Empty:(System.String)dataRow["EndTime"];
			entity.StartTime = (Convert.IsDBNull(dataRow["StartTime"]))?string.Empty:(System.String)dataRow["StartTime"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewRequestFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewRequestFilterBuilder : SqlFilterBuilder<ViewRequestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewRequestFilterBuilder class.
		/// </summary>
		public ViewRequestFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewRequestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewRequestFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewRequestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewRequestFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewRequestFilterBuilder

	#region ViewRequestParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewRequestParameterBuilder : ParameterizedSqlFilterBuilder<ViewRequestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewRequestParameterBuilder class.
		/// </summary>
		public ViewRequestParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewRequestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewRequestParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewRequestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewRequestParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewRequestParameterBuilder
	
	#region ViewRequestSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewRequest"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewRequestSortBuilder : SqlSortBuilder<ViewRequestColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewRequestSqlSortBuilder class.
		/// </summary>
		public ViewRequestSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewRequestSortBuilder

} // end namespace
