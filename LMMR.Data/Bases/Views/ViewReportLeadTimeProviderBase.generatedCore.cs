﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportLeadTimeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportLeadTimeProviderBaseCore : EntityViewProviderBase<ViewReportLeadTime>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportLeadTime&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportLeadTime&gt;"/></returns>
		protected static VList&lt;ViewReportLeadTime&gt; Fill(DataSet dataSet, VList<ViewReportLeadTime> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportLeadTime>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportLeadTime&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportLeadTime>"/></returns>
		protected static VList&lt;ViewReportLeadTime&gt; Fill(DataTable dataTable, VList<ViewReportLeadTime> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportLeadTime c = new ViewReportLeadTime();
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityID"]))?(long)0:(System.Int64)row["CityID"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.HotelId = (Convert.IsDBNull(row["HotelID"]))?(long)0:(System.Int64)row["HotelID"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.AvailabilityId = (Convert.IsDBNull(row["AvailabilityID"]))?(long)0:(System.Int64)row["AvailabilityID"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.LeadTimeForMeetingRoom = (Convert.IsDBNull(row["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)row["LeadTimeForMeetingRoom"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportLeadTime&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportLeadTime&gt;"/></returns>
		protected VList<ViewReportLeadTime> Fill(IDataReader reader, VList<ViewReportLeadTime> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportLeadTime entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportLeadTime>("ViewReportLeadTime",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportLeadTime();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CountryId = (System.Int64)reader[((int)ViewReportLeadTimeColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.CountryName)))?null:(System.String)reader[((int)ViewReportLeadTimeColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewReportLeadTimeColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
					entity.City = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.City)))?null:(System.String)reader[((int)ViewReportLeadTimeColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.HotelId = (System.Int64)reader[((int)ViewReportLeadTimeColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
					entity.Name = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.Name)))?null:(System.String)reader[((int)ViewReportLeadTimeColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.AvailabilityId = (System.Int64)reader[((int)ViewReportLeadTimeColumn.AvailabilityId)];
					//entity.AvailabilityId = (Convert.IsDBNull(reader["AvailabilityID"]))?(long)0:(System.Int64)reader["AvailabilityID"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewReportLeadTimeColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)ViewReportLeadTimeColumn.LeadTimeForMeetingRoom)];
					//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportLeadTime"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportLeadTime"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportLeadTime entity)
		{
			reader.Read();
			entity.CountryId = (System.Int64)reader[((int)ViewReportLeadTimeColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.CountryName)))?null:(System.String)reader[((int)ViewReportLeadTimeColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewReportLeadTimeColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
			entity.City = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.City)))?null:(System.String)reader[((int)ViewReportLeadTimeColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.HotelId = (System.Int64)reader[((int)ViewReportLeadTimeColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
			entity.Name = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.Name)))?null:(System.String)reader[((int)ViewReportLeadTimeColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.AvailabilityId = (System.Int64)reader[((int)ViewReportLeadTimeColumn.AvailabilityId)];
			//entity.AvailabilityId = (Convert.IsDBNull(reader["AvailabilityID"]))?(long)0:(System.Int64)reader["AvailabilityID"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewReportLeadTimeColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)ViewReportLeadTimeColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)ViewReportLeadTimeColumn.LeadTimeForMeetingRoom)];
			//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportLeadTime"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportLeadTime"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportLeadTime entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityID"]))?(long)0:(System.Int64)dataRow["CityID"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelID"]))?(long)0:(System.Int64)dataRow["HotelID"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.AvailabilityId = (Convert.IsDBNull(dataRow["AvailabilityID"]))?(long)0:(System.Int64)dataRow["AvailabilityID"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(dataRow["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)dataRow["LeadTimeForMeetingRoom"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportLeadTimeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportLeadTime"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportLeadTimeFilterBuilder : SqlFilterBuilder<ViewReportLeadTimeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeFilterBuilder class.
		/// </summary>
		public ViewReportLeadTimeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportLeadTimeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportLeadTimeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportLeadTimeFilterBuilder

	#region ViewReportLeadTimeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportLeadTime"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportLeadTimeParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportLeadTimeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeParameterBuilder class.
		/// </summary>
		public ViewReportLeadTimeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportLeadTimeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportLeadTimeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportLeadTimeParameterBuilder
	
	#region ViewReportLeadTimeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportLeadTime"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportLeadTimeSortBuilder : SqlSortBuilder<ViewReportLeadTimeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeSqlSortBuilder class.
		/// </summary>
		public ViewReportLeadTimeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportLeadTimeSortBuilder

} // end namespace
