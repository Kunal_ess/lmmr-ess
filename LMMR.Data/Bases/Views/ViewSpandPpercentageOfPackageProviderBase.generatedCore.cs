﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewSpandPpercentageOfPackageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewSpandPpercentageOfPackageProviderBaseCore : EntityViewProviderBase<ViewSpandPpercentageOfPackage>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewSpandPpercentageOfPackage&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewSpandPpercentageOfPackage&gt;"/></returns>
		protected static VList&lt;ViewSpandPpercentageOfPackage&gt; Fill(DataSet dataSet, VList<ViewSpandPpercentageOfPackage> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewSpandPpercentageOfPackage>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewSpandPpercentageOfPackage&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewSpandPpercentageOfPackage>"/></returns>
		protected static VList&lt;ViewSpandPpercentageOfPackage&gt; Fill(DataTable dataTable, VList<ViewSpandPpercentageOfPackage> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewSpandPpercentageOfPackage c = new ViewSpandPpercentageOfPackage();
					c.PackageId = (Convert.IsDBNull(row["PackageID"]))?(long)0:(System.Int64?)row["PackageID"];
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.SpecialPriceDate = (Convert.IsDBNull(row["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)row["SpecialPriceDate"];
					c.MeetingRoomPercent = (Convert.IsDBNull(row["MeetingRoomPercent"]))?0.0m:(System.Decimal?)row["MeetingRoomPercent"];
					c.DdrPercent = (Convert.IsDBNull(row["DDRPercent"]))?0.0m:(System.Decimal?)row["DDRPercent"];
					c.OtherItemTotal = (Convert.IsDBNull(row["OtherItemTotal"]))?0.0m:(System.Decimal?)row["OtherItemTotal"];
					c.ActualPrice = (Convert.IsDBNull(row["ActualPrice"]))?0.0m:(System.Decimal?)row["ActualPrice"];
					c.FullDayPrice = (Convert.IsDBNull(row["FullDayPrice"]))?0.0m:(System.Decimal?)row["FullDayPrice"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewSpandPpercentageOfPackage&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewSpandPpercentageOfPackage&gt;"/></returns>
		protected VList<ViewSpandPpercentageOfPackage> Fill(IDataReader reader, VList<ViewSpandPpercentageOfPackage> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewSpandPpercentageOfPackage entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewSpandPpercentageOfPackage>("ViewSpandPpercentageOfPackage",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewSpandPpercentageOfPackage();
					}
					
					entity.SuppressEntityEvents = true;

					entity.PackageId = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.PackageId)))?null:(System.Int64?)reader[((int)ViewSpandPpercentageOfPackageColumn.PackageId)];
					//entity.PackageId = (Convert.IsDBNull(reader["PackageID"]))?(long)0:(System.Int64?)reader["PackageID"];
					entity.Id = (System.Int64)reader[((int)ViewSpandPpercentageOfPackageColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.HotelId = (System.Int64)reader[((int)ViewSpandPpercentageOfPackageColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.SpecialPriceDate = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)ViewSpandPpercentageOfPackageColumn.SpecialPriceDate)];
					//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
					entity.MeetingRoomPercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.MeetingRoomPercent)];
					//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
					entity.DdrPercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.DdrPercent)];
					//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
					entity.OtherItemTotal = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.OtherItemTotal)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.OtherItemTotal)];
					//entity.OtherItemTotal = (Convert.IsDBNull(reader["OtherItemTotal"]))?0.0m:(System.Decimal?)reader["OtherItemTotal"];
					entity.ActualPrice = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.ActualPrice)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.ActualPrice)];
					//entity.ActualPrice = (Convert.IsDBNull(reader["ActualPrice"]))?0.0m:(System.Decimal?)reader["ActualPrice"];
					entity.FullDayPrice = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.FullDayPrice)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.FullDayPrice)];
					//entity.FullDayPrice = (Convert.IsDBNull(reader["FullDayPrice"]))?0.0m:(System.Decimal?)reader["FullDayPrice"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewSpandPpercentageOfPackage"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSpandPpercentageOfPackage"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewSpandPpercentageOfPackage entity)
		{
			reader.Read();
			entity.PackageId = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.PackageId)))?null:(System.Int64?)reader[((int)ViewSpandPpercentageOfPackageColumn.PackageId)];
			//entity.PackageId = (Convert.IsDBNull(reader["PackageID"]))?(long)0:(System.Int64?)reader["PackageID"];
			entity.Id = (System.Int64)reader[((int)ViewSpandPpercentageOfPackageColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.HotelId = (System.Int64)reader[((int)ViewSpandPpercentageOfPackageColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.SpecialPriceDate = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)ViewSpandPpercentageOfPackageColumn.SpecialPriceDate)];
			//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
			entity.MeetingRoomPercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.MeetingRoomPercent)];
			//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
			entity.DdrPercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.DdrPercent)];
			//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
			entity.OtherItemTotal = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.OtherItemTotal)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.OtherItemTotal)];
			//entity.OtherItemTotal = (Convert.IsDBNull(reader["OtherItemTotal"]))?0.0m:(System.Decimal?)reader["OtherItemTotal"];
			entity.ActualPrice = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.ActualPrice)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.ActualPrice)];
			//entity.ActualPrice = (Convert.IsDBNull(reader["ActualPrice"]))?0.0m:(System.Decimal?)reader["ActualPrice"];
			entity.FullDayPrice = (reader.IsDBNull(((int)ViewSpandPpercentageOfPackageColumn.FullDayPrice)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfPackageColumn.FullDayPrice)];
			//entity.FullDayPrice = (Convert.IsDBNull(reader["FullDayPrice"]))?0.0m:(System.Decimal?)reader["FullDayPrice"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewSpandPpercentageOfPackage"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSpandPpercentageOfPackage"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewSpandPpercentageOfPackage entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PackageId = (Convert.IsDBNull(dataRow["PackageID"]))?(long)0:(System.Int64?)dataRow["PackageID"];
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.SpecialPriceDate = (Convert.IsDBNull(dataRow["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["SpecialPriceDate"];
			entity.MeetingRoomPercent = (Convert.IsDBNull(dataRow["MeetingRoomPercent"]))?0.0m:(System.Decimal?)dataRow["MeetingRoomPercent"];
			entity.DdrPercent = (Convert.IsDBNull(dataRow["DDRPercent"]))?0.0m:(System.Decimal?)dataRow["DDRPercent"];
			entity.OtherItemTotal = (Convert.IsDBNull(dataRow["OtherItemTotal"]))?0.0m:(System.Decimal?)dataRow["OtherItemTotal"];
			entity.ActualPrice = (Convert.IsDBNull(dataRow["ActualPrice"]))?0.0m:(System.Decimal?)dataRow["ActualPrice"];
			entity.FullDayPrice = (Convert.IsDBNull(dataRow["FullDayPrice"]))?0.0m:(System.Decimal?)dataRow["FullDayPrice"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewSpandPpercentageOfPackageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfPackage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSpandPpercentageOfPackageFilterBuilder : SqlFilterBuilder<ViewSpandPpercentageOfPackageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageFilterBuilder class.
		/// </summary>
		public ViewSpandPpercentageOfPackageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSpandPpercentageOfPackageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSpandPpercentageOfPackageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSpandPpercentageOfPackageFilterBuilder

	#region ViewSpandPpercentageOfPackageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfPackage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSpandPpercentageOfPackageParameterBuilder : ParameterizedSqlFilterBuilder<ViewSpandPpercentageOfPackageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageParameterBuilder class.
		/// </summary>
		public ViewSpandPpercentageOfPackageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSpandPpercentageOfPackageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSpandPpercentageOfPackageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSpandPpercentageOfPackageParameterBuilder
	
	#region ViewSpandPpercentageOfPackageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfPackage"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewSpandPpercentageOfPackageSortBuilder : SqlSortBuilder<ViewSpandPpercentageOfPackageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageSqlSortBuilder class.
		/// </summary>
		public ViewSpandPpercentageOfPackageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewSpandPpercentageOfPackageSortBuilder

} // end namespace
