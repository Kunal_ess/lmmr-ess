﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewSelectAvailabilityProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewSelectAvailabilityProviderBaseCore : EntityViewProviderBase<ViewSelectAvailability>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewSelectAvailability&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewSelectAvailability&gt;"/></returns>
		protected static VList&lt;ViewSelectAvailability&gt; Fill(DataSet dataSet, VList<ViewSelectAvailability> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewSelectAvailability>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewSelectAvailability&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewSelectAvailability>"/></returns>
		protected static VList&lt;ViewSelectAvailability&gt; Fill(DataTable dataTable, VList<ViewSelectAvailability> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewSelectAvailability c = new ViewSelectAvailability();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.HotelId = (Convert.IsDBNull(row["HotelID"]))?(long)0:(System.Int64)row["HotelID"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.FullPropertyStatus = (Convert.IsDBNull(row["FullPropertyStatus"]))?(int)0:(System.Int32?)row["FullPropertyStatus"];
					c.LeadTimeForMeetingRoom = (Convert.IsDBNull(row["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)row["LeadTimeForMeetingRoom"];
					c.FullProrertyStatusBr = (Convert.IsDBNull(row["FullProrertyStatusBR"]))?(int)0:(System.Int32?)row["FullProrertyStatusBR"];
					c.UpdatedBy = (Convert.IsDBNull(row["UpdatedBy"]))?(long)0:(System.Int64?)row["UpdatedBy"];
					c.RoomId = (Convert.IsDBNull(row["RoomId"]))?(long)0:(System.Int64)row["RoomId"];
					c.MorningStatus = (Convert.IsDBNull(row["MorningStatus"]))?(int)0:(System.Int32?)row["MorningStatus"];
					c.AfternoonStatus = (Convert.IsDBNull(row["AfternoonStatus"]))?(int)0:(System.Int32?)row["AfternoonStatus"];
					c.NumberOfRoomsAvailable = (Convert.IsDBNull(row["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)row["NumberOfRoomsAvailable"];
					c.RoomType = (Convert.IsDBNull(row["RoomType"]))?(int)0:(System.Int32?)row["RoomType"];
					c.IsReserved = (Convert.IsDBNull(row["IsReserved"]))?false:(System.Boolean)row["IsReserved"];
					c.LastModifyTime = (Convert.IsDBNull(row["LastModifyTime"]))?DateTime.MinValue:(System.DateTime?)row["LastModifyTime"];
					c.AvailRoomId = (Convert.IsDBNull(row["AvailRoomID"]))?(long)0:(System.Int64)row["AvailRoomID"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewSelectAvailability&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewSelectAvailability&gt;"/></returns>
		protected VList<ViewSelectAvailability> Fill(IDataReader reader, VList<ViewSelectAvailability> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewSelectAvailability entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewSelectAvailability>("ViewSelectAvailability",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewSelectAvailability();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewSelectAvailabilityColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.HotelId = (System.Int64)reader[((int)ViewSelectAvailabilityColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewSelectAvailabilityColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.FullPropertyStatus = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.FullPropertyStatus)];
					//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
					entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)ViewSelectAvailabilityColumn.LeadTimeForMeetingRoom)];
					//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
					entity.FullProrertyStatusBr = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.FullProrertyStatusBr)];
					//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
					entity.UpdatedBy = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.UpdatedBy)))?null:(System.Int64?)reader[((int)ViewSelectAvailabilityColumn.UpdatedBy)];
					//entity.UpdatedBy = (Convert.IsDBNull(reader["UpdatedBy"]))?(long)0:(System.Int64?)reader["UpdatedBy"];
					entity.RoomId = (System.Int64)reader[((int)ViewSelectAvailabilityColumn.RoomId)];
					//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
					entity.MorningStatus = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.MorningStatus)];
					//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
					entity.AfternoonStatus = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.AfternoonStatus)];
					//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
					entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)ViewSelectAvailabilityColumn.NumberOfRoomsAvailable)];
					//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
					entity.RoomType = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.RoomType)];
					//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
					entity.IsReserved = (System.Boolean)reader[((int)ViewSelectAvailabilityColumn.IsReserved)];
					//entity.IsReserved = (Convert.IsDBNull(reader["IsReserved"]))?false:(System.Boolean)reader["IsReserved"];
					entity.LastModifyTime = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.LastModifyTime)))?null:(System.DateTime?)reader[((int)ViewSelectAvailabilityColumn.LastModifyTime)];
					//entity.LastModifyTime = (Convert.IsDBNull(reader["LastModifyTime"]))?DateTime.MinValue:(System.DateTime?)reader["LastModifyTime"];
					entity.AvailRoomId = (System.Int64)reader[((int)ViewSelectAvailabilityColumn.AvailRoomId)];
					//entity.AvailRoomId = (Convert.IsDBNull(reader["AvailRoomID"]))?(long)0:(System.Int64)reader["AvailRoomID"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewSelectAvailability"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSelectAvailability"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewSelectAvailability entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewSelectAvailabilityColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.HotelId = (System.Int64)reader[((int)ViewSelectAvailabilityColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewSelectAvailabilityColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.FullPropertyStatus = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.FullPropertyStatus)];
			//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
			entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)ViewSelectAvailabilityColumn.LeadTimeForMeetingRoom)];
			//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
			entity.FullProrertyStatusBr = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.FullProrertyStatusBr)];
			//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
			entity.UpdatedBy = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.UpdatedBy)))?null:(System.Int64?)reader[((int)ViewSelectAvailabilityColumn.UpdatedBy)];
			//entity.UpdatedBy = (Convert.IsDBNull(reader["UpdatedBy"]))?(long)0:(System.Int64?)reader["UpdatedBy"];
			entity.RoomId = (System.Int64)reader[((int)ViewSelectAvailabilityColumn.RoomId)];
			//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
			entity.MorningStatus = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.MorningStatus)];
			//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
			entity.AfternoonStatus = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.AfternoonStatus)];
			//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)ViewSelectAvailabilityColumn.NumberOfRoomsAvailable)];
			//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
			entity.RoomType = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewSelectAvailabilityColumn.RoomType)];
			//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
			entity.IsReserved = (System.Boolean)reader[((int)ViewSelectAvailabilityColumn.IsReserved)];
			//entity.IsReserved = (Convert.IsDBNull(reader["IsReserved"]))?false:(System.Boolean)reader["IsReserved"];
			entity.LastModifyTime = (reader.IsDBNull(((int)ViewSelectAvailabilityColumn.LastModifyTime)))?null:(System.DateTime?)reader[((int)ViewSelectAvailabilityColumn.LastModifyTime)];
			//entity.LastModifyTime = (Convert.IsDBNull(reader["LastModifyTime"]))?DateTime.MinValue:(System.DateTime?)reader["LastModifyTime"];
			entity.AvailRoomId = (System.Int64)reader[((int)ViewSelectAvailabilityColumn.AvailRoomId)];
			//entity.AvailRoomId = (Convert.IsDBNull(reader["AvailRoomID"]))?(long)0:(System.Int64)reader["AvailRoomID"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewSelectAvailability"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSelectAvailability"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewSelectAvailability entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelID"]))?(long)0:(System.Int64)dataRow["HotelID"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.FullPropertyStatus = (Convert.IsDBNull(dataRow["FullPropertyStatus"]))?(int)0:(System.Int32?)dataRow["FullPropertyStatus"];
			entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(dataRow["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)dataRow["LeadTimeForMeetingRoom"];
			entity.FullProrertyStatusBr = (Convert.IsDBNull(dataRow["FullProrertyStatusBR"]))?(int)0:(System.Int32?)dataRow["FullProrertyStatusBR"];
			entity.UpdatedBy = (Convert.IsDBNull(dataRow["UpdatedBy"]))?(long)0:(System.Int64?)dataRow["UpdatedBy"];
			entity.RoomId = (Convert.IsDBNull(dataRow["RoomId"]))?(long)0:(System.Int64)dataRow["RoomId"];
			entity.MorningStatus = (Convert.IsDBNull(dataRow["MorningStatus"]))?(int)0:(System.Int32?)dataRow["MorningStatus"];
			entity.AfternoonStatus = (Convert.IsDBNull(dataRow["AfternoonStatus"]))?(int)0:(System.Int32?)dataRow["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (Convert.IsDBNull(dataRow["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomsAvailable"];
			entity.RoomType = (Convert.IsDBNull(dataRow["RoomType"]))?(int)0:(System.Int32?)dataRow["RoomType"];
			entity.IsReserved = (Convert.IsDBNull(dataRow["IsReserved"]))?false:(System.Boolean)dataRow["IsReserved"];
			entity.LastModifyTime = (Convert.IsDBNull(dataRow["LastModifyTime"]))?DateTime.MinValue:(System.DateTime?)dataRow["LastModifyTime"];
			entity.AvailRoomId = (Convert.IsDBNull(dataRow["AvailRoomID"]))?(long)0:(System.Int64)dataRow["AvailRoomID"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewSelectAvailabilityFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSelectAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSelectAvailabilityFilterBuilder : SqlFilterBuilder<ViewSelectAvailabilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityFilterBuilder class.
		/// </summary>
		public ViewSelectAvailabilityFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSelectAvailabilityFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSelectAvailabilityFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSelectAvailabilityFilterBuilder

	#region ViewSelectAvailabilityParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSelectAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSelectAvailabilityParameterBuilder : ParameterizedSqlFilterBuilder<ViewSelectAvailabilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityParameterBuilder class.
		/// </summary>
		public ViewSelectAvailabilityParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSelectAvailabilityParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSelectAvailabilityParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSelectAvailabilityParameterBuilder
	
	#region ViewSelectAvailabilitySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSelectAvailability"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewSelectAvailabilitySortBuilder : SqlSortBuilder<ViewSelectAvailabilityColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilitySqlSortBuilder class.
		/// </summary>
		public ViewSelectAvailabilitySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewSelectAvailabilitySortBuilder

} // end namespace
