﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewForAvailabilityCheckProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewForAvailabilityCheckProviderBaseCore : EntityViewProviderBase<ViewForAvailabilityCheck>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewForAvailabilityCheck&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewForAvailabilityCheck&gt;"/></returns>
		protected static VList&lt;ViewForAvailabilityCheck&gt; Fill(DataSet dataSet, VList<ViewForAvailabilityCheck> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewForAvailabilityCheck>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewForAvailabilityCheck&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewForAvailabilityCheck>"/></returns>
		protected static VList&lt;ViewForAvailabilityCheck&gt; Fill(DataTable dataTable, VList<ViewForAvailabilityCheck> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewForAvailabilityCheck c = new ViewForAvailabilityCheck();
					c.HotelId = (Convert.IsDBNull(row["HotelID"]))?(long)0:(System.Int64)row["HotelID"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewForAvailabilityCheck&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewForAvailabilityCheck&gt;"/></returns>
		protected VList<ViewForAvailabilityCheck> Fill(IDataReader reader, VList<ViewForAvailabilityCheck> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewForAvailabilityCheck entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewForAvailabilityCheck>("ViewForAvailabilityCheck",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewForAvailabilityCheck();
					}
					
					entity.SuppressEntityEvents = true;

					entity.HotelId = (System.Int64)reader[((int)ViewForAvailabilityCheckColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewForAvailabilityCheckColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewForAvailabilityCheckColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewForAvailabilityCheck"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewForAvailabilityCheck"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewForAvailabilityCheck entity)
		{
			reader.Read();
			entity.HotelId = (System.Int64)reader[((int)ViewForAvailabilityCheckColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewForAvailabilityCheckColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewForAvailabilityCheckColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewForAvailabilityCheck"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewForAvailabilityCheck"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewForAvailabilityCheck entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelID"]))?(long)0:(System.Int64)dataRow["HotelID"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewForAvailabilityCheckFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForAvailabilityCheck"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForAvailabilityCheckFilterBuilder : SqlFilterBuilder<ViewForAvailabilityCheckColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckFilterBuilder class.
		/// </summary>
		public ViewForAvailabilityCheckFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForAvailabilityCheckFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForAvailabilityCheckFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForAvailabilityCheckFilterBuilder

	#region ViewForAvailabilityCheckParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForAvailabilityCheck"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForAvailabilityCheckParameterBuilder : ParameterizedSqlFilterBuilder<ViewForAvailabilityCheckColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckParameterBuilder class.
		/// </summary>
		public ViewForAvailabilityCheckParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForAvailabilityCheckParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForAvailabilityCheckParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForAvailabilityCheckParameterBuilder
	
	#region ViewForAvailabilityCheckSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForAvailabilityCheck"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewForAvailabilityCheckSortBuilder : SqlSortBuilder<ViewForAvailabilityCheckColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckSqlSortBuilder class.
		/// </summary>
		public ViewForAvailabilityCheckSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewForAvailabilityCheckSortBuilder

} // end namespace
