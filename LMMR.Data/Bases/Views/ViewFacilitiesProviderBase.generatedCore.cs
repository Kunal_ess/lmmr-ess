﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewFacilitiesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewFacilitiesProviderBaseCore : EntityViewProviderBase<ViewFacilities>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewFacilities&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewFacilities&gt;"/></returns>
		protected static VList&lt;ViewFacilities&gt; Fill(DataSet dataSet, VList<ViewFacilities> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewFacilities>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewFacilities&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewFacilities>"/></returns>
		protected static VList&lt;ViewFacilities&gt; Fill(DataTable dataTable, VList<ViewFacilities> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewFacilities c = new ViewFacilities();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.FacilityTypeName = (Convert.IsDBNull(row["FacilityTypeName"]))?string.Empty:(System.String)row["FacilityTypeName"];
					c.Expr1 = (Convert.IsDBNull(row["Expr1"]))?(long)0:(System.Int64)row["Expr1"];
					c.FacilityTypeId = (Convert.IsDBNull(row["FacilityTypeID"]))?(long)0:(System.Int64)row["FacilityTypeID"];
					c.FacilityName = (Convert.IsDBNull(row["FacilityName"]))?string.Empty:(System.String)row["FacilityName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewFacilities&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewFacilities&gt;"/></returns>
		protected VList<ViewFacilities> Fill(IDataReader reader, VList<ViewFacilities> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewFacilities entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewFacilities>("ViewFacilities",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewFacilities();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewFacilitiesColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.FacilityTypeName = (reader.IsDBNull(((int)ViewFacilitiesColumn.FacilityTypeName)))?null:(System.String)reader[((int)ViewFacilitiesColumn.FacilityTypeName)];
					//entity.FacilityTypeName = (Convert.IsDBNull(reader["FacilityTypeName"]))?string.Empty:(System.String)reader["FacilityTypeName"];
					entity.Expr1 = (System.Int64)reader[((int)ViewFacilitiesColumn.Expr1)];
					//entity.Expr1 = (Convert.IsDBNull(reader["Expr1"]))?(long)0:(System.Int64)reader["Expr1"];
					entity.FacilityTypeId = (System.Int64)reader[((int)ViewFacilitiesColumn.FacilityTypeId)];
					//entity.FacilityTypeId = (Convert.IsDBNull(reader["FacilityTypeID"]))?(long)0:(System.Int64)reader["FacilityTypeID"];
					entity.FacilityName = (reader.IsDBNull(((int)ViewFacilitiesColumn.FacilityName)))?null:(System.String)reader[((int)ViewFacilitiesColumn.FacilityName)];
					//entity.FacilityName = (Convert.IsDBNull(reader["FacilityName"]))?string.Empty:(System.String)reader["FacilityName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewFacilities"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewFacilities"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewFacilities entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewFacilitiesColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.FacilityTypeName = (reader.IsDBNull(((int)ViewFacilitiesColumn.FacilityTypeName)))?null:(System.String)reader[((int)ViewFacilitiesColumn.FacilityTypeName)];
			//entity.FacilityTypeName = (Convert.IsDBNull(reader["FacilityTypeName"]))?string.Empty:(System.String)reader["FacilityTypeName"];
			entity.Expr1 = (System.Int64)reader[((int)ViewFacilitiesColumn.Expr1)];
			//entity.Expr1 = (Convert.IsDBNull(reader["Expr1"]))?(long)0:(System.Int64)reader["Expr1"];
			entity.FacilityTypeId = (System.Int64)reader[((int)ViewFacilitiesColumn.FacilityTypeId)];
			//entity.FacilityTypeId = (Convert.IsDBNull(reader["FacilityTypeID"]))?(long)0:(System.Int64)reader["FacilityTypeID"];
			entity.FacilityName = (reader.IsDBNull(((int)ViewFacilitiesColumn.FacilityName)))?null:(System.String)reader[((int)ViewFacilitiesColumn.FacilityName)];
			//entity.FacilityName = (Convert.IsDBNull(reader["FacilityName"]))?string.Empty:(System.String)reader["FacilityName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewFacilities"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewFacilities"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewFacilities entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.FacilityTypeName = (Convert.IsDBNull(dataRow["FacilityTypeName"]))?string.Empty:(System.String)dataRow["FacilityTypeName"];
			entity.Expr1 = (Convert.IsDBNull(dataRow["Expr1"]))?(long)0:(System.Int64)dataRow["Expr1"];
			entity.FacilityTypeId = (Convert.IsDBNull(dataRow["FacilityTypeID"]))?(long)0:(System.Int64)dataRow["FacilityTypeID"];
			entity.FacilityName = (Convert.IsDBNull(dataRow["FacilityName"]))?string.Empty:(System.String)dataRow["FacilityName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewFacilitiesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFacilities"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFacilitiesFilterBuilder : SqlFilterBuilder<ViewFacilitiesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesFilterBuilder class.
		/// </summary>
		public ViewFacilitiesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFacilitiesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFacilitiesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFacilitiesFilterBuilder

	#region ViewFacilitiesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFacilities"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFacilitiesParameterBuilder : ParameterizedSqlFilterBuilder<ViewFacilitiesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesParameterBuilder class.
		/// </summary>
		public ViewFacilitiesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFacilitiesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFacilitiesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFacilitiesParameterBuilder
	
	#region ViewFacilitiesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFacilities"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewFacilitiesSortBuilder : SqlSortBuilder<ViewFacilitiesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesSqlSortBuilder class.
		/// </summary>
		public ViewFacilitiesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewFacilitiesSortBuilder

} // end namespace
