﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportAvailabilityProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportAvailabilityProviderBaseCore : EntityViewProviderBase<ViewReportAvailability>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportAvailability&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportAvailability&gt;"/></returns>
		protected static VList&lt;ViewReportAvailability&gt; Fill(DataSet dataSet, VList<ViewReportAvailability> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportAvailability>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportAvailability&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportAvailability>"/></returns>
		protected static VList&lt;ViewReportAvailability&gt; Fill(DataTable dataTable, VList<ViewReportAvailability> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportAvailability c = new ViewReportAvailability();
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityID"]))?(long)0:(System.Int64)row["CityID"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.HotelId = (Convert.IsDBNull(row["HotelID"]))?(long)0:(System.Int64)row["HotelID"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.MeetingRoomId = (Convert.IsDBNull(row["MeetingRoomID"]))?(long)0:(System.Int64)row["MeetingRoomID"];
					c.MeetingRoomName = (Convert.IsDBNull(row["MeetingRoomName"]))?string.Empty:(System.String)row["MeetingRoomName"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.MorningStatus = (Convert.IsDBNull(row["MorningStatus"]))?(int)0:(System.Int32?)row["MorningStatus"];
					c.AfternoonStatus = (Convert.IsDBNull(row["AfternoonStatus"]))?(int)0:(System.Int32?)row["AfternoonStatus"];
					c.RoomId = (Convert.IsDBNull(row["RoomId"]))?(long)0:(System.Int64)row["RoomId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportAvailability&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportAvailability&gt;"/></returns>
		protected VList<ViewReportAvailability> Fill(IDataReader reader, VList<ViewReportAvailability> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportAvailability entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportAvailability>("ViewReportAvailability",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportAvailability();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CountryId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.CountryName)))?null:(System.String)reader[((int)ViewReportAvailabilityColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
					entity.City = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.City)))?null:(System.String)reader[((int)ViewReportAvailabilityColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.HotelId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
					entity.HotelName = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.HotelName)))?null:(System.String)reader[((int)ViewReportAvailabilityColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.MeetingRoomId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.MeetingRoomId)];
					//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomID"]))?(long)0:(System.Int64)reader["MeetingRoomID"];
					entity.MeetingRoomName = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.MeetingRoomName)))?null:(System.String)reader[((int)ViewReportAvailabilityColumn.MeetingRoomName)];
					//entity.MeetingRoomName = (Convert.IsDBNull(reader["MeetingRoomName"]))?string.Empty:(System.String)reader["MeetingRoomName"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewReportAvailabilityColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.MorningStatus = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewReportAvailabilityColumn.MorningStatus)];
					//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
					entity.AfternoonStatus = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewReportAvailabilityColumn.AfternoonStatus)];
					//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
					entity.RoomId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.RoomId)];
					//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportAvailability"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportAvailability"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportAvailability entity)
		{
			reader.Read();
			entity.CountryId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.CountryName)))?null:(System.String)reader[((int)ViewReportAvailabilityColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
			entity.City = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.City)))?null:(System.String)reader[((int)ViewReportAvailabilityColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.HotelId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
			entity.HotelName = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.HotelName)))?null:(System.String)reader[((int)ViewReportAvailabilityColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.MeetingRoomId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.MeetingRoomId)];
			//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomID"]))?(long)0:(System.Int64)reader["MeetingRoomID"];
			entity.MeetingRoomName = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.MeetingRoomName)))?null:(System.String)reader[((int)ViewReportAvailabilityColumn.MeetingRoomName)];
			//entity.MeetingRoomName = (Convert.IsDBNull(reader["MeetingRoomName"]))?string.Empty:(System.String)reader["MeetingRoomName"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewReportAvailabilityColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.MorningStatus = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewReportAvailabilityColumn.MorningStatus)];
			//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
			entity.AfternoonStatus = (reader.IsDBNull(((int)ViewReportAvailabilityColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewReportAvailabilityColumn.AfternoonStatus)];
			//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
			entity.RoomId = (System.Int64)reader[((int)ViewReportAvailabilityColumn.RoomId)];
			//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportAvailability"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportAvailability"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportAvailability entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityID"]))?(long)0:(System.Int64)dataRow["CityID"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelID"]))?(long)0:(System.Int64)dataRow["HotelID"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.MeetingRoomId = (Convert.IsDBNull(dataRow["MeetingRoomID"]))?(long)0:(System.Int64)dataRow["MeetingRoomID"];
			entity.MeetingRoomName = (Convert.IsDBNull(dataRow["MeetingRoomName"]))?string.Empty:(System.String)dataRow["MeetingRoomName"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.MorningStatus = (Convert.IsDBNull(dataRow["MorningStatus"]))?(int)0:(System.Int32?)dataRow["MorningStatus"];
			entity.AfternoonStatus = (Convert.IsDBNull(dataRow["AfternoonStatus"]))?(int)0:(System.Int32?)dataRow["AfternoonStatus"];
			entity.RoomId = (Convert.IsDBNull(dataRow["RoomId"]))?(long)0:(System.Int64)dataRow["RoomId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportAvailabilityFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportAvailabilityFilterBuilder : SqlFilterBuilder<ViewReportAvailabilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityFilterBuilder class.
		/// </summary>
		public ViewReportAvailabilityFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportAvailabilityFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportAvailabilityFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportAvailabilityFilterBuilder

	#region ViewReportAvailabilityParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportAvailabilityParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportAvailabilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityParameterBuilder class.
		/// </summary>
		public ViewReportAvailabilityParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportAvailabilityParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportAvailabilityParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportAvailabilityParameterBuilder
	
	#region ViewReportAvailabilitySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportAvailability"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportAvailabilitySortBuilder : SqlSortBuilder<ViewReportAvailabilityColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilitySqlSortBuilder class.
		/// </summary>
		public ViewReportAvailabilitySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportAvailabilitySortBuilder

} // end namespace
