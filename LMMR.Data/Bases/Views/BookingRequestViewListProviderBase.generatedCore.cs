﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BookingRequestViewListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class BookingRequestViewListProviderBaseCore : EntityViewProviderBase<BookingRequestViewList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;BookingRequestViewList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;BookingRequestViewList&gt;"/></returns>
		protected static VList&lt;BookingRequestViewList&gt; Fill(DataSet dataSet, VList<BookingRequestViewList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<BookingRequestViewList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;BookingRequestViewList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<BookingRequestViewList>"/></returns>
		protected static VList&lt;BookingRequestViewList&gt; Fill(DataTable dataTable, VList<BookingRequestViewList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					BookingRequestViewList c = new BookingRequestViewList();
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.CountryId = (Convert.IsDBNull(row["CountryId"]))?(long)0:(System.Int64)row["CountryId"];
					c.CityId = (Convert.IsDBNull(row["CityId"]))?(long)0:(System.Int64)row["CityId"];
					c.ZoneId = (Convert.IsDBNull(row["ZoneId"]))?(long)0:(System.Int64)row["ZoneId"];
					c.Stars = (Convert.IsDBNull(row["Stars"]))?(int)0:(System.Int32?)row["Stars"];
					c.Longitude = (Convert.IsDBNull(row["Longitude"]))?string.Empty:(System.String)row["Longitude"];
					c.Latitude = (Convert.IsDBNull(row["Latitude"]))?string.Empty:(System.String)row["Latitude"];
					c.Logo = (Convert.IsDBNull(row["Logo"]))?string.Empty:(System.String)row["Logo"];
					c.IsActive = (Convert.IsDBNull(row["IsActive"]))?false:(System.Boolean)row["IsActive"];
					c.CancelationPolicyId = (Convert.IsDBNull(row["CancelationPolicyId"]))?(long)0:(System.Int64)row["CancelationPolicyId"];
					c.GroupId = (Convert.IsDBNull(row["GroupId"]))?(long)0:(System.Int64?)row["GroupId"];
					c.OperatorChoice = (Convert.IsDBNull(row["OperatorChoice"]))?string.Empty:(System.String)row["OperatorChoice"];
					c.HotelPlan = (Convert.IsDBNull(row["HotelPlan"]))?string.Empty:(System.String)row["HotelPlan"];
					c.ClientId = (Convert.IsDBNull(row["ClientId"]))?(long)0:(System.Int64?)row["ClientId"];
					c.ContractId = (Convert.IsDBNull(row["ContractId"]))?string.Empty:(System.String)row["ContractId"];
					c.StaffId = (Convert.IsDBNull(row["StaffId"]))?(long)0:(System.Int64)row["StaffId"];
					c.CurrencyId = (Convert.IsDBNull(row["CurrencyId"]))?(long)0:(System.Int64)row["CurrencyId"];
					c.ContractValue = (Convert.IsDBNull(row["ContractValue"]))?0.0m:(System.Decimal?)row["ContractValue"];
					c.TimeZone = (Convert.IsDBNull(row["TimeZone"]))?string.Empty:(System.String)row["TimeZone"];
					c.Phone = (Convert.IsDBNull(row["Phone"]))?string.Empty:(System.String)row["Phone"];
					c.PhoneExt = (Convert.IsDBNull(row["PhoneExt"]))?string.Empty:(System.String)row["PhoneExt"];
					c.RtMFrom = (Convert.IsDBNull(row["RT_M_From"]))?string.Empty:(System.String)row["RT_M_From"];
					c.RtMTo = (Convert.IsDBNull(row["RT_M_To"]))?string.Empty:(System.String)row["RT_M_To"];
					c.RtAFrom = (Convert.IsDBNull(row["RT_A_From"]))?string.Empty:(System.String)row["RT_A_From"];
					c.RtATo = (Convert.IsDBNull(row["RT_A_To"]))?string.Empty:(System.String)row["RT_A_To"];
					c.RtFFrom = (Convert.IsDBNull(row["RT_F_From"]))?string.Empty:(System.String)row["RT_F_From"];
					c.RtFTo = (Convert.IsDBNull(row["RT_F_To"]))?string.Empty:(System.String)row["RT_F_To"];
					c.IsCreditCardExcepted = (Convert.IsDBNull(row["IsCreditCardExcepted"]))?false:(System.Boolean)row["IsCreditCardExcepted"];
					c.CreditCardType = (Convert.IsDBNull(row["CreditCardType"]))?string.Empty:(System.String)row["CreditCardType"];
					c.Theme = (Convert.IsDBNull(row["Theme"]))?string.Empty:(System.String)row["Theme"];
					c.IsBedroomAvailable = (Convert.IsDBNull(row["IsBedroomAvailable"]))?false:(System.Boolean)row["IsBedroomAvailable"];
					c.NumberOfBedroom = (Convert.IsDBNull(row["NumberOfBedroom"]))?(int)0:(System.Int32?)row["NumberOfBedroom"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.ContactPerson = (Convert.IsDBNull(row["ContactPerson"]))?string.Empty:(System.String)row["ContactPerson"];
					c.PhoneNumber = (Convert.IsDBNull(row["PhoneNumber"]))?string.Empty:(System.String)row["PhoneNumber"];
					c.GoOnline = (Convert.IsDBNull(row["GoOnline"]))?false:(System.Boolean?)row["GoOnline"];
					c.ZipCode = (Convert.IsDBNull(row["ZipCode"]))?string.Empty:(System.String)row["ZipCode"];
					c.UpdatedBy = (Convert.IsDBNull(row["UpdatedBy"]))?(long)0:(System.Int64?)row["UpdatedBy"];
					c.HotelAddress = (Convert.IsDBNull(row["HotelAddress"]))?string.Empty:(System.String)row["HotelAddress"];
					c.CreationDate = (Convert.IsDBNull(row["CreationDate"]))?DateTime.MinValue:(System.DateTime)row["CreationDate"];
					c.CountryCodePhone = (Convert.IsDBNull(row["CountryCodePhone"]))?string.Empty:(System.String)row["CountryCodePhone"];
					c.CountryCodeFax = (Convert.IsDBNull(row["CountryCodeFax"]))?string.Empty:(System.String)row["CountryCodeFax"];
					c.FaxNumber = (Convert.IsDBNull(row["FaxNumber"]))?string.Empty:(System.String)row["FaxNumber"];
					c.IsRemoved = (Convert.IsDBNull(row["IsRemoved"]))?false:(System.Boolean)row["IsRemoved"];
					c.RequestGoOnline = (Convert.IsDBNull(row["RequestGoOnline"]))?false:(System.Boolean?)row["RequestGoOnline"];
					c.HotelUserId = (Convert.IsDBNull(row["HotelUserID"]))?(long)0:(System.Int64?)row["HotelUserID"];
					c.AccountingOfficer = (Convert.IsDBNull(row["AccountingOfficer"]))?string.Empty:(System.String)row["AccountingOfficer"];
					c.MeetingRoomId = (Convert.IsDBNull(row["MeetingRoomId"]))?(long)0:(System.Int64)row["MeetingRoomId"];
					c.MeetingRoomName = (Convert.IsDBNull(row["MeetingRoomName"]))?string.Empty:(System.String)row["MeetingRoomName"];
					c.Picture = (Convert.IsDBNull(row["Picture"]))?string.Empty:(System.String)row["Picture"];
					c.DayLight = (Convert.IsDBNull(row["DayLight"]))?(int)0:(System.Int32)row["DayLight"];
					c.Surface = (Convert.IsDBNull(row["Surface"]))?(long)0:(System.Int64?)row["Surface"];
					c.Height = (Convert.IsDBNull(row["Height"]))?0.0m:(System.Decimal?)row["Height"];
					c.MeetingRoomActive = (Convert.IsDBNull(row["MeetingRoomActive"]))?false:(System.Boolean)row["MeetingRoomActive"];
					c.MrPlan = (Convert.IsDBNull(row["MR_Plan"]))?string.Empty:(System.String)row["MR_Plan"];
					c.OrderNumber = (Convert.IsDBNull(row["OrderNumber"]))?(int)0:(System.Int32?)row["OrderNumber"];
					c.HalfdayPrice = (Convert.IsDBNull(row["HalfdayPrice"]))?0.0m:(System.Decimal?)row["HalfdayPrice"];
					c.FulldayPrice = (Convert.IsDBNull(row["FulldayPrice"]))?0.0m:(System.Decimal?)row["FulldayPrice"];
					c.IsOnline = (Convert.IsDBNull(row["IsOnline"]))?false:(System.Boolean)row["IsOnline"];
					c.IsDeleted = (Convert.IsDBNull(row["IsDeleted"]))?false:(System.Boolean)row["IsDeleted"];
					c.SppId = (Convert.IsDBNull(row["SPPId"]))?(long)0:(System.Int64)row["SPPId"];
					c.SpecialPriceDate = (Convert.IsDBNull(row["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)row["SpecialPriceDate"];
					c.DdrPercent = (Convert.IsDBNull(row["DDRPercent"]))?0.0m:(System.Decimal?)row["DDRPercent"];
					c.MeetingRoomPercent = (Convert.IsDBNull(row["MeetingRoomPercent"]))?0.0m:(System.Decimal?)row["MeetingRoomPercent"];
					c.AvailabilityId = (Convert.IsDBNull(row["AvailabilityId"]))?(long)0:(System.Int64)row["AvailabilityId"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.FullPropertyStatus = (Convert.IsDBNull(row["FullPropertyStatus"]))?(int)0:(System.Int32?)row["FullPropertyStatus"];
					c.LeadTimeForMeetingRoom = (Convert.IsDBNull(row["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)row["LeadTimeForMeetingRoom"];
					c.FullProrertyStatusBr = (Convert.IsDBNull(row["FullProrertyStatusBR"]))?(int)0:(System.Int32?)row["FullProrertyStatusBR"];
					c.AvailibilityOfRoomsId = (Convert.IsDBNull(row["AvailibilityOfRoomsId"]))?(long)0:(System.Int64)row["AvailibilityOfRoomsId"];
					c.MorningStatus = (Convert.IsDBNull(row["MorningStatus"]))?(int)0:(System.Int32?)row["MorningStatus"];
					c.AfternoonStatus = (Convert.IsDBNull(row["AfternoonStatus"]))?(int)0:(System.Int32?)row["AfternoonStatus"];
					c.NumberOfRoomsAvailable = (Convert.IsDBNull(row["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)row["NumberOfRoomsAvailable"];
					c.RoomType = (Convert.IsDBNull(row["RoomType"]))?(int)0:(System.Int32?)row["RoomType"];
					c.MeetingRoomConfigId = (Convert.IsDBNull(row["MeetingRoomConfigId"]))?(long)0:(System.Int64)row["MeetingRoomConfigId"];
					c.RoomShapeId = (Convert.IsDBNull(row["RoomShapeId"]))?(int)0:(System.Int32?)row["RoomShapeId"];
					c.MinCapacity = (Convert.IsDBNull(row["MinCapacity"]))?(int)0:(System.Int32?)row["MinCapacity"];
					c.MaxCapicity = (Convert.IsDBNull(row["MaxCapicity"]))?(int)0:(System.Int32?)row["MaxCapicity"];
					c.BookingAlgo = (Convert.IsDBNull(row["BookingAlgo"]))?0.0m:(System.Decimal?)row["BookingAlgo"];
					c.RequestAlgo = (Convert.IsDBNull(row["RequestAlgo"]))?0.0m:(System.Decimal?)row["RequestAlgo"];
					c.IsPriority = (Convert.IsDBNull(row["IsPriority"]))?string.Empty:(System.String)row["IsPriority"];
					c.ActualPkgPrice = (Convert.IsDBNull(row["ActualPkgPrice"]))?0.0m:(System.Decimal?)row["ActualPkgPrice"];
					c.ActualPkgPriceHalfDay = (Convert.IsDBNull(row["ActualPkgPriceHalfDay"]))?0.0m:(System.Decimal?)row["ActualPkgPriceHalfDay"];
					c.NumberOfRoomBooked = (Convert.IsDBNull(row["NumberOfRoomBooked"]))?(long)0:(System.Int64?)row["NumberOfRoomBooked"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;BookingRequestViewList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;BookingRequestViewList&gt;"/></returns>
		protected VList<BookingRequestViewList> Fill(IDataReader reader, VList<BookingRequestViewList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					BookingRequestViewList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<BookingRequestViewList>("BookingRequestViewList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new BookingRequestViewList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.HotelId = (System.Int64)reader[((int)BookingRequestViewListColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.HotelName = (reader.IsDBNull(((int)BookingRequestViewListColumn.HotelName)))?null:(System.String)reader[((int)BookingRequestViewListColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.CountryId = (System.Int64)reader[((int)BookingRequestViewListColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
					entity.CityId = (System.Int64)reader[((int)BookingRequestViewListColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
					entity.ZoneId = (System.Int64)reader[((int)BookingRequestViewListColumn.ZoneId)];
					//entity.ZoneId = (Convert.IsDBNull(reader["ZoneId"]))?(long)0:(System.Int64)reader["ZoneId"];
					entity.Stars = (reader.IsDBNull(((int)BookingRequestViewListColumn.Stars)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.Stars)];
					//entity.Stars = (Convert.IsDBNull(reader["Stars"]))?(int)0:(System.Int32?)reader["Stars"];
					entity.Longitude = (reader.IsDBNull(((int)BookingRequestViewListColumn.Longitude)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Longitude)];
					//entity.Longitude = (Convert.IsDBNull(reader["Longitude"]))?string.Empty:(System.String)reader["Longitude"];
					entity.Latitude = (reader.IsDBNull(((int)BookingRequestViewListColumn.Latitude)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Latitude)];
					//entity.Latitude = (Convert.IsDBNull(reader["Latitude"]))?string.Empty:(System.String)reader["Latitude"];
					entity.Logo = (reader.IsDBNull(((int)BookingRequestViewListColumn.Logo)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Logo)];
					//entity.Logo = (Convert.IsDBNull(reader["Logo"]))?string.Empty:(System.String)reader["Logo"];
					entity.IsActive = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsActive)];
					//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
					entity.CancelationPolicyId = (System.Int64)reader[((int)BookingRequestViewListColumn.CancelationPolicyId)];
					//entity.CancelationPolicyId = (Convert.IsDBNull(reader["CancelationPolicyId"]))?(long)0:(System.Int64)reader["CancelationPolicyId"];
					entity.GroupId = (reader.IsDBNull(((int)BookingRequestViewListColumn.GroupId)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.GroupId)];
					//entity.GroupId = (Convert.IsDBNull(reader["GroupId"]))?(long)0:(System.Int64?)reader["GroupId"];
					entity.OperatorChoice = (reader.IsDBNull(((int)BookingRequestViewListColumn.OperatorChoice)))?null:(System.String)reader[((int)BookingRequestViewListColumn.OperatorChoice)];
					//entity.OperatorChoice = (Convert.IsDBNull(reader["OperatorChoice"]))?string.Empty:(System.String)reader["OperatorChoice"];
					entity.HotelPlan = (reader.IsDBNull(((int)BookingRequestViewListColumn.HotelPlan)))?null:(System.String)reader[((int)BookingRequestViewListColumn.HotelPlan)];
					//entity.HotelPlan = (Convert.IsDBNull(reader["HotelPlan"]))?string.Empty:(System.String)reader["HotelPlan"];
					entity.ClientId = (reader.IsDBNull(((int)BookingRequestViewListColumn.ClientId)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.ClientId)];
					//entity.ClientId = (Convert.IsDBNull(reader["ClientId"]))?(long)0:(System.Int64?)reader["ClientId"];
					entity.ContractId = (reader.IsDBNull(((int)BookingRequestViewListColumn.ContractId)))?null:(System.String)reader[((int)BookingRequestViewListColumn.ContractId)];
					//entity.ContractId = (Convert.IsDBNull(reader["ContractId"]))?string.Empty:(System.String)reader["ContractId"];
					entity.StaffId = (System.Int64)reader[((int)BookingRequestViewListColumn.StaffId)];
					//entity.StaffId = (Convert.IsDBNull(reader["StaffId"]))?(long)0:(System.Int64)reader["StaffId"];
					entity.CurrencyId = (System.Int64)reader[((int)BookingRequestViewListColumn.CurrencyId)];
					//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyId"]))?(long)0:(System.Int64)reader["CurrencyId"];
					entity.ContractValue = (reader.IsDBNull(((int)BookingRequestViewListColumn.ContractValue)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.ContractValue)];
					//entity.ContractValue = (Convert.IsDBNull(reader["ContractValue"]))?0.0m:(System.Decimal?)reader["ContractValue"];
					entity.TimeZone = (reader.IsDBNull(((int)BookingRequestViewListColumn.TimeZone)))?null:(System.String)reader[((int)BookingRequestViewListColumn.TimeZone)];
					//entity.TimeZone = (Convert.IsDBNull(reader["TimeZone"]))?string.Empty:(System.String)reader["TimeZone"];
					entity.Phone = (reader.IsDBNull(((int)BookingRequestViewListColumn.Phone)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Phone)];
					//entity.Phone = (Convert.IsDBNull(reader["Phone"]))?string.Empty:(System.String)reader["Phone"];
					entity.PhoneExt = (reader.IsDBNull(((int)BookingRequestViewListColumn.PhoneExt)))?null:(System.String)reader[((int)BookingRequestViewListColumn.PhoneExt)];
					//entity.PhoneExt = (Convert.IsDBNull(reader["PhoneExt"]))?string.Empty:(System.String)reader["PhoneExt"];
					entity.RtMFrom = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtMFrom)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtMFrom)];
					//entity.RtMFrom = (Convert.IsDBNull(reader["RT_M_From"]))?string.Empty:(System.String)reader["RT_M_From"];
					entity.RtMTo = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtMTo)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtMTo)];
					//entity.RtMTo = (Convert.IsDBNull(reader["RT_M_To"]))?string.Empty:(System.String)reader["RT_M_To"];
					entity.RtAFrom = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtAFrom)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtAFrom)];
					//entity.RtAFrom = (Convert.IsDBNull(reader["RT_A_From"]))?string.Empty:(System.String)reader["RT_A_From"];
					entity.RtATo = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtATo)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtATo)];
					//entity.RtATo = (Convert.IsDBNull(reader["RT_A_To"]))?string.Empty:(System.String)reader["RT_A_To"];
					entity.RtFFrom = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtFFrom)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtFFrom)];
					//entity.RtFFrom = (Convert.IsDBNull(reader["RT_F_From"]))?string.Empty:(System.String)reader["RT_F_From"];
					entity.RtFTo = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtFTo)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtFTo)];
					//entity.RtFTo = (Convert.IsDBNull(reader["RT_F_To"]))?string.Empty:(System.String)reader["RT_F_To"];
					entity.IsCreditCardExcepted = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsCreditCardExcepted)];
					//entity.IsCreditCardExcepted = (Convert.IsDBNull(reader["IsCreditCardExcepted"]))?false:(System.Boolean)reader["IsCreditCardExcepted"];
					entity.CreditCardType = (reader.IsDBNull(((int)BookingRequestViewListColumn.CreditCardType)))?null:(System.String)reader[((int)BookingRequestViewListColumn.CreditCardType)];
					//entity.CreditCardType = (Convert.IsDBNull(reader["CreditCardType"]))?string.Empty:(System.String)reader["CreditCardType"];
					entity.Theme = (reader.IsDBNull(((int)BookingRequestViewListColumn.Theme)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Theme)];
					//entity.Theme = (Convert.IsDBNull(reader["Theme"]))?string.Empty:(System.String)reader["Theme"];
					entity.IsBedroomAvailable = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsBedroomAvailable)];
					//entity.IsBedroomAvailable = (Convert.IsDBNull(reader["IsBedroomAvailable"]))?false:(System.Boolean)reader["IsBedroomAvailable"];
					entity.NumberOfBedroom = (reader.IsDBNull(((int)BookingRequestViewListColumn.NumberOfBedroom)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.NumberOfBedroom)];
					//entity.NumberOfBedroom = (Convert.IsDBNull(reader["NumberOfBedroom"]))?(int)0:(System.Int32?)reader["NumberOfBedroom"];
					entity.Email = (reader.IsDBNull(((int)BookingRequestViewListColumn.Email)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.ContactPerson = (reader.IsDBNull(((int)BookingRequestViewListColumn.ContactPerson)))?null:(System.String)reader[((int)BookingRequestViewListColumn.ContactPerson)];
					//entity.ContactPerson = (Convert.IsDBNull(reader["ContactPerson"]))?string.Empty:(System.String)reader["ContactPerson"];
					entity.PhoneNumber = (reader.IsDBNull(((int)BookingRequestViewListColumn.PhoneNumber)))?null:(System.String)reader[((int)BookingRequestViewListColumn.PhoneNumber)];
					//entity.PhoneNumber = (Convert.IsDBNull(reader["PhoneNumber"]))?string.Empty:(System.String)reader["PhoneNumber"];
					entity.GoOnline = (reader.IsDBNull(((int)BookingRequestViewListColumn.GoOnline)))?null:(System.Boolean?)reader[((int)BookingRequestViewListColumn.GoOnline)];
					//entity.GoOnline = (Convert.IsDBNull(reader["GoOnline"]))?false:(System.Boolean?)reader["GoOnline"];
					entity.ZipCode = (reader.IsDBNull(((int)BookingRequestViewListColumn.ZipCode)))?null:(System.String)reader[((int)BookingRequestViewListColumn.ZipCode)];
					//entity.ZipCode = (Convert.IsDBNull(reader["ZipCode"]))?string.Empty:(System.String)reader["ZipCode"];
					entity.UpdatedBy = (reader.IsDBNull(((int)BookingRequestViewListColumn.UpdatedBy)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.UpdatedBy)];
					//entity.UpdatedBy = (Convert.IsDBNull(reader["UpdatedBy"]))?(long)0:(System.Int64?)reader["UpdatedBy"];
					entity.HotelAddress = (reader.IsDBNull(((int)BookingRequestViewListColumn.HotelAddress)))?null:(System.String)reader[((int)BookingRequestViewListColumn.HotelAddress)];
					//entity.HotelAddress = (Convert.IsDBNull(reader["HotelAddress"]))?string.Empty:(System.String)reader["HotelAddress"];
					entity.CreationDate = (System.DateTime)reader[((int)BookingRequestViewListColumn.CreationDate)];
					//entity.CreationDate = (Convert.IsDBNull(reader["CreationDate"]))?DateTime.MinValue:(System.DateTime)reader["CreationDate"];
					entity.CountryCodePhone = (reader.IsDBNull(((int)BookingRequestViewListColumn.CountryCodePhone)))?null:(System.String)reader[((int)BookingRequestViewListColumn.CountryCodePhone)];
					//entity.CountryCodePhone = (Convert.IsDBNull(reader["CountryCodePhone"]))?string.Empty:(System.String)reader["CountryCodePhone"];
					entity.CountryCodeFax = (reader.IsDBNull(((int)BookingRequestViewListColumn.CountryCodeFax)))?null:(System.String)reader[((int)BookingRequestViewListColumn.CountryCodeFax)];
					//entity.CountryCodeFax = (Convert.IsDBNull(reader["CountryCodeFax"]))?string.Empty:(System.String)reader["CountryCodeFax"];
					entity.FaxNumber = (reader.IsDBNull(((int)BookingRequestViewListColumn.FaxNumber)))?null:(System.String)reader[((int)BookingRequestViewListColumn.FaxNumber)];
					//entity.FaxNumber = (Convert.IsDBNull(reader["FaxNumber"]))?string.Empty:(System.String)reader["FaxNumber"];
					entity.IsRemoved = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsRemoved)];
					//entity.IsRemoved = (Convert.IsDBNull(reader["IsRemoved"]))?false:(System.Boolean)reader["IsRemoved"];
					entity.RequestGoOnline = (reader.IsDBNull(((int)BookingRequestViewListColumn.RequestGoOnline)))?null:(System.Boolean?)reader[((int)BookingRequestViewListColumn.RequestGoOnline)];
					//entity.RequestGoOnline = (Convert.IsDBNull(reader["RequestGoOnline"]))?false:(System.Boolean?)reader["RequestGoOnline"];
					entity.HotelUserId = (reader.IsDBNull(((int)BookingRequestViewListColumn.HotelUserId)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.HotelUserId)];
					//entity.HotelUserId = (Convert.IsDBNull(reader["HotelUserID"]))?(long)0:(System.Int64?)reader["HotelUserID"];
					entity.AccountingOfficer = (reader.IsDBNull(((int)BookingRequestViewListColumn.AccountingOfficer)))?null:(System.String)reader[((int)BookingRequestViewListColumn.AccountingOfficer)];
					//entity.AccountingOfficer = (Convert.IsDBNull(reader["AccountingOfficer"]))?string.Empty:(System.String)reader["AccountingOfficer"];
					entity.MeetingRoomId = (System.Int64)reader[((int)BookingRequestViewListColumn.MeetingRoomId)];
					//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomId"]))?(long)0:(System.Int64)reader["MeetingRoomId"];
					entity.MeetingRoomName = (reader.IsDBNull(((int)BookingRequestViewListColumn.MeetingRoomName)))?null:(System.String)reader[((int)BookingRequestViewListColumn.MeetingRoomName)];
					//entity.MeetingRoomName = (Convert.IsDBNull(reader["MeetingRoomName"]))?string.Empty:(System.String)reader["MeetingRoomName"];
					entity.Picture = (reader.IsDBNull(((int)BookingRequestViewListColumn.Picture)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Picture)];
					//entity.Picture = (Convert.IsDBNull(reader["Picture"]))?string.Empty:(System.String)reader["Picture"];
					entity.DayLight = (System.Int32)reader[((int)BookingRequestViewListColumn.DayLight)];
					//entity.DayLight = (Convert.IsDBNull(reader["DayLight"]))?(int)0:(System.Int32)reader["DayLight"];
					entity.Surface = (reader.IsDBNull(((int)BookingRequestViewListColumn.Surface)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.Surface)];
					//entity.Surface = (Convert.IsDBNull(reader["Surface"]))?(long)0:(System.Int64?)reader["Surface"];
					entity.Height = (reader.IsDBNull(((int)BookingRequestViewListColumn.Height)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.Height)];
					//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
					entity.MeetingRoomActive = (System.Boolean)reader[((int)BookingRequestViewListColumn.MeetingRoomActive)];
					//entity.MeetingRoomActive = (Convert.IsDBNull(reader["MeetingRoomActive"]))?false:(System.Boolean)reader["MeetingRoomActive"];
					entity.MrPlan = (reader.IsDBNull(((int)BookingRequestViewListColumn.MrPlan)))?null:(System.String)reader[((int)BookingRequestViewListColumn.MrPlan)];
					//entity.MrPlan = (Convert.IsDBNull(reader["MR_Plan"]))?string.Empty:(System.String)reader["MR_Plan"];
					entity.OrderNumber = (reader.IsDBNull(((int)BookingRequestViewListColumn.OrderNumber)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.OrderNumber)];
					//entity.OrderNumber = (Convert.IsDBNull(reader["OrderNumber"]))?(int)0:(System.Int32?)reader["OrderNumber"];
					entity.HalfdayPrice = (reader.IsDBNull(((int)BookingRequestViewListColumn.HalfdayPrice)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.HalfdayPrice)];
					//entity.HalfdayPrice = (Convert.IsDBNull(reader["HalfdayPrice"]))?0.0m:(System.Decimal?)reader["HalfdayPrice"];
					entity.FulldayPrice = (reader.IsDBNull(((int)BookingRequestViewListColumn.FulldayPrice)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.FulldayPrice)];
					//entity.FulldayPrice = (Convert.IsDBNull(reader["FulldayPrice"]))?0.0m:(System.Decimal?)reader["FulldayPrice"];
					entity.IsOnline = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsOnline)];
					//entity.IsOnline = (Convert.IsDBNull(reader["IsOnline"]))?false:(System.Boolean)reader["IsOnline"];
					entity.IsDeleted = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsDeleted)];
					//entity.IsDeleted = (Convert.IsDBNull(reader["IsDeleted"]))?false:(System.Boolean)reader["IsDeleted"];
					entity.SppId = (System.Int64)reader[((int)BookingRequestViewListColumn.SppId)];
					//entity.SppId = (Convert.IsDBNull(reader["SPPId"]))?(long)0:(System.Int64)reader["SPPId"];
					entity.SpecialPriceDate = (reader.IsDBNull(((int)BookingRequestViewListColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)BookingRequestViewListColumn.SpecialPriceDate)];
					//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
					entity.DdrPercent = (reader.IsDBNull(((int)BookingRequestViewListColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.DdrPercent)];
					//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
					entity.MeetingRoomPercent = (reader.IsDBNull(((int)BookingRequestViewListColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.MeetingRoomPercent)];
					//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
					entity.AvailabilityId = (System.Int64)reader[((int)BookingRequestViewListColumn.AvailabilityId)];
					//entity.AvailabilityId = (Convert.IsDBNull(reader["AvailabilityId"]))?(long)0:(System.Int64)reader["AvailabilityId"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)BookingRequestViewListColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)BookingRequestViewListColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.FullPropertyStatus = (reader.IsDBNull(((int)BookingRequestViewListColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.FullPropertyStatus)];
					//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
					entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)BookingRequestViewListColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.LeadTimeForMeetingRoom)];
					//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
					entity.FullProrertyStatusBr = (reader.IsDBNull(((int)BookingRequestViewListColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.FullProrertyStatusBr)];
					//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
					entity.AvailibilityOfRoomsId = (System.Int64)reader[((int)BookingRequestViewListColumn.AvailibilityOfRoomsId)];
					//entity.AvailibilityOfRoomsId = (Convert.IsDBNull(reader["AvailibilityOfRoomsId"]))?(long)0:(System.Int64)reader["AvailibilityOfRoomsId"];
					entity.MorningStatus = (reader.IsDBNull(((int)BookingRequestViewListColumn.MorningStatus)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.MorningStatus)];
					//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
					entity.AfternoonStatus = (reader.IsDBNull(((int)BookingRequestViewListColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.AfternoonStatus)];
					//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
					entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)BookingRequestViewListColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.NumberOfRoomsAvailable)];
					//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
					entity.RoomType = (reader.IsDBNull(((int)BookingRequestViewListColumn.RoomType)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.RoomType)];
					//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
					entity.MeetingRoomConfigId = (System.Int64)reader[((int)BookingRequestViewListColumn.MeetingRoomConfigId)];
					//entity.MeetingRoomConfigId = (Convert.IsDBNull(reader["MeetingRoomConfigId"]))?(long)0:(System.Int64)reader["MeetingRoomConfigId"];
					entity.RoomShapeId = (reader.IsDBNull(((int)BookingRequestViewListColumn.RoomShapeId)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.RoomShapeId)];
					//entity.RoomShapeId = (Convert.IsDBNull(reader["RoomShapeId"]))?(int)0:(System.Int32?)reader["RoomShapeId"];
					entity.MinCapacity = (reader.IsDBNull(((int)BookingRequestViewListColumn.MinCapacity)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.MinCapacity)];
					//entity.MinCapacity = (Convert.IsDBNull(reader["MinCapacity"]))?(int)0:(System.Int32?)reader["MinCapacity"];
					entity.MaxCapicity = (reader.IsDBNull(((int)BookingRequestViewListColumn.MaxCapicity)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.MaxCapicity)];
					//entity.MaxCapicity = (Convert.IsDBNull(reader["MaxCapicity"]))?(int)0:(System.Int32?)reader["MaxCapicity"];
					entity.BookingAlgo = (reader.IsDBNull(((int)BookingRequestViewListColumn.BookingAlgo)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.BookingAlgo)];
					//entity.BookingAlgo = (Convert.IsDBNull(reader["BookingAlgo"]))?0.0m:(System.Decimal?)reader["BookingAlgo"];
					entity.RequestAlgo = (reader.IsDBNull(((int)BookingRequestViewListColumn.RequestAlgo)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.RequestAlgo)];
					//entity.RequestAlgo = (Convert.IsDBNull(reader["RequestAlgo"]))?0.0m:(System.Decimal?)reader["RequestAlgo"];
					entity.IsPriority = (System.String)reader[((int)BookingRequestViewListColumn.IsPriority)];
					//entity.IsPriority = (Convert.IsDBNull(reader["IsPriority"]))?string.Empty:(System.String)reader["IsPriority"];
					entity.ActualPkgPrice = (reader.IsDBNull(((int)BookingRequestViewListColumn.ActualPkgPrice)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.ActualPkgPrice)];
					//entity.ActualPkgPrice = (Convert.IsDBNull(reader["ActualPkgPrice"]))?0.0m:(System.Decimal?)reader["ActualPkgPrice"];
					entity.ActualPkgPriceHalfDay = (reader.IsDBNull(((int)BookingRequestViewListColumn.ActualPkgPriceHalfDay)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.ActualPkgPriceHalfDay)];
					//entity.ActualPkgPriceHalfDay = (Convert.IsDBNull(reader["ActualPkgPriceHalfDay"]))?0.0m:(System.Decimal?)reader["ActualPkgPriceHalfDay"];
					entity.NumberOfRoomBooked = (reader.IsDBNull(((int)BookingRequestViewListColumn.NumberOfRoomBooked)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.NumberOfRoomBooked)];
					//entity.NumberOfRoomBooked = (Convert.IsDBNull(reader["NumberOfRoomBooked"]))?(long)0:(System.Int64?)reader["NumberOfRoomBooked"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="BookingRequestViewList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="BookingRequestViewList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, BookingRequestViewList entity)
		{
			reader.Read();
			entity.HotelId = (System.Int64)reader[((int)BookingRequestViewListColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.HotelName = (reader.IsDBNull(((int)BookingRequestViewListColumn.HotelName)))?null:(System.String)reader[((int)BookingRequestViewListColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.CountryId = (System.Int64)reader[((int)BookingRequestViewListColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
			entity.CityId = (System.Int64)reader[((int)BookingRequestViewListColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
			entity.ZoneId = (System.Int64)reader[((int)BookingRequestViewListColumn.ZoneId)];
			//entity.ZoneId = (Convert.IsDBNull(reader["ZoneId"]))?(long)0:(System.Int64)reader["ZoneId"];
			entity.Stars = (reader.IsDBNull(((int)BookingRequestViewListColumn.Stars)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.Stars)];
			//entity.Stars = (Convert.IsDBNull(reader["Stars"]))?(int)0:(System.Int32?)reader["Stars"];
			entity.Longitude = (reader.IsDBNull(((int)BookingRequestViewListColumn.Longitude)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Longitude)];
			//entity.Longitude = (Convert.IsDBNull(reader["Longitude"]))?string.Empty:(System.String)reader["Longitude"];
			entity.Latitude = (reader.IsDBNull(((int)BookingRequestViewListColumn.Latitude)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Latitude)];
			//entity.Latitude = (Convert.IsDBNull(reader["Latitude"]))?string.Empty:(System.String)reader["Latitude"];
			entity.Logo = (reader.IsDBNull(((int)BookingRequestViewListColumn.Logo)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Logo)];
			//entity.Logo = (Convert.IsDBNull(reader["Logo"]))?string.Empty:(System.String)reader["Logo"];
			entity.IsActive = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsActive)];
			//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
			entity.CancelationPolicyId = (System.Int64)reader[((int)BookingRequestViewListColumn.CancelationPolicyId)];
			//entity.CancelationPolicyId = (Convert.IsDBNull(reader["CancelationPolicyId"]))?(long)0:(System.Int64)reader["CancelationPolicyId"];
			entity.GroupId = (reader.IsDBNull(((int)BookingRequestViewListColumn.GroupId)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.GroupId)];
			//entity.GroupId = (Convert.IsDBNull(reader["GroupId"]))?(long)0:(System.Int64?)reader["GroupId"];
			entity.OperatorChoice = (reader.IsDBNull(((int)BookingRequestViewListColumn.OperatorChoice)))?null:(System.String)reader[((int)BookingRequestViewListColumn.OperatorChoice)];
			//entity.OperatorChoice = (Convert.IsDBNull(reader["OperatorChoice"]))?string.Empty:(System.String)reader["OperatorChoice"];
			entity.HotelPlan = (reader.IsDBNull(((int)BookingRequestViewListColumn.HotelPlan)))?null:(System.String)reader[((int)BookingRequestViewListColumn.HotelPlan)];
			//entity.HotelPlan = (Convert.IsDBNull(reader["HotelPlan"]))?string.Empty:(System.String)reader["HotelPlan"];
			entity.ClientId = (reader.IsDBNull(((int)BookingRequestViewListColumn.ClientId)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.ClientId)];
			//entity.ClientId = (Convert.IsDBNull(reader["ClientId"]))?(long)0:(System.Int64?)reader["ClientId"];
			entity.ContractId = (reader.IsDBNull(((int)BookingRequestViewListColumn.ContractId)))?null:(System.String)reader[((int)BookingRequestViewListColumn.ContractId)];
			//entity.ContractId = (Convert.IsDBNull(reader["ContractId"]))?string.Empty:(System.String)reader["ContractId"];
			entity.StaffId = (System.Int64)reader[((int)BookingRequestViewListColumn.StaffId)];
			//entity.StaffId = (Convert.IsDBNull(reader["StaffId"]))?(long)0:(System.Int64)reader["StaffId"];
			entity.CurrencyId = (System.Int64)reader[((int)BookingRequestViewListColumn.CurrencyId)];
			//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyId"]))?(long)0:(System.Int64)reader["CurrencyId"];
			entity.ContractValue = (reader.IsDBNull(((int)BookingRequestViewListColumn.ContractValue)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.ContractValue)];
			//entity.ContractValue = (Convert.IsDBNull(reader["ContractValue"]))?0.0m:(System.Decimal?)reader["ContractValue"];
			entity.TimeZone = (reader.IsDBNull(((int)BookingRequestViewListColumn.TimeZone)))?null:(System.String)reader[((int)BookingRequestViewListColumn.TimeZone)];
			//entity.TimeZone = (Convert.IsDBNull(reader["TimeZone"]))?string.Empty:(System.String)reader["TimeZone"];
			entity.Phone = (reader.IsDBNull(((int)BookingRequestViewListColumn.Phone)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Phone)];
			//entity.Phone = (Convert.IsDBNull(reader["Phone"]))?string.Empty:(System.String)reader["Phone"];
			entity.PhoneExt = (reader.IsDBNull(((int)BookingRequestViewListColumn.PhoneExt)))?null:(System.String)reader[((int)BookingRequestViewListColumn.PhoneExt)];
			//entity.PhoneExt = (Convert.IsDBNull(reader["PhoneExt"]))?string.Empty:(System.String)reader["PhoneExt"];
			entity.RtMFrom = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtMFrom)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtMFrom)];
			//entity.RtMFrom = (Convert.IsDBNull(reader["RT_M_From"]))?string.Empty:(System.String)reader["RT_M_From"];
			entity.RtMTo = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtMTo)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtMTo)];
			//entity.RtMTo = (Convert.IsDBNull(reader["RT_M_To"]))?string.Empty:(System.String)reader["RT_M_To"];
			entity.RtAFrom = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtAFrom)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtAFrom)];
			//entity.RtAFrom = (Convert.IsDBNull(reader["RT_A_From"]))?string.Empty:(System.String)reader["RT_A_From"];
			entity.RtATo = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtATo)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtATo)];
			//entity.RtATo = (Convert.IsDBNull(reader["RT_A_To"]))?string.Empty:(System.String)reader["RT_A_To"];
			entity.RtFFrom = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtFFrom)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtFFrom)];
			//entity.RtFFrom = (Convert.IsDBNull(reader["RT_F_From"]))?string.Empty:(System.String)reader["RT_F_From"];
			entity.RtFTo = (reader.IsDBNull(((int)BookingRequestViewListColumn.RtFTo)))?null:(System.String)reader[((int)BookingRequestViewListColumn.RtFTo)];
			//entity.RtFTo = (Convert.IsDBNull(reader["RT_F_To"]))?string.Empty:(System.String)reader["RT_F_To"];
			entity.IsCreditCardExcepted = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsCreditCardExcepted)];
			//entity.IsCreditCardExcepted = (Convert.IsDBNull(reader["IsCreditCardExcepted"]))?false:(System.Boolean)reader["IsCreditCardExcepted"];
			entity.CreditCardType = (reader.IsDBNull(((int)BookingRequestViewListColumn.CreditCardType)))?null:(System.String)reader[((int)BookingRequestViewListColumn.CreditCardType)];
			//entity.CreditCardType = (Convert.IsDBNull(reader["CreditCardType"]))?string.Empty:(System.String)reader["CreditCardType"];
			entity.Theme = (reader.IsDBNull(((int)BookingRequestViewListColumn.Theme)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Theme)];
			//entity.Theme = (Convert.IsDBNull(reader["Theme"]))?string.Empty:(System.String)reader["Theme"];
			entity.IsBedroomAvailable = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsBedroomAvailable)];
			//entity.IsBedroomAvailable = (Convert.IsDBNull(reader["IsBedroomAvailable"]))?false:(System.Boolean)reader["IsBedroomAvailable"];
			entity.NumberOfBedroom = (reader.IsDBNull(((int)BookingRequestViewListColumn.NumberOfBedroom)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.NumberOfBedroom)];
			//entity.NumberOfBedroom = (Convert.IsDBNull(reader["NumberOfBedroom"]))?(int)0:(System.Int32?)reader["NumberOfBedroom"];
			entity.Email = (reader.IsDBNull(((int)BookingRequestViewListColumn.Email)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			entity.ContactPerson = (reader.IsDBNull(((int)BookingRequestViewListColumn.ContactPerson)))?null:(System.String)reader[((int)BookingRequestViewListColumn.ContactPerson)];
			//entity.ContactPerson = (Convert.IsDBNull(reader["ContactPerson"]))?string.Empty:(System.String)reader["ContactPerson"];
			entity.PhoneNumber = (reader.IsDBNull(((int)BookingRequestViewListColumn.PhoneNumber)))?null:(System.String)reader[((int)BookingRequestViewListColumn.PhoneNumber)];
			//entity.PhoneNumber = (Convert.IsDBNull(reader["PhoneNumber"]))?string.Empty:(System.String)reader["PhoneNumber"];
			entity.GoOnline = (reader.IsDBNull(((int)BookingRequestViewListColumn.GoOnline)))?null:(System.Boolean?)reader[((int)BookingRequestViewListColumn.GoOnline)];
			//entity.GoOnline = (Convert.IsDBNull(reader["GoOnline"]))?false:(System.Boolean?)reader["GoOnline"];
			entity.ZipCode = (reader.IsDBNull(((int)BookingRequestViewListColumn.ZipCode)))?null:(System.String)reader[((int)BookingRequestViewListColumn.ZipCode)];
			//entity.ZipCode = (Convert.IsDBNull(reader["ZipCode"]))?string.Empty:(System.String)reader["ZipCode"];
			entity.UpdatedBy = (reader.IsDBNull(((int)BookingRequestViewListColumn.UpdatedBy)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.UpdatedBy)];
			//entity.UpdatedBy = (Convert.IsDBNull(reader["UpdatedBy"]))?(long)0:(System.Int64?)reader["UpdatedBy"];
			entity.HotelAddress = (reader.IsDBNull(((int)BookingRequestViewListColumn.HotelAddress)))?null:(System.String)reader[((int)BookingRequestViewListColumn.HotelAddress)];
			//entity.HotelAddress = (Convert.IsDBNull(reader["HotelAddress"]))?string.Empty:(System.String)reader["HotelAddress"];
			entity.CreationDate = (System.DateTime)reader[((int)BookingRequestViewListColumn.CreationDate)];
			//entity.CreationDate = (Convert.IsDBNull(reader["CreationDate"]))?DateTime.MinValue:(System.DateTime)reader["CreationDate"];
			entity.CountryCodePhone = (reader.IsDBNull(((int)BookingRequestViewListColumn.CountryCodePhone)))?null:(System.String)reader[((int)BookingRequestViewListColumn.CountryCodePhone)];
			//entity.CountryCodePhone = (Convert.IsDBNull(reader["CountryCodePhone"]))?string.Empty:(System.String)reader["CountryCodePhone"];
			entity.CountryCodeFax = (reader.IsDBNull(((int)BookingRequestViewListColumn.CountryCodeFax)))?null:(System.String)reader[((int)BookingRequestViewListColumn.CountryCodeFax)];
			//entity.CountryCodeFax = (Convert.IsDBNull(reader["CountryCodeFax"]))?string.Empty:(System.String)reader["CountryCodeFax"];
			entity.FaxNumber = (reader.IsDBNull(((int)BookingRequestViewListColumn.FaxNumber)))?null:(System.String)reader[((int)BookingRequestViewListColumn.FaxNumber)];
			//entity.FaxNumber = (Convert.IsDBNull(reader["FaxNumber"]))?string.Empty:(System.String)reader["FaxNumber"];
			entity.IsRemoved = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsRemoved)];
			//entity.IsRemoved = (Convert.IsDBNull(reader["IsRemoved"]))?false:(System.Boolean)reader["IsRemoved"];
			entity.RequestGoOnline = (reader.IsDBNull(((int)BookingRequestViewListColumn.RequestGoOnline)))?null:(System.Boolean?)reader[((int)BookingRequestViewListColumn.RequestGoOnline)];
			//entity.RequestGoOnline = (Convert.IsDBNull(reader["RequestGoOnline"]))?false:(System.Boolean?)reader["RequestGoOnline"];
			entity.HotelUserId = (reader.IsDBNull(((int)BookingRequestViewListColumn.HotelUserId)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.HotelUserId)];
			//entity.HotelUserId = (Convert.IsDBNull(reader["HotelUserID"]))?(long)0:(System.Int64?)reader["HotelUserID"];
			entity.AccountingOfficer = (reader.IsDBNull(((int)BookingRequestViewListColumn.AccountingOfficer)))?null:(System.String)reader[((int)BookingRequestViewListColumn.AccountingOfficer)];
			//entity.AccountingOfficer = (Convert.IsDBNull(reader["AccountingOfficer"]))?string.Empty:(System.String)reader["AccountingOfficer"];
			entity.MeetingRoomId = (System.Int64)reader[((int)BookingRequestViewListColumn.MeetingRoomId)];
			//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomId"]))?(long)0:(System.Int64)reader["MeetingRoomId"];
			entity.MeetingRoomName = (reader.IsDBNull(((int)BookingRequestViewListColumn.MeetingRoomName)))?null:(System.String)reader[((int)BookingRequestViewListColumn.MeetingRoomName)];
			//entity.MeetingRoomName = (Convert.IsDBNull(reader["MeetingRoomName"]))?string.Empty:(System.String)reader["MeetingRoomName"];
			entity.Picture = (reader.IsDBNull(((int)BookingRequestViewListColumn.Picture)))?null:(System.String)reader[((int)BookingRequestViewListColumn.Picture)];
			//entity.Picture = (Convert.IsDBNull(reader["Picture"]))?string.Empty:(System.String)reader["Picture"];
			entity.DayLight = (System.Int32)reader[((int)BookingRequestViewListColumn.DayLight)];
			//entity.DayLight = (Convert.IsDBNull(reader["DayLight"]))?(int)0:(System.Int32)reader["DayLight"];
			entity.Surface = (reader.IsDBNull(((int)BookingRequestViewListColumn.Surface)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.Surface)];
			//entity.Surface = (Convert.IsDBNull(reader["Surface"]))?(long)0:(System.Int64?)reader["Surface"];
			entity.Height = (reader.IsDBNull(((int)BookingRequestViewListColumn.Height)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.Height)];
			//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
			entity.MeetingRoomActive = (System.Boolean)reader[((int)BookingRequestViewListColumn.MeetingRoomActive)];
			//entity.MeetingRoomActive = (Convert.IsDBNull(reader["MeetingRoomActive"]))?false:(System.Boolean)reader["MeetingRoomActive"];
			entity.MrPlan = (reader.IsDBNull(((int)BookingRequestViewListColumn.MrPlan)))?null:(System.String)reader[((int)BookingRequestViewListColumn.MrPlan)];
			//entity.MrPlan = (Convert.IsDBNull(reader["MR_Plan"]))?string.Empty:(System.String)reader["MR_Plan"];
			entity.OrderNumber = (reader.IsDBNull(((int)BookingRequestViewListColumn.OrderNumber)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.OrderNumber)];
			//entity.OrderNumber = (Convert.IsDBNull(reader["OrderNumber"]))?(int)0:(System.Int32?)reader["OrderNumber"];
			entity.HalfdayPrice = (reader.IsDBNull(((int)BookingRequestViewListColumn.HalfdayPrice)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.HalfdayPrice)];
			//entity.HalfdayPrice = (Convert.IsDBNull(reader["HalfdayPrice"]))?0.0m:(System.Decimal?)reader["HalfdayPrice"];
			entity.FulldayPrice = (reader.IsDBNull(((int)BookingRequestViewListColumn.FulldayPrice)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.FulldayPrice)];
			//entity.FulldayPrice = (Convert.IsDBNull(reader["FulldayPrice"]))?0.0m:(System.Decimal?)reader["FulldayPrice"];
			entity.IsOnline = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsOnline)];
			//entity.IsOnline = (Convert.IsDBNull(reader["IsOnline"]))?false:(System.Boolean)reader["IsOnline"];
			entity.IsDeleted = (System.Boolean)reader[((int)BookingRequestViewListColumn.IsDeleted)];
			//entity.IsDeleted = (Convert.IsDBNull(reader["IsDeleted"]))?false:(System.Boolean)reader["IsDeleted"];
			entity.SppId = (System.Int64)reader[((int)BookingRequestViewListColumn.SppId)];
			//entity.SppId = (Convert.IsDBNull(reader["SPPId"]))?(long)0:(System.Int64)reader["SPPId"];
			entity.SpecialPriceDate = (reader.IsDBNull(((int)BookingRequestViewListColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)BookingRequestViewListColumn.SpecialPriceDate)];
			//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
			entity.DdrPercent = (reader.IsDBNull(((int)BookingRequestViewListColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.DdrPercent)];
			//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
			entity.MeetingRoomPercent = (reader.IsDBNull(((int)BookingRequestViewListColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.MeetingRoomPercent)];
			//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
			entity.AvailabilityId = (System.Int64)reader[((int)BookingRequestViewListColumn.AvailabilityId)];
			//entity.AvailabilityId = (Convert.IsDBNull(reader["AvailabilityId"]))?(long)0:(System.Int64)reader["AvailabilityId"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)BookingRequestViewListColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)BookingRequestViewListColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.FullPropertyStatus = (reader.IsDBNull(((int)BookingRequestViewListColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.FullPropertyStatus)];
			//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
			entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)BookingRequestViewListColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.LeadTimeForMeetingRoom)];
			//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
			entity.FullProrertyStatusBr = (reader.IsDBNull(((int)BookingRequestViewListColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.FullProrertyStatusBr)];
			//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
			entity.AvailibilityOfRoomsId = (System.Int64)reader[((int)BookingRequestViewListColumn.AvailibilityOfRoomsId)];
			//entity.AvailibilityOfRoomsId = (Convert.IsDBNull(reader["AvailibilityOfRoomsId"]))?(long)0:(System.Int64)reader["AvailibilityOfRoomsId"];
			entity.MorningStatus = (reader.IsDBNull(((int)BookingRequestViewListColumn.MorningStatus)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.MorningStatus)];
			//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
			entity.AfternoonStatus = (reader.IsDBNull(((int)BookingRequestViewListColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.AfternoonStatus)];
			//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)BookingRequestViewListColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.NumberOfRoomsAvailable)];
			//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
			entity.RoomType = (reader.IsDBNull(((int)BookingRequestViewListColumn.RoomType)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.RoomType)];
			//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
			entity.MeetingRoomConfigId = (System.Int64)reader[((int)BookingRequestViewListColumn.MeetingRoomConfigId)];
			//entity.MeetingRoomConfigId = (Convert.IsDBNull(reader["MeetingRoomConfigId"]))?(long)0:(System.Int64)reader["MeetingRoomConfigId"];
			entity.RoomShapeId = (reader.IsDBNull(((int)BookingRequestViewListColumn.RoomShapeId)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.RoomShapeId)];
			//entity.RoomShapeId = (Convert.IsDBNull(reader["RoomShapeId"]))?(int)0:(System.Int32?)reader["RoomShapeId"];
			entity.MinCapacity = (reader.IsDBNull(((int)BookingRequestViewListColumn.MinCapacity)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.MinCapacity)];
			//entity.MinCapacity = (Convert.IsDBNull(reader["MinCapacity"]))?(int)0:(System.Int32?)reader["MinCapacity"];
			entity.MaxCapicity = (reader.IsDBNull(((int)BookingRequestViewListColumn.MaxCapicity)))?null:(System.Int32?)reader[((int)BookingRequestViewListColumn.MaxCapicity)];
			//entity.MaxCapicity = (Convert.IsDBNull(reader["MaxCapicity"]))?(int)0:(System.Int32?)reader["MaxCapicity"];
			entity.BookingAlgo = (reader.IsDBNull(((int)BookingRequestViewListColumn.BookingAlgo)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.BookingAlgo)];
			//entity.BookingAlgo = (Convert.IsDBNull(reader["BookingAlgo"]))?0.0m:(System.Decimal?)reader["BookingAlgo"];
			entity.RequestAlgo = (reader.IsDBNull(((int)BookingRequestViewListColumn.RequestAlgo)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.RequestAlgo)];
			//entity.RequestAlgo = (Convert.IsDBNull(reader["RequestAlgo"]))?0.0m:(System.Decimal?)reader["RequestAlgo"];
			entity.IsPriority = (System.String)reader[((int)BookingRequestViewListColumn.IsPriority)];
			//entity.IsPriority = (Convert.IsDBNull(reader["IsPriority"]))?string.Empty:(System.String)reader["IsPriority"];
			entity.ActualPkgPrice = (reader.IsDBNull(((int)BookingRequestViewListColumn.ActualPkgPrice)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.ActualPkgPrice)];
			//entity.ActualPkgPrice = (Convert.IsDBNull(reader["ActualPkgPrice"]))?0.0m:(System.Decimal?)reader["ActualPkgPrice"];
			entity.ActualPkgPriceHalfDay = (reader.IsDBNull(((int)BookingRequestViewListColumn.ActualPkgPriceHalfDay)))?null:(System.Decimal?)reader[((int)BookingRequestViewListColumn.ActualPkgPriceHalfDay)];
			//entity.ActualPkgPriceHalfDay = (Convert.IsDBNull(reader["ActualPkgPriceHalfDay"]))?0.0m:(System.Decimal?)reader["ActualPkgPriceHalfDay"];
			entity.NumberOfRoomBooked = (reader.IsDBNull(((int)BookingRequestViewListColumn.NumberOfRoomBooked)))?null:(System.Int64?)reader[((int)BookingRequestViewListColumn.NumberOfRoomBooked)];
			//entity.NumberOfRoomBooked = (Convert.IsDBNull(reader["NumberOfRoomBooked"]))?(long)0:(System.Int64?)reader["NumberOfRoomBooked"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="BookingRequestViewList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="BookingRequestViewList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, BookingRequestViewList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryId"]))?(long)0:(System.Int64)dataRow["CountryId"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityId"]))?(long)0:(System.Int64)dataRow["CityId"];
			entity.ZoneId = (Convert.IsDBNull(dataRow["ZoneId"]))?(long)0:(System.Int64)dataRow["ZoneId"];
			entity.Stars = (Convert.IsDBNull(dataRow["Stars"]))?(int)0:(System.Int32?)dataRow["Stars"];
			entity.Longitude = (Convert.IsDBNull(dataRow["Longitude"]))?string.Empty:(System.String)dataRow["Longitude"];
			entity.Latitude = (Convert.IsDBNull(dataRow["Latitude"]))?string.Empty:(System.String)dataRow["Latitude"];
			entity.Logo = (Convert.IsDBNull(dataRow["Logo"]))?string.Empty:(System.String)dataRow["Logo"];
			entity.IsActive = (Convert.IsDBNull(dataRow["IsActive"]))?false:(System.Boolean)dataRow["IsActive"];
			entity.CancelationPolicyId = (Convert.IsDBNull(dataRow["CancelationPolicyId"]))?(long)0:(System.Int64)dataRow["CancelationPolicyId"];
			entity.GroupId = (Convert.IsDBNull(dataRow["GroupId"]))?(long)0:(System.Int64?)dataRow["GroupId"];
			entity.OperatorChoice = (Convert.IsDBNull(dataRow["OperatorChoice"]))?string.Empty:(System.String)dataRow["OperatorChoice"];
			entity.HotelPlan = (Convert.IsDBNull(dataRow["HotelPlan"]))?string.Empty:(System.String)dataRow["HotelPlan"];
			entity.ClientId = (Convert.IsDBNull(dataRow["ClientId"]))?(long)0:(System.Int64?)dataRow["ClientId"];
			entity.ContractId = (Convert.IsDBNull(dataRow["ContractId"]))?string.Empty:(System.String)dataRow["ContractId"];
			entity.StaffId = (Convert.IsDBNull(dataRow["StaffId"]))?(long)0:(System.Int64)dataRow["StaffId"];
			entity.CurrencyId = (Convert.IsDBNull(dataRow["CurrencyId"]))?(long)0:(System.Int64)dataRow["CurrencyId"];
			entity.ContractValue = (Convert.IsDBNull(dataRow["ContractValue"]))?0.0m:(System.Decimal?)dataRow["ContractValue"];
			entity.TimeZone = (Convert.IsDBNull(dataRow["TimeZone"]))?string.Empty:(System.String)dataRow["TimeZone"];
			entity.Phone = (Convert.IsDBNull(dataRow["Phone"]))?string.Empty:(System.String)dataRow["Phone"];
			entity.PhoneExt = (Convert.IsDBNull(dataRow["PhoneExt"]))?string.Empty:(System.String)dataRow["PhoneExt"];
			entity.RtMFrom = (Convert.IsDBNull(dataRow["RT_M_From"]))?string.Empty:(System.String)dataRow["RT_M_From"];
			entity.RtMTo = (Convert.IsDBNull(dataRow["RT_M_To"]))?string.Empty:(System.String)dataRow["RT_M_To"];
			entity.RtAFrom = (Convert.IsDBNull(dataRow["RT_A_From"]))?string.Empty:(System.String)dataRow["RT_A_From"];
			entity.RtATo = (Convert.IsDBNull(dataRow["RT_A_To"]))?string.Empty:(System.String)dataRow["RT_A_To"];
			entity.RtFFrom = (Convert.IsDBNull(dataRow["RT_F_From"]))?string.Empty:(System.String)dataRow["RT_F_From"];
			entity.RtFTo = (Convert.IsDBNull(dataRow["RT_F_To"]))?string.Empty:(System.String)dataRow["RT_F_To"];
			entity.IsCreditCardExcepted = (Convert.IsDBNull(dataRow["IsCreditCardExcepted"]))?false:(System.Boolean)dataRow["IsCreditCardExcepted"];
			entity.CreditCardType = (Convert.IsDBNull(dataRow["CreditCardType"]))?string.Empty:(System.String)dataRow["CreditCardType"];
			entity.Theme = (Convert.IsDBNull(dataRow["Theme"]))?string.Empty:(System.String)dataRow["Theme"];
			entity.IsBedroomAvailable = (Convert.IsDBNull(dataRow["IsBedroomAvailable"]))?false:(System.Boolean)dataRow["IsBedroomAvailable"];
			entity.NumberOfBedroom = (Convert.IsDBNull(dataRow["NumberOfBedroom"]))?(int)0:(System.Int32?)dataRow["NumberOfBedroom"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.ContactPerson = (Convert.IsDBNull(dataRow["ContactPerson"]))?string.Empty:(System.String)dataRow["ContactPerson"];
			entity.PhoneNumber = (Convert.IsDBNull(dataRow["PhoneNumber"]))?string.Empty:(System.String)dataRow["PhoneNumber"];
			entity.GoOnline = (Convert.IsDBNull(dataRow["GoOnline"]))?false:(System.Boolean?)dataRow["GoOnline"];
			entity.ZipCode = (Convert.IsDBNull(dataRow["ZipCode"]))?string.Empty:(System.String)dataRow["ZipCode"];
			entity.UpdatedBy = (Convert.IsDBNull(dataRow["UpdatedBy"]))?(long)0:(System.Int64?)dataRow["UpdatedBy"];
			entity.HotelAddress = (Convert.IsDBNull(dataRow["HotelAddress"]))?string.Empty:(System.String)dataRow["HotelAddress"];
			entity.CreationDate = (Convert.IsDBNull(dataRow["CreationDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreationDate"];
			entity.CountryCodePhone = (Convert.IsDBNull(dataRow["CountryCodePhone"]))?string.Empty:(System.String)dataRow["CountryCodePhone"];
			entity.CountryCodeFax = (Convert.IsDBNull(dataRow["CountryCodeFax"]))?string.Empty:(System.String)dataRow["CountryCodeFax"];
			entity.FaxNumber = (Convert.IsDBNull(dataRow["FaxNumber"]))?string.Empty:(System.String)dataRow["FaxNumber"];
			entity.IsRemoved = (Convert.IsDBNull(dataRow["IsRemoved"]))?false:(System.Boolean)dataRow["IsRemoved"];
			entity.RequestGoOnline = (Convert.IsDBNull(dataRow["RequestGoOnline"]))?false:(System.Boolean?)dataRow["RequestGoOnline"];
			entity.HotelUserId = (Convert.IsDBNull(dataRow["HotelUserID"]))?(long)0:(System.Int64?)dataRow["HotelUserID"];
			entity.AccountingOfficer = (Convert.IsDBNull(dataRow["AccountingOfficer"]))?string.Empty:(System.String)dataRow["AccountingOfficer"];
			entity.MeetingRoomId = (Convert.IsDBNull(dataRow["MeetingRoomId"]))?(long)0:(System.Int64)dataRow["MeetingRoomId"];
			entity.MeetingRoomName = (Convert.IsDBNull(dataRow["MeetingRoomName"]))?string.Empty:(System.String)dataRow["MeetingRoomName"];
			entity.Picture = (Convert.IsDBNull(dataRow["Picture"]))?string.Empty:(System.String)dataRow["Picture"];
			entity.DayLight = (Convert.IsDBNull(dataRow["DayLight"]))?(int)0:(System.Int32)dataRow["DayLight"];
			entity.Surface = (Convert.IsDBNull(dataRow["Surface"]))?(long)0:(System.Int64?)dataRow["Surface"];
			entity.Height = (Convert.IsDBNull(dataRow["Height"]))?0.0m:(System.Decimal?)dataRow["Height"];
			entity.MeetingRoomActive = (Convert.IsDBNull(dataRow["MeetingRoomActive"]))?false:(System.Boolean)dataRow["MeetingRoomActive"];
			entity.MrPlan = (Convert.IsDBNull(dataRow["MR_Plan"]))?string.Empty:(System.String)dataRow["MR_Plan"];
			entity.OrderNumber = (Convert.IsDBNull(dataRow["OrderNumber"]))?(int)0:(System.Int32?)dataRow["OrderNumber"];
			entity.HalfdayPrice = (Convert.IsDBNull(dataRow["HalfdayPrice"]))?0.0m:(System.Decimal?)dataRow["HalfdayPrice"];
			entity.FulldayPrice = (Convert.IsDBNull(dataRow["FulldayPrice"]))?0.0m:(System.Decimal?)dataRow["FulldayPrice"];
			entity.IsOnline = (Convert.IsDBNull(dataRow["IsOnline"]))?false:(System.Boolean)dataRow["IsOnline"];
			entity.IsDeleted = (Convert.IsDBNull(dataRow["IsDeleted"]))?false:(System.Boolean)dataRow["IsDeleted"];
			entity.SppId = (Convert.IsDBNull(dataRow["SPPId"]))?(long)0:(System.Int64)dataRow["SPPId"];
			entity.SpecialPriceDate = (Convert.IsDBNull(dataRow["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["SpecialPriceDate"];
			entity.DdrPercent = (Convert.IsDBNull(dataRow["DDRPercent"]))?0.0m:(System.Decimal?)dataRow["DDRPercent"];
			entity.MeetingRoomPercent = (Convert.IsDBNull(dataRow["MeetingRoomPercent"]))?0.0m:(System.Decimal?)dataRow["MeetingRoomPercent"];
			entity.AvailabilityId = (Convert.IsDBNull(dataRow["AvailabilityId"]))?(long)0:(System.Int64)dataRow["AvailabilityId"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.FullPropertyStatus = (Convert.IsDBNull(dataRow["FullPropertyStatus"]))?(int)0:(System.Int32?)dataRow["FullPropertyStatus"];
			entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(dataRow["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)dataRow["LeadTimeForMeetingRoom"];
			entity.FullProrertyStatusBr = (Convert.IsDBNull(dataRow["FullProrertyStatusBR"]))?(int)0:(System.Int32?)dataRow["FullProrertyStatusBR"];
			entity.AvailibilityOfRoomsId = (Convert.IsDBNull(dataRow["AvailibilityOfRoomsId"]))?(long)0:(System.Int64)dataRow["AvailibilityOfRoomsId"];
			entity.MorningStatus = (Convert.IsDBNull(dataRow["MorningStatus"]))?(int)0:(System.Int32?)dataRow["MorningStatus"];
			entity.AfternoonStatus = (Convert.IsDBNull(dataRow["AfternoonStatus"]))?(int)0:(System.Int32?)dataRow["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (Convert.IsDBNull(dataRow["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomsAvailable"];
			entity.RoomType = (Convert.IsDBNull(dataRow["RoomType"]))?(int)0:(System.Int32?)dataRow["RoomType"];
			entity.MeetingRoomConfigId = (Convert.IsDBNull(dataRow["MeetingRoomConfigId"]))?(long)0:(System.Int64)dataRow["MeetingRoomConfigId"];
			entity.RoomShapeId = (Convert.IsDBNull(dataRow["RoomShapeId"]))?(int)0:(System.Int32?)dataRow["RoomShapeId"];
			entity.MinCapacity = (Convert.IsDBNull(dataRow["MinCapacity"]))?(int)0:(System.Int32?)dataRow["MinCapacity"];
			entity.MaxCapicity = (Convert.IsDBNull(dataRow["MaxCapicity"]))?(int)0:(System.Int32?)dataRow["MaxCapicity"];
			entity.BookingAlgo = (Convert.IsDBNull(dataRow["BookingAlgo"]))?0.0m:(System.Decimal?)dataRow["BookingAlgo"];
			entity.RequestAlgo = (Convert.IsDBNull(dataRow["RequestAlgo"]))?0.0m:(System.Decimal?)dataRow["RequestAlgo"];
			entity.IsPriority = (Convert.IsDBNull(dataRow["IsPriority"]))?string.Empty:(System.String)dataRow["IsPriority"];
			entity.ActualPkgPrice = (Convert.IsDBNull(dataRow["ActualPkgPrice"]))?0.0m:(System.Decimal?)dataRow["ActualPkgPrice"];
			entity.ActualPkgPriceHalfDay = (Convert.IsDBNull(dataRow["ActualPkgPriceHalfDay"]))?0.0m:(System.Decimal?)dataRow["ActualPkgPriceHalfDay"];
			entity.NumberOfRoomBooked = (Convert.IsDBNull(dataRow["NumberOfRoomBooked"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomBooked"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region BookingRequestViewListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookingRequestViewList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingRequestViewListFilterBuilder : SqlFilterBuilder<BookingRequestViewListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListFilterBuilder class.
		/// </summary>
		public BookingRequestViewListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingRequestViewListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingRequestViewListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingRequestViewListFilterBuilder

	#region BookingRequestViewListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookingRequestViewList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingRequestViewListParameterBuilder : ParameterizedSqlFilterBuilder<BookingRequestViewListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListParameterBuilder class.
		/// </summary>
		public BookingRequestViewListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingRequestViewListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingRequestViewListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingRequestViewListParameterBuilder
	
	#region BookingRequestViewListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookingRequestViewList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BookingRequestViewListSortBuilder : SqlSortBuilder<BookingRequestViewListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListSqlSortBuilder class.
		/// </summary>
		public BookingRequestViewListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BookingRequestViewListSortBuilder

} // end namespace
