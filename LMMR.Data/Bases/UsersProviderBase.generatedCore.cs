﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UsersProviderBaseCore : EntityProviderBase<LMMR.Entities.Users, LMMR.Entities.UsersKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.UsersKey key)
		{
			return Delete(transactionManager, key.UserId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_userId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _userId)
		{
			return Delete(null, _userId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _userId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Users Get(TransactionManager transactionManager, LMMR.Entities.UsersKey key, int start, int pageLength)
		{
			return GetByUserId(transactionManager, key.UserId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Users index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Users"/> class.</returns>
		public LMMR.Entities.Users GetByUserId(System.Int64 _userId)
		{
			int count = -1;
			return GetByUserId(null,_userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Users"/> class.</returns>
		public LMMR.Entities.Users GetByUserId(System.Int64 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(null, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Users"/> class.</returns>
		public LMMR.Entities.Users GetByUserId(TransactionManager transactionManager, System.Int64 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Users"/> class.</returns>
		public LMMR.Entities.Users GetByUserId(TransactionManager transactionManager, System.Int64 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Users"/> class.</returns>
		public LMMR.Entities.Users GetByUserId(System.Int64 _userId, int start, int pageLength, out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Users"/> class.</returns>
		public abstract LMMR.Entities.Users GetByUserId(TransactionManager transactionManager, System.Int64 _userId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Users&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Users&gt;"/></returns>
		public static TList<Users> Fill(IDataReader reader, TList<Users> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Users c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Users")
					.Append("|").Append((System.Int64)reader[((int)UsersColumn.UserId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Users>(
					key.ToString(), // EntityTrackingKey
					"Users",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Users();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.UserId = (System.Int64)reader[((int)UsersColumn.UserId - 1)];
					c.FirstName = (reader.IsDBNull(((int)UsersColumn.FirstName - 1)))?null:(System.String)reader[((int)UsersColumn.FirstName - 1)];
					c.LastName = (reader.IsDBNull(((int)UsersColumn.LastName - 1)))?null:(System.String)reader[((int)UsersColumn.LastName - 1)];
					c.EmailId = (reader.IsDBNull(((int)UsersColumn.EmailId - 1)))?null:(System.String)reader[((int)UsersColumn.EmailId - 1)];
					c.Password = (reader.IsDBNull(((int)UsersColumn.Password - 1)))?null:(System.String)reader[((int)UsersColumn.Password - 1)];
					c.ParentId = (reader.IsDBNull(((int)UsersColumn.ParentId - 1)))?null:(System.Int64?)reader[((int)UsersColumn.ParentId - 1)];
					c.Usertype = (reader.IsDBNull(((int)UsersColumn.Usertype - 1)))?null:(System.Int32?)reader[((int)UsersColumn.Usertype - 1)];
					c.CreatedDate = (reader.IsDBNull(((int)UsersColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)UsersColumn.CreatedDate - 1)];
					c.LastLogin = (reader.IsDBNull(((int)UsersColumn.LastLogin - 1)))?null:(System.DateTime?)reader[((int)UsersColumn.LastLogin - 1)];
					c.IsActive = (System.Boolean)reader[((int)UsersColumn.IsActive - 1)];
					c.IsfromWl = (reader.IsDBNull(((int)UsersColumn.IsfromWl - 1)))?null:(System.Boolean?)reader[((int)UsersColumn.IsfromWl - 1)];
					c.IsRemoved = (reader.IsDBNull(((int)UsersColumn.IsRemoved - 1)))?null:(System.Boolean?)reader[((int)UsersColumn.IsRemoved - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Users"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Users"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Users entity)
		{
			if (!reader.Read()) return;
			
			entity.UserId = (System.Int64)reader[((int)UsersColumn.UserId - 1)];
			entity.FirstName = (reader.IsDBNull(((int)UsersColumn.FirstName - 1)))?null:(System.String)reader[((int)UsersColumn.FirstName - 1)];
			entity.LastName = (reader.IsDBNull(((int)UsersColumn.LastName - 1)))?null:(System.String)reader[((int)UsersColumn.LastName - 1)];
			entity.EmailId = (reader.IsDBNull(((int)UsersColumn.EmailId - 1)))?null:(System.String)reader[((int)UsersColumn.EmailId - 1)];
			entity.Password = (reader.IsDBNull(((int)UsersColumn.Password - 1)))?null:(System.String)reader[((int)UsersColumn.Password - 1)];
			entity.ParentId = (reader.IsDBNull(((int)UsersColumn.ParentId - 1)))?null:(System.Int64?)reader[((int)UsersColumn.ParentId - 1)];
			entity.Usertype = (reader.IsDBNull(((int)UsersColumn.Usertype - 1)))?null:(System.Int32?)reader[((int)UsersColumn.Usertype - 1)];
			entity.CreatedDate = (reader.IsDBNull(((int)UsersColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)UsersColumn.CreatedDate - 1)];
			entity.LastLogin = (reader.IsDBNull(((int)UsersColumn.LastLogin - 1)))?null:(System.DateTime?)reader[((int)UsersColumn.LastLogin - 1)];
			entity.IsActive = (System.Boolean)reader[((int)UsersColumn.IsActive - 1)];
			entity.IsfromWl = (reader.IsDBNull(((int)UsersColumn.IsfromWl - 1)))?null:(System.Boolean?)reader[((int)UsersColumn.IsfromWl - 1)];
			entity.IsRemoved = (reader.IsDBNull(((int)UsersColumn.IsRemoved - 1)))?null:(System.Boolean?)reader[((int)UsersColumn.IsRemoved - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Users"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Users"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Users entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (System.Int64)dataRow["UserId"];
			entity.FirstName = Convert.IsDBNull(dataRow["FirstName"]) ? null : (System.String)dataRow["FirstName"];
			entity.LastName = Convert.IsDBNull(dataRow["LastName"]) ? null : (System.String)dataRow["LastName"];
			entity.EmailId = Convert.IsDBNull(dataRow["EmailId"]) ? null : (System.String)dataRow["EmailId"];
			entity.Password = Convert.IsDBNull(dataRow["Password"]) ? null : (System.String)dataRow["Password"];
			entity.ParentId = Convert.IsDBNull(dataRow["ParentID"]) ? null : (System.Int64?)dataRow["ParentID"];
			entity.Usertype = Convert.IsDBNull(dataRow["Usertype"]) ? null : (System.Int32?)dataRow["Usertype"];
			entity.CreatedDate = Convert.IsDBNull(dataRow["CreatedDate"]) ? null : (System.DateTime?)dataRow["CreatedDate"];
			entity.LastLogin = Convert.IsDBNull(dataRow["LastLogin"]) ? null : (System.DateTime?)dataRow["LastLogin"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.IsfromWl = Convert.IsDBNull(dataRow["IsfromWL"]) ? null : (System.Boolean?)dataRow["IsfromWL"];
			entity.IsRemoved = Convert.IsDBNull(dataRow["IsRemoved"]) ? null : (System.Boolean?)dataRow["IsRemoved"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Users"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Users Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Users entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByUserId methods when available
			
			#region SearchTracerCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SearchTracer>|SearchTracerCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SearchTracerCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SearchTracerCollection = DataRepository.SearchTracerProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.SearchTracerCollection.Count > 0)
				{
					deepHandles.Add("SearchTracerCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SearchTracer>) DataRepository.SearchTracerProvider.DeepLoad,
						new object[] { transactionManager, entity.SearchTracerCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoom>|MeetingRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomCollection = DataRepository.MeetingRoomProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.MeetingRoomCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoom>) DataRepository.MeetingRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AvailabilityCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Availability>|AvailabilityCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AvailabilityCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AvailabilityCollection = DataRepository.AvailabilityProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.AvailabilityCollection.Count > 0)
				{
					deepHandles.Add("AvailabilityCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Availability>) DataRepository.AvailabilityProvider.DeepLoad,
						new object[] { transactionManager, entity.AvailabilityCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CreditCardDetailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CreditCardDetail>|CreditCardDetailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreditCardDetailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CreditCardDetailCollection = DataRepository.CreditCardDetailProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.CreditCardDetailCollection.Count > 0)
				{
					deepHandles.Add("CreditCardDetailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CreditCardDetail>) DataRepository.CreditCardDetailProvider.DeepLoad,
						new object[] { transactionManager, entity.CreditCardDetailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelCollectionGetByUpdatedBy
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Hotel>|HotelCollectionGetByUpdatedBy", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelCollectionGetByUpdatedBy' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelCollectionGetByUpdatedBy = DataRepository.HotelProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.HotelCollectionGetByUpdatedBy.Count > 0)
				{
					deepHandles.Add("HotelCollectionGetByUpdatedBy",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Hotel>) DataRepository.HotelProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelCollectionGetByUpdatedBy, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BedRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoom>|BedRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomCollection = DataRepository.BedRoomProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.BedRoomCollection.Count > 0)
				{
					deepHandles.Add("BedRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoom>) DataRepository.BedRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookingCollectionGetByCreatorId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Booking>|BookingCollectionGetByCreatorId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingCollectionGetByCreatorId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookingCollectionGetByCreatorId = DataRepository.BookingProvider.GetByCreatorId(transactionManager, entity.UserId);

				if (deep && entity.BookingCollectionGetByCreatorId.Count > 0)
				{
					deepHandles.Add("BookingCollectionGetByCreatorId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Booking>) DataRepository.BookingProvider.DeepLoad,
						new object[] { transactionManager, entity.BookingCollectionGetByCreatorId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomPictureVideoCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomPictureVideo>|MeetingRoomPictureVideoCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomPictureVideoCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomPictureVideoCollection = DataRepository.MeetingRoomPictureVideoProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.MeetingRoomPictureVideoCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomPictureVideoCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomPictureVideo>) DataRepository.MeetingRoomPictureVideoProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomPictureVideoCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UserDetailsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<UserDetails>|UserDetailsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserDetailsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UserDetailsCollection = DataRepository.UserDetailsProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.UserDetailsCollection.Count > 0)
				{
					deepHandles.Add("UserDetailsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<UserDetails>) DataRepository.UserDetailsProvider.DeepLoad,
						new object[] { transactionManager, entity.UserDetailsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BedRoomPictureImageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoomPictureImage>|BedRoomPictureImageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomPictureImageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomPictureImageCollection = DataRepository.BedRoomPictureImageProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.BedRoomPictureImageCollection.Count > 0)
				{
					deepHandles.Add("BedRoomPictureImageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoomPictureImage>) DataRepository.BedRoomPictureImageProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomPictureImageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BedRoomDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoomDesc>|BedRoomDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomDescCollection = DataRepository.BedRoomDescProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.BedRoomDescCollection.Count > 0)
				{
					deepHandles.Add("BedRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoomDesc>) DataRepository.BedRoomDescProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageByHotelCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageByHotel>|PackageByHotelCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageByHotelCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageByHotelCollection = DataRepository.PackageByHotelProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.PackageByHotelCollection.Count > 0)
				{
					deepHandles.Add("PackageByHotelCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageByHotel>) DataRepository.PackageByHotelProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageByHotelCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ServeyResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ServeyResponse>|ServeyResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServeyResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ServeyResponseCollection = DataRepository.ServeyResponseProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.ServeyResponseCollection.Count > 0)
				{
					deepHandles.Add("ServeyResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ServeyResponse>) DataRepository.ServeyResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.ServeyResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelOwnerLinkCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelOwnerLink>|HotelOwnerLinkCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelOwnerLinkCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelOwnerLinkCollection = DataRepository.HotelOwnerLinkProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.HotelOwnerLinkCollection.Count > 0)
				{
					deepHandles.Add("HotelOwnerLinkCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelOwnerLink>) DataRepository.HotelOwnerLinkProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelOwnerLinkCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region RpfCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Rpf>|RpfCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RpfCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.RpfCollection = DataRepository.RpfProvider.GetByUserid(transactionManager, entity.UserId);

				if (deep && entity.RpfCollection.Count > 0)
				{
					deepHandles.Add("RpfCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Rpf>) DataRepository.RpfProvider.DeepLoad,
						new object[] { transactionManager, entity.RpfCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomConfigCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomConfig>|MeetingRoomConfigCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomConfigCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomConfigCollection = DataRepository.MeetingRoomConfigProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.MeetingRoomConfigCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomConfigCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomConfig>) DataRepository.MeetingRoomConfigProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomConfigCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookingCollectionGetByAgencyUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Booking>|BookingCollectionGetByAgencyUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingCollectionGetByAgencyUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookingCollectionGetByAgencyUserId = DataRepository.BookingProvider.GetByAgencyUserId(transactionManager, entity.UserId);

				if (deep && entity.BookingCollectionGetByAgencyUserId.Count > 0)
				{
					deepHandles.Add("BookingCollectionGetByAgencyUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Booking>) DataRepository.BookingProvider.DeepLoad,
						new object[] { transactionManager, entity.BookingCollectionGetByAgencyUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelCollectionGetByClientId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Hotel>|HotelCollectionGetByClientId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelCollectionGetByClientId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelCollectionGetByClientId = DataRepository.HotelProvider.GetByClientId(transactionManager, entity.UserId);

				if (deep && entity.HotelCollectionGetByClientId.Count > 0)
				{
					deepHandles.Add("HotelCollectionGetByClientId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Hotel>) DataRepository.HotelProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelCollectionGetByClientId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelContactCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelContact>|HotelContactCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelContactCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelContactCollection = DataRepository.HotelContactProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.HotelContactCollection.Count > 0)
				{
					deepHandles.Add("HotelContactCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelContact>) DataRepository.HotelContactProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelContactCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomDesc>|MeetingRoomDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomDescCollection = DataRepository.MeetingRoomDescProvider.GetByUpdatedBy(transactionManager, entity.UserId);

				if (deep && entity.MeetingRoomDescCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomDesc>) DataRepository.MeetingRoomDescProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FinancialInfoCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FinancialInfo>|FinancialInfoCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FinancialInfoCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FinancialInfoCollection = DataRepository.FinancialInfoProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.FinancialInfoCollection.Count > 0)
				{
					deepHandles.Add("FinancialInfoCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FinancialInfo>) DataRepository.FinancialInfoProvider.DeepLoad,
						new object[] { transactionManager, entity.FinancialInfoCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Users object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Users instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Users Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Users entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SearchTracer>
				if (CanDeepSave(entity.SearchTracerCollection, "List<SearchTracer>|SearchTracerCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SearchTracer child in entity.SearchTracerCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.SearchTracerCollection.Count > 0 || entity.SearchTracerCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SearchTracerProvider.Save(transactionManager, entity.SearchTracerCollection);
						
						deepHandles.Add("SearchTracerCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SearchTracer >) DataRepository.SearchTracerProvider.DeepSave,
							new object[] { transactionManager, entity.SearchTracerCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoom>
				if (CanDeepSave(entity.MeetingRoomCollection, "List<MeetingRoom>|MeetingRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoom child in entity.MeetingRoomCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.MeetingRoomCollection.Count > 0 || entity.MeetingRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomProvider.Save(transactionManager, entity.MeetingRoomCollection);
						
						deepHandles.Add("MeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoom >) DataRepository.MeetingRoomProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Availability>
				if (CanDeepSave(entity.AvailabilityCollection, "List<Availability>|AvailabilityCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Availability child in entity.AvailabilityCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.AvailabilityCollection.Count > 0 || entity.AvailabilityCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AvailabilityProvider.Save(transactionManager, entity.AvailabilityCollection);
						
						deepHandles.Add("AvailabilityCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Availability >) DataRepository.AvailabilityProvider.DeepSave,
							new object[] { transactionManager, entity.AvailabilityCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CreditCardDetail>
				if (CanDeepSave(entity.CreditCardDetailCollection, "List<CreditCardDetail>|CreditCardDetailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CreditCardDetail child in entity.CreditCardDetailCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.CreditCardDetailCollection.Count > 0 || entity.CreditCardDetailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CreditCardDetailProvider.Save(transactionManager, entity.CreditCardDetailCollection);
						
						deepHandles.Add("CreditCardDetailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CreditCardDetail >) DataRepository.CreditCardDetailProvider.DeepSave,
							new object[] { transactionManager, entity.CreditCardDetailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Hotel>
				if (CanDeepSave(entity.HotelCollectionGetByUpdatedBy, "List<Hotel>|HotelCollectionGetByUpdatedBy", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Hotel child in entity.HotelCollectionGetByUpdatedBy)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.HotelCollectionGetByUpdatedBy.Count > 0 || entity.HotelCollectionGetByUpdatedBy.DeletedItems.Count > 0)
					{
						//DataRepository.HotelProvider.Save(transactionManager, entity.HotelCollectionGetByUpdatedBy);
						
						deepHandles.Add("HotelCollectionGetByUpdatedBy",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Hotel >) DataRepository.HotelProvider.DeepSave,
							new object[] { transactionManager, entity.HotelCollectionGetByUpdatedBy, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BedRoom>
				if (CanDeepSave(entity.BedRoomCollection, "List<BedRoom>|BedRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoom child in entity.BedRoomCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.BedRoomCollection.Count > 0 || entity.BedRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomProvider.Save(transactionManager, entity.BedRoomCollection);
						
						deepHandles.Add("BedRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoom >) DataRepository.BedRoomProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Booking>
				if (CanDeepSave(entity.BookingCollectionGetByCreatorId, "List<Booking>|BookingCollectionGetByCreatorId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Booking child in entity.BookingCollectionGetByCreatorId)
					{
						if(child.CreatorIdSource != null)
						{
							child.CreatorId = child.CreatorIdSource.UserId;
						}
						else
						{
							child.CreatorId = entity.UserId;
						}

					}

					if (entity.BookingCollectionGetByCreatorId.Count > 0 || entity.BookingCollectionGetByCreatorId.DeletedItems.Count > 0)
					{
						//DataRepository.BookingProvider.Save(transactionManager, entity.BookingCollectionGetByCreatorId);
						
						deepHandles.Add("BookingCollectionGetByCreatorId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Booking >) DataRepository.BookingProvider.DeepSave,
							new object[] { transactionManager, entity.BookingCollectionGetByCreatorId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoomPictureVideo>
				if (CanDeepSave(entity.MeetingRoomPictureVideoCollection, "List<MeetingRoomPictureVideo>|MeetingRoomPictureVideoCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomPictureVideo child in entity.MeetingRoomPictureVideoCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.MeetingRoomPictureVideoCollection.Count > 0 || entity.MeetingRoomPictureVideoCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomPictureVideoProvider.Save(transactionManager, entity.MeetingRoomPictureVideoCollection);
						
						deepHandles.Add("MeetingRoomPictureVideoCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomPictureVideo >) DataRepository.MeetingRoomPictureVideoProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomPictureVideoCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<UserDetails>
				if (CanDeepSave(entity.UserDetailsCollection, "List<UserDetails>|UserDetailsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(UserDetails child in entity.UserDetailsCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.UserDetailsCollection.Count > 0 || entity.UserDetailsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.UserDetailsProvider.Save(transactionManager, entity.UserDetailsCollection);
						
						deepHandles.Add("UserDetailsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< UserDetails >) DataRepository.UserDetailsProvider.DeepSave,
							new object[] { transactionManager, entity.UserDetailsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BedRoomPictureImage>
				if (CanDeepSave(entity.BedRoomPictureImageCollection, "List<BedRoomPictureImage>|BedRoomPictureImageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoomPictureImage child in entity.BedRoomPictureImageCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.BedRoomPictureImageCollection.Count > 0 || entity.BedRoomPictureImageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomPictureImageProvider.Save(transactionManager, entity.BedRoomPictureImageCollection);
						
						deepHandles.Add("BedRoomPictureImageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoomPictureImage >) DataRepository.BedRoomPictureImageProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomPictureImageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BedRoomDesc>
				if (CanDeepSave(entity.BedRoomDescCollection, "List<BedRoomDesc>|BedRoomDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoomDesc child in entity.BedRoomDescCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.BedRoomDescCollection.Count > 0 || entity.BedRoomDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomDescProvider.Save(transactionManager, entity.BedRoomDescCollection);
						
						deepHandles.Add("BedRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoomDesc >) DataRepository.BedRoomDescProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageByHotel>
				if (CanDeepSave(entity.PackageByHotelCollection, "List<PackageByHotel>|PackageByHotelCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageByHotel child in entity.PackageByHotelCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.PackageByHotelCollection.Count > 0 || entity.PackageByHotelCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageByHotelProvider.Save(transactionManager, entity.PackageByHotelCollection);
						
						deepHandles.Add("PackageByHotelCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageByHotel >) DataRepository.PackageByHotelProvider.DeepSave,
							new object[] { transactionManager, entity.PackageByHotelCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ServeyResponse>
				if (CanDeepSave(entity.ServeyResponseCollection, "List<ServeyResponse>|ServeyResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ServeyResponse child in entity.ServeyResponseCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.ServeyResponseCollection.Count > 0 || entity.ServeyResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ServeyResponseProvider.Save(transactionManager, entity.ServeyResponseCollection);
						
						deepHandles.Add("ServeyResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ServeyResponse >) DataRepository.ServeyResponseProvider.DeepSave,
							new object[] { transactionManager, entity.ServeyResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelOwnerLink>
				if (CanDeepSave(entity.HotelOwnerLinkCollection, "List<HotelOwnerLink>|HotelOwnerLinkCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelOwnerLink child in entity.HotelOwnerLinkCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.HotelOwnerLinkCollection.Count > 0 || entity.HotelOwnerLinkCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelOwnerLinkProvider.Save(transactionManager, entity.HotelOwnerLinkCollection);
						
						deepHandles.Add("HotelOwnerLinkCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelOwnerLink >) DataRepository.HotelOwnerLinkProvider.DeepSave,
							new object[] { transactionManager, entity.HotelOwnerLinkCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Rpf>
				if (CanDeepSave(entity.RpfCollection, "List<Rpf>|RpfCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Rpf child in entity.RpfCollection)
					{
						if(child.UseridSource != null)
						{
							child.Userid = child.UseridSource.UserId;
						}
						else
						{
							child.Userid = entity.UserId;
						}

					}

					if (entity.RpfCollection.Count > 0 || entity.RpfCollection.DeletedItems.Count > 0)
					{
						//DataRepository.RpfProvider.Save(transactionManager, entity.RpfCollection);
						
						deepHandles.Add("RpfCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Rpf >) DataRepository.RpfProvider.DeepSave,
							new object[] { transactionManager, entity.RpfCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoomConfig>
				if (CanDeepSave(entity.MeetingRoomConfigCollection, "List<MeetingRoomConfig>|MeetingRoomConfigCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomConfig child in entity.MeetingRoomConfigCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.MeetingRoomConfigCollection.Count > 0 || entity.MeetingRoomConfigCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomConfigProvider.Save(transactionManager, entity.MeetingRoomConfigCollection);
						
						deepHandles.Add("MeetingRoomConfigCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomConfig >) DataRepository.MeetingRoomConfigProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomConfigCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Booking>
				if (CanDeepSave(entity.BookingCollectionGetByAgencyUserId, "List<Booking>|BookingCollectionGetByAgencyUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Booking child in entity.BookingCollectionGetByAgencyUserId)
					{
						if(child.AgencyUserIdSource != null)
						{
							child.AgencyUserId = child.AgencyUserIdSource.UserId;
						}
						else
						{
							child.AgencyUserId = entity.UserId;
						}

					}

					if (entity.BookingCollectionGetByAgencyUserId.Count > 0 || entity.BookingCollectionGetByAgencyUserId.DeletedItems.Count > 0)
					{
						//DataRepository.BookingProvider.Save(transactionManager, entity.BookingCollectionGetByAgencyUserId);
						
						deepHandles.Add("BookingCollectionGetByAgencyUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Booking >) DataRepository.BookingProvider.DeepSave,
							new object[] { transactionManager, entity.BookingCollectionGetByAgencyUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Hotel>
				if (CanDeepSave(entity.HotelCollectionGetByClientId, "List<Hotel>|HotelCollectionGetByClientId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Hotel child in entity.HotelCollectionGetByClientId)
					{
						if(child.ClientIdSource != null)
						{
							child.ClientId = child.ClientIdSource.UserId;
						}
						else
						{
							child.ClientId = entity.UserId;
						}

					}

					if (entity.HotelCollectionGetByClientId.Count > 0 || entity.HotelCollectionGetByClientId.DeletedItems.Count > 0)
					{
						//DataRepository.HotelProvider.Save(transactionManager, entity.HotelCollectionGetByClientId);
						
						deepHandles.Add("HotelCollectionGetByClientId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Hotel >) DataRepository.HotelProvider.DeepSave,
							new object[] { transactionManager, entity.HotelCollectionGetByClientId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelContact>
				if (CanDeepSave(entity.HotelContactCollection, "List<HotelContact>|HotelContactCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelContact child in entity.HotelContactCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.HotelContactCollection.Count > 0 || entity.HotelContactCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelContactProvider.Save(transactionManager, entity.HotelContactCollection);
						
						deepHandles.Add("HotelContactCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelContact >) DataRepository.HotelContactProvider.DeepSave,
							new object[] { transactionManager, entity.HotelContactCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoomDesc>
				if (CanDeepSave(entity.MeetingRoomDescCollection, "List<MeetingRoomDesc>|MeetingRoomDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomDesc child in entity.MeetingRoomDescCollection)
					{
						if(child.UpdatedBySource != null)
						{
							child.UpdatedBy = child.UpdatedBySource.UserId;
						}
						else
						{
							child.UpdatedBy = entity.UserId;
						}

					}

					if (entity.MeetingRoomDescCollection.Count > 0 || entity.MeetingRoomDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomDescProvider.Save(transactionManager, entity.MeetingRoomDescCollection);
						
						deepHandles.Add("MeetingRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomDesc >) DataRepository.MeetingRoomDescProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FinancialInfo>
				if (CanDeepSave(entity.FinancialInfoCollection, "List<FinancialInfo>|FinancialInfoCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FinancialInfo child in entity.FinancialInfoCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.FinancialInfoCollection.Count > 0 || entity.FinancialInfoCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FinancialInfoProvider.Save(transactionManager, entity.FinancialInfoCollection);
						
						deepHandles.Add("FinancialInfoCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FinancialInfo >) DataRepository.FinancialInfoProvider.DeepSave,
							new object[] { transactionManager, entity.FinancialInfoCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UsersChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Users</c>
	///</summary>
	public enum UsersChildEntityTypes
	{

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for SearchTracerCollection
		///</summary>
		[ChildEntityType(typeof(TList<SearchTracer>))]
		SearchTracerCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for MeetingRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoom>))]
		MeetingRoomCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for AvailabilityCollection
		///</summary>
		[ChildEntityType(typeof(TList<Availability>))]
		AvailabilityCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CreditCardDetailCollection
		///</summary>
		[ChildEntityType(typeof(TList<CreditCardDetail>))]
		CreditCardDetailCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for HotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<Hotel>))]
		HotelCollectionGetByUpdatedBy,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for BedRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoom>))]
		BedRoomCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for BookingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Booking>))]
		BookingCollectionGetByCreatorId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for MeetingRoomPictureVideoCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomPictureVideo>))]
		MeetingRoomPictureVideoCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for UserDetailsCollection
		///</summary>
		[ChildEntityType(typeof(TList<UserDetails>))]
		UserDetailsCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for BedRoomPictureImageCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoomPictureImage>))]
		BedRoomPictureImageCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for BedRoomDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoomDesc>))]
		BedRoomDescCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for PackageByHotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageByHotel>))]
		PackageByHotelCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for ServeyResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<ServeyResponse>))]
		ServeyResponseCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for HotelOwnerLinkCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelOwnerLink>))]
		HotelOwnerLinkCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for RpfCollection
		///</summary>
		[ChildEntityType(typeof(TList<Rpf>))]
		RpfCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for MeetingRoomConfigCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomConfig>))]
		MeetingRoomConfigCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for BookingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Booking>))]
		BookingCollectionGetByAgencyUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for HotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<Hotel>))]
		HotelCollectionGetByClientId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for HotelContactCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelContact>))]
		HotelContactCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for MeetingRoomDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomDesc>))]
		MeetingRoomDescCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for FinancialInfoCollection
		///</summary>
		[ChildEntityType(typeof(TList<FinancialInfo>))]
		FinancialInfoCollection,
	}
	
	#endregion UsersChildEntityTypes
	
	#region UsersFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersFilterBuilder : SqlFilterBuilder<UsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersFilterBuilder class.
		/// </summary>
		public UsersFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersFilterBuilder
	
	#region UsersParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersParameterBuilder : ParameterizedSqlFilterBuilder<UsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersParameterBuilder class.
		/// </summary>
		public UsersParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersParameterBuilder
	
	#region UsersSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersSortBuilder : SqlSortBuilder<UsersColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersSqlSortBuilder class.
		/// </summary>
		public UsersSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersSortBuilder
	
} // end namespace
