﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CancelationPolicyProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CancelationPolicyProviderBaseCore : EntityProviderBase<LMMR.Entities.CancelationPolicy, LMMR.Entities.CancelationPolicyKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.CancelationPolicyKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CancelationPolicy_Country key.
		///		FK_CancelationPolicy_Country Description: 
		/// </summary>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CancelationPolicy objects.</returns>
		public TList<CancelationPolicy> GetByCountryId(System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(_countryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CancelationPolicy_Country key.
		///		FK_CancelationPolicy_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CancelationPolicy objects.</returns>
		/// <remarks></remarks>
		public TList<CancelationPolicy> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CancelationPolicy_Country key.
		///		FK_CancelationPolicy_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CancelationPolicy objects.</returns>
		public TList<CancelationPolicy> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CancelationPolicy_Country key.
		///		fkCancelationPolicyCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CancelationPolicy objects.</returns>
		public TList<CancelationPolicy> GetByCountryId(System.Int64 _countryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryId(null, _countryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CancelationPolicy_Country key.
		///		fkCancelationPolicyCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CancelationPolicy objects.</returns>
		public TList<CancelationPolicy> GetByCountryId(System.Int64 _countryId, int start, int pageLength,out int count)
		{
			return GetByCountryId(null, _countryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CancelationPolicy_Country key.
		///		FK_CancelationPolicy_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.CancelationPolicy objects.</returns>
		public abstract TList<CancelationPolicy> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.CancelationPolicy Get(TransactionManager transactionManager, LMMR.Entities.CancelationPolicyKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CancelationPolicy index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CancelationPolicy"/> class.</returns>
		public LMMR.Entities.CancelationPolicy GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CancelationPolicy index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CancelationPolicy"/> class.</returns>
		public LMMR.Entities.CancelationPolicy GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CancelationPolicy index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CancelationPolicy"/> class.</returns>
		public LMMR.Entities.CancelationPolicy GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CancelationPolicy index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CancelationPolicy"/> class.</returns>
		public LMMR.Entities.CancelationPolicy GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CancelationPolicy index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CancelationPolicy"/> class.</returns>
		public LMMR.Entities.CancelationPolicy GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CancelationPolicy index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CancelationPolicy"/> class.</returns>
		public abstract LMMR.Entities.CancelationPolicy GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CancelationPolicy&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CancelationPolicy&gt;"/></returns>
		public static TList<CancelationPolicy> Fill(IDataReader reader, TList<CancelationPolicy> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.CancelationPolicy c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CancelationPolicy")
					.Append("|").Append((System.Int64)reader[((int)CancelationPolicyColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CancelationPolicy>(
					key.ToString(), // EntityTrackingKey
					"CancelationPolicy",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.CancelationPolicy();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)CancelationPolicyColumn.Id - 1)];
					c.CountryId = (System.Int64)reader[((int)CancelationPolicyColumn.CountryId - 1)];
					c.CreationDate = (reader.IsDBNull(((int)CancelationPolicyColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)CancelationPolicyColumn.CreationDate - 1)];
					c.PolicyName = (reader.IsDBNull(((int)CancelationPolicyColumn.PolicyName - 1)))?null:(System.String)reader[((int)CancelationPolicyColumn.PolicyName - 1)];
					c.CancelationLimit = (reader.IsDBNull(((int)CancelationPolicyColumn.CancelationLimit - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.CancelationLimit - 1)];
					c.DefaultCountry = (System.Boolean)reader[((int)CancelationPolicyColumn.DefaultCountry - 1)];
					c.IsDeleted = (System.Boolean)reader[((int)CancelationPolicyColumn.IsDeleted - 1)];
					c.CountryDefaultPolicy = (System.Boolean)reader[((int)CancelationPolicyColumn.CountryDefaultPolicy - 1)];
					c.TimeFrameMin1 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMin1 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMin1 - 1)];
					c.TimeFrameMax1 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMax1 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMax1 - 1)];
					c.TimeFrameMin2 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMin2 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMin2 - 1)];
					c.TimeFrameMax2 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMax2 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMax2 - 1)];
					c.TimeFrameMin3 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMin3 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMin3 - 1)];
					c.TimeFrameMax3 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMax3 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMax3 - 1)];
					c.TimeFrameMin4 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMin4 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMin4 - 1)];
					c.TimeFramMax4 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFramMax4 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFramMax4 - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.CancelationPolicy"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CancelationPolicy"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.CancelationPolicy entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)CancelationPolicyColumn.Id - 1)];
			entity.CountryId = (System.Int64)reader[((int)CancelationPolicyColumn.CountryId - 1)];
			entity.CreationDate = (reader.IsDBNull(((int)CancelationPolicyColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)CancelationPolicyColumn.CreationDate - 1)];
			entity.PolicyName = (reader.IsDBNull(((int)CancelationPolicyColumn.PolicyName - 1)))?null:(System.String)reader[((int)CancelationPolicyColumn.PolicyName - 1)];
			entity.CancelationLimit = (reader.IsDBNull(((int)CancelationPolicyColumn.CancelationLimit - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.CancelationLimit - 1)];
			entity.DefaultCountry = (System.Boolean)reader[((int)CancelationPolicyColumn.DefaultCountry - 1)];
			entity.IsDeleted = (System.Boolean)reader[((int)CancelationPolicyColumn.IsDeleted - 1)];
			entity.CountryDefaultPolicy = (System.Boolean)reader[((int)CancelationPolicyColumn.CountryDefaultPolicy - 1)];
			entity.TimeFrameMin1 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMin1 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMin1 - 1)];
			entity.TimeFrameMax1 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMax1 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMax1 - 1)];
			entity.TimeFrameMin2 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMin2 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMin2 - 1)];
			entity.TimeFrameMax2 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMax2 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMax2 - 1)];
			entity.TimeFrameMin3 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMin3 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMin3 - 1)];
			entity.TimeFrameMax3 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMax3 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMax3 - 1)];
			entity.TimeFrameMin4 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFrameMin4 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFrameMin4 - 1)];
			entity.TimeFramMax4 = (reader.IsDBNull(((int)CancelationPolicyColumn.TimeFramMax4 - 1)))?null:(System.Int32?)reader[((int)CancelationPolicyColumn.TimeFramMax4 - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.CancelationPolicy"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CancelationPolicy"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.CancelationPolicy entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.CountryId = (System.Int64)dataRow["CountryId"];
			entity.CreationDate = Convert.IsDBNull(dataRow["CreationDate"]) ? null : (System.DateTime?)dataRow["CreationDate"];
			entity.PolicyName = Convert.IsDBNull(dataRow["PolicyName"]) ? null : (System.String)dataRow["PolicyName"];
			entity.CancelationLimit = Convert.IsDBNull(dataRow["CancelationLimit"]) ? null : (System.Int32?)dataRow["CancelationLimit"];
			entity.DefaultCountry = (System.Boolean)dataRow["DefaultCountry"];
			entity.IsDeleted = (System.Boolean)dataRow["IsDeleted"];
			entity.CountryDefaultPolicy = (System.Boolean)dataRow["CountryDefaultPolicy"];
			entity.TimeFrameMin1 = Convert.IsDBNull(dataRow["TimeFrameMin1"]) ? null : (System.Int32?)dataRow["TimeFrameMin1"];
			entity.TimeFrameMax1 = Convert.IsDBNull(dataRow["TimeFrameMax1"]) ? null : (System.Int32?)dataRow["TimeFrameMax1"];
			entity.TimeFrameMin2 = Convert.IsDBNull(dataRow["TimeFrameMin2"]) ? null : (System.Int32?)dataRow["TimeFrameMin2"];
			entity.TimeFrameMax2 = Convert.IsDBNull(dataRow["TimeFrameMax2"]) ? null : (System.Int32?)dataRow["TimeFrameMax2"];
			entity.TimeFrameMin3 = Convert.IsDBNull(dataRow["TimeFrameMin3"]) ? null : (System.Int32?)dataRow["TimeFrameMin3"];
			entity.TimeFrameMax3 = Convert.IsDBNull(dataRow["TimeFrameMax3"]) ? null : (System.Int32?)dataRow["TimeFrameMax3"];
			entity.TimeFrameMin4 = Convert.IsDBNull(dataRow["TimeFrameMin4"]) ? null : (System.Int32?)dataRow["TimeFrameMin4"];
			entity.TimeFramMax4 = Convert.IsDBNull(dataRow["TimeFramMax4"]) ? null : (System.Int32?)dataRow["TimeFramMax4"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CancelationPolicy"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.CancelationPolicy Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.CancelationPolicy entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CountryIdSource	
			if (CanDeepLoad(entity, "Country|CountryIdSource", deepLoadType, innerList) 
				&& entity.CountryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CountryId;
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryIdSource = tmpEntity;
				else
					entity.CountryIdSource = DataRepository.CountryProvider.GetById(transactionManager, entity.CountryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region OthersCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Others>|OthersCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OthersCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OthersCollection = DataRepository.OthersProvider.GetByCancellationPolicyId(transactionManager, entity.Id);

				if (deep && entity.OthersCollection.Count > 0)
				{
					deepHandles.Add("OthersCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Others>) DataRepository.OthersProvider.DeepLoad,
						new object[] { transactionManager, entity.OthersCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PolicyHotelMappingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PolicyHotelMapping>|PolicyHotelMappingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PolicyHotelMappingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PolicyHotelMappingCollection = DataRepository.PolicyHotelMappingProvider.GetByPolicyId(transactionManager, entity.Id);

				if (deep && entity.PolicyHotelMappingCollection.Count > 0)
				{
					deepHandles.Add("PolicyHotelMappingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PolicyHotelMapping>) DataRepository.PolicyHotelMappingProvider.DeepLoad,
						new object[] { transactionManager, entity.PolicyHotelMappingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CancelationPolicyLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CancelationPolicyLanguage>|CancelationPolicyLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CancelationPolicyLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CancelationPolicyLanguageCollection = DataRepository.CancelationPolicyLanguageProvider.GetByCancelationPolicyId(transactionManager, entity.Id);

				if (deep && entity.CancelationPolicyLanguageCollection.Count > 0)
				{
					deepHandles.Add("CancelationPolicyLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CancelationPolicyLanguage>) DataRepository.CancelationPolicyLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.CancelationPolicyLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.CancelationPolicy object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.CancelationPolicy instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.CancelationPolicy Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.CancelationPolicy entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CountryIdSource
			if (CanDeepSave(entity, "Country|CountryIdSource", deepSaveType, innerList) 
				&& entity.CountryIdSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryIdSource);
				entity.CountryId = entity.CountryIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Others>
				if (CanDeepSave(entity.OthersCollection, "List<Others>|OthersCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Others child in entity.OthersCollection)
					{
						if(child.CancellationPolicyIdSource != null)
						{
							child.CancellationPolicyId = child.CancellationPolicyIdSource.Id;
						}
						else
						{
							child.CancellationPolicyId = entity.Id;
						}

					}

					if (entity.OthersCollection.Count > 0 || entity.OthersCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OthersProvider.Save(transactionManager, entity.OthersCollection);
						
						deepHandles.Add("OthersCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Others >) DataRepository.OthersProvider.DeepSave,
							new object[] { transactionManager, entity.OthersCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PolicyHotelMapping>
				if (CanDeepSave(entity.PolicyHotelMappingCollection, "List<PolicyHotelMapping>|PolicyHotelMappingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PolicyHotelMapping child in entity.PolicyHotelMappingCollection)
					{
						if(child.PolicyIdSource != null)
						{
							child.PolicyId = child.PolicyIdSource.Id;
						}
						else
						{
							child.PolicyId = entity.Id;
						}

					}

					if (entity.PolicyHotelMappingCollection.Count > 0 || entity.PolicyHotelMappingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PolicyHotelMappingProvider.Save(transactionManager, entity.PolicyHotelMappingCollection);
						
						deepHandles.Add("PolicyHotelMappingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PolicyHotelMapping >) DataRepository.PolicyHotelMappingProvider.DeepSave,
							new object[] { transactionManager, entity.PolicyHotelMappingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CancelationPolicyLanguage>
				if (CanDeepSave(entity.CancelationPolicyLanguageCollection, "List<CancelationPolicyLanguage>|CancelationPolicyLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CancelationPolicyLanguage child in entity.CancelationPolicyLanguageCollection)
					{
						if(child.CancelationPolicyIdSource != null)
						{
							child.CancelationPolicyId = child.CancelationPolicyIdSource.Id;
						}
						else
						{
							child.CancelationPolicyId = entity.Id;
						}

					}

					if (entity.CancelationPolicyLanguageCollection.Count > 0 || entity.CancelationPolicyLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CancelationPolicyLanguageProvider.Save(transactionManager, entity.CancelationPolicyLanguageCollection);
						
						deepHandles.Add("CancelationPolicyLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CancelationPolicyLanguage >) DataRepository.CancelationPolicyLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.CancelationPolicyLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CancelationPolicyChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.CancelationPolicy</c>
	///</summary>
	public enum CancelationPolicyChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Country</c> at CountryIdSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
	
		///<summary>
		/// Collection of <c>CancelationPolicy</c> as OneToMany for OthersCollection
		///</summary>
		[ChildEntityType(typeof(TList<Others>))]
		OthersCollection,

		///<summary>
		/// Collection of <c>CancelationPolicy</c> as OneToMany for PolicyHotelMappingCollection
		///</summary>
		[ChildEntityType(typeof(TList<PolicyHotelMapping>))]
		PolicyHotelMappingCollection,

		///<summary>
		/// Collection of <c>CancelationPolicy</c> as OneToMany for CancelationPolicyLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<CancelationPolicyLanguage>))]
		CancelationPolicyLanguageCollection,
	}
	
	#endregion CancelationPolicyChildEntityTypes
	
	#region CancelationPolicyFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CancelationPolicyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CancelationPolicy"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CancelationPolicyFilterBuilder : SqlFilterBuilder<CancelationPolicyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyFilterBuilder class.
		/// </summary>
		public CancelationPolicyFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CancelationPolicyFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CancelationPolicyFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CancelationPolicyFilterBuilder
	
	#region CancelationPolicyParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CancelationPolicyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CancelationPolicy"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CancelationPolicyParameterBuilder : ParameterizedSqlFilterBuilder<CancelationPolicyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyParameterBuilder class.
		/// </summary>
		public CancelationPolicyParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CancelationPolicyParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CancelationPolicyParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CancelationPolicyParameterBuilder
	
	#region CancelationPolicySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CancelationPolicyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CancelationPolicy"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CancelationPolicySortBuilder : SqlSortBuilder<CancelationPolicyColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CancelationPolicySqlSortBuilder class.
		/// </summary>
		public CancelationPolicySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CancelationPolicySortBuilder
	
} // end namespace
