﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SpecialPriceAndPromoProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SpecialPriceAndPromoProviderBaseCore : EntityProviderBase<LMMR.Entities.SpecialPriceAndPromo, LMMR.Entities.SpecialPriceAndPromoKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SpecialPriceAndPromoKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceAndPromo_Hotel key.
		///		FK_SpecialPriceAndPromo_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceAndPromo objects.</returns>
		public TList<SpecialPriceAndPromo> GetByHotelId(System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceAndPromo_Hotel key.
		///		FK_SpecialPriceAndPromo_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceAndPromo objects.</returns>
		/// <remarks></remarks>
		public TList<SpecialPriceAndPromo> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceAndPromo_Hotel key.
		///		FK_SpecialPriceAndPromo_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceAndPromo objects.</returns>
		public TList<SpecialPriceAndPromo> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceAndPromo_Hotel key.
		///		fkSpecialPriceAndPromoHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceAndPromo objects.</returns>
		public TList<SpecialPriceAndPromo> GetByHotelId(System.Int64 _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceAndPromo_Hotel key.
		///		fkSpecialPriceAndPromoHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceAndPromo objects.</returns>
		public TList<SpecialPriceAndPromo> GetByHotelId(System.Int64 _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceAndPromo_Hotel key.
		///		FK_SpecialPriceAndPromo_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceAndPromo objects.</returns>
		public abstract TList<SpecialPriceAndPromo> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SpecialPriceAndPromo Get(TransactionManager transactionManager, LMMR.Entities.SpecialPriceAndPromoKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SpecialPriceAndPromo index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceAndPromo"/> class.</returns>
		public LMMR.Entities.SpecialPriceAndPromo GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceAndPromo index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceAndPromo"/> class.</returns>
		public LMMR.Entities.SpecialPriceAndPromo GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceAndPromo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceAndPromo"/> class.</returns>
		public LMMR.Entities.SpecialPriceAndPromo GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceAndPromo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceAndPromo"/> class.</returns>
		public LMMR.Entities.SpecialPriceAndPromo GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceAndPromo index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceAndPromo"/> class.</returns>
		public LMMR.Entities.SpecialPriceAndPromo GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceAndPromo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceAndPromo"/> class.</returns>
		public abstract LMMR.Entities.SpecialPriceAndPromo GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SpecialPriceAndPromo&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SpecialPriceAndPromo&gt;"/></returns>
		public static TList<SpecialPriceAndPromo> Fill(IDataReader reader, TList<SpecialPriceAndPromo> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SpecialPriceAndPromo c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SpecialPriceAndPromo")
					.Append("|").Append((System.Int64)reader[((int)SpecialPriceAndPromoColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SpecialPriceAndPromo>(
					key.ToString(), // EntityTrackingKey
					"SpecialPriceAndPromo",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SpecialPriceAndPromo();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)SpecialPriceAndPromoColumn.Id - 1)];
					c.HotelId = (System.Int64)reader[((int)SpecialPriceAndPromoColumn.HotelId - 1)];
					c.SpecialPriceDate = (reader.IsDBNull(((int)SpecialPriceAndPromoColumn.SpecialPriceDate - 1)))?null:(System.DateTime?)reader[((int)SpecialPriceAndPromoColumn.SpecialPriceDate - 1)];
					c.DdrPercent = (reader.IsDBNull(((int)SpecialPriceAndPromoColumn.DdrPercent - 1)))?null:(System.Decimal?)reader[((int)SpecialPriceAndPromoColumn.DdrPercent - 1)];
					c.MeetingRoomPercent = (reader.IsDBNull(((int)SpecialPriceAndPromoColumn.MeetingRoomPercent - 1)))?null:(System.Decimal?)reader[((int)SpecialPriceAndPromoColumn.MeetingRoomPercent - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SpecialPriceAndPromo"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpecialPriceAndPromo"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SpecialPriceAndPromo entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)SpecialPriceAndPromoColumn.Id - 1)];
			entity.HotelId = (System.Int64)reader[((int)SpecialPriceAndPromoColumn.HotelId - 1)];
			entity.SpecialPriceDate = (reader.IsDBNull(((int)SpecialPriceAndPromoColumn.SpecialPriceDate - 1)))?null:(System.DateTime?)reader[((int)SpecialPriceAndPromoColumn.SpecialPriceDate - 1)];
			entity.DdrPercent = (reader.IsDBNull(((int)SpecialPriceAndPromoColumn.DdrPercent - 1)))?null:(System.Decimal?)reader[((int)SpecialPriceAndPromoColumn.DdrPercent - 1)];
			entity.MeetingRoomPercent = (reader.IsDBNull(((int)SpecialPriceAndPromoColumn.MeetingRoomPercent - 1)))?null:(System.Decimal?)reader[((int)SpecialPriceAndPromoColumn.MeetingRoomPercent - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SpecialPriceAndPromo"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpecialPriceAndPromo"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SpecialPriceAndPromo entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.SpecialPriceDate = Convert.IsDBNull(dataRow["SpecialPriceDate"]) ? null : (System.DateTime?)dataRow["SpecialPriceDate"];
			entity.DdrPercent = Convert.IsDBNull(dataRow["DDRPercent"]) ? null : (System.Decimal?)dataRow["DDRPercent"];
			entity.MeetingRoomPercent = Convert.IsDBNull(dataRow["MeetingRoomPercent"]) ? null : (System.Decimal?)dataRow["MeetingRoomPercent"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpecialPriceAndPromo"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SpecialPriceAndPromo Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SpecialPriceAndPromo entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelId;
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, entity.HotelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region SpandPpackageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SpandPpackage>|SpandPpackageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpandPpackageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SpandPpackageCollection = DataRepository.SpandPpackageProvider.GetBySpandPid(transactionManager, entity.Id);

				if (deep && entity.SpandPpackageCollection.Count > 0)
				{
					deepHandles.Add("SpandPpackageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SpandPpackage>) DataRepository.SpandPpackageProvider.DeepLoad,
						new object[] { transactionManager, entity.SpandPpackageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SpandPbedroomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SpandPbedroom>|SpandPbedroomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpandPbedroomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SpandPbedroomCollection = DataRepository.SpandPbedroomProvider.GetBySpandPid(transactionManager, entity.Id);

				if (deep && entity.SpandPbedroomCollection.Count > 0)
				{
					deepHandles.Add("SpandPbedroomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SpandPbedroom>) DataRepository.SpandPbedroomProvider.DeepLoad,
						new object[] { transactionManager, entity.SpandPbedroomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SpecialPriceAndPromo object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SpecialPriceAndPromo instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SpecialPriceAndPromo Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SpecialPriceAndPromo entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SpandPpackage>
				if (CanDeepSave(entity.SpandPpackageCollection, "List<SpandPpackage>|SpandPpackageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SpandPpackage child in entity.SpandPpackageCollection)
					{
						if(child.SpandPidSource != null)
						{
							child.SpandPid = child.SpandPidSource.Id;
						}
						else
						{
							child.SpandPid = entity.Id;
						}

					}

					if (entity.SpandPpackageCollection.Count > 0 || entity.SpandPpackageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SpandPpackageProvider.Save(transactionManager, entity.SpandPpackageCollection);
						
						deepHandles.Add("SpandPpackageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SpandPpackage >) DataRepository.SpandPpackageProvider.DeepSave,
							new object[] { transactionManager, entity.SpandPpackageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SpandPbedroom>
				if (CanDeepSave(entity.SpandPbedroomCollection, "List<SpandPbedroom>|SpandPbedroomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SpandPbedroom child in entity.SpandPbedroomCollection)
					{
						if(child.SpandPidSource != null)
						{
							child.SpandPid = child.SpandPidSource.Id;
						}
						else
						{
							child.SpandPid = entity.Id;
						}

					}

					if (entity.SpandPbedroomCollection.Count > 0 || entity.SpandPbedroomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SpandPbedroomProvider.Save(transactionManager, entity.SpandPbedroomCollection);
						
						deepHandles.Add("SpandPbedroomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SpandPbedroom >) DataRepository.SpandPbedroomProvider.DeepSave,
							new object[] { transactionManager, entity.SpandPbedroomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SpecialPriceAndPromoChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SpecialPriceAndPromo</c>
	///</summary>
	public enum SpecialPriceAndPromoChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
	
		///<summary>
		/// Collection of <c>SpecialPriceAndPromo</c> as OneToMany for SpandPpackageCollection
		///</summary>
		[ChildEntityType(typeof(TList<SpandPpackage>))]
		SpandPpackageCollection,

		///<summary>
		/// Collection of <c>SpecialPriceAndPromo</c> as OneToMany for SpandPbedroomCollection
		///</summary>
		[ChildEntityType(typeof(TList<SpandPbedroom>))]
		SpandPbedroomCollection,
	}
	
	#endregion SpecialPriceAndPromoChildEntityTypes
	
	#region SpecialPriceAndPromoFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SpecialPriceAndPromoColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceAndPromo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceAndPromoFilterBuilder : SqlFilterBuilder<SpecialPriceAndPromoColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoFilterBuilder class.
		/// </summary>
		public SpecialPriceAndPromoFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceAndPromoFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceAndPromoFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceAndPromoFilterBuilder
	
	#region SpecialPriceAndPromoParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SpecialPriceAndPromoColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceAndPromo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceAndPromoParameterBuilder : ParameterizedSqlFilterBuilder<SpecialPriceAndPromoColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoParameterBuilder class.
		/// </summary>
		public SpecialPriceAndPromoParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceAndPromoParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceAndPromoParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceAndPromoParameterBuilder
	
	#region SpecialPriceAndPromoSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SpecialPriceAndPromoColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceAndPromo"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SpecialPriceAndPromoSortBuilder : SqlSortBuilder<SpecialPriceAndPromoColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoSqlSortBuilder class.
		/// </summary>
		public SpecialPriceAndPromoSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SpecialPriceAndPromoSortBuilder
	
} // end namespace
