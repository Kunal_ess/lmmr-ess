﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RankingAlgoConditionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RankingAlgoConditionProviderBaseCore : EntityProviderBase<LMMR.Entities.RankingAlgoCondition, LMMR.Entities.RankingAlgoConditionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.RankingAlgoConditionKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RankingAlgo_Condition_RankingAlgo_Master key.
		///		FK_RankingAlgo_Condition_RankingAlgo_Master Description: 
		/// </summary>
		/// <param name="_ruleId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.RankingAlgoCondition objects.</returns>
		public TList<RankingAlgoCondition> GetByRuleId(System.Int64 _ruleId)
		{
			int count = -1;
			return GetByRuleId(_ruleId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RankingAlgo_Condition_RankingAlgo_Master key.
		///		FK_RankingAlgo_Condition_RankingAlgo_Master Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ruleId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.RankingAlgoCondition objects.</returns>
		/// <remarks></remarks>
		public TList<RankingAlgoCondition> GetByRuleId(TransactionManager transactionManager, System.Int64 _ruleId)
		{
			int count = -1;
			return GetByRuleId(transactionManager, _ruleId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_RankingAlgo_Condition_RankingAlgo_Master key.
		///		FK_RankingAlgo_Condition_RankingAlgo_Master Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ruleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.RankingAlgoCondition objects.</returns>
		public TList<RankingAlgoCondition> GetByRuleId(TransactionManager transactionManager, System.Int64 _ruleId, int start, int pageLength)
		{
			int count = -1;
			return GetByRuleId(transactionManager, _ruleId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RankingAlgo_Condition_RankingAlgo_Master key.
		///		fkRankingAlgoConditionRankingAlgoMaster Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_ruleId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.RankingAlgoCondition objects.</returns>
		public TList<RankingAlgoCondition> GetByRuleId(System.Int64 _ruleId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRuleId(null, _ruleId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RankingAlgo_Condition_RankingAlgo_Master key.
		///		fkRankingAlgoConditionRankingAlgoMaster Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_ruleId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.RankingAlgoCondition objects.</returns>
		public TList<RankingAlgoCondition> GetByRuleId(System.Int64 _ruleId, int start, int pageLength,out int count)
		{
			return GetByRuleId(null, _ruleId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RankingAlgo_Condition_RankingAlgo_Master key.
		///		FK_RankingAlgo_Condition_RankingAlgo_Master Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ruleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.RankingAlgoCondition objects.</returns>
		public abstract TList<RankingAlgoCondition> GetByRuleId(TransactionManager transactionManager, System.Int64 _ruleId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.RankingAlgoCondition Get(TransactionManager transactionManager, LMMR.Entities.RankingAlgoConditionKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_RankingAlgo_Values index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.RankingAlgoCondition"/> class.</returns>
		public LMMR.Entities.RankingAlgoCondition GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RankingAlgo_Values index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.RankingAlgoCondition"/> class.</returns>
		public LMMR.Entities.RankingAlgoCondition GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RankingAlgo_Values index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.RankingAlgoCondition"/> class.</returns>
		public LMMR.Entities.RankingAlgoCondition GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RankingAlgo_Values index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.RankingAlgoCondition"/> class.</returns>
		public LMMR.Entities.RankingAlgoCondition GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RankingAlgo_Values index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.RankingAlgoCondition"/> class.</returns>
		public LMMR.Entities.RankingAlgoCondition GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RankingAlgo_Values index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.RankingAlgoCondition"/> class.</returns>
		public abstract LMMR.Entities.RankingAlgoCondition GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;RankingAlgoCondition&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;RankingAlgoCondition&gt;"/></returns>
		public static TList<RankingAlgoCondition> Fill(IDataReader reader, TList<RankingAlgoCondition> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.RankingAlgoCondition c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("RankingAlgoCondition")
					.Append("|").Append((System.Int64)reader[((int)RankingAlgoConditionColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<RankingAlgoCondition>(
					key.ToString(), // EntityTrackingKey
					"RankingAlgoCondition",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.RankingAlgoCondition();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)RankingAlgoConditionColumn.Id - 1)];
					c.RuleId = (System.Int64)reader[((int)RankingAlgoConditionColumn.RuleId - 1)];
					c.PointCondition = (reader.IsDBNull(((int)RankingAlgoConditionColumn.PointCondition - 1)))?null:(System.String)reader[((int)RankingAlgoConditionColumn.PointCondition - 1)];
					c.PointValue = (reader.IsDBNull(((int)RankingAlgoConditionColumn.PointValue - 1)))?null:(System.Decimal?)reader[((int)RankingAlgoConditionColumn.PointValue - 1)];
					c.MaxValue = (reader.IsDBNull(((int)RankingAlgoConditionColumn.MaxValue - 1)))?null:(System.Decimal?)reader[((int)RankingAlgoConditionColumn.MaxValue - 1)];
					c.MinValue = (reader.IsDBNull(((int)RankingAlgoConditionColumn.MinValue - 1)))?null:(System.Decimal?)reader[((int)RankingAlgoConditionColumn.MinValue - 1)];
					c.TypeIn = (reader.IsDBNull(((int)RankingAlgoConditionColumn.TypeIn - 1)))?null:(System.String)reader[((int)RankingAlgoConditionColumn.TypeIn - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.RankingAlgoCondition"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.RankingAlgoCondition"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.RankingAlgoCondition entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)RankingAlgoConditionColumn.Id - 1)];
			entity.RuleId = (System.Int64)reader[((int)RankingAlgoConditionColumn.RuleId - 1)];
			entity.PointCondition = (reader.IsDBNull(((int)RankingAlgoConditionColumn.PointCondition - 1)))?null:(System.String)reader[((int)RankingAlgoConditionColumn.PointCondition - 1)];
			entity.PointValue = (reader.IsDBNull(((int)RankingAlgoConditionColumn.PointValue - 1)))?null:(System.Decimal?)reader[((int)RankingAlgoConditionColumn.PointValue - 1)];
			entity.MaxValue = (reader.IsDBNull(((int)RankingAlgoConditionColumn.MaxValue - 1)))?null:(System.Decimal?)reader[((int)RankingAlgoConditionColumn.MaxValue - 1)];
			entity.MinValue = (reader.IsDBNull(((int)RankingAlgoConditionColumn.MinValue - 1)))?null:(System.Decimal?)reader[((int)RankingAlgoConditionColumn.MinValue - 1)];
			entity.TypeIn = (reader.IsDBNull(((int)RankingAlgoConditionColumn.TypeIn - 1)))?null:(System.String)reader[((int)RankingAlgoConditionColumn.TypeIn - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.RankingAlgoCondition"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.RankingAlgoCondition"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.RankingAlgoCondition entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.RuleId = (System.Int64)dataRow["RuleId"];
			entity.PointCondition = Convert.IsDBNull(dataRow["PointCondition"]) ? null : (System.String)dataRow["PointCondition"];
			entity.PointValue = Convert.IsDBNull(dataRow["PointValue"]) ? null : (System.Decimal?)dataRow["PointValue"];
			entity.MaxValue = Convert.IsDBNull(dataRow["MaxValue"]) ? null : (System.Decimal?)dataRow["MaxValue"];
			entity.MinValue = Convert.IsDBNull(dataRow["MinValue"]) ? null : (System.Decimal?)dataRow["MinValue"];
			entity.TypeIn = Convert.IsDBNull(dataRow["TypeIn"]) ? null : (System.String)dataRow["TypeIn"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.RankingAlgoCondition"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.RankingAlgoCondition Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.RankingAlgoCondition entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region RuleIdSource	
			if (CanDeepLoad(entity, "RankingAlgoMaster|RuleIdSource", deepLoadType, innerList) 
				&& entity.RuleIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.RuleId;
				RankingAlgoMaster tmpEntity = EntityManager.LocateEntity<RankingAlgoMaster>(EntityLocator.ConstructKeyFromPkItems(typeof(RankingAlgoMaster), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RuleIdSource = tmpEntity;
				else
					entity.RuleIdSource = DataRepository.RankingAlgoMasterProvider.GetById(transactionManager, entity.RuleId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RuleIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RuleIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RankingAlgoMasterProvider.DeepLoad(transactionManager, entity.RuleIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RuleIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.RankingAlgoCondition object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.RankingAlgoCondition instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.RankingAlgoCondition Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.RankingAlgoCondition entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region RuleIdSource
			if (CanDeepSave(entity, "RankingAlgoMaster|RuleIdSource", deepSaveType, innerList) 
				&& entity.RuleIdSource != null)
			{
				DataRepository.RankingAlgoMasterProvider.Save(transactionManager, entity.RuleIdSource);
				entity.RuleId = entity.RuleIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RankingAlgoConditionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.RankingAlgoCondition</c>
	///</summary>
	public enum RankingAlgoConditionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>RankingAlgoMaster</c> at RuleIdSource
		///</summary>
		[ChildEntityType(typeof(RankingAlgoMaster))]
		RankingAlgoMaster,
		}
	
	#endregion RankingAlgoConditionChildEntityTypes
	
	#region RankingAlgoConditionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RankingAlgoConditionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RankingAlgoCondition"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RankingAlgoConditionFilterBuilder : SqlFilterBuilder<RankingAlgoConditionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionFilterBuilder class.
		/// </summary>
		public RankingAlgoConditionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RankingAlgoConditionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RankingAlgoConditionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RankingAlgoConditionFilterBuilder
	
	#region RankingAlgoConditionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RankingAlgoConditionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RankingAlgoCondition"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RankingAlgoConditionParameterBuilder : ParameterizedSqlFilterBuilder<RankingAlgoConditionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionParameterBuilder class.
		/// </summary>
		public RankingAlgoConditionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RankingAlgoConditionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RankingAlgoConditionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RankingAlgoConditionParameterBuilder
	
	#region RankingAlgoConditionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RankingAlgoConditionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RankingAlgoCondition"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RankingAlgoConditionSortBuilder : SqlSortBuilder<RankingAlgoConditionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionSqlSortBuilder class.
		/// </summary>
		public RankingAlgoConditionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RankingAlgoConditionSortBuilder
	
} // end namespace
