﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AgencyClientProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AgencyClientProviderBaseCore : EntityProviderBase<LMMR.Entities.AgencyClient, LMMR.Entities.AgencyClientKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.AgencyClientKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyClient_AgentUser key.
		///		FK_AgencyClient_AgentUser Description: 
		/// </summary>
		/// <param name="_agencyUserId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyClient objects.</returns>
		public TList<AgencyClient> GetByAgencyUserId(System.Int64 _agencyUserId)
		{
			int count = -1;
			return GetByAgencyUserId(_agencyUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyClient_AgentUser key.
		///		FK_AgencyClient_AgentUser Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyUserId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyClient objects.</returns>
		/// <remarks></remarks>
		public TList<AgencyClient> GetByAgencyUserId(TransactionManager transactionManager, System.Int64 _agencyUserId)
		{
			int count = -1;
			return GetByAgencyUserId(transactionManager, _agencyUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyClient_AgentUser key.
		///		FK_AgencyClient_AgentUser Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyClient objects.</returns>
		public TList<AgencyClient> GetByAgencyUserId(TransactionManager transactionManager, System.Int64 _agencyUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByAgencyUserId(transactionManager, _agencyUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyClient_AgentUser key.
		///		fkAgencyClientAgentUser Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_agencyUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyClient objects.</returns>
		public TList<AgencyClient> GetByAgencyUserId(System.Int64 _agencyUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAgencyUserId(null, _agencyUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyClient_AgentUser key.
		///		fkAgencyClientAgentUser Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_agencyUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyClient objects.</returns>
		public TList<AgencyClient> GetByAgencyUserId(System.Int64 _agencyUserId, int start, int pageLength,out int count)
		{
			return GetByAgencyUserId(null, _agencyUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyClient_AgentUser key.
		///		FK_AgencyClient_AgentUser Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyClient objects.</returns>
		public abstract TList<AgencyClient> GetByAgencyUserId(TransactionManager transactionManager, System.Int64 _agencyUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.AgencyClient Get(TransactionManager transactionManager, LMMR.Entities.AgencyClientKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AgencyClient index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyClient"/> class.</returns>
		public LMMR.Entities.AgencyClient GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyClient index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyClient"/> class.</returns>
		public LMMR.Entities.AgencyClient GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyClient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyClient"/> class.</returns>
		public LMMR.Entities.AgencyClient GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyClient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyClient"/> class.</returns>
		public LMMR.Entities.AgencyClient GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyClient index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyClient"/> class.</returns>
		public LMMR.Entities.AgencyClient GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyClient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyClient"/> class.</returns>
		public abstract LMMR.Entities.AgencyClient GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AgencyClient&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AgencyClient&gt;"/></returns>
		public static TList<AgencyClient> Fill(IDataReader reader, TList<AgencyClient> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.AgencyClient c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AgencyClient")
					.Append("|").Append((System.Int64)reader[((int)AgencyClientColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AgencyClient>(
					key.ToString(), // EntityTrackingKey
					"AgencyClient",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.AgencyClient();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)AgencyClientColumn.Id - 1)];
					c.AgencyUserId = (System.Int64)reader[((int)AgencyClientColumn.AgencyUserId - 1)];
					c.FirstName = (reader.IsDBNull(((int)AgencyClientColumn.FirstName - 1)))?null:(System.String)reader[((int)AgencyClientColumn.FirstName - 1)];
					c.LastName = (reader.IsDBNull(((int)AgencyClientColumn.LastName - 1)))?null:(System.String)reader[((int)AgencyClientColumn.LastName - 1)];
					c.CompanyName = (reader.IsDBNull(((int)AgencyClientColumn.CompanyName - 1)))?null:(System.String)reader[((int)AgencyClientColumn.CompanyName - 1)];
					c.PhoneNumber = (reader.IsDBNull(((int)AgencyClientColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)AgencyClientColumn.PhoneNumber - 1)];
					c.CreationDate = (reader.IsDBNull(((int)AgencyClientColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)AgencyClientColumn.CreationDate - 1)];
					c.IsActive = (System.Boolean)reader[((int)AgencyClientColumn.IsActive - 1)];
					c.IsDeleted = (System.Boolean)reader[((int)AgencyClientColumn.IsDeleted - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.AgencyClient"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgencyClient"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.AgencyClient entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)AgencyClientColumn.Id - 1)];
			entity.AgencyUserId = (System.Int64)reader[((int)AgencyClientColumn.AgencyUserId - 1)];
			entity.FirstName = (reader.IsDBNull(((int)AgencyClientColumn.FirstName - 1)))?null:(System.String)reader[((int)AgencyClientColumn.FirstName - 1)];
			entity.LastName = (reader.IsDBNull(((int)AgencyClientColumn.LastName - 1)))?null:(System.String)reader[((int)AgencyClientColumn.LastName - 1)];
			entity.CompanyName = (reader.IsDBNull(((int)AgencyClientColumn.CompanyName - 1)))?null:(System.String)reader[((int)AgencyClientColumn.CompanyName - 1)];
			entity.PhoneNumber = (reader.IsDBNull(((int)AgencyClientColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)AgencyClientColumn.PhoneNumber - 1)];
			entity.CreationDate = (reader.IsDBNull(((int)AgencyClientColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)AgencyClientColumn.CreationDate - 1)];
			entity.IsActive = (System.Boolean)reader[((int)AgencyClientColumn.IsActive - 1)];
			entity.IsDeleted = (System.Boolean)reader[((int)AgencyClientColumn.IsDeleted - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.AgencyClient"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgencyClient"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.AgencyClient entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.AgencyUserId = (System.Int64)dataRow["AgencyUserId"];
			entity.FirstName = Convert.IsDBNull(dataRow["FirstName"]) ? null : (System.String)dataRow["FirstName"];
			entity.LastName = Convert.IsDBNull(dataRow["LastName"]) ? null : (System.String)dataRow["LastName"];
			entity.CompanyName = Convert.IsDBNull(dataRow["CompanyName"]) ? null : (System.String)dataRow["CompanyName"];
			entity.PhoneNumber = Convert.IsDBNull(dataRow["PhoneNumber"]) ? null : (System.String)dataRow["PhoneNumber"];
			entity.CreationDate = Convert.IsDBNull(dataRow["CreationDate"]) ? null : (System.DateTime?)dataRow["CreationDate"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.IsDeleted = (System.Boolean)dataRow["IsDeleted"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgencyClient"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.AgencyClient Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.AgencyClient entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AgencyUserIdSource	
			if (CanDeepLoad(entity, "AgentUser|AgencyUserIdSource", deepLoadType, innerList) 
				&& entity.AgencyUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AgencyUserId;
				AgentUser tmpEntity = EntityManager.LocateEntity<AgentUser>(EntityLocator.ConstructKeyFromPkItems(typeof(AgentUser), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AgencyUserIdSource = tmpEntity;
				else
					entity.AgencyUserIdSource = DataRepository.AgentUserProvider.GetById(transactionManager, entity.AgencyUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AgencyUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AgencyUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AgentUserProvider.DeepLoad(transactionManager, entity.AgencyUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AgencyUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region AgencyInvoiceCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AgencyInvoice>|AgencyInvoiceCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AgencyInvoiceCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AgencyInvoiceCollection = DataRepository.AgencyInvoiceProvider.GetByAgencyClientId(transactionManager, entity.Id);

				if (deep && entity.AgencyInvoiceCollection.Count > 0)
				{
					deepHandles.Add("AgencyInvoiceCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AgencyInvoice>) DataRepository.AgencyInvoiceProvider.DeepLoad,
						new object[] { transactionManager, entity.AgencyInvoiceCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.AgencyClient object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.AgencyClient instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.AgencyClient Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.AgencyClient entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AgencyUserIdSource
			if (CanDeepSave(entity, "AgentUser|AgencyUserIdSource", deepSaveType, innerList) 
				&& entity.AgencyUserIdSource != null)
			{
				DataRepository.AgentUserProvider.Save(transactionManager, entity.AgencyUserIdSource);
				entity.AgencyUserId = entity.AgencyUserIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AgencyInvoice>
				if (CanDeepSave(entity.AgencyInvoiceCollection, "List<AgencyInvoice>|AgencyInvoiceCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AgencyInvoice child in entity.AgencyInvoiceCollection)
					{
						if(child.AgencyClientIdSource != null)
						{
							child.AgencyClientId = child.AgencyClientIdSource.Id;
						}
						else
						{
							child.AgencyClientId = entity.Id;
						}

					}

					if (entity.AgencyInvoiceCollection.Count > 0 || entity.AgencyInvoiceCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AgencyInvoiceProvider.Save(transactionManager, entity.AgencyInvoiceCollection);
						
						deepHandles.Add("AgencyInvoiceCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AgencyInvoice >) DataRepository.AgencyInvoiceProvider.DeepSave,
							new object[] { transactionManager, entity.AgencyInvoiceCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AgencyClientChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.AgencyClient</c>
	///</summary>
	public enum AgencyClientChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AgentUser</c> at AgencyUserIdSource
		///</summary>
		[ChildEntityType(typeof(AgentUser))]
		AgentUser,
	
		///<summary>
		/// Collection of <c>AgencyClient</c> as OneToMany for AgencyInvoiceCollection
		///</summary>
		[ChildEntityType(typeof(TList<AgencyInvoice>))]
		AgencyInvoiceCollection,
	}
	
	#endregion AgencyClientChildEntityTypes
	
	#region AgencyClientFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AgencyClientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgencyClient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgencyClientFilterBuilder : SqlFilterBuilder<AgencyClientColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyClientFilterBuilder class.
		/// </summary>
		public AgencyClientFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgencyClientFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgencyClientFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgencyClientFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgencyClientFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgencyClientFilterBuilder
	
	#region AgencyClientParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AgencyClientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgencyClient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgencyClientParameterBuilder : ParameterizedSqlFilterBuilder<AgencyClientColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyClientParameterBuilder class.
		/// </summary>
		public AgencyClientParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgencyClientParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgencyClientParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgencyClientParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgencyClientParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgencyClientParameterBuilder
	
	#region AgencyClientSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AgencyClientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgencyClient"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AgencyClientSortBuilder : SqlSortBuilder<AgencyClientColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyClientSqlSortBuilder class.
		/// </summary>
		public AgencyClientSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AgencyClientSortBuilder
	
} // end namespace
