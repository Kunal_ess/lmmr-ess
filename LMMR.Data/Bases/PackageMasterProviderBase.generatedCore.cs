﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PackageMasterProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PackageMasterProviderBaseCore : EntityProviderBase<LMMR.Entities.PackageMaster, LMMR.Entities.PackageMasterKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.PackageMasterKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageMaster_Country key.
		///		FK_PackageMaster_Country Description: 
		/// </summary>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageMaster objects.</returns>
		public TList<PackageMaster> GetByCountryId(System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(_countryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageMaster_Country key.
		///		FK_PackageMaster_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageMaster objects.</returns>
		/// <remarks></remarks>
		public TList<PackageMaster> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageMaster_Country key.
		///		FK_PackageMaster_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageMaster objects.</returns>
		public TList<PackageMaster> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageMaster_Country key.
		///		fkPackageMasterCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageMaster objects.</returns>
		public TList<PackageMaster> GetByCountryId(System.Int64 _countryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryId(null, _countryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageMaster_Country key.
		///		fkPackageMasterCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageMaster objects.</returns>
		public TList<PackageMaster> GetByCountryId(System.Int64 _countryId, int start, int pageLength,out int count)
		{
			return GetByCountryId(null, _countryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageMaster_Country key.
		///		FK_PackageMaster_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageMaster objects.</returns>
		public abstract TList<PackageMaster> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.PackageMaster Get(TransactionManager transactionManager, LMMR.Entities.PackageMasterKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SectionType index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageMaster"/> class.</returns>
		public LMMR.Entities.PackageMaster GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionType index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageMaster"/> class.</returns>
		public LMMR.Entities.PackageMaster GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageMaster"/> class.</returns>
		public LMMR.Entities.PackageMaster GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageMaster"/> class.</returns>
		public LMMR.Entities.PackageMaster GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionType index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageMaster"/> class.</returns>
		public LMMR.Entities.PackageMaster GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageMaster"/> class.</returns>
		public abstract LMMR.Entities.PackageMaster GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PackageMaster&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PackageMaster&gt;"/></returns>
		public static TList<PackageMaster> Fill(IDataReader reader, TList<PackageMaster> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.PackageMaster c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PackageMaster")
					.Append("|").Append((System.Int64)reader[((int)PackageMasterColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PackageMaster>(
					key.ToString(), // EntityTrackingKey
					"PackageMaster",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.PackageMaster();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)PackageMasterColumn.Id - 1)];
					c.PackageName = (reader.IsDBNull(((int)PackageMasterColumn.PackageName - 1)))?null:(System.String)reader[((int)PackageMasterColumn.PackageName - 1)];
					c.IsActive = (reader.IsDBNull(((int)PackageMasterColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)PackageMasterColumn.IsActive - 1)];
					c.CountryId = (System.Int64)reader[((int)PackageMasterColumn.CountryId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageMaster"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageMaster"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.PackageMaster entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)PackageMasterColumn.Id - 1)];
			entity.PackageName = (reader.IsDBNull(((int)PackageMasterColumn.PackageName - 1)))?null:(System.String)reader[((int)PackageMasterColumn.PackageName - 1)];
			entity.IsActive = (reader.IsDBNull(((int)PackageMasterColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)PackageMasterColumn.IsActive - 1)];
			entity.CountryId = (System.Int64)reader[((int)PackageMasterColumn.CountryId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageMaster"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageMaster"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.PackageMaster entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.PackageName = Convert.IsDBNull(dataRow["PackageName"]) ? null : (System.String)dataRow["PackageName"];
			entity.IsActive = Convert.IsDBNull(dataRow["IsActive"]) ? null : (System.Boolean?)dataRow["IsActive"];
			entity.CountryId = (System.Int64)dataRow["CountryId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageMaster"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.PackageMaster Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.PackageMaster entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CountryIdSource	
			if (CanDeepLoad(entity, "Country|CountryIdSource", deepLoadType, innerList) 
				&& entity.CountryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CountryId;
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryIdSource = tmpEntity;
				else
					entity.CountryIdSource = DataRepository.CountryProvider.GetById(transactionManager, entity.CountryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region PackageByHotelCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageByHotel>|PackageByHotelCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageByHotelCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageByHotelCollection = DataRepository.PackageByHotelProvider.GetByPackageId(transactionManager, entity.Id);

				if (deep && entity.PackageByHotelCollection.Count > 0)
				{
					deepHandles.Add("PackageByHotelCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageByHotel>) DataRepository.PackageByHotelProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageByHotelCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ActualPackagePriceCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ActualPackagePrice>|ActualPackagePriceCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ActualPackagePriceCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ActualPackagePriceCollection = DataRepository.ActualPackagePriceProvider.GetByPackageId(transactionManager, entity.Id);

				if (deep && entity.ActualPackagePriceCollection.Count > 0)
				{
					deepHandles.Add("ActualPackagePriceCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ActualPackagePrice>) DataRepository.ActualPackagePriceProvider.DeepLoad,
						new object[] { transactionManager, entity.ActualPackagePriceCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BuildPackageConfigureCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BuildPackageConfigure>|BuildPackageConfigureCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildPackageConfigureCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BuildPackageConfigureCollection = DataRepository.BuildPackageConfigureProvider.GetByPackageItemId(transactionManager, entity.Id);

				if (deep && entity.BuildPackageConfigureCollection.Count > 0)
				{
					deepHandles.Add("BuildPackageConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BuildPackageConfigure>) DataRepository.BuildPackageConfigureProvider.DeepLoad,
						new object[] { transactionManager, entity.BuildPackageConfigureCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageMasterDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageMasterDescription>|PackageMasterDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageMasterDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageMasterDescriptionCollection = DataRepository.PackageMasterDescriptionProvider.GetByPackageMasterId(transactionManager, entity.Id);

				if (deep && entity.PackageMasterDescriptionCollection.Count > 0)
				{
					deepHandles.Add("PackageMasterDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageMasterDescription>) DataRepository.PackageMasterDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageMasterDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageItemMappingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageItemMapping>|PackageItemMappingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageItemMappingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageItemMappingCollection = DataRepository.PackageItemMappingProvider.GetByPackageId(transactionManager, entity.Id);

				if (deep && entity.PackageItemMappingCollection.Count > 0)
				{
					deepHandles.Add("PackageItemMappingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageItemMapping>) DataRepository.PackageItemMappingProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageItemMappingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.PackageMaster object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.PackageMaster instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.PackageMaster Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.PackageMaster entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CountryIdSource
			if (CanDeepSave(entity, "Country|CountryIdSource", deepSaveType, innerList) 
				&& entity.CountryIdSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryIdSource);
				entity.CountryId = entity.CountryIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<PackageByHotel>
				if (CanDeepSave(entity.PackageByHotelCollection, "List<PackageByHotel>|PackageByHotelCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageByHotel child in entity.PackageByHotelCollection)
					{
						if(child.PackageIdSource != null)
						{
							child.PackageId = child.PackageIdSource.Id;
						}
						else
						{
							child.PackageId = entity.Id;
						}

					}

					if (entity.PackageByHotelCollection.Count > 0 || entity.PackageByHotelCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageByHotelProvider.Save(transactionManager, entity.PackageByHotelCollection);
						
						deepHandles.Add("PackageByHotelCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageByHotel >) DataRepository.PackageByHotelProvider.DeepSave,
							new object[] { transactionManager, entity.PackageByHotelCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ActualPackagePrice>
				if (CanDeepSave(entity.ActualPackagePriceCollection, "List<ActualPackagePrice>|ActualPackagePriceCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ActualPackagePrice child in entity.ActualPackagePriceCollection)
					{
						if(child.PackageIdSource != null)
						{
							child.PackageId = child.PackageIdSource.Id;
						}
						else
						{
							child.PackageId = entity.Id;
						}

					}

					if (entity.ActualPackagePriceCollection.Count > 0 || entity.ActualPackagePriceCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ActualPackagePriceProvider.Save(transactionManager, entity.ActualPackagePriceCollection);
						
						deepHandles.Add("ActualPackagePriceCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ActualPackagePrice >) DataRepository.ActualPackagePriceProvider.DeepSave,
							new object[] { transactionManager, entity.ActualPackagePriceCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BuildPackageConfigure>
				if (CanDeepSave(entity.BuildPackageConfigureCollection, "List<BuildPackageConfigure>|BuildPackageConfigureCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BuildPackageConfigure child in entity.BuildPackageConfigureCollection)
					{
						if(child.PackageItemIdSource != null)
						{
							child.PackageItemId = child.PackageItemIdSource.Id;
						}
						else
						{
							child.PackageItemId = entity.Id;
						}

					}

					if (entity.BuildPackageConfigureCollection.Count > 0 || entity.BuildPackageConfigureCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BuildPackageConfigureProvider.Save(transactionManager, entity.BuildPackageConfigureCollection);
						
						deepHandles.Add("BuildPackageConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BuildPackageConfigure >) DataRepository.BuildPackageConfigureProvider.DeepSave,
							new object[] { transactionManager, entity.BuildPackageConfigureCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageMasterDescription>
				if (CanDeepSave(entity.PackageMasterDescriptionCollection, "List<PackageMasterDescription>|PackageMasterDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageMasterDescription child in entity.PackageMasterDescriptionCollection)
					{
						if(child.PackageMasterIdSource != null)
						{
							child.PackageMasterId = child.PackageMasterIdSource.Id;
						}
						else
						{
							child.PackageMasterId = entity.Id;
						}

					}

					if (entity.PackageMasterDescriptionCollection.Count > 0 || entity.PackageMasterDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageMasterDescriptionProvider.Save(transactionManager, entity.PackageMasterDescriptionCollection);
						
						deepHandles.Add("PackageMasterDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageMasterDescription >) DataRepository.PackageMasterDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.PackageMasterDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageItemMapping>
				if (CanDeepSave(entity.PackageItemMappingCollection, "List<PackageItemMapping>|PackageItemMappingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageItemMapping child in entity.PackageItemMappingCollection)
					{
						if(child.PackageIdSource != null)
						{
							child.PackageId = child.PackageIdSource.Id;
						}
						else
						{
							child.PackageId = entity.Id;
						}

					}

					if (entity.PackageItemMappingCollection.Count > 0 || entity.PackageItemMappingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageItemMappingProvider.Save(transactionManager, entity.PackageItemMappingCollection);
						
						deepHandles.Add("PackageItemMappingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageItemMapping >) DataRepository.PackageItemMappingProvider.DeepSave,
							new object[] { transactionManager, entity.PackageItemMappingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PackageMasterChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.PackageMaster</c>
	///</summary>
	public enum PackageMasterChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Country</c> at CountryIdSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
	
		///<summary>
		/// Collection of <c>PackageMaster</c> as OneToMany for PackageByHotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageByHotel>))]
		PackageByHotelCollection,

		///<summary>
		/// Collection of <c>PackageMaster</c> as OneToMany for ActualPackagePriceCollection
		///</summary>
		[ChildEntityType(typeof(TList<ActualPackagePrice>))]
		ActualPackagePriceCollection,

		///<summary>
		/// Collection of <c>PackageMaster</c> as OneToMany for BuildPackageConfigureCollection
		///</summary>
		[ChildEntityType(typeof(TList<BuildPackageConfigure>))]
		BuildPackageConfigureCollection,

		///<summary>
		/// Collection of <c>PackageMaster</c> as OneToMany for PackageMasterDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageMasterDescription>))]
		PackageMasterDescriptionCollection,

		///<summary>
		/// Collection of <c>PackageMaster</c> as OneToMany for PackageItemMappingCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageItemMapping>))]
		PackageItemMappingCollection,
	}
	
	#endregion PackageMasterChildEntityTypes
	
	#region PackageMasterFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PackageMasterColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageMaster"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageMasterFilterBuilder : SqlFilterBuilder<PackageMasterColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageMasterFilterBuilder class.
		/// </summary>
		public PackageMasterFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageMasterFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageMasterFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageMasterFilterBuilder
	
	#region PackageMasterParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PackageMasterColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageMaster"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageMasterParameterBuilder : ParameterizedSqlFilterBuilder<PackageMasterColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageMasterParameterBuilder class.
		/// </summary>
		public PackageMasterParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageMasterParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageMasterParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageMasterParameterBuilder
	
	#region PackageMasterSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PackageMasterColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageMaster"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PackageMasterSortBuilder : SqlSortBuilder<PackageMasterColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageMasterSqlSortBuilder class.
		/// </summary>
		public PackageMasterSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PackageMasterSortBuilder
	
} // end namespace
