﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="LanguageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class LanguageProviderBaseCore : EntityProviderBase<LMMR.Entities.Language, LMMR.Entities.LanguageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.LanguageKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Language Get(TransactionManager transactionManager, LMMR.Entities.LanguageKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Language index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Language"/> class.</returns>
		public LMMR.Entities.Language GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Language index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Language"/> class.</returns>
		public LMMR.Entities.Language GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Language index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Language"/> class.</returns>
		public LMMR.Entities.Language GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Language index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Language"/> class.</returns>
		public LMMR.Entities.Language GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Language index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Language"/> class.</returns>
		public LMMR.Entities.Language GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Language index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Language"/> class.</returns>
		public abstract LMMR.Entities.Language GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Language&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Language&gt;"/></returns>
		public static TList<Language> Fill(IDataReader reader, TList<Language> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Language c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Language")
					.Append("|").Append((System.Int64)reader[((int)LanguageColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Language>(
					key.ToString(), // EntityTrackingKey
					"Language",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Language();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)LanguageColumn.Id - 1)];
					c.Name = (reader.IsDBNull(((int)LanguageColumn.Name - 1)))?null:(System.String)reader[((int)LanguageColumn.Name - 1)];
					c.ShortName = (reader.IsDBNull(((int)LanguageColumn.ShortName - 1)))?null:(System.String)reader[((int)LanguageColumn.ShortName - 1)];
					c.ResourceFile = (reader.IsDBNull(((int)LanguageColumn.ResourceFile - 1)))?null:(System.String)reader[((int)LanguageColumn.ResourceFile - 1)];
					c.IsActive = (System.Boolean)reader[((int)LanguageColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Language"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Language"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Language entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)LanguageColumn.Id - 1)];
			entity.Name = (reader.IsDBNull(((int)LanguageColumn.Name - 1)))?null:(System.String)reader[((int)LanguageColumn.Name - 1)];
			entity.ShortName = (reader.IsDBNull(((int)LanguageColumn.ShortName - 1)))?null:(System.String)reader[((int)LanguageColumn.ShortName - 1)];
			entity.ResourceFile = (reader.IsDBNull(((int)LanguageColumn.ResourceFile - 1)))?null:(System.String)reader[((int)LanguageColumn.ResourceFile - 1)];
			entity.IsActive = (System.Boolean)reader[((int)LanguageColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Language"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Language"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Language entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.ShortName = Convert.IsDBNull(dataRow["ShortName"]) ? null : (System.String)dataRow["ShortName"];
			entity.ResourceFile = Convert.IsDBNull(dataRow["ResourceFile"]) ? null : (System.String)dataRow["ResourceFile"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Language"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Language Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Language entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region StaticLebelDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<StaticLebelDescription>|StaticLebelDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'StaticLebelDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.StaticLebelDescriptionCollection = DataRepository.StaticLebelDescriptionProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.StaticLebelDescriptionCollection.Count > 0)
				{
					deepHandles.Add("StaticLebelDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<StaticLebelDescription>) DataRepository.StaticLebelDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.StaticLebelDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CityLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CityLanguage>|CityLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CityLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CityLanguageCollection = DataRepository.CityLanguageProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.CityLanguageCollection.Count > 0)
				{
					deepHandles.Add("CityLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CityLanguage>) DataRepository.CityLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.CityLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ServayDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ServayDescription>|ServayDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServayDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ServayDescriptionCollection = DataRepository.ServayDescriptionProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.ServayDescriptionCollection.Count > 0)
				{
					deepHandles.Add("ServayDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ServayDescription>) DataRepository.ServayDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.ServayDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BedRoomDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoomDesc>|BedRoomDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomDescCollection = DataRepository.BedRoomDescProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.BedRoomDescCollection.Count > 0)
				{
					deepHandles.Add("BedRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoomDesc>) DataRepository.BedRoomDescProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelDesc>|HotelDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelDescCollection = DataRepository.HotelDescProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.HotelDescCollection.Count > 0)
				{
					deepHandles.Add("HotelDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelDesc>) DataRepository.HotelDescProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CountryLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CountryLanguage>|CountryLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CountryLanguageCollection = DataRepository.CountryLanguageProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.CountryLanguageCollection.Count > 0)
				{
					deepHandles.Add("CountryLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CountryLanguage>) DataRepository.CountryLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.CountryLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FrontEndBottomDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FrontEndBottomDescription>|FrontEndBottomDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FrontEndBottomDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FrontEndBottomDescriptionCollection = DataRepository.FrontEndBottomDescriptionProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.FrontEndBottomDescriptionCollection.Count > 0)
				{
					deepHandles.Add("FrontEndBottomDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FrontEndBottomDescription>) DataRepository.FrontEndBottomDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.FrontEndBottomDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OtherDescriptionLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<OtherDescriptionLanguage>|OtherDescriptionLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OtherDescriptionLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OtherDescriptionLanguageCollection = DataRepository.OtherDescriptionLanguageProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.OtherDescriptionLanguageCollection.Count > 0)
				{
					deepHandles.Add("OtherDescriptionLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<OtherDescriptionLanguage>) DataRepository.OtherDescriptionLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.OtherDescriptionLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageMasterDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageMasterDescription>|PackageMasterDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageMasterDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageMasterDescriptionCollection = DataRepository.PackageMasterDescriptionProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.PackageMasterDescriptionCollection.Count > 0)
				{
					deepHandles.Add("PackageMasterDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageMasterDescription>) DataRepository.PackageMasterDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageMasterDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ZoneLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ZoneLanguage>|ZoneLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ZoneLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ZoneLanguageCollection = DataRepository.ZoneLanguageProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.ZoneLanguageCollection.Count > 0)
				{
					deepHandles.Add("ZoneLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ZoneLanguage>) DataRepository.ZoneLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.ZoneLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CancelationPolicyLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CancelationPolicyLanguage>|CancelationPolicyLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CancelationPolicyLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CancelationPolicyLanguageCollection = DataRepository.CancelationPolicyLanguageProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.CancelationPolicyLanguageCollection.Count > 0)
				{
					deepHandles.Add("CancelationPolicyLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CancelationPolicyLanguage>) DataRepository.CancelationPolicyLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.CancelationPolicyLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region EmailConfigMappingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EmailConfigMapping>|EmailConfigMappingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EmailConfigMappingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EmailConfigMappingCollection = DataRepository.EmailConfigMappingProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.EmailConfigMappingCollection.Count > 0)
				{
					deepHandles.Add("EmailConfigMappingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EmailConfigMapping>) DataRepository.EmailConfigMappingProvider.DeepLoad,
						new object[] { transactionManager, entity.EmailConfigMappingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomDesc>|MeetingRoomDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomDescCollection = DataRepository.MeetingRoomDescProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.MeetingRoomDescCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomDesc>) DataRepository.MeetingRoomDescProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ServeyAnswerDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ServeyAnswerDescription>|ServeyAnswerDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServeyAnswerDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ServeyAnswerDescriptionCollection = DataRepository.ServeyAnswerDescriptionProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.ServeyAnswerDescriptionCollection.Count > 0)
				{
					deepHandles.Add("ServeyAnswerDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ServeyAnswerDescription>) DataRepository.ServeyAnswerDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.ServeyAnswerDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CmsDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CmsDescription>|CmsDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CmsDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CmsDescriptionCollection = DataRepository.CmsDescriptionProvider.GetByLanguageId(transactionManager, entity.Id);

				if (deep && entity.CmsDescriptionCollection.Count > 0)
				{
					deepHandles.Add("CmsDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CmsDescription>) DataRepository.CmsDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.CmsDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Language object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Language instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Language Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Language entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<StaticLebelDescription>
				if (CanDeepSave(entity.StaticLebelDescriptionCollection, "List<StaticLebelDescription>|StaticLebelDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(StaticLebelDescription child in entity.StaticLebelDescriptionCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.StaticLebelDescriptionCollection.Count > 0 || entity.StaticLebelDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.StaticLebelDescriptionProvider.Save(transactionManager, entity.StaticLebelDescriptionCollection);
						
						deepHandles.Add("StaticLebelDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< StaticLebelDescription >) DataRepository.StaticLebelDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.StaticLebelDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CityLanguage>
				if (CanDeepSave(entity.CityLanguageCollection, "List<CityLanguage>|CityLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CityLanguage child in entity.CityLanguageCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.CityLanguageCollection.Count > 0 || entity.CityLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CityLanguageProvider.Save(transactionManager, entity.CityLanguageCollection);
						
						deepHandles.Add("CityLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CityLanguage >) DataRepository.CityLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.CityLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ServayDescription>
				if (CanDeepSave(entity.ServayDescriptionCollection, "List<ServayDescription>|ServayDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ServayDescription child in entity.ServayDescriptionCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.ServayDescriptionCollection.Count > 0 || entity.ServayDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ServayDescriptionProvider.Save(transactionManager, entity.ServayDescriptionCollection);
						
						deepHandles.Add("ServayDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ServayDescription >) DataRepository.ServayDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.ServayDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BedRoomDesc>
				if (CanDeepSave(entity.BedRoomDescCollection, "List<BedRoomDesc>|BedRoomDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoomDesc child in entity.BedRoomDescCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.BedRoomDescCollection.Count > 0 || entity.BedRoomDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomDescProvider.Save(transactionManager, entity.BedRoomDescCollection);
						
						deepHandles.Add("BedRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoomDesc >) DataRepository.BedRoomDescProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelDesc>
				if (CanDeepSave(entity.HotelDescCollection, "List<HotelDesc>|HotelDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelDesc child in entity.HotelDescCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.HotelDescCollection.Count > 0 || entity.HotelDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelDescProvider.Save(transactionManager, entity.HotelDescCollection);
						
						deepHandles.Add("HotelDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelDesc >) DataRepository.HotelDescProvider.DeepSave,
							new object[] { transactionManager, entity.HotelDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CountryLanguage>
				if (CanDeepSave(entity.CountryLanguageCollection, "List<CountryLanguage>|CountryLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CountryLanguage child in entity.CountryLanguageCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.CountryLanguageCollection.Count > 0 || entity.CountryLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CountryLanguageProvider.Save(transactionManager, entity.CountryLanguageCollection);
						
						deepHandles.Add("CountryLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CountryLanguage >) DataRepository.CountryLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.CountryLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FrontEndBottomDescription>
				if (CanDeepSave(entity.FrontEndBottomDescriptionCollection, "List<FrontEndBottomDescription>|FrontEndBottomDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FrontEndBottomDescription child in entity.FrontEndBottomDescriptionCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.FrontEndBottomDescriptionCollection.Count > 0 || entity.FrontEndBottomDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FrontEndBottomDescriptionProvider.Save(transactionManager, entity.FrontEndBottomDescriptionCollection);
						
						deepHandles.Add("FrontEndBottomDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FrontEndBottomDescription >) DataRepository.FrontEndBottomDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.FrontEndBottomDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<OtherDescriptionLanguage>
				if (CanDeepSave(entity.OtherDescriptionLanguageCollection, "List<OtherDescriptionLanguage>|OtherDescriptionLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(OtherDescriptionLanguage child in entity.OtherDescriptionLanguageCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.OtherDescriptionLanguageCollection.Count > 0 || entity.OtherDescriptionLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OtherDescriptionLanguageProvider.Save(transactionManager, entity.OtherDescriptionLanguageCollection);
						
						deepHandles.Add("OtherDescriptionLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< OtherDescriptionLanguage >) DataRepository.OtherDescriptionLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.OtherDescriptionLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageMasterDescription>
				if (CanDeepSave(entity.PackageMasterDescriptionCollection, "List<PackageMasterDescription>|PackageMasterDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageMasterDescription child in entity.PackageMasterDescriptionCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.PackageMasterDescriptionCollection.Count > 0 || entity.PackageMasterDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageMasterDescriptionProvider.Save(transactionManager, entity.PackageMasterDescriptionCollection);
						
						deepHandles.Add("PackageMasterDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageMasterDescription >) DataRepository.PackageMasterDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.PackageMasterDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ZoneLanguage>
				if (CanDeepSave(entity.ZoneLanguageCollection, "List<ZoneLanguage>|ZoneLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ZoneLanguage child in entity.ZoneLanguageCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.ZoneLanguageCollection.Count > 0 || entity.ZoneLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ZoneLanguageProvider.Save(transactionManager, entity.ZoneLanguageCollection);
						
						deepHandles.Add("ZoneLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ZoneLanguage >) DataRepository.ZoneLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.ZoneLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CancelationPolicyLanguage>
				if (CanDeepSave(entity.CancelationPolicyLanguageCollection, "List<CancelationPolicyLanguage>|CancelationPolicyLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CancelationPolicyLanguage child in entity.CancelationPolicyLanguageCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.CancelationPolicyLanguageCollection.Count > 0 || entity.CancelationPolicyLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CancelationPolicyLanguageProvider.Save(transactionManager, entity.CancelationPolicyLanguageCollection);
						
						deepHandles.Add("CancelationPolicyLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CancelationPolicyLanguage >) DataRepository.CancelationPolicyLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.CancelationPolicyLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<EmailConfigMapping>
				if (CanDeepSave(entity.EmailConfigMappingCollection, "List<EmailConfigMapping>|EmailConfigMappingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EmailConfigMapping child in entity.EmailConfigMappingCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.EmailConfigMappingCollection.Count > 0 || entity.EmailConfigMappingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EmailConfigMappingProvider.Save(transactionManager, entity.EmailConfigMappingCollection);
						
						deepHandles.Add("EmailConfigMappingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EmailConfigMapping >) DataRepository.EmailConfigMappingProvider.DeepSave,
							new object[] { transactionManager, entity.EmailConfigMappingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoomDesc>
				if (CanDeepSave(entity.MeetingRoomDescCollection, "List<MeetingRoomDesc>|MeetingRoomDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomDesc child in entity.MeetingRoomDescCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.MeetingRoomDescCollection.Count > 0 || entity.MeetingRoomDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomDescProvider.Save(transactionManager, entity.MeetingRoomDescCollection);
						
						deepHandles.Add("MeetingRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomDesc >) DataRepository.MeetingRoomDescProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ServeyAnswerDescription>
				if (CanDeepSave(entity.ServeyAnswerDescriptionCollection, "List<ServeyAnswerDescription>|ServeyAnswerDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ServeyAnswerDescription child in entity.ServeyAnswerDescriptionCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.ServeyAnswerDescriptionCollection.Count > 0 || entity.ServeyAnswerDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ServeyAnswerDescriptionProvider.Save(transactionManager, entity.ServeyAnswerDescriptionCollection);
						
						deepHandles.Add("ServeyAnswerDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ServeyAnswerDescription >) DataRepository.ServeyAnswerDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.ServeyAnswerDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CmsDescription>
				if (CanDeepSave(entity.CmsDescriptionCollection, "List<CmsDescription>|CmsDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CmsDescription child in entity.CmsDescriptionCollection)
					{
						if(child.LanguageIdSource != null)
						{
							child.LanguageId = child.LanguageIdSource.Id;
						}
						else
						{
							child.LanguageId = entity.Id;
						}

					}

					if (entity.CmsDescriptionCollection.Count > 0 || entity.CmsDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CmsDescriptionProvider.Save(transactionManager, entity.CmsDescriptionCollection);
						
						deepHandles.Add("CmsDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CmsDescription >) DataRepository.CmsDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.CmsDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region LanguageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Language</c>
	///</summary>
	public enum LanguageChildEntityTypes
	{

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for StaticLebelDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<StaticLebelDescription>))]
		StaticLebelDescriptionCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for CityLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<CityLanguage>))]
		CityLanguageCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for ServayDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<ServayDescription>))]
		ServayDescriptionCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for BedRoomDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoomDesc>))]
		BedRoomDescCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for HotelDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelDesc>))]
		HotelDescCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for CountryLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<CountryLanguage>))]
		CountryLanguageCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for FrontEndBottomDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<FrontEndBottomDescription>))]
		FrontEndBottomDescriptionCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for OtherDescriptionLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<OtherDescriptionLanguage>))]
		OtherDescriptionLanguageCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for PackageMasterDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageMasterDescription>))]
		PackageMasterDescriptionCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for ZoneLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<ZoneLanguage>))]
		ZoneLanguageCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for CancelationPolicyLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<CancelationPolicyLanguage>))]
		CancelationPolicyLanguageCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for EmailConfigMappingCollection
		///</summary>
		[ChildEntityType(typeof(TList<EmailConfigMapping>))]
		EmailConfigMappingCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for MeetingRoomDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomDesc>))]
		MeetingRoomDescCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for ServeyAnswerDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<ServeyAnswerDescription>))]
		ServeyAnswerDescriptionCollection,

		///<summary>
		/// Collection of <c>Language</c> as OneToMany for CmsDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<CmsDescription>))]
		CmsDescriptionCollection,
	}
	
	#endregion LanguageChildEntityTypes
	
	#region LanguageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;LanguageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Language"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LanguageFilterBuilder : SqlFilterBuilder<LanguageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LanguageFilterBuilder class.
		/// </summary>
		public LanguageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LanguageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LanguageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LanguageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LanguageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LanguageFilterBuilder
	
	#region LanguageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;LanguageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Language"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LanguageParameterBuilder : ParameterizedSqlFilterBuilder<LanguageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LanguageParameterBuilder class.
		/// </summary>
		public LanguageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LanguageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LanguageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LanguageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LanguageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LanguageParameterBuilder
	
	#region LanguageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;LanguageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Language"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class LanguageSortBuilder : SqlSortBuilder<LanguageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LanguageSqlSortBuilder class.
		/// </summary>
		public LanguageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion LanguageSortBuilder
	
} // end namespace
