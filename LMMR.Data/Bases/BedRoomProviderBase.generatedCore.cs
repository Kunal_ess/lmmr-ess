﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BedRoomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BedRoomProviderBaseCore : EntityProviderBase<LMMR.Entities.BedRoom, LMMR.Entities.BedRoomKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BedRoomKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Hotel key.
		///		FK_BedRoom_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public TList<BedRoom> GetByHotelId(System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Hotel key.
		///		FK_BedRoom_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoom> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Hotel key.
		///		FK_BedRoom_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public TList<BedRoom> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Hotel key.
		///		fkBedRoomHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public TList<BedRoom> GetByHotelId(System.Int64 _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Hotel key.
		///		fkBedRoomHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public TList<BedRoom> GetByHotelId(System.Int64 _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Hotel key.
		///		FK_BedRoom_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public abstract TList<BedRoom> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Users key.
		///		FK_BedRoom_Users Description: 
		/// </summary>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public TList<BedRoom> GetByUpdatedBy(System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(_updatedBy, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Users key.
		///		FK_BedRoom_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoom> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Users key.
		///		FK_BedRoom_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public TList<BedRoom> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Users key.
		///		fkBedRoomUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public TList<BedRoom> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength)
		{
			int count =  -1;
			return GetByUpdatedBy(null, _updatedBy, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Users key.
		///		fkBedRoomUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public TList<BedRoom> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength,out int count)
		{
			return GetByUpdatedBy(null, _updatedBy, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Users key.
		///		FK_BedRoom_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoom objects.</returns>
		public abstract TList<BedRoom> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BedRoom Get(TransactionManager transactionManager, LMMR.Entities.BedRoomKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoom"/> class.</returns>
		public LMMR.Entities.BedRoom GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoom"/> class.</returns>
		public LMMR.Entities.BedRoom GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoom"/> class.</returns>
		public LMMR.Entities.BedRoom GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoom"/> class.</returns>
		public LMMR.Entities.BedRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoom"/> class.</returns>
		public LMMR.Entities.BedRoom GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoom"/> class.</returns>
		public abstract LMMR.Entities.BedRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BedRoom&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BedRoom&gt;"/></returns>
		public static TList<BedRoom> Fill(IDataReader reader, TList<BedRoom> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BedRoom c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BedRoom")
					.Append("|").Append((System.Int64)reader[((int)BedRoomColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BedRoom>(
					key.ToString(), // EntityTrackingKey
					"BedRoom",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BedRoom();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BedRoomColumn.Id - 1)];
					c.HotelId = (System.Int64)reader[((int)BedRoomColumn.HotelId - 1)];
					c.Name = (reader.IsDBNull(((int)BedRoomColumn.Name - 1)))?null:(System.String)reader[((int)BedRoomColumn.Name - 1)];
					c.Types = (reader.IsDBNull(((int)BedRoomColumn.Types - 1)))?null:(System.Int32?)reader[((int)BedRoomColumn.Types - 1)];
					c.Allotment = (reader.IsDBNull(((int)BedRoomColumn.Allotment - 1)))?null:(System.Int32?)reader[((int)BedRoomColumn.Allotment - 1)];
					c.Picture = (reader.IsDBNull(((int)BedRoomColumn.Picture - 1)))?null:(System.String)reader[((int)BedRoomColumn.Picture - 1)];
					c.IsActive = (System.Boolean)reader[((int)BedRoomColumn.IsActive - 1)];
					c.PriceDouble = (reader.IsDBNull(((int)BedRoomColumn.PriceDouble - 1)))?null:(System.Decimal?)reader[((int)BedRoomColumn.PriceDouble - 1)];
					c.PriceSingle = (reader.IsDBNull(((int)BedRoomColumn.PriceSingle - 1)))?null:(System.Decimal?)reader[((int)BedRoomColumn.PriceSingle - 1)];
					c.IsBreakFastInclude = (System.Boolean)reader[((int)BedRoomColumn.IsBreakFastInclude - 1)];
					c.BreakfastPrice = (reader.IsDBNull(((int)BedRoomColumn.BreakfastPrice - 1)))?null:(System.Decimal?)reader[((int)BedRoomColumn.BreakfastPrice - 1)];
					c.IsDeleted = (System.Boolean)reader[((int)BedRoomColumn.IsDeleted - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)BedRoomColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoom"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BedRoom entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BedRoomColumn.Id - 1)];
			entity.HotelId = (System.Int64)reader[((int)BedRoomColumn.HotelId - 1)];
			entity.Name = (reader.IsDBNull(((int)BedRoomColumn.Name - 1)))?null:(System.String)reader[((int)BedRoomColumn.Name - 1)];
			entity.Types = (reader.IsDBNull(((int)BedRoomColumn.Types - 1)))?null:(System.Int32?)reader[((int)BedRoomColumn.Types - 1)];
			entity.Allotment = (reader.IsDBNull(((int)BedRoomColumn.Allotment - 1)))?null:(System.Int32?)reader[((int)BedRoomColumn.Allotment - 1)];
			entity.Picture = (reader.IsDBNull(((int)BedRoomColumn.Picture - 1)))?null:(System.String)reader[((int)BedRoomColumn.Picture - 1)];
			entity.IsActive = (System.Boolean)reader[((int)BedRoomColumn.IsActive - 1)];
			entity.PriceDouble = (reader.IsDBNull(((int)BedRoomColumn.PriceDouble - 1)))?null:(System.Decimal?)reader[((int)BedRoomColumn.PriceDouble - 1)];
			entity.PriceSingle = (reader.IsDBNull(((int)BedRoomColumn.PriceSingle - 1)))?null:(System.Decimal?)reader[((int)BedRoomColumn.PriceSingle - 1)];
			entity.IsBreakFastInclude = (System.Boolean)reader[((int)BedRoomColumn.IsBreakFastInclude - 1)];
			entity.BreakfastPrice = (reader.IsDBNull(((int)BedRoomColumn.BreakfastPrice - 1)))?null:(System.Decimal?)reader[((int)BedRoomColumn.BreakfastPrice - 1)];
			entity.IsDeleted = (System.Boolean)reader[((int)BedRoomColumn.IsDeleted - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)BedRoomColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoom"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BedRoom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Types = Convert.IsDBNull(dataRow["Types"]) ? null : (System.Int32?)dataRow["Types"];
			entity.Allotment = Convert.IsDBNull(dataRow["Allotment"]) ? null : (System.Int32?)dataRow["Allotment"];
			entity.Picture = Convert.IsDBNull(dataRow["Picture"]) ? null : (System.String)dataRow["Picture"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.PriceDouble = Convert.IsDBNull(dataRow["PriceDouble"]) ? null : (System.Decimal?)dataRow["PriceDouble"];
			entity.PriceSingle = Convert.IsDBNull(dataRow["PriceSingle"]) ? null : (System.Decimal?)dataRow["PriceSingle"];
			entity.IsBreakFastInclude = (System.Boolean)dataRow["IsBreakFastInclude"];
			entity.BreakfastPrice = Convert.IsDBNull(dataRow["BreakfastPrice"]) ? null : (System.Decimal?)dataRow["BreakfastPrice"];
			entity.IsDeleted = (System.Boolean)dataRow["IsDeleted"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoom"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoom Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BedRoom entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelId;
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, entity.HotelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource

			#region UpdatedBySource	
			if (CanDeepLoad(entity, "Users|UpdatedBySource", deepLoadType, innerList) 
				&& entity.UpdatedBySource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UpdatedBy ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UpdatedBySource = tmpEntity;
				else
					entity.UpdatedBySource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UpdatedBy ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UpdatedBySource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UpdatedBySource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UpdatedBySource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UpdatedBySource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region BedRoomPictureImageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoomPictureImage>|BedRoomPictureImageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomPictureImageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomPictureImageCollection = DataRepository.BedRoomPictureImageProvider.GetByBedRoomId(transactionManager, entity.Id);

				if (deep && entity.BedRoomPictureImageCollection.Count > 0)
				{
					deepHandles.Add("BedRoomPictureImageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoomPictureImage>) DataRepository.BedRoomPictureImageProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomPictureImageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BedRoomTrailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoomTrail>|BedRoomTrailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomTrailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomTrailCollection = DataRepository.BedRoomTrailProvider.GetByBedroomId(transactionManager, entity.Id);

				if (deep && entity.BedRoomTrailCollection.Count > 0)
				{
					deepHandles.Add("BedRoomTrailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoomTrail>) DataRepository.BedRoomTrailProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomTrailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookedBedRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BookedBedRoom>|BookedBedRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookedBedRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookedBedRoomCollection = DataRepository.BookedBedRoomProvider.GetByBedRoomId(transactionManager, entity.Id);

				if (deep && entity.BookedBedRoomCollection.Count > 0)
				{
					deepHandles.Add("BookedBedRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BookedBedRoom>) DataRepository.BookedBedRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.BookedBedRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BedRoomDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoomDesc>|BedRoomDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomDescCollection = DataRepository.BedRoomDescProvider.GetByBedRoomId(transactionManager, entity.Id);

				if (deep && entity.BedRoomDescCollection.Count > 0)
				{
					deepHandles.Add("BedRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoomDesc>) DataRepository.BedRoomDescProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SpecialPriceForBedroomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SpecialPriceForBedroom>|SpecialPriceForBedroomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpecialPriceForBedroomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SpecialPriceForBedroomCollection = DataRepository.SpecialPriceForBedroomProvider.GetByBedroomId(transactionManager, entity.Id);

				if (deep && entity.SpecialPriceForBedroomCollection.Count > 0)
				{
					deepHandles.Add("SpecialPriceForBedroomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SpecialPriceForBedroom>) DataRepository.SpecialPriceForBedroomProvider.DeepLoad,
						new object[] { transactionManager, entity.SpecialPriceForBedroomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SpandPbedroomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SpandPbedroom>|SpandPbedroomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpandPbedroomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SpandPbedroomCollection = DataRepository.SpandPbedroomProvider.GetByBedRoomId(transactionManager, entity.Id);

				if (deep && entity.SpandPbedroomCollection.Count > 0)
				{
					deepHandles.Add("SpandPbedroomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SpandPbedroom>) DataRepository.SpandPbedroomProvider.DeepLoad,
						new object[] { transactionManager, entity.SpandPbedroomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BedRoom object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BedRoom instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoom Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BedRoom entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			
			#region UpdatedBySource
			if (CanDeepSave(entity, "Users|UpdatedBySource", deepSaveType, innerList) 
				&& entity.UpdatedBySource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UpdatedBySource);
				entity.UpdatedBy = entity.UpdatedBySource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<BedRoomPictureImage>
				if (CanDeepSave(entity.BedRoomPictureImageCollection, "List<BedRoomPictureImage>|BedRoomPictureImageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoomPictureImage child in entity.BedRoomPictureImageCollection)
					{
						if(child.BedRoomIdSource != null)
						{
							child.BedRoomId = child.BedRoomIdSource.Id;
						}
						else
						{
							child.BedRoomId = entity.Id;
						}

					}

					if (entity.BedRoomPictureImageCollection.Count > 0 || entity.BedRoomPictureImageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomPictureImageProvider.Save(transactionManager, entity.BedRoomPictureImageCollection);
						
						deepHandles.Add("BedRoomPictureImageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoomPictureImage >) DataRepository.BedRoomPictureImageProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomPictureImageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BedRoomTrail>
				if (CanDeepSave(entity.BedRoomTrailCollection, "List<BedRoomTrail>|BedRoomTrailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoomTrail child in entity.BedRoomTrailCollection)
					{
						if(child.BedroomIdSource != null)
						{
							child.BedroomId = child.BedroomIdSource.Id;
						}
						else
						{
							child.BedroomId = entity.Id;
						}

					}

					if (entity.BedRoomTrailCollection.Count > 0 || entity.BedRoomTrailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomTrailProvider.Save(transactionManager, entity.BedRoomTrailCollection);
						
						deepHandles.Add("BedRoomTrailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoomTrail >) DataRepository.BedRoomTrailProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomTrailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BookedBedRoom>
				if (CanDeepSave(entity.BookedBedRoomCollection, "List<BookedBedRoom>|BookedBedRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BookedBedRoom child in entity.BookedBedRoomCollection)
					{
						if(child.BedRoomIdSource != null)
						{
							child.BedRoomId = child.BedRoomIdSource.Id;
						}
						else
						{
							child.BedRoomId = entity.Id;
						}

					}

					if (entity.BookedBedRoomCollection.Count > 0 || entity.BookedBedRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookedBedRoomProvider.Save(transactionManager, entity.BookedBedRoomCollection);
						
						deepHandles.Add("BookedBedRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BookedBedRoom >) DataRepository.BookedBedRoomProvider.DeepSave,
							new object[] { transactionManager, entity.BookedBedRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BedRoomDesc>
				if (CanDeepSave(entity.BedRoomDescCollection, "List<BedRoomDesc>|BedRoomDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoomDesc child in entity.BedRoomDescCollection)
					{
						if(child.BedRoomIdSource != null)
						{
							child.BedRoomId = child.BedRoomIdSource.Id;
						}
						else
						{
							child.BedRoomId = entity.Id;
						}

					}

					if (entity.BedRoomDescCollection.Count > 0 || entity.BedRoomDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomDescProvider.Save(transactionManager, entity.BedRoomDescCollection);
						
						deepHandles.Add("BedRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoomDesc >) DataRepository.BedRoomDescProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SpecialPriceForBedroom>
				if (CanDeepSave(entity.SpecialPriceForBedroomCollection, "List<SpecialPriceForBedroom>|SpecialPriceForBedroomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SpecialPriceForBedroom child in entity.SpecialPriceForBedroomCollection)
					{
						if(child.BedroomIdSource != null)
						{
							child.BedroomId = child.BedroomIdSource.Id;
						}
						else
						{
							child.BedroomId = entity.Id;
						}

					}

					if (entity.SpecialPriceForBedroomCollection.Count > 0 || entity.SpecialPriceForBedroomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SpecialPriceForBedroomProvider.Save(transactionManager, entity.SpecialPriceForBedroomCollection);
						
						deepHandles.Add("SpecialPriceForBedroomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SpecialPriceForBedroom >) DataRepository.SpecialPriceForBedroomProvider.DeepSave,
							new object[] { transactionManager, entity.SpecialPriceForBedroomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SpandPbedroom>
				if (CanDeepSave(entity.SpandPbedroomCollection, "List<SpandPbedroom>|SpandPbedroomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SpandPbedroom child in entity.SpandPbedroomCollection)
					{
						if(child.BedRoomIdSource != null)
						{
							child.BedRoomId = child.BedRoomIdSource.Id;
						}
						else
						{
							child.BedRoomId = entity.Id;
						}

					}

					if (entity.SpandPbedroomCollection.Count > 0 || entity.SpandPbedroomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SpandPbedroomProvider.Save(transactionManager, entity.SpandPbedroomCollection);
						
						deepHandles.Add("SpandPbedroomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SpandPbedroom >) DataRepository.SpandPbedroomProvider.DeepSave,
							new object[] { transactionManager, entity.SpandPbedroomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BedRoomChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BedRoom</c>
	///</summary>
	public enum BedRoomChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UpdatedBySource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>BedRoom</c> as OneToMany for BedRoomPictureImageCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoomPictureImage>))]
		BedRoomPictureImageCollection,

		///<summary>
		/// Collection of <c>BedRoom</c> as OneToMany for BedRoomTrailCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoomTrail>))]
		BedRoomTrailCollection,

		///<summary>
		/// Collection of <c>BedRoom</c> as OneToMany for BookedBedRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<BookedBedRoom>))]
		BookedBedRoomCollection,

		///<summary>
		/// Collection of <c>BedRoom</c> as OneToMany for BedRoomDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoomDesc>))]
		BedRoomDescCollection,

		///<summary>
		/// Collection of <c>BedRoom</c> as OneToMany for SpecialPriceForBedroomCollection
		///</summary>
		[ChildEntityType(typeof(TList<SpecialPriceForBedroom>))]
		SpecialPriceForBedroomCollection,

		///<summary>
		/// Collection of <c>BedRoom</c> as OneToMany for SpandPbedroomCollection
		///</summary>
		[ChildEntityType(typeof(TList<SpandPbedroom>))]
		SpandPbedroomCollection,
	}
	
	#endregion BedRoomChildEntityTypes
	
	#region BedRoomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BedRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomFilterBuilder : SqlFilterBuilder<BedRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomFilterBuilder class.
		/// </summary>
		public BedRoomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomFilterBuilder
	
	#region BedRoomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BedRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomParameterBuilder : ParameterizedSqlFilterBuilder<BedRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomParameterBuilder class.
		/// </summary>
		public BedRoomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomParameterBuilder
	
	#region BedRoomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BedRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BedRoomSortBuilder : SqlSortBuilder<BedRoomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomSqlSortBuilder class.
		/// </summary>
		public BedRoomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BedRoomSortBuilder
	
} // end namespace
