﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MeetingRoomConfigTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MeetingRoomConfigTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.MeetingRoomConfigTrail, LMMR.Entities.MeetingRoomConfigTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.MeetingRoomConfigTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Trail_MeetingRoomConfig key.
		///		FK_MeetingRoomConfig_Trail_MeetingRoomConfig Description: 
		/// </summary>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfigTrail objects.</returns>
		public TList<MeetingRoomConfigTrail> GetByMeetingRoomId(System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(_meetingRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Trail_MeetingRoomConfig key.
		///		FK_MeetingRoomConfig_Trail_MeetingRoomConfig Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfigTrail objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomConfigTrail> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Trail_MeetingRoomConfig key.
		///		FK_MeetingRoomConfig_Trail_MeetingRoomConfig Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfigTrail objects.</returns>
		public TList<MeetingRoomConfigTrail> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Trail_MeetingRoomConfig key.
		///		fkMeetingRoomConfigTrailMeetingRoomConfig Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfigTrail objects.</returns>
		public TList<MeetingRoomConfigTrail> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Trail_MeetingRoomConfig key.
		///		fkMeetingRoomConfigTrailMeetingRoomConfig Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfigTrail objects.</returns>
		public TList<MeetingRoomConfigTrail> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength,out int count)
		{
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Trail_MeetingRoomConfig key.
		///		FK_MeetingRoomConfig_Trail_MeetingRoomConfig Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfigTrail objects.</returns>
		public abstract TList<MeetingRoomConfigTrail> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.MeetingRoomConfigTrail Get(TransactionManager transactionManager, LMMR.Entities.MeetingRoomConfigTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_MeetingRoomConfig_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfigTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfigTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfigTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfigTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfigTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> class.</returns>
		public abstract LMMR.Entities.MeetingRoomConfigTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MeetingRoomConfigTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MeetingRoomConfigTrail&gt;"/></returns>
		public static TList<MeetingRoomConfigTrail> Fill(IDataReader reader, TList<MeetingRoomConfigTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.MeetingRoomConfigTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MeetingRoomConfigTrail")
					.Append("|").Append((System.Int64)reader[((int)MeetingRoomConfigTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MeetingRoomConfigTrail>(
					key.ToString(), // EntityTrackingKey
					"MeetingRoomConfigTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.MeetingRoomConfigTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)MeetingRoomConfigTrailColumn.Id - 1)];
					c.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomConfigTrailColumn.MeetingRoomId - 1)];
					c.RoomShapeId = (System.Int32)reader[((int)MeetingRoomConfigTrailColumn.RoomShapeId - 1)];
					c.MinCapacity = (System.Int32)reader[((int)MeetingRoomConfigTrailColumn.MinCapacity - 1)];
					c.MaxCapicity = (System.Int32)reader[((int)MeetingRoomConfigTrailColumn.MaxCapicity - 1)];
					c.HotelId = (System.Int64)reader[((int)MeetingRoomConfigTrailColumn.HotelId - 1)];
					c.UpdatedBy = (System.Int64)reader[((int)MeetingRoomConfigTrailColumn.UpdatedBy - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)MeetingRoomConfigTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)MeetingRoomConfigTrailColumn.UpdateDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.MeetingRoomConfigTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)MeetingRoomConfigTrailColumn.Id - 1)];
			entity.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomConfigTrailColumn.MeetingRoomId - 1)];
			entity.RoomShapeId = (System.Int32)reader[((int)MeetingRoomConfigTrailColumn.RoomShapeId - 1)];
			entity.MinCapacity = (System.Int32)reader[((int)MeetingRoomConfigTrailColumn.MinCapacity - 1)];
			entity.MaxCapicity = (System.Int32)reader[((int)MeetingRoomConfigTrailColumn.MaxCapicity - 1)];
			entity.HotelId = (System.Int64)reader[((int)MeetingRoomConfigTrailColumn.HotelId - 1)];
			entity.UpdatedBy = (System.Int64)reader[((int)MeetingRoomConfigTrailColumn.UpdatedBy - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)MeetingRoomConfigTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)MeetingRoomConfigTrailColumn.UpdateDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.MeetingRoomConfigTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.MeetingRoomId = (System.Int64)dataRow["MeetingRoom_Id"];
			entity.RoomShapeId = (System.Int32)dataRow["RoomShapeId"];
			entity.MinCapacity = (System.Int32)dataRow["MinCapacity"];
			entity.MaxCapicity = (System.Int32)dataRow["MaxCapicity"];
			entity.HotelId = (System.Int64)dataRow["HotelID"];
			entity.UpdatedBy = (System.Int64)dataRow["UpdatedBy"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomConfigTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomConfigTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.MeetingRoomConfigTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region MeetingRoomIdSource	
			if (CanDeepLoad(entity, "MeetingRoomConfig|MeetingRoomIdSource", deepLoadType, innerList) 
				&& entity.MeetingRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MeetingRoomId;
				MeetingRoomConfig tmpEntity = EntityManager.LocateEntity<MeetingRoomConfig>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoomConfig), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MeetingRoomIdSource = tmpEntity;
				else
					entity.MeetingRoomIdSource = DataRepository.MeetingRoomConfigProvider.GetById(transactionManager, entity.MeetingRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MeetingRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomConfigProvider.DeepLoad(transactionManager, entity.MeetingRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MeetingRoomIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.MeetingRoomConfigTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.MeetingRoomConfigTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomConfigTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.MeetingRoomConfigTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region MeetingRoomIdSource
			if (CanDeepSave(entity, "MeetingRoomConfig|MeetingRoomIdSource", deepSaveType, innerList) 
				&& entity.MeetingRoomIdSource != null)
			{
				DataRepository.MeetingRoomConfigProvider.Save(transactionManager, entity.MeetingRoomIdSource);
				entity.MeetingRoomId = entity.MeetingRoomIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MeetingRoomConfigTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.MeetingRoomConfigTrail</c>
	///</summary>
	public enum MeetingRoomConfigTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>MeetingRoomConfig</c> at MeetingRoomIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoomConfig))]
		MeetingRoomConfig,
		}
	
	#endregion MeetingRoomConfigTrailChildEntityTypes
	
	#region MeetingRoomConfigTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MeetingRoomConfigTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfigTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomConfigTrailFilterBuilder : SqlFilterBuilder<MeetingRoomConfigTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailFilterBuilder class.
		/// </summary>
		public MeetingRoomConfigTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomConfigTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomConfigTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomConfigTrailFilterBuilder
	
	#region MeetingRoomConfigTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MeetingRoomConfigTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfigTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomConfigTrailParameterBuilder : ParameterizedSqlFilterBuilder<MeetingRoomConfigTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailParameterBuilder class.
		/// </summary>
		public MeetingRoomConfigTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomConfigTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomConfigTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomConfigTrailParameterBuilder
	
	#region MeetingRoomConfigTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MeetingRoomConfigTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfigTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MeetingRoomConfigTrailSortBuilder : SqlSortBuilder<MeetingRoomConfigTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailSqlSortBuilder class.
		/// </summary>
		public MeetingRoomConfigTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MeetingRoomConfigTrailSortBuilder
	
} // end namespace
